# Tafel

Bot Studio and NL Studio

## Getting Started

### Prerequisites

- Node (>=10.0.0)
- npm (>=6.0.0)

### Running a dev server

- `npm install` - this takes forever, as you know
- `npm start`

### Environtment on Development

`.env.local` contains the key / service token in the development environment

- `cp .env .env.local`
- `source .env.local`

```
REACT_APP_RUNTIME_WITH_CMS=true
REACT_APP_RUNTIME_CMS_NEW_ELEMENTS=true
REACT_APP_RUNTIME_ZAUN_URL=http://zaun.katalabs.io
REACT_APP_RUNTIME_EMPFANG_URL=https://user.katalabs.io
REACT_APP_RUNTIME_DOCS_URL=https://docs.kata.ai
REACT_APP_RUNTIME_BOTSTUDIO_TUTORIAL_URL=https://docs.kata.ai/tutorial/bot-studio/
REACT_APP_RUNTIME_NLSTUDIO_TUTORIAL_URL=https://docs.kata.ai/tutorial/nl-studio/
REACT_APP_RUNTIME_NEWS_30_CAST=true
REACT_APP_RUNTIME_NEWS_25_CAST=true
REACT_APP_RUNTIME_NEWS_25_BAR=false
REACT_APP_RUNTIME_NEWS_URL=https://docs.kata.ai/overview/release-notes/
REACT_APP_RUNTIME_SURVEY_25_CAST=false
REACT_APP_RUNTIME_MAINTENANCE=false
REACT_APP_RUNTIME_SENTRY_DSN=https://99a7413812854693ac129e26a79a97bd@sentry.io/1341667
REACT_APP_RUNTIME_SURVEY_URL=https://docs.google.com/forms/d/e/1FAIpQLScNAovi259qYUU786A0qu5bZ6D2SCQuAdqCmhEbdCdZruggkg/viewform
REACT_APP_RUNTIME_GA_TRACKING_CODE=
```

### Building for production

- `npm run build`

### Running unit/integration tests

- `npm test`

### Analyzing bundles

- `npm run analyze` - This will be generated on `webpack-bundle-analyzer` folder.

## Links

- [Changelog](CHANGELOG.md)
- [Contributing guidelines](CONTRIBUTING.md)
- [TSLint config](https://github.com/kata-ai/tslint-config-kata)
- [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0-beta.2/)
