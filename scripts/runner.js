const createTestCafe = require('testcafe');
const glob = require('fast-glob');
const { choosePort } = require('react-dev-utils/WebpackDevServerUtils');

const env = process.env.NODE_ENV || 'development';
const defaultPort = 1337;
let serverUrl = '';

if (env === 'production') {
  serverUrl = process.env.TEST_URL_PROD;
} else if (env === 'CI') {
  serverUrl = process.env.TEST_URL_CI;
} else {
  serverUrl = 'localhost';
}

let testcafe;

// Get files from given pattern
function getFiles(globPattern) {
  const pattern = Array.isArray(globPattern) ? globPattern : [globPattern];

  return glob.sync(pattern);
}

function startTest(browsers, dirPattern) {
  const files = getFiles(dirPattern);

  // Choose test server port first before running the test server
  return choosePort(serverUrl, defaultPort)
    .then(port => {
      if (!port || port === null) {
        console.error(
          `Can't find any port from ${defaultPort} to ${defaultPort + 10}`
        );

        process.exit(1);
      }

      console.log(`Running test server on ${serverUrl}:${port}`);

      return createTestCafe(serverUrl, port);
    })
    .then(tc => {
      testcafe = tc;

      return tc
        .createRunner()
        .src(files)
        .browsers(browsers)
        .run();
    })
    .then(failedCount => {
      console.log('Tests failed: ' + failedCount);
      testcafe.close();
    })
    .catch(err => {
      console.error(err);
      testcafe.close();
    });
}

module.exports = startTest;
module.exports.default = startTest;
