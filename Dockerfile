# Dockerfile

## Build process
FROM node:lts AS build-env
LABEL maintainer="kata.ai"

ENV NPM_TOKEN 870f6d8b-990c-468d-b87e-d19a1bc1efa0

# Set environment variables
ENV APPDIR /code

# Set the work directory
RUN mkdir -p ${APPDIR}
WORKDIR ${APPDIR}

ADD . ${APPDIR}

# Run build script
COPY ./docker/.npmrc .
COPY ./docker/config.json.template ./config.json
COPY ./docker/config.ext.json.template ./config.ext.json

RUN npm install
RUN node scripts/build.js

ARG NODE_ENV
ENV NODE_ENV ${NODE_ENV:-production}

RUN rm .npmrc
RUN rm -rf docker

## Run process
FROM node:lts-alpine

# Set environment variables
ENV APPDIR /code

# Set the work directory
RUN mkdir -p ${APPDIR}
WORKDIR ${APPDIR}

COPY --from=build-env /code/package*.json /code/server.js ./
COPY --from=build-env /code/build /code/build

RUN npm install --production

EXPOSE 8080
