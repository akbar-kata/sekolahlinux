# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## [Unreleased]

## [3.0.4] - 2019/01/16

### Bug fixes

- **botstudio:** keep mapping options when switching mode in transition (pull request #893)

## [3.0.3] - 2019/01/03

### Features

- **sentry:** integration sentry on CMS Studio (pull request #870)

### Bug fixes

- unhide element FAQ in cms form builder (pull request #868)
- fix Entity Name out of drawer when name is too long and add entity name validation (pull request #871)
- **utils:** updated env fallback handling

### Chores

- **cra:** upgrade (forcefully) to create-react-app 2.1 (pull request #863)
- **prettier:** added prettierignore
- runtime.js -> runtime-envs.js

### CI

- **bitbucket:** fixed react-app-runtime variables

### Refactors

- refactor react form in NL Studio to Formik (pull request #867)
- react form in team and account modules to formik (pull request #869)

## [3.0.2] - 2018/12/21

- **project:** fixed project env table overflowing (pull request #859)
- **cms:** make label only accepts alphanumeric (pull request #858)
- **deployments:** add copy button into webhook in channel list (pull request #860)
- **deployments:** change style copy button & add tooltip on hover in Channel List Page (pull request #861)
- **core:** fixed Platform 3.0 keeps appearing on first login (pull request #864)
- **botstudio:** fixed not being able to select method in Method action (pull request #865)
- debounce ReactSelect async search inputs by 500ms (pull request #866)

## [3.0.1] - 2018/12/12

- All-new Projects System
- All-new CMS Builder
- Integrate Kata Design Language System (DLS) to styles
- Improved Deployments flow
  - All-new Environments, easy managing of your bot versions
- Initial skeleton for Modules page
- Tech debt reductions (replaced some forms with Formik)

## [2.6.8] - 2018/19/22

- new improvement invite user/member to team
- fix show previews more than 4 labels
- fix error message when user getting blocked

## [2.6.7] - 2018/09/28

- Fix Google Tag Manager's invalid src

## [2.6.6] - 2018/09/27

- fix state form initial state validation

## [2.6.5] - 2018/09/27

- add Google Tag Manager's tag

## [2.6.4] - 2018/09/27

- disable popup new cast & survey bar

## [2.6.3] - 2018/09/19

- fix clear bot error log when switch bot

## [2.6.2] - 2018/08/16

- fix create team member
- fix get team member by teamId

## [2.6.1] - 2018/08/16

- fix animation on login & forgot password page
- fix set otp on change password

## [2.6.0] - 2018/08/15

- Add modal survey
- Revamp UX account page
- Revamp analytics
- Bugfixing pentest result
- Revamp design trancript
- Add landing page on login
- Fix FAQs Drawer
- Revamp style login page
- Fix form class error
- Fix delete action
- Fix save bot config

## [2.5.2] - 2018/07/26

### What's new:

- Add Twitter channel (pull request #565)
- Show carousel payload in key/value format (pull request #558)
- Open an action then other actions automatically close (pull request #561)
- Show dynamic carousel payload in key/value format (pull request #559)
- Add key/value to button action payload (pull request #554)

### Fixes:

- Updated Login page flow (pull request #556)
- Re-issue token NLU (pull request #543)
- Add id to train payload (pull request #568)
- CMS client fix unused label (pull request #571)

## [2.5.1] - 2018/07/19

### Fixes:

- Change not supported message type copy. (pull request #548)
- Fix error logs next page replace data (pull request #546)
- Upsize pipeline (pull request #547)
- Keep user token in cookie when "remember me" is checked (pull request #545)
- Get latest version when publish bot (pull request #538)
- Update env (pull request #537)
- Use NPM, upgrade some packages version, fix some files typing (pull request #536)
- increase per-page limit of nlstudio dashboard (pull request #535)

## [2.5.0] - 2018/07/12

### What's new:

- New Look and Feel
- Botstudio Dashboard
- Bot Error Log
- NLStudio Dashboard
- CMS ModuleDashboard

## [1.7.6] - 2018/05/31

- show confidence level trait on predict data training

## [1.7.5] - 2018/05/30

- Add english profiles on NLStudio

## [1.7.4] - 2018/05/08

- Handle if transcript startTime is string
- Fix display deployment and channel selector

## [1.7.3] - 2018/05/07

- Fix analytics payload

## [1.7.2] - 2018/04/18

- NLStudio can't create new entity

## [1.7.1] - 2018/03/09

- Clear tag entity
- Fix overlaping label

## [1.7.0] - 2018/03/02

- Improve UI NLStudio
- Improve annotation expand word boundary
- Feature NL Studio inherit
- Display facebook challenge token
- change qiscus channel APP_ID to UR
- add channel filter to analytic
- Preserve selected bot/nlu when reload/open new tab
- NLU selector pagination/load more
- Bot selector pagination/load more
- Reset state after user logout
- Fix display many word in dictionary
- Fix unwanted mutation in train text box
- Fix params user for delete entity
- Fix - Channel selector not properly hide when channel is empty

## [1.6.5] - 2018/02/07

### Hotfix

- Fix params user for delete entity

## [1.6.4] - 2018/02/07

### Hotfix

- Fix annotation and form not sync because unwanted data mutation

## [1.6.3] - 2018/02/07

### Hotfix

- Improve annotation word boundary
- Fix display many word in dictionary and label
- Fix display nlu entity when switch team

## [1.6.2] - 2018/02/06

### What's new:

- Forgot password auto redirect to change password
  ### Improvements:
- NL Studio
  - Handle overlaping annotation
  - Prevent empty string in training
  - Add belongs to in nlstudio log
  - Improve multiline annotation
  - Show root label
    ### Fixes:
- Botstudio
  - Fix parsing quote in kata ML
  - Change single quote to double quote
  - Handle custom and missing attribute in kata ML
  - Trim unused space in kata ML
- Change password field type
- Fix NLU selector when switch to team

## [1.6.1] - 2018/01/24

### What's new:

- NL Studio
  - Improve log styling to be consistent with train style
  - Display corrected log annotaion
    ### Fixes:
- NL Studio
  - Fix belongs to value from add training data
  - Fix add training data when data less than 1 page
  - Fix phrase saving when phrase does not have labels
  - Fix entities belongs to only to dict or phrase
  - Fix clear all entities in log form

## [1.6.0] - 2018/01/23

### What's new:

- Notification system using reapop
  ### Improvements:
- Bot Studio
  - implement JSON mode for Kata ML in version
  - add button download for Kata ML version
  - selective notification for saving draft
- NL Studio
  - add loading indicator when waiting for prediction
  - add new train data without refresh entire page
  - improve annotation selection to expand word boundary
  - improve annotation selection to through another annotated words
    ### Fixes:
- Server
  - fix `bundle-globals.js` mime/type to `application/javascript`
  - fix default api url when env var not available
- NL Studio
  - reset prediction result in every submit new sentence
  - fix belongsTo selection mechanism so it will not destroy annotaion
  - fix display label for entity type phrase
  - fix separation trait from entities for further manipulation
  - resolved all DOM validation error
  - fix count total page in train list

## [1.5.1] - 2018/01/02

### Hotfix:

- Fix bug on analytics not updating properly

## [1.5.0] - 2017/12/27

### Improvements:

- Render auto predict train data
- Handle ner label
- Add button docs menu in sidebar
- Add forgot password page
- Re-styling create first bot
- Add onboarding platform
  ### Fixes:
- Fix UI list, item & predict training
- Fix UI selection tags

## [1.4.1] - 2017/12/12

- Remove usage and billing menu from settings

## [1.4.0]

- Change api url to api.kata.ai
- Bostudio
  - validation UTF8
  - fix condition parsing
- Bot transcript improvement
  - display chat type image
  - add jump to time feature
  - fix timezone
  - fix ordering
- NlStudio improvement
  - enable add label and dictionary to entity
  - improve UX in trainig page
  - add training log page
  - enable training entity belongs to

## [1.3.0]

- Botstudio validation improvement

## [1.2.1] - 2017/11/21

- Format code
- Add 'ERROR' as exception to save draft
- Format code
- Fix condition parsing to display
- Add handler when fetch bot draft is empty

## [1.2.0] - 2017/11/20

- Add Internal analytics
- Add pricing page
- Add account management page
- Add team management page
- Add transcript page
- Botstudio Bugs Fix:
  - fix simple actions
  - add drag n drop state actions
  - allow action to have multiple text
  - add auto refresh after publishing bot
  - fix stringify condition
  - allow use same name if flow different
  - add handler when get unexpected server response
  - allow create NLU with multiple regex
  - fix action scheduler when using remove command
  - fix action scheduler start, end format
  - add delete flow feature

## [1.1.0] - 2017/11/10

### Improvements:

- Add signup page
- Integrate google analytics
- Integram switch user / team

## [1.0.0] - 2017/10/23

- Initial deployment

[unreleased]: https://bitbucket.org/yesboss/tafel/branches/compare/HEAD..v3.0.4#diff
[3.0.4]: https://bitbucket.org/yesboss/tafel/branches/compare/v3.0.4..v3.0.3#diff
[3.0.3]: https://bitbucket.org/yesboss/tafel/branches/compare/v3.0.3..v3.0.2#diff
[3.0.2]: https://bitbucket.org/yesboss/tafel/branches/compare/v3.0.2..v3.0.1#diff
[3.0.1]: https://bitbucket.org/yesboss/tafel/branches/compare/v3.0.1..v2.6.8#diff
[2.6.8]: https://bitbucket.org/yesboss/tafel/branches/compare/v2.6.8..v2.6.7#diff
[2.6.7]: https://bitbucket.org/yesboss/tafel/branches/compare/v2.6.7..v2.6.6#diff
[2.6.6]: https://bitbucket.org/yesboss/tafel/branches/compare/v2.6.6..v2.6.5#diff
[2.6.5]: https://bitbucket.org/yesboss/tafel/branches/compare/v2.6.5..v2.6.4#diff
[2.6.4]: https://bitbucket.org/yesboss/tafel/branches/compare/v2.6.4..v2.6.3#diff
[2.6.3]: https://bitbucket.org/yesboss/tafel/branches/compare/v2.6.3..v2.6.2#diff
[2.6.2]: https://bitbucket.org/yesboss/tafel/branches/compare/v2.6.2..v2.6.1#diff
[2.6.1]: https://bitbucket.org/yesboss/tafel/branches/compare/v2.6.1..v2.6.0#diff
[2.6.0]: https://bitbucket.org/yesboss/tafel/branches/compare/v2.6.0..v2.5.2#diff
[2.5.2]: https://bitbucket.org/yesboss/tafel/branches/compare/v2.5.2..v2.5.1#diff
[2.5.1]: https://bitbucket.org/yesboss/tafel/branches/compare/v2.5.1..v2.5.0#diff
[2.5.0]: https://bitbucket.org/yesboss/tafel/branches/compare/v2.5.0..v1.7.5#diff
[1.7.5]: https://bitbucket.org/yesboss/tafel/branches/compare/v1.7.5..v1.7.4#diff
[1.7.4]: https://bitbucket.org/yesboss/tafel/branches/compare/v1.7.4..v1.7.3#diff
[1.7.3]: https://bitbucket.org/yesboss/tafel/branches/compare/v1.7.3..v1.7.2#diff
[1.7.2]: https://bitbucket.org/yesboss/tafel/branches/compare/v1.7.2..v1.7.1#diff
[1.7.1]: https://bitbucket.org/yesboss/tafel/branches/compare/v1.7.1..v1.7.0#diff
[1.7.0]: https://bitbucket.org/yesboss/tafel/branches/compare/v1.7.0..v1.6.5#diff
[1.6.5]: https://bitbucket.org/yesboss/tafel/branches/compare/v1.6.5..v1.6.4#diff
[1.6.4]: https://bitbucket.org/yesboss/tafel/branches/compare/v1.6.4..v1.6.3#diff
[1.6.3]: https://bitbucket.org/yesboss/tafel/branches/compare/v1.6.3..v1.6.2#diff
[1.6.2]: https://bitbucket.org/yesboss/tafel/branches/compare/v1.6.2..v1.6.1#diff
[1.6.1]: https://bitbucket.org/yesboss/tafel/branches/compare/v1.6.1..v1.6.0#diff
[1.6.0]: https://bitbucket.org/yesboss/tafel/branches/compare/v1.6.0..v1.5.1#diff
[1.5.1]: https://bitbucket.org/yesboss/tafel/branches/compare/v1.5.1..v1.5.0#diff
[1.5.0]: https://bitbucket.org/yesboss/tafel/branches/compare/v1.5.0..v1.4.1#diff
[1.4.1]: https://bitbucket.org/yesboss/tafel/branches/compare/v1.4.1..v1.4.0#diff
[1.4.0]: https://bitbucket.org/yesboss/tafel/branches/compare/v1.4.0..v1.3.0#diff
[1.3.0]: https://bitbucket.org/yesboss/tafel/branches/compare/v1.3.0..v1.2.1#diff
[1.2.1]: https://bitbucket.org/yesboss/tafel/branches/compare/v1.2.1..v1.2.0#diff
[1.2.0]: https://bitbucket.org/yesboss/tafel/branches/compare/v1.2.0..v1.1.0#diff
[1.1.0]: https://bitbucket.org/yesboss/tafel/branches/compare/v1.1.0..v1.0.0#diff
