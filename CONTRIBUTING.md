# How to Contribute

## Project Architecture

### Tooling

The project runs on an ejected `create-react-app` configuration. We based our setup on [react-scripts version 4.0.8](https://github.com/wmonk/create-react-app-typescript/tree/wm-react-scripts-2.x), a **beta build** that supports Webpack 4.

### Folder Structure

Please take a close look at the following folder structures.

#### Global

```
src/
├── assets/
├── components/
│   ├── [component_name]/
│   │   ├── index.ts
│   │   └── (something).tsx
│   └── [component_name]/
│       ├── index.ts
│       └── (something).tsx
├── containers/
├── interfaces/
├── modules/
│   ├── [module_name]/
│   │   └── (something).{ts,tsx}
│   └── [module_name]/
│       └── (something).{ts,tsx}
├── stores/
├── utils/
├── index.tsx
└── (something).{ts,tsx}
```

- **assets:** Assets files (images, CSS, etc.), importable via webpack.
- **components:** Reusable component set.
- **containers:** Container components used to connect to the Redux store
- **interfaces:** Where we store reusable TypeScript types.
- **modules:** The core modules in the app. These are arranged in a code-splittable way.
- **stores:** Redux store.
- **utils:** Reusable utility functions.

## Commit message guidelines

This commit guidelines is based on the [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0-beta.2/) guidelines. Please read it carefully.

When merging pull requests, squash feature branches onto `develop` and write a standardized commit message while doing so.

The commit message should be structured as follows:

```
<type>(<optional scope>): <subject>
<BLANK LINE>
<body>
<BLANK LINE>
<footer>
```

The **header** is mandatory and the **scope** of the header is optional.

Any line of the commit message cannot be longer than 72 characters! This allows the message to be easier to read on GitHub as well as in various git tools.

The footer should contain a closing reference to an issue if available (e.g. `Fixes PLATFORM-1024`).

### Revert

If the commit reverts a previous commit, it should begin with `revert:`, followed by the header of the reverted commit. In the body it should say: `This reverts commit <hash>.`, where the hash is the SHA of the commit being reverted.

### Type

Must be one of the following:

- **build:** Changes that affect the build system or external dependencies (example scopes: gulp, broccoli, npm)
- **chore:** Other changes that don't modify src or test files
- **ci:** Changes to our CI configuration files and scripts (example scopes: travis, circle, bitbucket)
- **docs:** Documentation only changes
- **feat:** A new feature
- **fix:** A bug fix
- **perf:** A code change that improves performance
- **refactor:** A code change that neither fixes a bug nor adds a feature
- **style:** Changes that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc)
- **test:** Adding missing tests or correcting existing tests

### Scope

The scope should be the name of the module affected (as perceived by the person reading the changelog generated from commit messages).

Supported scopes are the names of the top-level folders inside the `modules/` directory. As the time of writing, they're:

- **admin**
- **analytic**
- **auth**
- **botstudio**
- **cms**
- **core**
- **deployments**
- **maintenance**
- **module**
- **nlstudio**
- **project**
- **setting**

### Examples

For more examples, try [here](https://github.com/angular/angular/commits/master).

#### Commit message with body

```
fix(release): need to depend on latest rxjs and zone.js

The version in our package.json gets copied to the one we publish, and users need the latest of these.
```

#### Commit message with description and breaking change in body

```
feat: allow provided config object to extend other configs

BREAKING CHANGE: `extends` key in config file is now used for extending other config files.
```

#### Commit message with no body

```
docs: added contributing guidelines
```

#### Commit message with scope

```
feat(botstudio): improved lazyloading of components
```

```
docs(changelog): update changelog with unreleased stuff
```

```
chore(release): 3.0.1
```

#### Commit message for a fix using an (optional) issue number.

```
fix: minor typos in code

See the issue for details on the typos fixed.

Fixes PLATFORM-1024
```
