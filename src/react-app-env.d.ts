/// <reference types="react-scripts" />

interface Window {
  /** Extended initial Redux store */
  __INITIAL_REDUX_STATE__: any;
  /** Runtime env variables */
  __REACT_APP_RUNTIME__: Record<string, string>;
}
