import COMMON from './types';

export function request<T>(type: string, data?: T) {
  return {
    type: type + COMMON._REQUEST,
    payload: data
  };
}

export function success<T>(type: string, data?: T) {
  return {
    type: type + COMMON._SUCCESS,
    payload: data
  };
}

export function failed<T>(type: string, data?: T) {
  return {
    type: type + COMMON._FAILED,
    payload: data
  };
}

export function build(state: string, type: string) {
  return type + state;
}
