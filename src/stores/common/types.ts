const COMMON = {
  _REQUEST: '_REQUEST',
  _SUCCESS: '_SUCCESS',
  _FAILED: '_FAILED'
};

export default COMMON;
