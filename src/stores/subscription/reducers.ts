import { combineReducers } from 'redux';

import { ReduxAction } from 'interfaces';
import { Store as SubscriptionStore } from 'interfaces/subscription';
import SUBSCRIPTION from './types';

const loadingInit = {
  fetch: false,
  add: false,
  update: false,
  remove: false
};

const loadings = (
  state: SubscriptionStore['loadings'] = loadingInit,
  { type, action, payload }: ReduxAction
) => {
  const actionCaps = action ? action.toUpperCase() : 'FETCH';
  switch (type) {
    case SUBSCRIPTION[`${actionCaps}_REQUEST`]:
      return Object.assign({}, state, {
        [action as string]: true
      });
    case SUBSCRIPTION[`${actionCaps}_SUCCESS`]:
    case SUBSCRIPTION[`${actionCaps}_FAIL`]:
      return Object.assign({}, state, {
        [action as string]: false
      });
    default:
      return state;
  }
};

const errorsInit = {
  fetch: null,
  add: null,
  update: null,
  remove: null
};

const errors = (
  state: SubscriptionStore['errors'] = errorsInit,
  { type, action, payload }: ReduxAction
) => {
  const actionCaps = action ? action.toUpperCase() : 'FETCH';
  switch (type) {
    case SUBSCRIPTION.ERROR_CLEAR:
    case SUBSCRIPTION[`${actionCaps}_REQUEST`]:
      return Object.assign({}, state, {
        [action as string]: null
      });
    case SUBSCRIPTION[`${actionCaps}_FAIL`]:
      return Object.assign({}, state, {
        [action as string]: payload.error
      });
    default:
      return state;
  }
};

const selected = (
  state: SubscriptionStore['selected'] = null,
  { type, payload }: ReduxAction
) => {
  switch (type) {
    case SUBSCRIPTION.SELECT:
      return payload;
    default:
      return state;
  }
};

const data = (
  state: SubscriptionStore['data'] = {},
  { type, payload }: ReduxAction
) => {
  switch (type) {
    case SUBSCRIPTION.FETCH_SUCCESS:
      return Object.assign({}, payload.data);
    case SUBSCRIPTION.ADD_SUCCESS:
    case SUBSCRIPTION.UPDATE_SUCCESS:
      return Object.assign({}, state, payload);
    case SUBSCRIPTION.REMOVE_SUCCESS:
      const { [payload.id]: deletedItem, ...rest } = state;
      return rest;
    default:
      return state;
  }
};

const index = (
  state: SubscriptionStore['index'] = [],
  { type, payload }: ReduxAction
) => {
  switch (type) {
    case SUBSCRIPTION.FETCH_SUCCESS:
      return [...payload.index];
    case SUBSCRIPTION.ADD_SUCCESS:
      return [...state, ...Object.keys(payload)];
    case SUBSCRIPTION.REMOVE_SUCCESS:
      return state.filter(id => id !== payload.id);
    default:
      return state;
  }
};

const lastUpdate = (
  state: SubscriptionStore['lastUpdate'] = Date.now(),
  { type, payload }: ReduxAction
) => {
  switch (type) {
    default:
      return state;
  }
};

export default combineReducers<SubscriptionStore>({
  selected,
  index,
  data,
  loadings,
  errors,
  lastUpdate
});
