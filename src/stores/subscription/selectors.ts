import * as Subscription from 'interfaces/subscription';

export function getSubscriptionLoading(
  state: Subscription.Store,
  type: string
): boolean {
  return state.loadings[type];
}

export function getSubscriptionError(
  state: Subscription.Store,
  type: string
): string | null {
  return state.errors[type];
}

export function getSubscriptionSelected(
  state: Subscription.Store
): string | null {
  return state.selected;
}

export function getSubscriptionLastUpdated(state: Subscription.Store): number {
  return state.lastUpdate;
}

export function getSubscriptionIndex(state: Subscription.Store): string[] {
  return state.index;
}

export function getSubscriptionData(
  state: Subscription.Store
): Subscription.DataMap {
  return state.data;
}

export function getSubscriptionDetail(
  state: Subscription.Store,
  subscriptionId: string
): Subscription.Data {
  return state.data[subscriptionId];
}
