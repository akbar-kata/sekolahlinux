import { all, fork, put, takeLatest } from 'redux-saga/effects';
import shortid from 'shortid';

import {
  subscriptionFail,
  selectSubscription,
  fetchSubscriptionSuccess,
  addSubscriptionSuccess,
  updateSubscriptionSuccess,
  removeSubscriptionSuccess
} from './actions';
import { closeModal } from 'stores/app/modal/actions';
// import { callApi } from 'stores/services';
import { ReduxAction } from 'interfaces';
import SUBSCRIPTION from './types';
import { addNotification } from 'stores/app/notification/actions';

function* fetchSubscription({ payload }: ReduxAction) {
  try {
    // const data = yield call(callApi, 'get', `/bots/${botId}/deployments?limit=10000000`);
    const data = [
      {
        id: 'd0d13c55-f67f-4a50-aa31-93806fcb28f7',
        deploymentId: 'd0d13c55-f67f-4a50-aa31-93806fkij89',
        active: 1,
        usages: {
          counters: {
            messages: 0,
            sessions: 0,
            maus: 0,
            users: 0
          },
          startDate: 1508146091000,
          endDate: 1508146098000
        },
        plans: [{}],
        rpmLimit: 100,
        startDate: 1508146091000,
        endDate: 1508146098000,
        kpi: '',
        label: 'subscriptionKu'
      },
      {
        id: 'd0d13c55-90di-4a50-hjd8-93806fcb28f7',
        deploymentId: 'd0d13c55-f67f-4a50-aa31-93806fkij89',
        active: 1,
        usages: {
          counters: {
            messages: 0,
            sessions: 0,
            maus: 0,
            users: 0
          },
          startDate: 1508146091000,
          endDate: 1508146098000
        },
        plans: [{}],
        rpmLimit: 100,
        startDate: 1508146091000,
        endDate: 1508146098000,
        kpi: '',
        label: 'label subscription'
      }
    ];
    yield put(fetchSubscriptionSuccess(data));
    if (data.length > 0) {
      yield put(selectSubscription(data[0].id));
    }
  } catch (err) {
    yield put(subscriptionFail('fetch', err.message));
  }
}

function* addSubscription({ payload: { data } }: ReduxAction) {
  try {
    // const resp = yield call(callApi, 'post', `/bots/${botId}/deployments`, {
    //   botId,
    //   name: data.name,
    //   botVersion: data.botVersion,
    //   channels: {}
    // });
    const resp = Object.assign({}, data, { id: shortid.generate() });
    yield put(addSubscriptionSuccess(resp));
    yield put(
      addNotification({
        title: 'Success',
        message: 'Subscription created successfully',
        status: 'success',
        dismissible: true,
        dismissAfter: 5000
      })
    );
    yield put(closeModal('SubscriptionCreate')); // 'SubscriptionCreate' is modal id registered when modal opened
  } catch (err) {
    yield put(
      addNotification({
        title: 'Failed',
        message: err.message,
        status: 'error',
        dismissible: true,
        dismissAfter: 5000
      })
    );
    yield put(subscriptionFail('add', err.message));
  }
}

function* updateSubscription({ payload: { id, data } }: ReduxAction) {
  try {
    // const resp = yield call(callApi, 'put', `/bots/${botId}/deployments/${id}`, {
    //   botId,
    //   deploymentId: id,
    //   name: data.name,
    //   botVersion: data.botVersion
    // });
    const resp = Object.assign({}, data, { id });
    yield put(updateSubscriptionSuccess(id, resp));
    yield put(
      addNotification({
        title: 'Success',
        message: 'Subscription updated successfully',
        status: 'success',
        dismissible: true,
        dismissAfter: 5000
      })
    );
    yield put(closeModal('SubscriptionUpdate')); // 'SubscriptionUpdate' is modal id registered when modal opened
  } catch (err) {
    yield put(
      addNotification({
        title: 'Failed',
        message: err.message,
        status: 'error',
        dismissible: true,
        dismissAfter: 5000
      })
    );
    yield put(subscriptionFail('update', err.message));
  }
}

function* removeSubscription({ payload: { id, name } }: ReduxAction) {
  try {
    // yield call(callApi, 'delete', `/bots/${botId}/deployments/${name}`);
    yield put(removeSubscriptionSuccess(id));
    yield put(
      addNotification({
        title: 'Success',
        message: 'Subscription deleted successfully',
        status: 'success',
        dismissible: true,
        dismissAfter: 5000
      })
    );
  } catch (err) {
    yield put(
      addNotification({
        title: 'Failed',
        message: err.message,
        status: 'error',
        dismissible: true,
        dismissAfter: 5000
      })
    );
    yield put(subscriptionFail('remove', err.message));
  }
}

function* watchSubscriptionFetch() {
  yield takeLatest(SUBSCRIPTION.FETCH_REQUEST, fetchSubscription);
}

function* watchSubscriptionAdd() {
  yield takeLatest(SUBSCRIPTION.ADD_REQUEST, addSubscription);
}

function* watchSubscriptionUpdate() {
  yield takeLatest(SUBSCRIPTION.UPDATE_REQUEST, updateSubscription);
}

function* watchSubscriptionRemove() {
  yield takeLatest(SUBSCRIPTION.REMOVE_REQUEST, removeSubscription);
}

function* subscriptionFlow() {
  yield all([
    fork(watchSubscriptionFetch),
    fork(watchSubscriptionAdd),
    fork(watchSubscriptionUpdate),
    fork(watchSubscriptionRemove)
  ]);
}

export default subscriptionFlow;
