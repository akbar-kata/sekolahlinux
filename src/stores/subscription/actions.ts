import { JsonArray } from 'interfaces';
import * as Subscription from 'interfaces/subscription';
import SUBSCRIPTION from './types';

export function clearSubscriptionError(action: string) {
  return {
    action,
    type: SUBSCRIPTION.ERROR_CLEAR
  };
}

function requestSubscription<T>(action: string, payload?: T) {
  return {
    action,
    payload,
    type: SUBSCRIPTION[`${action.toUpperCase()}_REQUEST`]
  };
}

export function subscriptionFail(action: string, error: string) {
  return {
    action,
    type: SUBSCRIPTION[`${action.toUpperCase()}_FAIL`],
    payload: {
      error
    }
  };
}

export function selectSubscription(toSelect: string) {
  return {
    type: SUBSCRIPTION.SELECT,
    payload: toSelect
  };
}

export function fetchSubscriptionRequest() {
  return requestSubscription('fetch');
}

export function fetchSubscriptionSuccess(raw: JsonArray) {
  const data = {};
  const index = raw.map((item: any) => {
    data[item.id] = item;
    return item.id;
  });
  return {
    type: SUBSCRIPTION.FETCH_SUCCESS,
    action: 'fetch',
    payload: {
      index,
      data
    }
  };
}

export function addSubscriptionRequest(data: Subscription.Data) {
  return requestSubscription<{ data: Subscription.Data }>('add', { data });
}

export function addSubscriptionSuccess(data: Subscription.Data) {
  return {
    type: SUBSCRIPTION.ADD_SUCCESS,
    action: 'add',
    payload: {
      [data.id]: data
    }
  };
}

export function updateSubscriptionRequest(id: string, data: Subscription.Data) {
  return requestSubscription<{
    id: string;
    data: Subscription.Data;
  }>('update', { id, data });
}

export function updateSubscriptionSuccess(id: string, data: Subscription.Data) {
  return {
    type: SUBSCRIPTION.UPDATE_SUCCESS,
    action: 'update',
    payload: {
      [id]: data
    }
  };
}

export function removeSubscriptionRequest(id: string, name: string) {
  return requestSubscription<{ id: string; name: string }>('remove', {
    id,
    name
  });
}

export function removeSubscriptionSuccess(id: string) {
  return {
    type: SUBSCRIPTION.REMOVE_SUCCESS,
    action: 'remove',
    payload: { id }
  };
}
