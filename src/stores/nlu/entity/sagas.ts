import { all, call, fork, put, takeLatest, select } from 'redux-saga/effects';
import get from 'lodash-es/get';
import isArray from 'lodash-es/isArray';

import { callApi, API_URL } from 'stores/services';

import * as NLUEntityActions from './actions';
import { ReduxAction } from 'interfaces';
import RootStore from 'interfaces/rootStore';
import { getProjectSelected } from 'stores/project/selectors';
import { setLoading } from 'stores/app/loadings/action';

import NLU_ENTITY from './types';
import { addNotification } from '../../app/notification/actions';
import { closeDrawer } from '../../app/drawer/actions';
import { closeModal } from '../../app/modal/actions';
import { getEntityIndex } from './selectors';
import NLSTUDIO_NLU from '../nlu/types';

/**
 * MOCKED DATA
 *
 * TODO: must be removed when projects ready
 */
// const mockProjectId = '2dd5aa62-12eb-44fc-88d9-6fadda396872';
// const mockUrl = `https://localhost:8080`;

function parseDictionary(raw: any[]) {
  const parsed = {};
  if (raw && raw.length) {
    raw.forEach(dict => {
      parsed[dict.key] = dict.words;
    });
  }
  return parsed;
}

function prepareDictionary(raw: any) {
  if (raw) {
    return Object.keys(raw).map(key => ({
      key,
      words: raw[key]
    }));
  }
  return [];
}

function* selectNlu({ payload }: ReduxAction) {
  const selectedProject = yield select((state: RootStore) =>
    getProjectSelected(state.project)
  );

  if (selectedProject) {
    yield put(
      NLUEntityActions.fetchEntityRequest(selectedProject, payload.data.id)
    );
  }
}

function* fetchEntity({ payload }: ReduxAction) {
  try {
    yield put(setLoading(NLU_ENTITY.FETCH_REQUEST, true));

    const data = yield call(
      callApi,
      'get',
      `/projects/${payload.projectId}/nlus/${payload.nluId ||
        payload.projectId}/entities`,
      undefined,
      API_URL
    );

    yield put(setLoading(NLU_ENTITY.FETCH_REQUEST, false));
    yield put(NLUEntityActions.fetchEntitySuccess(data));
  } catch (e) {
    const errorMessage = get(e, 'response.data.message', 'Unknown error.');
    yield put(
      addNotification({
        title: 'Failed loading entities',
        message: errorMessage,
        status: 'error',
        dismissible: true,
        dismissAfter: 5000
      })
    );
    yield put(setLoading(NLU_ENTITY.FETCH_REQUEST, false));
    yield put(NLUEntityActions.entityFail('fetch', errorMessage));
  }
}

function* createEntity({ payload: { project, nlu, data } }: ReduxAction) {
  try {
    yield put(setLoading(NLU_ENTITY.CREATE_REQUEST, true));

    const newData: any = {
      name: data.name,
      type: data.type,
      profile: data.profile,
      inherit: data.inherit,
      relProfile: data.relProfile,
      belongsTo: data.belongsTo,
      resolver: data.resolver,
      labels: data.labels,
      root: data.root
    };
    if (data.type === 'dict') {
      newData.dictionary = parseDictionary(data.dictionary);
    }
    yield call(
      callApi,
      'post',
      `/projects/${project}/nlus/${nlu}/entities`,
      newData
    );

    yield put(setLoading(NLU_ENTITY.CREATE_REQUEST, false));
    yield put(NLUEntityActions.createEntitySuccess(data));
    yield put(NLUEntityActions.selectEntity(data.name));
    yield put(
      addNotification({
        title: 'Entity Created',
        message: `Entity <strong>${
          data.name
        }</strong> is successfully created.`,
        status: 'success',
        allowHTML: true,
        dismissible: true,
        dismissAfter: 5000
      })
    );
    yield put(closeDrawer('EntityCreate'));
    yield put(closeModal('EntityCreate')); // 'EntityCreate' is modal id registered when modal opened
  } catch (err) {
    const errorMessage = get(
      err,
      'response.data.message',
      'Failed creating new Entity.'
    );
    yield put(setLoading(NLU_ENTITY.CREATE_REQUEST, false));
    yield put(NLUEntityActions.entityFail('create', errorMessage));
    yield put(
      addNotification({
        title: 'Failed',
        message: errorMessage,
        status: 'error',
        dismissible: true,
        dismissAfter: 5000
      })
    );
  }
}

function* updateEntity({ payload: { project, nlu, name, data } }: ReduxAction) {
  try {
    yield put(setLoading(NLU_ENTITY.UPDATE_REQUEST, true));

    const newData: any = {
      name,
      type: data.type,
      profile: data.profile,
      relProfile: data.relProfile,
      belongsTo: data.belongsTo,
      resolver: data.resolver,
      labels: data.labels,
      root: data.root
    };
    if (data.type === 'dict') {
      newData.dictionary = parseDictionary(data.dictionary);
    }
    yield call(
      callApi,
      'put',
      `/projects/${project}/nlus/${nlu}/entities/${name}`,
      newData
    );

    yield put(setLoading(NLU_ENTITY.UPDATE_REQUEST, false));
    yield put(NLUEntityActions.updateEntitySuccess(name, data));
    yield put(
      addNotification({
        title: 'Success',
        message: `Entity ${name} successfully updated.`,
        status: 'success',
        dismissible: true,
        dismissAfter: 5000
      })
    );
    yield put(closeDrawer('EntityUpdate'));
    yield put(closeModal('EntityUpdate')); // 'EntityUpdate' is modal id registered when modal opened
  } catch (err) {
    const errorMessage = get(
      err,
      'response.data.message',
      'Failed updating entity.'
    );

    yield put(setLoading(NLU_ENTITY.UPDATE_REQUEST, false));
    yield put(NLUEntityActions.entityFail('update', errorMessage));
    yield put(
      addNotification({
        title: 'Failed',
        message: errorMessage,
        status: 'error',
        dismissible: true,
        dismissAfter: 5000
      })
    );
  }
}

function* deleteEntity({ payload: { project, nlu, name } }: ReduxAction) {
  try {
    yield put(setLoading(NLU_ENTITY.DELETE_REQUEST, true));

    yield call(
      callApi,
      'delete',
      `/projects/${project}/nlus/${nlu}/entities/${name}`
    );

    yield put(setLoading(NLU_ENTITY.DELETE_REQUEST, false));
    yield put(NLUEntityActions.removeEntitySuccess(name));
    const entityIds = yield select((state: RootStore) =>
      getEntityIndex(state.nlu.entity)
    );
    if (entityIds.length > 0 && entityIds[0] !== name) {
      yield put(NLUEntityActions.selectEntity(entityIds[0]));
    } else if (entityIds.length > 1) {
      yield put(NLUEntityActions.selectEntity(entityIds[1]));
    }
    yield put(
      addNotification({
        title: 'Success',
        message: `Entity ${name} successfully deleted.`,
        status: 'success',
        dismissible: true,
        dismissAfter: 5000
      })
    );
  } catch (err) {
    const errorMessage = get(
      err,
      'response.data.message',
      'Failed deleting entity'
    );

    yield put(setLoading(NLU_ENTITY.DELETE_REQUEST, false));
    yield put(NLUEntityActions.entityFail('remove', errorMessage));
    yield put(
      addNotification({
        title: 'Failed',
        message: errorMessage,
        status: 'error',
        dismissible: true,
        dismissAfter: 5000
      })
    );
  }
}

function* fetchProfiles({ payload }: ReduxAction) {
  try {
    const result = yield call(
      callApi,
      'get',
      `/nlus/profiles?lang=${payload.lang}`
    );
    const data = result && isArray(result) ? result : [];
    yield put(NLUEntityActions.profilesEntitySuccess(data));
  } catch (err) {
    const errorMessage = get(
      err,
      'response.data.message',
      'Failed fetching NLU Profiles'
    );
    yield put(NLUEntityActions.profilesEntitySuccess([]));
    yield put(NLUEntityActions.entityFail('fetch_profiles', errorMessage));
  }
}

function* fetchRoot({ payload }: ReduxAction) {
  try {
    const result = yield call(callApi, 'get', `/nlus/public`);
    yield put(NLUEntityActions.rootEntitySuccess('fetch_root', result));
    yield put(NLUEntityActions.inheritEntitySuccess(result));
  } catch (err) {
    const errorMessage = get(
      err,
      'response.data.message',
      'Failed fetching root'
    );
    yield put(NLUEntityActions.inheritEntitySuccess([]));
    yield put(NLUEntityActions.entityFail('fetch_root', errorMessage));
  }
}

function* fetchDetail({ payload }: ReduxAction) {
  try {
    let data = yield call(
      callApi,
      'get',
      `/projects/${payload.project}/nlus/${payload.nlu}/entities/${
        payload.name
      }`
    );

    if (data.type === 'dict' && data.dictionary) {
      data = Object.assign({}, data, {
        dictionary: prepareDictionary(data.dictionary)
      });
    }
    yield put(NLUEntityActions.detailEntitySuccess(data));
  } catch (err) {
    const errorMessage = get(err, 'response.data.message', 'Unknown error');
    yield put(
      addNotification({
        title: 'Failed to get NLU detail',
        message: errorMessage,
        status: 'error',
        dismissible: true,
        dismissAfter: 5000
      })
    );
    yield put(NLUEntityActions.entityFail('detail', errorMessage));
  }
}

function* watchNluFetched() {
  yield takeLatest(NLSTUDIO_NLU.FETCH_SUCCESS, selectNlu);
}

function* watchEntityFetch() {
  yield takeLatest(NLU_ENTITY.FETCH_REQUEST, fetchEntity);
}

function* watchEntityCreate() {
  yield takeLatest(NLU_ENTITY.CREATE_REQUEST, createEntity);
}

function* watchEntityUpdate() {
  yield takeLatest(NLU_ENTITY.UPDATE_REQUEST, updateEntity);
}

function* watchEntityDelete() {
  yield takeLatest(NLU_ENTITY.DELETE_REQUEST, deleteEntity);
}

function* watchEntityProfiles() {
  yield takeLatest(NLU_ENTITY.PROFILES_REQUEST, fetchProfiles);
}

function* watchEntityRoot() {
  yield takeLatest(NLU_ENTITY.ROOT_REQUEST, fetchRoot);
}

function* watchEntityInherit() {
  yield takeLatest(NLU_ENTITY.INHERIT_REQUEST, fetchRoot);
}

function* watchEntityDetail() {
  yield takeLatest(NLU_ENTITY.DETAIL_REQUEST, fetchDetail);
}

function* entityFlow() {
  yield all([
    fork(watchNluFetched),
    fork(watchEntityFetch),
    fork(watchEntityCreate),
    fork(watchEntityUpdate),
    fork(watchEntityDelete),
    fork(watchEntityProfiles),
    fork(watchEntityRoot),
    fork(watchEntityInherit),
    fork(watchEntityDetail)
  ]);
}

export default entityFlow;
