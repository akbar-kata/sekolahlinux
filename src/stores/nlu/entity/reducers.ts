import { combineReducers } from 'redux';

import {
  EntityStore,
  EntityLoadingState,
  EntityDataMap,
  EntityErrorState
} from 'interfaces/nlu/entity';
import { ReduxAction } from 'interfaces';

import TYPES from './types';

const initialState: EntityStore = {
  loadings: {
    fetch: false,
    add: false,
    update: false,
    remove: false,
    profiles: false,
    root: false,
    inherit: false,
    detail: false
  },
  errors: {
    fetch: null,
    add: null,
    update: null,
    remove: null,
    profile: null,
    root: null,
    inherit: null,
    detail: null
  },
  selected: null,
  data: {},
  index: [],
  profiles: [],
  root: [],
  inherit: [],
  publicEntities: {},
  lastUpdate: Date.now(),
  detail: null
};

const selected = (state: string | null = null, action: ReduxAction) => {
  const { type, payload } = action;
  switch (type) {
    case TYPES.SELECT:
      return payload || null;
    default:
      return state;
  }
};

const loadings = (
  state: EntityLoadingState = initialState.loadings,
  { type, action, payload }: ReduxAction
) => {
  const actionCaps = action ? action.toUpperCase() : 'FETCH';
  switch (type) {
    case TYPES[`${actionCaps}_REQUEST`]:
      return Object.assign({}, state, {
        [action as string]: true
      });
    case TYPES[`${actionCaps}_SUCCESS`]:
    case TYPES[`${actionCaps}_FAIL`]:
      return Object.assign({}, state, {
        [action as string]: false
      });
    default:
      return state;
  }
};

const errors = (
  state: EntityErrorState = initialState.errors,
  { type, action, payload }: ReduxAction
) => {
  const actionCaps = action ? action.toUpperCase() : 'FETCH';
  switch (type) {
    case TYPES.ERROR_CLEAR:
    case TYPES[`${actionCaps}_REQUEST`]:
      return Object.assign({}, state, {
        [action as string]: null
      });
    case TYPES[`${actionCaps}_FAIL`]:
      return Object.assign({}, state, {
        [action as string]: payload.error
      });
    default:
      return state;
  }
};

function data(state: EntityDataMap = {}, { type, payload }: ReduxAction) {
  switch (type) {
    case TYPES.FETCH_SUCCESS:
      return Object.assign({}, payload.data);
    case TYPES.CREATE_SUCCESS:
    case TYPES.UPDATE_SUCCESS:
      return Object.assign({}, state, payload);
    case TYPES.DELETE_SUCCESS:
      const { [payload.name]: deletedItem, ...rest } = state;
      return rest;
    default:
      return state;
  }
}

function index(state: string[] = [], { type, payload }: ReduxAction) {
  switch (type) {
    case TYPES.FETCH_SUCCESS:
      return [...payload.index];
    case TYPES.CREATE_SUCCESS:
      return [...state, ...Object.keys(payload)];
    case TYPES.DELETE_SUCCESS:
      return state.filter(name => name !== payload.name);
    default:
      return state;
  }
}

const profiles = (state: any[] = [], { type, payload }: ReduxAction) => {
  switch (type) {
    case TYPES.PROFILES_SUCCESS:
      return [...payload];
    default:
      return state;
  }
};

const root = (state: any[] = [], { type, payload }: ReduxAction) => {
  switch (type) {
    case TYPES.ROOT_SUCCESS:
      return [...payload];
    default:
      return state;
  }
};

const inherit = (state: any[] = [], { type, payload }: ReduxAction) => {
  switch (type) {
    case TYPES.INHERIT_SUCCESS:
      return [...payload];
    default:
      return state;
  }
};

const publicEntities = (
  state: {} = initialState.publicEntities,
  { type, payload }: ReduxAction
) => {
  switch (type) {
    case TYPES.PUBLIC_ENTITIES_SUCCESS:
      return payload;
    default:
      return state;
  }
};

const lastUpdate = (state: number = Date.now(), { type }: ReduxAction) => {
  switch (type) {
    default:
      return state;
  }
};

const detail = (
  state: any = initialState.detail,
  { type, payload }: ReduxAction
) => {
  switch (type) {
    case TYPES.DETAIL_CLEAR:
      return null;
    case TYPES.DETAIL_SUCCESS:
      return payload;
    default:
      return state;
  }
};

export default combineReducers<EntityStore>({
  selected,
  loadings,
  errors,
  index,
  data,
  profiles,
  root,
  inherit,
  publicEntities,
  lastUpdate,
  detail
});
