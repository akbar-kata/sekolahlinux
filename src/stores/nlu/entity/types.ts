enum NLU_ENTITY {
  SELECT = '@@NLU/ENTITY/SELECT',
  ERROR_CLEAR = '@@NLU/ENTITY/ERROR_CLEAR',

  FETCH_REQUEST = '@@NLU/ENTITY/FETCH_REQUEST',
  FETCH_SUCCESS = '@@NLU/ENTITY/FETCH_SUCCESS',
  FETCH_FAIL = '@@NLU/ENTITY/FETCH_FAIL',

  CREATE_REQUEST = '@@NLU/ENTITY/CREATE_REQUEST',
  CREATE_SUCCESS = '@@NLU/ENTITY/CREATE_SUCCESS',
  CREATE_FAIL = '@@NLU/ENTITY/CREATE_FAIL',

  UPDATE_REQUEST = '@@NLU/ENTITY/UPDATE_REQUEST',
  UPDATE_SUCCESS = '@@NLU/ENTITY/UPDATE_SUCCESS',
  UPDATE_FAIL = '@@NLU/ENTITY/UPDATE_FAIL',

  DELETE_REQUEST = '@@NLU/ENTITY/DELETE_REQUEST',
  DELETE_SUCCESS = '@@NLU/ENTITY/DELETE_SUCCESS',
  DELETE_FAIL = '@@NLU/ENTITY/DELETE_FAIL',

  PROFILES_REQUEST = '@@NLU/ENTITY/PROFILES_REQUEST',
  PROFILES_SUCCESS = '@@NLU/ENTITY/PROFILES_SUCCESS',
  PROFILES_FAIL = '@@NLU/ENTITY/PROFILES_FAIL',

  ROOT_REQUEST = '@@NLU/ENTITY/ROOT_REQUEST',
  ROOT_SUCCESS = '@@NLU/ENTITY/ROOT_SUCCESS',
  ROOT_FAIL = '@@NLU/ENTITY/ROOT_FAIL',

  INHERIT_REQUEST = '@@NLU/ENTITY/INHERIT_REQUEST',
  INHERIT_SUCCESS = '@@NLU/ENTITY/INHERIT_SUCCESS',
  INHERIT_FAIL = '@@NLU/ENTITY/INHERIT_FAIL',

  DETAIL_REQUEST = '@@NLU/ENTITY/DETAIL_REQUEST',
  DETAIL_FAIL = '@@NLU/ENTITY/DETAIL_FAIL',
  DETAIL_SUCCESS = '@@NLU/ENTITY/DETAIL_SUCCESS',
  DETAIL_CLEAR = '@@NLU/ENTITY/DETAIL_CLEAR',

  PUBLIC_ENTITIES_REQUEST = '@@NLU/ENTITY/PUBLIC_ENTITIES_REQUEST',
  PUBLIC_ENTITIES_FAIL = '@@NLU/ENTITY/PUBLIC_ENTITIES_FAIL',
  PUBLIC_ENTITIES_SUCCESS = '@@NLU/ENTITY/PUBLIC_ENTITIES_SUCCESS'
}

export default NLU_ENTITY;
