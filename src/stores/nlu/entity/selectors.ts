import { EntityStore, Entity, EntityDataMap } from 'interfaces/nlu/entity';

export function getEntitySelected(state: EntityStore): string | null {
  return state.selected;
}

export function getEntityLoading(state: EntityStore, type: string): boolean {
  return state.loadings[type];
}

export function getEntityError(
  state: EntityStore,
  type: string
): string | null {
  return state.errors[type];
}

export function getEntityLastUpdated(state: EntityStore): number {
  return state.lastUpdate;
}

export function getEntityIndex(state: EntityStore): string[] {
  return state.index.sort((a, b) => {
    const nameA = (state.data[a].name || '').toLowerCase();
    const nameB = (state.data[b].name || '').toLowerCase();
    if (nameA < nameB) {
      return -1;
    }
    if (nameA > nameB) {
      return 1;
    }

    // names must be equal
    return 0;
  });
}

export function getEntityIndexCustomType(
  state: EntityStore,
  types: string[]
): string[] {
  return getEntityIndex(state).filter(
    id => types.indexOf(state.data[id].type) !== -1
  );
}

export function getEntityData(state: EntityStore): EntityDataMap {
  return state.data;
}

export function getEntityDetailUpdate(state: EntityStore) {
  return state.detail;
}

export function getEntityProfiles(state: EntityStore) {
  return state.profiles;
}

export function getEntityRoot(state: EntityStore) {
  return state.publicEntities;
}

export function getInheritRoot(state: EntityStore) {
  return state.inherit;
}

export function getDetailEntityInherit(
  state: EntityStore,
  name: string
): Entity {
  return state.publicEntities[name];
}

export function getEntityDetail(state: EntityStore, name: string): Entity {
  return state.data[name];
}

export function isExist(state: EntityStore, name: string): boolean {
  return state.index.indexOf(name) !== -1;
}
