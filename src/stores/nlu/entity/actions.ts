import { JsonArray } from 'interfaces';
import {
  Entity,
  EntityDataMap,
  EditEntityRequestData
} from 'interfaces/nlu/entity';

import NLU_ENTITY from './types';
import { callApiThunk } from 'stores/services';

function requestEntity<T>(action: string, payload?: T) {
  return {
    action,
    payload,
    type: NLU_ENTITY[`${action.toUpperCase()}_REQUEST`]
  };
}

export function entityFail(action: string, error: string) {
  return {
    action,
    type: NLU_ENTITY[`${action.toUpperCase()}_FAIL`],
    payload: {
      error
    }
  };
}

export function fetchEntityRequest(projectId: string, nluId: string | null) {
  return requestEntity('fetch', { projectId, nluId });
}

export function searchEntitiesRequest(keyword: string) {
  return function(dispatch: Function) {
    return dispatch(
      callApiThunk(
        'get',
        `/nlus/public?q=${keyword === '' ? '%20%20&limit=0' : keyword}`
      )
    ).then(response => {
      dispatch(inheritEntitySuccess(response));
      dispatch(publicEntitySuccess(response));
      const result = Object.keys(response)
        .map(key => {
          const arr = key.split('/');
          if (arr[1]) {
            const arr1 = arr[1].split(':');
            return `${arr[0]}/${arr1[0]}`;
          }
          return key;
        })
        .reduce((arr: string[], curr) => {
          if (arr.indexOf(curr) === -1) {
            return [...arr, curr];
          }
          return [...arr];
        }, []);
      return {
        options: result.map(item => ({
          label: item,
          value: item
        }))
      };
    });
  };
}

export function searchRootRequest(keyword: string) {
  return function(dispatch: Function) {
    return dispatch(
      callApiThunk(
        'get',
        `/nlus/public?q=${keyword === '' ? '%20%20&limit=0' : keyword}`
      )
    ).then(response => {
      dispatch(publicEntitySuccess(response));
      return {
        options: Object.keys(response).map(item => ({
          label: item,
          value: item
        }))
      };
    });
  };
}

export function fetchEntitySuccess(raw: any) {
  const data = {};
  const index = Object.keys(raw).map((item: any) => {
    data[item] = {
      name: item,
      ...raw[item]
    };
    return item;
  });
  return {
    type: NLU_ENTITY.FETCH_SUCCESS,
    action: 'fetch',
    payload: {
      index,
      data
    }
  };
}

export function selectEntity(toSelect: string | null) {
  return {
    type: NLU_ENTITY.SELECT,
    payload: toSelect
  };
}

function assignData(data: any): Entity {
  return Object.assign(
    {},
    {
      id: data.id,
      name: data.name,
      type: data.type,
      profile: data.profile,
      labels: data.labels,
      relProfile: data.relProfile,
      belongsTo: data.belongsTo,
      model: data.model,
      relModel: data.relModel,
      resolver: data.resolves,
      root: data.root,
      inherit: data.inherit,
      dictionary: data.dictionary
    }
  );
}

export function createEntityRequest(
  project: string,
  nlu: string,
  data: Entity
) {
  return requestEntity('create', {
    project,
    nlu,
    data
  });
}

export function createEntitySuccess(data: any) {
  return {
    type: NLU_ENTITY.CREATE_SUCCESS,
    action: 'create',
    payload: {
      [data.name]: assignData(data)
    } as EntityDataMap
  };
}

export function updateEntityRequest(
  project: string,
  nlu: string,
  name: string,
  data: Entity
) {
  return requestEntity<EditEntityRequestData>('update', {
    project,
    nlu,
    name,
    data
  });
}

export function updateEntitySuccess(name: string, data: Entity) {
  return {
    type: NLU_ENTITY.UPDATE_SUCCESS,
    action: 'update',
    payload: {
      [name]: assignData(data)
    } as EntityDataMap
  };
}

export function removeEntityRequest(
  project: string,
  nlu: string,
  name: string
) {
  return requestEntity('delete', {
    project,
    nlu,
    name
  });
}

export function removeEntitySuccess(name: string) {
  return {
    type: NLU_ENTITY.DELETE_SUCCESS,
    action: 'delete',
    payload: { name }
  };
}

export function profilesEntityRequest(lang: string) {
  return requestEntity('profiles', { lang });
}

export function profilesEntitySuccess(data: JsonArray) {
  return {
    type: NLU_ENTITY.PROFILES_SUCCESS,
    action: 'profiles',
    payload: data
  };
}

export function rootEntityRequest(type: string) {
  return requestEntity('root', {
    type
  });
}

export function rootEntitySuccess(action: string, data: JsonArray) {
  return {
    action,
    type: NLU_ENTITY.ROOT_SUCCESS,
    payload: data
  };
}

export function publicEntitySuccess(data: JsonArray) {
  return {
    type: NLU_ENTITY.PUBLIC_ENTITIES_SUCCESS,
    action: 'public',
    payload: data
  };
}

export function inheritEntityRequest() {
  return requestEntity('inherit');
}

export function inheritEntitySuccess(data: JsonArray) {
  const result = Object.keys(data)
    .map(key => {
      const arr = key.split('/');
      if (arr[1]) {
        const arr1 = arr[1].split(':');
        return `${arr[0]}/${arr1[0]}`;
      }
      return key;
    })
    .reduce((arr: string[], curr) => {
      if (arr.indexOf(curr) === -1) {
        return [...arr, curr];
      }
      return [...arr];
    }, []);
  return {
    type: NLU_ENTITY.INHERIT_SUCCESS,
    action: 'inherit',
    payload: result
  };
}

export function detailEntityClear() {
  return {
    type: NLU_ENTITY.DETAIL_CLEAR
  };
}

export function detailEntityRequest(
  project: string,
  nlu: string,
  name: string
) {
  return requestEntity('detail', { project, nlu, name });
}

export function detailEntitySuccess(data: JsonArray | null) {
  return {
    type: NLU_ENTITY.DETAIL_SUCCESS,
    action: 'detail',
    payload: data
  };
}
