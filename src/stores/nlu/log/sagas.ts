import { all, call, fork, put, select, takeLatest } from 'redux-saga/effects';
import get from 'lodash-es/get';
import isArray from 'lodash-es/isArray';

import { ReduxAction } from 'interfaces';
import RootStore from 'interfaces/rootStore';

import { callApi } from 'stores/services';
import { addNotification } from 'stores/app/notification/actions';
import { getProjectSelected } from 'stores/project/selectors';

import { prepareTrain } from 'utils/trainFormatter';

import {
  logFail,
  fetchLogSuccess,
  correctLogSuccess,
  fetchLogRequest
} from './actions';
import NLU_LOG from './types';
import NLSTUDIO_NLU from '../nlu/types';

function* selectNlu({ payload }: ReduxAction) {
  const selectedProject = yield select((state: RootStore) =>
    getProjectSelected(state.project)
  );

  if (selectedProject) {
    yield put(fetchLogRequest(selectedProject, payload.data.id));
  }
}

function* fetchLog({ payload }: ReduxAction) {
  try {
    const { result, ...pagination } = yield call(
      callApi,
      'get',
      `/projects/${payload.project}/nlus/${
        payload.id
      }/log?page=${payload.page || 1}&limit=${payload.limit || 10}`,
      undefined
    );

    yield put(
      fetchLogSuccess({
        data: isArray(result) ? result : [],
        pagination: {
          limit: pagination.limit,
          page: pagination.page,
          // this endpoint returns `total_pages` instead of the total items that exists
          total: pagination.total_pages
        }
      })
    );
  } catch (err) {
    const errorMessage = get(
      err,
      'response.data.message',
      'Oops... something went wrong'
    );
    yield put(
      addNotification({
        title: 'Failed to Load Training Log',
        message: errorMessage,
        status: 'error',
        dismissible: true,
        dismissAfter: 5000
      })
    );
    yield put(logFail('fetch', errorMessage));
  }
}

function* correctLog({
  payload: {
    project,
    nlu,
    data: { id, sentence, entities, traits }
  }
}: ReduxAction) {
  try {
    const newData = {
      logId: id,
      sentence: prepareTrain(sentence, entities, traits)
    };
    yield call(
      callApi,
      'post',
      `/projects/${project}/nlus/${nlu}/correct`,
      newData
    );
    yield put(correctLogSuccess(id, [...entities, ...traits]));
    yield put(
      addNotification({
        title: 'Success',
        message: `NLU successfully trained.`,
        status: 'success',
        dismissible: true,
        dismissAfter: 5000
      })
    );
  } catch (err) {
    const errorMessage = get(
      err,
      'response.data.message',
      'Failed to train NLU.'
    );
    yield put(
      addNotification({
        title: 'Failed',
        message: errorMessage,
        status: 'error',
        dismissible: true,
        dismissAfter: 5000
      })
    );
    yield put(logFail('add', errorMessage));
  }
}

function* watchNluFetched() {
  yield takeLatest(NLSTUDIO_NLU.FETCH_SUCCESS, selectNlu);
}

function* watchLogFetch() {
  yield takeLatest(NLU_LOG.FETCH_REQUEST, fetchLog);
}

function* watchLogCorrect() {
  yield takeLatest(NLU_LOG.CORRECT_REQUEST, correctLog);
}

function* logFlow() {
  yield all([
    fork(watchNluFetched),
    fork(watchLogFetch),
    fork(watchLogCorrect)
  ]);
}

export default logFlow;
