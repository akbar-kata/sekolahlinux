// import isArray from 'lodash-es/isArray';

import NLU_LOG from './types';

// import { JsonArray } from 'interfaces';
import { Pagination } from 'interfaces/common';
import { extractPrediction } from 'utils/trainFormatter';

export function clearLogError(action: string) {
  return {
    action,
    type: NLU_LOG.ERROR_CLEAR
  };
}

function requestLog<T>(action: string, payload?: T) {
  return {
    action,
    payload,
    type: NLU_LOG[`${action.toUpperCase()}_REQUEST`]
  };
}

export function logFail(action: string, error: string) {
  return {
    action,
    type: NLU_LOG[`${action.toUpperCase()}_FAIL`],
    payload: {
      error
    }
  };
}

/**
 * parse prediction from server to display
 * @param raw
 */
function parsePredicted(raw: { [key: string]: any[] }) {
  const newPredicted: any[] = [];
  Object.keys(raw).forEach(entity =>
    raw[entity].forEach(tag => {
      newPredicted.push({ ...tag, entity });
    })
  );
  return newPredicted;
}

// /**
//  * prepare predicted from display to save
//  * @param raw
//  */
// function preparePredicted(raw: any[]) {
//   let newPredicted: any = {};
//   if (raw && raw.length) {
//     raw.forEach(tag => {
//       const { entity, ...rest } = tag;
//       if (isArray(newPredicted[entity])) {
//         newPredicted[entity].push(rest);
//       } else {
//         newPredicted[entity] = [rest];
//       }
//     });
//   }
//   return newPredicted;
// }

export function fetchLogRequest(
  project: string,
  id: string | null,
  page: number = 1,
  limit: number = 10
) {
  return requestLog('fetch', { project, id, page, limit });
}

export function fetchLogSuccess({
  data,
  pagination
}: {
  data: any[];
  pagination: Pagination;
}) {
  const newData = {};
  const index = data.map((item: any) => {
    const { _id, predicted, corrected, ...rest } = item;
    newData[_id] = {
      id: _id,
      entities: corrected
        ? extractPrediction(corrected)
        : parsePredicted(predicted),
      ...rest
    };
    return _id;
  });
  return {
    type: NLU_LOG.FETCH_SUCCESS,
    action: 'fetch',
    payload: {
      index,
      pagination,
      data: newData
    }
  };
}

export function correctLogRequest(
  project: string,
  nlu: string,
  id: string,
  data: { [key: string]: any }
) {
  const { sentence, entities, traits } = data;
  return requestLog('correct', {
    project,
    nlu,
    data: {
      id,
      sentence,
      entities,
      traits
    }
  });
}

export function correctLogSuccess(id: string, data: any[]) {
  return {
    type: NLU_LOG.CORRECT_SUCCESS,
    action: 'correct',
    payload: {
      id,
      entities: data
    }
  };
}
