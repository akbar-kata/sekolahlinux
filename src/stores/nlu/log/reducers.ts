import { combineReducers } from 'redux';

import { ReduxAction } from 'interfaces';
import NLU_LOG from './types';
import {
  NluLogStore,
  NluLogLoadingState,
  NluLogErrors,
  NluLogDataMap
} from 'interfaces/nlu/log';
import { Pagination } from 'interfaces/common';

const initialState: NluLogStore = {
  loadings: {
    fetch: false,
    correct: false
  },
  errors: {
    fetch: null,
    correct: null
  },
  data: {},
  index: [],
  lastUpdate: Date.now(),
  pagination: {
    limit: 10,
    page: 1,
    total: 0
  }
};

const loadings = (
  state: NluLogLoadingState = initialState.loadings,
  { type, action, payload }: ReduxAction
) => {
  const actionCaps = action ? action.toUpperCase() : 'FETCH';
  switch (type) {
    case NLU_LOG[`${actionCaps}_REQUEST`]:
      return Object.assign({}, state, {
        [action as string]: true
      });
    case NLU_LOG[`${actionCaps}_SUCCESS`]:
    case NLU_LOG[`${actionCaps}_FAIL`]:
      return Object.assign({}, state, {
        [action as string]: false
      });
    default:
      return state;
  }
};

const errors = (
  state: NluLogErrors = initialState.errors,
  { type, action, payload }: ReduxAction
) => {
  const actionCaps = action ? action.toUpperCase() : 'FETCH';
  switch (type) {
    case NLU_LOG.ERROR_CLEAR:
    case NLU_LOG[`${actionCaps}_REQUEST`]:
      return Object.assign({}, state, {
        [action as string]: null
      });
    case NLU_LOG[`${actionCaps}_FAIL`]:
      return Object.assign({}, state, {
        [action as string]: payload.error
      });
    default:
      return state;
  }
};

const pagination = (
  state: Pagination = initialState.pagination,
  action: ReduxAction
) => {
  const { type, payload } = action;
  switch (type) {
    case NLU_LOG.FETCH_SUCCESS:
      return payload.pagination;
    default:
      return state;
  }
};

const data = (
  state: NluLogDataMap = initialState.data,
  { type, payload }: ReduxAction
) => {
  switch (type) {
    case NLU_LOG.FETCH_SUCCESS:
      return Object.assign({}, payload.data);
    case NLU_LOG.CORRECT_SUCCESS:
      return Object.assign({}, state, {
        [payload.id]: Object.assign({}, state[payload.id], {
          entities: payload.entities,
          trained: true
        })
      });
    default:
      return state;
  }
};

const index = (
  state: string[] = initialState.index,
  { type, payload }: ReduxAction
) => {
  switch (type) {
    case NLU_LOG.FETCH_SUCCESS:
      return [...payload.index];
    default:
      return state;
  }
};

const lastUpdate = (state: number = Date.now(), { type }: ReduxAction) => {
  switch (type) {
    default:
      return state;
  }
};

export default combineReducers<NluLogStore>({
  index,
  data,
  loadings,
  errors,
  pagination,
  lastUpdate
});
