import * as Log from 'interfaces/nlu/log';

export function getLogLoading(state: Log.NluLogStore, type: string): boolean {
  return state.loadings[type];
}

export function getLogError(
  state: Log.NluLogStore,
  type: string
): string | null {
  return state.errors[type];
}

export function getLogLastUpdated(state: Log.NluLogStore): number {
  return state.lastUpdate;
}

export function getLogIndex(state: Log.NluLogStore): string[] {
  return state.index;
}

export function getLogData(state: Log.NluLogStore): Log.NluLogDataMap {
  return state.data;
}

export function getLogDetail(
  state: Log.NluLogStore,
  id: string
): Log.NluLogData {
  return state.data[id];
}

export function getLogPage(state: Log.NluLogStore): Log.NluLogPagination {
  return state.pagination;
}
