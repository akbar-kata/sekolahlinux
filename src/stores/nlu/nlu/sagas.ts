import { all, call, takeLatest, fork, put } from 'redux-saga/effects';
import get from 'lodash-es/get';

import { ReduxAction } from 'interfaces';
import { callApi } from 'stores/services';
import { addNotification } from 'stores/app/notification/actions';

import * as NluActions from './actions';
import NLSTUDIO_NLU from './types';

function* fetchNlu({ payload }: ReduxAction) {
  try {
    yield put(NluActions.clearData());
    const data = yield call(
      callApi,
      'get',
      `/projects/${payload.projectId}/nlu`
    );
    yield put(NluActions.fetchNluSuccess(data));
  } catch (e) {
    const errorMessage = get(e, 'response.data.message', 'Unknown error.');
    yield put(NluActions.nluError('fetch', errorMessage));
  }
}

function* getReIssueToken({
  payload: { projectId, nluId, data }
}: ReduxAction) {
  try {
    const result = yield call(
      callApi,
      'get',
      `/projects/${projectId}/nlus/${nluId}/issue_token`
    );
    yield put(NluActions.reIssueTokenSuccess(result, nluId));
    yield put(
      addNotification({
        title: 'Success',
        message: 'Token successfully reissued.',
        status: 'success',
        dismissible: true,
        dismissAfter: 5000
      })
    );
  } catch (err) {
    const errorMessage = get(
      err,
      'response.data.message',
      'Failed to re-issue token.'
    );
    yield put(NluActions.nluError('reissue', errorMessage));
    yield put(
      addNotification({
        title: 'Failed',
        message: errorMessage,
        status: 'error',
        dismissible: true,
        dismissAfter: 5000
      })
    );
  }
}

function* snapshotNlu({ payload: { project, id } }: ReduxAction) {
  try {
    yield call(callApi, 'get', `/projects/${project}/nlus/${id}/snapshot`);
    yield put(NluActions.snapshotNluSuccess(project));
    yield put(
      addNotification({
        title: 'Success',
        message: `NLU successfully published.`,
        status: 'success',
        dismissible: true,
        dismissAfter: 5000
      })
    );
  } catch (err) {
    const errorMessage = get(err, 'response.data.message', 'Unknown error.');
    yield put(NluActions.nluError('snapshot', errorMessage));
    yield put(
      addNotification({
        title: 'Failed',
        message: errorMessage,
        status: 'error',
        dismissible: true,
        dismissAfter: 5000
      })
    );
  }
}

function* watchNluFetch() {
  yield takeLatest(NLSTUDIO_NLU.FETCH_REQUEST, fetchNlu);
}

function* watchReIssueToken() {
  yield takeLatest(NLSTUDIO_NLU.REISSUE_REQUEST, getReIssueToken);
}

function* watchNluSnapshot() {
  yield takeLatest(NLSTUDIO_NLU.SNAPSHOT_REQUEST, snapshotNlu);
}

function* nluFlow() {
  yield all([
    fork(watchNluFetch),
    fork(watchReIssueToken),
    fork(watchNluSnapshot)
  ]);
}

export default nluFlow;
