import { NluStore, Nlu } from 'interfaces/nlu';

export function getCurrentNlu(state: NluStore): Nlu | null {
  return state.data;
}

export function getNluLoadings(state: NluStore, type: string): boolean {
  return state.loadings[type];
}

export function getNluErrors(state: NluStore, type: string): string | null {
  return state.errors[type];
}
