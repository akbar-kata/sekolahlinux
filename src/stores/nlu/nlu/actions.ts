import NLSTUDIO_NLU from './types';
import { Nlu } from 'interfaces/nlu';

function requestNlu<T>(action: string, payload?: T) {
  return {
    action,
    payload,
    type: NLSTUDIO_NLU[`${action.toUpperCase()}_REQUEST`]
  };
}

function requestReIssueToken<T>(action: string, payload?: T) {
  return {
    action,
    payload,
    type: NLSTUDIO_NLU[`${action.toUpperCase()}_REQUEST`]
  };
}

export function fetchNluRequest(projectId: string) {
  return requestNlu('fetch', { projectId });
}

export function fetchNluSuccess(data: Nlu) {
  return {
    type: NLSTUDIO_NLU.FETCH_SUCCESS,
    action: 'fetch',
    payload: {
      data
    }
  };
}

export function nluError(action: string, error: string) {
  return {
    action,
    type: NLSTUDIO_NLU[`${action.toUpperCase()}_ERROR`],
    payload: {
      error
    }
  };
}

export function clearData() {
  return { type: NLSTUDIO_NLU.CLEAR_DATA };
}

export function clearErrors(action?: string) {
  return {
    action,
    type: NLSTUDIO_NLU.CLEAR_ERRORS,
    payload: {
      action
    }
  };
}

export function reIssueToken(projectId: string, nluId: string) {
  return requestReIssueToken('reissue', {
    projectId,
    nluId
  });
}

export function reIssueTokenSuccess(token: string, nluId: string) {
  return {
    type: NLSTUDIO_NLU.REISSUE_SUCCESS,
    action: 'reissue',
    payload: { token, nluId }
  };
}

export function snapshotNluRequest(project: string, id: string) {
  return requestNlu('snapshot', { project, id });
}

export function snapshotNluSuccess(name: string) {
  return {
    type: NLSTUDIO_NLU.SNAPSHOT_SUCCESS,
    action: 'snapshot',
    payload: name
  };
}
