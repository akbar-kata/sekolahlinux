import { combineReducers } from 'redux';

import { ReduxAction } from 'interfaces';
import { NluStore } from 'interfaces/nlu';
import NLSTUDIO_NLU from './types';

const initialState: NluStore = {
  loadings: {
    fetch: false,
    reissue: false,
    snapshot: false
  },
  errors: {
    fetch: null,
    reissue: null,
    snapshot: null
  },
  data: null
};

const loadings = (
  state: NluStore['loadings'] = initialState.loadings,
  { type, action }: ReduxAction
) => {
  const actionCaps = action ? action.toUpperCase() : 'FETCH';
  switch (type) {
    case NLSTUDIO_NLU[`${actionCaps}_REQUEST`]:
      return Object.assign({}, state, {
        [action as string]: true
      });
    case NLSTUDIO_NLU[`${actionCaps}_SUCCESS`]:
    case NLSTUDIO_NLU[`${actionCaps}_ERROR`]:
      return Object.assign({}, state, {
        [action as string]: false
      });
    default: {
      return state;
    }
  }
};

const data = (
  state: NluStore['data'] = initialState.data,
  action: ReduxAction
) => {
  const { type, payload } = action;

  switch (type) {
    case NLSTUDIO_NLU.FETCH_SUCCESS: {
      return Object.assign({}, payload.data);
    }
    case NLSTUDIO_NLU.CLEAR_DATA: {
      return initialState.data;
    }
    case NLSTUDIO_NLU.REISSUE_SUCCESS: {
      const raw = state;

      if (raw) {
        raw.token = payload.token;

        return {
          ...state,
          [raw.id]: raw
        };
      }
      return state;
    }
    default: {
      return state;
    }
  }
};

const errors = (
  state: NluStore['errors'] = initialState.errors,
  { type, action, payload }: ReduxAction
) => {
  const actionCaps = action ? action.toUpperCase() : 'FETCH';
  switch (type) {
    case NLSTUDIO_NLU.CLEAR_ERRORS:
    case NLSTUDIO_NLU[`${actionCaps}_REQUEST`]: {
      if (action) {
        return Object.assign({}, state, {
          [action]: initialState.errors[action]
        });
      }
      return initialState.errors;
    }
    case NLSTUDIO_NLU[`${actionCaps}_ERROR`]: {
      if (action) {
        return Object.assign({}, state, {
          [action]: payload.error
        });
      }
      return state;
    }
    default: {
      return state;
    }
  }
};

export default combineReducers<NluStore>({
  loadings,
  data,
  errors
});
