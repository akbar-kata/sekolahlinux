import NLU_TESTING from './types';

export function clearTesting(action: string) {
  return {
    action,
    type: NLU_TESTING.CLEAR,
    payload: { data: null }
  };
}

function requestTesting<T>(action: string, payload?: T) {
  return {
    action,
    payload,
    type: NLU_TESTING[`${action.toUpperCase()}_REQUEST`]
  };
}

export function testingFail(action: string, error: string) {
  return {
    action,
    type: NLU_TESTING[`${action.toUpperCase()}_FAIL`],
    payload: {
      error
    }
  };
}

export function fetchTestingRequest(
  projectId: string,
  nluName: string,
  sentence: string
) {
  return requestTesting('fetch', {
    projectId,
    nluName,
    sentence
  });
}

export function fetchTestingSuccess(raw: any) {
  let data = {};
  const index = raw.result.map((item: any) => {
    data = item;
    return item.id;
  });
  return {
    type: NLU_TESTING.FETCH_SUCCESS,
    action: 'fetch',
    payload: { index, data }
  };
}
