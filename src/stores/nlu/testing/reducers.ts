import { combineReducers } from 'redux';

import { ReduxAction } from 'interfaces';
import * as Testing from 'interfaces/nlu/testing';
import NLU_TESTING from './types';

const loadingInit = {
  fetch: false,
  add: false,
  update: false,
  remove: false
};

const loadings = (
  state: Testing.NluTestingLoadingState = loadingInit,
  { type, action, payload }: ReduxAction
) => {
  const actionCaps = action ? action.toUpperCase() : 'FETCH';
  switch (type) {
    case NLU_TESTING[`${actionCaps}_REQUEST`]:
      return Object.assign({}, state, {
        [action as string]: true
      });
    case NLU_TESTING[`${actionCaps}_SUCCESS`]:
    case NLU_TESTING[`${actionCaps}_FAIL`]:
      return Object.assign({}, state, {
        [action as string]: false
      });
    default:
      return state;
  }
};

const errorsInit = {
  fetch: null,
  add: null,
  update: null,
  remove: null
};

const errors = (
  state: Testing.NluTestingErrors = errorsInit,
  { type, action, payload }: ReduxAction
) => {
  const actionCaps = action ? action.toUpperCase() : 'FETCH';
  switch (type) {
    case NLU_TESTING.CLEAR:
    case NLU_TESTING[`${actionCaps}_REQUEST`]:
      return Object.assign({}, state, {
        [action as string]: null
      });
    case NLU_TESTING[`${actionCaps}_FAIL`]:
      return Object.assign({}, state, {
        [action as string]: payload.error
      });
    default:
      return state;
  }
};

const data = (
  state: Testing.NluTestingDataMap = {},
  { type, payload }: ReduxAction
) => {
  switch (type) {
    case NLU_TESTING.FETCH_SUCCESS:
      return Object.assign({}, payload.data);
    case NLU_TESTING.CLEAR:
      return {};
    default:
      return state;
  }
};

const index = (state: string[] = [], { type, payload }: ReduxAction) => {
  switch (type) {
    case NLU_TESTING.FETCH_SUCCESS:
      return [...payload.index];
    default:
      return state;
  }
};

export default combineReducers({
  index,
  data,
  loadings,
  errors
});
