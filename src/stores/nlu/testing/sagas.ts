import { all, call, fork, put, takeLatest } from 'redux-saga/effects';
import get from 'lodash-es/get';

import { testingFail, fetchTestingSuccess } from './actions';
import { callApi } from 'stores/services';
import { ReduxAction } from 'interfaces';
import NLU_TESTING from './types';

function* fetchTesting({
  payload: { projectId, nluName, sentence }
}: ReduxAction) {
  try {
    const predict = yield call(
      callApi,
      'post',
      `/projects/${projectId}/nlus/${nluName}/predict`,
      {
        sentence
      }
    );
    yield put(fetchTestingSuccess(predict));
  } catch (err) {
    const errorMessage = get(
      err,
      'response.data.message',
      'Oops.. something wrong'
    );
    yield put(testingFail('fetch', errorMessage));
  }
}

function* watchTestingFetch() {
  yield takeLatest(NLU_TESTING.FETCH_REQUEST, fetchTesting);
}

function* trainFlow() {
  yield all([fork(watchTestingFetch)]);
}

export default trainFlow;
