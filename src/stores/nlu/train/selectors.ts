import * as Train from 'interfaces/nlu/train';

export function getTrainLoading(
  state: Train.NluTrainStore,
  type: string
): boolean {
  return state.loadings[type];
}

export function getTrainError(
  state: Train.NluTrainStore,
  type: string
): string | null {
  return state.errors[type];
}

export function getTrainLastUpdated(state: Train.NluTrainStore): number {
  return state.lastUpdate;
}

export function getTrainIndex(state: Train.NluTrainStore): string[] {
  return state.index;
}

export function getTrainData(
  state: Train.NluTrainStore
): Train.NluTrainDataMap {
  return state.data;
}

export function getTrainDetail(
  state: Train.NluTrainStore,
  trainId: string
): Train.NluTrainData {
  return state.data[trainId];
}

export function getTrainPage(state: Train.NluTrainStore): Train.NluTrainPage {
  return state.pagination;
}
