import { combineReducers } from 'redux';

import { ReduxAction } from 'interfaces';
import NLU_TRAIN from './types';
import {
  NluTrainStore,
  NluTrainLoadingState,
  NluTrainErrors,
  NluTrainPage,
  NluTrainDataMap
} from 'interfaces/nlu/train';

const initialState: NluTrainStore = {
  loadings: {
    fetch: false
  },
  errors: {
    fetch: null
  },
  data: {},
  index: [],
  lastUpdate: Date.now(),
  pagination: {
    total: 0,
    page: 1,
    limit: 10
  }
};

const loadings = (
  state: NluTrainLoadingState = initialState.loadings,
  { type, action, payload }: ReduxAction
) => {
  const actionCaps = action ? action.toUpperCase() : 'FETCH';
  switch (type) {
    case NLU_TRAIN[`${actionCaps}_REQUEST`]:
      return Object.assign({}, state, {
        [action as string]: true
      });
    case NLU_TRAIN[`${actionCaps}_SUCCESS`]:
    case NLU_TRAIN[`${actionCaps}_FAIL`]:
      return Object.assign({}, state, {
        [action as string]: false
      });
    default:
      return state;
  }
};

const errors = (
  state: NluTrainErrors = initialState.errors,
  { type, action, payload }: ReduxAction
) => {
  const actionCaps = action ? action.toUpperCase() : 'FETCH';
  switch (type) {
    case NLU_TRAIN.ERROR_CLEAR:
    case NLU_TRAIN[`${actionCaps}_REQUEST`]:
      return Object.assign({}, state, {
        [action as string]: null
      });
    case NLU_TRAIN[`${actionCaps}_FAIL`]:
      return Object.assign({}, state, {
        [action as string]: payload.error
      });
    default:
      return state;
  }
};

const pagination = (
  state: NluTrainPage = initialState.pagination,
  { type, payload }: ReduxAction
) => {
  switch (type) {
    case NLU_TRAIN.FETCH_SUCCESS:
      return Object.assign({}, state, payload.page);
    case NLU_TRAIN.ADD_SUCCESS:
      return Object.assign({}, state, {
        total: state.total + 1
      });
    default:
      return state;
  }
};

const data = (
  state: NluTrainDataMap = initialState.data,
  { type, payload }: ReduxAction
) => {
  switch (type) {
    case NLU_TRAIN.FETCH_SUCCESS:
      return Object.assign({}, payload.data);
    case NLU_TRAIN.ADD_SUCCESS:
      return Object.assign({}, state, payload.data);
    default:
      return state;
  }
};

const index = (state: string[] = [], { type, payload }: ReduxAction) => {
  switch (type) {
    case NLU_TRAIN.FETCH_SUCCESS:
      return [...payload.index];
    case NLU_TRAIN.ADD_SUCCESS:
      return [
        ...Object.keys(payload.data),
        ...(state.length >= payload.page.limit ? state.slice(0, -1) : state)
      ];
    default:
      return state;
  }
};

const lastUpdate = (state: number = Date.now(), { type }: ReduxAction) => {
  switch (type) {
    default:
      return state;
  }
};

export default combineReducers<NluTrainStore>({
  index,
  data,
  lastUpdate,
  loadings,
  errors,
  pagination
});
