import { all, call, fork, put, takeLatest, select } from 'redux-saga/effects';
import shortid from 'shortid';
import get from 'lodash-es/get';

import {
  fetchTrainRequest,
  trainFail,
  fetchTrainSuccess,
  addTrainSuccess,
  clearTrain
} from './actions';
import { ReduxAction } from 'interfaces';
import RootStore from 'interfaces/rootStore';
import { callApi } from 'stores/services';
import { addNotification } from 'stores/app/notification/actions';
import { getProjectSelected } from 'stores/project/selectors';

import NLU_TRAIN from './types';
import { getTrainPage } from './selectors';
import { clearPredict } from '../predict/actions';
import NLSTUDIO_NLU from '../nlu/types';

function* selectNlu({ payload }: ReduxAction) {
  const selectedProject = yield select((state: RootStore) =>
    getProjectSelected(state.project)
  );

  if (selectedProject) {
    yield put(fetchTrainRequest(selectedProject, payload.data.id));
  }
}

function* fetchTrain({ payload: { project, id, page } }: ReduxAction) {
  try {
    yield put(clearTrain('fetch'));

    const { data, ...pagination } = yield call(
      callApi,
      'get',
      `/projects/${project}/nlus/${id || project}/training_data?page=${page}`
    );

    const { currentPage, ...rest } = pagination;
    yield put(
      fetchTrainSuccess({
        raw: data,
        page: {
          // `page` sometimes returns a string, so let's force it to be number.
          page: Number(currentPage),
          ...rest
        }
      })
    );
  } catch (err) {
    const errorMessage = get(err, 'response.data.message', 'Unknown error.');
    yield put(
      addNotification({
        title: 'Failed to load training data',
        message: errorMessage,
        status: 'error',
        dismissible: true,
        dismissAfter: 5000
      })
    );
    yield put(fetchTrainSuccess({ raw: [] }));
    yield put(trainFail('fetch', errorMessage));
  }
}

function* addTrain({
  payload: {
    project,
    nlu,
    data: { sentence, entities, traits }
  }
}: ReduxAction) {
  try {
    const page = yield select(({ nlu: { train } }: RootStore) =>
      getTrainPage(train)
    );
    if (page.current === 1) {
      yield put(
        addTrainSuccess(
          shortid.generate(),
          sentence,
          [...entities, ...traits],
          page
        )
      );
    }
    yield call(callApi, 'post', `/projects/${project}/nlus/${nlu}/train`, {
      sentence: [
        {
          input: sentence,
          entities: [...entities, ...traits].map(item => {
            const newEntity: any = {
              id: item.id,
              entity: item.entity,
              label: item.label,
              value: item.value,
              start: item.start,
              end: item.end
            };
            if (item.belongsTo) {
              newEntity.belongsTo = {
                entity: item.belongsTo.entity || item.belongsTo.name,
                id: item.belongsTo.id
              };
            }
            return newEntity;
          })
        }
      ]
    });
    if (page.current !== 1) {
      yield put(fetchTrainRequest(project, nlu));
    }
    yield put(clearPredict('clear'));
    yield put(
      addNotification({
        title: 'Success',
        message: `NLU successfully trained.`,
        status: 'success',
        allowHTML: true,
        dismissible: true,
        dismissAfter: 5000
      })
    );
  } catch (err) {
    const errorMessage = get(err, 'response.data.message', 'Unknown error.');
    yield put(
      addNotification({
        title: 'Failed',
        message: errorMessage,
        status: 'error',
        dismissible: true,
        dismissAfter: 5000
      })
    );
    yield put(trainFail('add', errorMessage));
  }
}

function* watchNluFetched() {
  yield takeLatest(NLSTUDIO_NLU.FETCH_SUCCESS, selectNlu);
}

function* watchTrainFetch() {
  yield takeLatest(NLU_TRAIN.FETCH_REQUEST, fetchTrain);
}

function* watchTrainAdd() {
  yield takeLatest(NLU_TRAIN.ADD_REQUEST, addTrain);
}

function* trainFlow() {
  yield all([
    fork(watchNluFetched),
    fork(watchTrainFetch),
    fork(watchTrainAdd)
  ]);
}

export default trainFlow;
