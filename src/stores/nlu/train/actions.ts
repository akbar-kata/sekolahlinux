import NLU_TRAIN from './types';

export function clearTrain(action: string) {
  return {
    action,
    type: NLU_TRAIN.CLEAR,
    payload: { data: null }
  };
}

function requestTrain<T>(action: string, payload?: T) {
  return {
    action,
    payload,
    type: NLU_TRAIN[`${action.toUpperCase()}_REQUEST`]
  };
}

export function trainFail(action: string, error: string) {
  return {
    action,
    type: NLU_TRAIN[`${action.toUpperCase()}_FAIL`],
    payload: {
      error
    }
  };
}

export function fetchTrainRequest(
  project: string,
  id: string | null,
  page: number = 1
) {
  return requestTrain('fetch', { project, id, page });
}

export function fetchTrainSuccess({
  raw,
  page = { page: 1, total: 0, limit: 10 }
}: any) {
  const data = {};
  const index = raw.map((item: any) => {
    data[item.id] = item;
    return item.id;
  });
  return {
    type: NLU_TRAIN.FETCH_SUCCESS,
    action: 'fetch',
    payload: {
      index,
      data,
      page: { ...page }
    }
  };
}

export function addTrainRequest(project: string, nlu: string, data: any) {
  const { sentence, entities, traits } = data;
  return requestTrain('add', {
    project,
    nlu,
    data: {
      sentence,
      entities,
      traits
    }
  });
}

export function addTrainSuccess(
  id: string,
  sentence: string,
  data: any[],
  page: any
) {
  return {
    page,
    type: NLU_TRAIN.ADD_SUCCESS,
    action: 'add',
    payload: {
      data: {
        [id]: {
          id,
          input: sentence,
          entities: data
        }
      }
    }
  };
}
