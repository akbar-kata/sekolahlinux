import { combineReducers } from 'redux';

import { ReduxAction } from 'interfaces';
import * as Predict from 'interfaces/nlu/predict';

import NLU_PREDICT from './types';

const loadingInit = {
  fetch: false,
  add: false,
  update: false,
  remove: false
};

const loadings = (
  state: Predict.NluPredictLoadingState = loadingInit,
  { type, action, payload }: ReduxAction
) => {
  const actionCaps = action ? action.toUpperCase() : 'FETCH';
  switch (type) {
    case NLU_PREDICT[`${actionCaps}_REQUEST`]:
      return Object.assign({}, state, {
        [action as string]: true
      });
    case NLU_PREDICT[`${actionCaps}_SUCCESS`]:
    case NLU_PREDICT[`${actionCaps}_FAIL`]:
      return Object.assign({}, state, {
        [action as string]: false
      });
    default:
      return state;
  }
};

const errorsInit = {
  fetch: null,
  add: null,
  update: null,
  remove: null
};

const errors = (
  state: Predict.NluPredictErrors = errorsInit,
  { type, action, payload }: ReduxAction
) => {
  const actionCaps = action ? action.toUpperCase() : 'FETCH';
  switch (type) {
    case NLU_PREDICT.CLEAR:
    case NLU_PREDICT[`${actionCaps}_REQUEST`]:
      return Object.assign({}, state, {
        [action as string]: null
      });
    case NLU_PREDICT[`${actionCaps}_FAIL`]:
      return Object.assign({}, state, {
        [action as string]: payload.error
      });
    default:
      return state;
  }
};

const data = (
  state: Predict.NluPredictDataMap = {},
  { type, payload }: ReduxAction
) => {
  switch (type) {
    case NLU_PREDICT.FETCH_REQUEST:
      return {};
    case NLU_PREDICT.FETCH_SUCCESS:
      return Object.assign({}, payload.data);
    case NLU_PREDICT.CLEAR:
      return {};
    default:
      return state;
  }
};

const lastUpdate = (state: number = Date.now(), { type }: ReduxAction) => {
  switch (type) {
    default:
      return state;
  }
};

const index = (state: string[] = [], { type, payload }: ReduxAction) => {
  switch (type) {
    case NLU_PREDICT.FETCH_REQUEST:
      return [];
    case NLU_PREDICT.FETCH_SUCCESS:
      return [...payload.index];
    default:
      return state;
  }
};

export default combineReducers({
  index,
  data,
  loadings,
  errors,
  lastUpdate
});
