import { all, call, fork, put, takeLatest } from 'redux-saga/effects';
import get from 'lodash-es/get';

import { predictFail, fetchPredictSuccess } from './actions';
import { callApi } from 'stores/services';
import { ReduxAction } from 'interfaces';
import NLU_PREDICT from './types';

function* fetchPredict({ payload: { projectId, nluName, data } }: ReduxAction) {
  try {
    const predict = yield call(
      callApi,
      'post',
      `/projects/${projectId}/nlus/${nluName}/predict`,
      {
        sentence: data.sentence,
        filterResult: data.filter || 'all',
        log: data.log
      }
    );
    yield put(fetchPredictSuccess(predict));
  } catch (err) {
    const errorMessage = get(
      err,
      'response.data.message',
      'Oops.. something wrong'
    );
    yield put(predictFail('fetch', errorMessage));
  }
}

function* watchPredictFetch() {
  yield takeLatest(NLU_PREDICT.FETCH_REQUEST, fetchPredict);
}

function* predictFlow() {
  yield all([fork(watchPredictFetch)]);
}

export default predictFlow;
