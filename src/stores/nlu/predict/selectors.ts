import * as Predict from 'interfaces/nlu/predict';

export function getPredictLoading(
  state: Predict.NluPredictStore,
  type: string
): boolean {
  return state.loadings[type];
}

export function getPredictError(
  state: Predict.NluPredictStore,
  type: string
): string | null {
  return state.errors[type];
}

export function getPredictLastUpdated(state: Predict.NluPredictStore): number {
  return state.lastUpdate;
}

export function getPredictIndex(state: Predict.NluPredictStore): string[] {
  return state.index;
}

export function getPredictData(state: Predict.NluPredictStore) {
  return state.data;
}
