import NLU_PREDICT from './types';
import { mapPredictToTraining } from 'utils/tranformPredict';
import { NluPredictParams as PredictParams } from 'interfaces/nlu/predict';

export function clearPredict(action: string) {
  return {
    action,
    type: NLU_PREDICT.CLEAR,
    payload: { data: {} }
  };
}
export function clearPrediction() {
  return {
    type: NLU_PREDICT.CLEAR,
    payload: { data: {} }
  };
}

function requestPredict<T>(action: string, payload?: T) {
  return {
    action,
    payload,
    type: NLU_PREDICT[`${action.toUpperCase()}_REQUEST`]
  };
}

export function predictFail(action: string, error: string) {
  return {
    action,
    type: NLU_PREDICT[`${action.toUpperCase()}_FAIL`],
    payload: {
      error
    }
  };
}

export function fetchPredictRequest(
  projectId: string,
  nluName: string,
  data: PredictParams
) {
  return requestPredict('fetch', {
    projectId,
    nluName,
    data
  });
}

export function fetchPredictSuccess(raw: any) {
  let data = {};
  const res = mapPredictToTraining(raw);
  const index = res.result.map((item: any) => {
    data = item;
    return item.id;
  });
  return {
    type: NLU_PREDICT.FETCH_SUCCESS,
    action: 'fetch',
    payload: { index, data }
  };
}
