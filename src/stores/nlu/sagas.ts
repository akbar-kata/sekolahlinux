import { all, fork, put, takeLatest, select } from 'redux-saga/effects';

import RootStore from 'interfaces/rootStore';

// import * as NluEntityActions from './entity/actions';
import * as NluActions from './nlu/actions';
// import * as NluTrainingActions from './train/actions';
// import * as NluLogActions from './log/actions';
import Nlu from './nlu/sagas';
import Entity from './entity/sagas';
import Log from './log/sagas';
import Train from './train/sagas';
import Predict from './predict/sagas';
import Testing from './testing/sagas';
import PROJECT_TYPES from 'stores/project/types';
import { ReduxAction } from 'interfaces';
import {
  getProjectSelected
  // getProjectOptions
} from 'stores/project/selectors';
// import { getCurrentNlu } from './nlu/selectors';

function* selectProjectHandler({ payload }: ReduxAction) {
  const selectedProject = yield select((state: RootStore) =>
    getProjectSelected(state.project)
  );

  if (selectedProject) {
    yield put(NluActions.fetchNluRequest(selectedProject));

    // const nluId: string | null = yield select((state: RootStore) => {
    //   const selectedNlu = getCurrentNlu(state.nlu.nlu);
    //   const options = getProjectOptions(state.project, selectedProject);

    //   return selectedNlu
    //     ? selectedNlu.id
    //     : options && options.nluId
    //     ? options.nluId
    //     : null;
    // });

    // if (nluId) {
    //   yield put(NluEntityActions.fetchEntityRequest(selectedProject, nluId));
    //   yield put(NluTrainingActions.fetchTrainRequest(selectedProject, nluId));
    //   yield put(NluLogActions.fetchLogRequest(selectedProject, nluId));
    // }
  }
}

function* watchProjectSelected() {
  yield takeLatest(PROJECT_TYPES.SELECT_PROJECT, selectProjectHandler);
}

function* botFlow() {
  yield all([
    fork(watchProjectSelected),
    fork(Nlu),
    fork(Entity),
    fork(Log),
    fork(Train),
    fork(Predict),
    fork(Testing)
  ]);
}

export default botFlow;
