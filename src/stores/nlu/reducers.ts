import { combineReducers } from 'redux';

import { NlStudioRootStore } from 'interfaces/nlu';

import Nlu from './nlu/reducers';
import Entity from './entity/reducers';
import Log from './log/reducers';
import Train from './train/reducers';
import Predict from './predict/reducers';
import Testing from './testing/reducers';

export default combineReducers<NlStudioRootStore>({
  nlu: Nlu,
  entity: Entity,
  log: Log,
  train: Train,
  predict: Predict,
  testing: Testing
});
