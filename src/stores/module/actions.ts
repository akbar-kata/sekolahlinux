import MODULE, {
  DisableModuleRequestAction,
  DisableModuleSuccessAction,
  EnableModuleRequestAction,
  EnableModuleSuccessAction,
  FetchModuleRequestAction,
  FetchModuleSuccessAction,
  SetSelectedModuleAction,
  SetSelectedModuleNoPushAction,
  UpdateModuleRequestAction,
  UpdateModuleSuccessAction
} from 'stores/module/types';

export function setSelectedModule(id: string): SetSelectedModuleAction {
  return {
    type: MODULE.SET_SELECTED_MODULE,
    payload: id
  };
}

export function setSelectedModuleNoPush(
  id: string
): SetSelectedModuleNoPushAction {
  return {
    type: MODULE.SET_SELECTED_MODULE_NO_PUSH,
    payload: id
  };
}

export function fetchModuleRequest(): FetchModuleRequestAction {
  return {
    type: MODULE.FETCH_MODULE_REQUEST
  };
}

export function fetchModuleSuccess(
  payload: FetchModuleSuccessAction['payload']
): FetchModuleSuccessAction {
  return {
    payload,
    type: MODULE.FETCH_MODULE_SUCCESS
  };
}

export function enableModuleRequest(
  payload: EnableModuleRequestAction['payload']
): EnableModuleRequestAction {
  return {
    payload,
    type: MODULE.ENABLE_MODULE_REQUEST
  };
}

export function enableModuleSuccess(
  payload: EnableModuleSuccessAction['payload']
): EnableModuleSuccessAction {
  return {
    payload,
    type: MODULE.ENABLE_MODULE_SUCCESS
  };
}

export function disableModuleRequest(
  payload: DisableModuleRequestAction['payload']
): DisableModuleRequestAction {
  return {
    payload,
    type: MODULE.DISABLE_MODULE_REQUEST
  };
}

export function disableModuleSuccess(
  payload: DisableModuleSuccessAction['payload']
): DisableModuleSuccessAction {
  return {
    payload,
    type: MODULE.DISABLE_MODULE_SUCCESS
  };
}

export function updateModuleRequest(
  payload: UpdateModuleRequestAction['payload']
): UpdateModuleRequestAction {
  return {
    payload,
    type: MODULE.UPDATE_MODULE_REQUEST
  };
}

export function updateModuleSuccess(
  payload: UpdateModuleSuccessAction['payload']
): UpdateModuleSuccessAction {
  return {
    payload,
    type: MODULE.UPDATE_MODULE_SUCCESS
  };
}
