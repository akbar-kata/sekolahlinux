import { combineReducers } from 'redux';
import { assertNever } from 'utils/redux';
import MODULE, {
  DisableModuleSuccessAction,
  EnableModuleSuccessAction,
  FetchModuleSuccessAction,
  SetSelectedModuleAction,
  SetSelectedModuleNoPushAction,
  UpdateModuleSuccessAction
} from './types';

import { Module, ModuleStore } from 'interfaces/module';
import { DataMap } from 'interfaces/common';

const selected = (
  state: string | null = null,
  action: SetSelectedModuleAction | SetSelectedModuleNoPushAction
) => {
  switch (action.type) {
    case MODULE.SET_SELECTED_MODULE:
    case MODULE.SET_SELECTED_MODULE_NO_PUSH:
      return action.payload;
    default:
      assertNever(action);
      return state;
  }
};

const data = (
  state: DataMap<Module> = {},
  action:
    | FetchModuleSuccessAction
    | DisableModuleSuccessAction
    | EnableModuleSuccessAction
    | UpdateModuleSuccessAction
) => {
  switch (action.type) {
    case MODULE.FETCH_MODULE_SUCCESS:
      const mapped: DataMap<Module> = {};
      action.payload.forEach(module => (mapped[module.id] = module));
      return mapped;
    case MODULE.DISABLE_MODULE_SUCCESS:
      return {
        ...state,
        [action.payload.id]: {
          ...state[action.payload.id],
          enabled: false
        }
      };
    case MODULE.ENABLE_MODULE_SUCCESS:
      return {
        ...state,
        [action.payload.id]: {
          ...state[action.payload.id],
          enabled: true
        }
      };
    case MODULE.UPDATE_MODULE_SUCCESS:
      return {
        ...state,
        [action.payload.id]: {
          ...state[action.payload.id],
          hasUpdate: false
        }
      };
    default:
      assertNever(action);
      return state;
  }
};
const index = (state: string[] = [], action: FetchModuleSuccessAction) => {
  switch (action.type) {
    case MODULE.FETCH_MODULE_SUCCESS:
      return action.payload.map(module => module.id);
    default:
      return state;
  }
};

export default combineReducers<ModuleStore>({
  selected,
  data,
  index
});
