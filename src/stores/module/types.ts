import { ExReduxAction, VoidExReduxAction } from 'interfaces';
import { Module } from 'interfaces/module';

enum MODULE {
  SET_SELECTED_MODULE = 'MODULE/SET_SELECTED_MODULE',
  SET_SELECTED_MODULE_NO_PUSH = 'MODULE/SET_SELECTED_MODULE_NO_PUSH',

  FETCH_MODULE_REQUEST = 'MODULE/ALL/FETCH_MODULE_REQUEST',
  FETCH_MODULE_SUCCESS = 'MODULE/ALL/FETCH_MODULE_SUCCESS',
  FETCH_MODULE_FAILED = 'MODULE/ALL/FETCH_MODULE_FAILED',

  ENABLE_MODULE_REQUEST = 'MODULE/ENABLE_MODULE_REQUEST',
  ENABLE_MODULE_SUCCESS = 'MODULE/ENABLE_MODULE_SUCCESS',
  ENABLE_MODULE_FAILED = 'MODULE/ENABLE_MODULE_FAILED',

  UPDATE_MODULE_REQUEST = 'MODULE/UPDATE_MODULE_REQUEST',
  UPDATE_MODULE_SUCCESS = 'MODULE/UPDATE_MODULE_SUCCESS',
  UPDATE_MODULE_FAILED = 'MODULE/UPDATE_MODULE_FAILED',

  DISABLE_MODULE_REQUEST = 'MODULE/DISABLE_MODULE_REQUEST',
  DISABLE_MODULE_SUCCESS = 'MODULE/DISABLE_MODULE_SUCCESS',
  DISABLE_MODULE_FAILED = 'MODULE/DISABLE_MODULE_FAILED'
}

export type SetSelectedModuleAction = ExReduxAction<
  MODULE.SET_SELECTED_MODULE,
  string
>;
export type SetSelectedModuleNoPushAction = ExReduxAction<
  MODULE.SET_SELECTED_MODULE_NO_PUSH,
  string
>;

export type DisableModuleRequestAction = ExReduxAction<
  MODULE.DISABLE_MODULE_REQUEST,
  Module
>;
export type DisableModuleSuccessAction = ExReduxAction<
  MODULE.DISABLE_MODULE_SUCCESS,
  Module
>;

export type FetchModuleRequestAction = VoidExReduxAction<
  MODULE.FETCH_MODULE_REQUEST
>;
export type FetchModuleSuccessAction = ExReduxAction<
  MODULE.FETCH_MODULE_SUCCESS,
  Module[]
>;

export type EnableModuleRequestAction = ExReduxAction<
  MODULE.ENABLE_MODULE_REQUEST,
  Module
>;
export type EnableModuleSuccessAction = ExReduxAction<
  MODULE.ENABLE_MODULE_SUCCESS,
  Module
>;

export type UpdateModuleRequestAction = ExReduxAction<
  MODULE.UPDATE_MODULE_REQUEST,
  Module
>;
export type UpdateModuleSuccessAction = ExReduxAction<
  MODULE.UPDATE_MODULE_SUCCESS,
  Module
>;

export default MODULE;
