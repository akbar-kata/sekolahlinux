import { all as sagaAll, fork, put, takeLatest } from 'redux-saga/effects';
import { LOCATION_CHANGE, push, RouterState } from 'connected-react-router';

import MODULE, {
  DisableModuleRequestAction,
  EnableModuleRequestAction,
  FetchModuleRequestAction,
  SetSelectedModuleAction,
  UpdateModuleRequestAction
} from './types';
import { TypedReduxAction } from 'interfaces';
import {
  disableModuleSuccess,
  enableModuleSuccess,
  fetchModuleSuccess,
  setSelectedModuleNoPush,
  updateModuleSuccess
} from './actions';
import { setLoading } from 'stores/app/loadings/action';
import { delay } from 'redux-saga';
import { addNotification } from 'stores/app/notification/actions';
import { Module } from 'interfaces/module';

const data: Module[] = [
  {
    id: 'modid-1',
    name: 'Titanfall Module',
    description:
      'Turn water into wine, tulisanna panjang biar nanti ada ellipsisnya, ' +
      'kalau ga panjang gimana mau ada ellipsisnya? iya kan, percaya deh ini ' +
      'panjang banget sampai tslint anggap ini error kalau ga dipisah pakai +',
    author: 'Kata.ai',
    version: '1.0.0',
    hasUpdate: true,
    enabled: false
  },
  {
    id: 'modid-2',
    name: 'Roshan',
    description: 'Has fallen to the radiant',
    author: 'Kata.ai',
    version: '1.0.1',
    hasUpdate: false,
    enabled: true,
    settings: {
      api_url: {
        id: 'api_url',
        name: 'api_url',
        label: 'API URL',
        type: 'text',
        value: ''
      },
      google_secret_token: {
        id: 'google_secret_token',
        name: 'google_secret_token',
        label: 'Google Secret Token',
        type: 'textarea',
        value: ''
      }
    }
  }
];

function* watchFetchDataRequest() {
  yield takeLatest(MODULE.FETCH_MODULE_REQUEST, fetchDataRequestHandler);
}

function* fetchDataRequestHandler(action: FetchModuleRequestAction) {
  yield put(setLoading(MODULE.FETCH_MODULE_REQUEST, true));
  yield delay(1000);
  yield put(fetchModuleSuccess(data));
  yield put(setLoading(MODULE.FETCH_MODULE_REQUEST, false));
}

function* watchEnableModuleRequest() {
  yield takeLatest(MODULE.ENABLE_MODULE_REQUEST, enableModuleRequestHandler);
}

function* enableModuleRequestHandler(action: EnableModuleRequestAction) {
  yield put(
    setLoading(`${MODULE.ENABLE_MODULE_REQUEST}_${action.payload.id}`, true)
  );
  yield delay(1000);
  yield put(push(`/module/enabled`));
  yield put(enableModuleSuccess(action.payload));
  yield put(
    addNotification({
      title: `Module "${action.payload.name}" enabled!`,
      message: `Module "${action.payload.name}" has been enabled`,
      status: 'success',
      dismissible: true,
      dismissAfter: 5000
    })
  );
  yield put(
    setLoading(`${MODULE.ENABLE_MODULE_REQUEST}_${action.payload.id}`, false)
  );
}

function* watchUpdateModuleRequest() {
  yield takeLatest(MODULE.UPDATE_MODULE_REQUEST, updateModuleRequestHandler);
}

function* updateModuleRequestHandler(action: UpdateModuleRequestAction) {
  yield put(
    setLoading(`${MODULE.UPDATE_MODULE_REQUEST}_${action.payload.id}`, true)
  );
  yield delay(3000);
  yield put(updateModuleSuccess(action.payload));
  yield put(
    addNotification({
      title: `Module "${action.payload.name}" updated!`,
      message: `Module "${action.payload.name}" has been updated`,
      status: 'success',
      dismissible: true,
      dismissAfter: 5000
    })
  );
  yield put(
    setLoading(`${MODULE.UPDATE_MODULE_REQUEST}_${action.payload.id}`, false)
  );
}

function* watchModuleSelected() {
  yield takeLatest(MODULE.SET_SELECTED_MODULE, moduleSelectedHandler);
}

function* moduleSelectedHandler(action: SetSelectedModuleAction) {
  yield put(push(`${action.payload}/overview`));
}

function* watchLocationChanged() {
  yield takeLatest(LOCATION_CHANGE, setSelectedModuleFromUrl);
}

function* setSelectedModuleFromUrl({ payload }: TypedReduxAction<RouterState>) {
  if (payload && payload.location && payload.location.pathname) {
    const rx = /\/module\/([A-Za-z0-9-_]+)/;
    const match = rx.exec(payload.location.pathname);
    if (match) {
      yield put(setSelectedModuleNoPush(match[1]));
    }
  }
}

function* watchDisableModuleRequest() {
  yield takeLatest(MODULE.DISABLE_MODULE_REQUEST, disableModuleRequestHandler);
}

function* disableModuleRequestHandler(action: DisableModuleRequestAction) {
  yield put(setLoading(MODULE.DISABLE_MODULE_REQUEST, true));
  yield delay(1000);
  yield put(
    addNotification({
      title: `Module "${action.payload.name}" disabled!`,
      message: `Module "${action.payload.name}" has been disabled`,
      status: 'success',
      dismissible: true,
      dismissAfter: 5000
    })
  );
  yield put(disableModuleSuccess(action.payload));
  yield put(setLoading(MODULE.DISABLE_MODULE_REQUEST, false));
}

export default function* root() {
  yield sagaAll([
    fork(watchLocationChanged),
    fork(watchModuleSelected),
    fork(watchDisableModuleRequest),
    fork(watchUpdateModuleRequest),
    fork(watchEnableModuleRequest),
    fork(watchFetchDataRequest)
  ]);
}
