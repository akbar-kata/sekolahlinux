import RootStore from 'interfaces/rootStore';
import { Module, ModuleStore } from 'interfaces/module';
import MODULE from 'stores/module/types';
import { getLoading } from 'stores/app/loadings/selectors';
import { Store as LoadingStore } from 'interfaces/app/loadings';

export function getSelectedModule(store: RootStore) {
  const selectedId = store.module.selected;
  return selectedId ? store.module.data[selectedId] : undefined;
}

export function getModuleData(store: ModuleStore) {
  return store.data;
}

export function getModuleIndex(store: ModuleStore) {
  return store.index;
}

export function getModuleLoadingState(
  store: LoadingStore,
  type: MODULE.UPDATE_MODULE_REQUEST | MODULE.ENABLE_MODULE_REQUEST,
  module: Module
) {
  return getLoading(store, `${type}_${module.id}`);
}
