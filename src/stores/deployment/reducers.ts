import { combineReducers } from 'redux';

import { ReduxAction } from 'interfaces';
import * as Deployment from 'interfaces/deployment';
import environment from './environment/reducers';
import channel from './channel/reducers';
import DEPLOYMENTS from './types';

const loadingInit = {
  fetch: false,
  add: false,
  update: false,
  remove: false
};

const loadings = (
  state: Deployment.Loadings = loadingInit,
  { type, action, payload }: ReduxAction
) => {
  const actionCaps = action ? action.toUpperCase() : 'FETCH';
  switch (type) {
    case DEPLOYMENTS[`${actionCaps}_REQUEST`]:
      return Object.assign({}, state, {
        [action as string]: true
      });
    case DEPLOYMENTS[`${actionCaps}_SUCCESS`]:
    case DEPLOYMENTS[`${actionCaps}_FAIL`]:
      return Object.assign({}, state, {
        [action as string]: false
      });
    default:
      return state;
  }
};

const errorsInit = {
  fetch: null,
  add: null,
  update: null,
  remove: null
};

const errors = (
  state: Deployment.Errors = errorsInit,
  { type, action, payload }: ReduxAction
) => {
  const actionCaps = action ? action.toUpperCase() : 'FETCH';
  switch (type) {
    case DEPLOYMENTS.ERROR_CLEAR:
    case DEPLOYMENTS[`${actionCaps}_REQUEST`]:
      return Object.assign({}, state, {
        [action as string]: null
      });
    case DEPLOYMENTS[`${actionCaps}_FAIL`]:
      return Object.assign({}, state, {
        [action as string]: payload.error
      });
    default:
      return state;
  }
};

const selected = (
  state: string | null = null,
  { type, payload }: ReduxAction
) => {
  switch (type) {
    case DEPLOYMENTS.FETCH_FAIL:
      return null;
    case DEPLOYMENTS.SELECT:
      return payload;
    default:
      return state;
  }
};

const data = (
  state: Deployment.DataMap = {},
  { type, payload }: ReduxAction
) => {
  switch (type) {
    case DEPLOYMENTS.FETCH_SUCCESS:
      return Object.assign({}, payload.data);
    case DEPLOYMENTS.ADD_SUCCESS:
    case DEPLOYMENTS.UPDATE_SUCCESS:
      return Object.assign({}, state, payload);
    case DEPLOYMENTS.REMOVE_SUCCESS:
      const { [payload.name]: deletedItem, ...rest } = state;
      return rest;
    default:
      return state;
  }
};

const index = (state: string[] = [], { type, payload }: ReduxAction) => {
  switch (type) {
    case DEPLOYMENTS.FETCH_FAIL:
      return [];
    case DEPLOYMENTS.FETCH_SUCCESS:
      return [...payload.index];
    case DEPLOYMENTS.ADD_SUCCESS:
      return [...state, ...Object.keys(payload)];
    case DEPLOYMENTS.REMOVE_SUCCESS:
      return state.filter(name => name !== payload.name);
    default:
      return state;
  }
};

const latestDeployment = (
  state: Deployment.Data | null = null,
  { type, payload }: ReduxAction
) => {
  switch (type) {
    case DEPLOYMENTS.FETCH_LATEST_DEPLOYMENT_SUCCESS:
      return payload;
    case DEPLOYMENTS.FETCH_LATEST_DEPLOYMENT_FAILED:
      return null;
    default:
      return state;
  }
};

const lastUpdate = (state: number = Date.now(), { type }: ReduxAction) => {
  switch (type) {
    default:
      return state;
  }
};

export default combineReducers<Deployment.Store>({
  selected,
  index,
  data,
  latestDeployment,
  loadings,
  errors,
  lastUpdate,
  environment,
  channel
});
