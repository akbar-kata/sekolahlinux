import { JsonArray } from 'interfaces';
import * as Channel from 'interfaces/channel';
import CHANNEL from './types';

export function clearChannelError(action: string) {
  return {
    action,
    type: CHANNEL.ERROR_CLEAR
  };
}

export function clearChannelList() {
  return {
    type: CHANNEL.CLEAR
  };
}

function requestChannel<T>(action: string, payload?: T) {
  return {
    action,
    payload,
    type: CHANNEL[`${action.toUpperCase()}_REQUEST`]
  };
}

export function channelFail(action: string, error: string) {
  return {
    action,
    type: CHANNEL[`${action.toUpperCase()}_FAIL`],
    payload: {
      error
    }
  };
}

export function selectChannel(toSelect: string | null) {
  return {
    type: CHANNEL.SELECT,
    payload: toSelect
  };
}

export function fetchChannelRequest(projectId: string, environmentId: string) {
  return requestChannel('fetch', {
    projectId,
    environmentId
  });
}

export function fetchChannelSuccess(raw: JsonArray) {
  const data = {};
  const index = raw
    .filter((item: any) => !!item && !!item.id)
    .map((item: any) => {
      data[item.id] = item;
      return item.id;
    });
  return {
    type: CHANNEL.FETCH_SUCCESS,
    action: 'fetch',
    payload: {
      index,
      data
    }
  };
}

export function addChannelRequest(
  projectId: string,
  environmentId: string,
  data: Channel.Data
) {
  return requestChannel('add', { projectId, environmentId, data });
}

export function addChannelSuccess(data: Channel.Data) {
  return {
    type: CHANNEL.ADD_SUCCESS,
    action: 'add',
    payload: { data }
  };
}

export function updateChannelRequest(
  id: string,
  projectId: string,
  environmentId: string,
  data: Channel.Data
) {
  return requestChannel('update', { id, projectId, environmentId, data });
}

export function updateChannelSuccess(id: string, data: Channel.Data) {
  return {
    type: CHANNEL.UPDATE_SUCCESS,
    action: 'update',
    payload: { id }
  };
}

export function removeChannelRequest(
  id: string,
  name: string,
  projectId: string,
  environmentId: string
) {
  return requestChannel('remove', { id, name, projectId, environmentId });
}

export function removeChannelSuccess(id: string) {
  return {
    type: CHANNEL.REMOVE_SUCCESS,
    action: 'remove',
    payload: { id }
  };
}
