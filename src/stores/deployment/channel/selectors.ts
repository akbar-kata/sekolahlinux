import * as Channel from 'interfaces/deployment/channel';

export function getChannelLoading(state: Channel.Store, type: string): boolean {
  return state.loadings[type];
}

export function getChannelError(
  state: Channel.Store,
  type: string
): string | null {
  return state.errors[type];
}

export function getChannelSelected(state: Channel.Store): string | null {
  return state.selected;
}

export function getChannelLastUpdated(state: Channel.Store): number {
  return state.lastUpdate;
}

export function getChannelIndex(state: Channel.Store): string[] {
  return state.index;
}

export function getChannelData(state: Channel.Store): Channel.DataMap {
  return state.data;
}

export function getChannelDetail(
  state: Channel.Store,
  channelId: string
): Channel.Data {
  return state.data[channelId];
}
