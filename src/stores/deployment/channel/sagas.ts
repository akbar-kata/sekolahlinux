import { select, all, call, fork, put, takeLatest } from 'redux-saga/effects';
import isArray from 'lodash-es/isArray';
import get from 'lodash-es/get';

import { ReduxAction } from 'interfaces';
import RootStore from 'interfaces/rootStore';
import {
  channelFail,
  selectChannel,
  fetchChannelRequest,
  fetchChannelSuccess,
  // addChannelSuccess,
  // updateChannelSuccess,
  removeChannelSuccess
} from './actions';
import { closeDrawer } from 'stores/app/drawer/actions';
import { callApi } from 'stores/services';
import ENVIRONMENT from 'stores/deployment/environment/types';
import CHANNEL from './types';
import { addNotification } from 'stores/app/notification/actions';
import { getProjectSelected } from 'stores/project/selectors';
import { getEnvironmentDetail } from '../environment/selectors';

function* selectEnvironment({ payload }: ReduxAction) {
  const PROJECT_ID = yield select((store: RootStore) => store.project.selected);

  if (PROJECT_ID) {
    yield put(fetchChannelRequest(PROJECT_ID, payload));
  }
}

function* fetchChannel({ payload }: ReduxAction) {
  try {
    const selectedProject = yield select((state: RootStore) =>
      getProjectSelected(state.project)
    );

    const resp = yield call(
      callApi,
      'get',
      `/projects/${selectedProject}/environments/${
        payload.environmentId
      }/channels`
    );
    const data = isArray(resp) ? resp : [];
    yield put(fetchChannelSuccess(data));

    if (data.length > 0 && data[0] && data[0].id) {
      yield put(selectChannel(data[0].id));
    }
  } catch (err) {
    const errorMessage = get(
      err,
      'response.data.message',
      'Failed to fetch channels.'
    );
    yield put(channelFail('fetch', errorMessage));
  }
}

function* addChannel({ payload: { environmentId, data } }: ReduxAction) {
  const selectedProject = yield select((state: RootStore) =>
    getProjectSelected(state.project)
  );

  try {
    yield call(
      callApi,
      'post',
      `/projects/${selectedProject}/environments/${environmentId}/channels`,
      {
        name: data.name,
        type: data.type,
        options: data.options,
        url: data.url
      }
    );

    // yield put(
    //   addChannelSuccess(
    //     Object.assign({}, resp, {
    //       options: JSON.parse(resp.options)
    //     })
    //   )
    // );
    yield put(
      addNotification({
        title: `Success`,
        message: `Channel <strong>${data.name}</strong> has been created.`,
        status: 'success',
        allowHTML: true,
        dismissible: true,
        dismissAfter: 5000
      })
    );
    yield put(closeDrawer('ChannelCreate')); // 'DeploymentCreate' is drawer id registered when drawer opened
  } catch (err) {
    const errorMessage = get(
      err,
      'response.data.message',
      `Failed to create <strong>${data.name}</strong> channel.`
    );
    yield put(
      addNotification({
        title: 'Failed',
        message: errorMessage,
        status: 'error',
        allowHTML: true,
        dismissible: true,
        dismissAfter: 5000
      })
    );
    yield put(channelFail('add', errorMessage));
  } finally {
    yield put(fetchChannelRequest(selectedProject, environmentId));
  }
}

function* updateChannel({
  payload: { id, projectId, environmentId, data }
}: ReduxAction) {
  try {
    yield call(
      callApi,
      'put',
      `/projects/${projectId}/environments/${environmentId}/channels/${id}`,
      {
        id,
        name: data.name,
        type: data.type,
        options: data.options,
        url: data.url
      }
    );
    // yield put(
    //   updateChannelSuccess(
    //     id,
    //     Object.assign({}, resp, {
    //       options: JSON.parse(resp.options)
    //     })
    //   )
    // );
    yield put(
      addNotification({
        title: `Success`,
        message: `Channel <strong>${data.name}</strong> has been updated.`,
        status: 'success',
        allowHTML: true,
        dismissible: true,
        dismissAfter: 5000
      })
    );
    yield put(closeDrawer('ChannelUpdate')); // 'DeploymentCreate' is drawer id registered when drawer opened
  } catch (err) {
    const errorMessage = get(
      err,
      'response.data.message',
      `Failed to update <strong>${data.name}</strong> channel.`
    );
    yield put(
      addNotification({
        title: 'Failed to Update Channel',
        message: errorMessage,
        status: 'error',
        allowHTML: true,
        dismissible: true,
        dismissAfter: 5000
      })
    );
    yield put(channelFail('update', errorMessage));
  } finally {
    yield put(fetchChannelRequest(projectId, environmentId));
  }
}

function* removeChannel({
  payload: { id, name, projectId, environmentId }
}: ReduxAction) {
  try {
    const env = yield select(
      ({ deployment }: RootStore) =>
        getEnvironmentDetail(deployment.environment, environmentId).id
    );

    yield call(
      callApi,
      'delete',
      `/projects/${projectId}/environments/${env}/channels/${id}`
    );
    yield put(removeChannelSuccess(id));
    yield put(
      addNotification({
        title: `Success`,
        message: `Channel <strong>${name}</strong> has been deleted.`,
        status: 'success',
        allowHTML: true,
        dismissible: true,
        dismissAfter: 5000
      })
    );
  } catch (err) {
    const errorMessage = get(
      err,
      'response.data.message',
      `Failed to delete <strong>${name}</strong> channel.`
    );
    yield put(
      addNotification({
        title: 'Failed to Update Channel',
        message: errorMessage,
        status: 'error',
        allowHTML: true,
        dismissible: true,
        dismissAfter: 5000
      })
    );
    yield put(channelFail('remove', errorMessage));
  } finally {
    yield put(fetchChannelRequest(projectId, environmentId));
  }
}

function* watchEnvironmentSelected() {
  yield takeLatest(ENVIRONMENT.SELECT, selectEnvironment);
}

function* watchChannelFetch() {
  yield takeLatest(CHANNEL.FETCH_REQUEST, fetchChannel);
}

function* watchChannelAdd() {
  yield takeLatest(CHANNEL.ADD_REQUEST, addChannel);
}

function* watchChannelUpdate() {
  yield takeLatest(CHANNEL.UPDATE_REQUEST, updateChannel);
}

function* watchChannelRemove() {
  yield takeLatest(CHANNEL.REMOVE_REQUEST, removeChannel);
}

function* channelFlow() {
  yield all([
    fork(watchEnvironmentSelected),
    fork(watchChannelFetch),
    fork(watchChannelAdd),
    fork(watchChannelUpdate),
    fork(watchChannelRemove)
  ]);
}

export default channelFlow;
