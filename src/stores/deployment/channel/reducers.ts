import { combineReducers } from 'redux';

import { ReduxAction } from 'interfaces';
import * as Channel from 'interfaces/channel';
import CHANNEL from './types';

const initialState: Channel.Store = {
  loadings: {
    fetch: false,
    add: false,
    update: false,
    remove: false
  },
  errors: {
    fetch: null,
    add: null,
    update: null,
    remove: null
  },
  selected: null,
  lastUpdate: Date.now(),
  data: {},
  index: []
};

const loadings = (
  state: Channel.Loadings = initialState.loadings,
  { type, action, payload }: ReduxAction
) => {
  const actionCaps = action ? action.toUpperCase() : 'FETCH';
  switch (type) {
    case CHANNEL[`${actionCaps}_REQUEST`]:
      return Object.assign({}, state, {
        [action as string]: true
      });
    case CHANNEL[`${actionCaps}_SUCCESS`]:
    case CHANNEL[`${actionCaps}_FAIL`]:
      return Object.assign({}, state, {
        [action as string]: false
      });
    default:
      return state;
  }
};

const errors = (
  state: Channel.Errors = initialState.errors,
  { type, action, payload }: ReduxAction
) => {
  const actionCaps = action ? action.toUpperCase() : 'FETCH';
  switch (type) {
    case CHANNEL.ERROR_CLEAR:
    case CHANNEL[`${actionCaps}_REQUEST`]:
      return Object.assign({}, state, {
        [action as string]: null
      });
    case CHANNEL[`${actionCaps}_FAIL`]:
      return Object.assign({}, state, {
        [action as string]: payload.error
      });
    default:
      return state;
  }
};

const selected = (
  state: string | null = initialState.selected,
  { type, payload }: ReduxAction
) => {
  switch (type) {
    case CHANNEL.FETCH_FAIL:
      return null;
    case CHANNEL.SELECT:
      return payload;
    case CHANNEL.CLEAR:
      return initialState.selected;
    default:
      return state;
  }
};

const data = (
  state: Channel.DataMap = initialState.data,
  { type, payload }: ReduxAction
) => {
  switch (type) {
    case CHANNEL.FETCH_SUCCESS:
      return Object.assign({}, payload.data);
    case CHANNEL.ADD_SUCCESS:
    case CHANNEL.UPDATE_SUCCESS:
      return Object.assign({}, state, payload);
    case CHANNEL.REMOVE_SUCCESS:
      const { [payload.id]: deletedItem, ...rest } = state;
      return rest;
    case CHANNEL.CLEAR:
      return initialState.data;
    default:
      return state;
  }
};

const index = (
  state: string[] = initialState.index,
  { type, payload }: ReduxAction
) => {
  switch (type) {
    case CHANNEL.CLEAR:
    case CHANNEL.FETCH_FAIL:
      return initialState.index;
    case CHANNEL.FETCH_SUCCESS:
      return [...payload.index];
    case CHANNEL.ADD_SUCCESS:
      return [...state, ...Object.keys(payload)];
    case CHANNEL.REMOVE_SUCCESS:
      return state.filter(id => id !== payload.id);
    default:
      return state;
  }
};

const lastUpdate = (state: number = Date.now(), { type }: ReduxAction) => {
  switch (type) {
    default:
      return state;
  }
};

export default combineReducers({
  selected,
  index,
  data,
  loadings,
  errors,
  lastUpdate
});
