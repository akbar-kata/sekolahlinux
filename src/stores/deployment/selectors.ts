import * as Deployment from 'interfaces/deployment';

export function getDeploymentLoading(
  state: Deployment.Store,
  type: string
): boolean {
  return state.loadings[type];
}

export function getDeploymentError(
  state: Deployment.Store,
  type: string
): string | null {
  return state.errors[type];
}

export function getDeploymentSelected(state: Deployment.Store): string | null {
  return state.selected;
}

export function getDeploymentLatest(
  state: Deployment.Store
): Deployment.Data | null {
  return state.selected ? state.data[state.selected] : null;
}

export function getDeploymentLatestVersion(
  state: Deployment.Store
): string | null {
  return state.selected ? state.data[state.selected].name : null;
}

export function getDeploymentLastUpdated(state: Deployment.Store): number {
  return state.lastUpdate;
}

export function getDeploymentIndex(state: Deployment.Store): string[] {
  return state.index;
}

export function getDeploymentData(state: Deployment.Store): Deployment.DataMap {
  return state.data;
}

export function getDeploymentDetail(
  state: Deployment.Store,
  deploymentId: string
): Deployment.Data {
  return state.data[deploymentId];
}

export function getLatestDeployment(store: Deployment.Store) {
  return store.latestDeployment;
}
