import { all, call, fork, put, select, takeLatest } from 'redux-saga/effects';
import get from 'lodash-es/get';

import Environment from './environment/sagas';
import Channel from './channel/sagas';
import DEPLOYMENTS from './types';

import { setLoading } from 'stores/app/loadings/action';
import { callApi } from 'stores/services';

import Store from 'interfaces/rootStore';
import * as CommonAction from 'stores/common/actions';
import * as DeploymentActions from './actions';
import * as EnvironmentActions from './environment/actions';
import { addNotification } from 'stores/app/notification/actions';
import { ReduxAction } from 'interfaces';
import { Data } from 'interfaces/deployment';
import { closeDrawer } from 'stores/app/drawer/actions';
import { closeModal } from 'stores/app/modal/actions';
import { getProjectSelected } from 'stores/project/selectors';
import PROJECT_TYPES from 'stores/project/types';
import { PaginatedData } from 'interfaces/common';

import { rollbackFormId } from 'modules/deployments/deployment-list/DeploymentList.Container';
import { DRAWER_ID } from 'modules/deployments/create-deployment/CreateDeployment.Container';

function* watchProjectSelected() {
  yield takeLatest(PROJECT_TYPES.SELECT_PROJECT, selectProjectHandler);
}

function* selectProjectHandler({ payload }: ReduxAction) {
  const selectedProject = yield select((state: Store) =>
    getProjectSelected(state.project)
  );

  if (selectedProject) {
    yield put(DeploymentActions.fetchDeploymentRequest());
    yield put(DeploymentActions.fetchLatestDeploymentRequest());
    yield put(EnvironmentActions.fetchEnvRequest());
  }
}

function* fetchDeployment() {
  const PROJECT_ID = yield select((store: Store) => store.project.selected);
  yield put(setLoading(DEPLOYMENTS.FETCH_REQUEST, true));

  try {
    const res: PaginatedData<Data> = yield call(
      callApi,
      'get',
      `/projects/${PROJECT_ID}/deployment/versions?limit=1000000`
    );

    // Sort descending (TODO: remove when backend implement sort)
    res.data = res.data.reverse();
    yield put(DeploymentActions.fetchDeploymentSuccess(res));
  } catch (err) {
    yield put(CommonAction.failed(DEPLOYMENTS.FETCH_REQUEST, err));
    const errorMessage = get(err, 'response.data.message', 'Unknown error.');
    yield put(
      addNotification({
        title: 'Failed to load deployments',
        message: errorMessage,
        status: 'error',
        dismissible: true,
        dismissAfter: 5000
      })
    );
    yield put(DeploymentActions.deploymentFail('fetch', errorMessage));
    yield put(setLoading(DEPLOYMENTS.FETCH_REQUEST, false));
  } finally {
    yield put(setLoading(DEPLOYMENTS.FETCH_REQUEST, false));
  }
}

function* watchDeploymentFetch() {
  yield takeLatest(DEPLOYMENTS.FETCH_REQUEST, fetchDeployment);
}

function* watchDeploymentAddRequest() {
  yield takeLatest(DEPLOYMENTS.ADD_REQUEST, handleDeploymentAddRequest);
}

function* handleDeploymentAddRequest(action: ReduxAction) {
  const projectId = yield select((store: Store) => store.project.selected);

  const payload: { projectId: string; data: Data } = action.payload;
  const dataWithIds: any = {
    ...payload.data,
    modules: null,
    cmsId: projectId,
    nluId: projectId,
    botId: projectId
  };
  try {
    yield put(setLoading(DEPLOYMENTS.ADD_REQUEST, true));
    const res = yield call(
      callApi,
      'post',
      `/projects/${projectId}/deployment/versions`,
      dataWithIds
    );

    yield put(
      addNotification({
        title: 'Deployment Created',
        message: 'Your new deployment has been created',
        status: 'success',
        dismissible: true,
        dismissAfter: 5000
      })
    );
    yield put(closeDrawer(DRAWER_ID));
    yield put(DeploymentActions.addDeploymentSuccess(res));
    yield put(DeploymentActions.fetchDeploymentRequest());
    yield put(DeploymentActions.fetchLatestDeploymentRequest());
  } catch (err) {
    yield put(
      addNotification({
        title: 'Error',
        message: `There are some errors when creating your deployment - ${err.toString()}`,
        status: 'error',
        dismissible: true,
        dismissAfter: 5000
      })
    );
  } finally {
    yield put(setLoading(DEPLOYMENTS.ADD_REQUEST, false));
  }
}

function* watchFetchLatestDeploymentRequest() {
  yield takeLatest(
    DEPLOYMENTS.FETCH_LATEST_DEPLOYMENT_REQUEST,
    handleFetchLatestDeploymentRequest
  );
}

function* handleFetchLatestDeploymentRequest() {
  const PROJECT_ID = yield select((store: Store) => store.project.selected);
  yield put(setLoading(DEPLOYMENTS.FETCH_LATEST_DEPLOYMENT_REQUEST, true));

  try {
    const res = yield call(
      callApi,
      'get',
      `/projects/${PROJECT_ID}/deployment`
    );
    yield put(DeploymentActions.fetchLatestDeploymentSuccess(res));
  } catch (err) {
    yield put(DeploymentActions.fetchLatestDeploymentFailed(err));
  } finally {
    yield put(setLoading(DEPLOYMENTS.FETCH_LATEST_DEPLOYMENT_REQUEST, false));
  }
}

function* watchRollbackDeploymentRequest() {
  yield takeLatest(
    DEPLOYMENTS.ROLLBACK_DEPLOYMENT_REQUEST,
    handleRollbackDeploymentRequest
  );
}

function* handleRollbackDeploymentRequest(action: ReduxAction) {
  const userId = yield select((store: Store) =>
    store.auth.token ? store.auth.token.detail.username : null
  );
  if (!userId) {
    yield put(
      DeploymentActions.rollbackDeploymentFailed('Author can not be null')
    );
    return;
  }
  const latestDeploymentId = yield select(
    (store: Store) => store.deployment.latestDeployment.id
  );

  const newVersion = action.payload.newVersion;

  yield put(setLoading(DEPLOYMENTS.ROLLBACK_DEPLOYMENT_REQUEST, true));

  try {
    const res = yield call(
      callApi,
      'post',
      `/deployments/${latestDeploymentId}/rollback`,
      {
        author: userId,
        changelog: action.payload.changelog,
        version: newVersion
      }
    );
    yield put(
      addNotification({
        title: 'Rollback Successful',
        message: `Successfully rolled back to version ${newVersion}`,
        status: 'success',
        dismissible: true,
        dismissAfter: 5000
      })
    );
    yield put(closeModal(rollbackFormId));
    yield put(DeploymentActions.rollbackDeploymentSuccess(res));
    yield put(DeploymentActions.fetchDeploymentRequest());
    yield put(DeploymentActions.fetchLatestDeploymentRequest());
  } catch (err) {
    yield put(DeploymentActions.rollbackDeploymentFailed(err));
    yield put(
      addNotification({
        title: 'Rollback Failed',
        message: `Error when trying to rollback to version ${newVersion}`,
        status: 'error',
        dismissible: true,
        dismissAfter: 5000
      })
    );
  } finally {
    yield put(setLoading(DEPLOYMENTS.ROLLBACK_DEPLOYMENT_REQUEST, false));
  }
}

function* deploymentFlow() {
  yield all([
    fork(watchProjectSelected),
    fork(watchDeploymentFetch),
    fork(watchDeploymentAddRequest),
    fork(watchFetchLatestDeploymentRequest),
    fork(watchRollbackDeploymentRequest),
    fork(Environment),
    fork(Channel)
  ]);
}

export default deploymentFlow;
