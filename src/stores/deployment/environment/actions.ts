import ENVIRONMENT from './types';
import { PaginatedData } from 'interfaces/common';
import * as Environment from 'interfaces/deployment/environment';

export function clearEnvError(action: string) {
  return {
    action,
    type: ENVIRONMENT.ERROR_CLEAR
  };
}

function requestEnv<T>(action: string, payload?: T) {
  return {
    action,
    payload,
    type: ENVIRONMENT[`${action.toUpperCase()}_REQUEST`]
  };
}

export function envFail(action: string, error: string) {
  return {
    action,
    type: ENVIRONMENT[`${action.toUpperCase()}_FAIL`],
    payload: {
      error
    }
  };
}

export function selectEnv(envId: string | null) {
  return {
    type: ENVIRONMENT.SELECT,
    payload: envId
  };
}

export function fetchEnvRequest() {
  return requestEnv('fetch');
}

export function fetchEnvSuccess(raw: PaginatedData<Environment.Data>) {
  const { data: envData } = raw;
  const data = {};

  const index = (envData as Environment.Data[]).map(
    (item: Environment.Data) => {
      data[item.id] = {
        ...item
      };
      return item.id;
    }
  );

  return {
    type: ENVIRONMENT.FETCH_SUCCESS,
    action: 'fetch',
    payload: {
      index,
      data
    }
  };
}

export function addEnvRequest(projectId: string, data: Environment.Data) {
  return requestEnv('add', { projectId, data });
}

export function addEnvSuccess(data: Environment.Data) {
  return {
    type: ENVIRONMENT.ADD_SUCCESS,
    action: 'add',
    payload: {
      [data.id]: data
    }
  };
}

export function updateEnvRequest(
  projectId: string,
  environmentId: string,
  data: Environment.Data
) {
  return requestEnv('update', { projectId, environmentId, data });
}

export function updateEnvSuccess(data: Environment.Data) {
  return {
    type: ENVIRONMENT.UPDATE_SUCCESS,
    action: 'update',
    payload: {
      [data.id]: data
    }
  };
}
