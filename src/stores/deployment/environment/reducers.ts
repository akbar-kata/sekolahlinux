import { combineReducers } from 'redux';

import * as Environment from 'interfaces/deployment/environment';
import { ReduxAction } from 'interfaces';

import ENVIRONMENT from './types';

const initialState: Environment.Store = {
  selected: null,
  loadings: {
    fetch: false,
    add: false,
    update: false,
    remove: false
  },
  errors: {
    fetch: null,
    add: null,
    update: null,
    remove: null
  },
  data: {},
  index: []
};

const loadings = (
  state: Environment.Loadings = initialState.loadings,
  { type, action, payload }: ReduxAction
) => {
  const actionCaps = action ? action.toUpperCase() : 'FETCH';
  switch (type) {
    case ENVIRONMENT[`${actionCaps}_REQUEST`]:
      return Object.assign({}, state, {
        [action as string]: true
      });
    case ENVIRONMENT[`${actionCaps}_SUCCESS`]:
    case ENVIRONMENT[`${actionCaps}_FAIL`]:
      return Object.assign({}, state, {
        [action as string]: false
      });
    default:
      return state;
  }
};

const errors = (
  state: Environment.Errors = initialState.errors,
  { type, action, payload }: ReduxAction
) => {
  const actionCaps = action ? action.toUpperCase() : 'FETCH';
  switch (type) {
    case ENVIRONMENT.ERROR_CLEAR:
    case ENVIRONMENT[`${actionCaps}_REQUEST`]:
      return Object.assign({}, state, {
        [action as string]: null
      });
    case ENVIRONMENT[`${actionCaps}_FAIL`]:
      return Object.assign({}, state, {
        [action as string]: payload.error
      });
    default:
      return state;
  }
};

const selected = (
  state: string | null = initialState.selected,
  { type, payload }: ReduxAction
) => {
  switch (type) {
    case ENVIRONMENT.SELECT:
      return payload;
    default:
      return state;
  }
};

const data = (
  state: Environment.DataMap = initialState.data,
  { type, payload }: ReduxAction
) => {
  switch (type) {
    case ENVIRONMENT.FETCH_SUCCESS:
      return Object.assign({}, payload.data);
    case ENVIRONMENT.ADD_SUCCESS:
    case ENVIRONMENT.UPDATE_SUCCESS:
      return Object.assign({}, state, payload);
    case ENVIRONMENT.REMOVE_SUCCESS:
      const { [payload.name]: deletedItem, ...rest } = state;
      return rest;
    default:
      return state;
  }
};

const index = (
  state: string[] = initialState.index,
  { type, payload }: ReduxAction
) => {
  switch (type) {
    case ENVIRONMENT.FETCH_SUCCESS:
      return [...payload.index];
    case ENVIRONMENT.ADD_SUCCESS:
      return [...state, ...Object.keys(payload)];
    case ENVIRONMENT.REMOVE_SUCCESS:
      return state.filter(name => name !== payload.name);
    default:
      return state;
  }
};

export default combineReducers({
  loadings,
  errors,
  selected,
  data,
  index
});
