import * as Environment from 'interfaces/deployment/environment';

export function getEnvironmentData(
  state: Environment.Store
): Environment.DataMap {
  return state.data;
}

export function getEnvironmentSelected(
  state: Environment.Store
): string | null {
  return state.selected;
}

export function getEnvironmentIndex(state: Environment.Store): string[] {
  return state.index;
}

export function getEnvironmentLoading(state: Environment.Store, type: string) {
  return state.loadings[type];
}

export function getEnvironmentError(
  state: Environment.Store,
  type: string
): string | null {
  return state.errors[type];
}

export function getEnvironmentDetail(
  state: Environment.Store,
  deploymentId: string
): Environment.Data {
  return state.data[deploymentId];
}
