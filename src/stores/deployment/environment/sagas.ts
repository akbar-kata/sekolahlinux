import { put, takeLatest, fork, all, call, select } from 'redux-saga/effects';
import get from 'lodash-es/get';

import { ReduxAction } from 'interfaces';

import {
  fetchEnvSuccess,
  envFail,
  addEnvSuccess,
  updateEnvSuccess
} from './actions';
import ENVIRONMENT from './types';
import { setLoading } from 'stores/app/loadings/action';
import { callApi } from 'stores/services';
import Store from 'interfaces/rootStore';
import { addNotification } from 'stores/app/notification/actions';
import { closeDrawer } from 'stores/app/drawer/actions';
import { fetchProjectsRequest } from 'stores/project/actions';

function* addEnvironment({ payload }: ReduxAction) {
  try {
    yield put(setLoading(ENVIRONMENT.ADD_REQUEST, true));

    const result = yield call(
      callApi,
      'post',
      `/projects/${payload.projectId}/environments`,
      payload.data
    );

    yield put(
      addNotification({
        title: `Success`,
        message: `<strong>${
          result.name
        }</strong> environment successfully created.`,
        status: 'success',
        allowHTML: true,
        dismissible: true,
        dismissAfter: 5000
      })
    );
    yield put(addEnvSuccess(result));
    yield put(closeDrawer('EnvironmentCreate'));
    yield put(fetchProjectsRequest());
  } catch (err) {
    const errorMessage = get(
      err,
      'response.data.message',
      'Failed to create environment.'
    );

    const res = errorMessage.match(/environments_slug_unique/g);

    if (res.length > 0) {
      yield put(
        addNotification({
          title: `Failed`,
          message: `the url <strong>${
            payload.data.slug
          } </strong> is already in use, please use another url `,
          status: 'error',
          allowHTML: true,
          dismissible: true,
          dismissAfter: 5000
        })
      );
    } else {
      yield put(
        addNotification({
          title: `Failed`,
          message: errorMessage,
          status: 'error',
          allowHTML: true,
          dismissible: true,
          dismissAfter: 5000
        })
      );
      yield put(closeDrawer('EnvironmentCreate'));
    }
    yield put(envFail('add', errorMessage));
    yield put(setLoading(ENVIRONMENT.ADD_REQUEST, false));
  } finally {
    yield put(setLoading(ENVIRONMENT.ADD_REQUEST, false));
  }
}

function* updateEnvironment({ payload }: ReduxAction) {
  try {
    yield put(setLoading(ENVIRONMENT.UPDATE_REQUEST, true));

    const result = yield call(
      callApi,
      'put',
      `/projects/${payload.projectId}/environments/${payload.environmentId}`,
      payload.data
    );

    yield put(
      addNotification({
        title: `Success`,
        message: `<strong>${
          result.name
        }</strong> environment successfully updated.`,
        status: 'success',
        allowHTML: true,
        dismissible: true,
        dismissAfter: 5000
      })
    );
    yield put(updateEnvSuccess(result));
  } catch (err) {
    const errorMessage = get(
      err,
      'response.data.message',
      'Failed to update environment.'
    );

    yield put(
      addNotification({
        title: `Failed`,
        message: errorMessage,
        status: 'error',
        allowHTML: true,
        dismissible: true,
        dismissAfter: 5000
      })
    );
    yield put(envFail('update', errorMessage));
    yield put(setLoading(ENVIRONMENT.UPDATE_REQUEST, false));
    yield put(closeDrawer('EnvironmentUpdate'));
  } finally {
    yield put(setLoading(ENVIRONMENT.UPDATE_REQUEST, false));
    yield put(closeDrawer('EnvironmentUpdate'));
  }
}

function* fetchEnvironment({ payload }: ReduxAction) {
  try {
    const PROJECT_ID = yield select((store: Store) => store.project.selected);
    yield put(setLoading(ENVIRONMENT.FETCH_REQUEST, true));

    const result = yield call(
      callApi,
      'get',
      `/projects/${PROJECT_ID}/environments`
    );

    yield put(fetchEnvSuccess(result));
  } catch (err) {
    const errorMessage = get(err, 'response.data.message', 'Unknown error.');
    yield put(
      addNotification({
        title: 'Failed to load environments.',
        message: errorMessage,
        status: 'error',
        dismissible: true,
        dismissAfter: 5000
      })
    );

    yield put(envFail('fetch', errorMessage));
  }
}

function* watchEnvironmentAdd() {
  yield takeLatest(ENVIRONMENT.ADD_REQUEST, addEnvironment);
}

function* watchEnvironmentUpdate() {
  yield takeLatest(ENVIRONMENT.UPDATE_REQUEST, updateEnvironment);
}

function* watchEnvironmentFetch() {
  yield takeLatest(ENVIRONMENT.FETCH_REQUEST, fetchEnvironment);
}

function* environmentFlow() {
  yield all([
    fork(watchEnvironmentFetch),
    fork(watchEnvironmentAdd),
    fork(watchEnvironmentUpdate)
  ]);
}

export default environmentFlow;
