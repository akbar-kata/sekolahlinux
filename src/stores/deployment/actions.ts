import * as Deployment from 'interfaces/deployment';
import DEPLOYMENTS from './types';
import { PaginatedData } from 'interfaces/common';

export function clearDeploymentError(action: string) {
  return {
    action,
    type: DEPLOYMENTS.ERROR_CLEAR
  };
}

function requestDeployment<T>(action: string, payload?: T) {
  return {
    action,
    payload,
    type: DEPLOYMENTS[`${action.toUpperCase()}_REQUEST`]
  };
}

export function deploymentFail(action: string, error: string) {
  return {
    action,
    type: DEPLOYMENTS[`${action.toUpperCase()}_FAIL`],
    payload: {
      error
    }
  };
}

export function selectDeployment(toSelect: string | null) {
  return {
    type: DEPLOYMENTS.SELECT,
    payload: toSelect
  };
}

export function fetchDeploymentRequest() {
  return requestDeployment('fetch');
}

export function fetchDeploymentSuccess(raw: PaginatedData<Deployment.Data>) {
  const { data: depData } = raw;
  const data = {};

  const index = (depData as Deployment.Data[]).map((item: Deployment.Data) => {
    data[item.name] = {
      ...item
    };

    return item.name;
  });

  return {
    type: DEPLOYMENTS.FETCH_SUCCESS,
    action: 'fetch',
    payload: {
      index,
      data
    }
  };
}

export function addDeploymentRequest(projectId: string, data: Deployment.Data) {
  return requestDeployment<{ projectId: string; data: Deployment.Data }>(
    'add',
    {
      projectId,
      data
    }
  );
}

export function addDeploymentSuccess(data: Deployment.Data) {
  return {
    type: DEPLOYMENTS.ADD_SUCCESS,
    action: 'add',
    payload: {
      [data.name]: data
    }
  };
}

export function updateDeploymentRequest(
  name: string,
  botId: string,
  data: Deployment.Data
) {
  return requestDeployment<{
    name: string;
    botId: string;
    data: Deployment.Data;
  }>('update', { name, botId, data });
}

export function updateDeploymentSuccess(name: string, data: Deployment.Data) {
  return {
    type: DEPLOYMENTS.UPDATE_SUCCESS,
    action: 'update',
    payload: {
      [name]: data
    }
  };
}

export function removeDeploymentRequest(name: string, botId: string) {
  return requestDeployment<{
    name: string;
    botId: string;
  }>('remove', { name, botId });
}

export function removeDeploymentSuccess(name: string) {
  return {
    type: DEPLOYMENTS.REMOVE_SUCCESS,
    action: 'remove',
    payload: { name }
  };
}

export function fetchLatestDeploymentRequest() {
  return {
    type: DEPLOYMENTS.FETCH_LATEST_DEPLOYMENT_REQUEST
  };
}
export function fetchLatestDeploymentSuccess(
  latestDeployment: Deployment.Data
) {
  return {
    type: DEPLOYMENTS.FETCH_LATEST_DEPLOYMENT_SUCCESS,
    payload: latestDeployment
  };
}

export function fetchLatestDeploymentFailed(error: any) {
  return {
    type: DEPLOYMENTS.FETCH_LATEST_DEPLOYMENT_FAILED,
    payload: error
  };
}

export function rollbackDeploymentRequest(payload: {
  changelog: string;
  newVersion: string;
}) {
  return {
    payload,
    type: DEPLOYMENTS.ROLLBACK_DEPLOYMENT_REQUEST
  };
}

export function rollbackDeploymentFailed(error: any) {
  return {
    type: DEPLOYMENTS.ROLLBACK_DEPLOYMENT_FAILED,
    payload: error
  };
}

export function rollbackDeploymentSuccess(data: any) {
  return {
    type: DEPLOYMENTS.ROLLBACK_DEPLOYMENT_SUCCESS,
    payload: data
  };
}
