import CHECK from './types';
import { callApiThunk } from 'stores/services';
import { JsonArray } from 'interfaces';

export function clearCheck() {
  return {
    type: CHECK.CLEAR,
    action: 'clear',
    payload: { data: null }
  };
}

// function requestCheck<T>(action: string, payload?: T) {
//   return {
//     type: CHECK[`${action.toUpperCase()}_REQUEST`],
//     action,
//     payload
//   };
// }

export function searchUserRequest(keyword: string) {
  return function(dispatch: Function) {
    return dispatch(
      callApiThunk(
        'get',
        `/users/search?username=${keyword ? keyword : ' '}&limit=20`
      )
    ).then(response => {
      dispatch(checkUserSuccess(response));

      return {
        options: response.map(item => ({
          label: item.username,
          value: item.userId
        }))
      };
    });
  };
}

export function checkUserFail(action: string, error: string) {
  return {
    action,
    type: CHECK[`${action.toUpperCase()}_FAIL`],
    payload: {
      error: true,
      message: error
    }
  };
}

export function checkUserSuccess(data: JsonArray) {
  return {
    type: CHECK.FETCH_SUCCESS,
    action: 'fetch',
    payload: data
  };
}
