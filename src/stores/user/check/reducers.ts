import { combineReducers } from 'redux';

import { ReduxAction } from 'interfaces';
import * as Check from 'interfaces/check';
import CHECK from './types';

const loadings = (
  state: Check.Loadings = { fetch: false },
  { type, payload }: ReduxAction
) => {
  switch (type) {
    case CHECK.FETCH_REQUEST:
      return { ...state, fetch: true };
    case CHECK.FETCH_SUCCESS:
    case CHECK.FETCH_FAIL:
      return { ...state, fetch: false };
    default:
      return state;
  }
};

const data = (state: Check.Data[] = [], { type, payload }: ReduxAction) => {
  switch (type) {
    case CHECK.FETCH_SUCCESS:
      return payload;
    case CHECK.CLEAR:
      return Object.assign({}, state, {});
    default:
      return state;
  }
};

const errors = (
  state: Check.Errors = { fetch: null },
  { type, payload }: ReduxAction
) => {
  switch (type) {
    case CHECK.FETCH_FAIL:
      return { ...state, fetch: payload };
    case CHECK.FETCH_SUCCESS:
      return { ...state, fetch: null };
    default:
      return state;
  }
};

export default combineReducers<Check.Store>({
  loadings,
  data,
  errors
});
