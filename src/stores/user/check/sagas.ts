import { all, call, fork, put, takeLatest } from 'redux-saga/effects';
import get from 'lodash-es/get';

import { checkUserSuccess, checkUserFail } from './actions';
import { ReduxAction } from 'interfaces';
import CHECK from './types';
import { callApi } from 'stores/services';

function* searchUser({ payload }: ReduxAction) {
  try {
    const token = yield call(
      callApi,
      'get',
      `/users/search?username=${
        payload.keyword ? payload.keyword : ''
      }&limit=20`
    );
    yield put(checkUserSuccess(token));
  } catch (error) {
    const errorMessage = get(
      error,
      'response.data.message',
      'Oops.. something wrong'
    );
    yield put(checkUserFail('fetch', errorMessage));
  }
}

function* watchCheckUser() {
  yield takeLatest(CHECK.FETCH_REQUEST, searchUser);
}

function* checkFlow() {
  yield all([fork(watchCheckUser)]);
}

export default checkFlow;
