import { JsonArray } from 'interfaces';
import * as Member from 'interfaces/member';
import MEMBER from './types';

function requestMember<T>(action: string, payload?: T) {
  return {
    action,
    payload,
    type: MEMBER[`${action.toUpperCase()}_REQUEST`]
  };
}

export function memberFail(action: string, error: string) {
  return {
    action,
    type: MEMBER[`${action.toUpperCase()}_FAIL`],
    payload: {
      error
    }
  };
}

export function selectMember(toSelect: string) {
  return {
    type: MEMBER.SELECT,
    payload: toSelect
  };
}

export function fetchMemberRequest(teamId: string) {
  return requestMember<{ teamId: string }>('fetch', { teamId });
}

export function fetchMemberSuccess(raw: JsonArray) {
  const data = {};
  const index = raw.map((item: any) => {
    data[item.userId] = item;
    return item.userId;
  });
  return {
    type: MEMBER.FETCH_SUCCESS,
    action: 'fetch',
    payload: { index, data }
  };
}

export function addMemberRequest(data: any) {
  return requestMember<{ data: any }>('add', { data });
}

export function addMemberSuccess(data: Member.Data) {
  return {
    type: MEMBER.ADD_SUCCESS,
    action: 'add',
    payload: {
      [data.userId]: data
    }
  };
}

export function updateMemberRequest(id: string, data: Member.Data) {
  return requestMember<{ id: string; data: Member.Data }>('update', {
    id,
    data
  });
}

export function updateMemberSuccess(id: string, data: Member.Data) {
  return {
    type: MEMBER.UPDATE_SUCCESS,
    action: 'update',
    payload: {
      [id]: data
    }
  };
}

export function removeMemberRequest(data: any) {
  return requestMember<{ data: any }>('remove', { data });
}

export function removeMemberSuccess(id: string) {
  return {
    type: MEMBER.REMOVE_SUCCESS,
    action: 'remove',
    payload: { id }
  };
}
