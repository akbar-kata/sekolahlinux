import * as Member from 'interfaces/member';

export function getMemberLoading(state: Member.Store, type: string): boolean {
  return state.loadings[type];
}

export function getMemberError(
  state: Member.Store,
  type: string
): string | null {
  return state.errors[type];
}

export function getMemberSelected(state: Member.Store): string | null {
  return state.selected;
}

export function getMemberLastUpdated(state: Member.Store): number {
  return state.lastUpdate;
}

export function getMemberIndex(state: Member.Store): string[] {
  return state.index;
}

export function getMemberData(state: Member.Store): Member.DataMap {
  return state.data;
}

export function getMemberDetail(
  state: Member.Store,
  memberId: string
): Member.Data {
  return state.data[memberId];
}
