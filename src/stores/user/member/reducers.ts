import { combineReducers } from 'redux';

import { ReduxAction } from 'interfaces';
import * as Member from 'interfaces/member';
import MEMBER from './types';
import { noopReducer } from 'utils/redux';

const initialState: Member.Store = {
  selected: null,
  loadings: {
    fetch: false,
    add: false,
    update: false,
    remove: false
  },
  errors: {
    fetch: null,
    add: null,
    update: null,
    remove: null
  },
  data: {},
  index: [],
  lastUpdate: Date.now()
};

const selected = noopReducer(initialState.selected);

const loadings = (
  state: Member.Store['loadings'] = initialState.loadings,
  { type, action, payload }: ReduxAction
) => {
  const actionCaps = action ? action.toUpperCase() : 'FETCH';
  switch (type) {
    case MEMBER[`${actionCaps}_REQUEST`]:
      return Object.assign({}, state, {
        [action as string]: true
      });
    case MEMBER[`${actionCaps}_SUCCESS`]:
    case MEMBER[`${actionCaps}_FAIL`]:
      return Object.assign({}, state, {
        [action as string]: false
      });
    default:
      return state;
  }
};

const errors = (
  state: Member.Store['errors'] = initialState.errors,
  { type, action, payload }: ReduxAction
) => {
  const actionCaps = action ? action.toUpperCase() : 'FETCH';
  switch (type) {
    case MEMBER.ERROR_CLEAR:
    case MEMBER[`${actionCaps}_REQUEST`]:
      return Object.assign({}, state, {
        [action as string]: null
      });
    case MEMBER[`${actionCaps}_FAIL`]:
      return Object.assign({}, state, {
        [action as string]: payload.error
      });
    default:
      return state;
  }
};

const data = (
  state: Member.Store['data'] = initialState.data,
  { type, payload }: ReduxAction
) => {
  switch (type) {
    case MEMBER.FETCH_SUCCESS:
      return Object.assign({}, payload.data);
    case MEMBER.ADD_SUCCESS:
    case MEMBER.UPDATE_SUCCESS:
      return Object.assign({}, state, payload);
    case MEMBER.REMOVE_SUCCESS:
      const { [payload.id]: deletedItem, ...rest } = state;
      return rest;
    default:
      return state;
  }
};

const index = (
  state: Member.Store['index'] = initialState.index,
  { type, payload }: ReduxAction
) => {
  switch (type) {
    case MEMBER.FETCH_SUCCESS:
      return [...payload.index];
    case MEMBER.ADD_SUCCESS:
      return [...state, ...Object.keys(payload)];
    case MEMBER.REMOVE_SUCCESS:
      return state.filter(id => id !== payload.id);
    default:
      return state;
  }
};

const lastUpdate = noopReducer(initialState.lastUpdate);

export default combineReducers<Member.Store>({
  selected,
  index,
  data,
  loadings,
  errors,
  lastUpdate
});
