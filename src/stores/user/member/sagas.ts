import { all, call, fork, put, takeLatest } from 'redux-saga/effects';
import get from 'lodash-es/get';

import {
  memberFail,
  fetchMemberSuccess,
  updateMemberSuccess,
  removeMemberSuccess,
  selectMember,
  addMemberSuccess,
  fetchMemberRequest
} from './actions';
import { closeModal } from 'stores/app/modal/actions';
import { callApi } from 'stores/services';
import { ReduxAction } from 'interfaces';
import MEMBER from './types';
import { clearCheck } from 'stores/user/check/actions';
import { addNotification } from 'stores/app/notification/actions';
import { closeDrawer } from 'stores/app/drawer/actions';

function* fetchMember({ payload: { teamId } }: ReduxAction) {
  try {
    const data = yield call(callApi, 'get', `/teams/${teamId}/users`);
    yield put(fetchMemberSuccess(data));
    if (data.length > 0) {
      yield put(selectMember(data[0].userId));
    }
  } catch (err) {
    const errorMessage = get(
      err,
      'response.data.message',
      'Oops.. something wrong'
    );
    yield put(memberFail('fetch', errorMessage));
  }
}

function* addMember({ payload: { data } }: ReduxAction) {
  try {
    const resp = yield call(
      callApi,
      'post',
      `/teams/${data.teamId}/users/${data.userId}`,
      {
        roleId: 'teamMember'
      }
    );
    yield put(addMemberSuccess(resp));
    yield put(fetchMemberRequest(data.teamname));
    yield put(closeDrawer('MemberCreate'));
    yield put(clearCheck());
    yield put(
      addNotification({
        title: 'Success',
        message: 'Member added successfully',
        status: 'success',
        dismissible: true,
        dismissAfter: 5000
      })
    );
  } catch (err) {
    const errorMessage = get(
      err,
      'response.data.message',
      'Oops.. something wrong'
    );
    yield put(
      addNotification({
        title: 'Failed',
        message: errorMessage,
        status: 'error',
        dismissible: true,
        dismissAfter: 5000
      })
    );
    yield put(memberFail('add', errorMessage));
  }
}

function* updateMember({ payload: { id, data } }: ReduxAction) {
  try {
    const resp = yield call(callApi, 'put', `/members/${id}`, {
      membername: data.membername,
      email: data.email
    });
    yield put(updateMemberSuccess(id, resp));
    yield put(closeModal('MemberUpdate'));
    yield put(
      addNotification({
        title: 'Success',
        message: 'Member updated successfully',
        status: 'success',
        dismissible: true,
        dismissAfter: 5000
      })
    );
  } catch (err) {
    const errorMessage = get(
      err,
      'response.data.message',
      'Oops.. something wrong'
    );
    yield put(
      addNotification({
        title: 'Failed',
        message: errorMessage,
        status: 'error',
        dismissible: true,
        dismissAfter: 5000
      })
    );
    yield put(memberFail('update', errorMessage));
  }
}

function* removeMember({ payload: { data } }: ReduxAction) {
  try {
    yield call(callApi, 'delete', `/teams/${data.teamId}/users/${data.userId}`);
    yield put(fetchMemberRequest(data.teamname));
    yield put(removeMemberSuccess(data.username));
    yield put(
      addNotification({
        title: 'Success',
        message: 'Member removed successfully',
        status: 'success',
        dismissible: true,
        dismissAfter: 5000
      })
    );
  } catch (err) {
    const errorMessage = get(
      err,
      'response.data.message',
      'Oops.. something wrong'
    );
    yield put(
      addNotification({
        title: 'Failed',
        message: errorMessage,
        status: 'error',
        dismissible: true,
        dismissAfter: 5000
      })
    );
    yield put(memberFail('remove', errorMessage));
  }
}

function* watchMemberFetch() {
  yield takeLatest(MEMBER.FETCH_REQUEST, fetchMember);
}

function* watchMemberAdd() {
  yield takeLatest(MEMBER.ADD_REQUEST, addMember);
}

function* watchMemberUpdate() {
  yield takeLatest(MEMBER.UPDATE_REQUEST, updateMember);
}

function* watchMemberRemove() {
  yield takeLatest(MEMBER.REMOVE_REQUEST, removeMember);
}

function* memberFlow() {
  yield all([
    fork(watchMemberFetch),
    fork(watchMemberAdd),
    fork(watchMemberUpdate),
    fork(watchMemberRemove)
  ]);
}

export default memberFlow;
