import { takeLatest, fork, all, call, put } from 'redux-saga/effects';
import get from 'lodash-es/get';

import ACCOUNT from './types';
import { callApi } from 'stores/services';
import {
  requestUpdateEmailFailed,
  requestUpdateEmailSuccess,
  setIsLoading,
  clearBags,
  requestUpdatePasswordSuccess,
  requestUpdatePasswordFailed
} from './actions';
import { ReduxAction } from 'interfaces';
import { addNotification } from 'stores/app/notification/actions';
import { setEmail } from 'stores/user/auth/actions';
import { setIsOtp } from '../auth/actions';

function* requestUpdateEmail({ payload }: ReduxAction) {
  yield put(clearBags());
  try {
    yield put(setIsLoading(true));
    yield call(callApi, 'put', `/users/${payload.credentials.id}`, {
      ...payload.credentials,
      email: payload.email
    });

    yield put(setEmail(payload.email));
    yield put(requestUpdateEmailSuccess(payload.email));
    yield put(
      addNotification({
        title: 'Email Updated',
        message: 'Your email has been updated.',
        status: 'success',
        dismissible: true,
        dismissAfter: 5000
      })
    );
    yield put(setIsLoading(false));
  } catch (err) {
    const errorMessage = get(
      err,
      'response.data.message',
      'Oops.. something wrong'
    );

    yield put(requestUpdateEmailFailed(errorMessage));
    yield put(
      addNotification({
        title: 'Failed to Update Email',
        message:
          'Oops... something went wrong! You failed to update your email.',
        status: 'error',
        dismissible: true,
        dismissAfter: 5000
      })
    );
  }
}

function* watchRequestUpdateEmail() {
  yield takeLatest(ACCOUNT.UPDATE_EMAIL_REQUEST, requestUpdateEmail);
}

function* requestUpdatePassword({ payload }: ReduxAction) {
  yield put(clearBags());
  try {
    yield put(setIsLoading(true));

    const res = yield call(
      callApi,
      'put',
      `/users/${payload.credentials.username}/password`,
      {
        ...payload.credentials,
        oldPassword: payload.oldPassword,
        newPassword: payload.newPassword
      }
    );
    if (res) {
      yield put(requestUpdatePasswordSuccess());
      yield put(setIsOtp());
      yield put(
        addNotification({
          title: 'Password Updated',
          message: 'Your password has been updated.',
          status: 'success',
          dismissible: true,
          dismissAfter: 5000
        })
      );
    } else {
      yield put(
        addNotification({
          title: 'Failed to Update Password',
          message:
            'Oops... something went wrong! You failed to update your password.',
          status: 'error',
          dismissible: true,
          dismissAfter: 5000
        })
      );
    }

    yield put(setIsLoading(false));
  } catch (err) {
    const errorMessage = get(
      err,
      'response.data.message',
      'Oops.. something wrong'
    );
    yield put(setIsLoading(false));
    yield put(
      addNotification({
        title: 'Failed to Update Password',
        message:
          'Oops... something went wrong! You failed to update your password.',
        status: 'error',
        dismissible: true,
        dismissAfter: 5000
      })
    );
    if (err.response.status === 403) {
      yield put(requestUpdatePasswordFailed('Current password does not match'));
    } else {
      yield put(requestUpdatePasswordFailed(errorMessage));
    }
  }
}

function* watchRequestUpdatePassword() {
  yield takeLatest(ACCOUNT.UPDATE_PASSWORD_REQUEST, requestUpdatePassword);
}

export default function* account() {
  yield all([fork(watchRequestUpdateEmail), fork(watchRequestUpdatePassword)]);
}
