import ACCOUNT from './types';
import { Store } from 'interfaces/account';

const rootDefault: Store = {
  isLoading: false,
  errors: undefined,
  messages: undefined
};

export default function accountReducers(
  state: Store = rootDefault,
  { type, payload }: any
) {
  switch (type) {
    case ACCOUNT.SET_IS_LOADING:
      return { ...state, isLoading: payload };
    case ACCOUNT.UPDATE_EMAIL_FAIL:
      return { ...state, errors: { ...state.errors, email: payload } };
    case ACCOUNT.UPDATE_PASSWORD_FAIL:
      return { ...state, errors: { ...state.errors, password: payload } };
    case ACCOUNT.UPDATE_EMAIL_SUCCESS:
      return {
        ...state,
        messages: {
          ...state.messages,
          email: 'Email updated succesfully'
        }
      };
    case ACCOUNT.UPDATE_PASSWORD_SUCCESS:
      return {
        ...state,
        messages: {
          ...state.messages,
          password: 'Password set successfully'
        }
      };
    case ACCOUNT.CLEAR_BAGS:
      return {
        ...state,
        errors: {},
        messages: {}
      };
    default:
      return state;
  }
}
