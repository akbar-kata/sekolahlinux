import ACCOUNT from './types';
import { Data as User } from 'interfaces/user';
export function setIsLoading(value: boolean) {
  return {
    type: ACCOUNT.SET_IS_LOADING,
    payload: value
  };
}

export function requestUpdateEmail(email: string, credentials: User) {
  return {
    type: ACCOUNT.UPDATE_EMAIL_REQUEST,
    payload: {
      email,
      credentials
    }
  };
}

export function requestUpdateEmailFailed(error: string) {
  return {
    type: ACCOUNT.UPDATE_EMAIL_FAIL,
    payload: error
  };
}

export function requestUpdateEmailSuccess(newEmail: string) {
  return {
    type: ACCOUNT.UPDATE_EMAIL_SUCCESS,
    payload: newEmail
  };
}

export function requestUpdatePassword(
  oldPassword: string,
  newPassword: string,
  credentials: User
) {
  return {
    type: ACCOUNT.UPDATE_PASSWORD_REQUEST,
    payload: {
      oldPassword,
      newPassword,
      credentials
    }
  };
}

export function requestUpdatePasswordFailed(error: string) {
  return {
    type: ACCOUNT.UPDATE_PASSWORD_FAIL,
    payload: error
  };
}

export function requestUpdatePasswordSuccess() {
  return {
    type: ACCOUNT.UPDATE_PASSWORD_SUCCESS
  };
}

export function clearBags() {
  return {
    type: ACCOUNT.CLEAR_BAGS
  };
}
