import {
  all,
  call,
  put,
  race,
  select,
  take,
  takeLatest
} from 'redux-saga/effects';
import get from 'lodash-es/get';
import { push } from 'connected-react-router';
import { addNotification } from 'stores/app/notification/actions';

import {
  authenticateToken,
  authorize,
  callApi,
  clearToken,
  getFromLocalStorage,
  getToken,
  setToken
} from './api';
import { callApi as callApiWithToken } from 'stores/services';
import { authFail, authSuccess, clearAuth, switchUserSuccess } from './actions';
import { fetchBotRequest } from 'stores/bot/actions';
import { getAuthToken, getIsAuthenticated } from './selectors';
import RootStore from 'interfaces/rootStore';
import * as Auth from 'interfaces/auth';
import { fetchProjectsRequest } from 'stores/project/actions';
import AUTH from './types';
import * as Cookies from 'js-cookie';
import { CK_LAST_PATH } from 'stores/app/sagas';

interface UserTeam {
  roleId: string;
  teamId: string;
  username: string;
}

/**
 *  API login request/response handler
 *  @param   {string}     username
 *  @param   {string}     password
 *  @param   {boolean}    remember
 *  @return  {Generator}
 */
function* login(username: string, password: string, remember: boolean = false) {
  try {
    const tokens: Auth.Token = yield call(authorize, username, password);
    const detail = yield call(
      callApi,
      tokens.id,
      'get',
      `/users/${tokens.userId}`
    );

    const userTeams =
      detail.teams && detail.teams.length
        ? // remove bad team data
          detail.teams.filter(
            (team: UserTeam) => team.roleId !== null || team.username !== null
          )
        : [];
    const teams = yield all(
      userTeams.map(team => {
        return call(
          callApi,
          tokens.id,
          'get',
          `/teams/${team.teamId}?namespaceId=platform`
        );
      })
    );
    tokens.detail = detail;
    tokens.teams = teams;

    if (remember) {
      // expiration token should be set only when `remember` is `true`.
      // the backend sends back an expiry date regardless if we check
      // that box or not.
      setToken(tokens, { expires: new Date((tokens.expire || 0) * 1000) });
    } else {
      setToken(tokens);
    }
    yield put(authSuccess(tokens));
    yield call(authenticate, tokens.id);
  } catch (error) {
    const errorAuthStatus = get(
      error,
      'data.status',
      'Oops... something went wrong.'
    );
    const errMsg =
      (error.response && error.response.status) === 403
        ? 'Invalid username or password'
        : errorAuthStatus;
    yield put(authFail(errMsg));
  }
}

/**
 * validate stored token in cookies
 * @param {boolean}    initial
 * @return  {Generator}
 */
export function* validate(initial?: boolean) {
  const tokens = getToken();
  if (tokens) {
    if (initial) {
      yield put(authSuccess(tokens));

      const cachedUser = getFromLocalStorage('team_selected');
      if (cachedUser) {
        yield put(
          switchUserSuccess(
            cachedUser.id,
            cachedUser.token,
            cachedUser.username,
            cachedUser.type
          )
        );
      }
    }
    yield call(authenticate, tokens.id);
  }
}

/**
 *  API authentication request/response handler. Used to validate the access token
 *  and/or get the user object. If an `invalid_token` error is returned, tries to
 *  refresh the access token before throwing.
 *  @param   {string}     tokenId
 *  @return  {Generator}
 */
function* authenticate(tokenId: string) {
  try {
    yield call(authenticateToken, tokenId);
  } catch (error) {
    yield call(logout);
  }
}

/**
 * create user token
 * @param  {string} id  userId
 */
function* createTeamToken({ payload }: any) {
  const { id, type, username } = payload;

  try {
    let token: any = null;

    if (type === 'user') {
      const userToken = yield select((state: RootStore) =>
        getAuthToken(state.auth)
      );

      token = {
        id,
        username,
        token: userToken.id,
        userType: 'user'
      };
    } else {
      const teamToken = yield call(callApiWithToken, 'post', '/tokens', {
        type: 'team',
        teamId: id
      });

      token = {
        id,
        username,
        token: teamToken ? teamToken.id : '',
        userType: 'team'
      };
    }

    if (token) {
      localStorage.setItem('team_selected', JSON.stringify(token));
      yield put(switchUserSuccess(id, token.token, username, token.userType));
      yield put(fetchBotRequest({ withSelect: false }));
      yield put(push('/project'));
      yield put(fetchProjectsRequest({ withSelect: false }));
      yield put(
        addNotification({
          title: `Switched to ${type}`,
          message: `You have successfully switched to <span class="text-primary font-weight-bold">${username}</span>`,
          status: 'success',
          dismissible: true,
          allowHTML: true,
          dismissAfter: 5000
        })
      );
    }
  } catch (error) {
    const errorMessage = get(
      error,
      'response.data.message',
      'Oops... something went wrong.'
    );
    yield put(authFail(errorMessage));
  }
}

/**
 *  User logout, deletes all tokens from local storage and update the store.
 *  @return  {Generator}
 */
function* logout() {
  clearToken();
  Cookies.set(CK_LAST_PATH, '/');
  sessionStorage.clear();
  localStorage.clear();
  yield put(clearAuth());
  window.location.assign('/');
}

/**
 *  The saga flow for authentication. Starts with either a direct login (with
 *  login/password) or a validation from the token stored in the local storage.
 *  Once the authentication is valid, listens for calls to refresh the access token
 *  until the user logs out.
 *  @return  {Generator}
 */
function* authFlowSaga() {
  const hasUser = yield select((state: RootStore) =>
    getIsAuthenticated(state.auth)
  );

  while (!hasUser) {
    yield call(loggedOutFlowSaga);
  }

  if (hasUser) {
    yield takeLatest(AUTH.SWITCH_USER_REQUEST, createTeamToken);
    yield takeLatest(AUTH.CLEAR_REQUEST, logout);
  }
}

/**
 *  Authentication starts either with classic login or with tokens fetched from
 *  localStorage
 *  @return  {Generator}
 */
function* loggedOutFlowSaga() {
  const { credentials, validation } = yield race({
    credentials: take(AUTH.REQUEST),
    validation: take(AUTH.VALIDATION)
  });
  if (credentials) {
    const { username, password, remember } = credentials.payload;
    yield call(login, username, password, remember);
  }
  if (validation) {
    yield call(validate, validation.payload.initial);
  }

  yield call(authFlowSaga);
}

export default authFlowSaga;
