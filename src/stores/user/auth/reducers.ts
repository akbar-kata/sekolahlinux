import { combineReducers } from 'redux';

import { ReduxAction } from 'interfaces';
import * as Auth from 'interfaces/auth';
import AUTH from './types';
import { setToken } from 'stores/user/auth/api';

const initialState: Auth.Store = {
  isLoading: false,
  token: null,
  selected: null,
  error: false
};

const isLoading = (
  state: boolean = initialState.isLoading,
  { type, payload }: ReduxAction
) => {
  switch (type) {
    case AUTH.REQUEST:
      return true;
    case AUTH.SUCCESS:
    case AUTH.FAIL:
      return false;
    default:
      return state;
  }
};

const token = (
  state: null | Auth.Token = initialState.token,
  { type, payload }: ReduxAction
) => {
  switch (type) {
    case AUTH.SUCCESS:
      return Object.assign({}, state, payload);
    case AUTH.CLEAR:
      return null;
    case AUTH.SET_EMAIL:
      if (state) {
        const copy = {
          ...state,
          detail: { ...state.detail, email: payload },
          teams: state.detail.teams
        };
        setToken(copy);
        return copy;
      }
      return state;
    case AUTH.SET_ISOTP:
      const nestedState = state;
      if (nestedState) {
        nestedState.isOtp = false;
      }

      return {
        ...state,
        nestedState
      };
    default:
      return state;
  }
};

const selected = (
  state: Auth.Selected | null = initialState.selected,
  { type, payload }: ReduxAction
) => {
  switch (type) {
    case AUTH.SUCCESS:
      return Object.assign(
        {},
        {
          id: payload.userId,
          token: payload.id,
          username:
            payload.detail && payload.detail.username
              ? payload.detail.username
              : '',
          type: payload.type
        }
      );
    case AUTH.SWITCH_USER_SUCCESS:
      return Object.assign({}, payload);
    default:
      return state;
  }
};

const error = (
  state: false | string = initialState.error,
  { type, payload }: ReduxAction
) => {
  switch (type) {
    case AUTH.SWITCH_USER_FAIL:
    case AUTH.FAIL:
      return payload;
    case AUTH.SUCCESS:
    case AUTH.CLEAR:
      return false;
    default:
      return state;
  }
};

export default combineReducers({
  isLoading,
  token,
  selected,
  error
});
