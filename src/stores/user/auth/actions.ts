import * as Auth from 'interfaces/auth';
import AUTH from './types';

export function requestAuth(
  username: string,
  password: string,
  remember: boolean = false
) {
  return {
    type: AUTH.REQUEST,
    payload: {
      username,
      password,
      remember
    }
  };
}

export function authSuccess(token: Auth.Token) {
  return {
    type: AUTH.SUCCESS,
    payload: token
  };
}

export function authFail(message: string) {
  return {
    type: AUTH.FAIL,
    payload: message
  };
}

export function reqSwitchUser(
  id: string,
  type: 'user' | 'team',
  username: string
) {
  return {
    type: AUTH.SWITCH_USER_REQUEST,
    payload: {
      id,
      type,
      username
    }
  };
}

export function switchUserSuccess(
  id: string,
  token: string,
  username: string,
  type: string = 'user'
) {
  return {
    type: AUTH.SWITCH_USER_SUCCESS,
    payload: {
      id,
      token,
      username,
      type
    }
  };
}

export function switchUserFail(message: string) {
  return {
    type: AUTH.SWITCH_USER_FAIL,
    payload: message
  };
}

export function validateAuth(initial?: boolean) {
  return {
    type: AUTH.VALIDATION,
    payload: { initial }
  };
}

export function clearAuthRequest() {
  return {
    type: AUTH.CLEAR_REQUEST
  };
}

export function clearAuth() {
  return {
    type: AUTH.CLEAR
  };
}

export function setEmail(email: string) {
  return {
    type: AUTH.SET_EMAIL,
    payload: email
  };
}

export function setIsOtp() {
  return {
    type: AUTH.SET_ISOTP
  };
}
