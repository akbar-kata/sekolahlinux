import axios from 'axios';
import Cookies from 'js-cookie';
import get from 'lodash-es/get';
import omit from 'lodash-es/omit';

import { JsonObject } from 'interfaces';
import { API_URL } from 'stores/services';
import * as Auth from 'interfaces/auth';

export function authorize(username: string, password: string) {
  return axios({
    method: 'post',
    baseURL: API_URL,
    url: '/login',
    data: {
      username,
      password
    },
    headers: { 'Content-Type': 'application/json; charset=utf-8' }
  })
    .then(resp => resp.data)
    .then(data => {
      return data;
    })
    .catch(err => {
      throw err.response;
    });
}

export function authenticateToken(tokenId: string) {
  return axios({
    method: 'get',
    baseURL: API_URL,
    url: `/tokens/${tokenId}`,
    headers: {
      'Content-Type': 'application/json; charset=utf-8',
      Authorization: `Bearer ${tokenId}`
    }
  })
    .then(resp => resp.data)
    .then(data => {
      return data;
    });
}

export function callApi(
  token: string,
  type: string,
  url: string,
  data?: JsonObject
) {
  return axios({
    url,
    data,
    method: type,
    baseURL: API_URL,
    headers: {
      'Content-Type': 'application/json; charset=utf-8',
      Authorization: `Bearer ${token}`
    }
  }).then(resp => resp.data);
}

export function setToken(
  token: Auth.Token,
  options?: Cookies.CookieAttributes
) {
  try {
    const rest = omit(token, 'teams');
    const newToken = JSON.stringify(rest);
    const tokens: any = {
      id: token.id,
      token: token.id,
      userType: 'user',
      username: rest.detail.username
    };

    const currentTeams = get(token, 'teams', []);

    Cookies.set('siteid', newToken, options);
    localStorage.setItem('teams', JSON.stringify(currentTeams));
    localStorage.setItem('team_selected', JSON.stringify(tokens));
  } catch (err) {
    // tslint:disable-next-line
    console.error(err);
  }
}

export function getToken(): Auth.Token | undefined {
  try {
    const tokens = Cookies.get('siteid');
    return JSON.parse(tokens!);
  } catch (err) {
    return undefined;
  }
}

export function clearToken() {
  // store.remove('siteid');
  Cookies.set('siteid', '');
  localStorage.setItem('teams', '');
  localStorage.setItem('team_selected', '');
  // Cookies.expire('siteid');
  Cookies.remove('siteid');
}

export function getFromLocalStorage(itemName: string) {
  try {
    const data = localStorage.getItem(itemName);

    return JSON.parse(data!);
  } catch (error) {
    return undefined;
  }
}
