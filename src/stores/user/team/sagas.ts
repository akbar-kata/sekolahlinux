import { all, call, fork, put, takeLatest, select } from 'redux-saga/effects';
import get from 'lodash-es/get';

import {
  teamFail,
  fetchTeamSuccess,
  addTeamSuccess,
  updateTeamSuccess,
  removeTeamSuccess,
  selectTeam,
  fetchTeamRequest
} from './actions';
import { getFromLocalStorage } from '../auth/api';
import { closeModal } from 'stores/app/modal/actions';
import { callApi } from 'stores/services';
import { ReduxAction } from 'interfaces';
import TEAM from './types';
import { addNotification } from 'stores/app/notification/actions';
import { closeDrawer } from 'stores/app/drawer/actions';
import RootStore from 'interfaces/rootStore';

function* fetchTeam({ payload: { userId } }: ReduxAction) {
  try {
    const data = yield call(callApi, 'get', `/users/${userId}/teams`);
    yield put(fetchTeamSuccess(data));
    if (data.length > 0) {
      yield put(selectTeam(data[0].teamId));
    }
  } catch (err) {
    const errorMessage = get(
      err,
      'response.data.message',
      'Oops.. something wrong'
    );
    yield put(teamFail('fetch', errorMessage));
  }
}

function* addTeam({ payload: { data } }: ReduxAction) {
  try {
    // const userId = yield select((store: RootStore) =>
    //   store.auth.token ? store.auth.token.detail.id : null
    // );
    const team = yield call(callApi, 'post', '/teams', {
      username: data.username,
      roleId: 'teamAdmin',
      password: ''
    });
    // yield call(callApi, 'post', `/teams/${team.id}/users/${userId}`, {
    //   roleId: 'teamAdmin'
    // });
    yield put(addTeamSuccess(team));
    yield put(
      addNotification({
        title: `${data.username} Team Created`,
        message: `You have successfully created ${data.username} team. `,
        status: 'success',
        dismissible: true,
        dismissAfter: 5000
      })
    );
    yield put(closeDrawer('TeamCreate'));
    const username = yield select((store: RootStore) =>
      store.auth.token ? store.auth.token.detail.username : null
    );
    if (username) {
      yield put(fetchTeamRequest(username));
    }
    const teamsStorage: any = getFromLocalStorage('teams');
    teamsStorage.push(team);
    localStorage.setItem('teams', JSON.stringify(teamsStorage));
  } catch (err) {
    const errorMessage = get(
      err,
      'response.data.message',
      'Oops.. something wrong'
    );
    let displayMessage = '';
    if (errorMessage.indexOf('Duplicate entry') !== -1) {
      displayMessage =
        'Ooops... that Team name is already taken. Please use different name.';
    } else {
      displayMessage = `Ooops... Something went wrong! You failed to create ${
        data.username
      } team.`;
    }
    yield put(
      addNotification({
        title: 'Failed to Create Team',
        message: displayMessage,
        status: 'error',
        dismissible: true,
        dismissAfter: 5000
      })
    );
    yield put(teamFail('add', errorMessage));
  }
}

function* updateTeam({ payload: { id, data } }: ReduxAction) {
  try {
    const resp = yield call(callApi, 'put', `/teams/${id}`, {
      teamname: data.teamname,
      email: data.email
    });
    yield put(updateTeamSuccess(id, resp));
    yield put(
      addNotification({
        title: `${data.teamname} Team Updated`,
        message: `You have successfully updated ${data.teamname} team.`,
        status: 'success',
        dismissible: true,
        dismissAfter: 5000
      })
    );
    yield put(closeModal('TeamUpdate'));
  } catch (err) {
    const errorMessage = get(
      err,
      'response.data.message',
      'Oops.. something wrong'
    );
    yield put(
      addNotification({
        title: 'Failed to Update Team',
        message: `Ooops... Something went wrong! You failed to update ${name} team.`,
        status: 'error',
        dismissible: true,
        dismissAfter: 5000
      })
    );
    yield put(teamFail('update', errorMessage));
  }
}

function* removeTeam({ payload: { id, name } }: ReduxAction) {
  try {
    yield call(callApi, 'delete', `/teams/${id}`);
    yield put(
      addNotification({
        title: `${name} Team Deleted`,
        message: `${name} team has been deleted.`,
        status: 'success',
        dismissible: true,
        dismissAfter: 5000
      })
    );
    yield put(removeTeamSuccess(id));
  } catch (err) {
    const errorMessage = get(
      err,
      'response.data.message',
      'Oops.. something wrong'
    );
    yield put(
      addNotification({
        title: 'Failed to Delete Team',
        message: `Ooops... Something went wrong! You failed to delete ${name} team.`,
        status: 'error',
        dismissible: true,
        dismissAfter: 5000
      })
    );
    yield put(teamFail('remove', errorMessage));
  }
}

function* watchTeamFetch() {
  yield takeLatest(TEAM.FETCH_REQUEST, fetchTeam);
}

function* watchTeamAdd() {
  yield takeLatest(TEAM.ADD_REQUEST, addTeam);
}

function* watchTeamUpdate() {
  yield takeLatest(TEAM.UPDATE_REQUEST, updateTeam);
}

function* watchTeamRemove() {
  yield takeLatest(TEAM.REMOVE_REQUEST, removeTeam);
}

function* teamFlow() {
  yield all([
    fork(watchTeamFetch),
    fork(watchTeamAdd),
    fork(watchTeamUpdate),
    fork(watchTeamRemove)
  ]);
}

export default teamFlow;
