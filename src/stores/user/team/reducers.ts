import { combineReducers, Reducer } from 'redux';

import { ReduxAction } from 'interfaces';
import { Store as TeamStore } from 'interfaces/team';
import TEAM from './types';

const loadingInit = {
  fetch: false,
  add: false,
  update: false,
  remove: false
};

const loadings: Reducer<TeamStore['loadings'], ReduxAction> = (
  state = loadingInit,
  { type, action, payload }
) => {
  const actionCaps = action ? action.toUpperCase() : 'FETCH';
  switch (type) {
    case TEAM[`${actionCaps}_REQUEST`]:
      return Object.assign({}, state, {
        [action as string]: true
      });
    case TEAM[`${actionCaps}_SUCCESS`]:
    case TEAM[`${actionCaps}_FAIL`]:
      return Object.assign({}, state, {
        [action as string]: false
      });
    default:
      return state;
  }
};

const errorsInit = {
  fetch: null,
  add: null,
  update: null,
  remove: null
};

const errors: Reducer<TeamStore['errors'], ReduxAction> = (
  state = errorsInit,
  { type, action, payload }
) => {
  const actionCaps = action ? action.toUpperCase() : 'FETCH';
  switch (type) {
    case TEAM.ERROR_CLEAR:
    case TEAM[`${actionCaps}_REQUEST`]:
      return Object.assign({}, state, {
        [action as string]: null
      });
    case TEAM[`${actionCaps}_FAIL`]:
      return Object.assign({}, state, {
        [action as string]: payload.error
      });
    default:
      return state;
  }
};

const selected: Reducer<TeamStore['selected'], ReduxAction> = (
  state = null,
  { type, payload }
) => {
  switch (type) {
    case TEAM.SELECT:
      return payload;
    default:
      return state;
  }
};

const data: Reducer<TeamStore['data'], ReduxAction> = (
  state = {},
  { type, payload }
) => {
  switch (type) {
    case TEAM.FETCH_SUCCESS:
      return Object.assign({}, payload.data);
    case TEAM.ADD_SUCCESS:
    case TEAM.UPDATE_SUCCESS:
      return Object.assign({}, state, payload);
    case TEAM.REMOVE_SUCCESS:
      const { [payload.id]: deletedItem, ...rest } = state;
      return rest;
    default:
      return state;
  }
};

const index: Reducer<TeamStore['index'], ReduxAction> = (
  state = [],
  { type, payload }
) => {
  switch (type) {
    case TEAM.FETCH_SUCCESS:
      return [...payload.index];
    case TEAM.ADD_SUCCESS:
      return [...state, ...Object.keys(payload)];
    case TEAM.REMOVE_SUCCESS:
      return state.filter(id => id !== payload.id);
    default:
      return state;
  }
};

const lastUpdate: Reducer<TeamStore['lastUpdate'], ReduxAction> = (
  state = Date.now(),
  { type, payload }
) => {
  switch (type) {
    default:
      return state;
  }
};

export default combineReducers<TeamStore>({
  selected,
  index,
  data,
  loadings,
  errors,
  lastUpdate
});
