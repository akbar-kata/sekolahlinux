import * as Team from 'interfaces/team';

export function getTeamLoading(state: Team.Store, type: string): boolean {
  return state.loadings[type];
}

export function getTeamError(state: Team.Store, type: string): string | null {
  return state.errors[type];
}

export function getTeamSelected(state: Team.Store): string | null {
  return state.selected;
}

export function getTeamLastUpdated(state: Team.Store): number {
  return state.lastUpdate;
}

export function getTeamIndex(state: Team.Store): string[] {
  return state.index;
}

export function getTeamData(state: Team.Store): Team.DataMap {
  return state.data;
}

export function getTeamDetail(state: Team.Store, teamId: string): Team.Data {
  return state.data[teamId];
}
