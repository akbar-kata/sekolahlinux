import * as Team from 'interfaces/team';
import TEAM from './types';
import { JsonArray } from 'interfaces';

export function clearTeamError(action: string) {
  return {
    action,
    type: TEAM.ERROR_CLEAR
  };
}

function requestTeam<T>(action: string, payload?: T) {
  return {
    action,
    payload,
    type: TEAM[`${action.toUpperCase()}_REQUEST`]
  };
}

export function teamFail(action: string, error: string) {
  return {
    action,
    type: TEAM[`${action.toUpperCase()}_FAIL`],
    payload: {
      error
    }
  };
}

export function selectTeam(toSelect: string) {
  return {
    type: TEAM.SELECT,
    payload: toSelect
  };
}

export function fetchTeamRequest(userId: string) {
  return requestTeam<{ userId: string }>('fetch', { userId });
}

export function fetchTeamSuccess(raw: JsonArray) {
  const data = {};
  const index = raw.map((item: any) => {
    data[item.teamId] = item;
    return item.teamId;
  });
  return {
    type: TEAM.FETCH_SUCCESS,
    action: 'fetch',
    payload: { index, data }
  };
}

export function addTeamRequest(data: Team.Data) {
  return requestTeam<{ data: Team.Data }>('add', { data });
}

export function addTeamSuccess(data: Team.Data) {
  return {
    type: TEAM.ADD_SUCCESS,
    action: 'add',
    payload: {
      data
    }
  };
}

export function updateTeamRequest(id: string, data: Team.Data) {
  return requestTeam<{ id: string; data: Team.Data }>('update', { id, data });
}

export function updateTeamSuccess(id: string, data: Team.Data) {
  return {
    type: TEAM.UPDATE_SUCCESS,
    action: 'update',
    payload: {
      [id]: data
    }
  };
}

export function removeTeamRequest(id: string, name: string) {
  return requestTeam<{ id: string; name: string }>('remove', { id, name });
}

export function removeTeamSuccess(id: string) {
  return {
    type: TEAM.REMOVE_SUCCESS,
    action: 'remove',
    payload: { id }
  };
}
