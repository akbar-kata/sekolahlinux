import PROJECT_TYPES from './types';
import { Project, Template } from 'interfaces/project';
import { PaginatedData } from 'interfaces/common';

export function selectProject(toSelect: string | null) {
  return {
    type: PROJECT_TYPES.SELECT_PROJECT,
    payload: toSelect
  };
}

export function fetchProjectsRequest(args?: { withSelect: boolean }) {
  const payload: { withSelect?: boolean } = { ...args };

  if (args === undefined || (args as any).withSelect === undefined) {
    payload.withSelect = true;
  }

  return {
    payload,
    type: PROJECT_TYPES.FETCH_PROJECT_REQUEST
  };
}
export function fetchProjectSuccess(projects: PaginatedData<Project>) {
  return {
    type: PROJECT_TYPES.FETCH_PROJECT_SUCCESS,
    payload: projects
  };
}

export function fetchTemplatesRequest() {
  return {
    type: PROJECT_TYPES.FETCH_TEMPLATE_REQUEST
  };
}
export function fetchTemplatesSuccess(templates: Template[]) {
  return {
    type: PROJECT_TYPES.FETCH_TEMPLATE_SUCCESS,
    payload: templates
  };
}

export function createProjectRequest(project: Partial<Project>) {
  return {
    type: PROJECT_TYPES.CREATE_PROJECT_REQUEST,
    payload: project
  };
}
export function createProjectSuccess(project: Project) {
  return {
    type: PROJECT_TYPES.CREATE_PROJECT_SUCCESS,
    payload: project
  };
}

export function deleteProjectRequest(projectId: string) {
  return {
    type: PROJECT_TYPES.DELETE_PROJECT_REQUEST,
    payload: projectId
  };
}
export function deleteProjectSuccess(projectId: string) {
  return {
    type: PROJECT_TYPES.DELETE_PROJECT_SUCCESS,
    payload: projectId
  };
}

export function updateProjectRequest(project: Project) {
  return {
    type: PROJECT_TYPES.UPDATE_PROJECT_REQUEST,
    payload: project
  };
}
export function updateProjectSuccess(project: Project) {
  return {
    type: PROJECT_TYPES.UPDATE_PROJECT_SUCCESS,
    payload: project
  };
}

export function loadMoreProjectRequest() {
  return {
    type: PROJECT_TYPES.LOAD_MORE_PROJECT_REQUEST
  };
}

export function loadMoreProjectSuccess(projects: PaginatedData<Project>) {
  return {
    type: PROJECT_TYPES.LOAD_MORE_PROJECT_SUCCESS,
    payload: projects
  };
}
