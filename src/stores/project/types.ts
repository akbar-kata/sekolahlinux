import { ExReduxAction, VoidExReduxAction } from 'interfaces';
import { PaginatedData } from 'interfaces/common';
import { Project, Template } from 'interfaces/project';

export enum PROJECT_TYPES {
  SELECT_PROJECT = 'PROJECT/SELECT_PROJECT',

  FETCH_PROJECT_REQUEST = 'PROJECT/FETCH_PROJECT_REQUEST',
  FETCH_PROJECT_SUCCESS = 'PROJECT/FETCH_PROJECT_SUCCESS',
  FETCH_PROJECT_FAILED = 'PROJECT/FETCH_PROJECT_FAILED',

  CREATE_PROJECT_REQUEST = 'PROJECT/CREATE_PROJECT_REQUEST',
  CREATE_PROJECT_SUCCESS = 'PROJECT/CREATE_PROJECT_SUCCESS',
  CREATE_PROJECT_FAILED = 'PROJECT/CREATE_PROJECT_FAILED',

  UPDATE_PROJECT_REQUEST = 'PROJECT/UPDATE_PROJECT_REQUEST',
  UPDATE_PROJECT_SUCCESS = 'PROJECT/UPDATE_PROJECT_SUCCESS',
  UPDATE_PROJECT_FAILED = 'PROJECT/UPDATE_PROJECT_FAILED',

  DELETE_PROJECT_REQUEST = 'PROJECT/DELETE_PROJECT_REQUEST',
  DELETE_PROJECT_SUCCESS = 'PROJECT/DELETE_PROJECT_SUCCESS',
  DELETE_PROJECT_FAILED = 'PROJECT/DELETE_PROJECT_FAILED',

  FETCH_TEMPLATE_REQUEST = 'PROJECT/FETCH_TEMPLATE_REQUEST',
  FETCH_TEMPLATE_SUCCESS = 'PROJECT/FETCH_TEMPLATE_SUCCESS',
  FETCH_TEMPLATE_FAILED = 'PROJECT/FETCH_TEMPLATE_FAILED',

  LOAD_MORE_PROJECT_REQUEST = 'PROJECT/LOAD_MORE_PROJECT_REQUEST',
  LOAD_MORE_PROJECT_SUCCESS = 'PROJECT/LOAD_MORE_PROJECT_SUCCESS',
  LOAD_MORE_PROJECT_FAILED = 'PROJECT/LOAD_MORE_PROJECT_FAILED'
}

export type SelectProjectAction = ExReduxAction<
  PROJECT_TYPES.SELECT_PROJECT,
  string
>;

export type FetchProjectRequestAction = ExReduxAction<
  PROJECT_TYPES.FETCH_PROJECT_REQUEST,
  {
    withSelect?: boolean;
  }
>;
export type FetchProjectSuccessAction = ExReduxAction<
  PROJECT_TYPES.FETCH_PROJECT_SUCCESS,
  PaginatedData<Project>
>;

export type CreateProjectRequestAction = ExReduxAction<
  PROJECT_TYPES.CREATE_PROJECT_REQUEST,
  Partial<Project>
>;
export type CreateProjectSuccessAction = ExReduxAction<
  PROJECT_TYPES.CREATE_PROJECT_SUCCESS,
  Project
>;

export type UpdateProjectRequestAction = ExReduxAction<
  PROJECT_TYPES.UPDATE_PROJECT_REQUEST,
  Project
>;
export type UpdateProjectSuccessAction = ExReduxAction<
  PROJECT_TYPES.UPDATE_PROJECT_SUCCESS,
  Project
>;

export type DeleteProjectRequestAction = ExReduxAction<
  PROJECT_TYPES.DELETE_PROJECT_REQUEST,
  string
>;
export type DeleteProjectSuccessAction = ExReduxAction<
  PROJECT_TYPES.DELETE_PROJECT_SUCCESS,
  string
>;

export type LoadMoreProjectRequestAction = VoidExReduxAction<
  PROJECT_TYPES.LOAD_MORE_PROJECT_REQUEST
>;
export type LoadMoreProjectSuccessAction = ExReduxAction<
  PROJECT_TYPES.LOAD_MORE_PROJECT_SUCCESS,
  PaginatedData<Project>
>;

export type FetchProjectTemplateRequestAction = VoidExReduxAction<
  PROJECT_TYPES.FETCH_TEMPLATE_REQUEST
>;
export type FetchProjectTemplateSuccessAction = ExReduxAction<
  PROJECT_TYPES.FETCH_TEMPLATE_SUCCESS,
  Template[]
>;

export default PROJECT_TYPES;
