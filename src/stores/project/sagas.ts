import { all, takeLatest, put, fork, call, select } from 'redux-saga/effects';
import { delay } from 'redux-saga';
import { push } from 'connected-react-router';
import { get } from 'lodash-es';

import { DRAWER_ID as EDIT_FORM_DRAWER_ID } from 'modules/project/EditProjectForm';
import { DRAWER_ID as NEW_FORM_DRAWER_ID } from 'modules/project/NewProjectForm';

import { setLoading } from 'stores/app/loadings/action';
import { closeDrawer } from 'stores/app/drawer/actions';
import { addNotification } from 'stores/app/notification/actions';
import { callApi } from 'stores/services';
import { getSelectedProjectDetail } from 'stores/project/selectors';

import { PaginatedData, Pagination } from 'interfaces/common';
import { Template } from 'interfaces/project';
import Store from 'interfaces/rootStore';

import * as env from 'utils/env';

import PROJECT_TYPES, {
  SelectProjectAction,
  FetchProjectRequestAction,
  CreateProjectRequestAction,
  DeleteProjectRequestAction,
  UpdateProjectRequestAction
} from './types';
import {
  fetchTemplatesSuccess,
  selectProject,
  fetchProjectSuccess,
  createProjectSuccess,
  deleteProjectSuccess,
  updateProjectSuccess,
  loadMoreProjectSuccess
} from './actions';

const API_URL = env.getRuntimeEnv(
  'REACT_APP_RUNTIME_ZAUN_URL',
  env.defaultEnvs['REACT_APP_RUNTIME_ZAUN_URL']
);

function* getSelectedFromUrl(strict: boolean = false) {
  const location = yield select((state: Store) => state.router.location);
  if (location.pathname && location.pathname.match(/\/project/)) {
    const arr = location.pathname.split('/');
    return arr[2] && (!strict || arr[2] !== 'null')
      ? { pathname: location.pathname, selected: arr[2] }
      : false;
  }
  return false;
}

const DEFAULT_PAGE_LIMIT = 10;

const templates: Template[] = [
  {
    id: '1',
    name: 'Template P1'
  },
  {
    id: '2',
    name: 'Template P2'
  },
  {
    id: '3',
    name: 'Template P3'
  }
];

function* fetchProject(id: string) {
  try {
    return yield call(callApi, 'get', `/projects/${id}`, undefined, API_URL);
  } catch (err) {
    return false;
  }
}

function* watchProjectSelected() {
  yield takeLatest(PROJECT_TYPES.SELECT_PROJECT, selectProjectHandler);
}
function* selectProjectHandler({ payload }: SelectProjectAction) {
  try {
    const resp = yield call(getSelectedFromUrl);
    const location = yield select((state: Store) => state.router.location);
    if (location.pathname && location.pathname.match(/\/setting/)) {
      yield put(push(resp.pathname.replace(resp.selected, payload)));
    }

    if (!resp) {
      yield put(push(`/project/${payload}/bot`));
    }

    if (resp && payload !== null) {
      if (resp.pathname.indexOf('cms/pages') > -1) {
        yield put(push(`/project/${payload}/cms/pages`));
      } else {
        yield put(push(resp.pathname.replace(resp.selected, payload)));
      }
    }
  } catch (err) {
    //
  }
}

function* watchFetchProjectsRequest() {
  yield takeLatest(PROJECT_TYPES.FETCH_PROJECT_REQUEST, fetchProjectsHandler);
}
function* fetchProjectsHandler({ payload }: FetchProjectRequestAction) {
  try {
    yield put(setLoading(PROJECT_TYPES.FETCH_PROJECT_REQUEST, true));
    const res: PaginatedData<any> = yield call(
      callApi,
      'get',
      `/projects?limit=${DEFAULT_PAGE_LIMIT}&page=1`,
      undefined,
      API_URL
    );

    let selected = null;
    let newData = [...(res.data || [])];
    if (newData.length > 0) {
      const selectedUrl = yield call(getSelectedFromUrl, true);
      if (selectedUrl) {
        selected = selectedUrl.selected;
        if (
          newData.findIndex(item => item.id === selectedUrl.selected) === -1
        ) {
          const detail = yield call(fetchProject, selectedUrl.selected);
          if (detail) {
            newData = [detail, ...newData];
          } else {
            selected = newData[0].id;
          }
        }
      } else {
        selected = newData[0].id;
      }
    }
    yield put(
      fetchProjectSuccess({
        ...res,
        data: newData
      })
    );
    // we need add a condition here in order to avoid redirect
    // from starter dashboard page to selected nlu entity page
    if (payload.withSelect) {
      yield put(selectProject(selected));
    }
  } catch (err) {
    const errorMessage = get(err, 'response.data.message', 'Unknown error.');

    yield put(
      addNotification({
        title: 'Failed to load projects',
        message: errorMessage,
        status: 'error',
        dismissible: true,
        dismissAfter: 5000
      })
    );
  } finally {
    yield put(setLoading(PROJECT_TYPES.FETCH_PROJECT_REQUEST, false));
  }
}

function* watchFetchTemplatesRequest() {
  yield takeLatest(PROJECT_TYPES.FETCH_TEMPLATE_REQUEST, fetchTemplates);
}
function* fetchTemplates() {
  yield put(setLoading(PROJECT_TYPES.FETCH_TEMPLATE_REQUEST, true));
  // TODO REPLACE WITH CALL TO SERVER
  yield delay(1000);
  yield put(setLoading(PROJECT_TYPES.FETCH_TEMPLATE_REQUEST, false));
  yield put(fetchTemplatesSuccess(templates));
}

function* watchCreateProjectRequest() {
  yield takeLatest(PROJECT_TYPES.CREATE_PROJECT_REQUEST, createProjectHandler);
}
function* createProjectHandler(action: CreateProjectRequestAction) {
  try {
    yield put(setLoading(PROJECT_TYPES.CREATE_PROJECT_REQUEST, true));

    yield put(setLoading(PROJECT_TYPES.LOAD_MORE_PROJECT_REQUEST, true));
    const project = action.payload;
    const res = yield call(callApi, 'post', `/projects`, project, API_URL);

    yield put(createProjectSuccess(res));
    yield put(closeDrawer(NEW_FORM_DRAWER_ID));
    yield put(
      addNotification({
        title: 'Project Created',
        message: `Project <strong>${res.name}</strong> has been created.`,
        status: 'success',
        dismissible: true,
        allowHTML: true,
        dismissAfter: 5000
      })
    );
    yield put(selectProject(res.id));
    yield put(setLoading(PROJECT_TYPES.CREATE_PROJECT_REQUEST, false));
  } catch (err) {
    const errorMessage = get(err, 'response.data.message', 'Unknown error.');

    yield put(
      addNotification({
        title: 'Failed to create project',
        message: errorMessage,
        status: 'error',
        dismissible: true,
        dismissAfter: 5000
      })
    );
    yield put(setLoading(PROJECT_TYPES.CREATE_PROJECT_REQUEST, false));
  }
}

function* watchDeleteProjectRequest() {
  yield takeLatest(PROJECT_TYPES.DELETE_PROJECT_REQUEST, deleteProjectHandler);
}
function* deleteProjectHandler(action: DeleteProjectRequestAction) {
  const selectedProject = yield select((state: Store) =>
    getSelectedProjectDetail(state.project, action.payload)
  );
  yield put(setLoading(PROJECT_TYPES.DELETE_PROJECT_REQUEST, true));

  try {
    yield call(
      callApi,
      'delete',
      `/projects/${action.payload}`,
      undefined,
      API_URL
    );

    yield put(deleteProjectSuccess(action.payload));
    yield put(
      addNotification({
        title: `Project Deleted`,
        message: `Project <strong>${
          selectedProject.name
        }</strong> has been deleted.`,
        status: 'success',
        allowHTML: true,
        dismissible: true,
        dismissAfter: 5000
      })
    );
  } catch (err) {
    const errorMessage = get(err, 'response.data.message', 'Unknown error.');

    yield put(
      addNotification({
        title: 'Failed to delete project',
        message: errorMessage,
        status: 'error',
        dismissible: true,
        dismissAfter: 5000
      })
    );
  } finally {
    yield put(setLoading(PROJECT_TYPES.DELETE_PROJECT_REQUEST, false));
  }
}

function* watchUpdateProjectRequest() {
  yield takeLatest(PROJECT_TYPES.UPDATE_PROJECT_REQUEST, updateProjectHandler);
}
function* updateProjectHandler(action: UpdateProjectRequestAction) {
  yield put(setLoading(PROJECT_TYPES.UPDATE_PROJECT_REQUEST, true));
  try {
    const project = action.payload;
    const res = yield call(
      callApi,
      'put',
      `/projects/${project.id}`,
      project,
      API_URL
    );

    yield put(
      addNotification({
        title: `Project Updated`,
        message: `Project <strong>${project.name}</strong> has been updated.`,
        status: 'success',
        allowHTML: true,
        dismissible: true,
        dismissAfter: 5000
      })
    );

    yield put(setLoading(PROJECT_TYPES.UPDATE_PROJECT_REQUEST, false));
    yield put(updateProjectSuccess(res));
    yield put(closeDrawer(EDIT_FORM_DRAWER_ID));
  } catch (err) {
    const errorMessage = get(err, 'response.data.message', 'Unknown error.');

    yield put(
      addNotification({
        title: 'Failed to update project',
        message: errorMessage,
        status: 'error',
        dismissible: true,
        dismissAfter: 5000
      })
    );

    yield put(setLoading(PROJECT_TYPES.UPDATE_PROJECT_REQUEST, false));
  }
}

function* watchLoadMoreProjectsRequest() {
  yield takeLatest(
    PROJECT_TYPES.LOAD_MORE_PROJECT_REQUEST,
    loadMoreProjectsHandler
  );
}
function* loadMoreProjectsHandler() {
  const existingPagination: Pagination = yield select(
    (store: Store) => store.project.pagination
  );
  const nextPage = existingPagination.page ? existingPagination.page + 1 : 0;

  yield put(setLoading(PROJECT_TYPES.LOAD_MORE_PROJECT_REQUEST, true));
  const res: PaginatedData<any> = yield call(
    callApi,
    'get',
    `/projects?limit=${DEFAULT_PAGE_LIMIT}&page=${nextPage}`,
    undefined,
    API_URL
  );

  yield put(setLoading(PROJECT_TYPES.LOAD_MORE_PROJECT_REQUEST, false));
  yield put(loadMoreProjectSuccess(res));
}

export default function* root() {
  yield all([
    fork(watchProjectSelected),
    fork(watchFetchProjectsRequest),
    fork(watchLoadMoreProjectsRequest),
    fork(watchFetchTemplatesRequest),
    fork(watchCreateProjectRequest),
    fork(watchDeleteProjectRequest),
    fork(watchUpdateProjectRequest)
  ]);
}
