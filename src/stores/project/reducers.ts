import { combineReducers } from 'redux';

import PROJECT_TYPES, {
  CreateProjectSuccessAction,
  FetchProjectSuccessAction,
  FetchProjectTemplateSuccessAction,
  LoadMoreProjectSuccessAction,
  SelectProjectAction,
  UpdateProjectSuccessAction,
  DeleteProjectSuccessAction
} from './types';
import { Project, Template } from 'interfaces/project';
import { DataMap, Pagination } from 'interfaces/common';
import { assertNever } from 'utils/redux';

const selected = (state: string | null = null, action: SelectProjectAction) => {
  switch (action.type) {
    case PROJECT_TYPES.SELECT_PROJECT:
      return action.payload;
    default:
      return state;
  }
};

const projects = (
  state: DataMap<Project> = {},
  action:
    | FetchProjectSuccessAction
    | LoadMoreProjectSuccessAction
    | UpdateProjectSuccessAction
    | CreateProjectSuccessAction
    | DeleteProjectSuccessAction
): DataMap<Project> => {
  switch (action.type) {
    case PROJECT_TYPES.FETCH_PROJECT_SUCCESS:
    case PROJECT_TYPES.LOAD_MORE_PROJECT_SUCCESS:
      const projectMap = {};
      if (action.payload.data instanceof Array) {
        action.payload.data.forEach(
          project => (projectMap[project.id!] = project)
        );
      }
      return { ...state, ...projectMap };
    case PROJECT_TYPES.UPDATE_PROJECT_SUCCESS:
      return { ...state, [action.payload.id!]: action.payload };
    case PROJECT_TYPES.CREATE_PROJECT_SUCCESS:
      return { ...state, [action.payload.id!]: action.payload };
    case PROJECT_TYPES.DELETE_PROJECT_SUCCESS:
      const { [action.payload as string]: deletedItem, ...rest } = state;
      return rest;
    default:
      assertNever(action);
      return state;
  }
};
const projectIndexes = (
  state: string[] = [],
  action:
    | FetchProjectSuccessAction
    | LoadMoreProjectSuccessAction
    | CreateProjectSuccessAction
    | DeleteProjectSuccessAction
): string[] => {
  switch (action.type) {
    case PROJECT_TYPES.FETCH_PROJECT_SUCCESS:
      if (action.payload.data instanceof Array) {
        return action.payload.data.map(project => project.id!);
      }
      return [];
    case PROJECT_TYPES.LOAD_MORE_PROJECT_SUCCESS:
      if (action.payload.data instanceof Array) {
        return [...state, ...action.payload.data.map(project => project.id!)];
      }
      return [...state];
    case PROJECT_TYPES.CREATE_PROJECT_SUCCESS:
      return [action.payload.id!, ...state];
    case PROJECT_TYPES.DELETE_PROJECT_SUCCESS:
      const indexes = state.filter(id => id !== action.payload);
      return indexes;
    default:
      assertNever(action);
      return state;
  }
};

const templates = (
  state: DataMap<Template> = {},
  action: FetchProjectTemplateSuccessAction
): DataMap<Template> => {
  switch (action.type) {
    case PROJECT_TYPES.FETCH_TEMPLATE_SUCCESS:
      const templateMap = {};
      action.payload.forEach(template => (templateMap[template.id] = template));
      return templateMap;
    default:
      return state;
  }
};

const templateIndexes = (
  state: string[] = [],
  action: FetchProjectTemplateSuccessAction
): string[] => {
  switch (action.type) {
    case PROJECT_TYPES.FETCH_TEMPLATE_SUCCESS:
      return action.payload.map(template => template.id);
    default:
      return state;
  }
};

const pagination = (
  state: Pagination = {
    limit: 10,
    page: 0,
    total: 0
  },
  action: FetchProjectSuccessAction | LoadMoreProjectSuccessAction
): Pagination => {
  switch (action.type) {
    case PROJECT_TYPES.FETCH_PROJECT_SUCCESS:
    case PROJECT_TYPES.LOAD_MORE_PROJECT_SUCCESS:
      const { data: deleted, ...paginationOnly } = action.payload;
      return paginationOnly;
    default:
      return state;
  }
};

export default combineReducers({
  selected,
  projects,
  projectIndexes,
  pagination,
  templates,
  templateIndexes
});
