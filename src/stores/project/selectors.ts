import { Store, Project } from 'interfaces/project';

export function getProjectSelected(store: Store) {
  return store.selected;
}
export function getProjects(store: Store) {
  return store.projects;
}
export function getProjectIndexes(store: Store) {
  return store.projectIndexes;
}
export function getTemplates(store: Store) {
  return store.templates;
}
export function getTemplateIndexes(store: Store) {
  return store.templateIndexes;
}
export function getProjectPagination(store: Store) {
  return store.pagination;
}
export function getProjectOptions(store: Store, project: string) {
  return store.projects[project].options;
}
export function projectHasNextPage(store: Store) {
  return (store.projectIndexes.length || 0) < store.pagination.total;
}

export function getSelectedProjectDetail(
  state: Store,
  projectId: string
): Project {
  return state.projects[projectId];
}
