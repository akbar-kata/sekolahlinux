import * as Auth from 'interfaces/auth';

export function getAuthLoading(state: Auth.Store): boolean {
  return state.isLoading;
}

export function getIsAuthenticated(state: Auth.Store): boolean {
  return state.token !== null;
}

export function getAuthToken(state: Auth.Store): null | Auth.Token {
  return state.token;
}

export function getAuthError(state: Auth.Store): false | string {
  return state.error;
}
