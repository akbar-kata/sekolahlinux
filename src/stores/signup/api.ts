import axios from 'axios';
// import store from 'store';

import { JsonObject } from 'interfaces';
import { API_URL } from 'stores/services';
import * as Auth from 'interfaces/auth';

const Cookies = require('cookies-js');

export function authorize(username: string, password: string) {
  return axios({
    method: 'post',
    baseURL: API_URL,
    url: '/login',
    data: {
      username,
      password
    },
    headers: { 'Content-Type': 'application/json; charset=utf-8' }
  })
    .then(resp => resp.data)
    .then(data => {
      return data;
    });
}

export function authenticateToken(tokenId: string) {
  return axios({
    method: 'get',
    baseURL: API_URL,
    url: `/tokens/${tokenId}`,
    headers: {
      'Content-Type': 'application/json; charset=utf-8',
      Authorization: `Bearer ${tokenId}`
    }
  })
    .then(resp => resp.data)
    .then(data => {
      return data;
    });
}

export function callApi(
  token: string,
  type: string,
  url: string,
  data?: JsonObject
) {
  return axios({
    url,
    data,
    method: type,
    baseURL: API_URL,
    headers: {
      'Content-Type': 'application/json; charset=utf-8',
      Authorization: `Bearer ${token}`
    }
  }).then(resp => resp.data);
}

export function setToken(token: Auth.Token) {
  // store.set('siteid', token);
  Cookies.set('siteid', JSON.stringify(token));
}

export function getToken(): Auth.Token | undefined {
  try {
    const tokens = Cookies.get('siteid');
    return JSON.parse(tokens);
  } catch (err) {
    return undefined;
  }
}

export function clearToken() {
  // store.remove('siteid');
  Cookies.set('siteid', '');
  Cookies.expire('siteid');
}
