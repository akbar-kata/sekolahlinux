import * as Auth from 'interfaces/auth';
import SIGNUP from './types';

export function requestAuth(username: string, password: string) {
  return {
    type: SIGNUP.REQUEST,
    payload: {
      username,
      password
    }
  };
}

export function authSuccess(token: Auth.Token) {
  return {
    type: SIGNUP.SUCCESS,
    payload: token
  };
}

export function authFail(message: string) {
  return {
    type: SIGNUP.FAIL,
    payload: message
  };
}

export function validateAuth(initial?: boolean) {
  return {
    type: SIGNUP.VALIDATION,
    payload: { initial }
  };
}

export function clearAuthRequest() {
  return {
    type: SIGNUP.CLEAR_REQUEST
  };
}

export function clearAuth() {
  return {
    type: SIGNUP.CLEAR
  };
}
