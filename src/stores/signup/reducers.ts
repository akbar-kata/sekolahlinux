import { combineReducers } from 'redux';

import { ReduxAction } from 'interfaces';
import * as Signup from 'interfaces/auth';
import SIGNUP from './types';

const isLoading = (state: boolean = false, { type, payload }: ReduxAction) => {
  switch (type) {
    case SIGNUP.REQUEST:
      return true;
    case SIGNUP.SUCCESS:
    case SIGNUP.FAIL:
      return false;
    default:
      return state;
  }
};

const token = (
  state: null | Signup.Token = null,
  { type, payload }: ReduxAction
) => {
  switch (type) {
    case SIGNUP.SUCCESS:
      return Object.assign({}, state, payload);
    case SIGNUP.CLEAR:
      return null;
    default:
      return state;
  }
};

const error = (
  state: false | string = false,
  { type, payload }: ReduxAction
) => {
  switch (type) {
    case SIGNUP.FAIL:
      return payload;
    case SIGNUP.SUCCESS:
    case SIGNUP.CLEAR:
      return false;
    default:
      return state;
  }
};

export default combineReducers({
  isLoading,
  token,
  error
});
