import { call, take, put, select, race, takeLatest } from 'redux-saga/effects';

import {
  authorize,
  authenticateToken,
  setToken,
  getToken,
  clearToken
} from './api';
import { authSuccess, authFail, clearAuth } from './actions';
import { getIsAuthenticated } from './selectors';
import RootStore from 'interfaces/rootStore';
import SIGNUP from './types';

/**
 *  API login request/response handler
 *  @param   {string}     username
 *  @param   {string}     password
 *  @return  {Generator}
 */
function* login(username: string, password: string) {
  try {
    const tokens = yield call(authorize, username, password);
    setToken(tokens);
    yield put(authSuccess(tokens));
    yield call(authenticate, tokens.id);
  } catch (error) {
    const errMsg =
      (error.response && error.response.status) === 403
        ? 'Username or password does not match any records'
        : error.message;
    yield put(authFail(errMsg));
  }
}

/**
 * validate stored token in cookies
 * @param {boolean}    initial
 * @return  {Generator}
 */
export function* validate(initial?: boolean) {
  const tokens = getToken();
  if (tokens) {
    if (initial) {
      yield put(authSuccess(tokens));
    }
    yield call(authenticate, tokens.id);
  }
}

/**
 *  API authentication request/response handler. Used to validate the access token
 *  and/or get the user object. If an `invalid_token` error is returned, tries to
 *  refresh the access token before throwing.
 *  @param   {string}     tokenId
 *  @return  {Generator}
 */
function* authenticate(tokenId: string) {
  try {
    yield call(authenticateToken, tokenId);
  } catch (error) {
    yield call(logout);
  }
}

/**
 *  User logout, deletes all tokens from local storage and update the store.
 *  @return  {Generator}
 */
function* logout() {
  clearToken();
  yield put(clearAuth());
  yield call(authFlowSaga);
}

/**
 *  The saga flow for authentication. Starts with either a direct login (with
 *  login/password) or a validation from the token stored in the local storage.
 *  Once the authentication is valid, listens for calls to refresh the access token
 *  until the user logs out.
 *  @return  {Generator}
 */
function* authFlowSaga() {
  const hasUser = yield select((state: RootStore) =>
    getIsAuthenticated(state.auth)
  );
  while (!hasUser) {
    yield call(loggedOutFlowSaga);
  }

  if (hasUser) {
    yield takeLatest(SIGNUP.CLEAR_REQUEST, logout);
  }
}

/**
 *  Authentication starts either with classic login or with tokens fetched from
 *  localStorage
 *  @return  {Generator}
 */
function* loggedOutFlowSaga() {
  const { credentials, validation } = yield race({
    credentials: take(SIGNUP.REQUEST),
    validation: take(SIGNUP.VALIDATION)
  });
  if (credentials) {
    yield call(
      login,
      credentials.payload.username,
      credentials.payload.password
    );
  }
  if (validation) {
    yield call(validate, validation.payload.initial);
  }

  yield call(authFlowSaga);
}

export default authFlowSaga;
