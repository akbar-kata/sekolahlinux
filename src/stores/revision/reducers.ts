import { combineReducers } from 'redux';
import { RevisionStore } from 'interfaces/revision';

import botReducers from './bot/reducers';
import cmsReducers from './cms/reducers';
import nluReducers from './nlu/reducers';

export default combineReducers<RevisionStore>({
  bot: botReducers,
  cms: cmsReducers,
  nlu: nluReducers
});
