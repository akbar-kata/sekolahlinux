import REVISION, {
  FetchBotRevisionsFailed,
  FetchBotRevisionsRequest,
  FetchBotRevisionsSuccess,
  FetchCmsRevisionsRequest,
  FetchCmsRevisionsSuccess,
  FetchCmsRevisionsFailed,
  FetchNluRevisionsRequest,
  FetchNluRevisionsSuccess,
  FetchNluRevisionsFailed
} from 'stores/revision/types';

export function fetchBotRevisionsRequest(
  projectId: FetchBotRevisionsRequest['payload']
) {
  return {
    type: REVISION.FETCH_BOT_REVISIONS_REQUEST,
    payload: projectId
  };
}

export function fetchBotRevisionsSuccess(
  data: FetchBotRevisionsSuccess['payload']
) {
  return {
    type: REVISION.FETCH_BOT_REVISIONS_SUCCESS,
    payload: data
  };
}

export function fetchBotRevisionsFailed(
  projectId: FetchBotRevisionsFailed['payload']
) {
  return {
    type: REVISION.FETCH_BOT_REVISIONS_FAILED,
    payload: projectId
  };
}

export function fetchCmsRevisionsRequest(
  projectId: FetchCmsRevisionsRequest['payload']
) {
  return {
    type: REVISION.FETCH_CMS_REVISIONS_REQUEST,
    payload: projectId
  };
}

export function fetchCmsRevisionsSuccess(
  data: FetchCmsRevisionsSuccess['payload']
) {
  return {
    type: REVISION.FETCH_CMS_REVISIONS_SUCCESS,
    payload: data
  };
}

export function fetchCmsRevisionsFailed(
  message: FetchCmsRevisionsFailed['payload']
) {
  return {
    type: REVISION.FETCH_CMS_REVISIONS_FAILED,
    payload: message
  };
}

export function fetchNluRevisionsRequest(
  projectId: FetchNluRevisionsRequest['payload']
) {
  return {
    type: REVISION.FETCH_NLU_REVISIONS_REQUEST,
    payload: projectId
  };
}

export function fetchNluRevisionsSuccess(
  data: FetchNluRevisionsSuccess['payload']
) {
  return {
    type: REVISION.FETCH_NLU_REVISIONS_SUCCESS,
    payload: data
  };
}

export function fetchNluRevisionsFailed(
  projectId: FetchNluRevisionsFailed['payload']
) {
  return {
    type: REVISION.FETCH_NLU_REVISIONS_FAILED,
    payload: projectId
  };
}
