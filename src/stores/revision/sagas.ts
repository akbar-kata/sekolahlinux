import { all, fork } from 'redux-saga/effects';

import botSagas from './bot/sagas';
import cmsSagas from './cms/sagas';
import nluSagas from './nlu/sagas';

export default function* root() {
  yield all([fork(botSagas)]);
  yield all([fork(cmsSagas)]);
  yield all([fork(nluSagas)]);
}
