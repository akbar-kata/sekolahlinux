import { ExReduxAction } from 'interfaces';
import { PaginatedData } from 'interfaces/common';
import { BotRevision, CmsRevision, NluRevision } from 'interfaces/revision';

enum REVISION {
  FETCH_BOT_REVISIONS_REQUEST = 'REVISION/FETCH_BOT_REVISIONS_REQUEST',
  FETCH_BOT_REVISIONS_SUCCESS = 'REVISION/FETCH_BOT_REVISIONS_SUCCESS',
  FETCH_BOT_REVISIONS_FAILED = 'REVISION/FETCH_BOT_REVISIONS_FAILED',

  FETCH_NLU_REVISIONS_REQUEST = 'REVISION/FETCH_NLU_REVISIONS_REQUEST',
  FETCH_NLU_REVISIONS_SUCCESS = 'REVISION/FETCH_NLU_REVISIONS_SUCCESS',
  FETCH_NLU_REVISIONS_FAILED = 'REVISION/FETCH_NLU_REVISIONS_FAILED',

  FETCH_CMS_REVISIONS_REQUEST = 'REVISION/FETCH_CMS_REVISIONS_REQUEST',
  FETCH_CMS_REVISIONS_SUCCESS = 'REVISION/FETCH_CMS_REVISIONS_SUCCESS',
  FETCH_CMS_REVISIONS_FAILED = 'REVISION/FETCH_CMS_REVISIONS_FAILED'
}

export type FetchBotRevisionsRequest = ExReduxAction<
  REVISION.FETCH_BOT_REVISIONS_REQUEST,
  string
>;
export type FetchBotRevisionsSuccess = ExReduxAction<
  REVISION.FETCH_BOT_REVISIONS_SUCCESS,
  PaginatedData<BotRevision>
>;
export type FetchBotRevisionsFailed = ExReduxAction<
  REVISION.FETCH_BOT_REVISIONS_FAILED,
  string
>;
export type FetchCmsRevisionsRequest = ExReduxAction<
  REVISION.FETCH_CMS_REVISIONS_REQUEST,
  string
>;
export type FetchCmsRevisionsSuccess = ExReduxAction<
  REVISION.FETCH_CMS_REVISIONS_SUCCESS,
  PaginatedData<CmsRevision>
>;
export type FetchCmsRevisionsFailed = ExReduxAction<
  REVISION.FETCH_CMS_REVISIONS_FAILED,
  string
>;

export type FetchNluRevisionsRequest = ExReduxAction<
  REVISION.FETCH_NLU_REVISIONS_REQUEST,
  string
>;
export type FetchNluRevisionsSuccess = ExReduxAction<
  REVISION.FETCH_NLU_REVISIONS_SUCCESS,
  PaginatedData<NluRevision>
>;
export type FetchNluRevisionsFailed = ExReduxAction<
  REVISION.FETCH_NLU_REVISIONS_FAILED,
  string
>;

export default REVISION;
