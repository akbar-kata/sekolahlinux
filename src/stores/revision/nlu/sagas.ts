import { all, call, fork, put, takeLatest } from 'redux-saga/effects';
import REVISION, { FetchNluRevisionsRequest } from 'stores/revision/types';
import { setLoading } from 'stores/app/loadings/action';
import { callApi } from 'stores/services';
import {
  fetchNluRevisionsFailed,
  fetchNluRevisionsSuccess
} from 'stores/revision/actions';
import NLSTUDIO_NLU from 'stores/nlu/nlu/types';

function* watchFetchNluRevisions() {
  yield takeLatest(
    REVISION.FETCH_NLU_REVISIONS_REQUEST,
    fetchNluRevisionsHandler
  );
}

function* watchNluSnapshotSuccess() {
  yield takeLatest(NLSTUDIO_NLU.SNAPSHOT_SUCCESS, fetchNluRevisionsHandler);
}

function* fetchNluRevisionsHandler(action: FetchNluRevisionsRequest) {
  yield put(setLoading(REVISION.FETCH_NLU_REVISIONS_REQUEST, true));

  try {
    const { data, ...pagination } = yield call(
      callApi,
      'get',
      `/projects/${action.payload}/nlu/revisions`
    );
    yield put(fetchNluRevisionsSuccess({ data, ...pagination }));
  } catch (e) {
    yield put(fetchNluRevisionsFailed(action.payload));
  } finally {
    yield put(setLoading(REVISION.FETCH_NLU_REVISIONS_REQUEST, false));
  }
}

export default function* root() {
  yield all([fork(watchFetchNluRevisions), fork(watchNluSnapshotSuccess)]);
}
