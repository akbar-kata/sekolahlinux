import { RevisionStore } from 'interfaces/revision';

export function getNluRevisionsData(store: RevisionStore) {
  return store.nlu.data;
}
export function getNluRevisionsIndex(store: RevisionStore) {
  return store.nlu.index;
}
export function getNluRevisionsPagination(store: RevisionStore) {
  return store.nlu.pagination;
}
