import { combineReducers } from 'redux';

import REVISION, { FetchNluRevisionsSuccess } from 'stores/revision/types';
import { NluRevisionStore } from 'interfaces/revision';

const initialState: NluRevisionStore = {
  data: {},
  index: [],
  pagination: {
    count: 0,
    limit: 0,
    page: 0,
    total: 0
  }
};

const data = (
  state: NluRevisionStore['data'] = initialState.data,
  action: FetchNluRevisionsSuccess
) => {
  switch (action.type) {
    case REVISION.FETCH_NLU_REVISIONS_SUCCESS:
      return action.payload.data.reduce((prev, cur) => {
        if (typeof cur.revision === 'string') {
          prev[cur.revision] = cur;
        }
        return prev;
      }, {});
    default:
      return state;
  }
};
const index = (
  state: NluRevisionStore['index'] = initialState.index,
  action: FetchNluRevisionsSuccess
) => {
  switch (action.type) {
    case REVISION.FETCH_NLU_REVISIONS_SUCCESS:
      return action.payload.data
        .filter(rev => rev.revision)
        .map(rev => rev.revision);
    default:
      return state;
  }
};

const pagination = (
  state: NluRevisionStore['pagination'] = initialState.pagination,
  action: FetchNluRevisionsSuccess
) => {
  switch (action.type) {
    case REVISION.FETCH_NLU_REVISIONS_SUCCESS:
      const { data: deleted, ...rest } = action.payload;
      return rest;
    default:
      return state;
  }
};

export default combineReducers({
  data,
  index,
  pagination
});
