import { combineReducers } from 'redux';

import REVISION, { FetchBotRevisionsSuccess } from 'stores/revision/types';
import { BotRevisionStore } from 'interfaces/revision/index';

const initialState: BotRevisionStore = {
  data: {},
  index: [],
  pagination: {
    count: 0,
    limit: 0,
    page: 0,
    total: 0
  }
};

const data = (
  state: BotRevisionStore['data'] = {},
  action: FetchBotRevisionsSuccess
) => {
  switch (action.type) {
    case REVISION.FETCH_BOT_REVISIONS_SUCCESS:
      return action.payload.data.reduce((prev, cur) => {
        if (typeof cur.revision === 'string') {
          prev[cur.revision] = cur;
        }
        return prev;
      }, {});
    default:
      return state;
  }
};
const index = (
  state: BotRevisionStore['index'] = [],
  action: FetchBotRevisionsSuccess
) => {
  switch (action.type) {
    case REVISION.FETCH_BOT_REVISIONS_SUCCESS:
      return action.payload.data
        .map(rev => rev.revision)
        .filter(rev => rev != null);
    default:
      return state;
  }
};
const pagination = (
  state: BotRevisionStore['pagination'] = initialState.pagination,
  action: FetchBotRevisionsSuccess
) => {
  switch (action.type) {
    case REVISION.FETCH_BOT_REVISIONS_SUCCESS:
      const { data: deleted, ...rest } = action.payload;
      return rest;
    default:
      return state;
  }
};

export default combineReducers({
  data,
  index,
  pagination
});
