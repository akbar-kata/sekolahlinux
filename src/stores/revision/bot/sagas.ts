import { all, call, fork, put, takeLatest } from 'redux-saga/effects';
import REVISION, { FetchBotRevisionsRequest } from 'stores/revision/types';
import { setLoading } from 'stores/app/loadings/action';
import { callApi } from 'stores/services';
import { fetchBotRevisionsSuccess } from 'stores/revision/actions';

function* watchFetchBotRevisions() {
  yield takeLatest(
    REVISION.FETCH_BOT_REVISIONS_REQUEST,
    fetchBotRevisionsHandler
  );
}

function* fetchBotRevisionsHandler(action: FetchBotRevisionsRequest) {
  yield put(setLoading(REVISION.FETCH_BOT_REVISIONS_REQUEST, true));
  const res = yield call(
    callApi,
    'get',
    `/projects/${action.payload}/bot/revisions`
  );

  yield put(setLoading(REVISION.FETCH_BOT_REVISIONS_REQUEST, false));

  yield put(fetchBotRevisionsSuccess(res));
}

export default function* root() {
  yield all([fork(watchFetchBotRevisions)]);
}
