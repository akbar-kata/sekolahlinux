import { RevisionStore } from 'interfaces/revision';

export function getBotRevisionsData(store: RevisionStore) {
  return store.bot.data;
}
export function getBotRevisionsIndex(store: RevisionStore) {
  return store.bot.index;
}
export function getBotRevisionsPagination(store: RevisionStore) {
  return store.bot.pagination;
}
