import { combineReducers } from 'redux';

import REVISION, { FetchCmsRevisionsSuccess } from 'stores/revision/types';
import { CmsRevisionStore } from 'interfaces/revision/index';

const initialState: CmsRevisionStore = {
  data: {},
  index: [],
  pagination: {
    count: 0,
    limit: 0,
    page: 0,
    total: 0
  }
};

const data = (
  state: CmsRevisionStore['data'] = initialState.data,
  action: FetchCmsRevisionsSuccess
) => {
  switch (action.type) {
    case REVISION.FETCH_CMS_REVISIONS_SUCCESS:
      return action.payload.data.reduce((prev, cur) => {
        if (typeof cur.revision === 'string') {
          prev[cur.revision] = cur;
        }
        return prev;
      }, {});
    default:
      return state;
  }
};
const index = (
  state: CmsRevisionStore['index'] = initialState.index,
  action: FetchCmsRevisionsSuccess
) => {
  switch (action.type) {
    case REVISION.FETCH_CMS_REVISIONS_SUCCESS:
      return action.payload.data.map(rev => rev.revision);
    default:
      return state;
  }
};
const pagination = (
  state: CmsRevisionStore['pagination'] = initialState.pagination,
  action: FetchCmsRevisionsSuccess
) => {
  switch (action.type) {
    case REVISION.FETCH_CMS_REVISIONS_SUCCESS:
      const { data: deleted, ...rest } = action.payload;
      return rest;
    default:
      return state;
  }
};

export default combineReducers({
  data,
  index,
  pagination
});
