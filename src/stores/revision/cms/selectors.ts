import { RevisionStore } from 'interfaces/revision';

export function getCmsRevisionsData(store: RevisionStore) {
  return store.cms.data;
}
export function getCmsRevisionsIndex(store: RevisionStore) {
  return store.cms.index;
}
export function getCmsRevisionsPagination(store: RevisionStore) {
  return store.cms.pagination;
}
