import { all, call, fork, put, takeLatest } from 'redux-saga/effects';
import get from 'lodash-es/get';

import REVISION, { FetchCmsRevisionsRequest } from 'stores/revision/types';
import { setLoading } from 'stores/app/loadings/action';
import { callApi } from 'stores/services';
import {
  fetchCmsRevisionsSuccess,
  fetchCmsRevisionsFailed
} from 'stores/revision/actions';

function* watchFetchCmsRevisions() {
  yield takeLatest(
    REVISION.FETCH_CMS_REVISIONS_REQUEST,
    fetchCmsRevisionsHandler
  );
}

function* fetchCmsRevisionsHandler(action: FetchCmsRevisionsRequest) {
  yield put(setLoading(REVISION.FETCH_CMS_REVISIONS_REQUEST, true));

  try {
    const res = yield call(
      callApi,
      'get',
      `/projects/${action.payload}/cms/revisions?limit=1000000`
    );
    res.data = res.data.sort(
      (a, b) =>
        new Date(b.created_at).valueOf() - new Date(a.created_at).valueOf()
    );
    yield put(fetchCmsRevisionsSuccess(res));
    yield put(setLoading(REVISION.FETCH_CMS_REVISIONS_REQUEST, false));
  } catch (err) {
    const errorMessage = get(err, 'response.data.message', 'Unknown error.');
    yield put(fetchCmsRevisionsFailed(errorMessage));
  }
}

export default function* root() {
  yield all([fork(watchFetchCmsRevisions)]);
}
