import { createStore, compose, applyMiddleware } from 'redux';
import { routerMiddleware } from 'connected-react-router';
import thunk from 'redux-thunk';
import createSagaMiddleware from 'redux-saga';
import createHistory from 'history/createBrowserHistory';
import * as Raven from 'raven-js';
import createRavenMiddleware from 'raven-for-redux';

import * as env from 'utils/env';

import rootReducer from './reducers';
import rootSaga from './sagas';

const __DSN__ = env.getRuntimeEnv(
  'REACT_APP_RUNTIME_SENTRY_DSN',
  env.defaultEnvs['REACT_APP_RUNTIME_SENTRY_DSN']
);

Raven.config(__DSN__, {
  dataCallback: data => {
    data.extra.state = JSON.parse(JSON.stringify(data.extra.state));
    delete data.extra.state.auth;

    return data;
  },
  release: process.env.REACT_APP_GIT_TAG
}).install();

const ravenMiddleware = createRavenMiddleware(Raven, {
  getUserContext: state => ({
    id: state.auth.token.detail.id,
    username: state.auth.token.detail.username,
    email: state.auth.token.detail.email
  })
});

const composeEnhancers =
  window['__REDUX_DEVTOOLS_EXTENSION_COMPOSE__'] || compose;
const history = createHistory();

export default function configureStore(initialState: any) {
  const sagaMiddleware = createSagaMiddleware();

  const store = createStore(
    rootReducer(history),
    composeEnhancers(
      applyMiddleware(thunk),
      applyMiddleware(sagaMiddleware),
      applyMiddleware(routerMiddleware(history)),
      applyMiddleware(ravenMiddleware)
    )
  );

  let sagaTask = sagaMiddleware.run(rootSaga);

  if (module.hot) {
    module.hot.accept('./reducers', () => {
      const nextReducer = require('./reducers').default;
      store.replaceReducer(nextReducer);
    });

    module.hot.accept('./sagas', () => {
      const nextSagas = require('./sagas').default;
      sagaTask.cancel();
      sagaTask.done.then(() => {
        sagaTask = sagaMiddleware.run(nextSagas);
      });
    });
  }

  return { store, history };
}
