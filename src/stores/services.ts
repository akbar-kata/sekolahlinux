import axios from 'axios';
import { call, select } from 'redux-saga/effects';

import RootStore from 'interfaces/rootStore';
import * as env from 'utils/env';

import { getAuthSelected } from './user/auth/selectors';
import { validate } from './user/auth/sagas';

export const API_URL = env.getRuntimeEnv(
  'REACT_APP_RUNTIME_ZAUN_URL',
  env.defaultEnvs['REACT_APP_RUNTIME_ZAUN_URL']
);
export const KATA_EMPFANG_URL = env.getRuntimeEnv(
  'REACT_APP_RUNTIME_EMPFANG_URL',
  env.defaultEnvs['REACT_APP_RUNTIME_EMPFANG_URL']
);

export function callApiSvc(
  token: string,
  type: string,
  url: string,
  data?: any,
  base?: string
) {
  return axios({
    url,
    data,
    method: type,
    baseURL: base ? base : API_URL,
    headers: {
      'Content-Type': 'application/json; charset=utf-8',
      Authorization: `Bearer ${token}`
    }
  }).then(resp => resp.data);
}

export function* callApi(type: string, url: string, data?: any, base?: string) {
  try {
    const selected = yield select((state: RootStore) =>
      getAuthSelected(state.auth)
    );

    return yield call(callApiSvc, selected.token, type, url, data, base);
  } catch (err) {
    if (err.response.status === 403) {
      yield call(validate);
    }
    throw err;
  }
}

export function callApiThunk(type: string, url: string, data?: any) {
  return (dispatch: Function, getState: Function) => {
    const token = getState().auth.selected
      ? getState().auth.selected.token
      : '';
    return axios({
      url,
      data,
      method: type,
      baseURL: API_URL,
      headers: {
        'Content-Type': 'application/json; charset=utf-8',
        Authorization: `Bearer ${token}`
      }
    }).then(resp => resp.data);
  };
}

export function request(type: string, url: string, data?: any) {
  return axios({
    url,
    data,
    method: type,
    baseURL: API_URL,
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    }
  }).then(resp => resp.data);
}
