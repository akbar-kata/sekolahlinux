import { combineReducers } from 'redux';

import { ReduxAction } from 'interfaces';
import { Store as PricingStore } from 'interfaces/pricing';
import PRICING from './types';
import { Metadata } from '../../interfaces/metadata';
import { TypedReduxAction } from '../../interfaces';

const loadingInit = {
  fetch: false,
  add: false,
  update: false,
  remove: false
};

const loadings = (
  state: PricingStore['loadings'] = loadingInit,
  { type, action, payload }: ReduxAction
) => {
  const actionCaps = action ? action.toUpperCase() : 'FETCH';
  switch (type) {
    case PRICING[`${actionCaps}_REQUEST`]:
      return Object.assign({}, state, {
        [action as string]: true
      });
    case PRICING[`${actionCaps}_SUCCESS`]:
    case PRICING[`${actionCaps}_FAIL`]:
      return Object.assign({}, state, {
        [action as string]: false
      });
    default:
      return state;
  }
};

const errorsInit = {
  fetch: null,
  add: null,
  update: null,
  remove: null
};

const errors = (
  state: PricingStore['errors'] = errorsInit,
  { type, action, payload }: ReduxAction
) => {
  const actionCaps = action ? action.toUpperCase() : 'FETCH';
  switch (type) {
    case PRICING.ERROR_CLEAR:
    case PRICING[`${actionCaps}_REQUEST`]:
      return Object.assign({}, state, {
        [action as string]: null
      });
    case PRICING[`${actionCaps}_FAIL`]:
      return Object.assign({}, state, {
        [action as string]: payload.error
      });
    default:
      return state;
  }
};

const selected = (
  state: PricingStore['selected'] = null,
  { type, payload }: ReduxAction
) => {
  switch (type) {
    case PRICING.SELECT:
      return payload;
    default:
      return state;
  }
};

const data = (
  state: PricingStore['data'] = {},
  { type, payload }: ReduxAction
) => {
  switch (type) {
    case PRICING.FETCH_SUCCESS:
      return Object.assign({}, payload.data);
    case PRICING.ADD_SUCCESS:
    case PRICING.UPDATE_SUCCESS:
      return Object.assign({}, state, payload);
    case PRICING.REMOVE_SUCCESS:
      const { [payload.id]: deletedItem, ...rest } = state;
      return rest;
    default:
      return state;
  }
};

const index = (
  state: PricingStore['index'] = [],
  { type, payload }: ReduxAction
) => {
  switch (type) {
    case PRICING.FETCH_SUCCESS:
      return [...payload.index];
    case PRICING.ADD_SUCCESS:
      return [...state, ...Object.keys(payload)];
    case PRICING.REMOVE_SUCCESS:
      return state.filter(id => id !== payload.id);
    default:
      return state;
  }
};

const metadata = (
  state: PricingStore['metadata'] = {
    limit: 10,
    total: 0,
    page: 0
  },
  { type, payload }: TypedReduxAction<Metadata>
): Metadata => {
  switch (type) {
    case PRICING.SET_METADATA:
      return Object.assign({}, state, payload);
    default:
      return state;
  }
};

const lastUpdate = (
  state: PricingStore['lastUpdate'] = Date.now(),
  { type, payload }: ReduxAction
) => {
  switch (type) {
    default:
      return state;
  }
};

export default combineReducers<PricingStore>({
  selected,
  index,
  data,
  loadings,
  errors,
  metadata,
  lastUpdate
});
