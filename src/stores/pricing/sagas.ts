import { all, fork, put, takeLatest, call } from 'redux-saga/effects';
import shortId from 'shortid';
import get from 'lodash-es/get';

import {
  pricingFail,
  selectPricing,
  fetchPricingSuccess,
  addPricingSuccess,
  setPricingMetadata,
  updatePricingSuccess,
  removePricingSuccess
} from './actions';
import { closeModal } from 'stores/app/modal/actions';
import { callApi } from 'stores/services';
import { ReduxAction, TypedReduxAction } from 'interfaces';
import PRICING from './types';
import { FetchRequest } from 'interfaces/pricing';
import { addNotification } from 'stores/app/notification/actions';

function* fetchPricing({ payload }: TypedReduxAction<FetchRequest>) {
  try {
    const pageNumber = payload ? (payload.page ? payload.page : 1) : 1;
    const res = yield call(callApi, 'get', `/pricingplans?page=${pageNumber}`);
    const data = res.data;
    yield put(setPricingMetadata(res));
    yield put(fetchPricingSuccess(data));
    if (data.length > 0) {
      yield put(selectPricing(data[0].id));
    }
  } catch (err) {
    const errorMessage = get(
      err,
      'response.data.message',
      'Oops.. something wrong'
    );
    yield put(pricingFail('fetch', errorMessage));
  }
}

function* addPricing({ payload: { data } }: ReduxAction) {
  try {
    yield call(callApi, 'post', '/pricingplans', data);
    yield put(addPricingSuccess({ ...data, id: shortId.generate() }));
    yield put(
      addNotification({
        title: 'Success',
        message: 'Pricing created successfully',
        status: 'success',
        dismissible: true,
        dismissAfter: 5000
      })
    );
    yield put(closeModal('PricingCreate')); // 'PricingCreate' is modal id registered when modal opened
  } catch (err) {
    const errorMessage = get(
      err,
      'response.data.message',
      'Oops.. something wrong'
    );
    yield put(
      addNotification({
        title: 'Failed',
        message: errorMessage,
        status: 'error',
        dismissible: true,
        dismissAfter: 5000
      })
    );
    yield put(pricingFail('add', errorMessage));
  }
}

function* updatePricing({ payload: { id, data } }: ReduxAction) {
  try {
    const resp = Object.assign({}, data, { id });
    yield put(updatePricingSuccess(id, resp));
    yield put(
      addNotification({
        title: 'Success',
        message: 'Pricing updated successfully',
        status: 'success',
        dismissible: true,
        dismissAfter: 5000
      })
    );
    yield put(closeModal('PricingUpdate')); // 'PricingUpdate' is modal id registered when modal opened
  } catch (err) {
    const errorMessage = get(
      err,
      'response.data.message',
      'Oops.. something wrong'
    );
    yield put(
      addNotification({
        title: 'Failed',
        message: errorMessage,
        status: 'error',
        dismissible: true,
        dismissAfter: 5000
      })
    );
    yield put(pricingFail('update', errorMessage));
  }
}

function* removePricing({ payload: { id, name } }: ReduxAction) {
  try {
    yield put(removePricingSuccess(id));
    yield put(
      addNotification({
        title: 'Success',
        message: 'Pricing deleted successfully',
        status: 'success',
        dismissible: true,
        dismissAfter: 5000
      })
    );
  } catch (err) {
    const errorMessage = get(
      err,
      'response.data.message',
      'Oops.. something wrong'
    );
    yield put(
      addNotification({
        title: 'Failed',
        message: errorMessage,
        status: 'error',
        dismissible: true,
        dismissAfter: 5000
      })
    );
    yield put(pricingFail('remove', errorMessage));
  }
}

function* watchPricingFetch() {
  yield takeLatest(PRICING.FETCH_REQUEST, fetchPricing);
}

function* watchPricingAdd() {
  yield takeLatest(PRICING.ADD_REQUEST, addPricing);
}

function* watchPricingUpdate() {
  yield takeLatest(PRICING.UPDATE_REQUEST, updatePricing);
}

function* watchPricingRemove() {
  yield takeLatest(PRICING.REMOVE_REQUEST, removePricing);
}

function* pricingFlow() {
  yield all([
    fork(watchPricingFetch),
    fork(watchPricingAdd),
    fork(watchPricingUpdate),
    fork(watchPricingRemove)
  ]);
}

export default pricingFlow;
