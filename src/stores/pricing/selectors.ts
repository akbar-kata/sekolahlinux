import * as Pricing from 'interfaces/pricing';
import { Metadata } from 'interfaces/metadata';

export function getPricingLoading(state: Pricing.Store, type: string): boolean {
  return state.loadings[type];
}

export function getPricingError(
  state: Pricing.Store,
  type: string
): string | null {
  return state.errors[type];
}

export function getPricingSelected(state: Pricing.Store): string | null {
  return state.selected;
}

export function getPricingLastUpdated(state: Pricing.Store): number {
  return state.lastUpdate;
}

export function getPricingIndex(state: Pricing.Store): string[] {
  return state.index;
}

export function getPricingData(state: Pricing.Store): Pricing.DataMap {
  return state.data;
}

export function getPricingMeta(state: Pricing.Store): Metadata {
  return state.metadata;
}

export function getPricingDetail(
  state: Pricing.Store,
  pricingId: string
): Pricing.Data {
  return state.data[pricingId];
}
