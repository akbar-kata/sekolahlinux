import { JsonArray } from 'interfaces';
import * as Pricing from 'interfaces/pricing';
import PRICING from './types';
import { Metadata } from 'interfaces/metadata';

export function clearPricingError(action: string) {
  return {
    action,
    type: PRICING.ERROR_CLEAR
  };
}

function requestPricing<T>(action: string, payload?: T) {
  return {
    action,
    payload,
    type: PRICING[`${action.toUpperCase()}_REQUEST`]
  };
}

export function pricingFail(action: string, error: string) {
  return {
    action,
    type: PRICING[`${action.toUpperCase()}_FAIL`],
    payload: {
      error
    }
  };
}

export function selectPricing(toSelect: string) {
  return {
    type: PRICING.SELECT,
    payload: toSelect
  };
}

export function fetchPricingRequest() {
  return requestPricing('fetch');
}
export function fetchPricingRequestWithPayload(page: number) {
  return requestPricing<Pricing.FetchRequest>('fetch', {
    page
  });
}

export function fetchPricingSuccess(raw: JsonArray) {
  const data = {};
  const index = raw.map((item: any) => {
    data[item.id] = item;
    return item.id;
  });
  return {
    type: PRICING.FETCH_SUCCESS,
    action: 'fetch',
    payload: {
      index,
      data
    }
  };
}

export function setPricingMetadata(meta: Metadata) {
  return {
    type: PRICING.SET_METADATA,
    payload: meta
  };
}

export function addPricingRequest(data: Pricing.Data) {
  return requestPricing<{ data: Pricing.Data }>('add', { data });
}

export function addPricingSuccess(data: Pricing.Data) {
  return {
    type: PRICING.ADD_SUCCESS,
    action: 'add',
    payload: {
      [data.id]: data
    }
  };
}

export function updatePricingRequest(id: string, data: Pricing.Data) {
  return requestPricing<{ id: string; data: Pricing.Data }>('update', {
    id,
    data
  });
}

export function updatePricingSuccess(id: string, data: Pricing.Data) {
  return {
    type: PRICING.UPDATE_SUCCESS,
    action: 'update',
    payload: {
      [id]: data
    }
  };
}

export function removePricingRequest(id: string, name: string) {
  return requestPricing<{ id: string; name: string }>('remove', { id, name });
}

export function removePricingSuccess(id: string) {
  return {
    type: PRICING.REMOVE_SUCCESS,
    action: 'remove',
    payload: { id }
  };
}
