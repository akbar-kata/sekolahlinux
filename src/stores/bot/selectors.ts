import { Pagination } from 'interfaces/common';
import { BotStore, BotMap, Bot } from 'interfaces/bot';
import prepareToSave from './utils/prepareToSave';

export function getBotSelected(state: BotStore): string | null {
  return state.selected;
}

export function getBotPage(state: BotStore): Pagination {
  return state.pagination;
}

export function getBotIndex(state: BotStore): string[] {
  return state.index;
}

export function getBotData(state: BotStore): BotMap {
  return state.data;
}

export function getBotDetail(state: BotStore, id: string): Bot {
  return state.data[id];
}

export function isExist(state: BotStore, name: string, id?: string): boolean {
  const duplicate = state.index.filter(
    idx => state.data[idx].name === name && idx !== id
  );
  return !!duplicate.length;
}

export function getPreparedBot(state: BotStore) {
  return prepareToSave(state);
}
