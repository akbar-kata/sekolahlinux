import { IntentStore, IntentMap } from 'interfaces/bot';

/**
 * get all intents data
 *
 * @param {IntentStore} state botstudio intents state tree
 * @return {IntentMap} return state tree as it is
 */
export function getData(state: IntentStore): IntentMap {
  return state.data;
}

/**
 * get all intents indexes
 *
 * @param {IntentStore} state botstudio intents state tree
 * @return {Array} return actions indexes
 */
export function getIndex(state: IntentStore): string[] {
  return state.index;
}

/**
 * get actions index with flow matched
 *
 * @param {ActionsReducer} state botstudio actions state tree
 * @param {string} flow name flow to be mathed
 * @return {Array} return matched actions indexes
 */
export function getByFlow(state: IntentStore, flow?: string | null): string[] {
  return !flow
    ? []
    : state.index.filter(id => {
        return state.data[id].flow === flow;
      });
}

/**
 * get duplicate
 */
export function isExist(
  state: IntentStore,
  flow: string,
  name: string,
  id?: string
): boolean {
  const duplicate = state.index.filter(
    idx =>
      state.data[idx].flow === flow &&
      state.data[idx].name === name &&
      idx !== id
  );
  return !!duplicate.length;
}
