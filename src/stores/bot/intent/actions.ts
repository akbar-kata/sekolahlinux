import shortid from 'shortid';

import { IntentMap, Intent } from 'interfaces/bot';
import TYPES from './types';

export function fetch(data: IntentMap) {
  return {
    type: TYPES.FETCH,
    payload: {
      data,
      index: Object.keys(data)
    }
  };
}

export function assignData(flow: string, data: any): Intent {
  const newData: Intent = {
    flow,
    name: data.name || '',
    type: data.type || 'text',
    initial: 'initial' in data ? data.initial : false,
    fallback: 'fallback' in data ? data.fallback : false
  };
  if (!!data.condition) {
    newData.condition = data.condition;
  }
  if (!!data.classifier) {
    newData.classifier = data.classifier;
  }
  if (!!data.attributes) {
    newData.attributes = data.attributes;
  }
  if (!!data.priority) {
    newData.priority = data.priority;
  }
  return newData;
}

export function create(flow: string, data: any) {
  if (!flow) {
    return;
  }
  return {
    type: TYPES.CREATE,
    payload: {
      [shortid.generate()]: assignData(flow, data)
    } as IntentMap
  };
}

export function update(flow: string, id: string, data: any) {
  if (!flow) {
    return;
  }
  return {
    type: TYPES.UPDATE,
    payload: {
      [id]: assignData(flow, data)
    } as IntentMap
  };
}

export function remove(id: string) {
  return {
    type: TYPES.DELETE,
    payload: {
      id
    }
  };
}
