import { JsonArray } from 'interfaces';
import { Bot } from 'interfaces/bot';
import { Pagination } from 'interfaces/common';
import BOT from './types';

function requestBot<T>(action: string, payload?: T) {
  return {
    action,
    payload,
    type: BOT[`${action.toUpperCase()}_REQUEST`]
  };
}

export function selectBot(projectId: string, revisionId: string) {
  return {
    type: BOT.SELECT,
    payload: { projectId, revisionId }
  };
}

export function setDraft(setBotDraft: boolean) {
  return {
    type: BOT.SET_DRAFT,
    payload: setBotDraft
  };
}

function formatTimezone(timezone?: any): string {
  let timezoneStr = '-';
  if (!!timezone) {
    timezoneStr = parseFloat(timezone).toFixed(2);
    timezoneStr = timezoneStr.replace('.', ':');
    if (timezone > 0) {
      timezoneStr = `+${timezoneStr}`;
    }
    timezoneStr = `GMT ${timezoneStr}`;
  }
  return timezoneStr;
}

export function fetchBotRequest(args?: { withSelect: boolean }) {
  const opts: { withSelect?: boolean } = { ...args };

  if (args === undefined || (args as any).withSelect === undefined) {
    opts.withSelect = true;
  }

  return requestBot('fetch', opts);
}

export function fetchBotSuccess({
  data,
  pagination
}: {
  data: JsonArray;
  pagination: Pagination;
}) {
  const newData = {};
  const index = data.map((item: any) => {
    item.timezoneStr = formatTimezone(item.timezone);
    newData[item.id] = item;
    return item.id;
  });
  return {
    type: BOT.FETCH,
    action: 'fetch',
    payload: {
      index,
      pagination,
      data: newData
    }
  };
}

export function fetchBotFailed() {
  return {
    type: BOT.FETCH,
    action: 'fetch',
    payload: {
      index: [],
      data: {},
      pagination: {
        limit: 10,
        page: 1,
        total: 0,
        count: 0
      }
    }
  };
}

export function addBotRequest(data: Bot) {
  return requestBot<{ data: Bot }>('add', { data });
}

export function addBotSuccess(data: Bot) {
  data.timezoneStr = formatTimezone(data.timezone);
  return {
    type: BOT.ADD,
    action: 'add',
    payload: {
      [data.id]: data
    }
  };
}

export function updateBotRequest(id: string, increment: string, data: Bot) {
  return requestBot<{
    id: string;
    increment: string;
    data: Bot;
  }>('update', { id, increment, data });
}

export function updateBotSuccess(id: string, data: Bot) {
  data.timezoneStr = formatTimezone(data.timezone);
  return {
    type: BOT.UPDATE,
    action: 'update',
    payload: {
      [id]: data
    }
  };
}

export function removeBotRequest(id: string, name: string) {
  return requestBot<{ id: string; name: string }>('remove', { id, name });
}

export function removeBotSuccess(id: string) {
  return {
    type: BOT.REMOVE,
    action: 'remove',
    payload: { id }
  };
}

export function saveDraftRequest(
  id: string,
  type: string,
  data: any,
  notif?: boolean
) {
  return requestBot<{
    id: string;
    type: string;
    data: Bot;
    notif?: boolean;
  }>('save_draft', { id, type, data, notif });
}

export function saveDraftSuccess() {
  return {
    type: BOT.SAVE_DRAFT_SUCCESS,
    action: 'save_draft'
  };
}

export function removeDraftRequest(id: string) {
  return requestBot<{ id: string }>('remove_draft', { id });
}

export function removeDraftSuccess() {
  return {
    type: BOT.REMOVE_DRAFT_SUCCESS,
    action: 'remove_draft'
  };
}

export function botFail(action: string, error: string) {
  return {
    action,
    type: BOT[`${action.toUpperCase()}_FAIL`],
    payload: {
      error
    }
  };
}

export function discardDraft() {
  return {
    type: BOT.DISCARD_DRAFT_REQUEST,
    action: 'discard_draft_request'
  };
}

export function discardDraftSuccess() {
  return {
    type: BOT.DISCARD_DRAFT_SUCCESS,
    action: 'discard_draft_success'
  };
}
