import errorsReducers from './errors/reducers';
import { combineReducers } from 'redux';

export default combineReducers({
  errors: errorsReducers
});
