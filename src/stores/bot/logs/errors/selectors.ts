import { LogErrors, LogError } from 'interfaces/bot/log';

export function getSelectedGroup(state: LogErrors): string {
  return state.selectedGroup;
}

export function getSelectedCode(state: LogErrors): string {
  return state.selectedCode;
}

export function getGroups(state: LogErrors): string[] {
  return state.groups;
}

export function getCodes(state: LogErrors): { [group: string]: string[] } {
  return state.codes;
}

export function getData(state: LogErrors): LogError[] {
  return state.data;
}

export function getStartDate(state: LogErrors): string {
  return state.startDate;
}

export function getEndDate(state: LogErrors): string {
  return state.endDate;
}

export function getRevision(state: LogErrors): string {
  return state.revision;
}

export function getEnvironment(state: LogErrors): string {
  return state.environment;
}
