import { all, call, fork, put, select, takeLatest } from 'redux-saga/effects';
import isEmpty from 'lodash-es/isEmpty';
import get from 'lodash-es/get';

import LOGS_ERRORS from './types';
import { ReduxAction } from 'interfaces';
import {
  setData,
  setPaginationToken,
  setEnvironment,
  pushData,
  getDataSuccess,
  getDataFailed,
  setRevision
} from './actions';
import DEPLOYMENT from 'stores/deployment/types';
import CHANNEL from 'stores/deployment/channel/types';
import Store from 'interfaces/rootStore';
import { callApi } from 'stores/services';
import { addNotification } from 'stores/app/notification/actions';
import BOT_TYPES from 'stores/bot/types';
import {
  getEnvironmentDetail,
  getEnvironmentSelected
} from 'stores/deployment/environment/selectors';
import { getChannelSelected } from 'stores/deployment/channel/selectors';

function* getData(action: ReduxAction) {
  const PROJECT_ID = yield select((store: Store) => store.project.selected);

  const request = yield select(({ deployment, bot: { log } }: Store) => {
    const selectedEnv = getEnvironmentSelected(deployment.environment);
    const selectedEnvDetail = selectedEnv
      ? getEnvironmentDetail(deployment.environment, selectedEnv)
      : null;
    return {
      environmentId: selectedEnvDetail ? selectedEnvDetail.id : null,
      channelId: getChannelSelected(deployment.channel),
      errorGroup: log.errors.selectedGroup,
      errorCode: log.errors.codes[log.errors.selectedGroup],
      start: new Date(log.errors.startDate).toISOString(),
      end: new Date(log.errors.endDate).toISOString(),
      nextToken: log.errors.paginationToken,
      revision: log.errors.revision
    };
  });
  try {
    const requestQuery = Object.entries(request)
      .map(entry => {
        const [key, value] = entry;
        if (value) {
          return `${key}=${encodeURIComponent(value.toString())}`;
        }
        return null;
      })
      .filter(query => query !== null)
      .join('&');

    if (!isEmpty(request.environmentId) && !isEmpty(request.channelId)) {
      const response = yield call(
        callApi,
        'get',
        `/projects/${PROJECT_ID}/errors?${requestQuery}`,
        request
      );
      yield put(setPaginationToken(response.nextToken));
      if (request.nextToken) {
        yield put(pushData(response.data));
      } else {
        yield put(setData(response.data));
      }
    }
    yield put(getDataSuccess());
  } catch (err) {
    const errorMessage = get(
      err,
      'response.data.message',
      'An unknown error has occured.'
    );
    yield put(
      addNotification({
        title: 'Failed Fetching Error Logs',
        message: errorMessage,
        status: 'error',
        dismissible: true,
        dismissAfter: 5000
      })
    );
    yield put(getDataFailed(errorMessage));
  }
}

function* getVersionLatest() {
  const latestVersion = yield select(
    (store: Store) => store.bot.version.latest
  );
  yield put(setRevision(latestVersion));
}

function* getLatestEnv() {
  const latestEnv = yield select((store: Store) =>
    get(store, ['deployment', 'index', '0'], null)
  );

  if (latestEnv) {
    yield put(setEnvironment(latestEnv));
  }
}

function* watchGetDataRequest() {
  yield takeLatest(
    [
      LOGS_ERRORS.GET_DATA_REQUEST,
      CHANNEL.SELECT,
      DEPLOYMENT.SELECT,
      LOGS_ERRORS.SET_START_DATE,
      LOGS_ERRORS.SET_END_DATE,
      LOGS_ERRORS.SET_ENVIRONMENT,
      LOGS_ERRORS.SET_CHANNEL,
      LOGS_ERRORS.SET_SELECTED_GROUP,
      LOGS_ERRORS.SET_SELECTED_CODE,
      LOGS_ERRORS.SET_REVISION
    ],
    getData
  );
}

function* watchVersionList() {
  yield takeLatest([BOT_TYPES.FETCH], getVersionLatest);
}

function* watchEnv() {
  yield takeLatest([DEPLOYMENT.FETCH_SUCCESS], getLatestEnv);
}

export default function*() {
  yield all([fork(watchGetDataRequest)]);
  yield all([fork(watchVersionList)]);
  yield all([fork(watchEnv)]);
}
