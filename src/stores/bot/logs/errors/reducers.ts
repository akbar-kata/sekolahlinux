import { ReduxAction } from 'interfaces';
import LOGS_ERRORS from './types';
import { combineReducers } from 'redux';
import { LogError } from 'interfaces/bot/log';

const range = (start, end) => {
  const length = end - start;
  return Array.from({ length }, (_, i) => start + i);
};

const defaultErrorGroup = '1000';
const groupsDefaults = {
  1000: 'User Error',
  4000: 'System Error'
};

// We generate quite large range number for error codes.
// This means that the error code will be large as our error code getting bigger.
// To avoid hitting maximum URL length on browser, we need to move from GET to POST
// in order to get the error log by error code range.
const codesDefaults = {
  1000: [...range(1001, 1101)],
  4000: [...range(4001, 4101)]
};

const groups = (state: object = groupsDefaults, action: ReduxAction) => {
  switch (action.type) {
    case LOGS_ERRORS.SET_GROUPS:
      return action.payload;
    default:
      return state;
  }
};

const codes = (
  state: { [group: string]: number[] } = codesDefaults,
  action: ReduxAction
) => {
  switch (action.type) {
    case LOGS_ERRORS.SET_CODES:
      return action.payload;
    default:
      return state;
  }
};

const selectedGroup = (
  state: string = defaultErrorGroup,
  action: ReduxAction
) => {
  switch (action.type) {
    case LOGS_ERRORS.SET_SELECTED_GROUP:
      return action.payload;
    default:
      return state;
  }
};

const selectedCode = (
  state: number = codesDefaults[defaultErrorGroup][0],
  action: ReduxAction
) => {
  switch (action.type) {
    case LOGS_ERRORS.SET_SELECTED_CODE:
      return action.payload;
    case LOGS_ERRORS.SET_SELECTED_GROUP:
      return codesDefaults[action.payload][0];
    default:
      return state;
  }
};

const paginationToken = (state: string | null = null, action: ReduxAction) => {
  switch (action.type) {
    case LOGS_ERRORS.SET_PAGINATION_TOKEN:
      return action.payload;
    default:
      return state;
  }
};

const data = (state: LogError[] = [], action: ReduxAction) => {
  switch (action.type) {
    case LOGS_ERRORS.SET_DATA:
      return action.payload;
    case LOGS_ERRORS.PUSH_DATA:
      return state.concat(action.payload);
    default:
      return state;
  }
};

const today = new Date();
today.setHours(0, 0, 0, 0);

const startDate = (
  state: string = today.toISOString(),
  action: ReduxAction
) => {
  switch (action.type) {
    case LOGS_ERRORS.SET_START_DATE:
      return action.payload;
    default:
      return state;
  }
};

const endDate = (
  state: string = new Date().toISOString(),
  action: ReduxAction
) => {
  switch (action.type) {
    case LOGS_ERRORS.SET_END_DATE:
      return action.payload;
    default:
      return state;
  }
};

const revision = (state: string = '', action: ReduxAction) => {
  switch (action.type) {
    case LOGS_ERRORS.SET_REVISION:
      return action.payload;
    default:
      return state;
  }
};

const environment = (state: string = '', action: ReduxAction) => {
  switch (action.type) {
    case LOGS_ERRORS.SET_ENVIRONMENT:
      return action.payload;
    default:
      return state;
  }
};

export default combineReducers({
  groups,
  codes,
  selectedGroup,
  selectedCode,
  paginationToken,
  startDate,
  endDate,
  data,
  revision,
  environment
});
