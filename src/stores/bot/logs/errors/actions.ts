import LOGS_ERRORS from './types';
import { LogError } from 'interfaces/bot/log';

export function setGroups(groups: string[]) {
  return {
    type: LOGS_ERRORS.SET_GROUPS,
    payload: groups
  };
}

export function setCodes(codes: { [group: string]: string[] }) {
  return {
    type: LOGS_ERRORS.SET_CODES,
    payload: codes
  };
}

export function setSelectedGroup(group: string) {
  return {
    type: LOGS_ERRORS.SET_SELECTED_GROUP,
    payload: group
  };
}

export function setSelectedCode(code: string) {
  return {
    type: LOGS_ERRORS.SET_SELECTED_CODE,
    payload: code
  };
}

export function setStartDate(date: string) {
  return {
    type: LOGS_ERRORS.SET_START_DATE,
    payload: date
  };
}

export function setEndDate(date: string) {
  return {
    type: LOGS_ERRORS.SET_END_DATE,
    payload: date
  };
}

export function setRevision(revision: string) {
  return {
    type: LOGS_ERRORS.SET_REVISION,
    payload: revision
  };
}

export function setEnvironment(env: string) {
  return {
    type: LOGS_ERRORS.SET_ENVIRONMENT,
    payload: env
  };
}

export function setChannel(channel: string) {
  return {
    type: LOGS_ERRORS.SET_CHANNEL,
    payload: channel
  };
}

export function setData(data: LogError[]) {
  return {
    type: LOGS_ERRORS.SET_DATA,
    payload: data
  };
}
export function pushData(data: LogError[]) {
  return {
    type: LOGS_ERRORS.PUSH_DATA,
    payload: data
  };
}

export function setPaginationToken(token: string) {
  return {
    type: LOGS_ERRORS.SET_PAGINATION_TOKEN,
    payload: token
  };
}

export function requestGetData() {
  return {
    type: LOGS_ERRORS.GET_DATA_REQUEST
  };
}

export function getDataSuccess() {
  return {
    type: LOGS_ERRORS.GET_DATA_SUCCESS
  };
}

export function getDataFailed(errors: string) {
  return {
    type: LOGS_ERRORS.GET_DATA_SUCCESS,
    payload: { errors }
  };
}
