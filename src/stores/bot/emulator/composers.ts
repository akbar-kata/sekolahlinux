export const MessageComposer = {
  type: {
    text(reply: any) {
      return reply.content;
    },
    data(reply: any) {
      if (
        MessageComposer.template[reply.payload[`${reply.payload.type}_type`]]
      ) {
        return {
          type: reply.payload.type,
          subType: reply.payload[`${reply.payload.type}_type`],
          items: MessageComposer.template[
            reply.payload[`${reply.payload.type}_type`]
          ](reply)
        };
      }
      return null;
    },
    unsupported(reply: any) {
      return null;
    }
  },
  template: {
    imagemap(reply: any) {
      return {
        description: reply.payload.items.altText,
        url: reply.payload.items.baseUrl,
        actions: reply.payload.items.actions.map(action => {
          return {
            text: action.text,
            type: action.type
          };
        })
      };
    }
  }
};
