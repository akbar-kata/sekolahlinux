import EMULATOR from './types';
import {
  RichMessage,
  MessageOwner,
  MessageType
} from 'interfaces/bot/emulator';

export function sendMessage(botId: string, sessionId: string, message: string) {
  return {
    type: EMULATOR.SEND_MESSAGE_REQUEST,
    payload: {
      botId,
      sessionId,
      message
    }
  };
}

export function addMessage(
  from: MessageOwner,
  message: string | RichMessage | null,
  metadata: any,
  type: MessageType
) {
  return {
    type: EMULATOR.ADD_MESSAGE,
    payload: {
      from,
      message,
      metadata,
      type,
      date: new Date()
    }
  };
}

export function sendMesageFail(error: any) {
  return {
    type: EMULATOR.SEND_MESSAGE_FAIL,
    payload: error
  };
}

export function setSession(session: any) {
  return {
    type: EMULATOR.SET_SESSION,
    payload: session
  };
}

export function clearBags() {
  return {
    type: EMULATOR.CLEAR_BAGS
  };
}

export function clearMessages() {
  return {
    type: EMULATOR.CLEAR_MESSAGE
  };
}

export function reset() {
  return {
    type: EMULATOR.RESET
  };
}
