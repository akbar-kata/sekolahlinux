const EMULATOR = {
  ADD_MESSAGE: 'BOT_STUDIO/EMULATOR/ADD_MESSAGE',
  CLEAR_MESSAGE: 'BOT_STUDIO/EMULATOR/CLEAR_MESSAGE',
  SEND_MESSAGE_REQUEST: 'BOT_STUDIO/EMULATOR/SEND_MESSAGE_REQUEST',
  SEND_MESSAGE_FAIL: 'BOT_STUDIO/EMULATOR/SEND_MESSAGE_FAIL',
  SEND_MESSAGE_SUCCESS: 'BOT_STUDIO/EMULATOR/SEND_MESSAGE_SUCCESS',
  START_SESSION_REQUEST: 'BOT_STUDIO/EMULATOR/START_SESSION_REQUEST',
  START_SESSION_SUCCESS: 'BOT_STUDIO/EMULATOR/START_SESSION_SUCCESS',
  START_SESSION_FAILED: 'BOT_STUDIO/EMULATOR/START_SESSION_FAILED',
  SET_SESSION: 'BOT_STUDIO/EMULATOR/SET_SESSION',
  SET_IS_LOADING: 'BOT_STUDIO/EMULATOR/SET_IS_LOADING',
  CLEAR_BAGS: 'BOT_STUDIO/EMULATOR/CLEAR_BAGS',
  RESET: 'BOT_STUDIO/EMULATOR/RESET'
};

export default EMULATOR;
