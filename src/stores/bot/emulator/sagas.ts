import { call, all, fork, takeLatest, put, select } from 'redux-saga/effects';

import { MessageOwner, MessageType } from 'interfaces/bot/emulator';
import Store from 'interfaces/rootStore';

import { callApi } from 'stores/services';
import { getProjectSelected } from 'stores/project/selectors';

import EMULATOR from './types';
import { MessageComposer } from './composers';
import { addMessage, sendMesageFail, setSession } from './actions';

function* sendMessage({ type, payload }: any) {
  try {
    const projectId = yield select((store: Store) =>
      getProjectSelected(store.project)
    );
    yield put(
      addMessage(
        MessageOwner.user,
        payload.message,
        {
          type: 'emulator-test-self-message',
          owner: 'user'
        },
        MessageType.text
      )
    );

    const activeSession = yield select(
      (store: Store) => store.bot.emulator.activeSession
    );

    const request: any = {
      session: activeSession,
      message: {
        type: 'text',
        content: payload.message
      }
    };
    const res = yield call(
      callApi,
      'post',
      `/projects/${projectId}/bot/converse`,
      request
    );
    yield put(setSession(res.session));
    if (res.responses && res.responses.length !== 0) {
      for (const reply of res.responses) {
        yield put(
          addMessage(
            MessageOwner.bot,
            MessageComposer.type[reply.type]
              ? MessageComposer.type[reply.type](reply)
              : null,
            reply,
            reply.type
          )
        );
      }
    }
  } catch (err) {
    if (err.response && err.response.data) {
      yield put(sendMesageFail(err.response.data));
    } else {
      yield put(sendMesageFail({ status: 'error', message: err.message }));
    }
  }
}

function* watchSendMessageRequest() {
  yield takeLatest(EMULATOR.SEND_MESSAGE_REQUEST, sendMessage);
}

export default function*() {
  yield all([fork(watchSendMessageRequest)]);
}
