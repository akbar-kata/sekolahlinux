import { ReduxAction } from 'interfaces';
import { Message, Store, Bags } from 'interfaces/bot/emulator';
import { combineReducers } from 'redux';
import EMULATOR from './types';

const storeDefaults: Store = {
  messages: [],
  isLoading: false,
  activeSession: {
    channel_id: 'console-channel',
    environment_id: 'console-environment',
    states: {},
    contexes: {},
    history: [],
    current: null,
    meta: null,
    timestamp: Date.now(),
    data: {},
    created_at: Date.now(),
    updated_at: Date.now(),
    session_start: Date.now(),
    session_id: 'test~from~console',
    id: 'test~from~console'
  },
  bags: {
    errors: {}
  }
};
export function messageReducer(
  state: Message[] = storeDefaults.messages,
  action: ReduxAction
) {
  switch (action.type) {
    case EMULATOR.ADD_MESSAGE:
      return [...state, action.payload];
    case EMULATOR.RESET:
    case EMULATOR.CLEAR_MESSAGE:
      return storeDefaults.messages;
    default:
      return state;
  }
}
export function sessionReducer(
  state: any = storeDefaults.activeSession,
  action: ReduxAction
) {
  switch (action.type) {
    case EMULATOR.SET_SESSION:
      return action.payload;
    case EMULATOR.RESET:
      return storeDefaults.activeSession;
    default:
      return state;
  }
}
export function loadingReducer(
  state: boolean = storeDefaults.isLoading,
  action: ReduxAction
) {
  switch (action.type) {
    case EMULATOR.SET_IS_LOADING:
      return action.payload;
    case EMULATOR.RESET:
      return false;
    default:
      return state;
  }
}
export function bagsReducer(
  state: Bags = storeDefaults.bags,
  action: ReduxAction
): Bags {
  switch (action.type) {
    case EMULATOR.SEND_MESSAGE_FAIL:
      return {
        ...state,
        errors: { ...state.errors, message: action.payload }
      };
    case EMULATOR.RESET:
    case EMULATOR.CLEAR_BAGS:
      return {
        errors: {}
      };
    default:
      return state;
  }
}

export default combineReducers({
  messages: messageReducer,
  activeSession: sessionReducer,
  isLoading: loadingReducer,
  bags: bagsReducer
});
