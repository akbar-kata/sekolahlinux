import shortid from 'shortid';
import isArray from 'lodash-es/isArray';
import isObject from 'lodash-es/isObject';
import isString from 'lodash-es/isString';

import { ObjectOfJson, JsonObject, Json } from 'interfaces';

function parseProcess(process: string | string[]) {
  if (process) {
    if (!isArray(process)) {
      return [process];
    }
    return process;
  }
  return undefined;
}

function parseKeywords(options: JsonObject) {
  let keywords: any[] = [];
  if (options.keywords) {
    keywords = Object.keys(options.keywords).map(key => ({
      key,
      words: options.keywords[key]
    }));
  }
  return {
    keywords,
    default: options.default,
    case: options.case,
    exact: options.exact
  };
}

function parseRegex(options: JsonObject) {
  const { index } = options;
  const newOptions: JsonObject = {
    regex: options.regex
  };
  if (index !== undefined && index !== null) {
    if (isObject(index)) {
      newOptions.multiple = true;
      newOptions.index = Object.keys(index).map((value, key) => ({
        key,
        number: value
      }));
    } else {
      newOptions.index = index;
    }
  }
  return newOptions;
}

function parseMultipleRegex(options: any) {
  const newOptions: any = {
    regex: []
  };
  if (options && options.regex) {
    newOptions.regex = Object.keys(options.regex).map(key => ({
      key,
      value: options.regex[key]
    }));
  }
  return newOptions;
}

function parseOptions(type: string, options: JsonObject): any {
  if (options) {
    switch (type) {
      case 'keyword':
        return parseKeywords(options);
      case 'regex':
        return parseRegex(options);
      case 'multipleRegex':
        return parseMultipleRegex(options);
      default:
        return options;
    }
  }
  return undefined;
}

export default function parseNLUs(raw: ObjectOfJson) {
  const nlusData = {};
  if (raw) {
    Object.keys(raw).forEach(id => {
      const newData: JsonObject = {
        name: id,
        type: raw[id].type
      };
      if (raw[id].type === 'method') {
        newData.options = {
          method: raw[id].method
        };
      } else {
        if (
          newData.type === 'regex' &&
          raw[id].options &&
          !isString((raw[id] as any).options.regex)
        ) {
          newData.type = 'multipleRegex';
        }
        newData.options = parseOptions(
          newData.type as string,
          raw[id].options as JsonObject
        );
      }
      if (raw[id].process) {
        newData.process = parseProcess(raw[id].process as
          | string
          | string[]) as Json;
      }
      nlusData[shortid.generate()] = newData;
    });
  }
  return nlusData;
}
