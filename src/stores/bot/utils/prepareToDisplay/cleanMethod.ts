function extractName(raw: string = ''): string {
  const arr = raw.split('(');
  return arr[0] || '';
}

function replaceAction(actions: any, ori: string, rep: string) {
  const newActions = {};
  Object.keys(actions || {}).forEach(act => {
    const newAct = { ...actions[act] };
    if (newAct.type === 'method' && newAct.method === ori) {
      newAct.method = rep;
    }
    newActions[act] = newAct;
  });
  return newActions;
}

function replaceState(states: any, ori: string, rep: string) {
  const newStates = {};
  Object.keys(states || {}).forEach(state => {
    const newState = { ...states[state] };
    if (newState.enter === ori) {
      newState.enter = rep;
    }
    if (newState.transit === ori) {
      newState.transit = rep;
    }
    if (newState.exit === ori) {
      newState.exit = rep;
    }
    if (newState.float && newState.float.mapping === ori) {
      newState.float.mapping = rep;
    }
    const newTrans = {};
    Object.keys(newState.transitions || {}).forEach(trans => {
      const newTran = { ...newState.transitions[trans] };
      if (newTran.mapping && newTran.mapping === ori) {
        newTran.mapping = rep;
      }
      newTrans[trans] = newTran;
    });
    newState.transitions = newTrans;
    newStates[state] = newState;
  });
  return newStates;
}

export default function cleanMethods(raw: any) {
  const { flows, methods, ...restKataML } = raw;
  const newMethods = { ...methods };
  const newFlows = {};
  Object.keys(flows || {}).forEach(flow => {
    // tslint:disable-next-line:no-shadowed-variable
    const { methods, ...rest } = flows[flow];
    const newFlow = { ...rest };

    Object.keys(methods || {}).forEach(method => {
      if (newMethods[method]) {
        // check if duplicate
        newMethods[`${flow}_${method}`] = methods[method];
        const ori = extractName(method);
        const rep = extractName(`${flow}_${method}`);
        newFlow.actions = replaceAction(newFlow.actions, ori, rep);
        newFlow.states = replaceState(newFlow.states, ori, rep);
      } else {
        newMethods[method] = methods[method];
      }
    });

    newFlows[flow] = newFlow;
  });

  return {
    ...restKataML,
    flows: newFlows,
    methods: newMethods
  };
}
