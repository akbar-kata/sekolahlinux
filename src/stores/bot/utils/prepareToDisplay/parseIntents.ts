import shortid from 'shortid';
import YAML from 'yamljs';
import isFunction from 'lodash-es/isFunction';

import { ObjectOfJson, JsonObject, JsonArray } from 'interfaces';
import { transformToArray } from './parseFlows';

function parseDict(raw: any) {
  return Object.keys(raw).map((value, key) => ({
    key,
    words: transformToArray(value)
  }));
}

function parseClassifier(raw?: any) {
  if (raw) {
    return raw.map(classifier => ({
      nlu: classifier.nlu,
      match: classifier.match,
      expression: classifier.expression,
      hint: classifier.hint,
      path: classifier.path,
      options: classifier.options
        ? YAML.stringify(classifier.options, Infinity, 2)
        : undefined,
      process: transformToArray(classifier.process),
      dict: classifier.dict ? parseDict(classifier.dict) : undefined
    }));
  }
  return raw;
}

function parseAttributes(raw: any) {
  const attrs: any[] = [];
  if (raw) {
    Object.keys(raw).forEach(id => {
      const attr = raw[id];
      attrs.push({
        name: id,
        nlu: attr.nlu,
        match: attr.match,
        hint: attr.hint,
        path: attr.path,
        options: attr.options
          ? YAML.stringify(attr.options, Infinity, 2)
          : undefined,
        process: transformToArray(attr.process),
        dict: attr.dict ? parseDict(attr.dict) : undefined
      });
    });
  }
  return attrs;
}

export default function parseIntents(flow: string, raw?: ObjectOfJson) {
  const intents = {};
  if (raw) {
    Object.keys(raw).forEach(id => {
      const { type, condition, classifier, attributes, ...rest } = raw[id];
      let parsedCond = transformToArray(condition);
      if (parsedCond) {
        parsedCond = (parsedCond as any[]).map(cond =>
          cond && isFunction(cond.replace)
            ? cond.replace(/\\/g, '').replace(/'/g, '"')
            : cond
        );
      }
      intents[shortid.generate()] = {
        flow,
        type: type || 'text',
        name: id,
        condition: parsedCond,
        classifier: parseClassifier(transformToArray(classifier) as JsonArray),
        attributes: parseAttributes(attributes as JsonObject),
        ...rest
      };
    });
  }
  return intents;
}
