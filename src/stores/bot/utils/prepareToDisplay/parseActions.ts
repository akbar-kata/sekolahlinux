import shortid from 'shortid';

import { ObjectOfJson } from 'interfaces';
import { convertToString, transformToArray } from './parseFlows';

function parseText(options: any) {
  if (options.data) {
    return {
      type: 'data',
      options: {
        data: options.data,
        path: options.path,
        template: options.template
      }
    };
  }

  return {
    type: 'text',
    options: {
      text: transformToArray(options.text)
    }
  };
}

function parseImage(options: any) {
  return {
    type: 'image',
    options: options.items
      ? {
          imageUrl: options.items.originalContentUrl,
          thumbnailUrl: options.items.previewImageUrl
        }
      : undefined
  };
}

function parseButton(options: any) {
  const actions =
    options.items && options.items.actions && options.items.actions.length
      ? options.items.actions
      : undefined;
  return {
    type: 'button',
    options: options.items
      ? {
          altText: options.altText,
          title: options.items.title,
          text: options.items.text,
          actions: actions
            ? actions.map(action => ({
                type: action.type,
                label: action.label,
                text: action.text,
                payload: action.payload
                  ? Object.keys(action.payload).map(key => ({
                      key,
                      value: convertToString(action.payload[key])
                    }))
                  : undefined,
                url: action.url || undefined
              }))
            : []
        }
      : undefined
  };
}

function parseDynamicCarousel(options: any) {
  return {
    type: 'dynamicCarousel',
    options: options.template
      ? {
          data: options.data,
          text: options.template.text,
          title: options.template.title,
          thumbnailImageUrl: options.template.thumbnailImageUrl,
          default_action: options.default_action,
          actions:
            options.template.actions && options.template.actions.length
              ? options.template.actions.map(action => ({
                  type: action.type,
                  label: action.label,
                  payload: action.payload
                    ? Object.keys(action.payload).map(key => ({
                        key,
                        value: convertToString(action.payload[key])
                      }))
                    : undefined,
                  url: action.url || undefined
                }))
              : []
        }
      : undefined
  };
}

function parseNormalCarousel(options: any) {
  return {
    type: 'carousel',
    options: {
      items:
        options.items && options.items.length
          ? options.items.map(item => {
              return {
                title: item.title,
                thumbnailImageUrl: item.thumbnailImageUrl,
                text: item.text,
                default_action: item.default_action,
                actions:
                  item.actions && item.actions.length
                    ? item.actions.map(action => ({
                        type: action.type,
                        label: action.label,
                        payload: action.payload
                          ? Object.keys(action.payload).map(key => ({
                              key,
                              value: convertToString(action.payload[key])
                            }))
                          : undefined,
                        url: action.url || undefined
                      }))
                    : []
              };
            })
          : []
    }
  };
}

function parseCarousel(options: any) {
  if (!options.items && options.template) {
    return parseDynamicCarousel(options);
  }

  return parseNormalCarousel(options);
}

function parseImageMap(options: any) {
  return {
    type: 'imageMap',
    options: options.items
      ? {
          baseUrl: options.items.baseUrl,
          altText: options.items.altText,
          baseSize: options.items.baseSize
            ? {
                width: options.items.baseSize.width,
                height: options.items.baseSize.height
              }
            : undefined,
          actions: options.items.actions
            ? options.items.actions.map(action => ({
                text: action.text,
                type: action.type,
                label: action.label,
                area: action.area
                  ? {
                      x: action.area.x,
                      y: action.area.y,
                      width: action.area.width,
                      height: action.area.height
                    }
                  : undefined
              }))
            : []
        }
      : undefined
  };
}

function parseSticker(options: any) {
  return {
    type: 'sticker',
    options: options.items
      ? {
          stickerId: options.items.stickerId,
          packageId: options.items.packageId
        }
      : undefined
  };
}

function parseVideo(options: any) {
  return {
    type: 'video',
    options: options.items
      ? {
          originalContentUrl: options.items.originalContentUrl,
          previewImageUrl: options.items.previewImageUrl
        }
      : undefined
  };
}

function parseAudio(options: any) {
  return {
    type: 'audio',
    options: options.items
      ? {
          originalContentUrl: options.items.originalContentUrl,
          duration: options.items.duration
        }
      : undefined
  };
}

function parseLocation(options: any) {
  return {
    type: 'location',
    options: options.items
      ? {
          title: options.items.title,
          address: options.items.address,
          latitude: options.items.latitude,
          longitude: options.items.longitude,
          locationImageUrl: options.items.locationImageUrl
        }
      : undefined
  };
}

function parseTemplate(options: any) {
  switch (options.type) {
    case 'image':
      return parseImage(options);
    case 'button':
      return parseButton(options);
    case 'carousel':
      return parseCarousel(options);
    case 'imagemap':
      return parseImageMap(options);
    case 'sticker':
      return parseSticker(options);
    case 'video':
      return parseVideo(options);
    case 'audio':
      return parseAudio(options);
    case 'location':
      return parseLocation(options);
    default:
      return {
        options,
        type: 'template'
      };
  }
}

function parseSchedule({ start, end, message, ...rest }: any) {
  try {
    return {
      ...rest,
      start,
      end,
      message: message
        ? {
            type: message.type,
            content: message.content,
            metadata: message.metadata
              ? Object.keys(message.metadata).map(key => ({
                  key,
                  value: convertToString(message.metadata[key])
                }))
              : []
          }
        : undefined
    };
  } catch (err) {
    return {};
  }
}

function parseAPI({ body, headers, query, form_data, ...rest }: any) {
  return {
    ...rest,
    body: body
      ? Object.keys(body).map(key => ({
          key,
          value: convertToString(body[key])
        }))
      : [],
    headers: headers
      ? Object.keys(headers).map(key => ({
          key,
          value: convertToString(headers[key])
        }))
      : [],
    query: query
      ? Object.keys(query).map(key => ({
          key,
          value: convertToString(query[key])
        }))
      : [],
    form_data: form_data
      ? Object.keys(form_data).map(key => ({
          key,
          value: convertToString(form_data[key])
        }))
      : []
  };
}

function parseCommand({ payload, ...rest }: any) {
  return {
    ...rest,
    payload: payload
      ? Object.keys(payload).map(key => ({
          key,
          value: convertToString(payload[key])
        }))
      : []
  };
}

function parseOptions(type: string, options: any): any {
  if (options) {
    switch (type) {
      case 'text':
        return parseText(options);
      case 'template':
        return parseTemplate(options);
      case 'schedule':
        return {
          type,
          options: parseSchedule(options)
        };
      case 'api':
        return {
          type,
          options: parseAPI(options)
        };
      case 'command': {
        return {
          type,
          options: parseCommand(options)
        };
      }
      default:
        return {
          type,
          options
        };
    }
  }
  return undefined;
}

// Parse method definitions here before we run through the main parser.
function parseMethodObject(method: string, options: any) {
  if (options) {
    return {
      method,
      options: options
        ? Object.keys(options).map(key => ({
            key,
            value: convertToString(options[key])
          }))
        : []
    };
  }
  return undefined;
}

export default function parseActions(flow: string, raw?: ObjectOfJson) {
  const actions = {};
  if (raw) {
    Object.keys(raw).forEach(id => {
      const { type, options, method, ...rest } = raw[id];

      const parsed = parseOptions(
        type as string,
        type === 'method'
          ? parseMethodObject(method as string, options)
          : options
      );

      actions[shortid.generate()] = {
        flow,
        name: id,
        ...parsed,
        ...rest
      };
    });
  }
  return actions;
}
