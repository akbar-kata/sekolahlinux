import shortid from 'shortid';
import isArray from 'lodash-es/isArray';
import isString from 'lodash-es/isString';

import { ObjectOfJson, Json } from 'interfaces';
import parseIntents from './parseIntents';
import parseStates from './parseStates';
import parseActions from './parseActions';
import parseMethods from './parseMethods';

export function transformToArray(data?: Json) {
  if (data) {
    if (isArray(data)) {
      return data;
    }

    return [data];
  }
  return undefined;
}

export function convertToString(raw: any) {
  if (isString(raw)) {
    return raw;
  }

  try {
    return JSON.stringify(raw);
  } catch (err) {
    return raw.toString();
  }
}

export default function parseFlow(raw: ObjectOfJson) {
  const flowsData = {};
  const intentsData = {};
  const nodesData = {};
  const connsData = {};
  const actionsData = {};
  const methodsData = {};
  Object.keys(raw).forEach(id => {
    const { actions, intents, states, methods, ...rest } = raw[id];
    const flowId = shortid.generate();
    flowsData[flowId] = {
      name: id,
      ...rest
    };
    const { nodes, conns } = parseStates(flowId, states as ObjectOfJson);
    Object.assign(nodesData, nodes);
    Object.assign(connsData, conns);
    Object.assign(intentsData, parseIntents(flowId, intents as ObjectOfJson));
    Object.assign(actionsData, parseActions(flowId, actions as ObjectOfJson));
    Object.assign(methodsData, parseMethods(methods as ObjectOfJson, flowId));
  });
  return {
    flows: flowsData,
    intents: intentsData,
    nodes: nodesData,
    connections: connsData,
    actions: actionsData,
    methods: methodsData
  };
}
