// import shortid from 'shortid';
import YAML from 'yamljs';
import isEmpty from 'lodash-es/isEmpty';

import { ObjectOfJson } from 'interfaces';
import parseNLUs from './parseNLUs';
import parseFlows from './parseFlows';
import parseMethods from './parseMethods';

function parseConfig(raw: ObjectOfJson) {
  const { id, name, timezone, desc, config, draft } = raw;
  const {
    rpmLimitResponse,
    burstMessageResponse,
    channelDisabledResponse,
    maintenanceResponse,
    ...rest
  } = config || {
    rpmLimitResponse: undefined,
    burstMessageResponse: undefined,
    channelDisabledResponse: undefined,
    maintenanceResponse: undefined
  };
  return {
    draft,
    id,
    name,
    timezone,
    desc,
    defaultMessages: {
      rpmLimitResponse,
      burstMessageResponse,
      channelDisabledResponse,
      maintenanceResponse
    },
    options: rest && !isEmpty(rest) ? YAML.stringify(rest, Infinity, 4) : ''
  };
}

export default function parseToUI(raw: any) {
  const { methods, ...rest } = parseFlows(raw.flows || {});
  return {
    config: raw ? parseConfig(raw) : {},
    ...rest,
    nlus: raw.nlus ? parseNLUs(raw.nlus) : {},
    methods: Object.assign(
      {},
      methods,
      parseMethods(raw.methods as ObjectOfJson, '__ROOT__')
    )
  };
}
