import shortid from 'shortid';
import isString from 'lodash-es/isString';
import isArray from 'lodash-es/isArray';
import isObject from 'lodash-es/isObject';
import isFunction from 'lodash-es/isFunction';
import YAML from 'yamljs';

import { ObjectOfJson } from 'interfaces';
import { convertToString } from './parseFlows';

function parseMapping(raw?: any) {
  if (raw) {
    if (isObject(raw)) {
      const newMap: any = [];
      Object.keys(raw).forEach(key => {
        if (isObject(raw[key])) {
          Object.keys(raw[key]).forEach(k => {
            // newMap[`${key}.${k}`] = convertToString(raw[key][k]);
            newMap.push({
              key: `${key}.${k}`,
              value: convertToString(raw[key][k])
            });
          });
        } else {
          newMap.push({
            key,
            value: convertToString(raw[key])
          });
        }
      });
      return newMap.length ? newMap : undefined;
    }
    return convertToString(raw);
  }
}

function parseAction(raw: any) {
  if (raw) {
    if (isArray(raw)) {
      return raw.map(action => {
        const { options, condition, ...rest } = action;
        return {
          id: shortid.generate(),
          actionType: 'advanced',
          condition:
            condition && isFunction(condition.replace)
              ? condition.replace(/\\/g, '').replace(/'/g, '"')
              : condition,
          options: options ? YAML.stringify(options, Infinity, 2) : undefined,
          useCustomOptions: options ? true : undefined,
          ...rest
        };
      });
    }

    if (isString(raw)) {
      return [
        {
          id: shortid.generate(),
          actionType: 'advanced',
          name: raw
        }
      ];
    }

    return [
      {
        id: shortid.generate(),
        actionType: 'advanced',
        ...raw
      }
    ];
  }
  return raw;
}

function generateOpts(raw: any, fallback: any) {
  return {
    enterMappingMode: !isString(raw.enter) ? 'simple' : 'method',
    transitMappingMode: !isString(raw.transit) ? 'simple' : 'method',
    exitMappingMode: !isString(raw.exit) ? 'simple' : 'method',
    transitionTab: 'fallback',
    enablefloat: !raw.float ? false : true,
    floatMappingMode:
      raw.float && isString(raw.float.mapping) ? 'method' : 'simple',
    enablefallback: !!fallback,
    fallbackMappingMode:
      fallback && isString(fallback.mapping) ? 'method' : 'simple'
  };
}

function normalizeTransition(transition: any) {
  if (!transition) {
    return undefined;
  }
  const cond = (transition as any).condition;
  return Object.assign({}, transition, {
    condition:
      cond && isFunction(cond.replace)
        ? cond.replace(/\\/g, '').replace(/'/g, '"')
        : cond
  });
}

export default function parseStates(flow: string, raw?: ObjectOfJson) {
  const nodeIds = {};
  const nodes = {};
  const conns = {};
  if (raw) {
    Object.keys(raw).forEach(id => {
      nodeIds[id] = shortid.generate();
    });
    Object.keys(raw).forEach(id => {
      const { action, transitions, float, enter, transit, exit, ...rest } = raw[
        id
      ];
      const fallback = normalizeTransition(
        transitions ? transitions[id] : undefined
      );
      nodes[nodeIds[id]] = {
        flow,
        fallback,
        id: nodeIds[id],
        name: id,
        action: parseAction(action),
        onEnter: parseMapping(enter),
        onTransit: parseMapping(transit),
        onExit: parseMapping(exit),
        float: normalizeTransition(float),
        relatedIds: transitions
          ? Object.keys(transitions).map(key => nodeIds[key])
          : [],
        ...rest,
        _opts: generateOpts(raw[id], fallback)
      };
      if (transitions) {
        Object.keys(transitions).forEach(transitionTo => {
          const { condition, priority, mapping, ...restTrans } = transitions[
            transitionTo
          ];
          const connId = shortid.generate();
          const newConns = {
            flow,
            id: connId,
            from: nodeIds[id],
            to: nodeIds[transitionTo],
            transition: {
              ...restTrans,
              mapping: parseMapping(mapping)
            },
            _opts: {
              mappingMode: !isString(mapping) ? 'simple' : 'method'
            }
          };
          if (priority) {
            newConns.transition.priority = priority.toString();
          }
          if (condition) {
            newConns.transition.condition = isFunction(condition.replace)
              ? condition.replace(/\\/g, '').replace(/'/g, '"')
              : condition;
          }
          conns[connId] = newConns;
        });
      }
    });
  }
  return {
    nodes,
    conns
  };
}
