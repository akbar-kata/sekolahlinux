import shortid from 'shortid';
import isObject from 'lodash-es/isObject';

import { ObjectOfJson } from 'interfaces';

const beautify = require('js-beautify').js_beautify;

function formatCode(raw: any) {
  return beautify(raw, {
    indent_size: 4
  });
}

export default function parseMethods(raw?: ObjectOfJson, flow?: string) {
  const methods = {};
  if (raw) {
    Object.keys(raw).forEach(id => {
      const arr = id.match(/\((.*?)\)/g); // search substring wrapped with '(' and ')'
      let name = id;
      let params: string[] = [];
      if (arr && arr.length > 0) {
        name = id.split('(')[0];
        params = arr[0]
          .replace(/\)|\(/g, '')
          .split(',')
          .map(param => param.trim());
      }
      const newMethod: any = {
        name,
        params,
        flow: '__ROOT__'
      };
      if (isObject(raw[id])) {
        newMethod.mode = 'advanced';
        newMethod.definition = formatCode(raw[id].code);
        newMethod.entry = raw[id].entry;
      } else {
        newMethod.mode = 'simple';
        newMethod.definition = formatCode(raw[id]);
      }
      methods[shortid.generate()] = newMethod;
    });
  }
  return methods;
}
