import YAML from 'yamljs';
import isString from 'lodash-es/isString';
import isArray from 'lodash-es/isArray';
import isEmpty from 'lodash-es/isEmpty';
import isFunction from 'lodash-es/isFunction';

import { simplifyArray, trim } from './parseFlows';

function parseMapping(raw?: any) {
  if (isString(raw)) {
    return raw;
  }

  if (isArray(raw)) {
    const newMap: any = {};
    raw.forEach(item => {
      newMap[trim(item.key)] = trim(item.value);
    });
    return newMap;
  }

  return undefined;
}

function parseAction(raw: any[]) {
  let newAction: any = undefined;
  if (raw) {
    if (
      raw.length === 1 &&
      !isEmpty(raw[0].name) &&
      isEmpty(raw[0].condition) &&
      isEmpty(raw[0].options)
    ) {
      newAction = trim(raw[0].name);
    } else {
      newAction = simplifyArray(
        raw.map(
          ({
            id,
            actionType,
            useCustomOptions,
            options,
            condition,
            ...rest
          }) => {
            const returnObj = {
              ...rest
            };
            if (condition) {
              returnObj.condition = isFunction(condition.replace)
                ? condition.replace(/'/g, '"').trim()
                : condition;
            }
            try {
              if (options) {
                returnObj.options = YAML.parse(options);
              }
            } catch (err) {
              // do nothing
            }
            return returnObj;
          }
        )
      );
    }
  }
  return newAction;
}

function getConnsByNodes(index: string[], data: any, nodeId?: string) {
  return !nodeId
    ? []
    : index.filter(id => {
        return data[id].from === nodeId;
      });
}

function parseTransition(raw: any) {
  const { id, priority, mapping, condition, ...rest } = raw;
  return {
    ...rest,
    condition:
      condition && isFunction(condition.replace)
        ? condition.replace(/'/g, '"').trim()
        : undefined,
    mapping: parseMapping(mapping),
    priority: priority ? parseInt(priority, 10) : undefined
  };
}

function parseTransitions(
  index: string[],
  data: any,
  nodes: any
): { [key: string]: any } {
  const newData = {};
  index
    .filter(
      id => data[id] && data[id].transition && data[id].from !== data[id].to
    )
    .sort((a, b) => {
      return data[a].transition.fallback === data[b].transition.fallback
        ? 0
        : data[a].transition.fallback
        ? 1
        : -1;
    })
    .forEach(connId => {
      const conn = data[connId];
      if (conn.to && nodes[conn.to]) {
        newData[nodes[conn.to].name] = parseTransition(conn.transition);
      }
    });
  return newData;
}

export default function parseStates(
  nodesIndex: string[],
  nodes: any,
  connsIndex: string[],
  conns: any
) {
  const states = {};
  nodesIndex.forEach(nodeId => {
    const {
      flow,
      id,
      name,
      action,
      onEnter,
      onTransit,
      onExit,
      float,
      fallback,
      relatedIds,
      _opts,
      ...rest
    } = nodes[nodeId];
    let newFloat;
    if (float) {
      const cond = float.condition;
      newFloat = Object.assign({}, float, {
        condition:
          cond && isFunction(cond.replace)
            ? cond.replace(/'/g, '"').trim()
            : undefined
      });
    }
    const transitions = parseTransitions(
      getConnsByNodes(connsIndex, conns, nodeId),
      conns,
      nodes
    );
    if (fallback) {
      transitions[name] = parseTransition(fallback);
    }
    const newStates: any = {
      transitions,
      action: !isEmpty(action) ? parseAction(action) : undefined,
      enter: !isEmpty(onEnter) ? parseMapping(onEnter) : undefined,
      transit: !isEmpty(onTransit) ? parseMapping(onTransit) : undefined,
      exit: !isEmpty(onExit) ? parseMapping(onExit) : undefined,
      float: !isEmpty(newFloat) ? newFloat : undefined,
      ...rest
    };
    states[name] = newStates;
  });
  return states;
}
