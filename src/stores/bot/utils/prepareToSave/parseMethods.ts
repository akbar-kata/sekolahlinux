import { MethodMap } from 'interfaces/bot';

export default function parseMethods(index: string[], raw?: MethodMap) {
  const methods = {};
  if (raw) {
    index.forEach(id => {
      const methodId = `${raw[id].name}(${raw[id].params.join(',')})`;
      let newMethod: any = {};
      if (raw[id].mode === 'advanced') {
        newMethod.code = raw[id].definition;
        newMethod.entry = raw[id].entry;
      } else {
        newMethod = raw[id].definition;
      }
      methods[methodId] = newMethod;
    });
  }
  return methods;
}
