import YAML from 'yamljs';

import { BotStore } from 'interfaces/bot';
import parseFlows, { getByFlow } from './parseFlows';
import parseNLUs from './parseNLUs';
import parseMethods from './parseMethods';

export default function prepareToSave(raw: BotStore) {
  const { options, draft, defaultMessages, ...rest } = raw.config;
  let newOptions = {};
  try {
    newOptions = YAML.parse(options);
  } catch (error) {
    // do nothing
  }

  return {
    ...rest,
    config: Object.assign({}, newOptions, defaultMessages),
    flows: parseFlows(raw),
    nlus: parseNLUs(raw.nlu.data),
    methods: parseMethods(getByFlow(raw.method, '__ROOT__'), raw.method.data)
  };
}
