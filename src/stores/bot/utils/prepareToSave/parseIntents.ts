import YAML from 'yamljs';
import isEmpty from 'lodash-es/isEmpty';
import isArray from 'lodash-es/isArray';
import isFunction from 'lodash-es/isFunction';

import { simplifyArray, trim } from './parseFlows';

function parseDict(raw: any[]) {
  const newData = {};
  raw.forEach(dict => {
    newData[dict.key] = simplifyArray(dict.words);
  });
  return newData;
}

function parseClassifier(raw?: any) {
  if (raw) {
    return raw.map(classifier => {
      const newClassifier: any = {
        nlu: classifier.nlu,
        match: trim(classifier.match),
        expression: trim(classifier.expression),
        hint: trim(classifier.hint),
        path: trim(classifier.path),
        process: simplifyArray(classifier.process),
        dict: classifier.dict ? parseDict(classifier.dict) : undefined
      };
      if (classifier.options) {
        try {
          newClassifier.options = YAML.parse(classifier.options);
        } catch (err) {
          newClassifier.options = {};
        }
      }
      return newClassifier;
    });
  }
  return raw;
}

function parseAttributes(raw?: any[]) {
  const attrs: any = {};
  if (raw) {
    raw.forEach(attr => {
      const newAttr: any = {
        nlu: attr.nlu,
        match: trim(attr.match),
        hint: trim(attr.hint),
        path: trim(attr.path),
        process: simplifyArray(attr.process),
        dict: attr.dict ? parseDict(attr.dict) : undefined
      };
      if (attr.options) {
        try {
          newAttr.options = YAML.parse(newAttr.options);
        } catch (err) {
          newAttr.options = {};
        }
      }
      attrs[attr.name] = newAttr;
    });
  }
  return attrs;
}

export default function parseIntents(index: string[], data: any) {
  const intents = {};
  index.forEach(id => {
    const { flow, name, condition, classifier, attributes, ...rest } = data[id];
    let parsedCond = [];
    if (condition && isArray(condition)) {
      parsedCond = condition.map(cond =>
        cond && isFunction(cond.replace) ? cond.replace(/'/g, '"').trim() : cond
      );
    }
    intents[name] = {
      ...rest,
      condition: !isEmpty(parsedCond) ? simplifyArray(parsedCond) : undefined,
      classifier: !isEmpty(classifier)
        ? simplifyArray(parseClassifier(classifier))
        : undefined,
      attributes: !isEmpty(attributes) ? parseAttributes(attributes) : undefined
    };
  });
  return intents;
}
