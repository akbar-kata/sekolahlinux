import isArray from 'lodash-es/isArray';
import isString from 'lodash-es/isString';
import isFunction from 'lodash-es/isFunction';

import { Json } from 'interfaces';
import { BotStore } from 'interfaces/bot';
import parseIntents from './parseIntents';
import parseStates from './parseStates';
import parseActions from './parseActions';
import parseMethods from './parseMethods';

export function transformToArray(data?: Json) {
  if (data) {
    if (isArray(data)) {
      return data;
    }
    return [data];
  }
  return undefined;
}

export function simplifyArray(data?: any[]) {
  if (data && isArray(data) && data.length === 1) {
    return data[0];
  }
  return data;
}

export function trim(value: any) {
  return value && isFunction(value.trim) ? value.trim() : value;
}

export function convertToString(raw: any) {
  if (isString(raw)) {
    return raw;
  }

  try {
    return JSON.stringify(raw);
  } catch (err) {
    return raw.toString();
  }
}

export function getByFlow(state: any, flow?: string): string[] {
  return !flow
    ? []
    : state.index.filter(id => {
        return state.data[id].flow === flow;
      });
}

export default function parseFlow(raw: BotStore) {
  const { flow, intent, node, connection, action, method } = raw;
  const newData = {};
  flow.index.forEach(flowId => {
    const { name, ...rest } = flow.data[flowId];
    newData[name] = {
      ...rest,
      intents: parseIntents(getByFlow(intent, flowId), intent.data),
      states: parseStates(
        getByFlow({ index: node.index, data: node.data }, flowId),
        node.data,
        getByFlow(
          {
            index: connection.index,
            data: connection.data
          },
          flowId
        ),
        connection.data
      ),
      actions: parseActions(getByFlow(action, flowId), action.data),
      methods: parseMethods(getByFlow(method, flowId), method.data)
    };
  });
  return newData;
}
