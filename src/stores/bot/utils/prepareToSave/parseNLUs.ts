import { BotNLUMap } from 'interfaces/bot';
import { JsonObject, Json } from 'interfaces';

function parseProcess(process: string[]) {
  if (process.length === 1) {
    return process[0];
  }
  return process;
}

function parseKeywords(options: JsonObject) {
  const keywords: JsonObject = {};
  if (options.keywords) {
    (options.keywords as any[]).forEach(keyword => {
      keywords[keyword.key] = keyword.words;
    });
  }
  return {
    keywords,
    default: options.default,
    case: options.case,
    exact: options.exact
  };
}

function parseRegex(options: JsonObject) {
  const { index, multiple } = options;
  const newOptions: JsonObject = {
    regex: options.regex
  };
  if (index !== undefined && index !== null) {
    if (multiple === true) {
      const newIndex: JsonObject = {};
      (index as any[]).forEach(idx => {
        newIndex[idx.key] = idx.number;
      });
      newOptions.index = newIndex;
    } else {
      newOptions.index = index;
    }
  }
  return newOptions;
}

function parseMultipleRegex({ regex }: any) {
  const newRegex = {};
  if (regex && regex.length) {
    regex.forEach(item => {
      newRegex[item.key] = item.value;
    });
  }
  return {
    regex: newRegex
  };
}

function parseOptions(type: string, options: JsonObject): any {
  if (options) {
    switch (type) {
      case 'keyword':
        return parseKeywords(options);
      case 'regex':
        return parseRegex(options);
      default:
        return options;
    }
  }
  return undefined;
}

export default function parseNLUs(raw: BotNLUMap) {
  const nlusData = {};
  if (raw) {
    Object.keys(raw).forEach(id => {
      const newData: JsonObject = {
        type: raw[id].type
      };
      if (raw[id].type === 'multipleRegex') {
        newData.type = 'regex';
        newData.options = parseMultipleRegex(raw[id].options || {});
      } else if (raw[id].type === 'method') {
        newData.method = raw[id].options.method;
      } else {
        newData.options = parseOptions(raw[id].type, raw[id].options);
      }
      if (raw[id].process) {
        newData.process = parseProcess(raw[id].process as string[]) as Json;
      }
      nlusData[raw[id].name] = newData;
    });
  }
  return nlusData;
}
