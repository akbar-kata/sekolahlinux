import { ActionMap } from 'interfaces/bot';
import { simplifyArray, trim } from './parseFlows';

function parseText(options: any) {
  const { text, ...rest } = options;
  return {
    type: 'text',
    options: {
      text: trim(simplifyArray(text)),
      ...rest
    }
  };
}

function parseData(options: any) {
  return {
    options,
    type: 'text'
  };
}

function parseImage(options: any) {
  return {
    type: 'template',
    options: {
      type: 'image',
      items: {
        originalContentUrl: trim(options.imageUrl),
        previewImageUrl: trim(options.thumbnailUrl)
      }
    }
  };
}

function parseButton(options: any) {
  return {
    type: 'template',
    options: {
      type: 'button',
      altText: trim(options.altText),
      items: {
        title: trim(options.title),
        text: trim(options.text),
        actions:
          options.actions && options.actions.length
            ? options.actions.map(action => {
                const newAction: any = {
                  type: trim(action.type),
                  label: trim(action.label),
                  text: trim(action.text),
                  url: action.url || undefined
                };
                if (action.payload) {
                  try {
                    if (action.payload && action.payload.length) {
                      const newPayload = {};
                      action.payload.forEach(item => {
                        newPayload[item.key] = item.value;
                      });
                      newAction.payload = newPayload;
                    }
                  } catch (err) {
                    newAction.payload = {};
                  }
                }
                return newAction;
              })
            : undefined
      }
    }
  };
}

function parseDynamicCarousel(options: any) {
  return {
    type: 'template',
    options: {
      type: 'carousel',
      data: options.data,
      template: {
        text: trim(options.text),
        title: trim(options.title),
        thumbnailImageUrl: trim(options.thumbnailImageUrl),
        default_action: options.default_action,
        actions:
          options.actions && options.actions.length
            ? options.actions.map(action => {
                const newAction: any = {
                  type: trim(action.type),
                  label: trim(action.label),
                  url: action.url || undefined
                };
                if (action.payload && action.payload.length) {
                  const newPayload = {};
                  action.payload.forEach(payload => {
                    newPayload[payload.key] = payload.value;
                  });
                  newAction.payload = newPayload;
                }
                return newAction;
              })
            : undefined
      }
    }
  };
}

function parseCarousel(options: any) {
  return {
    type: 'template',
    options: {
      type: 'carousel',
      items:
        options.items && options.items.length
          ? options.items.map(item => ({
              title: trim(item.title),
              text: trim(item.text),
              thumbnailImageUrl: trim(item.thumbnailImageUrl),
              default_action: item.default_action,
              actions:
                item.actions && item.actions.length
                  ? item.actions.map(action => {
                      const newAction: any = {
                        type: trim(action.type),
                        label: trim(action.label),
                        url: action.url || undefined
                      };
                      if (action.payload && action.payload.length) {
                        const newPayload = {};
                        action.payload.forEach(payload => {
                          newPayload[payload.key] = payload.value;
                        });
                        newAction.payload = newPayload;
                      }
                      return newAction;
                    })
                  : undefined
            }))
          : undefined
    }
  };
}

function parseImageMap(options: any) {
  return {
    type: 'template',
    options: {
      type: 'imagemap',
      items: {
        baseUrl: trim(options.baseUrl),
        altText: trim(options.altText),
        baseSize: options.baseSize
          ? {
              width: options.baseSize.width,
              height: options.baseSize.height
            }
          : undefined,
        actions: options.actions
          ? options.actions.map(action => ({
              text: trim(action.text),
              type: trim(action.type),
              label: trim(action.label),
              area: action.area
                ? {
                    x: action.area.x,
                    y: action.area.y,
                    width: action.area.width,
                    height: action.area.height
                  }
                : undefined
            }))
          : undefined
      }
    }
  };
}

function parseSticker(options: any) {
  return {
    type: 'template',
    options: {
      type: 'sticker',
      items: {
        stickerId: trim(options.stickerId),
        packageId: trim(options.packageId)
      }
    }
  };
}

function parseVideo(options: any) {
  return {
    type: 'template',
    options: {
      type: 'video',
      items: {
        originalContentUrl: trim(options.originalContentUrl),
        previewImageUrl: trim(options.previewImageUrl)
      }
    }
  };
}

function parseAudio(options: any) {
  return {
    type: 'template',
    options: {
      type: 'audio',
      items: {
        originalContentUrl: trim(options.originalContentUrl),
        duration: options.duration
      }
    }
  };
}

function parseLocation(options: any) {
  return {
    type: 'template',
    options: {
      type: 'location',
      items: {
        title: trim(options.title),
        address: trim(options.address),
        latitude: options.latitude,
        longitude: options.longitude,
        locationImageUrl: trim(options.locationImageUrl)
      }
    }
  };
}

function parseSchedule({ start, end, message, ...rest }: any) {
  try {
    let metadata: any = undefined;
    if (message && message.metadata && message.metadata.length) {
      metadata = {};
      message.metadata.forEach(meta => {
        metadata[meta.key] = meta.value;
      });
    }
    return {
      type: 'schedule',
      options: {
        ...rest,
        start,
        end,
        message: message
          ? {
              metadata,
              type: trim(message.type),
              content: message.content
            }
          : undefined
      }
    };
  } catch (err) {
    return {
      type: 'schedule'
    };
  }
}

function parseAPI({ body, headers, query, form_data, ...rest }: any) {
  const newData: any = {
    ...rest
  };
  if (body && body.length) {
    const newBody = {};
    body.forEach(item => {
      newBody[item.key] = item.value;
    });
    newData.body = newBody;
  }
  if (headers && headers.length) {
    const newHeaders = {};
    headers.forEach(item => {
      newHeaders[item.key] = item.value;
    });
    newData.headers = newHeaders;
  }
  if (query && query.length) {
    const newQuery = {};
    query.forEach(item => {
      newQuery[item.key] = item.value;
    });
    newData.query = newQuery;
  }
  if (form_data && form_data.length) {
    const newFormData = {};
    form_data.forEach(item => {
      newFormData[item.key] = item.value;
    });
    newData.form_data = newFormData;
  }
  return {
    type: 'api',
    options: newData
  };
}

function parseCommand({ payload, ...rest }: any) {
  const newData: any = {
    ...rest
  };

  if (payload && payload.length) {
    const newPayload = {};
    payload.forEach(item => {
      newPayload[item.key] = item.value;
    });
    newData.payload = newPayload;
  }

  return {
    type: 'command',
    options: newData
  };
}

function parseMethod({ method, options, ...rest }: any) {
  const newOptions = {
    ...rest
  };

  if (options && options.length) {
    options.forEach(item => {
      newOptions[item.key] = item.value;
    });
  }

  return {
    method,
    type: 'method',
    options: newOptions
  };
}

function parseOptions(type: string, options?: any): any {
  if (options) {
    switch (type) {
      case 'text':
        return parseText(options);
      case 'data':
        return parseData(options);
      case 'image':
        return parseImage(options);
      case 'button':
        return parseButton(options);
      case 'dynamicCarousel':
        return parseDynamicCarousel(options);
      case 'carousel':
        return parseCarousel(options);
      case 'imageMap':
        return parseImageMap(options);
      case 'sticker':
        return parseSticker(options);
      case 'video':
        return parseVideo(options);
      case 'audio':
        return parseAudio(options);
      case 'location':
        return parseLocation(options);
      case 'schedule':
        return parseSchedule(options);
      case 'api':
        return parseAPI(options);
      case 'method':
        return parseMethod(options);
      case 'command':
        return parseCommand(options);
      default:
        return {
          type,
          options
        };
    }
  }
  return undefined;
}

export default function parseActions(index: string[], data: ActionMap) {
  const actions = {};

  index.forEach(id => {
    const { flow, name, type, options, ...rest } = data[id];
    const parsed = parseOptions(type, options);

    actions[name] = {
      ...parsed,
      ...rest
    };
  });
  return actions;
}
