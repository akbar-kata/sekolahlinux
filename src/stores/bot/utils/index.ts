import prepareToDisplay from './prepareToDisplay';
import prepareToSave from './prepareToSave';
import cleanMethods from './prepareToDisplay/cleanMethod';

export { prepareToDisplay, cleanMethods, prepareToSave };
