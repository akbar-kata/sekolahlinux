import shortid from 'shortid';

import { MethodMap } from 'interfaces/bot';
import TYPES from './types';

export function fetch(data: MethodMap) {
  return {
    type: TYPES.FETCH,
    payload: {
      data,
      index: Object.keys(data)
    }
  };
}

export function assigns(data: MethodMap) {
  return {
    type: TYPES.ASSIGN,
    payload: data
  };
}

export function create(flow: string, data: any) {
  if (!flow) {
    return;
  }
  return {
    type: TYPES.CREATE,
    payload: {
      [shortid.generate()]: {
        flow,
        name: data.name,
        params: data.params,
        definition: data.definition,
        entry: data.entry,
        mode: data.mode
      }
    } as MethodMap
  };
}

export function update(flow: string, id: string, data: any) {
  if (!flow) {
    return;
  }
  return {
    type: TYPES.UPDATE,
    payload: {
      [id]: {
        flow,
        name: data.name,
        params: data.params,
        definition: data.definition,
        entry: data.entry,
        mode: data.mode
      }
    } as MethodMap
  };
}

export function remove(id: string) {
  return {
    type: TYPES.DELETE,
    payload: {
      id
    }
  };
}
