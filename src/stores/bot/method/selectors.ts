import { MethodStore, MethodMap } from 'interfaces/bot';

/**
 * get all methods data
 *
 * @param {MethodStore} state botstudio MethodMap state tree
 * @return {MethodMap} return state tree as it is
 */
export function getData(state: MethodStore): MethodMap {
  return state.data;
}

/**
 * get all methods indexes
 *
 * @param {MethodStore} state botstudio MethodMap state tree
 * @return {Array} return MethodMap indexes
 */
export function getIndex(state: MethodStore): string[] {
  return state.index;
}

/**
 * get methods index with flow matched
 *
 * @param {MethodStore} state botstudio MethodMap state tree
 * @param {string} flow name flow to be mathed
 * @return {Array} return matched MethodMap indexes
 */
export function getByFlow(
  state: MethodStore,
  flow?: string | null,
  withRoot?: boolean
): string[] {
  function isRoot(flowName: string): boolean {
    const cond = !withRoot ? false : flowName === '__ROOT__';
    return cond;
  }
  return !flow
    ? []
    : state.index.filter(id => {
        return state.data[id].flow === flow || isRoot(state.data[id].flow);
      });
}

/**
 * get duplicate
 */
export function isExist(
  state: MethodStore,
  flow: string,
  name: string,
  id?: string
): boolean {
  const duplicate = state.index.filter(
    idx =>
      state.data[idx].flow === flow &&
      state.data[idx].name === name &&
      idx !== id
  );
  return !!duplicate.length;
}
