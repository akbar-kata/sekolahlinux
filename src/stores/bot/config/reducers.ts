import { ReduxAction } from 'interfaces';
import { DataMap } from 'interfaces/common';
import TYPES from './types';

function configs(state: DataMap<any> = {}, { type, payload }: ReduxAction) {
  switch (type) {
    case TYPES.CLEAR:
      return {};
    case TYPES.ASSIGN:
    case TYPES.SAVE:
      return Object.assign({}, state, payload);
    case TYPES.SET_DRAFT:
      return Object.assign({}, state, {
        draft: true
      });
    case TYPES.REMOVE_DRAFT:
      return Object.assign({}, state, {
        draft: false
      });
    default:
      return state;
  }
}

export default configs;
