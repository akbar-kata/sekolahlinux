import { DataMap } from 'interfaces/common';
import { BotStore } from 'interfaces/bot';

/**
 * get all actions data
 *
 * @param {BotStore} botstudio state tree
 * @return {JsonObject} return state tree as it is
 */
export function getData(state: BotStore): DataMap<any> {
  return state.config;
}
