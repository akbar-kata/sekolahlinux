import { DataMap } from 'interfaces/common';
import TYPES from './types';

export function clear() {
  return {
    type: TYPES.CLEAR
  };
}

export function assign(data: DataMap<any>) {
  return {
    type: TYPES.ASSIGN,
    payload: data
  };
}

export function save(data: DataMap<any>) {
  return {
    type: TYPES.SAVE,
    payload: data
  };
}

export function setDraft() {
  return {
    type: TYPES.SET_DRAFT
  };
}

export function removeDraft() {
  return {
    type: TYPES.REMOVE_DRAFT
  };
}
