export default {
  CLEAR: 'KATA_ML_BOT/CONFIG/CLEAR',
  ASSIGN: 'KATA_ML_BOT/CONFIG/ASSIGN',
  SAVE: 'KATA_ML_BOT/CONFIG/SAVE',
  SET_DRAFT: 'KATA_ML_BOT/CONFIG/DRAFT/ASSIGN',
  REMOVE_DRAFT: 'KATA_ML_BOT/CONFIG/DRAFT/REMOVE/ASSIGN'
};
