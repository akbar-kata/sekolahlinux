import { ActionStore, ActionMap } from 'interfaces/bot';

/**
 * get all actions data
 *
 * @param {ActionStore} state botstudio actions state tree
 * @return {ActionMap} return state tree as it is
 */
export function getData(state: ActionStore): ActionMap {
  return state.data;
}

/**
 * get all actions indexes
 *
 * @param {ActionStore} state botstudio actions state tree
 * @return {Array} return actions indexes
 */
export function getIndex(state: ActionStore): string[] {
  return state.index;
}

/**
 * get actions index with flow matched
 *
 * @param {ActionStore} state botstudio actions state tree
 * @param {string} flow name flow to be mathed
 * @return {Array} return matched actions indexes
 */
export function getByFlow(state: ActionStore, flow?: string | null): string[] {
  return !flow
    ? []
    : state.index.filter(id => {
        return state.data[id].flow === flow;
      });
}

export function getIdByName(
  state: ActionStore,
  name: string,
  flow?: string | null
): string | undefined {
  const filtered = state.index.filter(
    id => state.data[id].flow === flow && state.data[id].name === name
  );
  return filtered.length ? filtered[0] : undefined;
}

/**
 * get duplicate
 */
export function isExist(
  state: ActionStore,
  flow: string,
  name: string,
  id?: string
): boolean {
  const duplicate = state.index.filter(
    idx =>
      state.data[idx].flow === flow &&
      state.data[idx].name === name &&
      idx !== id
  );
  return !!duplicate.length;
}
