import shortid from 'shortid';

import { ActionMap } from 'interfaces/bot';
import TYPES from './types';

export function clear() {
  return {
    type: TYPES.CLEAR
  };
}

export function fetch(data: ActionMap) {
  return {
    type: TYPES.FETCH,
    payload: {
      data,
      index: Object.keys(data)
    }
  };
}

export function create(flow: string, data: any) {
  if (!flow) {
    return;
  }
  const newId = shortid.generate();
  return {
    newId,
    type: TYPES.CREATE,
    name: data.name,
    payload: {
      [newId]: {
        flow,
        name: data.name,
        type: data.type,
        options: data.options
      }
    } as ActionMap
  };
}

export function update(flow: string, id: string, data: any) {
  if (!flow) {
    return;
  }
  return {
    type: TYPES.UPDATE,
    payload: {
      [id]: {
        flow,
        name: data.name,
        type: data.type,
        options: data.options
      }
    } as ActionMap
  };
}

export function remove(id: string) {
  return {
    type: TYPES.DELETE,
    payload: {
      id
    }
  };
}
