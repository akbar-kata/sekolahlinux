import { BotNLUStore, BotNLUMap } from 'interfaces/bot';

/**
 * get all nlus data
 *
 * @param {BotNLUStore} state botstudio nlus state tree
 * @return {BotNLUMap} return state tree as it is
 */
export function getData(state: BotNLUStore): BotNLUMap {
  return state.data;
}

/**
 * get all nlus indexes
 *
 * @param {BotNLUStore} state botstudio nlus state tree
 * @return {Array} return nlus indexes
 */
export function getIndex(state: BotNLUStore): string[] {
  return state.index;
}

/**
 * get duplicate
 */
export function isExist(
  state: BotNLUStore,
  name: string,
  id?: string
): boolean {
  const duplicate = state.index.filter(
    idx => state.data[idx].name === name && idx !== id
  );
  return !!duplicate.length;
}
