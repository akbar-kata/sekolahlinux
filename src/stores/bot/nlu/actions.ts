import shortid from 'shortid';

import { BotNLUMap, BotNLU } from 'interfaces/bot';
import TYPES from './types';

export function fetch(data: BotNLUMap) {
  return {
    type: TYPES.FETCH,
    payload: {
      data,
      index: Object.keys(data)
    }
  };
}

function assignData(data: any): BotNLU {
  return Object.assign(
    {},
    {
      // using object.assign so the undefined will not passed over
      name: data.name,
      type: data.type,
      options: data.options,
      method: data.method,
      process: data.process
    }
  );
}

export function create(data: any) {
  return {
    type: TYPES.CREATE,
    payload: {
      [shortid.generate()]: assignData(data)
    } as BotNLUMap
  };
}

export function update(id: string, data: any) {
  return {
    type: TYPES.UPDATE,
    payload: {
      [id]: assignData(data)
    } as BotNLUMap
  };
}

export function remove(id: string) {
  return {
    type: TYPES.DELETE,
    payload: {
      id
    }
  };
}
