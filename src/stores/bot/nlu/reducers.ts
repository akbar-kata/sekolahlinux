import { combineReducers } from 'redux';

import { ReduxAction } from 'interfaces';
import { BotNLUMap, BotNLUStore } from 'interfaces/bot';
import TYPES from './types';

function data(state: BotNLUMap = {}, { type, payload }: ReduxAction) {
  switch (type) {
    case TYPES.FETCH:
      return Object.assign({}, payload.data);
    case TYPES.CREATE:
    case TYPES.UPDATE:
      return Object.assign({}, state, payload);
    case TYPES.DELETE:
      const { [payload.id]: deletedItem, ...rest } = state;
      return rest;
    default:
      return state;
  }
}

function index(state: string[] = [], { type, payload }: ReduxAction) {
  switch (type) {
    case TYPES.FETCH:
      return [...payload.index];
    case TYPES.CREATE:
      return [...state, ...Object.keys(payload)];
    case TYPES.DELETE:
      return state.filter(id => id !== payload.id);
    default:
      return state;
  }
}

export default combineReducers<BotNLUStore>({
  index,
  data
});
