import { combineReducers } from 'redux';

import { ReduxAction } from 'interfaces';
import { StateNodeMap, StateNodeStore } from 'interfaces/bot';
import TYPES from './types';
import CONN_TYPES from '../connection/types';

function data(state: StateNodeMap = {}, { type, payload }: ReduxAction) {
  switch (type) {
    case TYPES.FETCH:
      return Object.assign({}, payload.data);
    case TYPES.ADD:
    case TYPES.UPDATE:
    case TYPES.CHANGE_POS:
      return Object.assign({}, state, payload);
    case TYPES.DELETE: {
      const { [payload.id]: deletedItem, ...rest } = state;
      return rest;
    }
    case TYPES.ARRANGE: {
      const newStates = { ...state };
      payload.forEach(node => {
        newStates[node.id].style = {
          top: node.top,
          left: node.left
        };
      });
      return newStates;
    }
    default:
      return state;
  }
}

function index(state: string[] = [], { type, payload }: ReduxAction) {
  switch (type) {
    case TYPES.FETCH:
      return [...payload.index];
    case TYPES.ADD:
      return [...state, ...Object.keys(payload)];
    case TYPES.DELETE:
      return state.filter(id => id !== payload.id);
    default:
      return state;
  }
}

function lastUpdated(state: number = Date.now(), { type }: ReduxAction) {
  switch (type) {
    case TYPES.FETCH:
    // case TYPES.ARRANGE:
    case TYPES.ADD:
    case TYPES.UPDATE:
    case TYPES.DELETE:
    case CONN_TYPES.ADD:
    case CONN_TYPES.UPDATE:
    case CONN_TYPES.DELETE:
      return Date.now();
    default:
      return state;
  }
}

export default combineReducers<StateNodeStore>({
  index,
  data,
  lastUpdated
});
