import { StateNodeStore, StateNodeMap } from 'interfaces/bot';

/**
 * get all nodes data
 *
 * @param {StateNodeStore} state botstudio states state tree
 * @return {StateNodeMap} return node tree as it is
 */
export function getData(state: StateNodeStore): StateNodeMap {
  return state.data;
}

/**
 * get all nodes indexes
 *
 * @param {StateNodeStore} state botstudio states state tree
 * @return {Array} return node indexes
 */
export function getIndex(state: StateNodeStore): string[] {
  return state.index;
}

/**
 * get nodes index with flow matched
 *
 * @param {StateNodeStore} state botstudio states state tree
 * @param {string} flow name flow to be mathed
 * @return {Array} return matched StateNodeMap indexes
 */
export function getByFlow(
  state: StateNodeStore,
  flow?: string | null
): string[] {
  return !flow
    ? []
    : state.index.filter(id => {
        return state.data[id].flow === flow;
      });
}

/**
 * get duplicate
 */
export function isExist(
  state: StateNodeStore,
  flow: string,
  name: string,
  id?: string
): boolean {
  const duplicate = state.index.filter(
    idx =>
      state.data[idx].flow === flow &&
      state.data[idx].name === name &&
      idx !== id
  );
  return !!duplicate.length;
}

/**
 * get state last updated
 *
 * @param {StateNodeStore} state botstudio states state tree
 * @return {number} return last updated timestamp
 */
export function getLastUpdated(state: StateNodeStore): number {
  return state.lastUpdated;
}
