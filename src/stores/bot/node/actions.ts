import shortid from 'shortid';

import { StateNodeMap, StateNode } from 'interfaces/bot';
import TYPES from './types';

export function fetch(data: StateNodeMap) {
  return {
    type: TYPES.FETCH,
    payload: {
      data,
      index: Object.keys(data)
    }
  };
}

export function arrage(data: { id: string; top: number; left: number }[]) {
  return {
    type: TYPES.ARRANGE,
    payload: data
  };
}

function assignData(flow: string, id: string, data: any): StateNode {
  return Object.assign(
    {},
    {
      // using object.assign so the undefined will not passed over
      flow,
      id,
      name: data.name,
      initial: data.initial,
      end: data.end,
      action: data.action,
      float: data.float,
      onEnter: data.onEnter,
      onTransit: data.onTransit,
      onExit: data.onExit,
      style: data.style,
      relatedIds: data.relatedIds || [],
      fallback: data.fallback,
      _opts: data._opts
    }
  );
}

export function create(flow: string, data: any) {
  if (!flow) {
    return;
  }
  const id = shortid.generate();
  return {
    type: TYPES.ADD,
    payload: {
      [id]: assignData(flow, id, data)
    } as StateNodeMap
  };
}

export function update(flow: string, id: string, data: any) {
  if (!flow) {
    return;
  }
  return {
    type: TYPES.UPDATE,
    payload: {
      [id]: assignData(flow, id, data)
    } as StateNodeMap
  };
}

/**
 * this action created to prevent rerendering nodes when nodes position changes
 */
export function changePosition(flow: string, id: string, data: any) {
  if (!flow) {
    return;
  }
  return {
    type: TYPES.CHANGE_POS,
    payload: {
      [id]: assignData(flow, id, data)
    } as StateNodeMap
  };
}

export function remove(id: string) {
  return {
    type: TYPES.DELETE,
    payload: {
      id
    }
  };
}
