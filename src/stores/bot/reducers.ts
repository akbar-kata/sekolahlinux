import { combineReducers } from 'redux';

import { ReduxAction } from 'interfaces';
import { Pagination } from 'interfaces/common';
import { BotStore, BotMap } from 'interfaces/bot';
import BOT from './types';

import flow from './flow/reducers';
import intent from './intent/reducers';
import node from './node/reducers';
import connection from './connection/reducers';
import action from './action/reducers';
import method from './method/reducers';
import nlu from './nlu/reducers';
import log from './logs/reducers';
import config from './config/reducers';
import emulator from './emulator/reducers';
import version from './version/reducers';

const selected = (
  state: string | null = null,
  { type, payload }: ReduxAction
) => {
  switch (type) {
    case BOT.SELECT:
      return payload.revisionId || null;
    default:
      return state;
  }
};

const draft = (state: string | null = null, { type, payload }: ReduxAction) => {
  switch (type) {
    case BOT.SET_DRAFT:
      return payload;
    default:
      return state;
  }
};

const index = (state: string[] = [], { type, payload }: ReduxAction) => {
  switch (type) {
    case BOT.FETCH:
      return [...payload.index];
    case BOT.ADD:
      return [...state, ...Object.keys(payload)];
    case BOT.REMOVE:
      return state.filter(id => id !== payload.id);
    default:
      return state;
  }
};

const data = (state: BotMap = {}, { type, payload }: ReduxAction) => {
  switch (type) {
    case BOT.FETCH:
      return Object.assign({}, payload.data);
    case BOT.ADD:
    case BOT.UPDATE:
      return Object.assign({}, state, payload);
    case BOT.REMOVE:
      const { [payload.id]: deletedItem, ...rest } = state;
      return rest;
    default:
      return state;
  }
};

const paginationInit = {
  limit: 10,
  page: 1,
  total: 0
};

const pagination = (
  state: Pagination = paginationInit,
  { type, payload }: ReduxAction
) => {
  switch (type) {
    case BOT.FETCH:
      return payload.pagination;
    default:
      return state;
  }
};

export default combineReducers<BotStore>({
  selected,
  draft,
  index,
  data,
  pagination,
  flow,
  intent,
  node,
  connection,
  action,
  method,
  nlu,
  log,
  config,
  emulator,
  version
});
