import { combineReducers } from 'redux';
import omit from 'lodash-es/omit';

import { ReduxAction } from 'interfaces';
import * as Version from 'interfaces/bot/version';

import VERSION from './types';

const initialState: Version.Store = {
  selected: null,
  latest: '',
  data: [],
  kataml: {},
  lastUpdate: Date.now(),
  loadings: {
    fetch: false,
    kataml: false
  },
  errors: {
    fetch: null,
    kataml: null
  }
};

const loadings = (
  state: Version.Store['loadings'] = initialState.loadings,
  { type, action, payload }: ReduxAction
) => {
  const actionCaps = action ? action.toUpperCase() : 'FETCH';
  switch (type) {
    case VERSION[`${actionCaps}_FETCH`]:
    case VERSION[`${actionCaps}_REQUEST`]:
      return Object.assign({}, state, {
        [action as string]: true
      });
    case VERSION[`${actionCaps}_SUCCESS`]:
    case VERSION[`${actionCaps}_FAIL`]:
      return Object.assign({}, state, {
        [action as string]: false
      });
    default:
      return state;
  }
};

const errors = (
  state: Version.Store['errors'] = initialState.errors,
  { type, action, payload }: ReduxAction
) => {
  const actionCaps = action ? action.toUpperCase() : 'FETCH';
  switch (type) {
    case VERSION.ERROR_CLEAR:
    case VERSION[`${actionCaps}_REQUEST`]:
      return Object.assign({}, state, {
        [action as string]: null
      });
    case VERSION[`${actionCaps}_FAIL`]:
      return Object.assign({}, state, {
        [action as string]: payload.error
      });
    default:
      return state;
  }
};

const latest = (
  state: Version.Store['latest'] = initialState.latest,
  { type, payload }: ReduxAction
) => {
  switch (type) {
    case VERSION.FETCH_SUCCESS:
      return payload.latest;
    default:
      return state;
  }
};

const data = (
  state: Version.Store['data'] = initialState.data,
  { type, payload }: ReduxAction
) => {
  switch (type) {
    case VERSION.FETCH_SUCCESS:
      return [...payload.data];
    default:
      return state;
  }
};

const kataml = (
  state: Version.Store['kataml'] = initialState.kataml,
  { type, payload }: ReduxAction
) => {
  switch (type) {
    case VERSION.KATAML_SUCCESS:
      return Object.assign({}, state, {
        [payload.id]: {
          yaml: payload.yaml,
          json: payload.json
        }
      });
    case VERSION.KATAML_CLEAR:
      const newState = omit(state, [payload.id]);

      return Object.assign({}, newState);
    default:
      return state;
  }
};

const selected = (
  state: Version.Store['selected'] = initialState.selected,
  { type, payload }: ReduxAction
) => {
  switch (type) {
    case VERSION.FETCH_REQUEST:
      return '';
    case VERSION.SELECT:
      return payload;
    default:
      return state;
  }
};

const lastUpdate = (state: number = Date.now(), { type }: ReduxAction) => {
  switch (type) {
    default:
      return state;
  }
};

export default combineReducers<Version.Store>({
  selected,
  kataml,
  latest,
  data,
  loadings,
  errors,
  lastUpdate
});
