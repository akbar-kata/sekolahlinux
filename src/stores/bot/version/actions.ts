import YAML from 'yamljs';
import transform from 'lodash-es/transform';
import isObject from 'lodash-es/isObject';

import { BotVersion } from 'interfaces/bot/version';

import { callApiThunk } from 'stores/services';

import VERSION from './types';

export function clearVersionError(action: string) {
  return {
    action,
    type: VERSION.ERROR_CLEAR
  };
}

export function selectVersion(version: string) {
  return {
    type: VERSION.SELECT,
    payload: version
  };
}

function requestVersion<T>(action: string, payload?: T) {
  return {
    action,
    payload,
    type: VERSION[`${action.toUpperCase()}_REQUEST`]
  };
}

export function versionFail(action: string, error: string) {
  return {
    action,
    type: VERSION[`${action.toUpperCase()}_FAIL`],
    payload: {
      error
    }
  };
}

export function fetchVersionRequest(botId: string) {
  return requestVersion<{ botId: string }>('fetch', { botId });
}

export function fetchVersionSuccess(data: BotVersion[], latest: string) {
  return {
    type: VERSION.FETCH_SUCCESS,
    action: 'fetch',
    payload: {
      latest,
      data
    }
  };
}

function deepOmit(kataml: any, keysToOmit: string | string[]) {
  const keysToOmitIndex = Array.isArray(keysToOmit) ? keysToOmit : [keysToOmit];

  const omitFromObject = (object: any) => {
    // the inner function which will be called recursivley
    return transform(object, (result, value, key) => {
      // transform to a new object
      if (keysToOmitIndex.indexOf(key) !== -1) {
        // if the key is in the index skip it
        return;
      }

      // if the key is an object run it through the inner function - omitFromObject
      result[key] = isObject(value) ? omitFromObject(value) : value;
    });
  };

  return omitFromObject(kataml); // return the inner function result
}

export function fetchKataMLRequest(botId: string, version: string) {
  return function(dispatch: Function) {
    dispatch({
      type: 'VERSION/KATAML_FETCH',
      action: 'kataml',
      payload: {
        botId,
        version
      }
    });

    return dispatch(
      callApiThunk('get', `/projects/${botId}/bot/revisions/${version}`)
    ).then(response => {
      const katamlNonStyle = deepOmit(response, 'style');
      const kataml = {
        botId,
        version,
        yaml: YAML.stringify(katamlNonStyle, Infinity, 4),
        json: JSON.stringify(katamlNonStyle, null, 2)
      };

      dispatch(
        fetchVersionKataMLSuccess(
          botId,
          version,
          YAML.stringify(katamlNonStyle, Infinity, 4),
          JSON.stringify(katamlNonStyle, null, 2)
        )
      );

      return { kataml };
    });
  };
}

export function fetchVersionKataMLRequest(botId: string, version: string) {
  return requestVersion<{ botId: string; version: string }>('kataml', {
    botId,
    version
  });
}

export function fetchVersionKataMLSuccess(
  botId: string,
  version: string,
  yaml: string,
  json: string
) {
  return {
    type: VERSION.KATAML_SUCCESS,
    action: 'kataml',
    payload: {
      yaml,
      json,
      id: `${botId}`
    }
  };
}

export function clearVersionKataML(botId: string, version: string) {
  return {
    type: VERSION.KATAML_CLEAR,
    payload: {
      id: `${botId}`
    }
  };
}
