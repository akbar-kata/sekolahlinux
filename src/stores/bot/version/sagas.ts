import { all, call, fork, put, takeLatest } from 'redux-saga/effects';
import YAML from 'yamljs';
import get from 'lodash-es/get';

import { ReduxAction } from 'interfaces';

import { callApi } from 'stores/services';
import BOT from 'stores/bot/types';

import VERSION from './types';
import {
  versionFail,
  selectVersion,
  fetchVersionRequest,
  fetchVersionSuccess,
  fetchVersionKataMLSuccess
} from './actions';

function* requestFetchVersion({ payload }: ReduxAction) {
  yield put(fetchVersionRequest(payload));
}

function* fetchVersion({ payload: { botId } }: ReduxAction) {
  try {
    const resp = yield call(callApi, 'get', `/bots/${botId}/versions`);
    const data = resp || {
      versions: {
        data: []
      },
      latest: ''
    };
    yield put(fetchVersionSuccess(data.versions.data || [], data.latest));
    if (data.versions.data.length > 0) {
      const latestVersion = data.versions.data.filter(
        version => version.tag === 'latest'
      );
      yield put(selectVersion(latestVersion[0].version));
    }
  } catch (err) {
    const errorMessage = get(
      err,
      'response.data.message',
      'Oops.. something wrong'
    );
    yield put(versionFail('fetch', errorMessage));
  }
}

function* fetchKataML({ payload: { botId, version } }: ReduxAction) {
  try {
    const data = yield call(
      callApi,
      'get',
      `/bots/${botId}:${version.replace('-latest', '')}`
    );
    yield put(
      fetchVersionKataMLSuccess(
        botId,
        version,
        YAML.stringify(data, Infinity, 4),
        JSON.stringify(data, null, 2)
      )
    );
  } catch (err) {
    const errorMessage = get(
      err,
      'response.data.message',
      'Oops.. something wrong'
    );
    yield put(versionFail('kataml', errorMessage));
  }
}

function* watchBotSelected() {
  yield takeLatest(BOT.SELECT, requestFetchVersion);
}

function* watchVersionFetch() {
  yield takeLatest(VERSION.FETCH_REQUEST, fetchVersion);
}

function* watchKataMLVersionFetch() {
  yield takeLatest(VERSION.KATAML_REQUEST, fetchKataML);
}

function* versionFlow() {
  yield all([
    fork(watchBotSelected),
    fork(watchVersionFetch),
    fork(watchKataMLVersionFetch)
  ]);
}

export default versionFlow;
