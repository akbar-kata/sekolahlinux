import * as Version from 'interfaces/bot/version';

export function getVersionLoading(state: Version.Store, type: string): boolean {
  return state.loadings[type];
}

export function getVersionError(
  state: Version.Store,
  type: string
): string | null {
  return state.errors[type];
}

export function getVersionSelected(
  state: Version.Store
): Version.BotVersion | null {
  return state.selected;
}

export function getVersionLatest(state: Version.Store): string {
  return state.latest;
}

export function getVersionLastUpdated(state: Version.Store): number {
  return state.lastUpdate;
}

export function getVersionData(state: Version.Store): Version.BotVersion[] {
  return state.data;
}

export function getVersionKataML(
  state: Version.Store,
  botId: string
): { yaml: string; json: string } | undefined {
  return state.kataml[`${botId}`];
}
