import {
  all,
  call,
  fork,
  put,
  select,
  takeLatest,
  throttle
} from 'redux-saga/effects';
import get from 'lodash-es/get';
import RootStore from 'interfaces/rootStore';
import { DataMap } from 'interfaces/common';
import { ReduxAction } from 'interfaces';
import BOT from './types';
import KATA_BOT_CONFIG from './config/types';
import { PROJECT_TYPES } from 'stores/project/types';
import { getPreparedBot } from 'stores/bot/selectors';
import { callApi } from 'stores/services';
import { addNotification } from 'stores/app/notification/actions';
import { closeDrawer } from 'stores/app/drawer/actions';
import { closeModal } from 'stores/app/modal/actions';
import { setLoading } from 'stores/app/loadings/action';

import * as BotAction from './actions';
import * as ConfigsActions from './config/actions';
import * as FlowsAction from './flow/actions';
import * as IntentsAction from './intent/actions';
import * as NodeAction from './node/actions';
import * as ConnectionAction from './connection/actions';
import * as ActionsAction from './action/actions';
import * as MethodsAction from './method/actions';
import * as NLUsActions from './nlu/actions';

import { prepareToDisplay, cleanMethods } from './utils';
import truncateRevId from 'modules/deployments/utils/truncateRevId';
import Emulator from './emulator/sagas';
import LogErrorsSagas from './logs/errors/sagas';

/**
 * get bot detail flow, intent, state, action, method, nlu or config
 */
function* fetchBot({ payload }: ReduxAction): DataMap<any> {
  let parsed: any = {};
  try {
    yield put(setLoading(BOT.DETAIL, true));
    let raw: any = {};
    let draft: boolean;
    try {
      raw = yield call(
        callApi,
        'get',
        `/projects/${payload.projectId}/bot/draft`
      );
      draft = true;
    } catch (err) {
      raw = yield call(callApi, 'get', `/projects/${payload.projectId}/bot`);
      draft = false;
    }
    const striped = cleanMethods(raw);
    parsed = prepareToDisplay(striped);
    yield put(FlowsAction.fetch(parsed.flows || {}));
    yield put(IntentsAction.fetch(parsed.intents || {}));
    yield put(NodeAction.fetch(parsed.nodes || {}));
    yield put(ConnectionAction.fetch(parsed.connections || {}));
    yield put(ActionsAction.fetch(parsed.actions || {}));
    yield put(MethodsAction.fetch(parsed.methods || {}));
    yield put(NLUsActions.fetch(parsed.nlus || {}));
    yield put(ConfigsActions.assign(parsed.config));
    yield put(BotAction.setDraft(draft));
    yield put(setLoading(BOT.DETAIL, false));
  } catch (err) {
    const errorMessage = get(
      err,
      'response.data.message',
      'Oops... something went wrong.'
    );
    yield put(
      addNotification({
        title: 'Error, failed to fetch bot',
        message: errorMessage,
        status: 'error'
      })
    );
  }
}

function* watchBotFetch() {
  yield takeLatest(BOT.SELECT, fetchBot);
}

function* addBot({ payload: { data } }: ReduxAction) {
  try {
    const PROJECT_ID = yield select(
      (store: RootStore) => store.project.selected
    );
    const defaultFlow = {
      default: {
        active: true,
        fallback: true,
        intents: {
          defaultIntent: { type: 'text', initial: false, fallback: true }
        },
        states: {
          init: {
            action: 'sayDontUnderstand',
            transitions: {
              init: {
                fallback: true
              }
            },
            initial: true,
            end: true,
            style: { top: 150, left: 150 }
          }
        },
        actions: {
          sayDontUnderstand: {
            type: 'text',
            options: { text: 'Maaf saya belum mengerti :(' }
          }
        },
        methods: {}
      }
    };
    const { id, name, desc, timezone, flows, ...rest } = data;
    const resp = yield call(
      callApi,
      'post',
      `/projects/${PROJECT_ID}/bot/revisions/`,
      {
        name,
        desc,
        timezone,
        flows: flows || defaultFlow,
        ...rest
      }
    );
    yield put(BotAction.addBotSuccess(resp));
    yield put(
      addNotification({
        title: `${data.name} Bot Created`,
        message: `You have successfully created ${
          data.name
        } Bot. Click on the card to enter the Bot.`,
        status: 'success'
      })
    );
    yield put(closeDrawer('NewBotForm'));
  } catch (err) {
    const errorMessage = get(
      err,
      'response.data.message',
      'Oops... something went wrong.'
    );
    yield put(
      addNotification({
        title: 'Failed to Create Bot',
        message: errorMessage,
        status: 'error'
      })
    );
  }
}

function* watchBotAdd() {
  yield takeLatest(BOT.ADD_REQUEST, addBot);
}

function* updateBot({
  payload: { revisionId, id, increment, data }
}: ReduxAction) {
  try {
    yield put(setLoading(BOT.UPDATE_REQUEST, true));
    const PROJECT_ID = yield select(
      (store: RootStore) => store.project.selected
    );
    const PROJECT = yield select(
      (store: RootStore) => store.project.projects[PROJECT_ID]
    );
    const USER_NAME = yield select((store: RootStore) =>
      store.auth.selected ? store.auth.selected.username : '-'
    );
    const resp = yield call(
      callApi,
      'put',
      `/projects/${PROJECT_ID}/bot/revisions/${PROJECT.botLatestRevision}`,
      {
        id,
        name: USER_NAME,
        projectName: PROJECT.name,
        desc: data.desc,
        timezone: data.timezone,
        flows: data.flows,
        nlus: data.nlus,
        methods: data.methods,
        config: data.config,
        author: data.author || '',
        changelog: data.changelog || ''
      }
    );
    yield put(BotAction.updateBotSuccess(id, resp));
    yield put(BotAction.setDraft(false));
    yield put(setLoading(BOT.UPDATE_REQUEST, true));
    if (increment) {
      yield put(
        addNotification({
          title: 'Publish Successful',
          message: `Revision <samp>${truncateRevId(
            resp.revision
          )}</samp> successfully created.`,
          status: 'success',
          allowHTML: true
        })
      );
    } else {
      yield put(
        addNotification({
          title: 'Update Successful',
          message: 'Bot configuration successfully updated.',
          status: 'success'
        })
      );
    }
    yield put(setLoading(BOT.UPDATE_REQUEST, false));
    yield put(closeModal('bot-publish-revision'));
  } catch (err) {
    const errorMessage = get(
      err,
      'response.data.message',
      'Oops... something went wrong.'
    );
    yield put(
      addNotification({
        title: increment ? 'Publish Failed' : 'Update Failed',
        message: errorMessage,
        status: 'error'
      })
    );

    yield put(setLoading(BOT.UPDATE_REQUEST, false));
    yield put(closeModal('bot-publish-revision'));
  }
}

function* watchBotUpdate() {
  yield takeLatest(BOT.UPDATE_REQUEST, updateBot);
}

function* reqSaveDraft() {
  try {
    const selectedBot = yield select(
      (store: RootStore) => store.project.selected
    );
    if (selectedBot) {
      const prepared = yield select((state: RootStore) =>
        getPreparedBot(state.bot)
      );
      const draft = yield select(({ bot }: RootStore) => bot.config.draft);
      const dataToSave = {
        id: selectedBot,
        name: prepared.name,
        desc: prepared.desc,
        timezone: prepared.timezone,
        flows: prepared.flows,
        nlus: prepared.nlus,
        methods: prepared.methods,
        config: prepared.config
      };
      yield put(setLoading(BOT.SAVE_DRAFT_REQUEST, true));
      yield put(
        BotAction.saveDraftRequest(
          selectedBot,
          !draft ? 'create' : 'update',
          dataToSave
        )
      );
    }
  } catch (err) {
    //
  }
}

function* saveDraft({ payload: { id, type, data, notif } }: ReduxAction) {
  try {
    if (type === 'create') {
      yield call(callApi, 'put', `/projects/${id}/bot/draft`, data);
    } else {
      yield call(callApi, 'post', `/projects/${id}/bot/draft`, data);
    }
    yield put(BotAction.saveDraftSuccess());
    yield put(setLoading(BOT.SAVE_DRAFT_REQUEST, false));
    yield put(BotAction.setDraft(true));
    if (notif) {
      yield put(
        addNotification({
          title: 'Success Save to Draft',
          message: 'You have successfully save bot to draft.',
          status: 'success',
          dismissible: true,
          dismissAfter: 5000
        })
      );
    }
  } catch (err) {
    const errorMessage = get(
      err,
      'response.data.message',
      'Oops.. something wrong'
    );
    yield put(
      addNotification({
        title: 'Failed save to Draft',
        message: `Ooops... Something went wrong! You failed to save Bot to Draft.`,
        status: 'error',
        dismissible: true,
        dismissAfter: 5000
      })
    );
    yield put(BotAction.botFail('save_draft', errorMessage));
  }
}

function* watchDraftSaveReq() {
  const regex = /^KATA_ML2(?!.*?.(FETCH|ASSIGN|CLEAR|REQUEST|ERROR|PERMANENT_DELETE)$).*$/g;
  yield throttle(15000, action => regex.test(action.type), reqSaveDraft);
}

function* watchConfigRequest() {
  yield takeLatest(KATA_BOT_CONFIG.SAVE, reqSaveDraft);
}

function* watchDraftSave() {
  yield throttle(15000, BOT.SAVE_DRAFT_REQUEST, saveDraft);
}

function* selectProjectHandler({ payload }: ReduxAction) {
  const projects = yield select((state: RootStore) => state.project.projects);
  const selectedProject = projects[payload];
  if (selectedProject) {
    yield put(BotAction.selectBot(payload, selectedProject.botLatestRevision));
  }
}

function* watchProjectSelected() {
  yield takeLatest(PROJECT_TYPES.SELECT_PROJECT, selectProjectHandler);
}

function* discardDraft() {
  const selectedProject = yield select(
    (state: RootStore) => state.project.selected
  );
  const botLatestRevision = yield select(
    (state: RootStore) =>
      state.project.projects[selectedProject].botLatestRevision
  );
  yield put(setLoading(BOT.DISCARD_DRAFT_REQUEST, true));
  if (selectedProject) {
    try {
      yield call(callApi, 'delete', `/projects/${selectedProject}/bot/draft`);
      yield put(setLoading(BOT.DISCARD_DRAFT_REQUEST, false));
      yield put(BotAction.setDraft(false));
      yield put(closeModal('bot-discard-draft'));
      yield put(BotAction.selectBot(selectedProject, botLatestRevision));
      yield put(
        addNotification({
          title: 'Discard Draft Success',
          message:
            'You have successfully discard draft & back to the last revision list.',
          status: 'success',
          dismissible: true,
          dismissAfter: 5000
        })
      );
    } catch (err) {
      yield put(closeModal('bot-discard-draft'));
      yield put(setLoading(BOT.DISCARD_DRAFT_REQUEST, false));
      const errorMessage = get(
        err,
        'response.data.message',
        'Oops.. something wrong'
      );
      yield put(
        addNotification({
          title: 'Failed to Discard Draft',
          message: errorMessage,
          status: 'error',
          dismissible: true,
          dismissAfter: 5000
        })
      );
    }
  }
}

function* watchDiscardDraft() {
  yield takeLatest(BOT.DISCARD_DRAFT_REQUEST, discardDraft);
}

function* botFlow() {
  yield all([
    fork(watchProjectSelected),
    fork(watchBotFetch),
    fork(watchBotAdd),
    fork(watchBotUpdate),
    fork(watchDraftSave),
    fork(watchDraftSaveReq),
    fork(LogErrorsSagas),
    fork(watchConfigRequest),
    fork(watchDiscardDraft),
    fork(Emulator)
  ]);
}

export default botFlow;
