import { FlowStore, FlowMap } from 'interfaces/bot';

/**
 * get all actions data
 *
 * @param {FlowStore} state botstudio flows state tree
 * @return {FlowMap} return state tree as it is
 */
export function getData(state: FlowStore): FlowMap {
  return state.data;
}

/**
 * get all actions indexes
 *
 * @param {FlowStore} state botstudio flows state tree
 * @return {Array} return actions indexes
 */
export function getIndex(state: FlowStore): string[] {
  return state.index;
}

/**
 * get selected flow
 *
 * @param {FlowStore} state botstudio flows state tree
 * @return {string | undefined} return flow id if selected and undefined otherwise
 */
export function getSelected(state: FlowStore): string | undefined | null {
  return state.selected;
}

/**
 * get duplicate
 */
export function isExist(state: FlowStore, name: string, id?: string): boolean {
  const duplicate = state.index.filter(
    idx => state.data[idx].name === name && idx !== id
  );
  return !!duplicate.length;
}
