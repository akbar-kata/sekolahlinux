import shortid from 'shortid';

import { FlowMap } from 'interfaces/bot';
import TYPES from './types';

export function clear() {
  return {
    type: TYPES.CLEAR
  };
}

export function fetch(data: FlowMap) {
  return {
    type: TYPES.FETCH,
    payload: {
      data,
      index: Object.keys(data)
    }
  };
}

export function select(id: string) {
  return {
    type: TYPES.SELECT,
    payload: {
      id
    }
  };
}

export function swap(from: number, to: number) {
  return {
    type: TYPES.SWAP,
    payload: {
      from,
      to
    }
  };
}

export function create(data: any) {
  return {
    type: TYPES.CREATE,
    payload: {
      [shortid.generate()]: {
        name: data.name,
        active: data.active,
        expire: data.expire,
        volatile: data.volatile,
        fallback: data.fallback
      }
    } as FlowMap
  };
}

export function update(id: string, data: any) {
  return {
    type: TYPES.UPDATE,
    payload: {
      [id]: {
        name: data.name,
        active: data.active,
        expire: data.expire,
        volatile: data.volatile,
        fallback: data.fallback
      }
    } as FlowMap
  };
}

export function remove(id: string) {
  return {
    type: TYPES.DELETE,
    payload: {
      id
    }
  };
}
