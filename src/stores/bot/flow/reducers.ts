import { combineReducers } from 'redux';

import { ReduxAction } from 'interfaces';
import { FlowMap, FlowStore } from 'interfaces/bot';
import TYPES from './types';

function data(state: FlowMap = {}, { type, payload }: ReduxAction) {
  switch (type) {
    case TYPES.CLEAR:
      return {};
    case TYPES.FETCH:
      return Object.assign({}, payload.data);
    case TYPES.CREATE:
    case TYPES.UPDATE:
      return Object.assign({}, state, payload);
    case TYPES.DELETE:
      const { [payload.id]: deletedItem, ...rest } = state;
      return rest;
    default:
      return state;
  }
}

function index(state: string[] = [], { type, payload }: ReduxAction) {
  switch (type) {
    case TYPES.CLEAR:
      return [];
    case TYPES.FETCH:
      return [...payload.index];
    case TYPES.SWAP: {
      const { from, to } = payload;
      if (state[from] === undefined || state[to] === undefined) {
        return state;
      }
      const a = from > to ? to : from;
      const b = from > to ? from : to;
      return [
        ...state.slice(0, a),
        state[b],
        ...state.slice(a + 1, b),
        state[a],
        ...state.slice(b + 1)
      ];
    }
    case TYPES.CREATE:
      return [...state, ...Object.keys(payload)];
    case TYPES.DELETE:
      return state.filter(id => id !== payload.id);
    default:
      return state;
  }
}

function selected(state: string | null = null, { type, payload }: ReduxAction) {
  switch (type) {
    case TYPES.CLEAR:
      return null;
    case TYPES.FETCH:
      return payload.index.length > 0 ? payload.index[0] : false;
    case TYPES.SELECT:
      return payload.id;
    case TYPES.DELETE: {
      if (state === payload.id) {
        return null;
      }

      return state;
    }
    default:
      return state;
  }
}

export default combineReducers<FlowStore>({
  selected,
  index,
  data
});
