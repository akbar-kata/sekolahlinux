import { StateConnectionStore, StateConnectionMap } from 'interfaces/bot';

/**
 * get all data data
 *
 * @param {StateConnectionStore} state botstudio states state tree
 * @return {StateConnectionMap} return data tree as it is
 */
export function getData(state: StateConnectionStore): StateConnectionMap {
  return state.data;
}

/**
 * get all data indexes
 *
 * @param {StateConnectionStore} state botstudio states state tree
 * @return {Array} return data indexes
 */
export function getIndex(state: StateConnectionStore): string[] {
  return state.index;
}

/**
 * get data index with flow matched
 *
 * @param {StateConnectionStore} state botstudio states state tree
 * @param {string} flow name flow to be mathed
 * @return {Array} return matched StateConnectionMap indexes
 */
export function getByFlow(
  state: StateConnectionStore,
  flow?: string | null
): string[] {
  return !flow
    ? []
    : state.index.filter(id => {
        return state.data[id].flow === flow;
      });
}
