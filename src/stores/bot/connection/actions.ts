import shortid from 'shortid';

import Store from 'interfaces/rootStore';
import { StateConnectionMap, StateConnection } from 'interfaces/bot';
import TYPES from './types';

export function fetch(raw: StateConnectionMap) {
  return {
    type: TYPES.FETCH,
    payload: {
      data: raw,
      index: Object.keys(raw)
    }
  };
}

function assignData(flow: string, id: string, data: any): StateConnection {
  return Object.assign(
    {},
    {
      // using object.assign so the undefined will not passed over
      flow,
      id,
      from: data.from,
      to: data.to,
      transition: data.transition,
      _opts: data._opts
    }
  );
}

export function create(flow: string, data: any) {
  if (!flow) {
    return;
  }
  const id = shortid.generate();
  return {
    type: TYPES.ADD,
    payload: {
      [id]: assignData(flow, id, data)
    } as StateConnectionMap
  };
}

export function update(flow: string, id: string, data: any) {
  if (!flow) {
    return;
  }
  return {
    type: TYPES.UPDATE,
    payload: {
      [id]: assignData(flow, id, data)
    } as StateConnectionMap
  };
}

export function remove(id: string) {
  return {
    type: TYPES.DELETE,
    payload: {
      id
    }
  };
}

/**
 * delete all connection which related to some node
 * @param {string} related  node id
 */
export function removeRelated(flow: string, related: string) {
  if (!flow) {
    return;
  }
  return (dispatch: Function, getState: Function) => {
    const {
      bot: {
        connection: { data, index }
      }
    }: Store = getState();
    index
      .filter((id: any) => {
        return (
          flow === data[id].flow &&
          (related === data[id].from || related === data[id].to)
        );
      })
      .forEach((id: any) => dispatch(remove(id)));
  };
}
