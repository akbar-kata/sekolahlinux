import * as UserCheck from 'interfaces/check';

export function getCheckUserLoading(
  state: UserCheck.Store,
  type: string
): boolean {
  return state.loadings[type];
}

export function getCheckUserError(
  state: UserCheck.Store,
  type: string
): string | null {
  return state.errors[type];
}

export function getDataUserCheck(state: UserCheck.Store) {
  return state.data;
}
