import ANALYTIC_ROOT from './types';
export function activateAnalytic(
  botId: string,
  deploymentId: string,
  type: 'basic' | 'advanced',
  payload: string[]
) {
  return {
    type: ANALYTIC_ROOT.ACTIVATE_ANALYTIC_REQUEST,
    payload: {
      botId,
      deploymentId,
      type,
      metrics: payload
    }
  };
}
export function checkAnalyticEnabled(botId: string, deploymentId: string) {
  return {
    type: ANALYTIC_ROOT.CHECK_ANALYTIC_ENABLED_REQUEST,
    payload: {
      botId,
      deploymentId
    }
  };
}
