import { takeLatest, call, all, fork, put, select } from 'redux-saga/effects';
import { ReduxAction, TypedReduxAction } from 'interfaces';

import RootStore from 'interfaces/rootStore';
import { ActivateAnalyticsRequestAction } from 'interfaces/analytic';

import { callApi } from 'stores/services';
import { addNotification } from 'stores/app/notification/actions';
import DEPLOYMENT from 'stores/deployment/types';

import ANALYTIC_ROOT from './types';

function* checkAnalyticsEnabled(action: ReduxAction) {
  const { botId, deploymentId } = yield select((state: RootStore) => {
    return {
      botId: state.bot.selected,
      deploymentId: state.deployment.selected
    };
  });
  if (botId && deploymentId) {
    try {
      const encodedDeploymentId = encodeURIComponent(deploymentId);
      const res = yield call(
        callApi,
        'get',
        `/analytics/${botId}/${encodedDeploymentId}/analytic_plan`
      );
      yield put({
        type: ANALYTIC_ROOT.CHECK_ANALYTIC_ENABLED_SUCCESS,
        payload: res
      });
    } catch (e) {
      yield put({
        type: ANALYTIC_ROOT.CHECK_ANALYTIC_ENABLED_FAIL,
        payload: e
      });
    }
  }
}

function* activateAnalytic({
  payload
}: TypedReduxAction<ActivateAnalyticsRequestAction>) {
  try {
    yield call(
      callApi,
      'post',
      `/analytics/${payload.botId}/${payload.deploymentId}/analytic_plan`,
      {
        metrics: payload.metrics
      }
    );

    const toastTitle = `${
      payload.type === 'advanced' ? 'Advanced' : 'Basic'
    } Analytics Enabled`;
    const toastMessage = `You have successfully enabled ${
      payload.type === 'advanced' ? 'advanced' : 'basic'
    } analytics.`;

    yield put({
      type: ANALYTIC_ROOT.ACTIVATE_ANALYTIC_SUCCESS,
      payload: payload.metrics
    });
    yield put(
      addNotification({
        title: toastTitle,
        message: toastMessage,
        status: 'success',
        dismissible: true,
        dismissAfter: 5000
      })
    );
  } catch (e) {
    const toastTitle = `Failed to Enable ${
      payload.type === 'advanced' ? 'Advanced' : 'Basic'
    } Analytics`;
    const toastMessage = `Oops... something went wrong! You failed to enable ${
      payload.type === 'advanced' ? 'advanced' : 'basic'
    } analytics.`;

    yield put(
      addNotification({
        title: toastTitle,
        message: toastMessage,
        status: 'error',
        dismissible: true,
        dismissAfter: 5000
      })
    );
    yield put({
      type: ANALYTIC_ROOT.ACTIVATE_ANALYTIC_FAIL,
      payload: e
    });
  }
}

function* watchCheckAnalyticsEnabled() {
  yield takeLatest(
    ANALYTIC_ROOT.CHECK_ANALYTIC_ENABLED_REQUEST,
    checkAnalyticsEnabled
  );
}

function* watchSelectedDeploymentChanged() {
  yield takeLatest(DEPLOYMENT.SELECT, checkAnalyticsEnabled);
}

function* watchActivateAnalytics() {
  yield takeLatest(ANALYTIC_ROOT.ACTIVATE_ANALYTIC_REQUEST, activateAnalytic);
}

export default function*() {
  yield all([
    fork(watchCheckAnalyticsEnabled),
    fork(watchActivateAnalytics),
    fork(watchSelectedDeploymentChanged)
  ]);
}
