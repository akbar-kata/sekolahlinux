import * as Root from 'interfaces/analytic/root';

export function getAnalyticMetricsEnabled(state: Root.Store): string[] {
  return state.metrics;
}
