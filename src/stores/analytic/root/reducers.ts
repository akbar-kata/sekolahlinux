import { ReduxAction } from 'interfaces';
import ANALYTIC_ROOT from './types';
import { Store } from 'interfaces/analytic/root';

const storeDefaults: Store = {
  metrics: []
};

const root = (state: Store = storeDefaults, action: ReduxAction) => {
  switch (action.type) {
    case ANALYTIC_ROOT.ACTIVATE_ANALYTIC_SUCCESS:
      return { ...state, metrics: action.payload };
    case ANALYTIC_ROOT.CHECK_ANALYTIC_ENABLED_SUCCESS:
      return { ...state, metrics: action.payload };
    case ANALYTIC_ROOT.CHECK_ANALYTIC_ENABLED_FAIL:
      return { ...state, metrics: [] };
    default:
      return state;
  }
};

export default root;
