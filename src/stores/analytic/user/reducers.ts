import { combineReducers } from 'redux';

import { ReduxAction, JsonArray } from 'interfaces';
import USER from './types';
import * as User from 'interfaces/analytic/user';

const initialState: User.Store = {
  isLoading: false,
  error: null,
  data: []
};

const isLoading = (
  state: boolean = initialState.isLoading,
  { type, payload }: ReduxAction
) => {
  switch (type) {
    case USER.REQUEST:
      return true;
    case USER.SUCCESS:
    case USER.FAIL:
      return false;
    default:
      return state;
  }
};

const error = (
  state: string | null = initialState.error,
  { type, payload }: ReduxAction
) => {
  switch (type) {
    case USER.ERROR_CLEAR:
    case USER.REQUEST:
      return null;
    case USER.FAIL:
      return payload.error;
    default:
      return state;
  }
};

const data = (
  state: JsonArray = initialState.data,
  { type, payload }: ReduxAction
) => {
  switch (type) {
    case USER.SUCCESS:
      return [...payload];
    default:
      return state;
  }
};

export default combineReducers({
  isLoading,
  error,
  data
});
