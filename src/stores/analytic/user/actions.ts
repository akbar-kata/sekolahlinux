import { JsonArray } from 'interfaces';
import USER from './types';

export function clearUserError(action: string) {
  return {
    action,
    type: USER.ERROR_CLEAR
  };
}

interface UserRequestParams {
  botId: string;
  deployment: string;
  channel?: string;
  start: number;
  end: number;
}

export function userRequest({
  botId,
  deployment,
  channel,
  start,
  end
}: UserRequestParams) {
  return {
    type: USER.REQUEST,
    payload: {
      botId,
      deployment,
      channel,
      start,
      end
    }
  };
}

export function userSuccess(data: JsonArray) {
  return {
    type: USER.SUCCESS,
    payload: data
  };
}

export function userFail(error: string) {
  return {
    type: USER.FAIL,
    payload: {
      error
    }
  };
}
