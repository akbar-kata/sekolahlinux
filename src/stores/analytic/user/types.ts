const USER = {
  ERROR_CLEAR: 'ANALYTIC/USER/ERROR_CLEAR',
  REQUEST: 'ANALYTIC/USER/REQUEST',
  FAIL: 'ANALYTIC/USER/FAIL',
  SUCCESS: 'ANALYTIC/USER/SUCCESS'
};

export default USER;
