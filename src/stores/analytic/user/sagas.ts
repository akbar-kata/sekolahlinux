import { all, call, fork, put, takeLatest } from 'redux-saga/effects';
import get from 'lodash-es/get';

import { userFail, userSuccess } from './actions';
import { callApi } from 'stores/services';
import { ReduxAction } from 'interfaces';
import USER from './types';

function* fethUser({ payload }: ReduxAction) {
  try {
    const data = yield call(
      callApi,
      'get',
      `/analytics/${payload.botId}/${payload.deployment}/users?startTimestamp=${
        payload.start
      }&endTimestamp=${payload.end}${
        payload.channel ? `&channelId=${payload.channel}` : ''
      }`
    );
    yield put(userSuccess(data));
  } catch (err) {
    const errorMessage = get(
      err,
      'response.data.message',
      'Oops... something went wrong'
    );
    yield put(userFail(errorMessage));
  }
}

function* watchUserFetch() {
  yield takeLatest(USER.REQUEST, fethUser);
}

function* userFlow() {
  yield all([fork(watchUserFetch)]);
}

export default userFlow;
