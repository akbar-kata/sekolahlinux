import { JsonArray } from 'interfaces';
import * as User from 'interfaces/analytic/user';

export function getAnalyticUserLoading(state: User.Store): boolean {
  return state.isLoading;
}

export function getAnalyticUserError(state: User.Store): string | null {
  return state.error;
}

export function getAnalyticUserData(state: User.Store): JsonArray {
  return state.data;
}
