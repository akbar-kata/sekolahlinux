import { JsonArray } from 'interfaces';
import * as Transcript from 'interfaces/analytic/transcript';

export function getAnalyticTranscriptLoading(
  state: Transcript.Store,
  type: string
): boolean {
  return state.loadings[type];
}

export function getAnalyticTranscriptError(
  state: Transcript.Store,
  type: string
): string | null {
  return state.errors[type];
}

export function getAnalyticTranscriptList(state: Transcript.Store): JsonArray {
  return state.list;
}

export function getAnalyticTranscriptConversation(
  state: Transcript.Store
): JsonArray {
  return state.conversation;
}
