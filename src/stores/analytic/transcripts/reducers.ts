import { combineReducers } from 'redux';

import { ReduxAction, JsonArray } from 'interfaces';
import TRANSCRIPTS from './types';
import * as Transcripts from 'interfaces/analytic/transcript';

const initialState: Transcripts.Store = {
  loadings: {
    list: false,
    list_new: false,
    conversation: false,
    conversation_new: false
  },
  errors: {
    list: null,
    list_new: null,
    conversation: null,
    conversation_new: null
  },
  // hasNewer: {
  //   list: false,
  //   conversation: false
  // },
  // hasOlder: {
  //   list: true,
  //   conversation: true
  // },
  list: [],
  conversation: []
};

const loadings = (
  state: Transcripts.Loadings = initialState.loadings,
  { type, action, payload }: ReduxAction
) => {
  const actionCaps = action ? action.toUpperCase() : 'LIST';
  switch (type) {
    case TRANSCRIPTS[`${actionCaps}_REQUEST`]:
      const reqAction: string =
        payload.end === -1 ? `${action as string}_new` : (action as string);
      return Object.assign({}, state, {
        [reqAction]: true
      });
    case TRANSCRIPTS[`${actionCaps}_SUCCESS`]:
    case TRANSCRIPTS[`${actionCaps}_FAIL`]:
      const newAction: string =
        payload.order === 'new'
          ? `${action as string}_new`
          : (action as string);
      return Object.assign({}, state, {
        [newAction]: false
      });
    default:
      return state;
  }
};

const errors = (
  state: Transcripts.Errors = initialState.errors,
  { type, action, payload }: ReduxAction
) => {
  const actionCaps = action ? action.toUpperCase() : 'FLOW';
  switch (type) {
    case TRANSCRIPTS.ERROR_CLEAR:
    case TRANSCRIPTS[`${actionCaps}_REQUEST`]:
      const reqAction: string =
        payload.end === -1 ? `${action as string}_new` : (action as string);
      return Object.assign({}, state, {
        [reqAction]: null
      });
    case TRANSCRIPTS[`${actionCaps}_FAIL`]:
      const newAction: string =
        payload.order === 'new'
          ? `${action as string}_new`
          : (action as string);
      return Object.assign({}, state, {
        [newAction]: payload.error
      });
    default:
      return state;
  }
};

const list = (
  state: JsonArray = initialState.list,
  { type, payload }: ReduxAction
) => {
  switch (type) {
    case TRANSCRIPTS.LIST_CLEAR:
      return initialState.list;
    case TRANSCRIPTS.LIST_SUCCESS: {
      if (payload.order === 'new') {
        return [...payload.data.reverse(), ...state];
      }
      return [...state, ...payload.data];
    }
    default:
      return state;
  }
};

const conversation = (
  state: JsonArray = initialState.conversation,
  { type, payload }: ReduxAction
) => {
  switch (type) {
    case TRANSCRIPTS.CONVERSATION_CLEAR:
      return initialState.conversation;
    case TRANSCRIPTS.CONVERSATION_SUCCESS: {
      if (payload.order === 'new') {
        return [...state, ...payload.data];
      }
      return [...payload.data, ...state];
    }
    default:
      return state;
  }
};

export default combineReducers({
  loadings,
  errors,
  list,
  conversation
});
