import { all, call, fork, put, takeLatest } from 'redux-saga/effects';
import moment from 'moment';
import get from 'lodash-es/get';

import { transcriptFail, listSuccess, conversationSuccess } from './actions';
import { callApi } from 'stores/services';
import { ReduxAction } from 'interfaces';
import { addNotification } from 'stores/app/notification/actions';
import TRANSCRIPT from './types';

function* fetchList({ payload }: ReduxAction) {
  const order = payload.end === -1 ? 'new' : 'old';
  try {
    const resp = yield call(
      callApi,
      'get',
      `/analytics/${payload.botId}/${
        payload.deployment
      }/list_transcript?startTimestamp=${payload.start}&endTimestamp=${
        payload.end
      }${payload.channel ? `&channelId=${payload.channel}` : ''}`
    );
    const data =
      resp && resp.length
        ? resp.map(item => {
            const startTime = item.updated_at || item.created_at;
            const preparedTIme =
              new Date(startTime).getTime() > 0
                ? startTime
                : parseInt(startTime, 10);
            return {
              ...item,
              startTime: !isNaN(preparedTIme)
                ? moment(preparedTIme).format('YYYY-MM-DD HH:mm:ss')
                : '-'
            };
          })
        : [];
    yield put(listSuccess(data, order));
  } catch (err) {
    const errorMessage = get(
      err,
      'response.data.message',
      'Oops... something went wrong'
    );
    yield put(
      addNotification({
        title: 'Failed to Load Transcript',
        message: 'Oops... something went wrong!',
        status: 'error',
        dismissible: true,
        dismissAfter: 5000
      })
    );
    yield put(transcriptFail('list', errorMessage, order));
  }
}

function* fetchConversation({ payload }: ReduxAction) {
  const order = payload.end === -1 ? 'new' : 'old';
  try {
    const resp = yield call(
      callApi,
      'get',
      `/analytics/${payload.botId}/${payload.deployment}/${
        payload.sessionId
      }/transcript?startTimestamp=${payload.start}&endTimestamp=${payload.end}${
        payload.channel ? `&channelId=${payload.channel}` : ''
      }`
    );
    const data =
      resp && resp.length
        ? resp.map(item => {
            const { messages, responses, created_at, ...rest } = item;
            let parsedMessages: any = null;
            let parsedResponses: any = null;
            try {
              parsedMessages = JSON.parse(messages);
              parsedResponses = JSON.parse(responses);
            } catch (err) {
              // do nothing
            }
            return {
              ...rest,
              created_at,
              messages: parsedMessages,
              responses: parsedResponses,
              dateStr: moment(created_at).format('YYYY-MM-DD HH:mm:ss')
            };
          })
        : [];
    yield put(conversationSuccess(data, order));
  } catch (err) {
    const errorMessage = get(
      err,
      'response.data.message',
      'Oops... something went wrong'
    );
    addNotification({
      title: 'Failed to Load Transcript',
      message: 'Oops... something went wrong!',
      status: 'error',
      dismissible: true,
      dismissAfter: 5000
    });
    yield put(transcriptFail('conversation', errorMessage, order));
  }
}

function* watchTranscriptList() {
  yield takeLatest(TRANSCRIPT.LIST_REQUEST, fetchList);
}

function* watchTranscriptConversation() {
  yield takeLatest(TRANSCRIPT.CONVERSATION_REQUEST, fetchConversation);
}

function* transcript() {
  yield all([fork(watchTranscriptList), fork(watchTranscriptConversation)]);
}

export default transcript;
