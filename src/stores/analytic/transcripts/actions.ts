import { JsonArray } from 'interfaces';
import TRANSCRIPT from './types';

export function cleartranscriptError(action: string) {
  return {
    action,
    type: TRANSCRIPT.ERROR_CLEAR
  };
}

export function clearList() {
  return {
    type: TRANSCRIPT.LIST_CLEAR
  };
}

function requestTranscript(
  action: string,
  payload: {
    botId: string;
    deployment: string;
    channel?: string;
    start: number;
    end: number;
    sessionId?: string;
  }
) {
  return {
    action,
    payload,
    type: TRANSCRIPT[`${action.toUpperCase()}_REQUEST`]
  };
}

export function transcriptFail(
  action: string,
  error: string,
  order: 'new' | 'old' = 'old'
) {
  return {
    action,
    type: TRANSCRIPT[`${action.toUpperCase()}_FAIL`],
    payload: {
      error
    }
  };
}

export function listRequest(
  botId: string,
  deployment: string,
  start: number,
  end: number,
  channel?: string
) {
  return requestTranscript('list', { botId, deployment, start, end, channel });
}

export function listSuccess(data: JsonArray, order: 'new' | 'old' = 'old') {
  return {
    type: TRANSCRIPT.LIST_SUCCESS,
    action: 'list',
    payload: {
      data,
      order
    }
  };
}

export function clearConversation() {
  return {
    type: TRANSCRIPT.CONVERSATION_CLEAR
  };
}

export function conversationRequest(
  botId: string,
  deployment: string,
  start: number,
  end: number,
  sessionId: string,
  channel?: string
) {
  return requestTranscript('conversation', {
    botId,
    deployment,
    channel,
    start,
    end,
    sessionId
  });
}

export function conversationSuccess(
  data: JsonArray,
  order: 'new' | 'old' = 'old'
) {
  return {
    type: TRANSCRIPT.CONVERSATION_SUCCESS,
    action: 'conversation',
    payload: {
      data,
      order
    }
  };
}
