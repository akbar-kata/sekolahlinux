import { JsonArray, JsonObject } from 'interfaces';
import OVERALL from './types';

export function clearOverallError(action: string) {
  return {
    action,
    type: OVERALL.ERROR_CLEAR
  };
}

function createRequest(
  action: string,
  payload: { startTimestamp: number; endTimestamp: number }
) {
  return {
    action,
    payload,
    type: OVERALL[`${action.toUpperCase()}_REQUEST`]
  };
}

export function overallGenericFail(action: string, error: string) {
  return {
    action,
    type: OVERALL[`${action.toUpperCase()}_FAIL`],
    payload: {
      error
    }
  };
}

export function overallRequest(startTimestamp: number, endTimestamp: number) {
  return createRequest('overall', { startTimestamp, endTimestamp });
}

export function overallSuccess(data: JsonObject) {
  return {
    type: OVERALL.OVERALL_SUCCESS,
    action: 'overall',
    payload: data
  };
}

export function botsGrowthRequest(
  startTimestamp: number,
  endTimestamp: number
) {
  return createRequest('bots_growth', { startTimestamp, endTimestamp });
}

export function botsGrowthSuccess(data: JsonArray) {
  return {
    type: OVERALL.BOTS_GROWTH_SUCCESS,
    action: 'bots_growth',
    payload: data
  };
}

export function usersGrowthRequest(
  startTimestamp: number,
  endTimestamp: number
) {
  return createRequest('users_growth', { startTimestamp, endTimestamp });
}

export function usersGrowthSuccess(data: JsonArray) {
  return {
    type: OVERALL.USERS_GROWTH_SUCCESS,
    action: 'users_growth',
    payload: data
  };
}
