import { combineReducers } from 'redux';

import { ReduxAction, JsonArray, JsonObject } from 'interfaces';
import OVERALL from './types';
import * as Overall from 'interfaces/analytic/overall';

const initialState: Overall.Store = {
  loadings: {
    overall: false,
    bots_growth: false,
    users_growth: false
  },
  errors: {
    overall: null,
    bots_growth: null,
    users_growth: null
  },
  overall: {},
  botsGrowth: [],
  usersGrowth: []
};

const loadings = (
  state: Overall.Loadings = initialState.loadings,
  { type, action, payload }: ReduxAction
) => {
  const actionCaps = action ? action.toUpperCase() : 'OVERALL';
  switch (type) {
    case OVERALL[`${actionCaps}_REQUEST`]:
      return Object.assign({}, state, {
        [action as string]: true
      });
    case OVERALL[`${actionCaps}_SUCCESS`]:
    case OVERALL[`${actionCaps}_FAIL`]:
      return Object.assign({}, state, {
        [action as string]: false
      });
    default:
      return state;
  }
};

const errors = (
  state: Overall.Errors = initialState.errors,
  { type, action, payload }: ReduxAction
) => {
  const actionCaps = action ? action.toUpperCase() : 'OVERALL';
  switch (type) {
    case OVERALL.ERROR_CLEAR:
    case OVERALL[`${actionCaps}_REQUEST`]:
      return Object.assign({}, state, {
        [action as string]: null
      });
    case OVERALL[`${actionCaps}_FAIL`]:
      return Object.assign({}, state, {
        [action as string]: payload.error
      });
    default:
      return state;
  }
};

const overall = (
  state: JsonObject = initialState.overall,
  { type, payload }: ReduxAction
) => {
  switch (type) {
    case OVERALL.OVERALL_SUCCESS:
      return { ...payload };
    default:
      return state;
  }
};

const botsGrowth = (
  state: JsonArray = initialState.botsGrowth,
  { type, payload }: ReduxAction
) => {
  switch (type) {
    case OVERALL.BOTS_GROWTH_SUCCESS:
      return [...payload];
    default:
      return state;
  }
};

const usersGrowth = (
  state: JsonArray = initialState.usersGrowth,
  { type, payload }: ReduxAction
) => {
  switch (type) {
    case OVERALL.USERS_GROWTH_SUCCESS:
      return [...payload];
    default:
      return state;
  }
};

export default combineReducers({
  loadings,
  errors,
  overall,
  botsGrowth,
  usersGrowth
});
