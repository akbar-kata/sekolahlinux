import { delay } from 'redux-saga';
import { all, fork, put, takeLatest, call } from 'redux-saga/effects';
import get from 'lodash-es/get';

import {
  overallGenericFail,
  overallSuccess,
  botsGrowthSuccess,
  usersGrowthSuccess
} from './actions';
import { ReduxAction, TypedReduxAction } from 'interfaces';
import OVERALL from './types';
import { callApi } from 'stores/services';
import { OverallRequest } from 'interfaces/analytic/overall';
import { formatMonthIndexToMonthName } from 'utils/formatter';
import isArray from 'lodash-es/isArray';

function* fetchOverall({ payload }: TypedReduxAction<OverallRequest>) {
  try {
    const res = yield call(callApi, 'post', '/analytics/internal', payload);
    const data = {
      botsTotal: res.numberOfBots[0].total,
      botsPerUser: res.avgNumberOfBotPerUsers[0].total,
      usersTotal: res.numberOfUsers[0].total,
      usersBuiltBot: res.numberOfUsersBuildBots[0].total
    };
    yield delay(2000);
    yield put(overallSuccess(data));
  } catch (err) {
    const errorMessage = get(
      err,
      'response.data.message',
      'Oops... something went wrong'
    );
    yield put(overallGenericFail('overall', errorMessage));
  }
}

function* fetchBotsGrowth({ payload }: ReduxAction) {
  try {
    const res = yield call(callApi, 'post', '/analytics/internal', payload);
    const data =
      res && isArray(res.growthOfBots)
        ? res.growthOfBots.map(grow => {
            const year = grow.date.substr(0, 4);
            const month = parseInt(grow.date.substr(4, 2), 10);
            const shortDate = `${year} ${formatMonthIndexToMonthName(month)}`;

            return { shortDate, totalBots: grow.total };
          })
        : [];
    yield put(botsGrowthSuccess(data));
  } catch (err) {
    const errorMessage = get(
      err,
      'response.data.message',
      'Oops... something went wrong'
    );
    yield put(overallGenericFail('bots_growth', errorMessage));
  }
}

function* fetchUsersGrowth({ payload }: ReduxAction) {
  try {
    const res = yield call(callApi, 'post', '/analytics/internal', payload);
    const data =
      res && isArray(res.growthOfUsers)
        ? res.growthOfUsers.map(grow => {
            const year = grow.date.substr(0, 4);
            const month = parseInt(grow.date.substr(4, 2), 10);
            const shortDate = `${year} ${formatMonthIndexToMonthName(month)}`;

            return { shortDate, totalUsers: grow.total };
          })
        : [];
    yield put(usersGrowthSuccess(data));
  } catch (err) {
    const errorMessage = get(
      err,
      'response.data.message',
      'Oops... something went wrong'
    );
    yield put(overallGenericFail('users_growth', errorMessage));
  }
}

function* watchAllOverall() {
  yield takeLatest(OVERALL.OVERALL_REQUEST, fetchOverall);
}

function* watchAllBotsGrowth() {
  yield takeLatest(OVERALL.BOTS_GROWTH_REQUEST, fetchBotsGrowth);
}

function* watchAllUsersGrowth() {
  yield takeLatest(OVERALL.USERS_GROWTH_REQUEST, fetchUsersGrowth);
}

function* overall() {
  yield all([
    fork(watchAllOverall),
    fork(watchAllBotsGrowth),
    fork(watchAllUsersGrowth)
  ]);
}

export default overall;
