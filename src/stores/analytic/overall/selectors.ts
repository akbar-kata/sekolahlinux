import { JsonObject, JsonArray } from 'interfaces';
import * as Overall from 'interfaces/analytic/overall';

export function getAnalyticAllLoading(
  state: Overall.Store,
  type: string
): boolean {
  return state.loadings[type];
}

export function getAnalyticAllError(
  state: Overall.Store,
  type: string
): string | null {
  return state.errors[type];
}

export function getAnalyticAllOverall(state: Overall.Store): JsonObject {
  return state.overall;
}

export function getAnalyticAllBotsGrowth(state: Overall.Store): JsonArray {
  return state.botsGrowth;
}

export function getAnalyticAllUsersGrowth(state: Overall.Store): JsonArray {
  return state.usersGrowth;
}
