import { all, fork } from 'redux-saga/effects';

import overallFlow from './overall/sagas';
import userFlow from './user/sagas';
import conversationFlow from './conversation/sagas';
import transcriptFlow from './transcripts/sagas';
import rootFlow from './root/sagas';

function* analyticFlow() {
  yield all([
    fork(overallFlow),
    fork(userFlow),
    fork(conversationFlow),
    fork(transcriptFlow),
    fork(rootFlow)
  ]);
}

export default analyticFlow;
