import { combineReducers } from 'redux';
import moment from 'moment';

import { ReduxAction } from 'interfaces';
import ANALYTIC from './types';
import * as Analytic from 'interfaces/analytic';
import OverallReducer from './overall/reducers';
import UserReducer from './user/reducers';
import ConversationReducer from './conversation/reducers';
import TranscriptReducer from './transcripts/reducers';
import RootReducer from './root/reducers';

const initialState: Analytic.DateRange = {
  start: moment()
    .subtract(30, 'day')
    .startOf('day')
    .valueOf(),
  end: moment()
    .endOf('day')
    .valueOf()
};

const dateRage = (
  state: Analytic.DateRange = initialState,
  { type, payload }: ReduxAction
) => {
  switch (type) {
    case ANALYTIC.DATE_CHANGE:
      return Object.assign({}, state, {
        start: payload.start,
        end: payload.end
      });
    default:
      return state;
  }
};

export default combineReducers({
  dateRage,
  overall: OverallReducer,
  user: UserReducer,
  conversation: ConversationReducer,
  transcript: TranscriptReducer,
  root: RootReducer
});
