import * as Analytic from 'interfaces/analytic';

export function getAnalyticDate(state: Analytic.Store): Analytic.DateRange {
  return state.dateRage;
}
