import TYPES from './types';

export function changeAnalyticDate(start: number, end: number) {
  return {
    type: TYPES.DATE_CHANGE,
    payload: {
      start,
      end
    }
  };
}
