import { all, call, fork, put, takeLatest } from 'redux-saga/effects';
import get from 'lodash-es/get';

import {
  conversationFail,
  flowSuccess,
  intentSuccess,
  messageSuccess
} from './actions';
import { callApi } from 'stores/services';
import { ReduxAction } from 'interfaces';
import CONVERSATION from './types';

function* fetchFlow({ payload }: ReduxAction) {
  try {
    const data = yield call(
      callApi,
      'get',
      `/analytics/${payload.botId}/${payload.deployment}/flows?startTimestamp=${
        payload.start
      }&endTimestamp=${payload.end}${
        payload.channel ? `&channelId=${payload.channel}` : ''
      }`
    );
    yield put(flowSuccess(data));
  } catch (err) {
    const errorMessage = get(
      err,
      'response.data.message',
      'Oops... something went wrong.'
    );
    yield put(conversationFail('flow', errorMessage));
  }
}

function* fetchIntent({ payload }: ReduxAction) {
  try {
    const data = yield call(
      callApi,
      'get',
      `/analytics/${payload.botId}/${
        payload.deployment
      }/intents?startTimestamp=${payload.start}&endTimestamp=${payload.end}${
        payload.channel ? `&channelId=${payload.channel}` : ''
      }`
    );
    yield put(intentSuccess(data));
  } catch (err) {
    const errorMessage = get(
      err,
      'response.data.message',
      'Oops... something went wrong.'
    );
    yield put(conversationFail('intent', errorMessage));
  }
}

function* fetchMessage({ payload }: ReduxAction) {
  try {
    const data = yield call(
      callApi,
      'get',
      `/analytics/${payload.botId}/${payload.deployment}/texts?startTimestamp=${
        payload.start
      }&endTimestamp=${payload.end}${
        payload.channel ? `&channelId=${payload.channel}` : ''
      }`
    );
    yield put(messageSuccess(data));
  } catch (err) {
    const errorMessage = get(
      err,
      'response.data.message',
      'Oops... something went wrong.'
    );
    yield put(conversationFail('message', errorMessage));
  }
}

function* watchConversationFlow() {
  yield takeLatest(CONVERSATION.FLOW_REQUEST, fetchFlow);
}

function* watchConversationIntent() {
  yield takeLatest(CONVERSATION.INTENT_REQUEST, fetchIntent);
}

function* watchConversationMessage() {
  yield takeLatest(CONVERSATION.MESSAGE_REQUEST, fetchMessage);
}

function* conversation() {
  yield all([
    fork(watchConversationFlow),
    fork(watchConversationIntent),
    fork(watchConversationMessage)
  ]);
}

export default conversation;
