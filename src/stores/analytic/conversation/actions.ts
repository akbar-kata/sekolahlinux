import { JsonArray } from 'interfaces';
import CONVERSATION from './types';

export function clearConversationError(action: string) {
  return {
    action,
    type: CONVERSATION.ERROR_CLEAR
  };
}

function requestConversation(
  action: string,
  payload: {
    botId: string;
    deployment: string;
    channel?: string;
    start: number;
    end: number;
  }
) {
  return {
    action,
    payload,
    type: CONVERSATION[`${action.toUpperCase()}_REQUEST`]
  };
}

export function conversationFail(action: string, error: string) {
  return {
    action,
    type: CONVERSATION[`${action.toUpperCase()}_FAIL`],
    payload: {
      error
    }
  };
}

export function flowRequest(
  botId: string,
  deployment: string,
  start: number,
  end: number,
  channel?: string
) {
  return requestConversation('flow', {
    botId,
    deployment,
    start,
    end,
    channel
  });
}

export function flowSuccess(data: JsonArray) {
  return {
    type: CONVERSATION.FLOW_SUCCESS,
    action: 'flow',
    payload: data
  };
}

export function intentRequest(
  botId: string,
  deployment: string,
  start: number,
  end: number,
  channel?: string
) {
  return requestConversation('intent', {
    botId,
    deployment,
    start,
    end,
    channel
  });
}

export function intentSuccess(data: JsonArray) {
  return {
    type: CONVERSATION.INTENT_SUCCESS,
    action: 'intent',
    payload: data
  };
}

export function messageRequest(
  botId: string,
  deployment: string,
  start: number,
  end: number,
  channel?: string
) {
  return requestConversation('message', {
    botId,
    deployment,
    start,
    end,
    channel
  });
}

export function messageSuccess(data: JsonArray) {
  return {
    type: CONVERSATION.MESSAGE_SUCCESS,
    action: 'message',
    payload: data
  };
}
