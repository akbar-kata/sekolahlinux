import { JsonArray } from 'interfaces';
import * as Conversation from 'interfaces/analytic/conversation';

export function getAnalyticConvLoading(
  state: Conversation.Store,
  type: string
): boolean {
  return state.loadings[type];
}

export function getAnalyticConvError(
  state: Conversation.Store,
  type: string
): string | null {
  return state.errors[type];
}

export function getAnalyticConvFlow(state: Conversation.Store): JsonArray {
  return state.flow;
}

export function getAnalyticConvIntent(state: Conversation.Store): JsonArray {
  return state.intent;
}

export function getAnalyticConvMessage(state: Conversation.Store): JsonArray {
  return state.message;
}
