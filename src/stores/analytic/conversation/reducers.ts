import { combineReducers } from 'redux';

import { ReduxAction, JsonArray } from 'interfaces';
import CONVERSATION from './types';
import * as Conversation from 'interfaces/analytic/conversation';

const initialState: Conversation.Store = {
  loadings: {
    flow: false,
    intent: false,
    message: false
  },
  errors: {
    flow: null,
    intent: null,
    message: null
  },
  flow: [],
  intent: [],
  message: []
};

const loadings = (
  state: Conversation.Loadings = initialState.loadings,
  { type, action, payload }: ReduxAction
) => {
  const actionCaps = action ? action.toUpperCase() : 'FLOW';
  switch (type) {
    case CONVERSATION[`${actionCaps}_REQUEST`]:
      return Object.assign({}, state, {
        [action as string]: true
      });
    case CONVERSATION[`${actionCaps}_SUCCESS`]:
    case CONVERSATION[`${actionCaps}_FAIL`]:
      return Object.assign({}, state, {
        [action as string]: false
      });
    default:
      return state;
  }
};

const errors = (
  state: Conversation.Errors = initialState.errors,
  { type, action, payload }: ReduxAction
) => {
  const actionCaps = action ? action.toUpperCase() : 'FLOW';
  switch (type) {
    case CONVERSATION.ERROR_CLEAR:
    case CONVERSATION[`${actionCaps}_REQUEST`]:
      return Object.assign({}, state, {
        [action as string]: null
      });
    case CONVERSATION[`${actionCaps}_FAIL`]:
      return Object.assign({}, state, {
        [action as string]: payload.error
      });
    default:
      return state;
  }
};

const flow = (
  state: JsonArray = initialState.flow,
  { type, payload }: ReduxAction
) => {
  switch (type) {
    case CONVERSATION.FLOW_SUCCESS:
      return [...payload];
    default:
      return state;
  }
};

const intent = (
  state: JsonArray = initialState.intent,
  { type, payload }: ReduxAction
) => {
  switch (type) {
    case CONVERSATION.INTENT_SUCCESS:
      return [...payload];
    default:
      return state;
  }
};

const message = (
  state: JsonArray = initialState.message,
  { type, payload }: ReduxAction
) => {
  switch (type) {
    case CONVERSATION.MESSAGE_SUCCESS:
      return [...payload];
    default:
      return state;
  }
};

export default combineReducers({
  loadings,
  errors,
  flow,
  intent,
  message
});
