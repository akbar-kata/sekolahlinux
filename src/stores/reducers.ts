import { combineReducers, Reducer } from 'redux';
import { connectRouter } from 'connected-react-router';
import { History } from 'history';
import { reducer as notificationsReducer } from 'reapop';

import authType from './user/auth/types';
import account from './user/account/reducers';
import check from './user/check/reducers';
import auth from './user/auth/reducers';
import analytic from './analytic/reducers';
import bot from './bot/reducers';
import app from './app/reducers';
import cms from './cms/reducers';
import deployment from './deployment/reducers';
import pricing from './pricing/reducers';
import nlu from './nlu/reducers';
import member from './user/member/reducers';
import team from './user/team/reducers';
import subscription from './subscription/reducers';
import project from './project/reducers';
import module from './module/reducers';
import revision from './revision/reducers';

import RootStore from 'interfaces/rootStore';

const reducer = (history: History) =>
  combineReducers<RootStore>({
    account,
    analytic,
    app,
    auth,
    bot,
    check,
    cms,
    deployment,
    member,
    module,
    nlu,
    pricing,
    project,
    revision,
    subscription,
    team,

    notifications: notificationsReducer(),
    router: connectRouter(history)
  });

const buildRootReducer: (h: History) => Reducer = (history: History) => (
  state,
  action
) => {
  let newState = state;
  if (action.type === authType.CLEAR) {
    newState = undefined;
  }

  return reducer(history)(newState, action);
};

export default buildRootReducer;
