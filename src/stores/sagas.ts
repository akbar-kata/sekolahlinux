import { fork, all } from 'redux-saga/effects';

import App from './app/sagas';
import Analytic from './analytic/sagas';
import Account from './user/account/sagas';
import Auth from './user/auth/sagas';
import Bot from './bot/sagas';
import Check from './user/check/sagas';
import Cms2 from './cms/sagas';
import Deployment from './deployment/sagas';
import Pricing from './pricing/sagas';
import Member from './user/member/sagas';
import Subscription from './subscription/sagas';
import Team from './user/team/sagas';
import NlStudio from './nlu/sagas';
import Project from './project/sagas';
import Module from './module/sagas';

import Revision from './revision/sagas';

import Notification from './app/notification/sagas';

export default function* root() {
  yield all([
    fork(App),
    fork(Account),
    fork(Analytic),
    fork(Auth),
    fork(Bot),
    fork(Check),
    fork(Cms2),
    fork(Deployment),
    fork(Pricing),
    fork(Member),
    fork(Team),
    fork(Subscription),
    fork(NlStudio),
    fork(Project),
    fork(Module),
    fork(Revision),

    fork(Notification)
  ]);
}
