import { Json } from 'interfaces';
import DRAWER from './types';

export function openDrawer(id: string, data?: Json) {
  return {
    type: DRAWER.OPEN,
    payload: {
      id,
      data
    }
  };
}

export function closeDrawer(id: string) {
  return {
    type: DRAWER.CLOSE,
    payload: {
      id
    }
  };
}
