const DRAWER = {
  OPEN: 'DRAWER/OPEN',
  CLOSE: 'DRAWER/CLOSE'
};

export default DRAWER;
