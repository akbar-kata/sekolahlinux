import { combineReducers } from 'redux';

import { ReduxAction } from 'interfaces';
import * as Drawer from 'interfaces/app/drawer';
import DRAWER from './types';

const open = (state: Drawer.Open = {}, { type, payload }: ReduxAction) => {
  switch (type) {
    case DRAWER.OPEN:
      return Object.assign({}, state, {
        [payload.id]: true
      });
    case DRAWER.CLOSE:
      return Object.assign({}, state, {
        [payload.id]: false
      });
    default:
      return state;
  }
};

const data = (state: Drawer.Data = {}, { type, payload }: ReduxAction) => {
  switch (type) {
    case DRAWER.OPEN:
      if (payload.data) {
        return Object.assign({}, state, {
          [payload.id]: payload.data
        });
      }

      return state;
    case DRAWER.CLOSE:
      return Object.assign({}, state, {
        [payload.id]: undefined
      });
    default:
      return state;
  }
};

export default combineReducers({
  open,
  data
});
