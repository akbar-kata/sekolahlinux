import { Json } from 'interfaces';
import * as Drawer from 'interfaces/app/drawer';

export function isDrawerOpen(state: Drawer.Store, id: string): boolean {
  return state.open[id];
}

export function getDrawerData(state: Drawer.Store, id: string): Json {
  return state.data[id];
}
