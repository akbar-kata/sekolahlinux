import { combineReducers } from 'redux';

import modal from './modal/reducers';
import drawer from './drawer/reducers';
import sidebar from './sidebar/reducers';
import loadings from './loadings/reducers';

export default combineReducers({
  modal,
  drawer,
  sidebar,
  loadings
});
