import { combineReducers } from 'redux';

import { ReduxAction } from 'interfaces';
import { DataMap } from 'interfaces/common';
import LOADINGS from './types';

const data = (state: DataMap<boolean> = {}, action: ReduxAction) => {
  switch (action.type) {
    case LOADINGS.SET_LOADING:
      return { ...state, [action.payload.id]: action.payload.loading };
    default:
      return state;
  }
};

export default combineReducers({
  data
});
