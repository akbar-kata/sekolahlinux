import { all, select, fork, put, takeLatest } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'connected-react-router';
import Cookies from 'js-cookie';

import RootStore from 'interfaces/rootStore';
import { isRootURL } from 'utils/url';
import { openSidebar, closeSidebar } from './sidebar/actions';

export const CK_LAST_PATH = 'ktapp.last_path';

function* onLocationChange() {
  const { pathname } = yield select(
    (state: RootStore) => state.router.location
  );

  if (!/^(\/login|\/setting)/.test(pathname)) {
    Cookies.set(CK_LAST_PATH, pathname);
  }

  if (isRootURL(pathname)) {
    yield put(closeSidebar());
  } else {
    yield put(openSidebar());
  }
}

function* watchLocation() {
  yield takeLatest(LOCATION_CHANGE, onLocationChange);
}

function* appFlow() {
  yield all([fork(watchLocation)]);
}

export default appFlow;
