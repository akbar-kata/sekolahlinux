import { combineReducers } from 'redux';

import { ReduxAction } from 'interfaces';
import * as Modal from 'interfaces/app/modal';
import MODAL from './types';

const open = (state: Modal.Open = {}, { type, payload }: ReduxAction) => {
  switch (type) {
    case MODAL.OPEN:
      return Object.assign({}, state, {
        [payload.id]: true
      });
    case MODAL.CLOSE:
      return Object.assign({}, state, {
        [payload.id]: false
      });
    default:
      return state;
  }
};

const data = (state: Modal.Data = {}, { type, payload }: ReduxAction) => {
  switch (type) {
    case MODAL.OPEN:
      if (payload.data) {
        return Object.assign({}, state, {
          [payload.id]: payload.data
        });
      }
      return state;
    case MODAL.CLOSE:
      return Object.assign({}, state, {
        [payload.id]: undefined
      });
    default:
      return state;
  }
};

export default combineReducers({
  open,
  data
});
