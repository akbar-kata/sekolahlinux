import { Json } from 'interfaces';
import * as Modal from 'interfaces/app/modal';

export function isModalOpen(state: Modal.Store, id: string): boolean {
  return state.open[id];
}

export function getModalData(state: Modal.Store, id: string): Json {
  return state.data[id];
}
