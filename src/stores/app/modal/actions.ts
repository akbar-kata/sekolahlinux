import { Json } from 'interfaces';
import MODAL from './types';

export function openModal(id: string, data?: Json) {
  return {
    type: MODAL.OPEN,
    payload: {
      id,
      data
    }
  };
}

export function closeModal(id: string) {
  return {
    type: MODAL.CLOSE,
    payload: {
      id
    }
  };
}
