const MODAL = {
  OPEN: 'MODAL/OPEN',
  CLOSE: 'MODAL/CLOSE'
};

export default MODAL;
