import * as Sidebar from 'interfaces/app/sidebar';

export function isSidebarOpen(state: Sidebar.Store): boolean {
  return state.isOpen;
}
