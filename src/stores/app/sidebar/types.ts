const SIDEBAR = {
  OPEN: 'APP/SIDEBAR/OPEN',
  CLOSE: 'APP/SIDEBAR/CLOSE',
  TOGGLE: 'APP/SIDEBAR/TOGGLE'
};

export default SIDEBAR;
