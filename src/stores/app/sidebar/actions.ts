import TYPES from './types';

export function openSidebar() {
  return {
    type: TYPES.OPEN
  };
}

export function closeSidebar() {
  return {
    type: TYPES.CLOSE
  };
}

export function toggleSidebar() {
  return {
    type: TYPES.TOGGLE
  };
}
