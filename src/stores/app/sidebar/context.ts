import { connect } from 'react-redux';
import Store from 'interfaces/rootStore';
import { isSidebarOpen } from 'stores/app/sidebar/selectors';
import { closeSidebar, openSidebar } from 'stores/app/sidebar/actions';

export interface WithSidebarStates {
  isSidebarOpen: boolean;
}

export interface WithSidebarDispatches {
  openSidebar();

  closeSidebar();
}

const mapStateToProps = (store: Store): WithSidebarStates => ({
  isSidebarOpen: isSidebarOpen(store.app.sidebar)
});
const mapDispatchToProps: WithSidebarDispatches = {
  openSidebar,
  closeSidebar
};

export default function withSidebar(component: any) {
  return connect(
    mapStateToProps,
    mapDispatchToProps
  )(component);
}
