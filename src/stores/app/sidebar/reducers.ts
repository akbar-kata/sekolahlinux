import { combineReducers } from 'redux';

import { ReduxAction } from 'interfaces';
import * as Sidebar from 'interfaces/app/sidebar';
import TYPES from './types';

const initialState: Sidebar.Store = {
  isOpen: true
};

const isOpen = (
  state: boolean = initialState.isOpen,
  { type, payload }: ReduxAction
) => {
  switch (type) {
    case TYPES.OPEN:
      return true;
    case TYPES.CLOSE:
      return false;
    case TYPES.TOGGLE:
      return !state;
    default:
      return state;
  }
};

export default combineReducers({
  isOpen
});
