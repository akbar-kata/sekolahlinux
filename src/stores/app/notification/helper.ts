/**
 * Convert status in a understandable status for the Notification component
 * @param {String|Number} status
 * @returns {String} status an understandable status
 */
export function convertStatus(status: any) {
  const reHttpStatusCode = /^\d{3}$/;
  // convert HTTP status code
  if (reHttpStatusCode.test(status)) {
    switch (true) {
      case /^1/.test(status):
        return 'info';
      case /^2/.test(status):
        return 'success';
      case /^(4|5)/.test(status):
        return 'error';
      default:
        return 'default';
    }
  }
  return status;
}

/**
 * Treat data of a notification
 * @param {Object} notification
 * @returns {Object} a notification
 */
export function treatNotification(notification: any) {
  if (notification.dismissAfter) {
    notification.dismissAfter = parseInt(notification.dismissAfter, 10);
  }
  if (notification.image) {
    notification.status = 'default';
  } else {
    notification.status = convertStatus(notification.status);
  }
  if (!notification.buttons) {
    notification.buttons = [];
  }
  return notification;
}

/**
 * Preload an image
 * @param {String} url url of image to load
 * @param {Function} cb Function called when image is loaded or not
 * @returns {void}
 */
export function preloadImage(url: string) {
  return new Promise((resolve, reject) => {
    const image = new Image();
    image.src = url;
    image.onload = () => {
      resolve(image);
    };
    image.onerror = () => {
      reject(image);
    };
  });
}
