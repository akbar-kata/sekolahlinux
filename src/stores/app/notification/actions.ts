import { Json } from 'interfaces';
// import { types } from 'reapop';

import types from './types';

/**
 * Add a notification (action creator)
 *
 * @param {Object} notification
 * @returns {{type: string, payload: {Object}}}
 * @private
 */
export function addNotification(notification: any) {
  return {
    type: types.ADD,
    payload: {
      position: 'br',
      dismissible: true,
      dismissAfter: 5000,
      ...notification
    }
  };
}

/**
 * Update a notification (action creator)
 *
 * @param {Object} notification
 * @returns {{type: string, payload: {Object}}}
 * @private
 */
export function updateNotification(notification: Json) {
  return {
    type: types.UPDATE,
    payload: notification
  };
}

/**
 * Remove a notification (action creator)
 *
 * @param {Object} notification
 * @returns {{type: string, payload: {Object}}}
 */
export function removeNotification(notification: Json) {
  return {
    type: types.REMOVE,
    payload: notification
  };
}

/**
 * Remove all notifications (action creator)
 *
 * @returns {{type: string}}
 */
export function removeNotifications() {
  return {
    type: types.REMOVES
  };
}
