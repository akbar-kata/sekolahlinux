import { put, takeEvery, all, fork, select } from 'redux-saga/effects';
import { types } from 'reapop';

import { Json, ReduxAction } from 'interfaces';
import NOTIFICATION from './types';
import { treatNotification, preloadImage } from './helper';

function* addNotification({ payload }: ReduxAction) {
  try {
    let notification: any = Object.assign({}, payload);
    if (!notification.id) {
      notification.id = new Date().getTime();
    }
    notification = treatNotification(notification);
    // if there is an image, we preload it
    // and add notification when image is loaded
    if (notification.image) {
      yield preloadImage(notification.image);
    }
    yield put(_addNotification(notification));
  } catch (err) {
    // do nothing
  }
}

function* updateNotification({ payload }: ReduxAction) {
  try {
    let notification: any = Object.assign({}, payload);
    if (!notification.id) {
      throw new Error(
        'A notification must have an `id` property to be updated'
      );
    }

    const notifications = yield select((state: any) => state.notifications);

    const index = notifications.findIndex((oldNotification: any) => {
      return oldNotification.id === notification.id;
    });
    const currNotification = notifications[index];

    notification = treatNotification(notification);

    // if image is different, then we preload it
    // and update notification when image is loaded
    if (
      notification.image &&
      (!currNotification.image ||
        (currNotification.image &&
          notification.image !== currNotification.image))
    ) {
      yield preloadImage(notification.image);
    }
    yield put(_updateNotification(notification));
  } catch (err) {
    // do nothing
  }
}

function* removeNotification({ payload }: ReduxAction) {
  yield put(_removeNotification(payload));
}

function* removeNotifications({ payload }: ReduxAction) {
  yield put(_removeNotifications());
}

/**
 * Add a notification (action creator)
 *
 * @param {Object} notification
 * @returns {{type: string, payload: {Object}}}
 * @private
 */
function _addNotification(notification: Json) {
  return {
    type: types.ADD_NOTIFICATION,
    payload: notification
  };
}

/**
 * Update a notification (action creator)
 *
 * @param {Object} notification
 * @returns {{type: string, payload: {Object}}}
 * @private
 */
function _updateNotification(notification: Json) {
  return {
    type: types.UPDATE_NOTIFICATION,
    payload: notification
  };
}

/**
 * Remove a notification (action creator)
 *
 * @param {Object} notification
 * @returns {{type: string, payload: {Object}}}
 */
function _removeNotification(notification: Json) {
  return {
    type: types.REMOVE_NOTIFICATION,
    payload: notification
  };
}

/**
 * Remove all notifications (action creator)
 *
 * @returns {{type: string}}
 */
function _removeNotifications() {
  return {
    type: types.REMOVE_NOTIFICATIONS
  };
}

function* watchAddNotification() {
  yield takeEvery(NOTIFICATION.ADD, addNotification);
}

function* watchUpdateNotification() {
  yield takeEvery(NOTIFICATION.UPDATE, updateNotification);
}

function* watchRemoveNotification() {
  yield takeEvery(NOTIFICATION.REMOVE, removeNotification);
}

function* watchRemoveNotifications() {
  yield takeEvery(NOTIFICATION.REMOVES, removeNotifications);
}

function* notificationFlow() {
  yield all([
    fork(watchAddNotification),
    fork(watchUpdateNotification),
    fork(watchRemoveNotification),
    fork(watchRemoveNotifications)
  ]);
}

export default notificationFlow;
