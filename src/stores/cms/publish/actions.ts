import CMS_PUBLISH from './types';

export function publishCmsRequest(id: string) {
  return {
    type: CMS_PUBLISH.REQUEST,
    payload: id
  };
}

export function publishCmsSuccess(data: string) {
  return {
    type: CMS_PUBLISH.SUCCESS,
    payload: data
  };
}

export function publishCmsFail(data: string) {
  return {
    type: CMS_PUBLISH.FAIL,
    payload: data
  };
}
