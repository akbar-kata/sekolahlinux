import { all, fork, takeLatest, put, call, select } from 'redux-saga/effects';
import get from 'lodash-es/get';
import RootStore from 'interfaces/rootStore';

import { callApi } from 'stores/services';
import { addNotification } from 'stores/app/notification/actions';
import { setLoading } from 'stores/app/loadings/action';
import { closeModal } from 'stores/app/modal/actions';
import { publishCmsFail, publishCmsSuccess } from './actions';
import CMS_PUBLISH from './types';
import truncateRevId from 'modules/deployments/utils/truncateRevId';
import { fetchCmsPagesRequest } from 'stores/cms/pages/actions';

function* publishCms() {
  yield put(setLoading(CMS_PUBLISH.REQUEST, true));
  try {
    const PROJECT_ID = yield select(
      (store: RootStore) => store.project.selected
    );
    const request: any = {
      data: null
    };
    const resp = yield call(
      callApi,
      'post',
      `/projects/${PROJECT_ID}/cms/revisions/`,
      request
    );
    yield put(publishCmsSuccess('id'));
    yield put(closeModal('cms-publish-revision'));
    yield put(
      addNotification({
        title: 'Publish Successful',
        message: `Revision <samp>${truncateRevId(
          resp.revision
        )}</samp> successfully created.`,
        status: 'success',
        allowHTML: true
      })
    );
    yield put(fetchCmsPagesRequest());
    yield put(setLoading(CMS_PUBLISH.REQUEST, false));
  } catch (err) {
    const errorMessage = get(
      err,
      'response.data.message',
      'Oops.. something wrong'
    );
    yield put(
      addNotification({
        title: 'Publish Failed',
        message: errorMessage,
        status: 'error'
      })
    );
    yield put(setLoading(CMS_PUBLISH.REQUEST, false));
    yield put(publishCmsFail(errorMessage));
    yield put(closeModal('cms-publish-revision'));
  }
}

function* watchCmsPublish() {
  yield takeLatest(CMS_PUBLISH.REQUEST, publishCms);
}

function* cmsPublishFlow() {
  yield all([fork(watchCmsPublish)]);
}

export default cmsPublishFlow;
