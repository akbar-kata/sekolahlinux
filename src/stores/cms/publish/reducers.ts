import { combineReducers } from 'redux';
import { ReduxAction } from 'interfaces';

const data = ({ payload }: ReduxAction) => payload;

export default combineReducers({
  data
});
