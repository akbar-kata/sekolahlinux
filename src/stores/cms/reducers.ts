import { combineReducers } from 'redux';

import pages from './pages/reducers';
import users from './users/reducers';
import forms from './forms/reducers';
import elements from './elements/reducers';
import * as Store from 'interfaces/cms/index';
import { ReduxAction } from 'interfaces';
import PAGE from 'stores/cms/pages/types';
import CMS from 'stores/cms/types';

const settings = (
  // @ts-ignore
  state: Store.CmsData = {},
  { type, payload }: ReduxAction
) => {
  switch (type) {
    case CMS.FETCH_SUCCESS:
      return payload.data;
    case CMS.EDIT_SUCCESS:
      return payload.data;
    default:
      return state;
  }
};

// @ts-ignore
const detail = (state: Store.Cms = {}, { type, payload }: ReduxAction) => {
  switch (type) {
    case PAGE.FETCH_SUCCESS:
      const { pages: deleted, ...rest } = payload;
      return rest;
    default:
      return state;
  }
};

export default combineReducers<Store.CmsStore>({
  settings,
  detail,
  pages,
  users,
  forms,
  elements
});
