import { all, fork, select, takeLatest, put, call } from 'redux-saga/effects';
import get from 'lodash-es/get';

import { ReduxAction, TypedReduxAction } from 'interfaces';
import RootStore from 'interfaces/rootStore';
import { CmsData } from 'interfaces/cms';
import PROJECT_TYPES from 'stores/project/types';

import Page from './pages/sagas';
import User from './users/sagas';
import Form from './forms/sagas';
import Element from './elements/sagas';
import Publish from './publish/sagas';

import CMS from './types';
import { getProjectSelected } from 'stores/project/selectors';
import * as CmsActions from './actions';
import * as CmsPagesActions from './pages/actions';
import * as CommonAction from 'stores/common/actions';
import { setLoading } from 'stores/app/loadings/action';
import { callApi } from 'stores/services';
import { addNotification } from 'stores/app/notification/actions';
import CMS_USER from './users/types';

function* fetchSettings({ payload }: TypedReduxAction<CmsData>) {
  const PROJECT_ID = yield select((store: RootStore) => store.project.selected);
  yield put(setLoading(CMS.FETCH_REQUEST, true));
  try {
    const res = yield call(
      callApi,
      'get',
      `projects/${PROJECT_ID}/cms/settings`
    );
    yield put(CmsActions.fetchCmsSettingsSuccess(res));
  } catch (err) {
    const errorMessage = get(
      err,
      'response.data.message',
      'Oops... something went wrong.'
    );
    yield put(
      addNotification({
        title: 'Failed to load CMS',
        message: errorMessage,
        status: 'error',
        dismissible: true,
        dismissAfter: 5000
      })
    );
  } finally {
    yield put(setLoading(CMS.FETCH_REQUEST, false));
  }
}

function* updateSettings({ payload }: TypedReduxAction<CmsData>) {
  const PROJECT_ID = yield select((store: RootStore) => store.project.selected);
  yield put(setLoading(CMS.EDIT_REQUEST, true));
  try {
    const settings = payload.settings;
    yield call(callApi, 'put', `projects/${PROJECT_ID}/cms/settings`, settings);
    yield put(CmsActions.editCmsSettingsSuccess(payload));
    yield put(
      addNotification({
        title: 'Success',
        message: `You have successfully updated CMS. Click on the card to enter the CMS.`,
        status: 'success',
        dismissible: true,
        dismissAfter: 5000
      })
    );
  } catch (err) {
    const errorMessage = get(
      err,
      'response.data.message',
      'Oops... something went wrong.'
    );
    yield put(
      addNotification({
        title: 'Failed to create CMS',
        message: errorMessage,
        status: 'error',
        dismissible: true,
        dismissAfter: 5000
      })
    );
  } finally {
    yield put(setLoading(CMS.EDIT_REQUEST, false));
  }
}

function* selectProjectHandler({ payload }: ReduxAction) {
  const selectedProject = yield select((state: RootStore) =>
    getProjectSelected(state.project)
  );

  if (selectedProject) {
    yield put(CmsPagesActions.fetchCmsPagesRequest());
    yield put(CmsActions.requestCmsSettings());
    yield put(CommonAction.request(CMS_USER.FETCH_DATA));
  }
}

function* watchFetchSettings() {
  yield takeLatest(CMS.FETCH_REQUEST, fetchSettings);
}

function* watchUpdateSettings() {
  yield takeLatest(CMS.EDIT_REQUEST, updateSettings);
}

function* watchProjectSelected() {
  yield takeLatest(PROJECT_TYPES.SELECT_PROJECT, selectProjectHandler);
}

function* cmsFlow() {
  yield all([
    fork(watchFetchSettings),
    fork(watchUpdateSettings),
    fork(watchProjectSelected),
    fork(Page),
    fork(User),
    fork(Form),
    fork(Element),
    fork(Publish)
  ]);
}

export default cmsFlow;
