import { ExReduxAction } from 'interfaces';
import { CmsStore } from 'interfaces/cms';
import { Swap } from 'interfaces/common';

enum PAGE {
  FETCH_REQUEST = 'CMS/PAGE/FETCH_REQUEST',
  FETCH_SUCCESS = 'CMS/PAGE/FETCH_SUCCESS',
  FETCH_FAILED = 'CMS/PAGE/FETCH_FAILED',

  ADD_REQUEST = 'CMS/PAGE/ADD_REQUEST',
  ADD_SUCCESS = 'CMS/PAGE/ADD_SUCCESS',
  ADD_FAILED = 'CMS/PAGE/ADD_FAILED',

  EDIT_REQUEST = 'CMS/PAGE/EDIT_REQUEST',
  EDIT_SUCCESS = 'CMS/PAGE/EDIT_SUCCESS',
  EDIT_FAILED = 'CMS/PAGE/EDIT_FAILED',

  REMOVE_REQUEST = 'CMS/PAGE/REMOVE_REQUEST',
  REMOVE_SUCCESS = 'CMS/PAGE/REMOVE_SUCCESS',
  REMOVE_FAILED = 'CMS/PAGE/REMOVE_FAILED',

  SELECT = 'CMS/PAGE/SELECT',
  SELECT_NO_PUSH = 'CMS/PAGE/SELECT_NO_PUSH',

  SWAP = 'CMS/PAGE/SWAP'
}

export type FetchCmsPagesRequestAction = ExReduxAction<
  PAGE.FETCH_REQUEST,
  string
>;
export type FetchCmsPagesSuccessAction = ExReduxAction<
  PAGE.FETCH_SUCCESS,
  CmsStore
>;
export type FetchCmsPagesFailedAction = ExReduxAction<
  PAGE.FETCH_FAILED,
  string
>;

export type AddCmsPagesRequestAction = ExReduxAction<PAGE.ADD_REQUEST, string>;
export type AddCmsPagesSuccessAction = ExReduxAction<
  PAGE.ADD_SUCCESS,
  CmsStore
>;
export type AddCmsPagesFailedAction = ExReduxAction<PAGE.ADD_FAILED, string>;

export type EditCmsPagesRequest = ExReduxAction<PAGE.EDIT_REQUEST, string>;
export type EditCmsPagesSuccess = ExReduxAction<PAGE.EDIT_SUCCESS, CmsStore>;
export type EditCmsPagesFailed = ExReduxAction<PAGE.EDIT_FAILED, string>;

export type RemoveCmsPagesRequest = ExReduxAction<PAGE.REMOVE_REQUEST, string>;
export type RemoveCmsPagesSuccess = ExReduxAction<
  PAGE.REMOVE_SUCCESS,
  CmsStore
>;
export type RemoveCmsPagesFailed = ExReduxAction<PAGE.REMOVE_FAILED, string>;

export type FetchCmsPageSelect = ExReduxAction<PAGE.SELECT, string>;

export type SwapCmsPage = ExReduxAction<PAGE.SWAP, Swap>;

export default PAGE;
