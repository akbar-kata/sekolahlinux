import { CmsStore } from 'interfaces/cms';

export function getDetailCms(store: CmsStore) {
  return store;
}
export function getCmsPagesData(store: CmsStore) {
  return store.pages.data;
}

export function getCmsPagesIndex(state: CmsStore): string[] {
  return state.pages.index;
}

export function getCmsPageName(state: CmsStore, label: string) {
  return state.pages.data[label].name;
}
