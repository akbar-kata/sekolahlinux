import PAGE, {
  AddCmsPagesFailedAction,
  FetchCmsPagesFailedAction,
  FetchCmsPagesSuccessAction,
  SwapCmsPage
} from './types';
import { Page } from 'interfaces/cms';

export function selectPage(data: Page) {
  return {
    type: PAGE.SELECT,
    payload: data
  };
}

export function selectPageNoPush(data: Page) {
  return {
    type: PAGE.SELECT_NO_PUSH,
    payload: data
  };
}

function requestPage<T>(action: string, payload?: T) {
  return {
    action,
    payload,
    type: PAGE[`${action.toUpperCase()}_REQUEST`]
  };
}

export function requestPageFail(action: string, error: string) {
  return {
    action,
    type: PAGE[`${action.toUpperCase()}_FAIL`],
    payload: {
      error
    }
  };
}

export function fetchCmsPagesRequest() {
  return {
    type: PAGE.FETCH_REQUEST
  };
}

export function fetchCmsPagesSuccess(
  data: FetchCmsPagesSuccessAction['payload']
) {
  return {
    type: PAGE.FETCH_SUCCESS,
    payload: data
  };
}

export function fetchCmsPagesFailed(
  data: FetchCmsPagesFailedAction['payload']
) {
  return {
    type: PAGE.FETCH_FAILED,
    payload: data
  };
}

export function AddCmsPagesSuccess(data: Page) {
  return {
    type: PAGE.ADD_SUCCESS,
    action: 'add',
    payload: {
      [data.label]: data
    }
  };
}

export function AddCmsPagesFailed(data: AddCmsPagesFailedAction['payload']) {
  return {
    type: PAGE.ADD_FAILED,
    payload: data
  };
}

export function editCmsPagesSuccess(label: string, data: Page) {
  return {
    type: PAGE.EDIT_SUCCESS,
    action: 'edit',
    payload: {
      [label]: data
    }
  };
}

export function removeCmsPagesSuccess(label: string) {
  return {
    type: PAGE.REMOVE_SUCCESS,
    action: 'remove',
    payload: { label }
  };
}

export function addPageRequest(data: Page) {
  return requestPage<{
    data: Page;
  }>('add', { data });
}

export function editPageRequest(data: Page) {
  return requestPage<{
    data: Page;
  }>('edit', { data });
}

export function removePageRequest(data: Page) {
  return requestPage<{
    data: Page;
  }>('remove', { data });
}

export function swapPage(first: number, second: number): SwapCmsPage {
  return {
    type: PAGE.SWAP,
    payload: {
      first,
      second
    }
  };
}
