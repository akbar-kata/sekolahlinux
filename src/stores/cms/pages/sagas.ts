import { all, call, fork, put, select, takeLatest } from 'redux-saga/effects';
import { push } from 'connected-react-router';

import Store from 'interfaces/rootStore';

import PAGE, { FetchCmsPagesRequestAction } from 'stores/cms/pages/types';
import { setLoading } from 'stores/app/loadings/action';
import { callApi } from 'stores/services';
import { fetchCmsPagesSuccess } from 'stores/cms/pages/actions';
import * as CommonAction from 'stores/common/actions';
import { addNotification } from 'stores/app/notification/actions';
import { closeDrawer } from 'stores/app/drawer/actions';

import {
  AddCmsPagesSuccess,
  editCmsPagesSuccess,
  removeCmsPagesSuccess
} from './actions';

function* fetchCmsPages(action: FetchCmsPagesRequestAction) {
  const PROJECT_ID = yield select((store: Store) => store.project.selected);

  yield put(setLoading(PAGE.FETCH_REQUEST, true));

  try {
    const res = yield call(
      callApi,
      'get',
      `/projects/${PROJECT_ID}/cms?getElements=0`
    );
    yield put(fetchCmsPagesSuccess(res));
    yield put(setLoading(PAGE.FETCH_REQUEST, false));
  } catch (err) {
    yield put(CommonAction.failed(PAGE.FETCH_REQUEST, err));
    const res: any = {
      detail: {
        id: '',
        revision: '',
        modules: '',
        created_at: '',
        updated_at: '',
        settings: {}
      },
      pages: {}
    };
    yield put(fetchCmsPagesSuccess(res));
    yield put(
      addNotification({
        title: 'Error',
        message: 'Error fetching list of forms',
        status: 'error',
        dismissible: true,
        dismissAfter: 5000
      })
    );
  } finally {
    yield put(setLoading(PAGE.FETCH_REQUEST, false));
  }
}

function* saveCmsPages({ payload }: any) {
  const PROJECT_ID = yield select((store: Store) => store.project.selected);
  const revision = yield select((store: Store) => store.cms.detail.revision);
  yield put(setLoading(PAGE.ADD_REQUEST, true));

  try {
    const resp = yield call(
      callApi,
      'put',
      `/projects/${PROJECT_ID}/cms/revisions/${revision}?command=add&type=page`,
      {
        name: payload.data.name,
        description: payload.data.description,
        label: payload.data.label,
        index: 1
      }
    );
    yield put(AddCmsPagesSuccess(resp));
    yield put(closeDrawer('drawer-new-page'));
    yield put(
      addNotification({
        title: 'Success',
        message: `You have successfully created ${payload.data.name} page.`,
        status: 'success',
        dismissible: true,
        dismissAfter: 5000
      })
    );
  } catch (e) {
    yield put(CommonAction.failed(PAGE.ADD_REQUEST, e));
    yield put(
      addNotification({
        title: 'Error',
        message: `Ooops... Something went wrong! You failed to create ${
          payload.name
        } page.`,
        status: 'error',
        dismissible: true,
        dismissAfter: 5000
      })
    );
  } finally {
    yield put(setLoading(PAGE.ADD_REQUEST, false));
  }
}

function* updateCmsPages({ payload }: any) {
  const PROJECT_ID = yield select((store: Store) => store.project.selected);
  const revision = yield select((store: Store) => store.cms.detail.revision);
  yield put(setLoading(PAGE.EDIT_REQUEST, true));

  try {
    const resp = yield call(
      callApi,
      'put',
      `/projects/${PROJECT_ID}/cms/revisions/${revision}?command=edit&type=page`,
      {
        name: payload.data.name,
        description: payload.data.description,
        label: payload.data.label,
        cmsId: payload.data.cmsId,
        id: payload.data.id,
        revision: payload.data.revision,
        index: 1
      }
    );
    yield put(editCmsPagesSuccess(payload.data.label, resp));
    yield put(closeDrawer('drawer-edit-page'));
    yield put(
      addNotification({
        title: 'Success',
        message: `You have successfully updated ${payload.data.name} page.`,
        status: 'success',
        dismissible: true,
        dismissAfter: 5000
      })
    );
  } catch (e) {
    yield put(CommonAction.failed(PAGE.EDIT_REQUEST, e));
    yield put(
      addNotification({
        title: 'Error',
        message: `Ooops... Something went wrong! You failed to update ${
          payload.data.name
        } page.`,
        status: 'error',
        dismissible: true,
        dismissAfter: 5000
      })
    );
  } finally {
    yield put(setLoading(PAGE.EDIT_REQUEST, false));
  }
}

function* removeCmsPages({ payload }: any) {
  const PROJECT_ID = yield select((store: Store) => store.project.selected);
  const revision = yield select((store: Store) => store.cms.detail.revision);
  yield put(setLoading(PAGE.REMOVE_REQUEST, true));
  try {
    yield call(
      callApi,
      'put',
      `/projects/${PROJECT_ID}/cms/revisions/${revision}?command=delete&type=page`,
      {
        id: payload.data.id,
        revision: payload.data.revision
      }
    );
    yield put(removeCmsPagesSuccess(payload.data.label));

    yield put(
      addNotification({
        title: 'Success',
        message: `${payload.data.name} page has been deleted.`,
        status: 'success',
        dismissible: true,
        dismissAfter: 5000
      })
    );
  } catch (err) {
    yield put(CommonAction.failed(PAGE.REMOVE_REQUEST, err));
    yield put(
      addNotification({
        title: 'Error',
        message: `Ooops... Something went wrong! You failed to update ${
          payload.data.name
        } page.`,
        status: 'error',
        dismissible: true,
        dismissAfter: 5000
      })
    );
  } finally {
    yield put(setLoading(PAGE.REMOVE_REQUEST, false));
  }
}

function* selectPageHandler({ payload }: any) {
  const PROJECT_ID = yield select((store: Store) => store.project.selected);
  try {
    yield put(push(`/project/${PROJECT_ID}/cms/pages/${payload.label}/forms`));
    // yield put(
    //   addNotification({
    //     title: 'Success',
    //     message: `${payload.name} page has been selected.`,
    //     status: 'success',
    //     dismissible: true,
    //     dismissAfter: 5000
    //   })
    // );
  } catch (err) {
    // yield put(
    //   addNotification({
    //     title: 'Error',
    //     message: `Ooops... Something went wrong! You failed to select ${
    //       payload.name
    //     } page.`,
    //     status: 'error',
    //     dismissible: true,
    //     dismissAfter: 5000
    //   })
    // );
  }
}

function* swapPageForm({ payload }: any) {
  const PROJECT_ID = yield select((store: Store) => store.project.selected);
  const revision = yield select((store: Store) => store.cms.detail.revision);
  const pageIndexes: string[] = yield select(
    (store: Store) => store.cms.pages.index
  );
  const pageData = yield select((store: Store) => store.cms.pages.data);
  const newSortedPage = pageIndexes.map((id, index) => {
    return { index, id: pageData[id].id };
  });
  try {
    yield call(
      callApi,
      'put',
      `/projects/${PROJECT_ID}/cms/revisions/${revision}?command=edit&type=page`,
      newSortedPage as any
    );
  } catch (e) {
    yield put(CommonAction.failed(PAGE.SWAP, e));
    yield put(
      addNotification({
        title: 'Error',
        message: `Ooops... Something went wrong! You failed to update ${
          payload.data.name
        } form.`,
        status: 'error',
        dismissible: true,
        dismissAfter: 5000
      })
    );
  }
}

function* watchPageSwap() {
  yield takeLatest(PAGE.SWAP, swapPageForm);
}

function* watchPageSelected() {
  yield takeLatest(PAGE.SELECT, selectPageHandler);
}

function* watchFetchCmsPages() {
  yield takeLatest(PAGE.FETCH_REQUEST, fetchCmsPages);
}

function* watchSaveCmsPages() {
  yield takeLatest(PAGE.ADD_REQUEST, saveCmsPages);
}

function* watchUpdateCmsPages() {
  yield takeLatest(PAGE.EDIT_REQUEST, updateCmsPages);
}

function* watchRemoveCmsPages() {
  yield takeLatest(PAGE.REMOVE_REQUEST, removeCmsPages);
}

export default function* root() {
  yield all([
    fork(watchFetchCmsPages),
    fork(watchSaveCmsPages),
    fork(watchUpdateCmsPages),
    fork(watchRemoveCmsPages),
    fork(watchPageSelected),
    fork(watchPageSwap)
  ]);
}
