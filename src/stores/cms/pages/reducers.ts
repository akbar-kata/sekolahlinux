import { combineReducers, Reducer } from 'redux';

import { CmsPagesStore } from 'interfaces/cms';
import PAGE from 'stores/cms/pages/types';
import { ReduxAction } from 'interfaces';
import { Swap } from 'interfaces/common';

const data: Reducer<CmsPagesStore['data'], ReduxAction> = (
  state = {},
  { type, action, payload }
) => {
  switch (type) {
    case PAGE.FETCH_SUCCESS:
      return payload.pages;
    case PAGE.ADD_SUCCESS:
    case PAGE.EDIT_SUCCESS:
      return Object.assign({}, state, payload);
    case PAGE.REMOVE_SUCCESS:
      const { [payload.label as string]: deleted, ...rest } = state;
      return rest;
    case PAGE.FETCH_FAILED:
      return Object.assign({}, state, {});
    default:
      return state;
  }
};

const index: Reducer<CmsPagesStore['index'], ReduxAction> = (
  state = [],
  { type, payload }
) => {
  switch (type) {
    case PAGE.FETCH_SUCCESS:
      return Object.keys(payload.pages).sort(
        (a, b) => payload.pages[a].index - payload.pages[b].index
      );
    case PAGE.ADD_SUCCESS:
      return [...state, ...Object.keys(payload)];
    case PAGE.REMOVE_SUCCESS:
      return state.filter(id => id !== (payload.label as string));
    case PAGE.FETCH_FAILED:
      return Object.assign({}, state, []);
    case PAGE.SWAP:
      const copyState = [...state];
      const swapArg = payload as Swap;
      const secondCopy = copyState[swapArg.second];
      copyState[swapArg.second] = copyState[swapArg.first];
      copyState[swapArg.first] = secondCopy;
      return copyState;
    default:
      return state;
  }
};

const selected: Reducer<CmsPagesStore['selected'], ReduxAction> = (
  state: string | null = null,
  { type, payload }
) => {
  switch (type) {
    case PAGE.SELECT:
    case PAGE.SELECT_NO_PUSH:
      return payload.id;
    default:
      return state;
  }
};

const label: Reducer<CmsPagesStore['label'], ReduxAction> = (
  state: string | null = null,
  { type, payload }
) => {
  switch (type) {
    case PAGE.SELECT:
    case PAGE.SELECT_NO_PUSH:
      return payload.label;
    default:
      return state;
  }
};

const pageName: Reducer<CmsPagesStore['pageName'], ReduxAction> = (
  state: string | null = null,
  { type, payload }: ReduxAction
) => {
  switch (type) {
    case PAGE.SELECT:
    case PAGE.SELECT_NO_PUSH:
      return payload.name;
    default:
      return state;
  }
};

export default combineReducers<CmsPagesStore>({
  data,
  index,
  selected,
  label,
  pageName
});
