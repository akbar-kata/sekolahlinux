import { combineReducers } from 'redux';

import * as Store from 'interfaces/cms';
import FORM from './types';
import { ReduxAction } from 'interfaces';
import { Swap } from 'interfaces/common';

const data = (
  state: Record<string, Store.Form> = {},
  { type, action, payload }: ReduxAction
) => {
  switch (type) {
    case FORM.FETCH_SUCCESS:
      return payload.forms;
    case FORM.ADD_SUCCESS:
      return Object.assign({}, state, payload);
    case FORM.EDIT_SUCCESS:
      return Object.assign({}, state, payload);
    case FORM.REMOVE_SUCCESS:
      const { [payload.label as string]: deleted, ...rest } = state;
      return rest;
    case FORM.FETCH_FAILED:
      return Object.assign({}, state, {});
    default:
      return state;
  }
};

const index = (state: string[] = [], { type, payload }: ReduxAction) => {
  switch (type) {
    case FORM.FETCH_SUCCESS:
      return Object.keys(payload.forms).sort(
        (a, b) => payload.forms[a].index - payload.forms[b].index
      );
    case FORM.ADD_SUCCESS:
      return [...state, ...Object.keys(payload)];
    case FORM.REMOVE_SUCCESS:
      return state.filter(id => id !== (payload.label as string));
    case FORM.FETCH_FAILED:
      return Object.assign({}, state, []);
    case FORM.SWAP_FORM:
      const swapArg = payload as Swap;
      const secondCopy = state[swapArg.second];
      state[swapArg.second] = state[swapArg.first];
      state[swapArg.first] = secondCopy;
      return state;
    default:
      return state;
  }
};

const selected = (
  state: string | null = null,
  { type, payload }: ReduxAction
) => {
  switch (type) {
    case FORM.SELECT:
      return payload.id;
    default:
      return state;
  }
};

const label = (state: string | null = null, { type, payload }: ReduxAction) => {
  switch (type) {
    case FORM.SELECT:
      return payload.label;
    default:
      return state;
  }
};

export default combineReducers<Store.CmsFormsStore>({
  data,
  index,
  selected,
  label
});
