import { all, fork, put, takeLatest, call, select } from 'redux-saga/effects';
import { push } from 'connected-react-router';

import Store from 'interfaces/rootStore';

import { Page as PageData } from 'interfaces/cms';
import FORM from 'stores/cms/forms/types';
import PAGE from 'stores/cms/pages/types';
import { setLoading } from 'stores/app/loadings/action';
import { callApi } from 'stores/services';
import { fetchCmsFormsSuccess } from 'stores/cms/forms/actions';
import * as CommonAction from 'stores/common/actions';
import { addNotification } from 'stores/app/notification/actions';
import { closeDrawer } from 'stores/app/drawer/actions';

import {
  AddCmsFormsSuccess,
  editCmsFormsSuccess,
  removeCmsFormsSuccess
} from './actions';
import { TypedReduxAction } from 'interfaces';
import { selectPage } from '../pages/actions';

function* fetchCmsForms({ payload }: TypedReduxAction<PageData>) {
  yield put(setLoading(FORM.FETCH_REQUEST, true));
  const PROJECT_ID = yield select((store: Store) => store.project.selected);

  try {
    const res = yield call(
      callApi,
      'get',
      `/projects/${PROJECT_ID}/cms?getElements=0`
    );
    if (Object.keys(res.pages).length > 0) {
      yield put(fetchCmsFormsSuccess(res.pages[payload.label]));
      yield put(selectPage(res.pages[payload.label]));
    }
    yield put(setLoading(PAGE.FETCH_REQUEST, false));
  } catch (err) {
    yield put(CommonAction.failed(FORM.FETCH_REQUEST, err));
    const res: any = {
      forms: {}
    };
    yield put(fetchCmsFormsSuccess(res));
  } finally {
    yield put(setLoading(FORM.FETCH_REQUEST, false));
  }
}

function* saveCmsForms({ payload }: any) {
  const PROJECT_ID = yield select((store: Store) => store.project.selected);
  const revision = yield select((store: Store) => store.cms.detail.revision);
  const pageId = yield select((store: Store) => store.cms.pages.selected);
  yield put(setLoading(FORM.ADD_REQUEST, true));

  try {
    const resp = yield call(
      callApi,
      'put',
      `/projects/${PROJECT_ID}/cms/revisions/${revision}?command=add&type=form`,
      {
        pageId,
        name: payload.data.name,
        label: payload.data.label,
        index: 1
      }
    );
    yield put(AddCmsFormsSuccess(resp));
    yield put(closeDrawer('drawer-new-form'));
    yield put(
      addNotification({
        title: 'Success',
        message: `You have successfully created ${payload.data.name} form.`,
        status: 'success',
        dismissible: true,
        dismissAfter: 5000
      })
    );
  } catch (e) {
    yield put(CommonAction.failed(FORM.ADD_REQUEST, e));
    yield put(
      addNotification({
        title: 'Error',
        message: `Ooops... Something went wrong! You failed to create ${
          payload.name
        } form.`,
        status: 'error',
        dismissible: true,
        dismissAfter: 5000
      })
    );
  } finally {
    yield put(setLoading(FORM.ADD_REQUEST, false));
  }
}

function* updateCmsForms({ payload }: any) {
  const PROJECT_ID = yield select((store: Store) => store.project.selected);
  const revision = yield select((store: Store) => store.cms.detail.revision);
  const pageId = yield select((store: Store) => store.cms.pages.selected);
  yield put(setLoading(FORM.EDIT_REQUEST, true));
  try {
    const resp = yield call(
      callApi,
      'put',
      `/projects/${PROJECT_ID}/cms/revisions/${revision}?command=edit&type=form`,
      {
        pageId,
        name: payload.data.name,
        label: payload.data.label,
        id: payload.data.id,
        revision: payload.data.revision,
        index: 1
      }
    );
    yield put(editCmsFormsSuccess(payload.data.label, resp));
    yield put(closeDrawer('drawer-edit-form'));
    yield put(
      addNotification({
        title: 'Success',
        message: `You have successfully updated ${payload.data.name} form.`,
        status: 'success',
        dismissible: true,
        dismissAfter: 5000
      })
    );
  } catch (e) {
    yield put(CommonAction.failed(FORM.EDIT_REQUEST, e));
    yield put(
      addNotification({
        title: 'Error',
        message: `Ooops... Something went wrong! You failed to update ${
          payload.data.name
        } form.`,
        status: 'error',
        dismissible: true,
        dismissAfter: 5000
      })
    );
  } finally {
    yield put(setLoading(FORM.EDIT_REQUEST, false));
  }
}

function* removeCmsForms({ payload }: any) {
  const PROJECT_ID = yield select((store: Store) => store.project.selected);
  const revision = yield select((store: Store) => store.cms.detail.revision);
  yield put(setLoading(FORM.REMOVE_REQUEST, true));
  try {
    yield call(
      callApi,
      'put',
      `/projects/${PROJECT_ID}/cms/revisions/${revision}?command=delete&type=form`,
      {
        id: payload.data.id,
        revision: payload.data.revision
      }
    );
    yield put(removeCmsFormsSuccess(payload.data.label));
    yield put(
      addNotification({
        title: 'Success',
        message: `${payload.data.name} form has been deleted.`,
        status: 'success',
        dismissible: true,
        dismissAfter: 5000
      })
    );
  } catch (err) {
    yield put(CommonAction.failed(FORM.REMOVE_REQUEST, err));
    yield put(
      addNotification({
        title: 'Error',
        message: `Ooops... Something went wrong! You failed to update ${
          payload.data.name
        } page.`,
        status: 'error',
        dismissible: true,
        dismissAfter: 5000
      })
    );
  } finally {
    yield put(setLoading(FORM.REMOVE_REQUEST, false));
  }
}

function* selectFormHandler({ payload }: any) {
  const PROJECT_ID = yield select((store: Store) => store.project.selected);
  const revision = yield select((store: Store) => store.cms.detail.revision);
  const label = yield select((store: Store) => store.cms.pages.label);

  try {
    yield put(
      push(
        `/project/${PROJECT_ID}/cms/pages/${label}/forms/${
          payload.id
        }/elements/${revision}`
      )
    );
    // yield put(
    //   addNotification({
    //     title: 'Success',
    //     message: `${payload.name} form has been selected.`,
    //     status: 'success',
    //     dismissible: true,
    //     dismissAfter: 5000
    //   })
    // );
  } catch (err) {
    yield put(
      addNotification({
        title: 'Error',
        message: `Ooops... Something went wrong! You failed to select ${
          payload.name
        } page.`,
        status: 'error',
        dismissible: true,
        dismissAfter: 5000
      })
    );
  }
}

function* swapCmsForm({ payload }: any) {
  const PROJECT_ID = yield select((store: Store) => store.project.selected);
  const revision = yield select((store: Store) => store.cms.detail.revision);
  const formIndexes: string[] = yield select(
    (store: Store) => store.cms.forms.index
  );
  const formData = yield select((store: Store) => store.cms.forms.data);
  const newSortedForm = formIndexes.map((id, index) => {
    return { index, id: formData[id].id };
  });
  try {
    yield call(
      callApi,
      'put',
      `/projects/${PROJECT_ID}/cms/revisions/${revision}?command=edit&type=form`,
      newSortedForm as any
    );
  } catch (e) {
    yield put(CommonAction.failed(FORM.EDIT_REQUEST, e));
    yield put(
      addNotification({
        title: 'Error',
        message: `Ooops... Something went wrong! You failed to update ${
          payload.data.name
        } form.`,
        status: 'error',
        dismissible: true,
        dismissAfter: 5000
      })
    );
  } finally {
    yield put(setLoading(FORM.EDIT_REQUEST, false));
  }
}

function* watchFormSelected() {
  yield takeLatest(FORM.SELECT, selectFormHandler);
}
function* watchFormSwap() {
  yield takeLatest(FORM.SWAP_FORM, swapCmsForm);
}

function* watchFetchCmsForms() {
  yield takeLatest(FORM.FETCH_REQUEST, fetchCmsForms);
}

function* watchSaveCmsForms() {
  yield takeLatest(FORM.ADD_REQUEST, saveCmsForms);
}

function* watchUpdateCmsForms() {
  yield takeLatest(FORM.EDIT_REQUEST, updateCmsForms);
}

function* watchRemoveCmsForms() {
  yield takeLatest(FORM.REMOVE_REQUEST, removeCmsForms);
}

export default function* root() {
  yield all([
    fork(watchFetchCmsForms),
    fork(watchSaveCmsForms),
    fork(watchUpdateCmsForms),
    fork(watchRemoveCmsForms),
    fork(watchFormSelected),
    fork(watchFormSwap)
  ]);
}
