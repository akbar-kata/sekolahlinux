import { ExReduxAction } from 'interfaces';
import { Form } from 'interfaces/cms';
import { Swap } from 'interfaces/common';

enum FORM {
  FETCH_REQUEST = 'CMS/FORM/FETCH_REQUEST',
  FETCH_SUCCESS = 'CMS/FORM/FETCH_SUCCESS',
  FETCH_FAILED = 'CMS/FORM/FETCH_FAILED',

  ADD_REQUEST = 'CMS/FORM/ADD_REQUEST',
  ADD_SUCCESS = 'CMS/FORM/ADD_SUCCESS',
  ADD_FAILED = 'CMS/FORM/ADD_FAILED',

  EDIT_REQUEST = 'CMS/FORM/EDIT_REQUEST',
  EDIT_SUCCESS = 'CMS/FORM/EDIT_SUCCESS',
  EDIT_FAILED = 'CMS/FORM/EDIT_FAILED',

  REMOVE_REQUEST = 'CMS/FORM/REMOVE_REQUEST',
  REMOVE_SUCCESS = 'CMS/FORM/REMOVE_SUCCESS',
  REMOVE_FAILED = 'CMS/FORM/REMOVE_FAILED',

  SELECT = 'CMS/FORM/SELECT',

  SWAP_FORM = 'CMS/FORM/SWAP'
}

export type FetchCmsFormsRequest = ExReduxAction<FORM.FETCH_REQUEST, string>;
export type FetchCmsFormsSuccess = ExReduxAction<FORM.FETCH_SUCCESS, Form>;
export type FetchCmsFormsFailedAction = ExReduxAction<
  FORM.FETCH_FAILED,
  string
>;

export type AddCmsFormsRequestAction = ExReduxAction<FORM.ADD_REQUEST, string>;
export type AddCmsFormsSuccessAction = ExReduxAction<FORM.ADD_SUCCESS, Form>;
export type AddCmsFormsFailedAction = ExReduxAction<FORM.ADD_FAILED, string>;

export type EditCmsFormsRequest = ExReduxAction<FORM.EDIT_REQUEST, string>;
export type EditCmsFormsSuccess = ExReduxAction<FORM.EDIT_SUCCESS, Form>;
export type EditCmsFormsFailed = ExReduxAction<FORM.EDIT_FAILED, string>;

export type RemoveCmsFormsRequest = ExReduxAction<FORM.REMOVE_REQUEST, string>;
export type RemoveCmsFormsSuccess = ExReduxAction<FORM.REMOVE_SUCCESS, Form>;
export type RemoveCmsFormsFailed = ExReduxAction<FORM.REMOVE_FAILED, string>;

export type FetchCmsPageSelect = ExReduxAction<FORM.SELECT, string>;

export type SwapCmsForm = ExReduxAction<FORM.SWAP_FORM, Swap>;

export default FORM;
