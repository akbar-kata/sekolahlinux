import {
  AddCmsFormsFailedAction,
  FetchCmsFormsFailedAction,
  FetchCmsFormsSuccess,
  SwapCmsForm
} from './types';
import { Form } from 'interfaces/cms';
import FORM from 'stores/cms/forms/types';

export function selectForm(data: Form) {
  return {
    type: FORM.SELECT,
    payload: data
  };
}

function requestForm<T>(action: string, payload?: T) {
  return {
    action,
    payload,
    type: FORM[`${action.toUpperCase()}_REQUEST`]
  };
}

export function requestFormFail(action: string, error: string) {
  return {
    action,
    type: FORM[`${action.toUpperCase()}_FAIL`],
    payload: {
      error
    }
  };
}

export function fetchCmsFormsRequest(label: string) {
  return {
    type: FORM.FETCH_REQUEST,
    payload: {
      label
    }
  };
}

export function fetchCmsFormsSuccess(data: FetchCmsFormsSuccess['payload']) {
  return {
    type: FORM.FETCH_SUCCESS,
    payload: data
  };
}

export function fetchCmsFormsFailed(
  data: FetchCmsFormsFailedAction['payload']
) {
  return {
    type: FORM.FETCH_FAILED,
    payload: data
  };
}

export function AddCmsFormsSuccess(data: Form) {
  return {
    type: FORM.ADD_SUCCESS,
    action: 'add',
    payload: {
      [data.label]: data
    }
  };
}

export function AddCmsFormsFailed(data: AddCmsFormsFailedAction['payload']) {
  return {
    type: FORM.ADD_FAILED,
    payload: data
  };
}

export function editCmsFormsSuccess(label: string, data: Form) {
  return {
    type: FORM.EDIT_SUCCESS,
    action: 'edit',
    payload: {
      [label]: data
    }
  };
}

export function removeCmsFormsSuccess(label: string) {
  return {
    type: FORM.REMOVE_SUCCESS,
    action: 'remove',
    payload: { label }
  };
}

export function addFormRequest(data: Form) {
  return requestForm<{
    data: Form;
  }>('add', { data });
}

export function editFormRequest(data: Form) {
  return requestForm<{
    data: Form;
  }>('edit', { data });
}

export function removeFormRequest(data: Form) {
  return requestForm<{
    data: Form;
  }>('remove', { data });
}

export function swapForm(first: number, second: number): SwapCmsForm {
  return {
    type: FORM.SWAP_FORM,
    payload: {
      first,
      second
    }
  };
}
