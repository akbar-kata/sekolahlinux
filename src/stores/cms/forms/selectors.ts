import { CmsStore } from 'interfaces/cms';

export function getCmsFormsData(store: CmsStore) {
  return store.forms.data;
}

export function getCmsFormsIndex(state: CmsStore): string[] {
  return state.forms.index;
}

export function getCmsFormName(state: CmsStore, label: string) {
  return state.forms.data[label].name;
}

export function getDetailForm(state: CmsStore, label: string) {
  return state.forms.data[label];
}
