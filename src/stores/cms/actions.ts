import CMS from './types';
import { CmsData } from 'interfaces/cms';

function requestCms<T>(action: string, payload?: T) {
  return {
    action,
    payload,
    type: CMS[`${action.toUpperCase()}_REQUEST`]
  };
}

export function requestCmsSettings() {
  return {
    type: CMS.FETCH_REQUEST
  };
}

export function editCmsSettingsRequest(data: CmsData) {
  return requestCms<{}>('edit', data);
}

export function fetchCmsSettingsSuccess(data: CmsData) {
  return {
    type: CMS.FETCH_SUCCESS,
    payload: {
      data
    }
  };
}
export function editCmsSettingsSuccess(data: CmsData) {
  return {
    type: CMS.EDIT_SUCCESS,
    payload: { data }
  };
}
