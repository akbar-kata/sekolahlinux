import { TypedReduxAction } from 'interfaces';
import { LOCATION_CHANGE, push, RouterState } from 'connected-react-router';
import { all, call, fork, put, select, takeLatest } from 'redux-saga/effects';
import get from 'lodash-es/get';

import { UserData } from 'interfaces/cms';
import { default as CMS_USER } from './types';
import * as CommonAction from 'stores/common/actions';
import COMMON from 'stores/common/types';
import Store from 'interfaces/rootStore';
import { setSelected } from './actions';
import { callApi } from 'stores/services';
import { setLoading } from 'stores/app/loadings/action';
import { setData } from 'stores/cms/users/actions';
import { addNotification } from 'stores/app/notification/actions';
import { closeDrawer } from 'stores/app/drawer/actions';
import { DRAWER_ID } from 'modules/cms/users/NewUserForm';

function* saveUser({ payload }: any) {
  const PROJECT_ID = yield select((store: Store) => store.project.selected);

  try {
    yield put(setLoading(CMS_USER.SAVE_USER, true));
    const response = yield call(
      callApi,
      'post',
      `cms/${PROJECT_ID}/users/${payload.email}`,
      {
        namespace: payload.namespace,
        environmentName: payload.environmentName
      }
    );
    yield put(
      addNotification({
        title: 'Success',
        message: `You have successfully added ${payload.email}.`,
        status: 'success',
        dismissible: true,
        dismissAfter: 5000
      })
    );
    yield put(CommonAction.success(CMS_USER.SAVE_USER, response));
    yield put(setLoading(CMS_USER.SAVE_USER, false));
    yield put(CommonAction.request(CMS_USER.FETCH_DATA, payload));
    yield put(closeDrawer(DRAWER_ID));
  } catch (e) {
    yield put(CommonAction.failed(CMS_USER.SAVE_USER, e));
    const errorMessage = get(
      e,
      'response.data.message',
      'Failed to invite user to environment.'
    );
    // const res = errorMessage.match(/unique/g);
    // if (res.length > 0) {
    //   yield put(
    //     addNotification({
    //       title: `Failed`,
    //       message: `the email <strong>${
    //         payload.email
    //       } </strong> is already invited`,
    //       status: 'error',
    //       allowHTML: true,
    //       dismissible: true,
    //       dismissAfter: 5000
    //     })
    //   );
    // } else {
    yield put(
      addNotification({
        title: `Failed`,
        message: errorMessage,
        status: 'error',
        allowHTML: true,
        dismissible: true,
        dismissAfter: 5000
      })
    );
    // }
  } finally {
    yield put(setLoading(CMS_USER.SAVE_USER, false));
  }
}

function* fetchUserData() {
  const PROJECT_ID = yield select((store: Store) => store.project.selected);
  yield put(setLoading(CMS_USER.FETCH_DATA, true));
  try {
    const users: UserData[] = yield call(
      callApi,
      'get',
      `/cms/${PROJECT_ID}/users`
    );
    const usersAsMap = {};
    users.forEach(data => {
      if (data.id) {
        usersAsMap[data.id] = data;
      }
    });
    yield put(setData(usersAsMap));
    yield put(CommonAction.success(CMS_USER.FETCH_DATA, usersAsMap));
  } catch (e) {
    yield put(
      addNotification({
        title: 'Error',
        message: `Error fetching list of user on CMS`,
        status: 'error',
        dismissible: true,
        dismissAfter: 5000
      })
    );
  } finally {
    yield put(setLoading(CMS_USER.FETCH_DATA, false));
  }
}

function* deleteData({ payload }: TypedReduxAction<string>) {
  const PROJECT_ID = yield select((store: Store) => store.project.selected);
  yield put(setLoading(CMS_USER.DELETE_DATA, true));
  try {
    const success = yield call(
      callApi,
      'delete',
      `/cms/${PROJECT_ID}/users/${payload}`
    );
    if (success) {
      yield put(CommonAction.success(CMS_USER.DELETE_DATA));
      yield put(
        addNotification({
          title: 'Success',
          message: `User removed from CMS`,
          status: 'success',
          dismissible: true,
          dismissAfter: 5000
        })
      );
    } else {
      yield put(
        addNotification({
          title: 'Error',
          message: `Error removing user from CMS`,
          status: 'error',
          dismissible: true,
          dismissAfter: 5000
        })
      );
    }
    yield put(CommonAction.request(CMS_USER.FETCH_DATA, payload));
  } catch (e) {
    yield put(CommonAction.failed(CMS_USER.DELETE_DATA, e));
    yield put(
      addNotification({
        title: 'Error',
        message: `Error removing user from CMS`,
        status: 'error',
        dismissible: true,
        dismissAfter: 5000
      })
    );
  } finally {
    yield put(setLoading(CMS_USER.DELETE_DATA, false));
  }
}

function* pushSelectedUserUrl({ payload }: TypedReduxAction<string>) {
  yield put(push(`users/${payload}`));
}

function* setSelectedUserFromUrl({ payload }: TypedReduxAction<RouterState>) {
  if (payload && payload.location && payload.location.pathname) {
    const rx = /\/cms\/([0-9]+)\/users\/([0-9]+)/;
    const match = rx.exec(payload.location.pathname);
    if (match) {
      yield put(setSelected(match[2], false));
    }
  }
}

function* watchSaveUser() {
  yield takeLatest(
    CommonAction.build(COMMON._REQUEST, CMS_USER.SAVE_USER),
    saveUser
  );
}

function* watchFetchUserData() {
  yield takeLatest(
    CommonAction.build(COMMON._REQUEST, CMS_USER.FETCH_DATA),
    fetchUserData
  );
}

function* watchUserSelected() {
  yield takeLatest(CMS_USER.SET_SELECTED_USER, pushSelectedUserUrl);
}

function* watchLocationChanged() {
  yield takeLatest(LOCATION_CHANGE, setSelectedUserFromUrl);
}

function* watchDelete() {
  yield takeLatest(
    CommonAction.build(COMMON._REQUEST, CMS_USER.DELETE_DATA),
    deleteData
  );
}

export default function*() {
  yield all([
    fork(watchSaveUser),
    fork(watchFetchUserData),
    fork(watchLocationChanged),
    fork(watchDelete),
    fork(watchUserSelected)
  ]);
}
