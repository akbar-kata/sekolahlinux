import { combineReducers } from 'redux';

import { TypedReduxAction } from 'interfaces';
import { UserData, CmsUsersStore } from 'interfaces/cms';
import CMS_USER from './types';
import { DataMap } from 'interfaces/common';

const data = (
  state: CmsUsersStore['data'] = {},
  { type, payload }: TypedReduxAction<DataMap<UserData>>
) => {
  switch (type) {
    case CMS_USER.SET_DATA:
      return payload;
    default:
      return state;
  }
};

const index = (
  state: CmsUsersStore['index'] = [],
  { type, payload }: TypedReduxAction<string>
) => {
  switch (type) {
    case CMS_USER.SET_DATA:
      return Object.keys(payload).map(key => payload[key].id);
    default:
      return state;
  }
};

const selected = (
  state: CmsUsersStore['selected'] = null,
  { type, payload }: TypedReduxAction<string | null>
) => {
  switch (type) {
    case CMS_USER.SET_SELECTED_USER:
      return payload;
    default:
      return state;
  }
};

export default combineReducers<CmsUsersStore>({
  data,
  index,
  selected
});
