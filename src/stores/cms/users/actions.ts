import { UserData } from 'interfaces/cms';
import { default as CMS_USER } from './types';
import { DataMap } from 'interfaces/common';

export function setData(data: DataMap<UserData>) {
  return {
    type: CMS_USER.SET_DATA,
    payload: data
  };
}

export function setSelected(data: string | null, push: boolean = true) {
  return {
    type: push
      ? CMS_USER.SET_SELECTED_USER
      : CMS_USER.SET_SELECTED_USER_NO_PUSH,
    payload: data
  };
}
