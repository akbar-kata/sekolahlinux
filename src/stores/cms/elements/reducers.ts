import { combineReducers, Reducer } from 'redux';

import ELEMENT from './types';
import { CmsElementsStore } from 'interfaces/cms';
import { ReduxAction } from 'interfaces';

const initialState: CmsElementsStore = {
  elements: []
};

const elements: Reducer<CmsElementsStore['elements'], ReduxAction> = (
  state = initialState.elements,
  { type, payload }
) => {
  switch (type) {
    case ELEMENT.SET_ELEMENTS:
      return Object.values(payload.data);
    case ELEMENT.SET_ELEMENT_DATA:
      if (typeof state[payload.index][payload.field] === 'object') {
        state[payload.index][payload.field] = {
          ...state[payload.index][payload.field],
          ...payload.data
        };
      } else {
        state[payload.index][payload.field] = payload.data;
      }
      return state;
    default:
      return state;
  }
};

export default combineReducers<CmsElementsStore>({
  elements
});
