import { ExReduxAction } from 'interfaces';

enum ELEMENT {
  FETCH_REQUEST = 'CMS/ELEMENT/FETCH_REQUEST',
  FETCH_SUCCESS = 'CMS/ELEMENT/FETCH_SUCCESS',
  FETCH_FAILED = 'CMS/ELEMENT/FETCH_FAILED',

  SELECT = 'CMS/ELEMENT/SELECT',
  SAVE_ELEMENTS = 'CMS/ELEMENT/SAVE_ELEMENTS',
  FETCH_ELEMENTS = 'CMS/ELEMENT/FETCH_ELEMENTS',
  DELETE_ELEMENT = 'CMS/ELEMENT/DELETE_ELEMENT',
  SET_ELEMENTS = 'CMS/ELEMENT/SET_ELEMENTS',
  SET_ELEMENT_DATA = 'CMS/FORM_SET_ELEMENT_DATA'
}

export type FetchCmsElementsRequest = ExReduxAction<
  ELEMENT.FETCH_REQUEST,
  string
>;
export type FetchCmsElementsSuccess = ExReduxAction<
  ELEMENT.FETCH_SUCCESS,
  Element
>;
export type FetchCmsElementsFailed = ExReduxAction<
  ELEMENT.FETCH_FAILED,
  string
>;

export type FetchCmsPageSelect = ExReduxAction<ELEMENT.SELECT, string>;

export default ELEMENT;
