import { all, call, fork, put, select, takeLatest } from 'redux-saga/effects';

import ELEMENT from 'stores/cms/elements/types';
import { setLoading } from 'stores/app/loadings/action';
import { callApi } from 'stores/services';
import {
  setElement,
  fetchCmsElementsRequest
} from 'stores/cms/elements/actions';
import * as CommonAction from 'stores/common/actions';
import { addNotification } from 'stores/app/notification/actions';
import Store from 'interfaces/rootStore';

import COMMON from 'stores/common/types';
import { ElementMetadata } from 'interfaces/cms';
import { TypedReduxAction } from 'interfaces';

// import { fetchCmsFormsSuccess, selectForm } from '../forms/actions';

function* fetchCmsElements({ payload }: TypedReduxAction<ElementMetadata>) {
  yield put(setLoading(ELEMENT.FETCH_REQUEST, true));
  try {
    const PROJECT_ID = yield select((store: Store) => store.project.selected);
    const res = yield call(
      callApi,
      'get',
      `/projects/${PROJECT_ID}/cms/revisions/${payload.revision}/forms/${
        payload.formId
      }/elements?limit=1000000`
    );
    if (res.data) {
      yield put(setElement(res));
    }
    yield put(setLoading(ELEMENT.FETCH_REQUEST, false));
  } catch (err) {
    yield put(CommonAction.failed(ELEMENT.FETCH_REQUEST, err));
    const res: any = {
      forms: {},
      elements: {}
    };
    yield put(setElement(res));
  } finally {
    yield put(setLoading(ELEMENT.FETCH_REQUEST, false));
  }
}

function transformFormData(formData: any, index: number, formId?: string) {
  return {
    formId,
    index,
    name: formData.name,
    label: formData.label,
    type: formData.type,
    meta: formData.meta
  };
}

function* saveElements({
  payload
}: TypedReduxAction<{
  elements: ElementMetadata[];
  removed: string[];
}>) {
  const PROJECT_ID = yield select((store: Store) => store.project.selected);
  const revision = yield select((store: Store) => store.cms.detail.revision);
  const formId = yield select((store: Store) => store.cms.forms.selected);
  const label = yield select((store: Store) => store.cms.pages.label);

  yield put(setLoading(ELEMENT.SAVE_ELEMENTS, true));
  const { elements, removed } = payload;
  try {
    // Payload contains new elements and updated, need to properly separate them
    const newElements: ElementMetadata[] = [];
    // Update structure is different
    const updatedElements: any[] = [];

    elements.forEach((el, index) => {
      // Element with ID is the one needs to be updated
      if (el.id) {
        updatedElements.push({
          id: el.id,
          ...transformFormData(el, index)
        });
      } else {
        newElements.push(transformFormData(el, index, formId));
      }
    });
    yield call(deleteElements, removed);

    if (newElements.length > 0) {
      yield call(
        callApi,
        'put',
        `/projects/${PROJECT_ID}/cms/revisions/${revision}?command=add&type=element`,
        newElements as any
      );
    }
    if (updatedElements.length > 0) {
      yield call(
        callApi,
        'put',
        `/projects/${PROJECT_ID}/cms/revisions/${revision}?command=edit&type=element`,
        updatedElements as any
      );
    }

    yield put(CommonAction.success(ELEMENT.SAVE_ELEMENTS, payload));
    yield put(CommonAction.request(ELEMENT.FETCH_ELEMENTS, payload));
    const request = {
      formId,
      label,
      revision
    };
    yield put(fetchCmsElementsRequest(request));
    yield put(
      addNotification({
        title: 'Success',
        message: 'Elements saved',
        status: 'success',
        dismissible: true,
        dismissAfter: 5000
      })
    );
  } catch (e) {
    yield put(CommonAction.failed(ELEMENT.SAVE_ELEMENTS, e));
    yield put(
      addNotification({
        title: 'Error',
        message: `Error saving form elements`,
        status: 'error',
        dismissible: true,
        dismissAfter: 5000
      })
    );
  } finally {
    yield put(setLoading(ELEMENT.SAVE_ELEMENTS, false));
  }
}

function* deleteElements(ids: string[]) {
  const PROJECT_ID = yield select((store: Store) => store.project.selected);
  const revision = yield select((store: Store) => store.cms.detail.revision);
  const formId = yield select((store: Store) => store.cms.forms.selected);
  const failed: string[] = [];
  const errorMessage = {
    title: 'Error',
    message: 'Unable to delete the element',
    status: 'error',
    dismissible: true,
    dismissAfter: 5000
  };
  // batch delete to rest api
  try {
    const responses = yield all(
      ids.map(id =>
        call(
          callApi,
          'put',
          `/projects/${PROJECT_ID}/cms/revisions/${revision}?command=delete&type=element`,
          {
            id,
            formId
          }
        )
      )
    );
    responses.forEach((resp, index) => {
      if (!resp) {
        failed.push(ids[index]);
      }
    });
  } catch (error) {
    yield put(addNotification(errorMessage));
  }
  if (failed.length) {
    yield put(addNotification(errorMessage));
  }
}

function* deleteElement({ payload }: any) {
  const PROJECT_ID = yield select((store: Store) => store.project.selected);
  const revision = yield select((store: Store) => store.cms.detail.revision);
  const elements = yield select((store: Store) => store.cms.elements);
  yield put(setLoading(ELEMENT.DELETE_ELEMENT, true));
  try {
    const response = yield call(
      callApi,
      'put',
      `/projects/${PROJECT_ID}/cms/revisions/${revision}?command=delete&type=element`,
      {
        id: payload.id,
        formId: payload.formId
      }
    );
    if (response) {
      yield put(setElement(elements.filter(el => el.id !== payload.id)));
      yield put(CommonAction.success(ELEMENT.DELETE_ELEMENT));
    } else {
      yield put(
        addNotification({
          title: 'Error',
          message: 'Unable to delete the element',
          status: 'error',
          dismissible: true,
          dismissAfter: 5000
        })
      );
    }
  } catch (e) {
    yield put(CommonAction.failed(ELEMENT.DELETE_ELEMENT, e));
    yield put(
      addNotification({
        title: 'Error',
        message: `Error deleting form's elements`,
        status: 'error',
        dismissible: true,
        dismissAfter: 5000
      })
    );
  } finally {
    yield put(setLoading(ELEMENT.DELETE_ELEMENT, false));
  }
}

function* watchSaveElements() {
  yield takeLatest(
    CommonAction.build(COMMON._REQUEST, ELEMENT.SAVE_ELEMENTS),
    saveElements
  );
}

function* watchDeleteElement() {
  yield takeLatest(
    CommonAction.build(COMMON._REQUEST, ELEMENT.DELETE_ELEMENT),
    deleteElement
  );
}

function* watchFetchCmsElements() {
  yield takeLatest(ELEMENT.FETCH_REQUEST, fetchCmsElements);
}

export default function* root() {
  yield all([
    fork(watchFetchCmsElements),
    fork(watchSaveElements),
    fork(watchDeleteElement)
  ]);
}
