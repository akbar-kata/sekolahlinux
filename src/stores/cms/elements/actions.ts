import ELEMENT, { FetchCmsElementsSuccess } from './types';
import { ElementMetadata as Element } from 'interfaces/cms';

export function selectElement(data: Element) {
  return {
    type: ELEMENT.SELECT,
    payload: data
  };
}

// function requestElement<T>(action: string, payload?: T) {
//   return {
//     type: ELEMENTS[`${action.toUpperCase()}_REQUEST`],
//     action,
//     payload
//   };
// }

export function fetchCmsElementsRequest(data: any) {
  return {
    type: ELEMENT.FETCH_REQUEST,
    payload: {
      formId: data.formId,
      label: data.label,
      revision: data.revision,
      formLabel: data.formLabel
    }
  };
}

export function fetchCmsElementsSuccess(
  data: FetchCmsElementsSuccess['payload']
) {
  return {
    type: ELEMENT.FETCH_SUCCESS,
    payload: data
  };
}

export function setElement(elements: Element[]) {
  return {
    type: ELEMENT.SET_ELEMENTS,
    payload: elements
  };
}

export function setElementData(index: number, field: string, data: any) {
  return {
    type: ELEMENT.SET_ELEMENT_DATA,
    payload: {
      index,
      field,
      data
    }
  };
}
