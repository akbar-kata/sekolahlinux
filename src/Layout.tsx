import React from 'react';

import {
  Container,
  Content,
  Sidebar,
  SidebarMain,
  SidebarSub,
  SidebarMainMenu,
  SidebarSubMenu
} from './components/Layout';
import { Button, FloatingButton } from 'components/Button';
import { SplitButton } from 'components/SplitButton';
import {
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  confirm
} from 'components/Modal';
import { Circle, Robot } from 'components/Loading';
import { Drawer } from 'components/Drawer';
import DrawerHeader from 'components/Drawer/DrawerHeader';
import DrawerBody from 'components/Drawer/DrawerBody';
import DrawerFooter from 'components/Drawer/DrawerFooter';
import { Form, Text, ReactSelect } from 'components/Form';
import { Banner } from 'components/Banner';
import { Tooltip, TooltipTarget } from 'components/Tooltip';
import { DropdownItem } from 'components/Dropdown';
import { DateRangePicker } from 'components/DateRangePicker';
import { Dashboard } from 'components/Dashboard';
import { LightBox } from 'components/LightBox';

// tslint:disable
const items = [
  {
    src:
      'data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22800%22%20height%3D%22400%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20800%20400%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_15ba800aa1d%20text%20%7B%20fill%3A%23555%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A40pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_15ba800aa1d%22%3E%3Crect%20width%3D%22800%22%20height%3D%22400%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%22285.921875%22%20y%3D%22218.3%22%3EFirst%20slide%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E',
    altText: 'Slide1',
    caption: 'Slide 1'
  },
  {
    src:
      'data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22800%22%20height%3D%22400%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20800%20400%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_15ba800aa20%20text%20%7B%20fill%3A%23444%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A40pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_15ba800aa20%22%3E%3Crect%20width%3D%22800%22%20height%3D%22400%22%20fill%3D%22%23666%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%22247.3203125%22%20y%3D%22218.3%22%3ESecond%20slide%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E',
    altText: 'Slide2',
    caption: 'Slide 2'
  },
  {
    src:
      'data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22800%22%20height%3D%22400%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20800%20400%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_15ba800aa21%20text%20%7B%20fill%3A%23333%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A40pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_15ba800aa21%22%3E%3Crect%20width%3D%22800%22%20height%3D%22400%22%20fill%3D%22%23555%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%22277%22%20y%3D%22218.3%22%3EThird%20slide%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E',
    altText: 'Slide3',
    caption: 'Slide 3'
  }
];
// tslint:enable

const entityType = [
  {
    label: 'Trait',
    value: 'trait'
  },
  {
    label: 'Dictionary',
    value: 'dict'
  },
  {
    label: 'Phrase',
    value: 'phrase'
  },
  {
    label: 'Trait 1',
    value: 'trait 1'
  },
  {
    label: 'Dictionary 1',
    value: 'dict 1'
  },
  {
    label: 'Phrase 1',
    value: 'phrase 1'
  },
  {
    label: 'Trait 2',
    value: 'trait 2'
  },
  {
    label: 'Dictionary 2',
    value: 'dict 2'
  },
  {
    label: 'Phrase 2',
    value: 'phrase 2'
  }
];
export default class Layout extends React.Component<any, any> {
  state = {
    open: false,
    carouselOpen: false,
    isDrawerOpen: false,
    loading: false,
    activeItemIndex: 0
  };

  openModal = () => {
    this.setState({ open: true });
  };

  openCarousel = () => {
    this.setState({ carouselOpen: true });
  };

  closeCarousel = () => {
    this.setState({ carouselOpen: false });
  };

  closeModal = () => {
    this.setState({ loading: true });
    setTimeout(() => {
      this.setState({ open: false, loading: false });
    }, 750);
  };

  openDrawer = () => {
    this.setState({ isDrawerOpen: true });
  };

  closeDrawer = () => {
    this.setState({ isDrawerOpen: false });
  };

  nextCarouselItem = () => {
    const nextIndex =
      this.state.activeItemIndex === items.length - 1
        ? 0
        : this.state.activeItemIndex + 1;
    this.setState({ activeItemIndex: nextIndex });
  };

  prevCarouselItem = () => {
    const nextIndex =
      this.state.activeItemIndex === 0
        ? items.length - 1
        : this.state.activeItemIndex - 1;
    this.setState({ activeItemIndex: nextIndex });
  };

  render() {
    const defaultVal = Object.assign({ type: 'trait', profile: 'default' });
    const slides = items.map(item => {
      return <img src={item.src} alt={item.altText} key={item.altText} />;
    });

    return (
      <Container>
        <LightBox
          isOpen={this.state.carouselOpen}
          onClose={this.closeCarousel}
          currentIndex={this.state.activeItemIndex}
          slides={slides}
        />
        <Sidebar>
          <SidebarMain>
            <SidebarMainMenu to="/botstudio" icon="bot">
              Bot
            </SidebarMainMenu>
            <SidebarMainMenu to="/nlstudio" icon="nlu">
              NLU
            </SidebarMainMenu>
            <SidebarMainMenu to="/cms" icon="docs">
              CMS
            </SidebarMainMenu>
            <SidebarMainMenu to="/analytics" icon="analytics">
              Analytics
            </SidebarMainMenu>
            <SidebarMainMenu to="/settings" icon="settings">
              Settings
            </SidebarMainMenu>
          </SidebarMain>
          <SidebarSub
            titleElement={
              <select className="form-control kata-bot-selector mb-3">
                <option value="test">NLU 1</option>
                <option value="test">NLU 1</option>
                <option value="test">NLU 1</option>
              </select>
            }
          >
            <SidebarSubMenu to="nlstudio/entity" icon="entity">
              Entity
            </SidebarSubMenu>
            <SidebarSubMenu to="nlstudio/entity" icon="training">
              Training
            </SidebarSubMenu>
            <SidebarSubMenu to="nlstudio/entity" icon="log">
              Logs
            </SidebarSubMenu>
          </SidebarSub>
        </Sidebar>
        <Content>
          <Modal show={this.state.open} onClose={this.closeModal}>
            <ModalHeader>Modal Title</ModalHeader>
            <ModalBody>Test body</ModalBody>
            <ModalFooter>
              <Button
                color="primary"
                onClick={this.closeModal}
                loading={this.state.loading}
              >
                Close
              </Button>
            </ModalFooter>
          </Modal>
          <Circle size={30} />
          <Robot />
          <Drawer isOpen={this.state.isDrawerOpen} onClose={this.closeDrawer}>
            <DrawerHeader title="Title" />
            <DrawerBody>
              <Form defaultValues={defaultVal} onSubmit={this.props.onSubmit}>
                <form>
                  <ReactSelect
                    field="type"
                    clearable
                    simpleValue
                    options={entityType}
                    placeholder="Select Type"
                    onChange={() => {
                      console.dir('select action');
                    }}
                    className="kata-form__input-select"
                    clearRenderer={() => {
                      return (
                        <i className="icon-remove kata-form__input-select--icon" />
                      );
                    }}
                    arrowRenderer={() => {
                      return null;
                    }}
                  />
                </form>
              </Form>
            </DrawerBody>
            <DrawerFooter>
              <button
                type="button"
                className="br2 mt3 mb0 pv3 ph4 btn btn-primary"
                onClick={this.closeDrawer}
              >
                <i className="icon-plus mr2" /> Close
              </button>
            </DrawerFooter>
          </Drawer>
          <Dashboard title={'Components'}>
            <div className="kata-info__container kata-intents__attributes-row1--wrapper pr-1">
              <div className="h3">Typography</div>
              <div className="kata-info__content">
                <div className="heading1">
                  Understanding Conversation Humanizing Interaction
                </div>
                <div className="heading2">
                  Understanding Conversation Humanizing Interaction
                </div>
                <div className="title">
                  Understanding Conversation Humanizing Interaction
                </div>
                <div className="subtitle">
                  Understanding Conversation Humanizing Interaction
                </div>
                <div className="label">
                  Understanding Conversation Humanizing Interaction
                </div>
                <div className="text-small">
                  Understanding Conversation Humanizing Interaction
                </div>
              </div>
            </div>

            <div className="kata-info__container kata-intents__attributes-row1--wrapper pr-1">
              <div className="h3">Modal & Drawer</div>
              <div className="kata-info__content">
                <button onClick={this.openModal}>Open Modal</button>
                <button onClick={this.openCarousel}>Open Carousel</button>
                <button onClick={this.openDrawer}>Open Drawer</button>
              </div>
            </div>

            <div className="kata-info__container kata-intents__attributes-row1--wrapper pr-1">
              <div className="h3">Buttons</div>
              <div className="kata-info__content">
                <Button color="primary" loading>
                  Loading
                </Button>
                <FloatingButton icon="arrow-down" />
              </div>
              <div className="kata-info__content">
                <button
                  onClick={() =>
                    confirm({
                      title: 'Remove Train',
                      message: 'Are you sure want to remove this train data?',
                      okLabel: 'Remove'
                    }).then(result =>
                      result ? alert('success') : alert('failed')
                    )
                  }
                >
                  Confirm
                </button>
              </div>
              <div className="kata-info__content">
                <TooltipTarget
                  trigger="hover"
                  placement="bottom"
                  component={
                    <Tooltip>
                      Lorem ipsum dolor sit amet, ex nemore oblique rationibus
                      has, erat aliquam id his, alii malorum te duo. Mea te
                      omnes nullam suscipiantur, eu veniam dolore nam. Per
                      adipisci tacimates ei, quodsi consequuntur at pro. Quidam
                      alterum expetendis et mei, per et natum reprimique
                      complectitur, vel etiam ubique ex.
                    </Tooltip>
                  }
                >
                  <i className="icon-bot" />
                </TooltipTarget>
              </div>
              <div className="kata-info__content">
                <FloatingButton icon="trash" />
              </div>
              <div className="kata-info__content">
                <SplitButton title="Publish" color="primary">
                  <DropdownItem>Hehe</DropdownItem>
                  <DropdownItem>Hoho</DropdownItem>
                </SplitButton>
              </div>
            </div>

            <div className="kata-info__container">
              <div className="h3">Datepicker</div>
              <div className="kata-info__content">
                <div className="kata-info__content">
                  <DateRangePicker
                    changeDate={(startDate, endDate) => {
                      console.log('changeDate', startDate, endDate); // tslint:disable-line
                    }}
                  />
                </div>
              </div>
            </div>
            <div className="kata-info__container">
              <div className="h3">Forms</div>
              <div className="kata-info__content">
                <Form>
                  <form className="kata-form full-size">
                    <div className="row">
                      <div className="col">
                        <div className="kata-form__element">
                          <label className="kata-form__label">Text Input</label>
                          <Text
                            field="name"
                            className="kata-form__input-text"
                            placeholder="text input..."
                          />
                        </div>
                      </div>
                      <div className="col">
                        <div className="kata-form__element">
                          <label className="kata-form__label">
                            Singe Select
                          </label>
                          <ReactSelect
                            field="type"
                            clearable
                            simpleValue
                            options={entityType}
                            placeholder="Select Type"
                            onChange={() => {
                              console.dir('select action');
                            }}
                            className="kata-form__input-select"
                            clearRenderer={() => {
                              return (
                                <i className="icon-remove kata-form__input-select--icon" />
                              );
                            }}
                            arrowRenderer={() => {
                              return null;
                            }}
                          />
                        </div>
                      </div>
                    </div>
                  </form>
                </Form>
              </div>
              <div className="kata-info__container">
                <div className="h3">Banner</div>
                <div className="kata-info__content">
                  <div className="kata-info__content">
                    <Banner state="success" message="Hallo" />
                  </div>
                  <div className="kata-info__content">
                    <Banner state="info" message="Hallo" />
                  </div>
                  <div className="kata-info__content">
                    <Banner state="error" message="Hallo" />
                  </div>
                  <div className="kata-info__content">
                    <Banner state="warning" message="Hallo" />
                  </div>
                </div>
              </div>
            </div>
          </Dashboard>
        </Content>
      </Container>
    );
  }
}
