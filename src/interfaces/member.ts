export interface Data {
  userId: string;
  roleId: string;
  username: string;
  roleName: string;
}

export interface DataMap {
  [key: string]: Data;
}

export interface Loadings {
  fetch: boolean;
  add: boolean;
  update: boolean;
  remove: boolean;
}

export interface Errors {
  fetch: string | null;
  add: string | null;
  update: string | null;
  remove: string | null;
}

export interface Store {
  selected: string | null;
  index: string[];
  data: DataMap;
  lastUpdate: number;
  loadings: Loadings;
  errors: Errors;
}
