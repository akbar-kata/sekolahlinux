export interface NluTestingData {
  id?: string;
  input: string;
  output?: any;
}

export interface NluTestingDataMap {
  [key: string]: NluTestingData;
}

export interface NluTestingLoadingState {
  fetch: boolean;
  add: boolean;
  update: boolean;
  remove: boolean;
}

export interface NluTestingErrors {
  fetch: string | null;
  add: string | null;
  update: string | null;
  remove: string | null;
}

export interface NluTestingStore {
  index: string[];
  data: NluTestingData;
  loadings: NluTestingLoadingState;
  errors: NluTestingErrors;
}
