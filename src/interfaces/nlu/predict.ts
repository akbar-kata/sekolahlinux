export interface NluPredictData {
  id?: string;
  input: string;
  output?: any;
}

export interface NluPredictDataMap {
  [key: string]: NluPredictData;
}

export interface NluPredictLoadingState {
  fetch: boolean;
  add: boolean;
  update: boolean;
  remove: boolean;
}

export interface NluPredictErrors {
  fetch: string | null;
  add: string | null;
  update: string | null;
  remove: string | null;
}

export interface NluPredictStore {
  index: string[];
  data: NluPredictData;
  lastUpdate: number;
  loadings: NluPredictLoadingState;
  errors: NluPredictErrors;
}

export interface NluPredictParams {
  sentence: string;
  filter: string;
  log: boolean;
}
