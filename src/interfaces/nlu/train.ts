export type NluTrainData = {
  id: string;
  input: string;
  data: any;
  output?: any;
  trained?: boolean;
};

export type NluTrainDataMap = {
  [key: string]: NluTrainData;
};

export type NluTrainLoadingState = {
  fetch: boolean;
};

export type NluTrainErrors = {
  fetch: string | null;
};

export type NluTrainPage = {
  page: number;
  total: number;
  limit: number;
};

export type NluTrainStore = {
  index: string[];
  data: NluTrainDataMap;
  lastUpdate: number;
  loadings: NluTrainLoadingState;
  errors: NluTrainErrors;
  pagination: NluTrainPage;
};
