import { DataMap } from 'interfaces/common';

export type Entity = {
  id?: string;
  name: string;
  type: string;
  profile: string;
  relProfile: string;
  belongsTo: string;
  model: any;
  relModel: any;
  resolver: string;
  labels?: string[];
  root: string | null;
  inherit: string | null;
  dictionary: Record<string, string[]>;
};

export type EntityDataMap = {
  [key: string]: Entity;
};

export interface EditEntityRequestData {
  project: string;
  nlu: string;
  name: string;
  data: Entity;
}

export type EntityLoadingState = {
  fetch: boolean;
  add: boolean;
  update: boolean;
  remove: boolean;
  profiles: boolean;
  root: boolean;
  inherit: boolean;
  detail: boolean;
};

export type EntityErrorState = {
  fetch: string | null;
  add: string | null;
  update: string | null;
  remove: string | null;
  profile: string | null;
  root: string | null;
  inherit: string | null;
  detail: string | null;
};

export type EntityStore = {
  selected: string | null;
  loadings: EntityLoadingState;
  errors: EntityErrorState;
  index: string[];
  data: EntityDataMap;
  profiles: any[];
  root: any[];
  inherit: any[];
  publicEntities: DataMap<any>;
  lastUpdate: number;
  detail: any;
};
