export type NluLogData = {
  id?: string;
  input: string;
  nluId: string;
  predicted: any[];
  trained: boolean;
  corrected?: string;
  created_at: number;
  updated_at: number;
};

export type NluLogDataMap = {
  [key: string]: NluLogData;
};

export type NluLogLoadingState = {
  fetch: boolean;
  correct: boolean;
};

export type NluLogErrors = {
  fetch: string | null;
  correct: string | null;
};

export type NluLogPagination = {
  limit: number;
  page: number;
  total: number;
};

export type NluLogStore = {
  index: string[];
  data: NluLogDataMap;
  lastUpdate: number;
  loadings: NluLogLoadingState;
  errors: NluLogErrors;
  pagination: NluLogPagination;
};
