import { EntityStore, EntityDataMap } from './entity';
import { NluLogStore } from './log';
import { NluTrainStore } from './train';
import { NluPredictStore } from './predict';
import { NluTestingStore } from './testing';

export type Nlu = {
  id: string;
  name: string;
  lang: string;
  entities: EntityDataMap;
  snapshot: string;
  tag: string;
  token: string;
  visibility: string;
};

export type NluLoadingState = {
  fetch: boolean;
  reissue: boolean;
  snapshot: boolean;
};

export type NluErrorState = {
  fetch: string | null;
  reissue: string | null;
  snapshot: string | null;
};

export interface NluStore {
  loadings: NluLoadingState;
  errors: NluErrorState;
  data: Nlu | null;
}

export type NlStudioRootStore = {
  nlu: NluStore;
  entity: EntityStore;
  log: NluLogStore;
  train: NluTrainStore;
  predict: NluPredictStore;
  testing: NluTestingStore;
};
