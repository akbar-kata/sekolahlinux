import { RouterState } from 'connected-react-router';

import AppStore from './app';
import * as Analytic from './analytic';
import * as Auth from './auth';
import * as Check from './check';
import * as Bot from './bot';
import * as Cms from './cms';
import * as Deployment from './deployment';
import * as Pricing from './pricing';
import * as Member from './member';
import * as Team from './team';
import * as Subscription from './subscription';
import * as Account from './account';
import * as Nlu from './nlu';
import * as Project from './project';
import * as Module from 'interfaces/module';
import * as Revision from 'interfaces/revision';

interface Store {
  account: Account.Store;
  analytic: Analytic.Store;
  app: AppStore;
  auth: Auth.Store;
  bot: Bot.BotStore;
  check: Check.Store;
  cms: Cms.CmsStore;
  deployment: Deployment.Store;
  member: Member.Store;
  module: Module.ModuleStore;
  nlu: Nlu.NlStudioRootStore;
  pricing: Pricing.Store;
  project: Project.Store;
  revision: Revision.RevisionStore;
  subscription: Subscription.Store;
  team: Team.Store;

  notifications: any;
  router: RouterState;
}

export default Store;
