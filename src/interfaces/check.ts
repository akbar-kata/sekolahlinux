export interface Data {
  id: string;
  username: string;
  email: string;
}

export interface DataMap {
  [key: string]: Data;
}

export interface Loadings {
  fetch: boolean;
}

export interface Errors {
  fetch: string | null;
}

export interface Store {
  data: Data[];
  loadings: Loadings;
  errors: Errors;
}
