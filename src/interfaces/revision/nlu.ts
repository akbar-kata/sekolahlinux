import { Omit } from 'utility-types';
import { PaginatedData } from 'interfaces/common';

export interface NluRevision {
  id?: string;
  name: string;
  lang: string;
  revision: string;
  visibility: string;
  created_at: string;
  updated_at: string;
}

export interface NluRevisionStore {
  data: Record<string, NluRevision>;
  index: string[];
  pagination: Omit<PaginatedData<NluRevision>, 'data'>;
}
