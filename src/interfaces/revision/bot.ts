import { Omit } from 'utility-types';
import { PaginatedData } from 'interfaces/common';

export interface BotRevision {
  flows: {
    intents: any;
    actions: any;
    states: any;
  };
  timezone: number;
  methods: any;
  name: string;
  id: string;
  lang: string;
  config: any;
  revision: string;
  desc: string;
  nlus: any;
  changelog: string;
  created_at: string;
  author: string;
}

export interface BotRevisionStore {
  data: Record<string, BotRevision>;
  index: string[];
  pagination: Omit<PaginatedData<BotRevision>, 'data'>;
}
