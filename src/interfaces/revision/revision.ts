import { BotRevisionStore } from './bot';
import { CmsRevisionStore } from './cms';
import { NluRevisionStore } from './nlu';

export interface RevisionStore {
  bot: BotRevisionStore;
  cms: CmsRevisionStore;
  nlu: NluRevisionStore;
}
