import { Omit } from 'utility-types';
import { PaginatedData } from 'interfaces/common';

export interface CmsRevision {
  id?: string;
  name: string;
  label: string;
  created_at: string;
  updated_at: string;
  revision: string;
  modules?: any;
}

export interface CmsRevisionStore {
  data: Record<string, CmsRevision>;
  index: string[];
  pagination: Omit<PaginatedData<CmsRevision>, 'data'>;
}
