import { JsonObject } from 'interfaces';

export interface Data {
  id: string;
  name: string;
  depId: string;
  depVersion: string;
  slug: string;
  channels: EnvironmentChannel[];
}

export interface EnvironmentChannel {
  id: string;
  name: string;
  type: string;
  options: JsonObject;
  url: string;
  webhook: string;
}

export interface Loadings {
  fetch: boolean;
  add: boolean;
  update: boolean;
  remove: boolean;
}

export interface Errors {
  fetch: string | null;
  add: string | null;
  update: string | null;
  remove: string | null;
}
export interface DataMap {
  [key: string]: Data;
}

export type ChannelType =
  | 'generic'
  | 'line'
  | 'fbmessenger'
  | 'slack'
  | 'qiscus'
  | 'twitter'
  | 'telegram';

export interface Store {
  selected: string | null;
  index: string[];
  data: DataMap;
  loadings: Loadings;
  errors: Errors;
}
