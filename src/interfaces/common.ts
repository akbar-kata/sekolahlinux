export interface DataMap<T> {
  [key: string]: T;
}

export interface PaginatedDataMap<T> {
  data: DataMap<T>;
  limit: number | null;
  page: number | null;
  total: number;
  count?: number;
}

export interface PaginatedData<T> {
  data: T[];
  limit: number | null;
  page: number | null;
  total: number;
  count?: number;
}

export interface Pagination {
  limit: number | null;
  page: number | null;
  total: number;
  count?: number;
}

export interface Swap {
  first: number;
  second: number;
}
