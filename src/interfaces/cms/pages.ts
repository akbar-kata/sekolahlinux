import { Form } from './forms';

export interface Page {
  id: string;
  name: string;
  label: string;
  description: string;
  created_at: string;
  updated_at: string;
  revision?: string;
  index: number;
  cmsId?: any;
  forms?: Record<string, Form>;
}

export interface CmsPagesStore {
  data: Record<string, Page>;
  index: string[];
  selected: string;
  label: string;
  pageName: string;
}
