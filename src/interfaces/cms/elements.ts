export type ElementDefinition = {
  type: string;
  title: string;
  element?: string;
  meta: ElementMetadata;
};

export type ElementMetadata = {
  id?: string;
  tempId?: string;
  formId?: string;
  type: string;
  name: string;
  meta: Record<string, any>;
  revision?: string;
};

export interface CmsElementsStore {
  elements: ElementMetadata[];
}
