import { CmsElementsStore } from './elements';
import { CmsPagesStore } from './pages';
import { CmsFormsStore } from './forms';
import { CmsUsersStore } from './users';

export interface Cms {
  id?: string;
  revision: string;
  modules?: any;
  created_at: string;
  updated_at: string;
  settings?: any;
}

export interface CmsStore {
  settings: CmsData;
  detail: Cms;
  pages: CmsPagesStore;
  forms: CmsFormsStore;
  elements: CmsElementsStore;
  users: CmsUsersStore;
}

export type CmsData = {
  botId: string;
  settings: any;
};
