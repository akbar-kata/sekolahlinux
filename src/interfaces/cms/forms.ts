export interface Form {
  id: string;
  revision: string;
  label: string;
  pageId: string;
  type: string;
  name: string;
  index: number;
  created_at: string;
  updated_at: string;
  elements?: any;
}

export interface CmsFormsStore {
  data: Record<string, Form>;
  index: string[];
  selected: string;
  label: string;
}
