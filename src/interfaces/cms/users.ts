export type UserData = {
  id?: string;
  email: string;
  botId?: string;
  created_at?: Date;
  updated_at?: Date;
  userId?: string;
  username?: string;
  namespace?: string;
  environment?: string;
};

export interface CmsUsersStore {
  data: Record<string, UserData>;
  index: string[];
  selected: string | null;
}
