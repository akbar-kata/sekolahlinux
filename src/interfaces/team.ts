export interface Data {
  roleId: string;
  teamId: string;
  username: string;
  bots: number;
  members: number;
}

export interface DataMap {
  [key: string]: Data;
}

export interface Loadings {
  fetch: boolean;
  add: boolean;
  update: boolean;
  remove: boolean;
}

export interface Errors {
  fetch: string | null;
  add: string | null;
  update: string | null;
  remove: string | null;
}

export interface Store {
  selected: string | null;
  index: string[];
  data: DataMap;
  lastUpdate: number;
  loadings: Loadings;
  errors: Errors;
}
