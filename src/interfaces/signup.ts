export interface Token {
  id: string;
  type: string;
  label: string;
  userId: string;
  teamId: string;
  botId?: string;
  roleId: string;
  expire?: number;
}

export interface Store {
  isLoading: boolean;
  token: null | Token;
  error: false | string;
}
