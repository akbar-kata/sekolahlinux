import { Action } from 'redux';

export type Json = string | number | boolean | JsonObject | JsonArray;

export interface JsonObject {
  [x: string]: Json;
}

export interface JsonArray extends Array<Json> {}

export interface ObjectOfJson {
  [key: string]: JsonObject | ObjectOfJson;
}

export interface ReduxAction<T = any> extends Action<T> {
  action?: string;
  payload: any;
}

export interface TypedReduxAction<T> {
  type: string;
  payload: T;
}

export interface ExReduxAction<T, P> {
  type: T;
  payload: P;
}
export interface VoidExReduxAction<T> {
  type: T;
}
