export interface Metadata {
  limit: number;
  total: number;
  page: number;
}
