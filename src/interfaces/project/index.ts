import { DataMap, Pagination } from 'interfaces/common';

export interface ProjectOptions {
  timezone: number;
  nluVisibility: string;
  nluLang: string;
  nluId?: string;
}
export interface CreateProjectOptions {
  bot: boolean;
  cms: boolean;
  nlu: boolean;
}

export interface Project {
  id?: string;
  label: string;
  name: string;
  description?: string;
  timezone: string;
  botId: string;
  nluId: string;
  cmsId: string;
  botLatestRevision: string;
  nluLatestRevision: string;
  cmsLatestRevision: string;
  modules: any[];
  environments: any[];
  options: Partial<ProjectOptions>;
}

export interface Template {
  id: string;
  name: string;
}

export interface Store {
  selected: string | null;
  projects: DataMap<Project>;
  projectIndexes: string[];
  pagination: Pagination;
  templates: DataMap<Template>;
  templateIndexes: string[];
}
