import * as Environment from './deployment/environment';
import * as Channel from './deployment/channel';

export interface Data {
  id: string;
  name: string;
  version: string;
  botRevision: string;
  nluRevision: string;
  cmsRevision: string;
  tag: string | null;
  author: string | null;
  changelog: string | null;
  created_at: string;
}

export interface DataMap {
  [key: string]: Data;
}

export interface Loadings {
  fetch: boolean;
  add: boolean;
  update: boolean;
  remove: boolean;
}

export interface Errors {
  fetch: string | null;
  add: string | null;
  update: string | null;
  remove: string | null;
}

export interface Store {
  selected: string | null;
  index: string[];
  data: DataMap;
  latestDeployment: Data;
  lastUpdate: number;
  loadings: Loadings;
  errors: Errors;
  environment: Environment.Store;
  channel: Channel.Store;
}
