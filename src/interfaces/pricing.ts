import * as Tiering from './pricingTiering';
import { Metadata } from 'interfaces/metadata';

export interface Data {
  id: string;
  name: string;
  description: string;
  currency: string;
  tierings: Tiering.Data[];
}

export interface DataMap {
  [key: string]: Data;
}

export interface Loadings {
  fetch: boolean;
  add: boolean;
  update: boolean;
  remove: boolean;
}

export interface Errors {
  fetch: string | null;
  add: string | null;
  update: string | null;
  remove: string | null;
}

export interface Store {
  selected: string | null;
  index: string[];
  data: DataMap;
  metadata: Metadata;
  lastUpdate: number;
  loadings: Loadings;
  errors: Errors;
}

export interface FetchRequest {
  page?: number;
}
