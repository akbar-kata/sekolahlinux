export interface Data {
  id: string;
  name: string;
  type: string;
  startCount: number;
  endCount: number;
  commitment: number;
  unitPrice: number;
}

export interface DataMap {
  [key: string]: Data;
}
