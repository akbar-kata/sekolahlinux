export interface Token {
  id: string;
  type: string;
  label: string;
  userId: string;
  teamId: string;
  botId?: string;
  roleId: string;
  expire?: number;
  detail: any;
  teams: any[];
  isOtp: any;
}

export interface Selected {
  id: string;
  token: string;
  username: string;
}

export interface Store {
  isLoading: boolean;
  token: null | Token;
  selected: Selected | null;
  error: false | string;
}
