import { DataMap } from '../common';

export interface StateAction {
  name: string;
  condition?: string | string[];
  options?: DataMap<any>;
}

export type StateActionMap = DataMap<StateAction>;

export type Mapping = DataMap<string>;

export interface StateTransition {
  condition?: string | string[];
  priority?: number;
  fallback?: boolean;
  mapping?: string | Mapping;
}

export interface StateNode {
  flow: string;
  name: string;
  initial?: boolean;
  end?: boolean;
  action: string | StateAction | StateAction[];
  float?: StateTransition;
  onEnter?: Mapping | string;
  onTransit?: Mapping | string;
  onExit?: Mapping | string;
  style: {
    left: number | string;
    top: number | string;
  };
  _opts: {
    enterMappingMode: 'simple' | 'method';
    transitMappingMode: 'simple' | 'method';
    exitMappingMode: 'simple' | 'method';
    transitionTab: 'fallback' | 'float';
    enablefloat?: boolean;
    floatMapping: 'simple' | 'method';
    enablefallback?: boolean;
    fallbackMapping: 'simple' | 'method';
  };
}

export type StateNodeMap = DataMap<StateNode>;

export interface StateConnection {
  flow: string;
  from: string;
  to: string;
  transition: StateTransition;
  _opts: {
    mappingMode: 'simple' | 'method';
  };
}

export type StateConnectionMap = DataMap<StateConnection>;

export interface StateNodeStore {
  index: string[];
  data: StateNodeMap;
  lastUpdated: number;
}

export interface StateConnectionStore {
  index: string[];
  data: StateConnectionMap;
}
