import { DataMap } from '../common';

export type Flow = {
  name: string;
  active: boolean;
  fallback: boolean;
  volatile: boolean;
  expire: number | false;
};

export type FlowMap = DataMap<Flow>;

export type FlowStore = {
  selected: string | null;
  index: string[];
  data: FlowMap;
};
