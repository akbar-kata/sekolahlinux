import { DataMap } from '../common';

export interface Action {
  flow: string;
  name: string;
  type: string;
  options: DataMap<string>;
  method?: string | string[];
  condition?: string | string[];
}

export type ActionMap = DataMap<Action>;

export interface ActionStore {
  index: string[];
  data: ActionMap;
}
