import { DataMap } from '../common';

export interface BotNLU {
  name: string;
  type: string;
  options: DataMap<any>;
  method?: string;
  process?: string[];
}

export type BotNLUMap = DataMap<BotNLU>;

export interface BotNLUStore {
  index: string[];
  data: BotNLUMap;
}
