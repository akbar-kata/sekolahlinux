import { DataMap } from '../common';

export type IntentDictionary = DataMap<string | string[]>;

export interface IntentClassifier {
  nlu: string;
  match: string;
  expression?: string;
  hint?: string;
  options?: DataMap<any>;
  process?: string | string[];
  dict?: IntentDictionary;
}

export interface IntentAttribute {
  nlu: string;
  path: string;
  hint?: string;
  options?: DataMap<any>;
  process?: string | string[];
  dict?: IntentDictionary;
}

export type IntentAtrributeMap = DataMap<IntentAttribute>;

export type Intent = {
  flow: string;
  name: string;
  type: string;
  condition?: string | string[] | false;
  classifier?: IntentClassifier | IntentClassifier[];
  attributes?: IntentAtrributeMap;
  priority?: number;
  fallback?: boolean;
  initial?: boolean;
};

export type IntentMap = DataMap<Intent>;

export type IntentStore = {
  index: string[];
  data: IntentMap;
};
