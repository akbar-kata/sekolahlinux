import { DataMap } from '../common';

export interface Method {
  flow: string;
  entry?: string;
  name: string;
  params: string[];
  definition: string;
  mode?: string;
}

export type MethodMap = DataMap<Method>;

export interface MethodStore {
  index: string[];
  data: MethodMap;
}
