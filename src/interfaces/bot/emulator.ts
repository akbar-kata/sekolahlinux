import { JsonObject } from 'interfaces';
export enum MessageOwner {
  user = 'user',
  bot = 'bot'
}
export enum MessageType {
  data = 'data',
  text = 'text',
  unsupported = 'null'
}
export interface RichMessageAction {
  text: string;
  type: string;
}
export interface RichMessageItem {
  description: string;
  url: string;
  actions: RichMessageAction[];
}
export interface RichMessage {
  type: string;
  subType: string;
  items: RichMessageItem;
}
export interface Message {
  from: MessageOwner;
  message: string & RichMessage;
  metadata: any;
  type: MessageType;
  date: Date;
}
export interface Bags {
  errors: JsonObject;
}
export interface Store {
  messages: Message[];
  activeSession: any;
  isLoading: boolean;
  bags: Bags;
}
