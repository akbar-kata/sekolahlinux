import { DataMap, Pagination } from '../common';
import { FlowStore } from './flow';
import { IntentStore } from './intent';
import { StateNodeStore, StateConnectionStore } from './state';
import { ActionStore } from './action';
import { MethodStore } from './method';
import { BotNLUStore } from './nlu';
import { Logs } from './log';
import { Store as EmulatorStore } from './emulator';
import { Store as VersionStore } from './version';

export type Bot = {
  id: string;
  name: string;
  version: string;
  desc: string;
  lang: string;
  timezone: number;
  author: string;
  changelog: string;
  timezoneStr?: string;
};

export type BotMap = DataMap<Bot>;

export type BotStore = {
  selected: string | null;
  draft: string | null;
  index: string[];
  data: BotMap;
  pagination: Pagination;

  // botstudio store
  flow: FlowStore;
  intent: IntentStore;
  node: StateNodeStore;
  connection: StateConnectionStore;
  action: ActionStore;
  method: MethodStore;
  nlu: BotNLUStore;
  log: Logs;
  config: DataMap<any>;
  emulator: EmulatorStore;
  version: VersionStore;
};
