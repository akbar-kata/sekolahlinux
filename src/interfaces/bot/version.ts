export interface Loadings {
  fetch: boolean;
  kataml: boolean;
}

export interface Errors {
  fetch: string | null;
  kataml: string | null;
}

export interface KataML {
  [key: string]: {
    yaml: string;
    json: string;
  };
}

export interface BotVersion {
  version: string;
  tag?: string | null;
  author?: string | null;
  changelog?: string | null;
  created_at: string;
}

export interface Store {
  selected: BotVersion | null;
  latest: string;
  data: BotVersion[];
  kataml: KataML;
  lastUpdate: number;
  loadings: Loadings;
  errors: Errors;
}
