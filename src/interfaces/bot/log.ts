export interface LogError {
  errorId: string;
  timestamp: string;
  botId: string;
  deploymentId: string;
  channelId: string;
  userId: string;
  errorGroup: number;
  errorCode: number;
  errorMessage: string;
}

export interface LogErrors {
  groups: string[];
  codes: {
    [group: string]: string[];
  };
  selectedGroup: string;
  selectedCode: string;
  data: LogError[];
  startDate: string;
  endDate: string;
  revision: string;
  paginationToken: string;
  environment: string;
}

export interface Logs {
  errors: LogErrors;
}
