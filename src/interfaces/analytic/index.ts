import * as Overall from './overall';
import * as User from './user';
import * as Conversation from './conversation';
import * as Transcript from './transcript';
import * as Root from './root';

export interface DateRange {
  start: number;
  end: number;
}

export interface ActivateAnalyticsRequestAction {
  botId: string;
  deploymentId: string;
  type: 'basic' | 'advanced';
  metrics: string[];
}

export interface Store {
  dateRage: DateRange;
  overall: Overall.Store;
  user: User.Store;
  conversation: Conversation.Store;
  transcript: Transcript.Store;
  root: Root.Store;
}
