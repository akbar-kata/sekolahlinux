import { JsonArray } from 'interfaces';

export interface Store {
  isLoading: boolean;
  error: string | null;
  data: JsonArray;
}
