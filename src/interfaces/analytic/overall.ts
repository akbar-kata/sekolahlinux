import { JsonObject, JsonArray } from 'interfaces';

export interface Loadings {
  overall: boolean;
  bots_growth: boolean;
  users_growth: boolean;
}

export interface Errors {
  overall: string | null;
  bots_growth: string | null;
  users_growth: string | null;
}

export interface Store {
  loadings: Loadings;
  errors: Errors;
  overall: JsonObject;
  botsGrowth: JsonArray;
  usersGrowth: JsonArray;
}

export interface OverallRequest extends JsonObject {
  startTimestamp: number;
  endTimestamp: number;
}
