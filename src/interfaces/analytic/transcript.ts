import { JsonArray } from 'interfaces';

export interface Loadings {
  list: boolean;
  list_new: boolean;
  conversation: boolean;
  conversation_new: boolean;
}

export interface Errors {
  list: string | null;
  list_new: string | null;
  conversation: string | null;
  conversation_new: string | null;
}

// interface More {
//   list: boolean;
//   conversation: boolean;
// }

export interface Store {
  loadings: Loadings;
  errors: Errors;
  // hasOlder: More;
  // hasNewer: More;
  list: JsonArray;
  conversation: JsonArray;
}
