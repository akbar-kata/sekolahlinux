import { JsonArray } from 'interfaces';

export interface Loadings {
  flow: boolean;
  intent: boolean;
  message: boolean;
}

export interface Errors {
  flow: string | null;
  intent: string | null;
  message: string | null;
}

export interface Store {
  loadings: Loadings;
  errors: Errors;
  flow: JsonArray;
  intent: JsonArray;
  message: JsonArray;
}
