import * as Usages from './usage';
import * as Plans from './pricing';

export interface Data {
  id: string;
  deploymentId: string;
  active: number;
  usages: Usages.Data[];
  plans: Plans.Data[];
  rpmLimit: number;
  startDate: number;
  endDate: number;
  kpi: string;
  label: string;
}

export interface DataMap {
  [key: string]: Data;
}

export interface Loadings {
  fetch: boolean;
  add: boolean;
  update: boolean;
  remove: boolean;
}

export interface Errors {
  fetch: string | null;
  add: string | null;
  update: string | null;
  remove: string | null;
}

export interface Store {
  selected: string | null;
  index: string[];
  data: DataMap;
  lastUpdate: number;
  loadings: Loadings;
  errors: Errors;
}
