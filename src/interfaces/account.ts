export interface Bag {
  [key: string]: any;
}

export interface Store {
  isLoading: boolean;
  errors?: Bag;
  messages?: Bag;
}
