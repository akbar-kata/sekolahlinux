export interface Data {
  counters: {
    messages: number;
    sessions: number;
    maus: number;
    users: number;
  };
  startDate: number;
  endDate: number;
}
