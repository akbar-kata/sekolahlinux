import { Json } from '../index';

export interface Open {
  [id: string]: boolean;
}

export interface Data {
  [id: string]: Json;
}

export interface Store {
  open: Open;
  data: Data;
}
