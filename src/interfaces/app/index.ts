import * as Modal from './modal';
import * as Drawer from './drawer';
import * as Sidebar from './sidebar';
import * as Loadings from './loadings';

interface AppStore {
  modal: Modal.Store;
  drawer: Drawer.Store;
  sidebar: Sidebar.Store;
  loadings: Loadings.Store;
}

export default AppStore;
