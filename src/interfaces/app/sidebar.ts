export interface Menu {
  title: string;
  path: string;
  icon: string;
  exact?: boolean;
}

export interface Store {
  isOpen: boolean;
}
