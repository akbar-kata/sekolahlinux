import { DataMap } from 'interfaces/common';

export interface ModuleSetting {
  id: string;
  name: string;
  label: string;
  type: 'text' | 'textarea';
  value: string | null;
}

export interface ModuleSettings {
  [id: string]: ModuleSetting;
}

export interface Module {
  id: string;
  name: string;
  description: string;
  version: string;
  author: string;
  hasUpdate: boolean;
  enabled: boolean;
  changeLog?: string;
  updatedAt?: Date;
  settings?: ModuleSettings;
}

export interface ModuleStore {
  selected: string | null;
  data: DataMap<Module>;
  index: string[];
}
