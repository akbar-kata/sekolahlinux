// Fonts
import './assets/fonts/museo-sans/museo-sans.css';
import './assets/fonts/kata-icons/kata-icons.css';

// Vendor
import 'bootstrap/scss/bootstrap.scss';
import 'react-toggle/style.css';
import 'react-tagsinput/react-tagsinput.css';
import 'assets/sass/vendor/react-toggle.scss';
import 'assets/sass/vendor/react-tags-input.scss';
import 'assets/sass/vendor/react-select.scss';
import 'assets/sass/vendor/react-dates.scss';
import 'assets/sass/vendor/loading.css';
import 'assets/sass/vendor/ace-theme-kata.scss';

// Main
import 'assets/sass/kata-style.scss';
