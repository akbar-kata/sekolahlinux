import React from 'react';
import ReactDOM from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import * as Sentry from '@sentry/browser';

import * as env from 'utils/env';

import configureStore from './stores';
import * as serviceWorker from './utils/registerServiceWorker';

const __DSN__ = env.getRuntimeEnv(
  'REACT_APP_RUNTIME_SENTRY_DSN',
  env.defaultEnvs['REACT_APP_RUNTIME_SENTRY_DSN']
);

Sentry.init({
  dsn: __DSN__,
  release: process.env.REACT_APP_GIT_TAG
});

// Load initial state from window object, else use undefined
const initialState = window.__INITIAL_REDUX_STATE__ || undefined;
const { store, history } = configureStore(initialState);

export async function render() {
  const { Main } = await import('./main');
  ReactDOM.render(
    <AppContainer>
      <Main store={store} history={history} />
    </AppContainer>,
    document.getElementById('root')
  );
}

export async function start() {
  await render();

  if (module.hot) {
    module.hot.accept('./main', render);
  }

  // We don't use service workers, but for safety, we need to call unregister()
  // once so no stale service workers will manipulate the page.
  serviceWorker.unregister();
}
