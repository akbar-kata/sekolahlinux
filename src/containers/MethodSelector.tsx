import React from 'react';
import { connect } from 'react-redux';

import RootStore from 'interfaces/rootStore';
import { getSelected as getSelectedFlow } from 'stores/bot/flow/selectors';
import {
  getData as getMethodData,
  getByFlow as getMethodByFlow
} from 'stores/bot/method/selectors';
import { ReactSelect } from 'components/FormikWrapper';

interface PropsFromState {
  methodIndex: string[];
  methodData: {
    [key: string]: any;
  };
}

interface PropsFromDispatch {}

interface Props extends PropsFromState, PropsFromDispatch {
  field: string;
  filter?: string;
  root?: boolean;
  useRoot: boolean;
  disabled?: boolean;
}

interface States {}

class MethodSelector extends React.Component<Props, States> {
  composeMethodOptions = (): { [key: string]: string }[] => {
    const { methodIndex, methodData } = this.props;
    return methodIndex.map(id => ({
      label: methodData[id].name,
      value: methodData[id].name
    }));
  };

  render() {
    const { field, ...props } = this.props;

    return (
      <ReactSelect
        name={field}
        clearable={false}
        simpleValue
        options={this.composeMethodOptions()}
        {...props}
      />
    );
  }
}

const mapStateToProps = (
  { bot: { flow, method } }: RootStore,
  ownProps
): PropsFromState => {
  const activeFlow = ownProps.root ? '__ROOT__' : getSelectedFlow(flow);
  return {
    methodData: getMethodData(method),
    methodIndex: getMethodByFlow(
      method,
      activeFlow,
      ownProps.useRoot ? true : false
    )
  };
};

const mapDispatchToProps: PropsFromDispatch = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  { pure: false }
)(MethodSelector);
