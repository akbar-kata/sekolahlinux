import React from 'react';
import { connect } from 'react-redux';

import RootStore from 'interfaces/rootStore';
import * as Channel from 'interfaces/channel';
import {
  getChannelIndex,
  getChannelData,
  getChannelError,
  getChannelLoading,
  getChannelSelected,
  getChannelDetail
} from 'stores/deployment/channel/selectors';
import { selectChannel } from 'stores/deployment/channel/actions';

interface PropsFromState {
  error: string | null;
  loading: boolean;
  index: string[];
  data: Channel.DataMap;
  selected: string | null;
  getDetail: (id: string) => Channel.Data;
}

interface PropsFromDispatch {
  selectChannel: (channel: string) => ReturnType<typeof selectChannel>;
}

export interface ChannelContainerProps
  extends PropsFromState,
    PropsFromDispatch {}

export interface Props extends PropsFromState, PropsFromDispatch {
  children: (props: ChannelContainerProps) => React.ReactNode;
}

interface State {}

class ChannelContainer extends React.Component<Props, State> {
  render() {
    const { children, ...rest } = this.props;
    return children({
      ...rest
    });
  }
}
const mapStateToProps = ({
  deployment: { channel }
}: RootStore): PropsFromState => {
  return {
    loading: getChannelLoading(channel, 'fetch'),
    error: getChannelError(channel, 'fetch'),
    index: getChannelIndex(channel),
    data: getChannelData(channel),
    selected: getChannelSelected(channel),
    getDetail: (id: string) => getChannelDetail(channel, id)
  };
};

const mapDispatchToProps: PropsFromDispatch = {
  selectChannel
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ChannelContainer);
