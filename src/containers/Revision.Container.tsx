import React from 'react';
import { connect } from 'react-redux';

import RootStore from 'interfaces/rootStore';
import * as Deployment from 'interfaces/deployment';

import { setRevision } from 'stores/bot/logs/errors/actions';
import { getProjectSelected } from 'stores/project/selectors';
import {
  selectDeployment,
  fetchDeploymentRequest
} from 'stores/deployment/actions';
import {
  getDeploymentData,
  getDeploymentIndex,
  getDeploymentSelected
} from 'stores/deployment/selectors';

interface PropsFromState {
  selectedProject: string | null;
  index: string[];
  data: Deployment.DataMap;
  selected: string | null;
}

interface PropsFromDispatch {
  fetchDeployments: () => ReturnType<typeof fetchDeploymentRequest>;
  setDeployment: (revision: string) => ReturnType<typeof setRevision>;
}

export interface RevisionContainerProps
  extends PropsFromState,
    PropsFromDispatch {}

export interface OwnProps {
  children: (props: RevisionContainerProps) => React.ReactNode;
}

export interface Props extends RevisionContainerProps, OwnProps {}

class RevisionContainer extends React.Component<Props> {
  componentDidMount() {
    if (!this.props.index || this.props.index.length === 0) {
      this.props.fetchDeployments();
    }
  }
  render() {
    const { children, ...rest } = this.props;
    return children({
      ...rest
    });
  }
}

const mapStateToProps = ({ project, deployment }: RootStore) => {
  return {
    selectedProject: getProjectSelected(project),
    index: getDeploymentIndex(deployment),
    data: getDeploymentData(deployment),
    selected: getDeploymentSelected(deployment)
  };
};

const mapDispatchToProps = (dispatch: Function): PropsFromDispatch => {
  return {
    fetchDeployments: () => dispatch(fetchDeploymentRequest()),
    setDeployment: (revision: string) => dispatch(selectDeployment(revision))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RevisionContainer);
