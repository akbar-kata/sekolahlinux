import React from 'react';
import { connect } from 'react-redux';

import RootStore from 'interfaces/rootStore';
import { getAuthSelected } from 'stores/user/auth/selectors';

interface PropsFromState {
  user: string | null;
}

interface PropsFromDispatch {}

interface Props extends PropsFromState, PropsFromDispatch {
  children: (props: any) => React.ReactElement<any>;
}

interface States {}

class CurrentUserContainer extends React.Component<Props, States> {
  render() {
    const { children, ...rest } = this.props;
    return children(rest);
  }
}

const mapStateToProps = ({ auth }: RootStore): PropsFromState => {
  const token = getAuthSelected(auth);
  return {
    user: token ? token.username : null
  };
};

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CurrentUserContainer);
