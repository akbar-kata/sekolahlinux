import React from 'react';
import { Dispatch } from 'redux';
import { connect } from 'react-redux';

import RootStore from 'interfaces/rootStore';
import * as Environment from 'interfaces/deployment/environment';

import { selectDeployment } from 'stores/deployment/actions';
import { selectEnv } from 'stores/deployment/environment/actions';
import {
  getEnvironmentIndex,
  getEnvironmentData,
  getEnvironmentError,
  getEnvironmentLoading,
  getEnvironmentSelected,
  getEnvironmentDetail
} from 'stores/deployment/environment/selectors';

interface PropsFromState {
  error: string | null;
  loading: boolean;
  index: string[];
  data: Environment.DataMap;
  selected: string | null;
  getDetail: (id: string) => Environment.Data;
}

interface PropsFromDispatch {
  selectDeployment: (deployment: string) => ReturnType<typeof selectDeployment>;
  setEnvironment: (env: string) => ReturnType<typeof selectEnv>;
}

export interface EnvironmentContainerProps
  extends PropsFromState,
    PropsFromDispatch {}

export interface Props extends EnvironmentContainerProps {
  children: (props: EnvironmentContainerProps) => React.ReactNode;
}

interface State {}

class EnvironmentContainer extends React.Component<Props, State> {
  render() {
    const { children, ...rest } = this.props;
    return children({
      ...rest
    });
  }
}
const mapStateToProps = (store: RootStore): PropsFromState => {
  const { deployment } = store;

  return {
    loading: getEnvironmentLoading(deployment.environment, 'fetch'),
    error: getEnvironmentError(deployment.environment, 'fetch'),
    index: getEnvironmentIndex(deployment.environment),
    data: getEnvironmentData(deployment.environment),
    selected: getEnvironmentSelected(deployment.environment),
    getDetail: (id: string) => getEnvironmentDetail(deployment.environment, id)
  };
};

const mapDispatchToProps = (dispatch: Dispatch): PropsFromDispatch => {
  return {
    selectDeployment: (id: string) => dispatch(selectDeployment(id)),
    setEnvironment: (env: string) => dispatch(selectEnv(env))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EnvironmentContainer);
