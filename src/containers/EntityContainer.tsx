import React from 'react';
import { connect } from 'react-redux';

import RootStore from 'interfaces/rootStore';
import {
  getEntityIndex,
  getEntityData,
  getEntityError,
  getEntityLoading
} from 'stores/nlu/entity/selectors';

interface PropsFromState {
  error: string | null;
  loading: boolean;
  entities: any[];
  traits: any[];
}

interface PropsFromDispatch {}

interface EntityContainerProps extends PropsFromState, PropsFromDispatch {}

interface Props extends EntityContainerProps {
  children: (props: EntityContainerProps) => React.ReactNode;
}

interface State {}

class EntityContainer extends React.Component<Props, State> {
  render() {
    const { children, ...rest } = this.props;
    return children({
      ...rest
    });
  }
}
const mapStateToProps = ({ nlu: { entity } }: RootStore): PropsFromState => {
  const index = getEntityIndex(entity);
  const data = getEntityData(entity);
  return {
    loading: getEntityLoading(entity, 'fetch'),
    error: getEntityError(entity, 'fetch'),
    entities: index.filter(id => data[id].type !== 'trait').map(id => data[id]),
    traits: index.filter(id => data[id].type === 'trait').map(id => data[id])
  };
};

export default connect(mapStateToProps)(EntityContainer);
