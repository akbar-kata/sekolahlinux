import React from 'react';
import { Dispatch } from 'redux';
import { connect } from 'react-redux';

import RootStore from 'interfaces/rootStore';
import * as Deployment from 'interfaces/deployment';
import {
  getDeploymentIndex,
  getDeploymentData,
  getDeploymentError,
  getDeploymentLoading,
  getDeploymentSelected,
  getDeploymentDetail
} from 'stores/deployment/selectors';
import { selectDeployment } from 'stores/deployment/actions';
import { setEnvironment } from 'stores/bot/logs/errors/actions';
import { getEnvironment } from 'stores/bot/logs/errors/selectors';

interface PropsFromState {
  error: string | null;
  loading: boolean;
  index: string[];
  data: Deployment.DataMap;
  selected: string | null;
  selectedEnv: string;
  getDetail: (id: string) => Deployment.Data;
}

interface PropsFromDispatch {
  selectDeployment: (deployment: string) => ReturnType<typeof selectDeployment>;
  setEnvironment: (env: string) => ReturnType<typeof setEnvironment>;
}

export interface DeploymentContainerProps
  extends PropsFromState,
    PropsFromDispatch {}

export interface Props extends DeploymentContainerProps {
  children: (props: DeploymentContainerProps) => React.ReactNode;
}

interface State {}

class DeploymentContainer extends React.Component<Props, State> {
  render() {
    const { children, ...rest } = this.props;
    return children({
      ...rest
    });
  }
}
const mapStateToProps = (store: RootStore): PropsFromState => {
  const { deployment, bot } = store;

  return {
    loading: getDeploymentLoading(deployment, 'fetch'),
    error: getDeploymentError(deployment, 'fetch'),
    index: getDeploymentIndex(deployment),
    data: getDeploymentData(deployment),
    selected: getDeploymentSelected(deployment),
    getDetail: (id: string) => getDeploymentDetail(deployment, id),
    selectedEnv: getEnvironment(bot.log.errors)
  };
};

const mapDispatchToProps = (dispatch: Dispatch): PropsFromDispatch => {
  return {
    selectDeployment: (id: string) => dispatch(selectDeployment(id)),
    setEnvironment: (env: string) => dispatch(setEnvironment(env))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DeploymentContainer);
