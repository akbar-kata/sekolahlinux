import React from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps, withRouter } from 'react-router-dom';

import RootStore from 'interfaces/rootStore';
import { getProjectSelected } from 'stores/project/selectors';

interface PropsFromState {
  selected: string | null;
}

interface PropsFromDispatch {}

interface RouteParams {
  selectedId: string;
}

export interface Props
  extends PropsFromState,
    PropsFromDispatch,
    RouteComponentProps<RouteParams> {
  children: (selectedId: string) => React.ReactNode;
}

interface State {}

class SelectedUrl extends React.Component<Props, State> {
  render() {
    const { children, match, selected } = this.props;
    return children(selected || match.params.selectedId);
  }
}

const mapStateToProps = (rootStore: RootStore): PropsFromState => {
  const { project } = rootStore;
  return {
    selected: getProjectSelected(project)
  };
};

const mapDispatchToProps: PropsFromDispatch = {};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(SelectedUrl)
);
