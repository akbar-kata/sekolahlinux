import React from 'react';
import { connect } from 'react-redux';

import RootStore from 'interfaces/rootStore';
import {
  getData as getNLUData,
  getIndex as getNLUIndex
} from 'stores/bot/nlu/selectors';
import { ReactSelect } from 'components/FormikWrapper';

interface PropsFromState {
  nluIndex: string[];
  nluData: {
    [key: string]: any;
  };
}

interface PropsFromDispatch {}

interface Props extends PropsFromState, PropsFromDispatch {
  field: string;
  filter?: string;
}

interface States {}

class NLUSelector extends React.Component<Props, States> {
  composeNLUOptions = (): { [key: string]: string }[] => {
    const { nluIndex, nluData } = this.props;
    return nluIndex.map(id => ({
      label: nluData[id].name,
      value: nluData[id].name
    }));
  };

  render() {
    return (
      <ReactSelect
        name={this.props.field}
        clearable={false}
        simpleValue
        options={this.composeNLUOptions()}
      />
    );
  }
}

const mapStateToProps = ({ bot: { nlu } }: RootStore): PropsFromState => {
  return {
    nluData: getNLUData(nlu),
    nluIndex: getNLUIndex(nlu)
  };
};

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  { pure: false }
)(NLUSelector);
