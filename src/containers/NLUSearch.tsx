import React from 'react';
import { connect } from 'react-redux';

import RootStore from 'interfaces/rootStore';
import { searchEntitiesRequest as searchNLURequest } from 'stores/nlu/entity/actions';
// import { getSearched } from 'stores/nlu/nlu/selectors';

interface PropsFromState {
  // options: any[];
}

interface PropsFromDispatch {
  search: Function;
}

export interface Props extends PropsFromState, PropsFromDispatch {
  children: (props: any) => React.ReactElement<any>;
}

interface State {}

class NLUSearch extends React.Component<Props, State> {
  render() {
    const { children, ...rest } = this.props;
    return children(rest);
  }
}
const mapStateToProps = ({ nlu }: RootStore): PropsFromState => {
  return {};
  // return {
  //   options: getSearched(nlu)
  // };
};

const mapDispatchToProps: PropsFromDispatch = {
  search: (keyword: string) => searchNLURequest(keyword)
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NLUSearch);
