import React from 'react';
import classnames from 'classnames';

import styles from './Skeleton.module.scss';

interface SkeletonProps {
  className?: string;
  style?: React.CSSProperties;
  numberOfLines?: number;
  small?: boolean;
}

const Skeleton: React.FC<SkeletonProps> = ({
  className,
  numberOfLines,
  small
}) => {
  if (numberOfLines && numberOfLines > 1) {
    return (
      <div className={classnames(styles.multiline, className)}>
        {[...Array(numberOfLines)].map((_, i) => (
          <div
            key={i}
            className={classnames(styles.root, small && styles.small)}
          />
        ))}
      </div>
    );
  }

  return (
    <div
      className={classnames(styles.root, small && styles.small, className)}
    />
  );
};

Skeleton.defaultProps = {
  numberOfLines: 1
};

export default Skeleton;
