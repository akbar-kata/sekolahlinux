import * as React from 'react';

import './EmptyMessage.scss';
import classNames from 'classnames';

interface Props {
  autoSize?: boolean;
  image?: string;
  title?: string | React.ReactElement<any>;
}

export default class EmptyMessage extends React.Component<Props> {
  static defaultProps = {
    autoSize: false,
    image: require('assets/images/form-empty.svg'),
    title: 'Empty'
  };

  render() {
    return (
      <div className="kata-empty-message">
        <img
          className={classNames({
            'kata-empty-message__image-autosize': this.props.autoSize
          })}
          src={this.props.image}
          alt="Empty Message"
        />
        <div className="kata-empty-message__title">{this.props.title}</div>
        <div className="kata-empty-message__description">
          {this.props.children}
        </div>
      </div>
    );
  }
}
