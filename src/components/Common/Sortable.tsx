import React from 'react';
import { findDOMNode } from 'react-dom';
import { DragSource, DropTarget } from 'react-dnd';

const ITEM_TYPE = 'SORTABLE_ITEM';

const actionSource = {
  isDragging(props: any, monitor: any) {
    // If your component gets unmounted while dragged
    // (like a card in Kanban board dragged between lists)
    // you can implement something like this to keep its
    // appearance dragged:
    return monitor.getItem().id === props.id;
  },

  beginDrag(props: any) {
    return {
      id: props.id,
      index: props.index
    };
  }
};

const actionTarget = {
  hover(props: any, monitor: any, component: any) {
    const dragIndex = monitor.getItem().index;
    const hoverIndex = props.index;

    // Don't replace items with themselves
    if (dragIndex === hoverIndex) {
      return;
    }
    // Determine rectangle on screen
    const hoverBoundingRect = (findDOMNode(
      component
    ) as any).getBoundingClientRect();
    // Get vertical middle
    const hoverMiddleY = (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2;

    // Determine mouse position
    const clientOffset = monitor.getClientOffset();

    // Get pixels to the top
    const hoverClientY = clientOffset.y - hoverBoundingRect.top;

    // Only perform the move when the mouse has crossed half of the items height
    // When dragging downwards, only move when the cursor is below 50%
    // When dragging upwards, only move when the cursor is above 50%

    // Dragging downwards
    if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
      return;
    }

    // Dragging upwards
    if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
      return;
    }

    // Time to actually perform the action
    props.onMove(dragIndex, hoverIndex);

    // Note: we're mutating the monitor item here!
    // Generally it's better to avoid mutations,
    // but it's good here for the sake of performance
    // to avoid expensive index searches.
    monitor.getItem().index = hoverIndex;
  }
};

class SortableComponent extends React.Component<any, any> {
  render() {
    const {
      isDragging,
      connectDragSource,
      connectDropTarget,
      children
    } = this.props;

    const opacity = isDragging ? 0 : 1;

    return connectDragSource(
      connectDropTarget(<div style={{ opacity }}>{children}</div>)
    );
  }
}

const PreparedSource = DragSource(
  ITEM_TYPE,
  actionSource,
  (connectDrag, monitor) => ({
    connectDragSource: connectDrag.dragSource(),
    isDragging: monitor.isDragging()
  })
)(SortableComponent);

const PreparedTarget = DropTarget(ITEM_TYPE, actionTarget, connectDrag => ({
  connectDropTarget: connectDrag.dropTarget()
}))(PreparedSource);

export default PreparedTarget;
