import React, { Component } from 'react';

import { Modal, ModalHeader, ModalBody, ModalFooter } from 'components/Modal';

interface Props {
  children?: any;
  title: string;
  message: string;
  open: boolean;
  oncancel(): void;
}

interface States {
  open: boolean;
}

class ConfirmDialogPrompt extends Component<Props, States> {
  state = {
    open: false
  };
  constructor(props: Props) {
    super(props);
    this.state = {
      open: props.open ? props.open : true
    };
  }

  static getDerivedStateFromProps(nextProps: Props, prevState: Props) {
    if (nextProps) {
      return {
        open: nextProps.open
      };
    }
    return null;
  }

  render() {
    return (
      <Modal show={this.state.open} onClose={this.props.oncancel}>
        <ModalHeader>{this.props.title}</ModalHeader>
        <ModalBody>{this.props.message}</ModalBody>
        <ModalFooter>{this.props.children}</ModalFooter>
      </Modal>
    );
  }
}

export default ConfirmDialogPrompt;
