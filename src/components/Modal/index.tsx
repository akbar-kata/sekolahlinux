import Modal from './Modal';
import ModalHeader from './ModalHeader';
import ModalBody from './ModalBody';
import ModalFooter from './ModalFooter';
import confirm from './ConfirmDialog';

export { Modal, ModalHeader, ModalBody, ModalFooter, confirm };
