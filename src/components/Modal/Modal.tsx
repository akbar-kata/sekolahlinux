import React from 'react';
import ReactDOM from 'react-dom';

import ModalWrapper from './ModalWrapper';

interface Props {
  className?: string;
  show: boolean;
  noBackdrop?: boolean;
  onClose(): void;
}

interface States {
  show: boolean;
  visible: boolean;
}

class Modal extends React.Component<Props, States> {
  el: HTMLDivElement;

  constructor(props: Props) {
    super(props);
    this.el = document.createElement('div');
    this.state = {
      show: false,
      visible: false
    };
  }

  static getDerivedStateFromProps(props: Props, state: States) {
    if (!props.show) {
      return {
        show: props.show,
        visible: false
      };
    }

    return {
      show: props.show,
      visible: true
    };
  }

  componentDidMount() {
    try {
      document.body.appendChild(this.el);
    } catch (error) {
      // do nothing
    }
  }

  componentWillUnmount() {
    try {
      document.body.removeChild(this.el);
    } catch (error) {
      // do nothing
    }
  }

  onClose = () => {
    this.props.onClose();
  };

  render() {
    const { noBackdrop, className } = this.props;
    const { show, visible } = this.state;
    const wrapper = (
      <ModalWrapper
        {...{ className, noBackdrop, show, visible, onClose: this.onClose }}
      >
        {this.props.children}
      </ModalWrapper>
    );
    return ReactDOM.createPortal(wrapper, this.el);
  }
}

export default Modal;
