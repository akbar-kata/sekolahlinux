import React from 'react';
import classNames from 'classnames';

interface Props {
  className?: string;
  children?: any;
}

export default class ModalBody extends React.Component<Props> {
  render() {
    const classes = classNames(
      'modal-body',
      'kata-modal__body',
      this.props.className
    );

    return <div className={classes}>{this.props.children}</div>;
  }
}
