import React from 'react';

interface States {
  active: boolean;
}
interface Props {
  active?: boolean;
  onToggle?(state: boolean);
  renderActive(): JSX.Element;
  renderInactive(): JSX.Element;
}

export default class ToggleButton extends React.Component<Props, States> {
  static defaultProps = {
    active: false
  };

  state = {
    active: false
  };

  static getDerivedStateFromProps(props: Props) {
    return {
      active: props.active
    };
  }

  handleToggle = () => {
    this.setState(
      {
        active: !this.state.active
      },
      () => {
        if (this.props.onToggle) {
          this.props.onToggle(this.state.active);
        }
      }
    );
  };

  render() {
    return (
      <div className="kata-toggle-button" onClick={this.handleToggle}>
        {this.renderState()}
      </div>
    );
  }

  renderState() {
    if (this.state.active) {
      return this.props.renderActive();
    }
    return this.props.renderInactive();
  }
}
