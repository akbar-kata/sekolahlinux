import React from 'react';
import classNames from 'classnames';

interface Props {
  children: any;
  isActive: boolean;
  animating: boolean;
  direction: string;
  className?: string;
  onAnimationEnd: Function;
}

class LightBoxItem extends React.Component<Props> {
  private el: React.RefObject<HTMLDivElement>;

  constructor(props: Props) {
    super(props);

    this.slideAnimation = this.slideAnimation.bind(this);
    this.el = React.createRef();
  }

  componentDidMount() {
    this.el.current!.addEventListener('animationend', () => {
      this.props.onAnimationEnd();
    });
  }

  render() {
    const { className, isActive, children, animating, direction } = this.props;
    const classes = classNames(
      'kata-lightbox__item',
      {
        active: isActive,
        'slide-left': animating && direction === 'next' && isActive,
        'slide-right': animating && direction === 'prev' && isActive
      },
      className
    );

    return (
      <div
        className={classes}
        style={{
          animationName: this.slideAnimation()
        }}
        ref={this.el}
      >
        {children}
      </div>
    );
  }

  private slideAnimation() {
    return this.props.direction === 'prev' ? 'prevSlide' : 'nextSlide';
  }
}

export default LightBoxItem;
