import React, { ReactElement, Fragment } from 'react';
import ReactDOM from 'react-dom';
import classNames from 'classnames';

import { Button } from 'components/Button';

import LightBoxArrow from './LightBoxArrow';
import LightBoxItem from './LightBoxItem';

interface Props {
  currentIndex: number;
  isOpen: boolean;
  slides: React.ReactElement<any>[];
  className?: string;
  onClose: () => void;
}

interface State {
  position: number;
  nextPosition: number;
  direction: string;
  animating: boolean;
}

class LightBox extends React.Component<Props, State> {
  static defaultProps = {
    currentIndex: 0
  };

  el: HTMLDivElement;
  totalItems: number = 0;

  state = {
    position: 0,
    nextPosition: 0,
    direction: 'next',
    animating: false
  };

  constructor(props: Props) {
    super(props);

    this.el = document.createElement('div');
    this.renderItems = this.renderItems.bind(this);
    this.renderArrows = this.renderArrows.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.preventEventPropagation = this.preventEventPropagation.bind(this);
    this.setTotalItems = this.setTotalItems.bind(this);
    this.nextItem = this.nextItem.bind(this);
    this.prevItem = this.prevItem.bind(this);
    this.handleAnimationEnd = this.handleAnimationEnd.bind(this);
  }

  static getDerivedStateFromProps(props: Props, state: State) {
    if (props.currentIndex) {
      return {
        ...state,
        position: props.currentIndex
      };
    }

    return null;
  }

  componentDidMount() {
    try {
      document.body.appendChild(this.el);
    } catch (error) {
      // do nothing
    }
  }

  componentWillUnmount() {
    try {
      document.body.removeChild(this.el);
    } catch (error) {
      // do nothing
    }
  }

  componentDidUpdate(prevProps: Props, prevState: State) {
    // to add/remove noscroll class on document body
    if (prevProps.isOpen !== this.props.isOpen) {
      if (this.props.isOpen) {
        document.body.classList.add('noscroll');
      } else {
        document.body.classList.remove('noscroll');
      }
    }
  }

  render() {
    const items = this.renderItems();
    const arrows = this.renderArrows();
    const classes = classNames('kata-lightbox', this.props.className);
    const backdropClasses = classNames('kata-lightbox__backdrop', {
      'kata-lightbox--show': this.props.isOpen
    });
    const carouselWrapper = (
      <div className={backdropClasses} tabIndex={-1} onClick={this.handleClose}>
        <div className={classes} onClick={this.preventEventPropagation}>
          {items}
          {arrows}
          <Button
            type="button"
            className="kata-lightbox__close"
            aria-label="Close"
            isIcon
            onClick={this.props.onClose}
          >
            <i className="icon-close" />
          </Button>
        </div>
      </div>
    );

    return ReactDOM.createPortal(carouselWrapper, this.el);
  }

  private renderArrows() {
    return (
      <Fragment>
        <LightBoxArrow direction="left" onClick={this.prevItem} />
        <LightBoxArrow direction="right" onClick={this.nextItem} />
      </Fragment>
    );
  }

  // Get carousel item from children.
  private renderItems() {
    const { slides } = this.props;
    const { direction, animating, position } = this.state;
    this.setTotalItems(slides.length || 0);

    return slides.map((slide: ReactElement<any>, index: number) => {
      const isActiveItem = position === index;

      return (
        <LightBoxItem
          key={index}
          isActive={isActiveItem}
          direction={direction}
          animating={animating}
          onAnimationEnd={this.handleAnimationEnd}
        >
          {slide}
        </LightBoxItem>
      );
    });
  }

  private preventEventPropagation(event: React.SyntheticEvent<any>) {
    event.preventDefault();
    event.stopPropagation();
  }

  private handleClose(e: React.SyntheticEvent<any>) {
    this.props.onClose();
  }

  private setTotalItems(totalItems: number) {
    this.totalItems = totalItems;
  }

  private nextItem() {
    const nextIndex =
      this.state.position === this.totalItems - 1 ? 0 : this.state.position + 1;
    this.doSliding('next', nextIndex);
  }

  private prevItem() {
    const nextIndex =
      this.state.position === 0 ? this.totalItems - 1 : this.state.position - 1;
    this.doSliding('prev', nextIndex);
  }

  private doSliding(direction: string, nextPosition: number) {
    this.setState({
      nextPosition,
      direction,
      animating: true
    });
  }

  private handleAnimationEnd() {
    if (this.state.animating) {
      this.setState({
        animating: false,
        position: this.state.nextPosition
      });
    }
  }
}

export default LightBox;
