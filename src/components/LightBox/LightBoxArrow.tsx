import React from 'react';
import classNames from 'classnames';

import { FloatingButton } from 'components/Button';

type ArrowDirection = 'left' | 'right';

interface Props {
  direction: ArrowDirection;
  className?: string;
  onClick: Function;
}

class LightBoxArrow extends React.Component<Props> {
  render() {
    const { direction, className, onClick } = this.props;
    const classes = classNames(
      'kata-lightbox__arrow',
      `kata-lightbox__arrow--${this.renderArrow(direction)}`,
      className
    );

    return (
      <FloatingButton
        icon={`arrow-${direction}`}
        className={classes}
        onClick={onClick}
      />
    );
  }

  private renderArrow(direction: ArrowDirection) {
    const isPrev = direction === 'left';

    return isPrev ? 'prev' : 'next';
  }
}

export default LightBoxArrow;
