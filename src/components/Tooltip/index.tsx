import './Tooltip.scss';

import Tooltip from './Tooltip';
import TooltipTarget from './TooltipTarget';

export { Tooltip, TooltipTarget };
