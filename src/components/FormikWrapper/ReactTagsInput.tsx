import React from 'react';
import classnames from 'classnames';
import TagsInput from 'react-tagsinput';
import { Field, FieldAttributes } from 'formik';

import InputWrapper from './InputWrapper';

interface Props extends FieldAttributes<any> {}

function FormInputReactTagsInput({
  className,
  disabled,
  onChange,
  onBlur,
  ...props
}: Props) {
  return (
    <Field {...props}>
      {({ field, form }) => {
        const classes = classnames('react-tagsinput', className);
        return (
          <InputWrapper>
            <TagsInput
              {...props} // Send the rest of your props to React-Select
              value={field.value || []} // Set the value to the forms value
              focusedClassName="react-tagsinput--focused"
              onChange={val => {
                if (typeof onChange === 'function') {
                  onChange(val);
                }
                form.setFieldValue(props.name, val);
              }} // On Change, update the form value
              className={classes}
              disabled={!!disabled}
              addKeys={[9, 13, 188]}
            />
            {!disabled ? (
              <small className="kata-form__tag-label">
                You can input mutiple labels separated by comma or tab
              </small>
            ) : null}
          </InputWrapper>
        );
      }}
    </Field>
  );
}

export default FormInputReactTagsInput;
