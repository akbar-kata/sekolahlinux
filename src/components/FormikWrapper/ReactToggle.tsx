import React, { SFC } from 'react';
import { Field, FieldAttributes } from 'formik';
import Toggle from 'react-toggle';

import InputWrapper from './InputWrapper';

interface Props extends FieldAttributes<any> {
  pure?: boolean;
  noFalse?: boolean;
  label?: string;
  innerClassName?: string;
}

const ReactToggle: SFC<Props> = props => {
  const {
    pure = false,
    label,
    innerClassName,
    noFalse,
    onChange,
    ...rest
  } = props;

  return (
    <Field {...rest}>
      {({ field, form }) => (
        <InputWrapper name={rest.name}>
          <label className={innerClassName}>
            <Toggle
              {...rest}
              checked={!!field.value}
              onChange={event => {
                if (typeof onChange === 'function') {
                  onChange(event);
                }
                form.setFieldValue(
                  rest.name,
                  noFalse
                    ? !event.target.checked
                      ? undefined
                      : true
                    : event.target.checked
                );
              }}
              onBlur={() => form.setFieldTouched(rest.name)}
            />
            {!!label && <span className="ml-1">{label}</span>}
          </label>
        </InputWrapper>
      )}
    </Field>
  );
};

export default ReactToggle;
