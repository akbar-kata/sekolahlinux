import React from 'react';
import shortid from 'shortid';
import AceEditor from 'react-ace';
import { Field, FieldAttributes } from 'formik';
import 'brace';

import 'brace/mode/javascript';
import 'brace/mode/json';
import 'brace/mode/yaml';
import 'brace/theme/tomorrow';

import InputWrapper from './InputWrapper';

const beautify = require('js-beautify').js_beautify;

interface Props extends FieldAttributes<any> {
  mode: 'javascript' | 'json' | 'yaml';
  width?: number;
  height?: number;
  autoFormat?: boolean;
}

interface State {
  elId: string;
  style: {
    maxWidth: number;
    border: string;
  };
}

class FormInputReactAce extends React.Component<Props, State> {
  constructor(props: any) {
    super(props);
    this.state = {
      elId: shortid.generate(),
      style: {
        maxWidth: 500,
        border: '1px solid #ddd'
      }
    };
  }

  componentDidMount() {
    const codeEl = document.getElementById(this.state.elId);
    this.setState({
      style: {
        maxWidth:
          codeEl && codeEl.parentElement
            ? codeEl.parentElement.clientWidth
            : 500,
        border: '1px solid #ddd'
      }
    });
  }

  render() {
    const {
      autoFormat,
      mode,
      width,
      height,
      onChange,
      onBlur,
      ...props
    } = this.props;

    return (
      <Field {...props}>
        {({ field, form, meta }) => {
          return (
            <InputWrapper name={props.name} meta={meta}>
              <div id={this.state.elId} style={this.state.style}>
                <AceEditor
                  mode={mode}
                  theme="tomorrow"
                  value={field.value}
                  onChange={(value, event) => {
                    if (typeof onChange === 'function') {
                      onChange(event, value);
                    }
                    form.setFieldValue(props.name, value);
                  }}
                  onBlur={event => {
                    if (typeof onBlur === 'function') {
                      onBlur(event);
                    }
                    let newValue = field.value || '';
                    if (autoFormat) {
                      newValue = beautify(newValue, {
                        indent_size: 2
                      });
                    }
                    form.setFieldValue(props.name, newValue);
                  }}
                  name={this.state.elId}
                  editorProps={{ $blockScrolling: true }}
                  width={`${width || this.state.style.maxWidth - 5}px`}
                  height={`${height || 300}px`}
                  {...props}
                />
              </div>
            </InputWrapper>
          );
        }}
      </Field>
    );
  }
}

export default FormInputReactAce;
