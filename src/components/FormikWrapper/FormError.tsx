import React from 'react';
import { connect, FormikContext } from 'formik';
import get from 'lodash-es/get';

interface Props {
  name?: string;
}

const FormError = (props: Props & { formik: FormikContext<any> }) => {
  const error = get(props, `formik.errors.${props.name || ''}`, false);

  return typeof error === 'string' ? (
    <div className="kata-form__error-label">{error}</div>
  ) : null;
};

export default connect<Props>(FormError);
