import React, { ReactNode } from 'react';
import { Field, FastField, FieldAttributes } from 'formik';

import InputWrapper from './InputWrapper';

interface Props extends FieldAttributes<any> {
  prependElement?: ReactNode;
  appendElement?: ReactNode;
  pure?: boolean;
}

const NewField = (props: Props) => {
  const {
    prependElement,
    appendElement,
    pure = true,
    component = 'input',
    onChange,
    onBlur,
    children,
    ...rest
  } = props;

  const renderComponent = ({
    value = '',
    onChange: onFieldChange,
    ...field
  }) => {
    const handleChange = (e: React.ChangeEvent<any>) => {
      if (typeof onChange === 'function') {
        onChange(e);
      }
      return onFieldChange(e);
    };

    if (typeof component === 'string') {
      const { innerRef, ...childProps } = rest;
      return React.createElement(component as any, {
        ref: innerRef,
        ...field,
        ...childProps,
        value,
        children,
        onChange: handleChange
      });
    }

    return React.createElement(component as any, {
      ...field,
      ...rest,
      value,
      children,
      onChange: handleChange
    });
  };

  const renderWrapper = field => {
    if (prependElement || appendElement) {
      return (
        <div className="input-group">
          {prependElement && (
            <div className="kata-input-group--prepend input-group-prepend">
              {prependElement}
            </div>
          )}
          {renderComponent(field)}
          {appendElement && (
            <div className="kata-input-group--append input-group-append">
              {appendElement}
            </div>
          )}
        </div>
      );
    }
    return renderComponent(field);
  };

  const Element = pure ? FastField : Field;

  return (
    <Element {...rest}>
      {({ field, meta }) => (
        <InputWrapper name={rest.name} meta={meta}>
          {renderWrapper(field)}
        </InputWrapper>
      )}
    </Element>
  );
};

export default NewField;
