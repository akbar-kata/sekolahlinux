import React from 'react';
import Slider from 'react-rangeslider';
import { Field, FieldAttributes } from 'formik';
import isNumber from 'lodash-es/isNumber';

import InputWrapper from './InputWrapper';

import 'assets/sass/vendor/react-rangeslider.scss';

interface Props extends FieldAttributes<any> {
  min?: number;
  max?: number;
  step?: number;
  labels?: { 0: string; 100: string };
  tooltip?: boolean;
}

function ReactRangeSlider({
  min = 0,
  max = 100,
  step = 1,
  labels = {
    0: '0',
    100: '100'
  },
  tooltip = false,
  onChange,
  ...props
}: Props) {
  return (
    <Field {...props}>
      {({ field, form }) => (
        <InputWrapper>
          <Slider
            {...props}
            value={field.value}
            handleLabel={isNumber(field.value) ? field.value.toString() : ''}
            labels={labels}
            min={min}
            max={max}
            step={step}
            onChange={value => {
              if (typeof onChange === 'function') {
                onChange(value);
              }
              form.setFieldValue(props.name, value);
            }}
            tooltip={tooltip}
          />
        </InputWrapper>
      )}
    </Field>
  );
}

export default ReactRangeSlider;
