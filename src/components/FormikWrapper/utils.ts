import cleandeep from 'clean-deep';
import isEmpty from 'lodash-es/isEmpty';

export const isValid = (errors): boolean => {
  return isEmpty(cleandeep(errors));
};

export const cleanDeep = (errors): any => {
  return cleandeep(errors);
};
