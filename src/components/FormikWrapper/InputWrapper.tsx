import React, { ReactNode } from 'react';
import classnames from 'classnames';
import { connect, FormikContext } from 'formik';
import get from 'lodash-es/get';

interface Props {
  name?: string;
  meta?: { error?: string; touched?: boolean };
  children: ReactNode;
}

const InputWrapper = (props: Props & { formik: FormikContext<any> }) => {
  let error;
  let touched;
  if (props.meta) {
    error = get(props, `meta.error`, false);
    touched = get(props, `meta.touched`, false);
  } else {
    error = get(props, `formik.errors.${props.name || ''}`, false);
    touched = get(props, `formik.touched.${props.name || ''}`, false);
  }

  return (
    <div
      className={classnames({
        'has-error': error && touched
      })}
    >
      {props.children}
      {error && touched && (
        <div className="kata-form__error-label">{error}</div>
      )}
    </div>
  );
};

export default connect<Props>(InputWrapper);
