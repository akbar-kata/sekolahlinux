import React, { SFC } from 'react';
import { Field, FieldAttributes } from 'formik';
import Select from 'react-select';

import InputWrapper from './InputWrapper';

interface Props extends FieldAttributes<any> {}

const ReactSelect: SFC<Props> = ({ onChange, ...props }: Props) => (
  <Field {...props}>
    {({ field, form }) => (
      <InputWrapper name={props.name}>
        <Select
          {...props}
          onChange={value => {
            if (typeof onChange === 'function') {
              onChange(value);
            }
            form.setFieldValue(props.name, value);
          }}
          onBlur={() => form.setFieldTouched(props.name)}
          value={field.value}
          className="kata-form__input-select"
          clearRenderer={() => {
            return <i className="icon-remove kata-form__input-select--icon" />;
          }}
        />
      </InputWrapper>
    )}
  </Field>
);

export default ReactSelect;
