import React from 'react';
import DateTime from 'react-datetime';
import { Field, FieldAttributes } from 'formik';

import 'react-datetime/css/react-datetime.css';

import InputWrapper from './InputWrapper';

interface Props extends FieldAttributes<any> {}

function FormInputReactDateTime({ onChange, onBlur, ...props }: Props) {
  return (
    <Field {...props}>
      {({ field, form }) => {
        return (
          <InputWrapper name={props.name}>
            <DateTime
              {...props} // Send the rest of your props to React-Select
              value={field.value} // Set the value to the forms value
              onChange={val => {
                if (typeof onChange === 'function') {
                  onChange(val);
                }
                form.setFieldValue(props.name, val);
              }} // On Change, update the form value
              onBlur={() => {
                if (typeof onBlur === 'function') {
                  onBlur();
                }
              }} // And the same goes for touched
            />
          </InputWrapper>
        );
      }}
    </Field>
  );
}

export default FormInputReactDateTime;
