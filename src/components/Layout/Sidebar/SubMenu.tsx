import React from 'react';
import { NavLink } from 'react-router-dom';

interface Props {
  to: string;
  exact?: boolean;
  icon: string;
  children: any;
}

export default (props: Props) => (
  <NavLink
    className="kata-sidebar__sub-menu"
    activeClassName="kata-sidebar__sub-menu--active"
    to={props.to}
    exact={props.exact || false}
  >
    <i className={`icon-${props.icon} kata-sidebar__sub-icon`} />{' '}
    {props.children}
  </NavLink>
);
