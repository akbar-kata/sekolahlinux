import React, { ReactElement } from 'react';
import classnames from 'classnames';
import { Scrollbars } from 'react-custom-scrollbars';

import './Sidebar.scss';

interface Props {
  collapsed?: boolean;
  hasTop?: boolean;
  children: any;
  isHidden?: boolean;
}

export default (props: Props) => {
  const classes = classnames(
    'kata-sidebar',
    props.collapsed ? 'kata-sidebar--collapsed' : false,
    props.isHidden ? 'kata-sidebar--hidden' : false
  );
  return (
    <div className={classes}>
      <div
        className={classnames(
          'kata-sidebar__wrapper',
          props.hasTop ? 'kata-sidebar__wrapper--has-top' : false
        )}
      >
        <Scrollbars>
          {React.Children.map(props.children, (Item: ReactElement<any>) => {
            return Item &&
              Item.type &&
              ['SidebarMain', 'SidebarSub'].some(
                type => type === (Item.type as any).displayName
              )
              ? React.cloneElement(Item, {
                  hasTop: props.hasTop,
                  collapsed: props.collapsed
                })
              : Item;
          })}
        </Scrollbars>
      </div>
    </div>
  );
};
