import React, { SFC } from 'react';
import classnames from 'classnames';

interface Props {
  children: any;

  // Private
  collapsed?: boolean;
  hasTop?: boolean;
}

const SidebarMain: SFC<Props> = (props: Props) => (
  <div
    className={classnames(
      'kata-sidebar__main',
      props.hasTop ? 'kata-sidebar__main--has-top' : false
    )}
  >
    {props.children}
  </div>
);

SidebarMain.displayName = 'SidebarMain';

export default SidebarMain;
