import React from 'react';
import { NavLink } from 'react-router-dom';

interface Props {
  to: string;
  exact?: boolean;
  icon: string;
  children: string;
}

export default (props: Props) => (
  <NavLink
    className="kata-sidebar__main-menu"
    activeClassName="kata-sidebar__main-menu--active"
    to={props.to}
    exact={props.exact || false}
  >
    <div className="kata-sidebar__main-menu-icon">
      <i className={`icon-${props.icon}`} />
    </div>
    {props.children}
  </NavLink>
);
