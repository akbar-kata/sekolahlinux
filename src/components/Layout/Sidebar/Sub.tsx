import React, { SFC } from 'react';

interface Props {
  titleElement?: React.ReactElement<any> | null;
  children: any;

  // Private
  collapsed?: boolean;
  hasTop?: boolean;
}

const SidebarSub: SFC<Props> = (props: Props) => (
  <div className="kata-sidebar__sub">
    {props.titleElement && props.titleElement}
    {props.children}
  </div>
);

SidebarSub.displayName = 'SidebarSub';

export default SidebarSub;
