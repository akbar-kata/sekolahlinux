import React from 'react';
import classnames from 'classnames';

import './Container.scss';

interface Props {
  children: any;
  hasTop?: boolean;
}

export default (props: Props) => (
  <div
    className={classnames(
      'kata-container',
      props.hasTop ? 'kata-container--has-top' : false
    )}
  >
    {props.children}
  </div>
);
