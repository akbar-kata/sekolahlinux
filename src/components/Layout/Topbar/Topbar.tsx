import React, { SFC } from 'react';
import { NavLink } from 'react-router-dom';

import './Topbar.scss';

interface Props {
  logo?: string;
  children: any;
}

const Topbar: SFC<Props> = (props: Props) => (
  <div className="kata-topbar">
    {props.logo && (
      <NavLink to="/" exact className="kata-logo">
        <img src={props.logo} alt="Kata.ai" />
      </NavLink>
    )}
    {props.children}
  </div>
);

export default Topbar;
