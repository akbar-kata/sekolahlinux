import React from 'react';

import './Content.scss';

interface Props {
  children: any;
}

export default (props: Props) => (
  <div className="kata-content">{props.children}</div>
);
