import React, { SFC } from 'react';
import classnames from 'classnames';

import './AppRoot.scss';

interface Props {
  logo?: string;
  className?: string;
}

const AppRoot: SFC<Props> = ({ children, className }) => (
  <div className={classnames('kata-root', className)}>{children}</div>
);

export default AppRoot;
