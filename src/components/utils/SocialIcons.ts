const socialIcons = {
  line: require('assets/images/socials/line.svg'),
  fbmessenger: require('assets/images/socials/fbmessenger.svg'),
  slack: require('assets/images/socials/slack.svg'),
  telegram: require('assets/images/socials/telegram.svg'),
  qiscus: require('assets/images/socials/qiscus.svg'),
  whatsapp: require('assets/images/socials/whatsapp.svg'),
  twitter: require('assets/images/socials/twitter.svg'),
  generic: require('assets/images/socials/generic.svg')
};

export default socialIcons;
