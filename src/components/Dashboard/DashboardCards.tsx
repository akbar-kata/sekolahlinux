import React from 'react';
import classnames from 'classnames';

interface Props {
  className?: string;
  centered?: boolean;
  cardsPerRow?: 2 | 3;
}

export default class DashboardCards extends React.Component<Props> {
  render() {
    const { centered, className, cardsPerRow = 3 } = this.props;
    return (
      <div
        className={classnames(
          'kata-dashboard__cards',
          `kata-dashboard__cards--columns-${cardsPerRow}`,
          className,
          centered ? 'kata-dashboard__cards--centered' : null
        )}
      >
        {React.Children.map(
          this.props.children,
          (Item: React.ReactNode) => Item
        )}
      </div>
    );
  }
}
