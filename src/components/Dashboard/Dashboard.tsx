import React, { Fragment } from 'react';
import classNames from 'classnames';

import './Dashboard.scss';
import { Tooltip, TooltipTarget } from 'components/Tooltip';

interface Props {
  title?: string;
  tooltip?: string;
  subTitle?: string;
  paragraph?: string;
  className?: string;
  isStarter?: boolean;
  isSettings?: boolean;
  headerContent?: any;
  image?: any;
  isHeadingCentered?: boolean;
}

export default class Dashboard extends React.Component<Props> {
  static defaultProps = {
    isStarter: false
  };

  renderStarter = () => {
    const {
      className,
      title,
      tooltip,
      subTitle,
      headerContent,
      image,
      children,
      isHeadingCentered
    } = this.props;
    return (
      <div className={classNames('kata-dashboard', className)}>
        <div className="kata-dashboard__heading kata-dashboard__heading--starter kata-dashboard--normalizer">
          <div className="kata-dashboard__container">
            <div className="kata-dashboard__heading-flex-inner">
              <div
                className={classNames({
                  'kata-dashboard__heading-content': true,
                  'kata-dashboard__heading-content__centered': isHeadingCentered
                })}
              >
                <div className="kata-dashboard__header">
                  <h1 className="kata-dashboard__title kata-dashboard__title--starter heading1">
                    {title}
                  </h1>
                  {tooltip && (
                    <TooltipTarget component={<Tooltip>{tooltip}</Tooltip>}>
                      <i className="icon-info kata-dashboard__tooltip" />
                    </TooltipTarget>
                  )}
                </div>
                {subTitle && (
                  <h2 className="kata-dashboard__subtitle title">{subTitle}</h2>
                )}
                {headerContent && (
                  <div className="kata-dashboard__header-content kata-dashboard__header-content--starter">
                    {headerContent}
                  </div>
                )}
              </div>
              {image && (
                <div className="kata-dashboard__heading-image">
                  <img src={image} alt="header-img" />
                </div>
              )}
            </div>
          </div>
        </div>

        <div className="kata-dashboard__content kata-dashboard__content--starter kata-dashboard--normalizer">
          <div className="kata-dashboard__container">{children}</div>
        </div>
      </div>
    );
  };

  render() {
    const {
      className,
      title,
      tooltip,
      subTitle,
      paragraph,
      headerContent,
      children,
      isStarter,
      isSettings
    } = this.props;

    if (isStarter) {
      return this.renderStarter();
    }

    return (
      <div className={classNames('kata-dashboard', className)}>
        <div
          className={classNames(
            'kata-dashboard__container',
            'kata-dashboard__container--flex',
            isSettings ? 'kata-dashboard__container--settings' : null
          )}
        >
          <div className="kata-dashboard__header">
            {headerContent || (
              <Fragment>
                <h1 className="kata-dashboard__title heading1">{title}</h1>
                {tooltip && (
                  <TooltipTarget
                    trigger="hover"
                    component={<Tooltip>{tooltip}</Tooltip>}
                  >
                    <i className="icon-info kata-dashboard__tooltip" />
                  </TooltipTarget>
                )}
              </Fragment>
            )}
          </div>
          {subTitle && (
            <h2 className="kata-dashboard__subtitle title">{subTitle}</h2>
          )}
          {paragraph && (
            <p className="kata-dashboard__paragraph body-text">{paragraph}</p>
          )}

          <div className="kata-dashboard__content mt-3">{children}</div>
        </div>
      </div>
    );
  }
}
