import React, { UIEventHandler } from 'react';
import classnames from 'classnames';

import './Board.scss';

interface Props {
  className?: string;
  bodyClassName?: string;
  onScroll?: UIEventHandler<HTMLDivElement>;
  headingChildren?: JSX.Element;
  footerChildren?: JSX.Element;
}

export default class Board extends React.Component<Props> {
  render() {
    const { className } = this.props;
    return (
      <div
        className={classnames('kata-board', className)}
        onScroll={this.props.onScroll}
      >
        {this.renderHeading()}
        <div
          className={classnames('kata-board__body', this.props.bodyClassName)}
        >
          {this.props.children}
        </div>
        {this.renderFooter()}
      </div>
    );
  }

  renderHeading() {
    if (this.props.headingChildren) {
      return (
        <div className="kata-board__heading">{this.props.headingChildren}</div>
      );
    }
    return null;
  }

  renderFooter() {
    if (this.props.footerChildren) {
      return (
        <div className="kata-board__footer">{this.props.footerChildren}</div>
      );
    }
    return null;
  }
}
