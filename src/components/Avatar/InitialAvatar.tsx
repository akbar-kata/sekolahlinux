import React from 'react';
import classnames from 'classnames';

import './Avatar.scss';

interface Props {
  initial: string;
  color?: string;
  className?: string;
  innerClassName?: string;
}

class InitialAvatar extends React.Component<Props> {
  static defaultProps = {
    color: 'success'
  };

  render() {
    const wrapperClasses = classnames(
      'kata-avatar',
      this.props.color,
      this.props.className
    );

    const innerWrapperClasses = classnames(
      'kata-avatar--initial',
      this.props.innerClassName
    );

    return (
      <div className={wrapperClasses}>
        <span className={innerWrapperClasses}>{this.props.initial}</span>
      </div>
    );
  }
}

export default InitialAvatar;
