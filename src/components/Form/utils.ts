export default {
  clone,
  get,
  set,
  mapValues,
  makePathArray,
  pickBy,
  isObject,
  isObjectEmpty,
  isArray
};

function clone(a: any) {
  try {
    return JSON.parse(
      JSON.stringify(a, (key, value) => {
        if (typeof value === 'function') {
          return value.toString();
        }
        return value;
      })
    );
  } catch (e) {
    return a;
  }
}

function get(obj: any, path: any, def?: any) {
  if (!path) {
    return obj;
  }
  const pathObj = makePathArray(path);
  let val;
  try {
    val = pathObj.reduce((current, pathPart) => current[pathPart], obj);
  } catch (e) {
    //
  }
  return typeof val !== 'undefined' ? val : def;
}

function set(obj: any = {}, path: any, value?: any) {
  const keys = makePathArray(path);
  let keyPart;
  let newObj = obj;
  if (isStringValidNumber(keys[0]) && !isArray(obj)) {
    newObj = [];
  }
  if (!isStringValidNumber(keys[0]) && !isObject(obj)) {
    newObj = {};
  }

  let cursor = newObj;
  // tslint:disable-next-line
  while ((keyPart = keys.shift()) && keys.length) {
    if (isStringValidNumber(keys[0]) && !isArray(cursor[keyPart])) {
      cursor[keyPart] = [];
    }
    if (!isStringValidNumber(keys[0]) && !isObject(cursor[keyPart])) {
      cursor[keyPart] = {};
    }
    cursor = cursor[keyPart];
  }
  cursor[keyPart] = assignValue(value);
  return newObj;
}

function assignValue(value: any) {
  if (
    value === '' ||
    value === null ||
    (isArray(value) && !value.length) ||
    (isObject(value) && isObjectEmpty(clone(value)))
  ) {
    return undefined;
  }

  return value;
}

function mapValues(obj: any, cb: any) {
  const newObj = {};
  Object.keys(obj).forEach(key => {
    newObj[key] = cb(obj[key], key);
  });
  return newObj;
}

function makePathArray(obj: any) {
  return flattenDeep(obj)
    .join('.')
    .replace('[', '.')
    .replace(']', '')
    .split('.');
}

function pickBy(obj: any, cb: any) {
  const newObj = {};
  for (const key in obj) {
    if (cb(obj[key], key)) {
      newObj[key] = obj[key];
    }
  }
  return newObj;
}

function flattenDeep(arr: any, newArr: any = []) {
  if (!isArray(arr)) {
    newArr.push(arr);
  } else {
    for (const i of arr) {
      flattenDeep(i, newArr);
    }
  }
  return newArr;
}

function isArray(a: any) {
  return Array.isArray(a);
}

function isObject(a: any) {
  return !Array.isArray(a) && typeof a === 'object' && a !== null;
}

function isObjectEmpty(obj: any) {
  for (const key in obj) {
    if (obj.hasOwnProperty(key)) {
      return false;
    }
  }
  return true;
}

function isStringValidNumber(str: any) {
  return !isNaN(str);
}
