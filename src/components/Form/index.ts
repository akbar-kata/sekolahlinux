/**
 * TODO: porting to typescript in correct way
 */
import Form, { FormDefaultProps } from './Form';
import FormField from './formField';
import FormError from './formError';
import FormInput from './formInput';
// Inputs
import Select from './inputs/select';
import ReactSelect from './inputs/reactSelect';
import Checkbox from './inputs/checkbox';
import ReactToggle from './inputs/reactToggle';
import Textarea from './inputs/textarea';
import NestedForm from './inputs/nestedForm';
import Text from './inputs/text';
import RadioGroup from './inputs/radioGroup';
import Radio from './inputs/radio';
import ReactTagsInput from './inputs/reactTagsInput';

// import Example from './example'; // TODO: this should be removed

export {
  Form,
  FormDefaultProps,
  FormField,
  FormError,
  FormInput,
  // Inputs
  Select,
  ReactSelect,
  Checkbox,
  ReactToggle,
  Textarea,
  NestedForm,
  Text,
  RadioGroup,
  Radio,
  ReactTagsInput

  // Example // TODO: this should be removed
};
