import React from 'react';
import shortid from 'shortid';
import AceEditor from 'react-ace';
import 'brace';

import 'brace/mode/javascript';
import 'brace/mode/json';
import 'brace/mode/yaml';
import 'brace/theme/tomorrow';

import FormInput from '../formInput';

const beautify = require('js-beautify').js_beautify;

interface Props {
  mode: 'javascript' | 'json' | 'yaml';
  width?: number;
  height?: number;
  autoFormat?: boolean;
  [key: string]: any;
}

interface State {
  elId: string;
  style: {
    maxWidth: number;
    border: string;
  };
}

class FormInputReactAce extends React.Component<Props, State> {
  constructor(props: any) {
    super(props);
    this.state = {
      elId: shortid.generate(),
      style: {
        maxWidth: 500,
        border: '1px solid #ddd'
      }
    };
  }

  componentDidMount() {
    const codeEl = document.getElementById(this.state.elId);
    this.setState({
      style: {
        maxWidth:
          codeEl && codeEl.parentElement
            ? codeEl.parentElement.clientWidth
            : 500,
        border: '1px solid #ddd'
      }
    });
  }

  render() {
    const {
      field,
      mode,
      height,
      width,
      autoFormat,
      showErrors,
      errorBefore,
      onChange,
      onBlur,
      isForm,
      noTouch,
      errorProps,
      placeholder,
      ...rest
    } = this.props;
    return (
      <FormInput
        field={field}
        showErrors={showErrors}
        errorBefore={errorBefore}
        isForm={isForm}
        errorProps={errorProps}
      >
        {({ setValue, getValue, setTouched }) => {
          const editorValue = getValue();
          return (
            <div id={this.state.elId} style={this.state.style}>
              <AceEditor
                mode={mode}
                theme="tomorrow"
                value={editorValue}
                onChange={(value: any) => setValue(value)}
                onBlur={(value: any) =>
                  setValue(
                    autoFormat && editorValue
                      ? beautify(editorValue, {
                          indent_size: 2
                        })
                      : editorValue
                  )
                }
                name={this.state.elId}
                editorProps={{ $blockScrolling: true }}
                width={`${width || this.state.style.maxWidth - 5}px`}
                height={`${height || 300}px`}
                {...rest}
              />
            </div>
          );
        }}
      </FormInput>
    );
  }
}

export default FormInputReactAce;
