import React from 'react';
//
import FormInput from '../formInput';
import _ from '../utils';

export default function FormInputNestedForm({
  field,
  children,
  errorProps,
  ...rest
}: any) {
  return (
    <FormInput field={field} errorBefore isForm errorProps={errorProps}>
      {({ setValue, getValue, getTouched, setNestedError }) => {
        let newChildren = children;
        if (Array.isArray(children)) {
          // console.warn(
          //   "NestedForm's only child must be a single ReactForm component. Using the first child of:",
          //   children,
          // )
          newChildren = children[0];
        }
        return React.cloneElement(newChildren, {
          ...rest,
          /* Let the parent form set defaultValues */
          values: getValue(),
          /* Respond to the parent form's dirty submission attempts */
          touched: getTouched(),
          /* Notify the parent of any nestedForm-level errors and values */
          onChange: ({ values, errors }, props, initial) => {
            !_.isObjectEmpty(errors)
              ? setNestedError(true)
              : setNestedError(false);
            setValue(values, initial);
          }
        });
      }}
    </FormInput>
  );
}
