import React from 'react';

import FormInput from '../formInput';

const Select = require('react-select').default;

export default function FormInputReactSelect({
  field,
  async,
  showErrors,
  errorBefore,
  onChange,
  onBlur,
  isForm,
  noTouch,
  errorProps,
  placeholder,
  ...rest
}: any) {
  const Component = async ? Select.Async : Select;
  return (
    <FormInput
      field={field}
      showErrors={showErrors}
      errorBefore={errorBefore}
      isForm={isForm}
      errorProps={errorProps}
    >
      {({ setValue, getValue, setTouched }) => {
        return (
          <Component
            {...rest} // Send the rest of your props to React-Select
            value={getValue()} // Set the value to the forms value
            onChange={val => {
              if (onChange) {
                onChange(val);
              }
              setValue(val);
            }} // On Change, update the form value
            onBlur={() => {
              if (onBlur) {
                onBlur();
              }
              setTouched();
            }} // And the same goes for touched
            className="kata-form__input-select"
            clearRenderer={() => {
              return (
                <i className="icon-remove kata-form__input-select--icon" />
              );
            }}
            clearable
          />
        );
      }}
    </FormInput>
  );
}
