import React from 'react';
import DateTime from 'react-datetime';

import 'react-datetime/css/react-datetime.css';

import FormInput from '../formInput';

export default function FormInputReactSelect({
  field,
  showErrors,
  errorBefore,
  onChange,
  onBlur,
  isForm,
  noTouch,
  errorProps,
  placeholder,
  ...rest
}: any) {
  return (
    <FormInput
      field={field}
      showErrors={showErrors}
      errorBefore={errorBefore}
      isForm={isForm}
      errorProps={errorProps}
    >
      {({ setValue, getValue, setTouched }) => {
        return (
          <DateTime
            {...rest} // Send the rest of your props to React-Select
            value={getValue()} // Set the value to the forms value
            onChange={val => {
              if (onChange) {
                onChange(val);
              }
              setValue(val);
            }} // On Change, update the form value
            onBlur={() => {
              if (onBlur) {
                onBlur();
              }
              setTouched();
            }} // And the same goes for touched
          />
        );
      }}
    </FormInput>
  );
}
