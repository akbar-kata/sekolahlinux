import React from 'react';
import classnames from 'classnames';
import TagsInput from 'react-tagsinput';

import FormInput from '../formInput';

export default function FormInputReactTagsInput({
  field,
  showErrors,
  errorBefore,
  className,
  onChange,
  onBlur,
  isForm,
  noTouch,
  errorProps,
  placeholder,
  disabled,
  ...rest
}: any) {
  return (
    <FormInput
      field={field}
      showErrors={showErrors}
      errorBefore={errorBefore}
      isForm={isForm}
      errorProps={errorProps}
    >
      {({ setValue, getValue, setTouched }) => {
        const classes = classnames(
          'react-tagsinput',
          'kata-tagsinput',
          className
        );
        return (
          <div>
            <TagsInput
              {...rest} // Send the rest of your props to React-Select
              value={getValue() || []} // Set the value to the forms value
              focusedClassName="react-tagsinput--focused"
              onChange={val => {
                if (onChange) {
                  onChange(val);
                }
                setValue(val);
              }} // On Change, update the form value
              onBlur={() => {
                if (onBlur) {
                  onBlur();
                }
                setTouched();
              }} // And the same goes for touched
              className={classes}
              disabled={!!disabled}
              addKeys={[9, 13, 188]}
            />
            {!disabled ? (
              <small className="kata-form__tag-label">
                You can input mutiple labels separated by comma or tab
              </small>
            ) : null}
          </div>
        );
      }}
    </FormInput>
  );
}
