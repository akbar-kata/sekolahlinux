import React from 'react';
//
import { buildHandler } from './util';
import FormInput from '../formInput';

export default function FormInputCheckbox({
  field,
  label,
  inline,
  noFalse,
  showErrors,
  errorBefore,
  onChange,
  onBlur,
  isForm,
  noTouch,
  errorProps,
  ...rest
}: any) {
  return (
    <FormInput
      field={field}
      showErrors={showErrors}
      errorBefore={errorBefore}
      isForm={isForm}
      errorProps={errorProps}
      className={!!inline ? 'FormInput-inline' : ''}
    >
      {({ setValue, getValue, setTouched }) => {
        return (
          <label>
            <input
              {...rest}
              type="checkbox"
              checked={getValue()}
              onChange={buildHandler(onChange, e =>
                setValue(
                  noFalse
                    ? !e.target.checked
                      ? undefined
                      : true
                    : e.target.checked,
                  noTouch
                )
              )}
              onBlur={buildHandler(onBlur, () => setTouched())}
            />
            {!!label && <span className="ml2">{label}</span>}
          </label>
        );
      }}
    </FormInput>
  );
}
