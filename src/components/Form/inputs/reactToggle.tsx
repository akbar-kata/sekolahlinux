import React from 'react';
import Toggle from 'react-toggle';

import { buildHandler } from './util';
import FormInput from '../formInput';

export default function FormInputCheckbox({
  field,
  label,
  inline,
  noFalse,
  showErrors,
  errorBefore,
  onChange,
  onBlur,
  isForm,
  noTouch,
  errorProps,
  innerClassName,
  ...rest
}: any) {
  return (
    <FormInput
      field={field}
      showErrors={showErrors}
      errorBefore={errorBefore}
      isForm={isForm}
      errorProps={errorProps}
      className={!!inline ? 'FormInput-inline' : ''}
    >
      {({ setValue, getValue, setTouched }) => {
        return (
          <label className={innerClassName}>
            <Toggle
              {...rest}
              checked={!!getValue()}
              onChange={buildHandler(onChange, e =>
                setValue(
                  noFalse
                    ? !e.target.checked
                      ? undefined
                      : true
                    : e.target.checked,
                  noTouch
                )
              )}
              onBlur={buildHandler(onBlur, () => setTouched())}
            />
            {!!label && <span className="ml-1">{label}</span>}
          </label>
        );
      }}
    </FormInput>
  );
}
