import React from 'react';
//
import { buildHandler } from './util';
import FormInput from '../formInput';

export default function FormInputText({
  prependElement,
  appendElement,
  field,
  showErrors,
  errorBefore,
  onChange,
  onBlur,
  isForm,
  noTouch,
  errorProps,
  ...rest
}: any) {
  return (
    <FormInput
      field={field}
      showErrors={showErrors}
      errorBefore={errorBefore}
      isForm={isForm}
      errorProps={errorProps}
    >
      {({ setValue, getValue, setTouched }) => {
        const input = (
          <input
            {...rest}
            value={getValue('')}
            onChange={e => {
              if (onChange) {
                onChange(e);
              }
              setValue(e.target.value);
            }}
            onBlur={buildHandler(onBlur, () => setTouched())}
          />
        );
        if (prependElement || appendElement) {
          return (
            <div className="input-group">
              {prependElement && (
                <div className="input-group-prepend">{prependElement}</div>
              )}
              {input}
              {appendElement && (
                <div className="input-group-append">{appendElement}</div>
              )}
            </div>
          );
        }
        return input;
      }}
    </FormInput>
  );
}
