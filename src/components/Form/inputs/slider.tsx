import React from 'react';
import Slider from 'react-rangeslider';

import 'assets/sass/vendor/react-rangeslider.scss';

interface Props {
  min?: number;
  max?: number;
  step?: number;
  value: number;
}

interface States {
  value: number;
  min?: number;
  max?: number;
  step?: number;
}

export default class FormInputSlider extends React.Component<Props, States> {
  constructor(props: Props) {
    super(props);
    this.state = {
      value: props.value,
      min: props.min,
      max: props.max,
      step: props.step
    };
  }

  static getDerivedStateFromProps(nextProps: Props, prevState: Props) {
    if (nextProps) {
      return {
        min: nextProps.min,
        max: nextProps.max,
        value: nextProps.value,
        step: nextProps.step
      };
    }
    return null;
  }

  handleChange = value => {
    this.setState({
      value
    });
  };

  render() {
    const { value, min, max, step } = this.state;
    const valueLabels = {
      0: min,
      100: max
    };
    return (
      <Slider
        min={min}
        max={max}
        step={step}
        labels={valueLabels}
        value={value}
        handleLabel={value}
        onChange={this.handleChange}
        tooltip={false}
      />
    );
  }
}
