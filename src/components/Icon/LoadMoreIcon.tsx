import React from 'react';

interface LoadMoreProps {
  className?: string;
  color?: string;
}

const LoadMoreIcon: React.SFC<LoadMoreProps> = ({ className, color }) => (
  <svg
    className={className}
    xmlns="http://www.w3.org/2000/svg"
    width="16"
    height="12"
    viewBox="0 0 16 12"
  >
    <g fill={color || '#006fe6'} fillRule="evenodd">
      {/* tslint:disable-next-line:max-line-length */}
      <path d="M9.557 9.684A3.99 3.99 0 0 1 8 10c-2.048 0-3.722-1.553-3.954-3.54l.248.247a.997.997 0 0 0 1.414 0 .999.999 0 0 0 0-1.414l-2-2a1 1 0 0 0-1.414 0l-2 2a1.001 1.001 0 0 0 1.413 1.414l.313-.313C2.224 9.52 4.825 12 8 12c.806 0 1.592-.159 2.335-.473.51-.216.747-.802.533-1.31a1.003 1.003 0 0 0-1.31-.533M15.708 5.293a1.001 1.001 0 0 0-1.415 0l-.313.312C13.776 2.481 11.176 0 8 0c-.721 0-1.428.127-2.101.377a1 1 0 0 0 .697 1.874A4.039 4.039 0 0 1 8 2c2.048 0 3.722 1.553 3.954 3.539l-.246-.246a1.001 1.001 0 0 0-1.415 1.414l1.999 2a.998.998 0 0 0 1.415.001l2-2c.39-.391.39-1.024 0-1.415" />
    </g>
  </svg>
);

export default LoadMoreIcon;
