import { FormApi } from './Form';
import React from 'react';
import { ValidationLogic } from './Validators';

interface Props {
  form: FormApi;
  name: string;
  validations?: ValidationLogic[];
}

/**
 * This is the base class of all form based elements, it doesn't have to be an
 * input. But it won't make sense because it handle data management and validation
 * management to the forms.
 */
export default abstract class FormElement<V, P, S> extends React.Component<
  Props & P,
  S
> {
  /**
   * Retrieve the data from the form data store.
   * @param {V | null} defaultValue
   * @returns {V}
   */
  getData(defaultValue: V | null): V {
    return this.props.form.getData(this.props.name, defaultValue);
  }

  /**
   * Set the data for the form data store.
   * @param {V} value
   */
  setData(value: V) {
    this.props.form.setData(this.props.name, value);
  }

  /**
   * Get the validation results, even though this class is responsible for
   * validation, the result management is handled by the form as the source of truth.
   * @returns {any}
   */
  getValidationResult() {
    return this.props.form.getValidation(this.props.name);
  }

  /**
   * A helper method that check for the validation results whether its undefined
   * or null.
   * @returns {boolean}
   */
  isValid() {
    const validationResult = this.getValidationResult();
    return validationResult === undefined || validationResult === null;
  }

  /**
   * Run the validation logic, you can override this if you need more
   * control for the validation.
   */
  validate() {
    const validationResult = {};
    let valid = true;
    if (this.props.validations) {
      this.props.validations.forEach(validator => {
        const result = validator(this.getData(null));
        if (!result.valid) {
          validationResult[result.key] = result.message;
          valid = false;
        }
      });
    }

    this.props.form.setValidation(
      this.props.name,
      valid ? null : validationResult
    );
  }

  /**
   * Render the validation error results, you can override this
   * if you need more control on how to display the validation errors.
   * @returns {any}
   */
  renderValidationErrors() {
    const validationResult = this.getValidationResult();
    if (!this.isValid()) {
      return Object.keys(validationResult).map(key => (
        <label
          key={key}
          htmlFor={this.props.name}
          className="kata-form__error-label"
        >
          {validationResult[key]}
        </label>
      ));
    }
    return null;
  }
}
