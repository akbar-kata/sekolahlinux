import FormElement from './FormElement';
import React from 'react';

import classNames from 'classnames';

export type SelectOption = {
  label: string;
  value: any;
};

interface Props {
  label: string;
  options: SelectOption[];
}

interface States {
  value: string;
}

export default class FormSelect extends FormElement<string, Props, States> {
  state = {
    value: this.getData(''),
    validationResult: null
  };

  inputValue = (event: React.FormEvent<HTMLSelectElement>) => {
    console.dir(event.currentTarget.value);
    this.setState(
      {
        value: event.currentTarget.value
      },
      () => this.setData(this.state.value)
    );
  };

  renderOptions() {
    return this.props.options.map((option, index) => (
      <option value={option.value} key={`${index}_${option.value}`}>
        {option.label}
      </option>
    ));
  }

  componentWillReceiveProps(nextProps: Props) {
    if (nextProps.options.length !== this.props.options.length) {
      this.setData(nextProps.options[0].value);
    }
  }

  render() {
    return (
      <div className="kata-form__element">
        <label htmlFor={this.props.name} className="kata-form__label">
          {this.props.label}
        </label>
        <select
          className={classNames({
            'kata-form__input-select': true,
            'kata-form__input-select--error': !this.isValid()
          })}
          name={this.props.name}
          title={this.props.label}
          onInput={this.inputValue}
        >
          {this.renderOptions()}
        </select>
        {this.renderValidationErrors()}
      </div>
    );
  }

  runValidation = () => {
    this.validate();
  };
}
