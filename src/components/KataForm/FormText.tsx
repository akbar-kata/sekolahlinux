import FormElement from './FormElement';
import React from 'react';

import classNames from 'classnames';

interface Props {
  type: string;
  label: string;
}

interface States {
  value: string;
}

export default class FormText extends FormElement<string, Props, States> {
  state = {
    value: this.getData(''),
    validationResult: null
  };

  inputValue = (event: React.FormEvent<HTMLInputElement>) => {
    this.setState(
      {
        value: event.currentTarget.value
      },
      () => this.setData(this.state.value)
    );
  };

  render() {
    return (
      <div className="kata-form__element">
        <label htmlFor={this.props.name} className="kata-form__label">
          {this.props.label}
        </label>
        <input
          className={classNames({
            'kata-form__input-text': true,
            'kata-form__input-text--error': !this.isValid()
          })}
          type="text"
          placeholder={this.props.label}
          name={this.props.name}
          title={this.props.label}
          onInput={this.inputValue}
          onBlur={this.runValidation}
          value={this.state.value}
        />
        {this.renderValidationErrors()}
      </div>
    );
  }

  runValidation = () => {
    this.validate();
  };
}
