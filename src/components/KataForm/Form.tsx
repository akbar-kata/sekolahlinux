import React, { FormEvent } from 'react';

interface Props {
  action?: string;
  encType?: string;
  method?: string;
  onSubmit?: FormSubmitHandler;
  elements: (form: FormApi) => React.ReactElement<any>;
}

interface States {
  data: FormStore;
  validation: FormStore;
}

export type FormStore = {
  [key: string]: any;
};
export type FormSubmitHandler = (
  data: FormStore,
  validation: FormStore,
  event: FormEvent<any>
) => void;

/**
 * All of these Api is passed to the FormElement
 */
export type SetApi = (key: string, value: any) => void;
export type GetApi = (key: string, defaultValue: any) => any;
export type SetValidationApi = (key: string, value: any) => void;
export type GetValidationApi = (key: string) => any;
export type FormApi = {
  setData: SetApi;
  getData: GetApi;
  setValidation: SetValidationApi;
  getValidation: GetValidationApi;
};

export default class Form extends React.Component<Props, States> {
  state = {
    data: {},
    validation: {}
  };

  setData = (key: string, value: any) => {
    this.setState({
      data: { ...this.state.data, [key]: value }
    });
  };

  getData = (key: string, defaultValue: any) => {
    return this.state.data[key] || defaultValue;
  };

  setValidation = (key: string, value: any) => {
    this.setState({
      validation: { ...this.state.validation, [key]: value }
    });
  };

  getValidation = (key: string) => {
    return this.state.validation[key];
  };

  triggerOnSubmit = (event: FormEvent<any>) => {
    if (this.props.onSubmit) {
      this.props.onSubmit(
        { ...this.state.data },
        { ...this.state.validation },
        event
      );
    }
  };

  renderElements() {
    return this.props.elements({
      setData: this.setData,
      getData: this.getData,
      setValidation: this.setValidation,
      getValidation: this.getValidation
    });
  }

  render() {
    return (
      <form
        encType={this.props.encType}
        method={this.props.method}
        action={this.props.action}
        onSubmit={this.triggerOnSubmit}
      >
        {this.renderElements()}
      </form>
    );
  }
}
