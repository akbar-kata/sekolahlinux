import required from './required';

export type ValidationResult = {
  valid: boolean;
  message: string;
  key: string;
};

export type ValidationLogic = (value: any) => ValidationResult;

export { required };
