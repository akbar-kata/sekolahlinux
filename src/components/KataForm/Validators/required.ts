export default function required(message: string) {
  return function(value: any) {
    const valid =
      value !== undefined && value !== null && value.toString().trim() !== '';
    return {
      valid,
      message,
      key: 'required'
    };
  };
}
