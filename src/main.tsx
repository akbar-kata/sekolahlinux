import React from 'react';
import { Provider } from 'react-redux';
import { Switch, Route, Redirect } from 'react-router-dom';
import { ConnectedRouter } from 'connected-react-router';
import NotificationsSystem from 'reapop';

import reapopTheme from 'components/ReapopTheme';
import { Robot } from 'components/Loading';

import ProtectedRoute from './modules/auth/Route';
import Auth from './modules/auth';
import AuthChecker from './modules/auth/AuthChecker';
import App from './modules/core/App.Container';
import Analytics from './modules/core/Analytics';
import Maintenance from './modules/maintenance';
import Layout from './Layout';

import * as env from 'utils/env';

interface MainProps {
  store: any;
  history: any;
}

export const Main: React.FC<MainProps> = ({ store, history }) => {
  if (env.getRuntimeEnv('REACT_APP_RUNTIME_MAINTENANCE') === 'true') {
    return (
      <Provider store={store}>
        <ConnectedRouter history={history}>
          <React.Fragment>
            <Switch>
              <Route path="/" component={Maintenance} />
            </Switch>
          </React.Fragment>
        </ConnectedRouter>
      </Provider>
    );
  }
  return (
    <Provider store={store}>
      <ConnectedRouter history={history}>
        <React.Suspense fallback={<Robot />}>
          <NotificationsSystem theme={reapopTheme} />
          <AuthChecker />
          <Switch>
            <Route path="/(login|forgot|change-password)" component={Auth} />
            {process.env.NODE_ENV !== 'production' && (
              <Route path="/layout" component={Layout} />
            )}
            <ProtectedRoute path="/" component={Analytics(App)} />
            <Route render={() => <Redirect to="/login" />} />
          </Switch>
        </React.Suspense>
      </ConnectedRouter>
    </Provider>
  );
};
