export function mapPredictToTraining(prediction: any) {
  const result: any[] = [];
  let id = 1;
  const pred = prediction.result[0].output;
  let entity: any = [];
  for (const i in pred) {
    if (pred) {
      for (entity of pred[i]) {
        // tslint:disable-next-line:no-increment-decrement
        entity.id = id++;
        entity.entity = i;
        if (entity.belongsTo && pred[entity.belongsTo.name]) {
          const belongsTo = pred[entity.belongsTo.name][entity.belongsTo.index];
          if (belongsTo) {
            if (!belongsTo.id) {
              // tslint:disable-next-line:no-increment-decrement
              belongsTo.id = id++;
              entity.belongsTo.id = belongsTo.id;
              entity.belongsTo.entity = entity.belongsTo.name;
              delete entity.belongsTo.name;
              delete entity.belongsTo.index;
            }
          } else {
            delete entity.belongsTo;
          }
        }
        result.push(entity);
      }
    }
  }

  return {
    result: [
      {
        input: prediction.result[0].input,
        entities: result
      }
    ]
  };
}
