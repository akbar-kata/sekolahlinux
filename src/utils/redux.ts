import { Action, AnyAction, Reducer } from 'redux';

/**
 * This function checks if the passed object has the type of never.
 * Useful for checking whether our reducers handled all available actions or not.
 * @param tobe Type to check to be never
 */
export function assertNever(tobe: never) {
  return undefined;
}

/**
 * Defines reducers that have no available actions yet.
 *
 * @param state Reducer state
 * @param action Reducer actions
 */
export function noopReducer<S = any, A extends Action = AnyAction>(
  initialState: S
): Reducer<S, A> {
  return (state = initialState, _) => state;
}
