import shortid from 'shortid';

export function extractPrediction(input: string): any {
  let input2 = '';

  const regEnt = new RegExp(/(\(\@[^\)]+\))/, 'g');
  const tokens = input.split(regEnt);
  const esd: any = [];
  let prev = 0;

  for (let i = 1; i < tokens.length; i += 2) {
    input2 += tokens[i - 1];
    prev += tokens[i - 1].length;

    const edesc = getEntityDetail(tokens[i]);
    edesc.start = prev;
    prev += edesc.value.length;
    input2 += edesc.value;
    edesc.end = prev;
    esd.push(edesc);
  }
  input2 += tokens[tokens.length - 1];

  // intents
  const regInt = new RegExp(/(#\w+:\w+)/, 'g');
  const tokens2 = input2.split(regInt);
  const raw = tokens2[0].trim();
  for (let i = 1; i < tokens2.length; i += 2) {
    const e: any = {};
    if (tokens2[i].match(/#(\w+):(\w+)/)) {
      e.entity = RegExp.$1;
      e.id = shortid.generate();
      e.label = RegExp.$2;
      e.value = e.label;
      e.start = 0;
      e.end = raw.length;
      e.type = 'trait';
      esd.push(e);
    }
  }

  return esd;
}

function getEntityDetail(str: string): any {
  const result: any = {};

  if (str.match(/^\(@(\w+)(?:-(\d+))?(?:\.(\w+))?(?:\:(\w+))?\s+(.*)\)$/)) {
    if (RegExp.$3) {
      result.belongsTo = {
        entity: RegExp.$1,
        id: RegExp.$2
      };
      result.entity = RegExp.$3;
      result.id = shortid.generate();
    } else {
      result.entity = RegExp.$1;
      result.id = RegExp.$2 || shortid.generate();
    }

    if (RegExp.$4) {
      result.label = RegExp.$4;
    }

    result.value = RegExp.$5;
  }

  return result;
}

function findEntityIndex(arr: any[], { entity, start, end }: any) {
  return (
    arr
      .filter(item => item.entity === entity)
      .sort((a, b) => {
        if (!a) {
          return 1;
        }
        if (!b) {
          return -1;
        }
        return a.start - b.start;
      })
      .findIndex(item => item.start === start && item.end === end) || 0
  );
}

export function prepareTrain(
  sentence: string,
  entities: any[],
  traits: any[]
): string {
  let newSentece = sentence;
  const formatted: any[] = sentence.split('');
  let offset = 0;
  if (entities && entities.length) {
    entities
      .sort((a, b) => {
        if (!a) {
          return 1;
        }
        if (!b) {
          return -1;
        }
        return a.start - b.start;
      })
      .forEach(tag => {
        if (!tag) {
          return;
        }
        const start = tag.start - offset;
        const end = tag.end - tag.start;
        let el = '';
        if (tag.belongsTo) {
          el = `(@${tag.belongsTo.name}-${tag.belongsTo.index}.${
            tag.entity
          } ${sentence.substr(tag.start, end)})`;
        } else if (tag.label) {
          el = `(@${tag.entity}-${findEntityIndex(
            entities,
            tag
          )}:${tag.label.toLowerCase()} ${sentence.substr(tag.start, end)})`;
        } else {
          el = `(@${tag.entity}-${findEntityIndex(
            entities,
            tag
          )} ${sentence.substr(tag.start, end)})`;
        }
        formatted.splice(start, end, el);
        offset += tag.end - tag.start - 1;
      });
    newSentece = formatted.join('');
  }
  if (traits && traits.length) {
    traits.forEach(tag => {
      newSentece += ` #${tag.entity}${tag.value ? `:${tag.value}` : ''}`;
    });
  }
  return newSentece;
}
