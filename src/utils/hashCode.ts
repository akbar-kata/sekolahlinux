/**
 * Implementation of Java's `String#hashCode`
 *
 * @param str string, etc. to generate to hashCode.
 */
export default function hashCode(str: any) {
  let hash = 0;
  // tslint:disable-next-line:no-increment-decrement
  for (let i = 0; i < str.length; i++) {
    // tslint:disable-next-line:no-bitwise
    hash = str.charCodeAt(i) + ((hash << 5) - hash);
  }
  return hash;
}
