/**
 * Get the incremented version number according to Semantic Versioning.
 * Extracted as a utility function for reuse in multiple modules.
 *
 * @param version Version number in string.
 * @param type The type of the patch, `major`, `minor`, or `patch`.
 */
export default function renderIncrementVersion(
  version: string,
  type: 'major' | 'minor' | 'patch' = 'patch'
): string {
  const [major = 0, minor = 0, patch = 0] = (version || '0.0.0').split('.');

  // When the major/minor version is incremented, all smaller version
  // numbers will be rolled back to 0.
  if (type === 'major') {
    return `${Number(major) + 1}.${0}.${0}`;
  }

  if (type === 'minor') {
    return `${major}.${Number(minor) + 1}.${0}`;
  }

  return `${major}.${minor}.${Number(patch) + 1}`;
}
