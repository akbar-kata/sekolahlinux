import sha256 from 'sha256';
import hashCode from './hashCode';

export function hexGenerator(entity: any) {
  const color = sha256(entity)
    .toString()
    .substr(0, 10);
  const hex = intToRGB(hashCode(color));
  const rgb = hexToRgb(`#${hex}`);
  const background = `background :rgba(${rgb ? rgb.r : rgb}, ${
    rgb ? rgb.g : rgb
  }, ${rgb ? rgb.b : rgb}, 0.3);`;
  return background;
}

export function colorGenerator(value: string) {
  const color = sha256(value)
    .toString()
    .substr(0, 10);
  const hex = intToRGB(hashCode(color));
  const rgb = hexToRgb(`#${hex}`);
  if (rgb) {
    return `rgba(${rgb.r}, ${rgb.g}, ${rgb.b}, 0.3)`;
  }
  return `rgba(3, 3, 3, 0.3)`;
}

export function highLight(entity: any) {
  return 'color :blue; padding: 4px;';
}

function hexToRgb(hex: any) {
  const shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
  const newHex = hex.replace(shorthandRegex, (m, r, g, b) => {
    return r + r + g + g + b + b;
  });

  const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(newHex);
  return result
    ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
      }
    : null;
}

function intToRGB(i: any) {
  // tslint:disable-next-line:no-bitwise
  const c = (i & 0x00ffffff).toString(16).toUpperCase();

  return '00000'.substring(0, 6 - c.length) + c;
}
