export const ROOTS = [
  'bot',
  'nlstudio',
  'cms',
  'analytic',
  'setting/account',
  'setting/teams',
  'project'
];

export const isRootURL = (path: string, roots: string[] = ROOTS) => {
  if (!path.length || !roots.length) {
    return false;
  }
  const regex = new RegExp(`^/(${roots.join('|')})`);
  return path.search(regex) !== -1 && path.replace(regex, '').length <= 1;
};
