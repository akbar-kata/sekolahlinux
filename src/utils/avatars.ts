import sha256 from 'sha256';
import hashCode from './hashCode';

const avatars = {
  user: [
    require('assets/images/avatars/user/user-1.png'),
    require('assets/images/avatars/user/user-2.png'),
    require('assets/images/avatars/user/user-3.png'),
    require('assets/images/avatars/user/user-4.png'),
    require('assets/images/avatars/user/user-5.png'),
    require('assets/images/avatars/user/user-6.png'),
    require('assets/images/avatars/user/user-7.png'),
    require('assets/images/avatars/user/user-8.png'),
    require('assets/images/avatars/user/user-9.png')
  ],
  team: [
    require('assets/images/avatars/team/team-1.png'),
    require('assets/images/avatars/team/team-2.png'),
    require('assets/images/avatars/team/team-3.png'),
    require('assets/images/avatars/team/team-4.png'),
    require('assets/images/avatars/team/team-5.png'),
    require('assets/images/avatars/team/team-6.png'),
    require('assets/images/avatars/team/team-7.png'),
    require('assets/images/avatars/team/team-8.png'),
    require('assets/images/avatars/team/team-9.png')
  ],
  project: [
    require('assets/images/avatars/project/bot-01.png'),
    require('assets/images/avatars/project/bot-02.png'),
    require('assets/images/avatars/project/bot-03.png'),
    require('assets/images/avatars/project/bot-04.png'),
    require('assets/images/avatars/project/bot-05.png'),
    require('assets/images/avatars/project/bot-06.png'),
    require('assets/images/avatars/project/bot-07.png'),
    require('assets/images/avatars/project/bot-08.png'),
    require('assets/images/avatars/project/bot-09.png'),
    require('assets/images/avatars/project/bot-10.png'),
    require('assets/images/avatars/project/bot-11.png'),
    require('assets/images/avatars/project/bot-12.png'),
    require('assets/images/avatars/project/bot-13.png'),
    require('assets/images/avatars/project/bot-14.png'),
    require('assets/images/avatars/project/bot-15.png'),
    require('assets/images/avatars/project/bot-16.png'),
    require('assets/images/avatars/project/bot-17.png'),
    require('assets/images/avatars/project/bot-18.png'),
    require('assets/images/avatars/project/bot-19.png'),
    require('assets/images/avatars/project/bot-20.png')
  ]
};

/**
 * Generates a random Avatar based on seed.
 *
 * @param seed The seed. Could be an ID, name, whatever.
 * @param type The avatar type. Defaults to `bot`
 */
export function randomizeAvatarWithSeed(
  seed: any,
  type: 'user' | 'team' | 'project' = 'project'
) {
  const avatarSeed = sha256(seed)
    .toString()
    .substr(0, 10);
  const index = hashCode(avatarSeed);

  return avatars[type][Math.floor(Math.abs(index % avatars[type].length))];
}
