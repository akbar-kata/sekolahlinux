import get from 'lodash-es/get';

/**
 * Loads environment variable set at runtime.
 *
 * @param name key of environment variable to load
 * @param defaultValue fallback value
 */
export function getRuntimeEnv(name: string, defaultValue?: string) {
  return get(
    window,
    ['__KATA_RUNTIME__', name],
    defaultValue || defaultEnvs[name] || undefined
  );
}

/**
 * Loads build-time environment variables through `process.env`.
 *
 * @param name key of environment variable to load
 * @param defaultValue fallback value
 */
export function getReactEnv(name: string, defaultValue?: string) {
  return get(process.env, name, defaultValue || defaultEnvs[name] || undefined);
}

/**
 * Fallback environment variable.
 */
export const defaultEnvs = {
  REACT_APP_RUNTIME_WITH_CMS: 'true',
  REACT_APP_RUNTIME_CMS_NEW_ELEMENTS: 'true',
  REACT_APP_RUNTIME_ZAUN_URL: 'http://zaun.katalabs.io',
  REACT_APP_RUNTIME_EMPFANG_URL: 'https://user.katalabs.io',
  REACT_APP_RUNTIME_DOCS_URL: 'https://docs.kata.ai',
  REACT_APP_RUNTIME_BOTSTUDIO_TUTORIAL_URL:
    'https://docs.kata.ai/tutorial/bot-studio/',
  REACT_APP_RUNTIME_NLSTUDIO_TUTORIAL_URL:
    'https://docs.kata.ai/tutorial/nl-studio/',
  REACT_APP_RUNTIME_NEWS_30_CAST: 'true',
  REACT_APP_RUNTIME_NEWS_25_CAST: 'true',
  REACT_APP_RUNTIME_NEWS_25_BAR: 'false',
  REACT_APP_RUNTIME_NEWS_URL: 'https://docs.kata.ai/overview/release-notes/',
  REACT_APP_RUNTIME_SURVEY_25_CAST: 'false',
  REACT_APP_RUNTIME_MAINTENANCE: 'false',
  REACT_APP_RUNTIME_SENTRY_DSN:
    'https://65d0517b695943ba91c70a76319377db@sentry.io/1342454',
  REACT_APP_RUNTIME_SURVEY_URL:
    'https://docs.google.com/forms/d/e/1FAIpQLScNAovi259qYUU786A0qu5bZ6D2SCQuAdqCmhEbdCdZruggkg/viewform',
  REACT_APP_RUNTIME_GA_TRACKING_CODE: 'UA-108806080-3'
};
