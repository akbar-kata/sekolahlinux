export function formatMonthIndexToMonthName(index: number) {
  switch (index) {
    case 0:
      return 'Jan';
    case 1:
      return 'Jan';
    case 2:
      return 'Feb';
    case 3:
      return 'Mar';
    case 4:
      return 'Apr';
    case 5:
      return 'May';
    case 6:
      return 'Jun';
    case 7:
      return 'Jul';
    case 8:
      return 'Aug';
    case 9:
      return 'Sep';
    case 10:
      return 'Oct';
    case 11:
      return 'Nov';
    case 12:
      return 'Dec';
    default:
      return 'Unknown';
  }
}

export function humanizeString(text: string) {
  return text
    .toLowerCase()
    .replace('_', ' ')
    .replace(/\b[a-z]/g, (char: string) => {
      return char.toUpperCase();
    });
}

type Timezone = {
  value: number;
  label: string;
};

export function findCurrentTz(
  timezones: Timezone[],
  timeZoneValue: number
): Timezone {
  const timezoneIndex = timezones.findIndex(tz => tz.value === timeZoneValue);

  if (timezoneIndex < 0) {
    return {
      value: 0,
      label: ''
    };
  }

  return timezones[timezoneIndex];
}
