import React, { Component } from 'react';
import { Formik, Form, FormikProps } from 'formik';
import isEmpty from 'lodash-es/isEmpty';
import isPlainObject from 'lodash-es/isPlainObject';

import { Button } from 'components/Button';
import {
  Drawer,
  DrawerBody,
  DrawerHeader,
  DrawerFooter
} from 'components/Drawer';
import { Field, FormError, ReactSelect } from 'components/FormikWrapper';

import { Data } from 'interfaces/channel';

import SocialIcons from 'components/utils/SocialIcons';
import labels from './channelLabels';
import validationSchema from './validationSchema';

interface FieldMapping {
  text: string;
  optional: boolean;
}

class GravatarOption extends React.PureComponent<any, any> {
  handleMouseDown = (event: any) => {
    event.preventDefault();
    event.stopPropagation();
    this.props.onSelect(this.props.option, event);
  };

  handleMouseEnter = (event: any) => {
    this.props.onFocus(this.props.option, event);
  };

  handleMouseMove = (event: any) => {
    if (this.props.isFocused) {
      return;
    }
    this.props.onFocus(this.props.option, event);
  };

  render() {
    return (
      <div
        className={this.props.className}
        onMouseDown={this.handleMouseDown}
        onMouseEnter={this.handleMouseEnter}
        onMouseMove={this.handleMouseMove}
        title={this.props.option.title}
      >
        <img
          src={SocialIcons[this.props.option.value]}
          width={15}
          alt=""
          className="mr-2"
        />
        {this.props.children}
      </div>
    );
  }
}

class GravatarValue extends React.PureComponent<any, any> {
  render() {
    return (
      <div className="Select-value" title={this.props.value.title}>
        <span className="Select-value-label">
          <img
            src={SocialIcons[this.props.value.value]}
            width={15}
            alt=""
            className="mr-2"
          />
          {this.props.children}
        </span>
      </div>
    );
  }
}

interface Props {
  isOpen: boolean;
  type: string;
  id?: string;
  data?: any;
  onCancel: Function;
  onSubmit(data: Data);
}

interface States {}

const inputId = 'form-input-channel';

const channelType = [
  {
    label: 'Line',
    value: 'line'
  },
  {
    label: 'FB Messenger',
    value: 'fbmessenger'
  },
  {
    label: 'Slack',
    value: 'slack'
  },
  {
    label: 'Telegram',
    value: 'telegram'
  },
  {
    label: 'Qiscus',
    value: 'qiscus'
  },
  {
    label: 'Twitter',
    value: 'twitter'
  },
  {
    label: 'Generic',
    value: 'generic'
  }
];

function getMapping(type: string) {
  if (!labels[type]) {
    return undefined;
  }

  return labels[type];
}

class ChannelForm extends Component<Props, States> {
  componentDidMount() {
    const el = document.getElementById(inputId);
    if (el && el.focus) {
      el.focus();
    }
  }

  isVisible = (allows: string[], type: string): boolean => {
    return allows.indexOf(type) !== -1;
  };

  onClose = () => {
    this.props.onCancel();
  };

  onSubmit = ({ options, defaultMessages, ...rest }: any) => {
    const newData = {
      ...rest,
      options: {
        ...options,
        ...defaultMessages
      }
    };
    this.props.onSubmit(newData);
  };

  renderChannelFields = (type: string) => {
    const channelFields = getMapping(type);
    const getField = (
      currentField: object | string,
      prop: 'text' | 'optional' = 'text'
    ) =>
      !isPlainObject(currentField)
        ? currentField
        : (currentField as FieldMapping)[prop];

    return !isEmpty(channelFields)
      ? Object.keys(channelFields).map(field => {
          const currentField = channelFields[field];

          return (
            <div className="kata-info__container" key={field}>
              <div className="kata-info__label">
                {getField(currentField)}{' '}
                {getField(currentField, 'optional') === true ? (
                  <small className="text-muted">(optional)</small>
                ) : null}
              </div>
              <div className="kata-info__content">
                <Field
                  name={field === 'url' ? 'url' : `options.${field}`}
                  className="form-control kata-form__input-text"
                />
                {field === 'token' && <FormError name="options" />}
              </div>
            </div>
          );
        })
      : null;
  };

  innerForm = (formApi: FormikProps<any>) => (
    <Form className="kata-form full-size">
      <DrawerHeader title={`${this.props.type} Channel`} />
      <DrawerBody>
        <div className="kata-info__container">
          <div className="kata-info__label">Name</div>
          <div className="kata-info__content">
            <Field name="name" className="form-control kata-form__input-text" />
          </div>
        </div>
        <div className="kata-info__container">
          <div className="kata-info__label">Type</div>
          <div className="kata-info__content">
            <ReactSelect
              name="type"
              clearable={false}
              simpleValue
              options={channelType}
              optionComponent={GravatarOption}
              valueComponent={GravatarValue}
              onChange={val => {
                formApi.setFieldValue('options', {});
                switch (val) {
                  case 'line':
                    return formApi.setFieldValue('url', 'https://api.line.me');
                  case 'fbmessenger':
                    return formApi.setFieldValue(
                      'url',
                      'https://graph.facebook.com/v2.6/me/messages'
                    );
                  case 'slack':
                    return formApi.setFieldValue(
                      'url',
                      'https://slack.com/api'
                    );
                  case 'telegram':
                    return formApi.setFieldValue(
                      'url',
                      'https://api.telegram.org'
                    );
                  case 'twitter':
                    return formApi.setFieldValue(
                      'url',
                      'https://api.twitter.com'
                    );
                  default:
                    return formApi.setFieldValue('url', '');
                }
              }}
            />
          </div>
        </div>
        {this.renderChannelFields(formApi.values.type)}
      </DrawerBody>
      <DrawerFooter>
        <Button
          color="primary"
          className="text-capitalize mr-1"
          type="submit"
          disabled={!formApi.isValid}
        >
          {this.props.type}
        </Button>
        <Button type="button" color="secondary" onClick={this.onClose}>
          Cancel
        </Button>
      </DrawerFooter>
    </Form>
  );

  render() {
    const { isOpen, data } = this.props;
    return (
      <Drawer isOpen={isOpen} onClose={this.onClose}>
        <Formik
          onSubmit={this.props.onSubmit}
          initialValues={data}
          validationSchema={validationSchema}
        >
          {this.innerForm}
        </Formik>
      </Drawer>
    );
  }
}

export default ChannelForm;
