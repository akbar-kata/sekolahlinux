import * as yup from 'yup';
import isPlainObject from 'lodash-es/isPlainObject';
import labels from './channelLabels';

function isVisible(allows: string[], type: string) {
  return allows.indexOf(type) !== -1;
}
function getLabel(type: string, label: string) {
  if (!labels[type]) {
    return undefined;
  }
  return !isPlainObject(labels[type][label])
    ? labels[type][label]
    : labels[type][label].text;
}

const validationSchema = yup.lazy((values: any) => {
  return yup.object().shape({
    name: yup
      .string()
      .required('Name is required')
      .matches(
        /^[A-Za-z][-A-Za-z0-9_-]*[A-Za-z0-9]$/,
        'Name must contain only alphanumeric characters (A-Z, a-z, 0-9), `-`, and `_`.'
      ),
    type: yup.string().required('Type must be selected'),
    url: yup
      .string()
      .url('URL is not valid')
      .required('URL is required'),
    options: yup.object().shape({
      token: yup.lazy(val => {
        return !isVisible(
          ['line', 'fbmessenger', 'twitter', 'slack', 'telegram'],
          values.type
        ) && !values.token
          ? yup.mixed()
          : yup
              .string()
              .required(`${getLabel(values.type, 'token')} is required`);
      }),
      secret: yup.lazy(val => {
        return !isVisible(['twitter'], values.type) && !values.options.secret
          ? yup.mixed()
          : yup
              .string()
              .required(`${getLabel(values.type, 'secret')} is required`);
      }),
      apiKey: yup.lazy(val => {
        return !isVisible(['twitter'], values.type) && !values.options.apiKey
          ? yup.mixed()
          : yup
              .string()
              .required(`${getLabel(values.type, 'apiKey')} is required`);
      }),
      apiSecret: yup.lazy(val => {
        return !isVisible(['twitter'], values.type) && !values.options.apiSecret
          ? yup.mixed()
          : yup
              .string()
              .required(`${getLabel(values.type, 'apiSecret')} is required`);
      })
    })
  });
});

export default validationSchema;
