import React, { Fragment } from 'react';

import { Drawer, DrawerBody, DrawerHeader } from 'components/Drawer';
import { confirm } from 'components/Modal';
import { FloatingButton } from 'components/Button';

import SocialIcons from 'components/utils/SocialIcons';

interface Props {
  isOpen: boolean;
  data: any;
  onUpdate(data: any);
  onDelete(id: string, name: string);
  onClose(): void;
  notify(opts: any): void;
}

class ChannelDetail extends React.Component<Props> {
  copyToClipboard = (id: string, str: any) => {
    const el = document.createElement('textarea');
    el.defaultValue = '';
    el.value = str;
    el.setAttribute('readonly', '');
    el.style.position = 'absolute';
    el.style.left = '-9999px';
    document.body.appendChild(el);
    const selected =
      document.getSelection()!.rangeCount > 0
        ? document.getSelection()!.getRangeAt(0)
        : false;
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
    if (selected) {
      document.getSelection()!.removeAllRanges();
      document.getSelection()!.addRange(selected);
    }

    this.props.notify({
      title: 'Success',
      message: `You have successfully copied the ${id}.`,
      status: 'success',
      dismissible: true,
      dismissAfter: 5000
    });
  };
  onDelete = () => {
    confirm({
      title: 'Delete Channel',
      message: `Are you sure you want to delete this channel?`,
      okLabel: 'Delete',
      cancelLabel: 'Cancel'
    }).then(res => {
      if (res) {
        this.props.onDelete(this.props.data.id, this.props.data.name);
      }
    });
  };

  render() {
    const { isOpen, data, onClose } = this.props;
    return (
      <Drawer isOpen={isOpen} onClose={onClose}>
        <DrawerHeader title={`View Channel`} />
        <DrawerBody>
          {data && (
            <Fragment>
              <div className="kata-info__container">
                <div className="kata-info__label">ID</div>
                <input
                  className="form-control kata-form__input-text"
                  disabled
                  value={data.id}
                />
              </div>
              <div className="kata-info__container">
                <table className="kata-light-table mt-2 mb-2">
                  <thead>
                    <tr>
                      <th>Name</th>
                      <th>Type</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>
                        <span className="pl-1">{data.name}</span>
                      </td>
                      <td className="text-center">
                        <img
                          className="kata-channel__list-icon mb-1 mt-1"
                          src={SocialIcons[data.type]}
                          alt={data.type}
                        />
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              {data.options && (
                <Fragment>
                  {data.webhook && (
                    <div className="kata-info__container">
                      <div className="kata-info__label">Webhook</div>
                      <div className="kata-channel__input-wrapper">
                        <textarea
                          className="form-control kata-form__input-text kata-channel__input-webhook"
                          readOnly
                          id="webhook"
                        >
                          {data.webhook}
                        </textarea>
                        <FloatingButton
                          icon="copy"
                          color="primary"
                          className="kata-channel__input-webhook--copy"
                          onClick={() =>
                            this.copyToClipboard('webhook', `${data.webhook}`)
                          }
                        />
                      </div>
                    </div>
                  )}
                  {data.options.token && (
                    <div className="kata-info__container">
                      <div className="kata-info__label">Token</div>
                      <div className="kata-info__content kata-channel--break-word">
                        <div className="kata-channel__input-wrapper">
                          <textarea
                            className="form-control kata-form__input-text kata-channel__input-token"
                            readOnly
                            id="token"
                          >
                            {data.options.token}
                          </textarea>
                          <FloatingButton
                            icon="copy"
                            color="primary"
                            className="kata-channel__input-webhook--copy"
                            onClick={() =>
                              this.copyToClipboard(
                                'token',
                                `${data.options.token}`
                              )
                            }
                          />
                        </div>
                      </div>
                    </div>
                  )}
                  {data.options.refreshToken && (
                    <div className="kata-info__container">
                      <div className="kata-info__label">Refresh Token</div>
                      <div className="kata-info__content kata-channel--break-word">
                        {data.options.refreshToken}
                      </div>
                    </div>
                  )}
                  {data.options.secret && (
                    <div className="kata-info__container">
                      <div className="kata-info__label">Channel Secret</div>
                      <input
                        className="form-control kata-form__input-text"
                        disabled
                        value={data.options.secret}
                      />
                    </div>
                  )}
                  {data.options.challenge && (
                    <div className="kata-info__container">
                      <div className="kata-info__label">Challenge Token</div>
                      <input
                        className="form-control kata-form__input-text"
                        disabled
                        value={data.options.challenge}
                      />
                    </div>
                  )}
                </Fragment>
              )}
              <div className="kata-info__container">
                <div className="kata-info__label">URL</div>
                <input
                  className="form-control kata-form__input-text"
                  disabled
                  value={data.url}
                />
              </div>
            </Fragment>
          )}
        </DrawerBody>
      </Drawer>
    );
  }
}

export default ChannelDetail;
