import React from 'react';
import SocialIcons from 'components/utils/SocialIcons';
import { TooltipTarget, Tooltip } from 'components/Tooltip';
import { EmptyMessage } from 'components/Common';
import { Button, FloatingButton } from 'components/Button';
import { confirm } from 'components/Modal';
interface Props {
  isLoading: boolean;
  error: string | null;
  index: string[];
  data: any;
  openInfo: Function;
  notify(opts: any): void;
  onUpdate(data: any);
  onDelete(id: string, name: string);
}

const width150 = {
  width: 150
};

const width100 = {
  width: 100
};

class ChannelList extends React.Component<Props, any> {
  onCopyWebhook = (key: string, webhook: string) => {
    const el = document.createElement('textarea');
    el.defaultValue = '';
    el.value = webhook;
    el.setAttribute('readonly', '');
    el.style.position = 'absolute';
    el.style.left = '-9999px';
    document.body.appendChild(el);
    const selected =
      document.getSelection()!.rangeCount > 0
        ? document.getSelection()!.getRangeAt(0)
        : false;
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
    if (selected) {
      document.getSelection()!.removeAllRanges();
      document.getSelection()!.addRange(selected);
    }
    this.props.notify({
      title: 'Success',
      message: `You have successfully copied the ${key}.`,
      status: 'success',
      dismissible: true,
      dismissAfter: 5000
    });
  };

  onDelete = (key: string) => e => {
    e.stopPropagation();
    confirm({
      title: 'Delete Channel',
      message: `Are you sure you want to delete this channel?`,
      okLabel: 'Delete',
      cancelLabel: 'Cancel'
    }).then(res => {
      if (res) {
        this.props.onDelete(this.props.data[key].id, this.props.data[key].name);
      }
    });
  };

  renderMessage(child: React.ReactNode) {
    return (
      <tr>
        <td colSpan={6} className="text-center">
          {child}
        </td>
      </tr>
    );
  }

  renderLoading() {
    return this.renderMessage(<h5 className="p-4 text-bold">Loading...</h5>);
  }

  renderError() {
    return this.renderMessage(
      <EmptyMessage title="Channel is empty">
        You don’t have a channel yet. Create a channel by click the “Create
        Channel” on top
      </EmptyMessage>
    );
  }

  renderData() {
    const { index, data } = this.props;
    if (index.length === 0) {
      return this.renderMessage(
        <EmptyMessage title="Channel is empty">
          You don’t have a channel yet. Create a channel by click the “Create
          Channel” on top
        </EmptyMessage>
      );
    }

    return index.map((key: string) => (
      <tr key={key} onClick={() => this.props.openInfo(data[key])}>
        <td style={width150}>{data[key].name}</td>
        <td style={width100} className="text-center">
          <img
            className="kata-channel__list-icon"
            src={SocialIcons[data[key].type]}
            alt={data[key].type}
          />
        </td>
        <td className="kata-channel__table-webhook">
          <div id="webhook" className="kata-channel__text-webhook--copy">
            {data[key].webhook}
          </div>
          <div
            className="kata-channel__button-webhook--copy"
            onClick={e => {
              e.stopPropagation();
              this.onCopyWebhook('webhook', data[key].webhook);
            }}
          >
            <TooltipTarget
              trigger="hover"
              placement="top"
              component={<Tooltip>Copy</Tooltip>}
            >
              <FloatingButton
                icon="copy"
                size="sm"
                color="primary"
                className="kata-channel__input-webhook--copy"
              />
            </TooltipTarget>
          </div>
        </td>
        <td style={width150} className="text-center">
          <div className="kata-channel__list-card-buttons">
            <Button
              color="white"
              onClick={e => {
                e.stopPropagation();
                this.props.onUpdate(data[key]);
              }}
            >
              <i className="icon-edit text-primary" />{' '}
              <span className="text-primary">Update</span>
            </Button>{' '}
            <Button color="white" onClick={this.onDelete(key)}>
              <i className="icon-trash text-danger" />{' '}
              <span className="text-danger">Delete</span>
            </Button>
          </div>
        </td>
      </tr>
    ));
  }

  render() {
    const { isLoading, error } = this.props;
    return (
      <table className="kata-table table-hover table-striped">
        <thead>
          <tr>
            <th style={width150}>Name</th>
            <th className="text-center" style={width100}>
              Type
            </th>
            <th className="text-left">Webhook</th>
            <th style={width150} className="text-center">
              Action
            </th>
          </tr>
        </thead>
        <tbody>
          {(!isLoading &&
            ((error !== null && this.renderError()) || this.renderData())) ||
            this.renderLoading()}
        </tbody>
      </table>
    );
  }
}

export default ChannelList;
