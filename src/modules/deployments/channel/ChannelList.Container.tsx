import React, { Fragment } from 'react';
import { connect } from 'react-redux';

import RootStore from 'interfaces/rootStore';

import {
  updateChannelRequest,
  removeChannelRequest,
  fetchChannelRequest
} from 'stores/deployment/channel/actions';
import {
  getChannelLoading,
  getChannelError,
  getChannelIndex,
  getChannelData
} from 'stores/deployment/channel/selectors';
import * as Environment from 'interfaces/deployment/environment';
import { openDrawer, closeDrawer } from 'stores/app/drawer/actions';
import { addNotification } from 'stores/app/notification/actions';
import { isDrawerOpen, getDrawerData } from 'stores/app/drawer/selectors';

import ChannelList from './ChannelList';
import ChannelDetail from './ChannelDetail';
import ChannelForm from './ChannelForm';
import { getProjectSelected } from 'stores/project/selectors';
import { getEnvironmentSelected } from 'stores/deployment/environment/selectors';
import { selectEnv } from 'stores/deployment/environment/actions';

import ErrorBoundary from 'modules/core/ErrorBoundary';

interface PropsFromState {
  selectedProject: string | null;
  environmentId: string | null;
  isLoading: boolean;
  error: string | null;
  index: string[];
  data: any;
  isInfoOpen: boolean;
  dataInfo: any;
  isUpdateOpen: boolean;
  dataToUpdate: any;
}

interface PropsFromDispatch {
  fetchChannelRequest: typeof fetchChannelRequest;
  selectEnv: typeof selectEnv;
  openInfo: Function;
  closeUpdate: Function;
  onUpdate: Function;
  onDelete: Function;
  openUpdate: (data: any) => void;
  closeInfo();
  notify(opts: any): void;
}

interface OwnProps {
  selectedEnv: Environment.Data | null;
}

interface Props extends PropsFromDispatch, PropsFromState, OwnProps {}

interface State {}

const updateDrawerId = 'ChannelUpdate';
const infoDrawerId = 'ChannelDetail';

class ChannelListContainer extends React.Component<Props, State> {
  getDataToUpdate = () => {
    const { dataToUpdate } = this.props;
    if (dataToUpdate) {
      const { options, ...rest } = this.props.dataToUpdate;
      const {
        rpmLimitResponse,
        burstMessageResponse,
        channelDisabledResponse,
        maintenanceResponse,
        ...restOpts
      } = options || {
        rpmLimitResponse: undefined,
        burstMessageResponse: undefined,
        channelDisabledResponse: undefined,
        maintenanceResponse: undefined
      };
      return {
        ...rest,
        options: {
          ...restOpts
        },
        defaultMessages: {
          rpmLimitResponse,
          burstMessageResponse,
          channelDisabledResponse,
          maintenanceResponse
        }
      };
    }
    return {};
  };

  openUpdate = data => {
    this.props.closeInfo();
    this.props.openUpdate(data);
  };

  onUpdate = data => {
    this.props.onUpdate(
      data.id,
      this.props.selectedProject,
      this.props.environmentId,
      data
    );
  };

  onDelete = (id, name) => {
    this.props.onDelete(
      id,
      name,
      this.props.selectedProject,
      this.props.environmentId
    );
    this.props.closeInfo();
  };

  render() {
    return (
      <Fragment>
        <ChannelDetail
          isOpen={this.props.isInfoOpen}
          data={this.props.dataInfo}
          notify={this.props.notify}
          onUpdate={this.openUpdate}
          onDelete={this.onDelete}
          onClose={this.props.closeInfo}
        />
        <ChannelForm
          isOpen={this.props.isUpdateOpen}
          type="update"
          data={this.getDataToUpdate()}
          onSubmit={this.onUpdate}
          onCancel={this.props.closeUpdate}
        />
        <ErrorBoundary>
          <ChannelList
            isLoading={this.props.isLoading}
            error={this.props.error}
            index={this.props.index}
            data={this.props.data}
            notify={this.props.notify}
            onUpdate={this.openUpdate}
            onDelete={this.onDelete}
            openInfo={this.props.openInfo}
          />
        </ErrorBoundary>
      </Fragment>
    );
  }
}

const mapStateToProps = ({
  project,
  deployment,
  app: { drawer }
}: RootStore): PropsFromState => {
  return {
    selectedProject: getProjectSelected(project),
    environmentId: getEnvironmentSelected(deployment.environment),
    isLoading: getChannelLoading(deployment.channel, 'fetch'),
    error: getChannelError(deployment.channel, 'fetch'),
    index: getChannelIndex(deployment.channel),
    data: getChannelData(deployment.channel),
    isInfoOpen: isDrawerOpen(drawer, infoDrawerId),
    dataInfo: getDrawerData(drawer, infoDrawerId),
    isUpdateOpen: isDrawerOpen(drawer, updateDrawerId),
    dataToUpdate: getDrawerData(drawer, updateDrawerId)
  };
};

const mapDispatchToProps: PropsFromDispatch = {
  fetchChannelRequest,
  selectEnv,
  openUpdate: (data: any) => openDrawer(updateDrawerId, data),
  closeUpdate: () => closeDrawer(updateDrawerId),
  openInfo: (data: any) => openDrawer(infoDrawerId, data),
  closeInfo: () => closeDrawer(infoDrawerId),
  onUpdate: (id: string, botId: string, deploymentId: string, data: any) =>
    updateChannelRequest(id, botId, deploymentId, data),
  onDelete: (id: string, name: string, botId: string, deploymentId: string) =>
    removeChannelRequest(id, name, botId, deploymentId),
  notify: (opts: any) => addNotification(opts)
};

export default connect<PropsFromState, PropsFromDispatch, OwnProps, RootStore>(
  mapStateToProps,
  mapDispatchToProps
)(ChannelListContainer);
