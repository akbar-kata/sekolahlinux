import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps, Link } from 'react-router-dom';

import RootStore from 'interfaces/rootStore';
import * as Environment from 'interfaces/deployment/environment';

import SelectedUrl from 'containers/SelectedUrl';

import { Dashboard } from 'components/Dashboard';

import { getProjectSelected } from 'stores/project/selectors';

import {
  fetchDeploymentRequest,
  fetchLatestDeploymentRequest
} from 'stores/deployment/actions';
import {
  getChannelLoading,
  getChannelError,
  getChannelIndex,
  getChannelData
} from 'stores/deployment/channel/selectors';
import {
  fetchChannelRequest,
  clearChannelList
} from 'stores/deployment/channel/actions';
import {
  getEnvironmentSelected,
  getEnvironmentDetail,
  getEnvironmentData
} from 'stores/deployment/environment/selectors';
import {
  fetchEnvRequest,
  selectEnv
} from 'stores/deployment/environment/actions';

import ChannelList from './ChannelList.Container';
import CreateChannel from './ChannelCreate.Container';

interface RouteParams {
  selectedEnvironment: string;
}

interface PropsFromState {
  envData: Environment.DataMap;
  project: string | null;
  selectedEnv: Environment.Data | null;
  isLoading: boolean;
  error: string | null;
  index: string[];
  data: any;
}

interface PropsFromDispatch {
  fetchEnvRequest: typeof fetchEnvRequest;
  selectEnv: typeof selectEnv;
  fetchDeploymentRequest: typeof fetchDeploymentRequest;
  fetchLatestDeploymentRequest: typeof fetchLatestDeploymentRequest;
  fetchChannelRequest: typeof fetchChannelRequest;
  clearChannelList: typeof clearChannelList;
}

interface Props
  extends PropsFromState,
    PropsFromDispatch,
    RouteComponentProps<RouteParams> {}

class DeploymentChannelPage extends React.Component<Props> {
  componentDidMount() {
    this.props.fetchEnvRequest();
    this.props.fetchDeploymentRequest();
    this.props.fetchLatestDeploymentRequest();

    if (this.props.project) {
      this.props.selectEnv(this.props.match.params.selectedEnvironment);
    }
  }

  componentWillUnmount() {
    this.props.clearChannelList();
  }

  render() {
    return (
      <Dashboard
        headerContent={
          <SelectedUrl>
            {projectId => {
              const parentPath = `/project/${projectId}/deployment`;
              return (
                <Fragment>
                  <Link
                    className="btn kata-btn-float kata-subpage__back-btn kata-btn-float__primary"
                    to={`${parentPath}/environments`}
                  >
                    <i className="icon-arrow-left" />
                  </Link>
                  <div className="kata-channel__header-wrapper">
                    <div className="float-left kata-channel__header-left">
                      <h1 className="heading1 kata-subpage__title">
                        {this.props.selectedEnv
                          ? this.props.selectedEnv.name
                          : this.props.error
                          ? '-'
                          : 'Loading...'}
                      </h1>
                      <Link
                        to={`${parentPath}/environments`}
                        className="text-muted"
                      >
                        Environment
                      </Link>
                      <span className="kata-subpage__bread-separator">/</span>
                      <span className="text-primary">
                        {this.props.selectedEnv
                          ? this.props.selectedEnv.name
                          : this.props.error
                          ? '-'
                          : 'Loading...'}
                      </span>
                    </div>
                    <div className="kata-channel__header-right">
                      <CreateChannel />
                    </div>
                  </div>
                </Fragment>
              );
            }}
          </SelectedUrl>
        }
      >
        <div className="kata-channel__table-wrapper">
          <ChannelList selectedEnv={this.props.selectedEnv} />
        </div>
      </Dashboard>
    );
  }
}

const mapStateToProps = ({
  project,
  deployment
}: RootStore): PropsFromState => {
  const selectedEnv = getEnvironmentSelected(deployment.environment);
  const envData = getEnvironmentData(deployment.environment);

  return {
    envData,
    project: getProjectSelected(project),
    selectedEnv: selectedEnv
      ? getEnvironmentDetail(deployment.environment, selectedEnv)
      : null,
    isLoading: getChannelLoading(deployment.channel, 'fetch'),
    error: getChannelError(deployment.channel, 'fetch'),
    index: getChannelIndex(deployment.channel),
    data: getChannelData(deployment.channel)
  };
};

const mapDispatchToProps: PropsFromDispatch = {
  fetchEnvRequest,
  selectEnv,
  fetchDeploymentRequest,
  fetchLatestDeploymentRequest,
  fetchChannelRequest,
  clearChannelList
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DeploymentChannelPage);
