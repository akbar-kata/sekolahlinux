import React, { Fragment } from 'react';
import { connect } from 'react-redux';

import { Button } from 'components/Button';

import RootStore from 'interfaces/rootStore';

import { addChannelRequest } from 'stores/deployment/channel/actions';
import {
  getChannelLoading,
  getChannelError
} from 'stores/deployment/channel/selectors';
import { openDrawer, closeDrawer } from 'stores/app/drawer/actions';
import { isDrawerOpen } from 'stores/app/drawer/selectors';

import ChannelForm from './ChannelForm';
import { getProjectSelected } from 'stores/project/selectors';
import { getEnvironmentSelected } from 'stores/deployment/environment/selectors';

interface PropsFromState {
  selectedProject: string | null;
  environmentId: string | null;
  isLoading: boolean;
  error: string | null;
  isOpen: boolean;
}

interface PropsFromDispatch {
  openAdd: (data?: any) => ReturnType<typeof openDrawer>;
  closeAdd: () => ReturnType<typeof closeDrawer>;
  onAdd: typeof addChannelRequest;
}

interface Props extends PropsFromState, PropsFromDispatch {}

interface State {}

const addModalId = 'ChannelCreate';
class ChannelCreate extends React.Component<Props, State> {
  openAdd = () => {
    this.props.openAdd();
  };

  public onAdd = data => {
    if (this.props.selectedProject && this.props.environmentId) {
      this.props.onAdd(
        this.props.selectedProject,
        this.props.environmentId,
        data
      );
    }
  };

  render() {
    return (
      <Fragment>
        <Button color="primary" onClick={this.openAdd}>
          <i className="icon-add mr-1" />
          Create Channel
        </Button>

        <ChannelForm
          isOpen={this.props.isOpen}
          type="create"
          onSubmit={this.onAdd}
          onCancel={this.props.closeAdd}
        />
      </Fragment>
    );
  }
}

const mapStateToProps = ({
  project,
  deployment: { environment, channel },
  bot,
  app: { drawer }
}: RootStore): PropsFromState => {
  return {
    selectedProject: getProjectSelected(project),
    environmentId: getEnvironmentSelected(environment),
    isLoading: getChannelLoading(channel, 'fetch'),
    error: getChannelError(channel, 'fetch'),
    isOpen: isDrawerOpen(drawer, addModalId)
  };
};

const mapDispatchToProps: PropsFromDispatch = {
  openAdd: (data?: any) => openDrawer(addModalId, data),
  closeAdd: () => closeDrawer(addModalId),
  onAdd: (projectId: string, deploymentId: string, data: any) =>
    addChannelRequest(projectId, deploymentId, data)
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ChannelCreate);
