const labels = {
  line: {
    token: 'Channel Access Token',
    refresh: {
      text: 'Refresh Token',
      optional: true
    },
    secret: 'Channel Secret'
  },
  fbmessenger: {
    token: 'Page Access Token',
    secret: 'App Secret'
  },
  slack: {
    token: 'Bot Token'
  },
  telegram: {
    token: 'Bot User OAuth Access Token'
  },
  qiscus: {
    token: 'Secret Key',
    botEmail: 'Bot Email',
    // appId: 'App ID',
    url: 'URL'
  },
  twitter: {
    token: 'Access Token',
    secret: 'Access Token Secret',
    apiKey: 'Consumer Key (API Key)',
    apiSecret: 'Consumer Secret (API Secret)'
  },
  generic: {
    token: 'Token',
    refreshToken: {
      text: 'Refresh Token',
      optional: true
    },
    secret: {
      text: 'Secret',
      optional: true
    },
    url: 'URL'
  }
};

export default labels;
