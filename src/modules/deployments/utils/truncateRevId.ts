import truncate from 'lodash-es/truncate';

export default function truncateRevId(revId: string) {
  return truncate(revId, {
    length: 7,
    omission: ''
  });
}
