import React from 'react';
import { Form, Formik, FormikState } from 'formik';

// import { Field } from 'components/FormikWrapper';
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'components/Modal';
import { Field } from 'components/FormikWrapper';
import { Button } from 'components/Button';
import { Data } from 'interfaces/deployment';
import { connect } from 'react-redux';
import Store from 'interfaces/rootStore';
import { getLatestDeployment } from 'stores/deployment/selectors';
import { rollbackDeploymentRequest } from 'stores/deployment/actions';
import * as yup from 'yup';

interface PropsFromState {
  latestDeployment: Data;
}
interface PropsFromDispatch {
  rollbackDeployment: Function;
}

interface Props {
  isOpen: boolean;
  formData: Data;
  closeModal: () => void;
}

type ComponentProps = PropsFromState & PropsFromDispatch & Props;

class RollbackForm extends React.Component<ComponentProps> {
  validationSchema = yup.object().shape({
    changelog: yup.string().required('Changelog description is required')
  });
  handleSubmit = (values: any) => {
    this.props.rollbackDeployment(
      values.changelog,
      this.props.formData.version
    );
  };
  render() {
    const { isOpen, closeModal } = this.props;
    return (
      <Modal show={isOpen} onClose={closeModal}>
        <Formik
          validationSchema={this.validationSchema}
          initialValues={{
            changelog: this.props.formData ? this.props.formData.changelog : ''
          }}
          onSubmit={this.handleSubmit}
        >
          {this.innerForm}
        </Formik>
      </Modal>
    );
  }

  private innerForm = (api: FormikState<any>) => {
    return (
      <Form>
        <ModalHeader onClose={this.props.closeModal}>
          Rollback Deployment
        </ModalHeader>
        <ModalBody>
          <p>
            You will rollback the deployment from version{' '}
            {this.props.latestDeployment
              ? this.props.latestDeployment.version
              : '0.0.0'}{' '}
            to version{' '}
            {this.props.formData ? this.props.formData.version : '0.0.0'}.
          </p>
          <div className="mt4 mb-2">
            <label className="control-label kata-form__label">
              Deployment Change Log
            </label>
            <Field
              name="changelog"
              component="textarea"
              rows={4}
              className="kata-form__input-textarea"
            />
          </div>
          <p className="text-danger kata-text__warning">
            IMPORTANT: Existing NL training data will also be rolled back to
            version{' '}
            {this.props.formData ? this.props.formData.version : '0.0.0'}
          </p>
        </ModalBody>
        <ModalFooter>
          <Button
            color="primary"
            type="submit"
            onClick={() => this.handleSubmit(api.values)}
          >
            Rollback
          </Button>
          <Button type="button" onClick={() => this.props.closeModal()}>
            Cancel
          </Button>
        </ModalFooter>
      </Form>
    );
  };
}

const mapStateToProps = ({ deployment }: Store): PropsFromState => ({
  latestDeployment: getLatestDeployment(deployment)
});
const mapDispatchToProps = (dispatch): PropsFromDispatch => ({
  rollbackDeployment: (changelog: string, newVersion: string) =>
    dispatch(
      rollbackDeploymentRequest({
        newVersion,
        changelog
      })
    )
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RollbackForm);
