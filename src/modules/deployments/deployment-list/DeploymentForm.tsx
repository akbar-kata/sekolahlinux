import React, { Fragment } from 'react';
import { Formik, FormikState } from 'formik';
import get from 'lodash-es/get';

import { Button } from 'components/Button';
import {
  Drawer,
  DrawerHeader,
  DrawerBody,
  DrawerFooter
} from 'components/Drawer';
import { Field } from 'components/FormikWrapper';
import { Data } from 'interfaces/deployment';

interface Props {
  isOpen: boolean;
  formData: Data;
  type?: 'update' | 'view';
  closeDrawer: Function;
}

class DeploymentForm extends React.Component<Props> {
  public static defaultProps = {
    type: 'view'
  };

  public render() {
    const { isOpen, closeDrawer } = this.props;
    const initialValues = get(this.props, 'formData', {});

    return (
      <Drawer isOpen={isOpen} onClose={() => closeDrawer()}>
        <Formik
          initialValues={initialValues}
          render={this.innerForm}
          onSubmit={values => {
            // no submit action in this form
          }}
        />
      </Drawer>
    );
  }

  /*
  private renderIncrementVersion = (
    values: Data,
    type: 'major' | 'minor' | 'patch' = 'patch'
  ): string => {
    const [major = 0, minor = 0, patch = 0] = (values.name || '0.0.0').split(
      '.'
    );

    if (type === 'patch') {
      return `${major}.${minor}.${Number(patch) + 1}`;
    } else if (type === 'minor') {
      return `${major}.${Number(minor) + 1}.${0}`;
    } else {
      return `${Number(major) + 1}.${0}.${0}`;
    }
  };
  */

  private canUpdate = () => {
    return this.props.type === 'update';
  };

  private innerForm = ({ values }: FormikState<Data>) => {
    return (
      <Fragment>
        <DrawerHeader title={`${this.props.type} Deployment`} />
        <DrawerBody>
          <div className="mt4 mb-2">
            <label className="control-label kata-form__label">
              Deployment's Name
            </label>
            <Field
              name="name"
              className="kata-form__input-text"
              disabled={!this.canUpdate()}
            />
          </div>
          <div className="mt4 mb-2">
            <label className="control-label kata-form__label">
              Bot Revision
            </label>
            <Field
              name="botRevision"
              className="kata-form__input-text"
              disabled={!this.canUpdate()}
            />
          </div>
          <div className="mt4 mb-2">
            <label className="control-label kata-form__label">
              NLU Revision
            </label>
            <Field
              name="nluRevision"
              className="kata-form__input-text"
              disabled={!this.canUpdate()}
            />
          </div>
          <div className="mt4 mb-2">
            <label className="control-label kata-form__label">
              CMS Revision
            </label>
            <Field
              name="cmsRevision"
              className="kata-form__input-text"
              disabled={!this.canUpdate()}
            />
          </div>
          <div className="mt4 mb-2">
            <label className="control-label kata-form__label">
              Deployment Change Log
            </label>
            <Field
              name="changelog"
              component="textarea"
              rows={4}
              className="kata-form__input-textarea"
              disabled={!this.canUpdate()}
            />
          </div>
        </DrawerBody>
        <DrawerFooter>
          <Button color="primary" type="submit" disabled={!this.canUpdate()}>
            Create
          </Button>{' '}
          <Button onClick={this.props.closeDrawer}>Cancel</Button>
        </DrawerFooter>
      </Fragment>
    );
  };
}

export default DeploymentForm;
