import React, { Fragment } from 'react';
import { Dispatch } from 'redux';
import { connect } from 'react-redux';

import { Robot } from 'components/Loading';
import { EmptyMessage } from 'components/Common';

import RootStore from 'interfaces/rootStore';
import { Data, DataMap } from 'interfaces/deployment';

import {
  fetchDeploymentRequest,
  fetchLatestDeploymentRequest
} from 'stores/deployment/actions';
import {
  getDeploymentIndex,
  getDeploymentData,
  getDeploymentLoading,
  getLatestDeployment
} from 'stores/deployment/selectors';
import { openDrawer, closeDrawer } from 'stores/app/drawer/actions';
import { isDrawerOpen, getDrawerData } from 'stores/app/drawer/selectors';
import { getModalData, isModalOpen } from 'stores/app/modal/selectors';
import { openModal, closeModal } from 'stores/app/modal/actions';

import DeploymentList from './DeploymentList';
import DeploymentForm from './DeploymentForm';
import RollbackForm from './RollbackForm';
import { getProjectSelected } from 'stores/project/selectors';

import ErrorBoundary from 'modules/core/ErrorBoundary';

interface PropsFromState {
  selectedProject: string | null;
  isLoading: boolean;
  index: string[];
  data: DataMap;
  isDetailOpen: boolean;
  isRollbackFormOpen: boolean;
  drawerData: any;
  modalData: any;
  latestDeployment: Data;
}

interface PropsFromDispatch {
  openDetail: (data: any) => ReturnType<typeof openDrawer>;
  closeDetail: () => ReturnType<typeof closeDrawer>;
  openRollbackForm: (data: any) => ReturnType<typeof openDrawer>;
  closeRollbackForm: () => ReturnType<typeof closeDrawer>;
  fetchDeployments: typeof fetchDeploymentRequest;
  fetchLatestDeployment: typeof fetchLatestDeploymentRequest;
}

type Props = PropsFromState & PropsFromDispatch;

export const detailId = 'DeploymentDetail';
export const rollbackFormId = 'DeploymentRollback';

const deploymentEmptyImg = require('assets/images/deployment-empty.svg');

class DeploymentListContainer extends React.Component<Props> {
  componentDidMount() {
    this.props.fetchDeployments();
    this.props.fetchLatestDeployment();
  }

  render() {
    const {
      isLoading,
      index,
      data,
      openDetail,
      closeDetail,
      isDetailOpen,
      isRollbackFormOpen,
      openRollbackForm,
      closeRollbackForm
    } = this.props;

    if (isLoading) {
      return <Robot />;
    }

    if (this.props.index.length === 0) {
      return (
        <EmptyMessage image={deploymentEmptyImg} title="Deployment is empty">
          This project don’t have any deployment yet. Create deployment by
          clicking the "New Deployment" button above.
        </EmptyMessage>
      );
    }

    return (
      <Fragment>
        <ErrorBoundary>
          <DeploymentList
            latestDeployment={this.props.latestDeployment}
            index={index}
            data={data}
            openDetail={openDetail}
            openRollbackForm={openRollbackForm}
          />
        </ErrorBoundary>
        <DeploymentForm
          type="view"
          isOpen={isDetailOpen}
          closeDrawer={closeDetail}
          formData={this.props.drawerData}
        />
        <RollbackForm
          isOpen={isRollbackFormOpen}
          closeModal={closeRollbackForm}
          formData={this.props.modalData}
        />
      </Fragment>
    );
  }
}

const mapStateToProps = ({ app, project, deployment }: RootStore) => ({
  selectedProject: getProjectSelected(project),
  isLoading: getDeploymentLoading(deployment, 'fetch'),
  index: getDeploymentIndex(deployment),
  data: getDeploymentData(deployment),
  isDetailOpen: isDrawerOpen(app.drawer, detailId),
  isRollbackFormOpen: isModalOpen(app.modal, rollbackFormId),
  drawerData: getDrawerData(app.drawer, detailId),
  modalData: getModalData(app.modal, rollbackFormId),
  latestDeployment: getLatestDeployment(deployment)
});

const mapDispatchToProps = (dispatch: Dispatch) => ({
  openDetail: (data: any) => dispatch(openDrawer(detailId, data)),
  closeDetail: () => dispatch(closeDrawer(detailId)),
  openRollbackForm: (data: any) => dispatch(openModal(rollbackFormId, data)),
  closeRollbackForm: () => dispatch(closeModal(rollbackFormId)),
  fetchDeployments: () => dispatch(fetchDeploymentRequest()),
  fetchLatestDeployment: () => dispatch(fetchLatestDeploymentRequest())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DeploymentListContainer);
