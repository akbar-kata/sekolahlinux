import React, { Fragment } from 'react';

import * as Deployment from 'interfaces/deployment';
import Card from 'components/Card';
import { Button } from 'components/Button';
import truncateRevId from '../utils/truncateRevId';

interface Props {
  index: string[];
  data: Deployment.DataMap;
  openDetail: Function;
  openRollbackForm: Function;
  latestDeployment: Deployment.Data;
}

const DeploymentList: React.SFC<Props> = ({
  index,
  data,
  openDetail,
  openRollbackForm,
  latestDeployment
}) => (
  <Fragment>
    {index.map(i => {
      const v = data[i];

      return (
        <Card key={v.name} className="kata-version-list-card mb-2">
          <div className="kata-version-list-card__version">
            <span className="heading2">{v.version}</span>
          </div>

          <div className="kata-version-list-card__tag">
            {v.tag && (
              <span className="badge badge-secondary kata-version-list-card__badge">
                {v.tag}
              </span>
            )}
            {latestDeployment && v.version === latestDeployment.version ? (
              <span className="badge badge-secondary kata-version-list-card__badge">
                latest
              </span>
            ) : null}
          </div>

          <div className="kata-version-list-card__revisions">
            <div className="kata-version-list-card__revision">
              <h5 className="text-label">Bot Revision</h5>
              <p>
                <samp title={v.botRevision}>
                  {truncateRevId(v.botRevision) || '-'}
                </samp>
              </p>
            </div>
            <div className="kata-version-list-card__revision">
              <h5 className="text-label">NLU Revision</h5>
              <p>
                <samp title={v.nluRevision}>
                  {truncateRevId(v.nluRevision) || '-'}
                </samp>
              </p>
            </div>
            <div className="kata-version-list-card__revision">
              <h5 className="text-label">CMS Revision</h5>
              <p>
                <samp title={v.cmsRevision}>
                  {truncateRevId(v.cmsRevision) || '-'}
                </samp>
              </p>
            </div>
          </div>

          <div className="kata-version-list-card__buttons">
            <Button color="white" onClick={() => openDetail(v)}>
              <i className="icon-view" /> <span>View</span>
            </Button>{' '}
            <Button
              color="white"
              onClick={() => openRollbackForm(v)}
              disabled={
                latestDeployment && v.version === latestDeployment.version
              }
            >
              <i className="icon-refresh" /> <span>Rollback</span>
            </Button>
          </div>
        </Card>
      );
    })}
  </Fragment>
);

export default DeploymentList;
