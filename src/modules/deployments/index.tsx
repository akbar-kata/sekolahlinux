import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';

import SelectedUrl from 'containers/SelectedUrl';
import { Robot } from 'components/Loading';

const Overview = React.lazy(() => import('./overview'));
const Environments = React.lazy(() => import('./environments'));
const Channel = React.lazy(() => import('./channel'));

const DeploymentsModule: React.SFC = () => {
  return (
    <SelectedUrl>
      {projectId => {
        const parentPath = `/project/${projectId}/deployment`;
        return (
          <React.Suspense fallback={<Robot />}>
            <Switch>
              <Route path={`${parentPath}/overview`} component={Overview} />
              <Route
                exact
                path={`${parentPath}/environments`}
                component={Environments}
              />
              <Route
                path={`${parentPath}/environments/:selectedEnvironment/channels`}
                component={Channel}
              />
              <Route
                render={() => <Redirect to={`${parentPath}/overview`} />}
              />
            </Switch>
          </React.Suspense>
        );
      }}
    </SelectedUrl>
  );
};

export default DeploymentsModule;
