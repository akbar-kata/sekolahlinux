import React from 'react';

import {
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from 'components/Dropdown';
import { Button } from 'components/Button';
import Card from 'components/Card';
import { InitialAvatar } from 'components/Avatar';
import { DashboardCards } from 'components/Dashboard';

import {
  Data,
  DataMap,
  EnvironmentChannel
} from 'interfaces/deployment/environment';
import { openDrawer } from 'stores/app/drawer/actions';
import { TooltipTarget, Tooltip } from 'components/Tooltip';

interface Props {
  envData: DataMap;
  envDataIndex: string[];
  openCreateEnv: (data: any) => ReturnType<typeof openDrawer>;
  openDetailEnv: (data: any) => ReturnType<typeof openDrawer>;
  openUpdateEnv: (data: any) => ReturnType<typeof openDrawer>;
  selectEnvironment: (id: string | null) => void;
}

interface State {}

class EnvironmentList extends React.Component<Props, State> {
  render() {
    return <DashboardCards>{this.renderAnotherEnvs()}</DashboardCards>;
  }

  constructInitialFromName = (name: string) => {
    const initial = name.toUpperCase().charAt(0);

    return initial;
  };

  findColorFromName = (name: string) => {
    const normalizedName = name.toLowerCase();

    if (normalizedName.includes('prod')) {
      return 'danger';
    }

    if (normalizedName.includes('stag')) {
      return 'warning';
    }

    return 'success';
  };

  renderChannelButton = (data: any) => {
    const channels: EnvironmentChannel[] = data.channels;
    const count = channels.length;

    if (count > 0) {
      return this.renderChannelCount(data);
    }

    return data.slug || data.depVersion ? (
      <Button
        color="primary"
        block
        onClick={() => this.props.selectEnvironment(data.id)}
      >
        Create Channel
      </Button>
    ) : (
      <Button
        color="primary"
        block
        onClick={() => this.props.openCreateEnv(data)}
      >
        Create Environment
      </Button>
    );
  };

  renderChannelCount = (data: Data) => {
    const channels: EnvironmentChannel[] = data.channels;
    const channelCount = channels.length;
    const buttonLabel = (count: number) => (count > 1 ? 'Channels' : 'Channel');
    const button = (count: number) => (
      <Button
        color="success"
        block
        onClick={() => this.props.selectEnvironment(data.id)}
      >
        {`${count} ${buttonLabel(count)}`}
      </Button>
    );

    return button(channelCount);
  };

  renderAnotherEnvs = () => {
    const { envData, envDataIndex } = this.props;

    const initialData: DataMap = {
      0: {
        id: '0',
        name: 'Development',
        depId: '',
        depVersion: '',
        slug: '',
        channels: []
      },
      1: {
        id: '1',
        name: 'Staging',
        depId: '',
        depVersion: '',
        slug: '',
        channels: []
      },
      2: {
        id: '2',
        name: 'Production',
        depId: '',
        depVersion: '',
        slug: '',
        channels: []
      }
    };

    // Filter out the environments already available
    let finalData = {};
    const arr = Object.values(envData);
    const result = arr.map(a => a.name);
    const filteredData = Object.values(initialData).filter(data => {
      return result.indexOf(data.name) === -1;
    });
    const filteredIndex = Object.values(filteredData).map(a => {
      finalData[a.id] = initialData[a.id];

      return a.id;
    });

    finalData = Object.assign(finalData, envData);
    const indexData = filteredIndex.concat(envDataIndex);

    return this.renderEnvironmentCards(indexData, finalData);
  };

  renderEnvironmentCards = (envIndex: string[], envData: DataMap) => {
    const { openDetailEnv, openUpdateEnv } = this.props;

    const sortMap = {
      Development: 0,
      Staging: 1,
      Production: 2
    };

    // HACK: normally id is null if none exists, but it exists as a placeholder. just check against every nullable value available.
    return envIndex
      .sort((a, b) => sortMap[envData[a].name] - sortMap[envData[b].name])
      .map((envId: string) => (
        <Card
          key={envData[envId].id}
          title={envData[envId].name}
          className="kata-environment__card"
          action={
            envData[envId].depId && envData[envId].depVersion ? (
              <Dropdown>
                <DropdownToggle caret={false}>
                  <Button isIcon>
                    <i className="icon-more" />
                  </Button>
                </DropdownToggle>
                <DropdownMenu right>
                  <DropdownItem onClick={() => openDetailEnv(envData[envId])}>
                    View
                  </DropdownItem>
                  <DropdownItem onClick={() => openUpdateEnv(envData[envId])}>
                    Update
                  </DropdownItem>
                </DropdownMenu>
              </Dropdown>
            ) : null
          }
          initialAvatar={
            <InitialAvatar
              initial={this.constructInitialFromName(envData[envId].name)}
              color={this.findColorFromName(envData[envId].name)}
              className="mr-1"
            />
          }
        >
          <div className="mb-2">
            <div className="kata-environment__card-info">
              <div className="kata-environment__card-info--key">
                Deployment Version
              </div>
              <div className="kata-environment__card-info--value">
                {envData[envId].depVersion}
              </div>
            </div>
            <div className="kata-environment__card-info">
              <div className="kata-environment__card-info--key">
                dashboard.kata.ai/
              </div>
              {envData[envId].slug ? (
                <TooltipTarget
                  component={<Tooltip>{envData[envId].slug}</Tooltip>}
                  placement="bottom"
                >
                  <div
                    className={`kata-environment__card-info--value kata-environment__ellipsis`}
                  >
                    {envData[envId].slug}
                  </div>
                </TooltipTarget>
              ) : (
                <div
                  className={`kata-environment__card-info--value kata-environment__ellipsis`}
                >
                  {envData[envId].slug}
                </div>
              )}
            </div>
            <br />
            {this.renderChannelButton(envData[envId])}
          </div>
        </Card>
      ));
  };
}

export default EnvironmentList;
