import React from 'react';

import { Dashboard } from 'components/Dashboard';

import EnvironmentList from './EnvironmentList.Container';

export default () => (
  <Dashboard
    title={'Environments'}
    tooltip="You can set up different environments for a specific purpose."
  >
    <EnvironmentList />
  </Dashboard>
);
