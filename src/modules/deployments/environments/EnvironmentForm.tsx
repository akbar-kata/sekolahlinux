import React, { Fragment } from 'react';
import { Formik, FormikState, FormikActions, Form } from 'formik';
import get from 'lodash-es/get';
import * as yup from 'yup';

import * as Deployment from 'interfaces/deployment';
import * as Environment from 'interfaces/deployment/environment';

import {
  Drawer,
  DrawerBody,
  DrawerHeader,
  DrawerFooter
} from 'components/Drawer';
import { Field, ReactSelect } from 'components/FormikWrapper';
import SocialIcons from 'components/utils/SocialIcons';
import { Button, SupportButton } from 'components/Button';
import { closeDrawer as closeDrawerAction } from 'stores/app/drawer/actions';
import {
  addEnvRequest,
  updateEnvRequest
} from 'stores/deployment/environment/actions';

interface Props {
  isOpen: boolean;
  formData: Environment.Data;
  type?: 'update' | 'view' | 'create';
  closeDrawer: () => ReturnType<typeof closeDrawerAction>;
  addEnv: typeof addEnvRequest;
  updateEnv: typeof updateEnvRequest;
  selectEnv: (id: string | null) => void;
  isCreating?: boolean;
  isUpdating?: boolean;
  currentProject: string | null;
  depIsLoading: boolean;
  depData: Deployment.DataMap;
  depIndex: string[];
}

class EnvironmentForm extends React.Component<Props> {
  static defaultProps = {
    type: 'view'
  };

  validationSchema = yup.object().shape({
    name: yup.string().required('Environment name is required'),
    depVersion: yup.string().required('Deployment version is required'),
    slug: yup.string().required('Environment URL is required')
  });

  submitForm = (
    values: Environment.Data,
    actions: FormikActions<Environment.Data>
  ) => {
    if (this.props.currentProject) {
      if (this.props.type === 'update') {
        this.props.updateEnv(
          this.props.currentProject,
          this.props.formData.id,
          values
        );
      } else if (this.props.type === 'create') {
        this.props.addEnv(this.props.currentProject, values);
      }
    }
  };

  mapDepVersions() {
    return this.props.depIndex.map(idx => {
      const data = this.props.depData[idx];

      return {
        label: data.version,
        value: data.version
      };
    });
  }

  render() {
    const { isOpen, closeDrawer } = this.props;
    const initialValues = get(this.props, 'formData', {});

    return (
      <Drawer isOpen={isOpen} onClose={() => closeDrawer()}>
        <Formik
          initialValues={initialValues}
          validationSchema={this.validationSchema}
          render={this.renderInnerForm}
          onSubmit={this.submitForm}
        />
      </Drawer>
    );
  }

  private isViewing = () => {
    return this.props.type === 'view';
  };

  private isCreating = () => {
    return this.props.type === 'create';
  };

  private canEdit = () => {
    return this.props.type === 'update' || this.props.type === 'create';
  };

  private renderInnerForm = (innerFormProps: FormikState<Environment.Data>) => {
    const { values } = innerFormProps;

    return (
      <Form className="kata-form full-size">
        <DrawerHeader title={`${this.props.type} Environment`} />
        <DrawerBody>
          <div className="mt4 mb-2">
            <label className="control-label kata-form__label">
              Environment Name
            </label>
            <Field
              name="name"
              className="kata-form__input-text"
              disabled
              readOnly
            />
          </div>

          {/* TODO: need to add select option for update version */}
          <div className="mt4 mb-2">
            <label className="control-label kata-form__label">
              Deployment Version
            </label>
            <ReactSelect
              name="depVersion"
              simpleValue
              options={this.mapDepVersions()}
              disabled={this.isViewing() || this.props.depIsLoading}
            />
          </div>

          <div className="mt4 mb-2">
            <label className="control-label kata-form__label">
              Environment URL
            </label>
            {/* if create mode, check if it's creating. if updating, also check the slug. */}
            {this.isCreating() ? (
              <Field
                name="slug"
                className="kata-form__input-text kata-form__input-text--group-prepend"
                disabled={!this.isCreating()}
                prependElement={
                  <span className="kata-input-group__text input-group-text">
                    https://dashboard.kata.ai/
                  </span>
                }
              />
            ) : this.isViewing() ? (
              <Field
                name="slug"
                className="kata-form__input-text kata-form__input-text--group-prepend"
                disabled
                prependElement={
                  <span className="kata-input-group__text input-group-text">
                    https://dashboard.kata.ai/
                  </span>
                }
              />
            ) : (
              <Field
                name="slug"
                className="kata-form__input-text kata-form__input-text--group-prepend"
                disabled={this.props.formData.slug}
                prependElement={
                  <span className="kata-input-group__text input-group-text">
                    https://dashboard.kata.ai/
                  </span>
                }
              />
            )}
          </div>

          {this.props.type !== 'create' && (
            <Fragment>
              <div className="mt4 mb-2">
                <label className="control-label kata-form__label">
                  Channels
                </label>
                {this.renderChannels(values.channels)}
              </div>

              <div className="mt4 mb-2">
                {/* HACK: normally id is null if none exists, but it exists as a placeholder. just check against every nullable value available. */}
                <SupportButton
                  disabled={!values.id || !values.depId || !values.depVersion}
                  onClick={() => this.props.selectEnv(values.id)}
                >
                  View Channels
                </SupportButton>
              </div>
            </Fragment>
          )}
        </DrawerBody>
        {this.canEdit() && (
          <DrawerFooter>
            <Button
              type="submit"
              color="primary"
              loading={this.props.isCreating || this.props.isUpdating}
            >
              {this.props.type === 'create' ? 'Create' : 'Update'}
            </Button>
            <Button
              type="button"
              color="secondary"
              onClick={this.props.closeDrawer}
            >
              Close
            </Button>
          </DrawerFooter>
        )}
      </Form>
    );
  };

  private renderChannels = (channels: Environment.EnvironmentChannel[]) => {
    return (
      <table className="kata-environment__table">
        <thead>
          <tr>
            <th>Name</th>
            <th style={{ width: 104 }}>Type</th>
          </tr>
        </thead>
        <tbody>
          {channels.length ? (
            channels.map((channel: Environment.EnvironmentChannel) => (
              <tr key={channel.id}>
                <td>{channel.name}</td>
                <td>
                  <img
                    src={SocialIcons[channel.type]}
                    width={24}
                    alt={channel.type}
                  />
                </td>
              </tr>
            ))
          ) : (
            <tr>
              <td colSpan={2} className="text-center">
                <span className="text-muted">No channels.</span>
              </td>
            </tr>
          )}
        </tbody>
      </table>
    );
  };
}

export default EnvironmentForm;
