import React, { Fragment } from 'react';
import { push } from 'connected-react-router';
import { connect } from 'react-redux';

import { Robot } from 'components/Loading';

import RootStore from 'interfaces/rootStore';
import * as Environment from 'interfaces/deployment/environment';
import * as Deployment from 'interfaces/deployment';

import {
  fetchDeploymentRequest,
  fetchLatestDeploymentRequest
} from 'stores/deployment/actions';
import {
  fetchEnvRequest,
  addEnvRequest,
  updateEnvRequest
} from 'stores/deployment/environment/actions';
import { openDrawer, closeDrawer } from 'stores/app/drawer/actions';

import {
  getEnvironmentData,
  getEnvironmentIndex,
  getEnvironmentLoading
} from 'stores/deployment/environment/selectors';
import { isDrawerOpen, getDrawerData } from 'stores/app/drawer/selectors';
import {
  getDeploymentIndex,
  getDeploymentData,
  getDeploymentLoading
} from 'stores/deployment/selectors';
import { getProjectSelected } from 'stores/project/selectors';

import EnvironmentList from './EnvironmentList';
import EnvironmentForm from './EnvironmentForm';
import ErrorBoundary from 'modules/core/ErrorBoundary';

interface PropsFromState {
  currentProject: string | null;
  envIsLoading: boolean;
  envData: Environment.DataMap;
  envIndex: string[];
  isCreateOpen: boolean;
  isDetailOpen: boolean;
  isUpdateOpen: boolean;
  createData: any;
  isCreating: boolean;
  detailData: any;
  updateData: any;
  isUpdating: boolean;
  depIsLoading: boolean;
  depData: Deployment.DataMap;
  depIndex: string[];
}

interface PropsFromDispatch {
  push: typeof push;
  fetchEnv: typeof fetchEnvRequest;
  fetchDeployments: typeof fetchDeploymentRequest;
  fetchLatestDeployment: typeof fetchLatestDeploymentRequest;
  addEnv: typeof addEnvRequest;
  updateEnv: typeof updateEnvRequest;
  openCreateEnv: (data: any) => ReturnType<typeof openDrawer>;
  closeCreateEnv: () => ReturnType<typeof closeDrawer>;
  openDetailEnv: (data: any) => ReturnType<typeof openDrawer>;
  closeDetailEnv: () => ReturnType<typeof closeDrawer>;
  openUpdateEnv: (data: any) => ReturnType<typeof openDrawer>;
  closeUpdateEnv: () => ReturnType<typeof closeDrawer>;
}

const envCreateId = 'EnvironmentCreate';
const envDetailId = 'EnvironmentDetail';
const envUpdateId = 'EnvironmentUpdate';

interface Props extends PropsFromState, PropsFromDispatch {}

class EnvironmentListContainer extends React.Component<Props> {
  componentDidMount() {
    this.props.fetchEnv();
    this.props.fetchDeployments();
    this.props.fetchLatestDeployment();
  }

  selectEnvironment = (id: string | null) => {
    this.props.closeCreateEnv();
    this.props.closeDetailEnv();
    this.props.closeUpdateEnv();
    this.props.push(`./environments/${id}/channels`);
  };

  render() {
    if (this.props.envIsLoading) {
      return <Robot />;
    }

    return (
      <Fragment>
        <ErrorBoundary>
          <EnvironmentList
            envDataIndex={this.props.envIndex}
            envData={this.props.envData}
            openCreateEnv={this.props.openCreateEnv}
            openDetailEnv={this.props.openDetailEnv}
            openUpdateEnv={this.props.openUpdateEnv}
            selectEnvironment={this.selectEnvironment}
          />
        </ErrorBoundary>

        {/* Create environment drawer */}
        <EnvironmentForm
          type="create"
          isOpen={this.props.isCreateOpen}
          closeDrawer={this.props.closeCreateEnv}
          formData={this.props.createData}
          depData={this.props.depData}
          depIndex={this.props.depIndex}
          currentProject={this.props.currentProject}
          addEnv={this.props.addEnv}
          updateEnv={this.props.updateEnv}
          depIsLoading={this.props.depIsLoading}
          isCreating={this.props.isCreating}
          selectEnv={this.selectEnvironment}
        />

        {/* View environment drawer */}
        <EnvironmentForm
          type="view"
          isOpen={this.props.isDetailOpen}
          closeDrawer={this.props.closeDetailEnv}
          formData={this.props.detailData}
          depData={this.props.depData}
          depIndex={this.props.depIndex}
          currentProject={this.props.currentProject}
          addEnv={this.props.addEnv}
          updateEnv={this.props.updateEnv}
          depIsLoading={this.props.depIsLoading}
          selectEnv={this.selectEnvironment}
        />

        {/* Update environment drawer */}
        <EnvironmentForm
          type="update"
          isOpen={this.props.isUpdateOpen}
          closeDrawer={this.props.closeUpdateEnv}
          formData={this.props.updateData}
          depData={this.props.depData}
          depIndex={this.props.depIndex}
          currentProject={this.props.currentProject}
          addEnv={this.props.addEnv}
          updateEnv={this.props.updateEnv}
          depIsLoading={this.props.depIsLoading}
          selectEnv={this.selectEnvironment}
        />
      </Fragment>
    );
  }
}

const mapStateToProps = ({ deployment, project, app }: RootStore) => {
  return {
    currentProject: getProjectSelected(project),
    envIsLoading: getEnvironmentLoading(deployment.environment, 'fetch'),
    envData: getEnvironmentData(deployment.environment),
    envIndex: getEnvironmentIndex(deployment.environment),
    isCreateOpen: isDrawerOpen(app.drawer, envCreateId),
    createData: getDrawerData(app.drawer, envCreateId),
    isCreating: getEnvironmentLoading(deployment.environment, 'add'),
    isDetailOpen: isDrawerOpen(app.drawer, envDetailId),
    detailData: getDrawerData(app.drawer, envDetailId),
    isUpdateOpen: isDrawerOpen(app.drawer, envUpdateId),
    updateData: getDrawerData(app.drawer, envUpdateId),
    isUpdating: getEnvironmentLoading(deployment.environment, 'update'),
    depIsLoading: getDeploymentLoading(deployment, 'fetch'),
    depData: getDeploymentData(deployment),
    depIndex: getDeploymentIndex(deployment)
  };
};

const mapDispatchToProps: PropsFromDispatch = {
  push,
  fetchEnv: fetchEnvRequest,
  addEnv: addEnvRequest,
  updateEnv: updateEnvRequest,
  fetchDeployments: fetchDeploymentRequest,
  fetchLatestDeployment: fetchLatestDeploymentRequest,
  openCreateEnv: (data: any) => openDrawer(envCreateId, data),
  closeCreateEnv: () => closeDrawer(envCreateId),
  openDetailEnv: (data: any) => openDrawer(envDetailId, data),
  closeDetailEnv: () => closeDrawer(envDetailId),
  openUpdateEnv: (data: any) => openDrawer(envUpdateId, data),
  closeUpdateEnv: () => closeDrawer(envUpdateId)
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EnvironmentListContainer);
