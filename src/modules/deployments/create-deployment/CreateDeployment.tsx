import React from 'react';
import { Form, Formik } from 'formik';
import * as yup from 'yup';

import {
  Drawer,
  DrawerBody,
  DrawerFooter,
  DrawerHeader
} from 'components/Drawer';
import ReactSelect from 'components/FormikWrapper/ReactSelect';
import { Button } from 'components/Button';
import Field from 'components/FormikWrapper/Field';

interface PropsFromState {
  selectedProject: string | null;
  isLoading: boolean;
  latestVersion: string;
  cmsRevisions: string[];
  botRevisions: string[];
  nluRevisions: string[];
}

interface OtherProps {
  disabled?: boolean;
  isOpen: boolean;
  toggleDrawer: () => void;
  onSubmit: any;
}

type CreateDeploymentProps = PropsFromState & OtherProps;

class CreateDeployment extends React.Component<CreateDeploymentProps> {
  validationSchema = yup.object().shape({
    version: yup.string().required('Version is required'),
    botRevision: yup.string().required('Bot revision is required'),
    nluRevision: yup.string().required('NLU revision is required'),
    cmsRevision: yup.string().required('CMS revision is required'),
    changelog: yup.string().required('Changelog description is required')
  });

  constructor(props: CreateDeploymentProps) {
    super(props);
  }

  renderIncrementVersion = (
    type: 'major' | 'minor' | 'patch' = 'patch'
  ): string => {
    const [major = 0, minor = 0, patch = 0] = (
      this.props.latestVersion || '0.0.0'
    ).split('.');

    if (type === 'patch') {
      return `${major}.${minor}.${Number(patch) + 1}`;
    }

    if (type === 'minor') {
      return `${major}.${Number(minor) + 1}.${0}`;
    }

    return `${Number(major) + 1}.${0}.${0}`;
  };

  getLatestVersion(versions: string[]): string {
    if (versions[0]) {
      return versions[0];
    }
    return '-';
  }

  innerForm = () => (
    <Form className="kata-form full-size">
      <DrawerHeader title="Create Deployment" />
      <DrawerBody>
        <div className="kata-info__container">
          <label htmlFor="name" className="kata-info__label">
            Deployment Version
          </label>
          <div className="kata-info__content">
            {this.props.isLoading ? (
              <div>Loading...</div>
            ) : (
              <ReactSelect
                name="version"
                simpleValue
                options={[
                  {
                    label: `Major (${this.renderIncrementVersion('major')})`,
                    value: this.renderIncrementVersion('major')
                  },
                  {
                    label: `Minor (${this.renderIncrementVersion('minor')})`,
                    value: this.renderIncrementVersion('minor')
                  },
                  {
                    label: `Patch (${this.renderIncrementVersion()})`,
                    value: this.renderIncrementVersion()
                  }
                ]}
              />
            )}
          </div>
        </div>
        <div className="kata-info__container">
          <label htmlFor="name" className="kata-info__label">
            Bot Revision
          </label>
          <div className="kata-info__content">
            <Field
              className="form-control kata-form__input-text"
              disabled
              name="botRevision"
            />
          </div>
        </div>
        <div className="kata-info__container">
          <label htmlFor="name" className="kata-info__label">
            NLU Revision
          </label>
          <div className="kata-info__content">
            <Field
              className="form-control kata-form__input-text"
              disabled
              name="nluRevision"
            />
          </div>
        </div>
        <div className="kata-info__container">
          <label htmlFor="name" className="kata-info__label">
            CMS Revision
          </label>
          <div className="kata-info__content">
            <Field
              className="form-control kata-form__input-text"
              disabled
              name="cmsRevision"
            />
          </div>
        </div>
        <div className="kata-info__container">
          <label htmlFor="name" className="kata-info__label">
            Deployment Change Log
          </label>
          <div className="kata-info__content">
            <Field
              component="textarea"
              name="changelog"
              rows={4}
              className="form-control kata-form__input-textarea"
            />
          </div>
        </div>
      </DrawerBody>
      <DrawerFooter>
        <Button color="primary" type="submit">
          Submit
        </Button>
      </DrawerFooter>
    </Form>
  );

  render() {
    const { toggleDrawer, isOpen, onSubmit } = this.props;

    return (
      <Drawer onClose={toggleDrawer} isOpen={isOpen}>
        <Formik
          validationSchema={this.validationSchema}
          onSubmit={onSubmit}
          isInitialValid={false}
          initialValues={{
            changelog: '',
            botRevision: this.getLatestVersion(this.props.botRevisions),
            nluRevision: this.getLatestVersion(this.props.nluRevisions),
            cmsRevision: this.getLatestVersion(this.props.cmsRevisions)
          }}
        >
          {this.innerForm}
        </Formik>
      </Drawer>
    );
  }
}

export default CreateDeployment;
