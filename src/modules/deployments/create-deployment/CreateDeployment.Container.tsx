import React, { Fragment } from 'react';
import { Dispatch } from 'redux';
import { connect } from 'react-redux';

import { Button } from 'components/Button';

import Store from 'interfaces/rootStore';
import * as Deployment from 'interfaces/deployment';

import PROJECT_TYPES from 'stores/project/types';
import { fetchVersionRequest } from 'stores/bot/version/actions';
import { getLoading } from 'stores/app/loadings/selectors';
import {
  getDeploymentLoading,
  getLatestDeployment
} from 'stores/deployment/selectors';

import {
  fetchBotRevisionsRequest,
  fetchCmsRevisionsRequest,
  fetchNluRevisionsRequest
} from 'stores/revision/actions';
import REVISION from 'stores/revision/types';
import { getCmsRevisionsIndex } from 'stores/revision/cms/selectors';
import { getBotRevisionsIndex } from 'stores/revision/bot/selectors';
import { getNluRevisionsIndex } from 'stores/revision/nlu/selectors';
import {
  addDeploymentRequest,
  fetchLatestDeploymentRequest
} from 'stores/deployment/actions';
import { closeDrawer, openDrawer } from 'stores/app/drawer/actions';
import { isDrawerOpen } from 'stores/app/drawer/selectors';

import CreateDeployment from './CreateDeployment';

interface PropsFromState {
  selectedProject: string | null;
  isLoading: boolean;
  latestDeployment: Deployment.Data | null;
  isExist: (name: string) => boolean;
  cmsRevisions: string[];
  botRevisions: string[];
  nluRevisions: string[];
  isDrawerOpen: boolean;
}

interface PropsFromDispatch {
  fetchVersion: Function;
  fetchCmsRevision: Function;
  fetchBotRevision: Function;
  fetchNluRevision: Function;
  addDeploymentRequest: Function;
  fetchLatestDeploymentRequest: Function;
  toggleDrawer: any;
}

interface OtherProps {
  disabled?: boolean;
}

type CreateDeploymentProps = PropsFromState & PropsFromDispatch & OtherProps;
export const DRAWER_ID = 'CreateDeploymentDrawer';

interface CreateDeploymentState {
  isOpen: boolean;
}

class CreateDeploymentContainer extends React.Component<
  CreateDeploymentProps,
  CreateDeploymentState
> {
  constructor(props: CreateDeploymentProps) {
    super(props);

    this.state = {
      isOpen: false
    };
  }

  toggleDrawer = () => {
    this.props.toggleDrawer(!this.props.isDrawerOpen);
  };

  onSubmit = data => {
    this.props.addDeploymentRequest(this.props.selectedProject, data);
  };

  componentDidMount() {
    this.props.fetchCmsRevision(this.props.selectedProject);
    this.props.fetchBotRevision(this.props.selectedProject);
    this.props.fetchNluRevision(this.props.selectedProject);
    this.props.fetchLatestDeploymentRequest();
  }

  renderInner() {
    const { disabled, ...rest } = this.props;

    if (!disabled) {
      return (
        <CreateDeployment
          isOpen={this.props.isDrawerOpen}
          toggleDrawer={this.toggleDrawer}
          onSubmit={this.onSubmit}
          selectedProject={this.props.selectedProject}
          latestVersion={
            this.props.latestDeployment
              ? this.props.latestDeployment.version
              : '0.0.0'
          }
          cmsRevisions={this.props.cmsRevisions}
          botRevisions={this.props.botRevisions}
          nluRevisions={this.props.nluRevisions}
          {...rest}
        />
      );
    }

    return null;
  }

  render() {
    const { disabled } = this.props;

    return (
      <Fragment>
        <Button
          className="kata-deployment-overview-dashboard__create-button"
          color="primary"
          onClick={this.toggleDrawer}
          disabled={disabled || this.props.isLoading}
        >
          <i className="icon-add" />
          <span>New Deployment</span>
        </Button>

        {this.renderInner()}
      </Fragment>
    );
  }
}

const mapStateToProps = ({ app, project, deployment, revision }: Store) => {
  return {
    selectedProject: project.selected,
    isLoading:
      project.selected === '' ||
      getLoading(app.loadings, PROJECT_TYPES.FETCH_PROJECT_REQUEST) ||
      getLoading(app.loadings, REVISION.FETCH_CMS_REVISIONS_REQUEST) ||
      getLoading(app.loadings, REVISION.FETCH_BOT_REVISIONS_REQUEST) ||
      getLoading(app.loadings, REVISION.FETCH_NLU_REVISIONS_REQUEST) ||
      getDeploymentLoading(deployment, 'fetch'),
    latestDeployment: getLatestDeployment(deployment),
    isExist: (name: string) => {
      return deployment.index.some(item => item === name);
    },
    cmsRevisions: getCmsRevisionsIndex(revision),
    botRevisions: getBotRevisionsIndex(revision),
    nluRevisions: getNluRevisionsIndex(revision),
    isDrawerOpen: isDrawerOpen(app.drawer, DRAWER_ID)
  };
};

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    fetchVersion: (botId: string) => dispatch(fetchVersionRequest(botId)),
    fetchCmsRevision: (projectId: string) =>
      dispatch(fetchCmsRevisionsRequest(projectId)),
    fetchBotRevision: (projectId: string) =>
      dispatch(fetchBotRevisionsRequest(projectId)),
    fetchNluRevision: (projectId: string) =>
      dispatch(fetchNluRevisionsRequest(projectId)),
    addDeploymentRequest: (projectId: string, data: Deployment.Data) =>
      dispatch(addDeploymentRequest(projectId, data)),
    toggleDrawer: (state: boolean) => {
      if (state) {
        dispatch(openDrawer(DRAWER_ID));
      } else {
        dispatch(closeDrawer(DRAWER_ID));
      }
    },
    fetchLatestDeploymentRequest: () => dispatch(fetchLatestDeploymentRequest())
  };
};

export default connect<PropsFromState, PropsFromDispatch, OtherProps, Store>(
  mapStateToProps,
  mapDispatchToProps
)(CreateDeploymentContainer);
