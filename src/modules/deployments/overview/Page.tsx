import React from 'react';
import { connect } from 'react-redux';
import { Dashboard } from 'components/Dashboard';

import RootStore from 'interfaces/rootStore';

import CreateDeploymentContainer from '../create-deployment/CreateDeployment.Container';
import DeploymentListContainer from '../deployment-list/DeploymentList.Container';

import './Page.scss';

interface PropsFromState {
  selectedProject: string | null;
}

interface PropsFromDispatch {}

type AllProps = PropsFromState & PropsFromDispatch;

class Page extends React.Component<AllProps> {
  render() {
    return (
      <Dashboard
        className="kata-deployment-overview-dashboard"
        title="Overview"
        tooltip="Every deployment you made will be recorded here."
      >
        <CreateDeploymentContainer />
        <DeploymentListContainer />
      </Dashboard>
    );
  }
}

const mapStateToProps = ({ project }: RootStore): PropsFromState => {
  return {
    selectedProject: project.selected
  };
};

const mapDispatchToProps = (dispatch: Function): PropsFromDispatch => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Page);
