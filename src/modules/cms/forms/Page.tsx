import React, { Fragment, SyntheticEvent } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Dispatch } from 'redux';

import Store from 'interfaces/rootStore';
import { Form as FormData } from 'interfaces/cms';
import { LoadingState } from 'interfaces/app/loadings';

import Card from 'components/Card';
import Sortable from 'components/Common/Sortable';
import withDragDropContext from 'components/Common/WithDragDropContext';
import { Dashboard } from 'components/Dashboard';
import { confirm } from 'components/Modal';
import { Tooltip, TooltipTarget } from 'components/Tooltip';
import { Button, FloatingButton } from 'components/Button';
import { Robot } from 'components/Loading';
import { EmptyMessage } from 'components/Common';

import FORM from 'stores/cms/forms/types';
import {
  fetchCmsFormsRequest,
  removeFormRequest,
  selectForm,
  swapForm
} from 'stores/cms/forms/actions';
import { getCmsFormsData, getCmsFormsIndex } from 'stores/cms/forms/selectors';
import { isDrawerOpen } from 'stores/app/drawer/selectors';
import { openDrawer } from 'stores/app/drawer/actions';
import { getLoading } from 'stores/app/loadings/selectors';

import NewFormContainer, { DRAWER_ID } from './NewFormContainer';
import EditFormContainer, {
  DRAWER_ID as EDIT_DRAWER_ID
} from './EditFormContainer';

import './Page.scss';
import ErrorBoundary from 'modules/core/ErrorBoundary';

interface PropsFromState {
  projectId: string | null;
  pageName: string;
  loadings: LoadingState;
  data: Record<string, FormData>;
  index: string[];
  newFormDrawerOpen: boolean;
  match?: any;
}

interface PropsFromDispatch {
  fetchCmsFormRequest: typeof fetchCmsFormsRequest;
  onSelectForm: (item: any) => ReturnType<typeof selectForm>;
  deleteData: typeof removeFormRequest;
  openDrawer: () => ReturnType<typeof openDrawer>;
  openEditDrawer: (data: FormData) => ReturnType<typeof openDrawer>;
  swapForm: typeof swapForm;
}

interface Props extends PropsFromState, PropsFromDispatch {}

class CmsDetailPage extends React.Component<Props> {
  openForm = (form: FormData, event: SyntheticEvent<any>) => {
    event.stopPropagation();
    // this.props.selectForm(form);
  };

  deleteData = (data: FormData) => {
    confirm({
      title: 'Delete Form',
      message: `Are you sure want to delete form ${data.name}?`,
      okLabel: 'Delete',
      cancelLabel: 'Cancel'
    }).then(res => {
      if (res) {
        this.props.deleteData(data);
      }
    });
  };

  editData = (data: FormData) => {
    this.props.openEditDrawer(data);
  };

  componentDidMount() {
    this.props.fetchCmsFormRequest(this.props.match.params.pageId);
  }

  renderData() {
    const { index, data, onSelectForm } = this.props;

    return index.map((p, i) => {
      const item = data[p];
      return (
        <Sortable key={i} index={i} id={p} onMove={this.onSortMove}>
          <Card
            key={p}
            className="kata-cms-forms-list-card mb-2"
            onClick={() => {
              onSelectForm(item);
            }}
          >
            <div className="kata-cms-forms-list-card__drag-icon">
              <i className="icon-drag" />
            </div>
            <div className="kata-cms-forms-list-card__avatar">
              <img
                src={require('assets/images/cms-forms-avatar.svg')}
                alt="Form Icon"
              />
            </div>
            <div className="kata-cms-forms-list-card__name kata-cms-forms-list-card__ellipsis">
              <span className="heading2">{item.name}</span>
            </div>
            <div className="kata-cms-forms-list-card__metadata">
              <div className="kata-cms-forms-list-card__metadata-item">
                <h5 className="text-label">Label</h5>
                <p className="paragraph">{item.label}</p>
              </div>
            </div>
            <div className="kata-cms-forms-list-card__buttons">
              {/* <Button
              color="white"
              onClick={e => {
                e.stopPropagation();
                this.editData(item);
              }}
            >
              <i className="icon-edit" /> <span>Update</span>
            </Button>{' '} */}
              <Button
                className="kata-btn--danger"
                color="white"
                onClick={e => {
                  e.stopPropagation();
                  this.deleteData(item);
                }}
              >
                <i className="icon-trash" /> <span>Delete</span>
              </Button>
            </div>
          </Card>
        </Sortable>
      );
    });
  }

  renderEmpty() {
    return (
      <EmptyMessage
        image={require('assets/images/pages-empty.svg')}
        title="No Forms"
      >
        This Form don’t have any form yet. Create Form by click the floating
        button in below.
      </EmptyMessage>
    );
  }

  render() {
    if (this.props.loadings.fetch) {
      return <Robot />;
    }
    return (
      <ErrorBoundary>
        <Dashboard
          className="kata-cms-forms-dashboard"
          title={'Forms'}
          tooltip="Form is where you build a workspace for users to add, edit, or remove contents on your chatbot."
          headerContent={
            <Fragment>
              <Link
                className="btn kata-btn-float kata-subpage__back-btn kata-btn-float__primary"
                to={`/project/${this.props.projectId}/cms/pages`}
              >
                <i className="icon-arrow-left" />
              </Link>
              <div className="kata-channel__header-wrapper">
                <div className="float-left kata-channel__header-left">
                  <h1 className="heading1 kata-subpage__title">
                    {this.props.pageName}
                  </h1>
                  <Link
                    to={`/project/${this.props.projectId}/cms/pages`}
                    className="text-muted"
                  >
                    Page
                  </Link>
                  <span className="kata-subpage__bread-separator">/</span>
                  <span className="text-primary">{this.props.pageName}</span>
                </div>
              </div>
            </Fragment>
          }
        >
          <TooltipTarget
            placement="top"
            component={<Tooltip>Create Form</Tooltip>}
          >
            <FloatingButton
              color="primary"
              icon="add"
              className="kata-floating-action"
              onClick={this.props.openDrawer}
            />
          </TooltipTarget>
          {this.props.index.length === 0
            ? this.renderEmpty()
            : this.renderData()}
          <NewFormContainer />
          <EditFormContainer />
        </Dashboard>
      </ErrorBoundary>
    );
  }

  private onSortMove = (a, b) => {
    this.props.swapForm(a, b);
  };
}

const mapStateToProps = (store: Store, props: Props) => ({
  projectId: store.project.selected,
  pageName: store.cms.pages.pageName,
  loadings: {
    fetch: getLoading(store.app.loadings, FORM.FETCH_REQUEST),
    save: getLoading(store.app.loadings, FORM.ADD_REQUEST),
    remove: getLoading(store.app.loadings, FORM.REMOVE_REQUEST),
    update: false
  } as LoadingState,
  data: getCmsFormsData(store.cms),
  index: getCmsFormsIndex(store.cms),
  newFormDrawerOpen: isDrawerOpen(store.app.drawer, DRAWER_ID)
});

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    fetchCmsFormRequest: (label: string) =>
      dispatch(fetchCmsFormsRequest(label)),
    openEditDrawer: (data: any) => dispatch(openDrawer(EDIT_DRAWER_ID, data)),
    openDrawer: () => dispatch(openDrawer(DRAWER_ID)),
    deleteData: (form: FormData) => dispatch(removeFormRequest(form)),
    onSelectForm: (item: any) => dispatch(selectForm(item)),
    swapForm: (first: number, second: number) =>
      dispatch(swapForm(first, second))
  };
};

const connectedFormsPage = connect(
  mapStateToProps,
  mapDispatchToProps
)(CmsDetailPage);

export default withDragDropContext(connectedFormsPage);
