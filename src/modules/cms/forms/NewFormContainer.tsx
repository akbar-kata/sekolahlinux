import React from 'react';
import { connect } from 'react-redux';

import Store from 'interfaces/rootStore';
import { Form as FormData } from 'interfaces/cms';

import FORM from 'stores/cms/forms/types';
import { addFormRequest } from 'stores/cms/forms/actions';
import { closeDrawer } from 'stores/app/drawer/actions';
import { isDrawerOpen } from 'stores/app/drawer/selectors';
import { getLoading } from 'stores/app/loadings/selectors';

import Form from './Form';

export const DRAWER_ID = 'drawer-new-form';

interface PropsFromState {
  loading: boolean;
  isOpen: boolean;
  forms: Record<string, FormData>;
}

interface PropsFromDispatch {
  closeDrawer(): void;
  saveNewForm(data: FormData);
}

interface Props extends PropsFromState, PropsFromDispatch {}

const NewFormContainer = (props: Props) => (
  <Form
    type="create"
    isOpen={props.isOpen}
    loading={props.loading}
    onCancel={props.closeDrawer}
    onSubmit={props.saveNewForm}
    data={props.forms}
  />
);

const mapStateToProps = (store: Store) => ({
  loading: getLoading(store.app.loadings, FORM.ADD_REQUEST),
  isOpen: isDrawerOpen(store.app.drawer, DRAWER_ID),
  forms: store.cms.forms.data
});

const mapDispatchToProps = {
  closeDrawer: () => closeDrawer(DRAWER_ID),
  saveNewForm: (data: FormData) => addFormRequest(data)
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NewFormContainer);
