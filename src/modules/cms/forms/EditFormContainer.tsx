import React from 'react';
import { connect } from 'react-redux';

import Store from 'interfaces/rootStore';
import { Form as FormData } from 'interfaces/cms';

import { closeDrawer } from 'stores/app/drawer/actions';
import { isDrawerOpen, getDrawerData } from 'stores/app/drawer/selectors';
import FORM from 'stores/cms/forms/types';
import { editFormRequest } from 'stores/cms/forms/actions';
import { getLoading } from 'stores/app/loadings/selectors';

import Form from './Form';

export const DRAWER_ID = 'drawer-edit-form';

interface PropsFromState {
  loading: boolean;
  isOpen: boolean;
  editData: any;
}

interface PropsFromDispatch {
  closeDrawer(): void;
  updateForm(data: any);
}

interface Props extends PropsFromState, PropsFromDispatch {}

const EditFormContainer = (props: Props) => (
  <Form
    type="update"
    data={props.editData}
    isOpen={props.isOpen}
    loading={props.loading}
    onCancel={props.closeDrawer}
    onSubmit={props.updateForm}
  />
);

const mapStateToProps = (store: Store): PropsFromState => ({
  loading: getLoading(store.app.loadings, FORM.EDIT_REQUEST),
  isOpen: isDrawerOpen(store.app.drawer, DRAWER_ID),
  editData: getDrawerData(store.app.drawer, DRAWER_ID)
});

const mapDispatchToProps: PropsFromDispatch = {
  closeDrawer: () => closeDrawer(DRAWER_ID),
  updateForm: (data: FormData) => editFormRequest(data)
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditFormContainer);
