import React from 'react';
import { Formik, Form } from 'formik';
import * as yup from 'yup';

import { Field } from 'components/FormikWrapper';
import { Button } from 'components/Button';
import {
  Drawer,
  DrawerBody,
  DrawerFooter,
  DrawerHeader
} from 'components/Drawer';

import { Form as FormData } from 'interfaces/cms';

interface Props {
  type: 'create' | 'update';
  loading: boolean;
  isOpen: boolean;
  data?: any;
  onCancel(): void;
  onSubmit(data: FormData);
}

class FormForm extends React.Component<Props> {
  validationSchema = yup.object().shape({
    name: yup
      .string()
      .required('Name is required')
      .max(20, 'Name only allowed 20 characters'),
    label: yup
      .string()
      .required('label is required')
      .max(20, 'Label only allowed 20 characters')
      .matches(/^[a-zA-Z\d]+$/, {
        message: 'Label may only contain a combination of strings or number.',
        excludeEmptyString: true
      })
      .test(
        'isLabelExists',
        'Label already exist, use another label',
        value => !this.isLabelExists(value)
      )
  });

  isLabelExists = (name: string) => {
    const { data } = this.props;
    const pages: any[] = Object.values(data);
    if (name === '') {
      return false;
    }

    const found = pages.filter(ele => ele.label === name);
    return found.length > 0;
  };

  innerForm = formApi => (
    <Form className="kata-form full-size">
      <DrawerHeader title={`${this.props.type} Form`} />
      <DrawerBody>
        <div className="kata-form__element">
          <label className="control-label kata-form__label">Name</label>
          <Field
            name="name"
            placeholder="Name"
            className="kata-form__input-text"
          />
        </div>
        <div className="mt4 mb-2">
          <label className="control-label kata-form__label">Label</label>
          <Field
            name="label"
            placeholder="Label"
            className="kata-form__input-text"
          />
        </div>
      </DrawerBody>
      <DrawerFooter>
        <Button
          className="mr-2 text-capitalize"
          color="primary"
          type="submit"
          disabled={this.props.loading}
          loading={this.props.loading}
        >
          {this.props.type}
        </Button>
        <Button onClick={this.props.onCancel}>Cancel</Button>
      </DrawerFooter>
    </Form>
  );

  render() {
    return (
      <Drawer isOpen={this.props.isOpen} onClose={this.props.onCancel}>
        <Formik
          onSubmit={this.props.onSubmit}
          initialValues={this.props.data}
          validationSchema={this.validationSchema}
        >
          {this.innerForm}
        </Formik>
      </Drawer>
    );
  }
}

export default FormForm;
