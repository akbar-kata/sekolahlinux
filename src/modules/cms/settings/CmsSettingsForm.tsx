import * as React from 'react';
import { connect } from 'react-redux';
import { Formik, Form } from 'formik';
import * as yup from 'yup';

import Store from 'interfaces/rootStore';
import { CmsData } from 'interfaces/cms';
import { LoadingState } from 'interfaces/app/loadings';

import CMS from 'stores/cms/types';
import Button from 'components/Button/Button';
import { Circle } from 'components/Loading';
import { Field } from 'components/FormikWrapper';
import { getLoading } from 'stores/app/loadings/selectors';

import { editCmsSettingsRequest } from 'stores/cms/actions';

interface PropsFromStore {
  loadings: LoadingState;
  selectedCmsData: any;
}

interface PropsFromDispatch {
  updateCms(cms: CmsData);
}

interface OwnProps {}

interface Props extends PropsFromStore, PropsFromDispatch, OwnProps {}

interface States {
  description: string;
  logo: string;
}

class CmsSettingsForm extends React.Component<Props, States> {
  state = {
    description: this.props.selectedCmsData
      ? this.props.selectedCmsData.description
      : '',
    logo: this.props.selectedCmsData ? this.props.selectedCmsData.logo : ''
  };

  validationSchema = yup.object().shape({
    description: yup.string().required('Description is required'),
    logo: yup.string()
  });

  static getDerivedStateFromProps(props: Props, state: States) {
    if (props.selectedCmsData.description !== state.description) {
      return {
        description: props.selectedCmsData.description,
        logo: props.selectedCmsData.logo
      };
    }
    return null;
  }

  innerForm = formApi => (
    <Form className="kata-form full-size">
      <div className="kata-info__container">
        <div className="kata-info__label">Description</div>
        <div className="kata-info__content">
          <Field
            name="description"
            component="textarea"
            rows={7}
            className="kata-form__input-textarea"
            placeholder="Description"
          />
        </div>
      </div>
      <div className="kata-info__container">
        <div className="kata-info__label">CMS Logo</div>
        <div className="kata-info__content">
          <Field
            name="logo"
            className="kata-form__input-text"
            placeholder="Logo URL e.g. https://mycdn.com/logo.png"
          />
        </div>
      </div>
      <div className="kata-form__element text-right">
        <Button
          type="submit"
          color={'primary'}
          loading={this.props.loadings.update}
          disabled={this.props.loadings.update}
        >
          Update
        </Button>
      </div>
    </Form>
  );

  onSubmit = data => {
    this.props.updateCms({
      botId: this.props.selectedCmsData.botId,
      settings: {
        description: data.description,
        logo: data.logo
      }
    });
  };

  render() {
    if (this.props.loadings.fetch) {
      return (
        <div className="kata-cms-settings__wrapper">
          <div className="kata-cms-settings__wrapper-inner">
            <div className="kata-cms-settings__loader">
              <Circle />
            </div>
          </div>
        </div>
      );
    }

    return (
      <div className="kata-cms-settings__wrapper">
        <div className="kata-cms-settings__wrapper-inner">
          <Formik
            initialValues={{
              description: this.props.selectedCmsData.description,
              logo: this.props.selectedCmsData.logo
            }}
            onSubmit={this.onSubmit}
            validationSchema={this.validationSchema}
          >
            {this.innerForm}
          </Formik>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (store: Store) => ({
  loadings: {
    fetch: getLoading(store.app.loadings, CMS.FETCH_REQUEST),
    update: getLoading(store.app.loadings, CMS.EDIT_REQUEST)
  },
  selectedCmsData: store.cms.settings
});

const mapDispatchToProps = {
  updateCms: (data: CmsData) => editCmsSettingsRequest(data)
};

export default connect<PropsFromStore, PropsFromDispatch, OwnProps, Store>(
  mapStateToProps,
  mapDispatchToProps
)(CmsSettingsForm);
