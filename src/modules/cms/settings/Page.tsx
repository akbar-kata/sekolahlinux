import * as React from 'react';
import { connect } from 'react-redux';

import Store from 'interfaces/rootStore';
import { LoadingState } from 'interfaces/app/loadings';

import Robot from 'components/Loading/Robot';
import Board from 'components/Board/Board';
import Dashboard from 'components/Dashboard/Dashboard';

import CMS from 'stores/cms/types';
import { getLoading } from 'stores/app/loadings/selectors';

import CmsSettingsForm from './CmsSettingsForm';
import './style.scss';
import ErrorBoundary from 'modules/core/ErrorBoundary';

interface PropsFromStore {
  loadings: LoadingState;
  selectedCmsData: any;
}

interface PropsFromDispatch {}

interface OwnProps {}

interface Props extends PropsFromStore, PropsFromDispatch, OwnProps {}

interface States {}

class Page extends React.Component<Props, States> {
  render() {
    if (this.props.loadings.fetch) {
      return <Robot />;
    }
    return (
      <div className="kata-cms-settings__container">
        <ErrorBoundary>
          <Dashboard
            title="Settings"
            tooltip="You can manage your CMS credentials here."
            isSettings
          >
            <Board>
              <CmsSettingsForm />
            </Board>
          </Dashboard>
        </ErrorBoundary>
      </div>
    );
  }
}

const mapStateToProps = (store: Store) => ({
  loadings: {
    fetch: getLoading(store.app.loadings, CMS.FETCH_REQUEST)
  },
  selectedCmsData: store.cms.detail
});

const mapDispatchToProps = {};

export default connect<PropsFromStore, PropsFromDispatch, OwnProps, Store>(
  mapStateToProps,
  mapDispatchToProps
)(Page);
