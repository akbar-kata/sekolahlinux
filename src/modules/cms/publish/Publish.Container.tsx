import React from 'react';
import { connect } from 'react-redux';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import { FormikActions } from 'formik';

import RootStore from 'interfaces/rootStore';

import CMS_PUBLISH from 'stores/cms/publish/types';
import { isModalOpen } from 'stores/app/modal/selectors';
import { publishCmsRequest } from 'stores/cms/publish/actions';
import { getProjectSelected } from 'stores/project/selectors';
import { closeModal, openModal } from 'stores/app/modal/actions';
import { getLoading } from 'stores/app/loadings/selectors';

import { isRootURL } from 'utils/url';

import Publish from './Publish';

interface PropsFromState {
  isOpen: boolean;
  selectedBot: string | null;
  isSaving: boolean;
}

interface PropsFromDispatch {
  openModal: () => ReturnType<typeof openModal>;
  closeModal: () => ReturnType<typeof closeModal>;
  publish: (id: string) => void;
}

export interface PublishContainerProps
  extends PropsFromState,
    PropsFromDispatch,
    RouteComponentProps<any> {}

export interface PublishContainerState {
  changelog: string;
}

export const MODAL_ID = 'cms-publish-revision';

class PublishContainer extends React.Component<
  PublishContainerProps,
  PublishContainerState
> {
  state = {
    changelog: ''
  };

  doPublish = (actions: FormikActions<any>) => {
    if (this.props.selectedBot) {
      this.props.publish(this.props.selectedBot);
    }
  };

  onPublish = () => {
    this.props.openModal();
  };

  render() {
    return (
      !isRootURL(this.props.location.pathname) && (
        <Publish
          onPublish={this.onPublish}
          doPublish={this.doPublish}
          {...this.props}
        />
      )
    );
  }
}

const mapStateToProps = ({ app, project }: RootStore): PropsFromState => {
  return {
    isOpen: isModalOpen(app.modal, MODAL_ID),
    selectedBot: getProjectSelected(project),
    isSaving: getLoading(app.loadings, CMS_PUBLISH.REQUEST)
  };
};

const mapDispatchToProps = (dispatch: Function): PropsFromDispatch => {
  return {
    openModal: () => dispatch(openModal(MODAL_ID)),
    closeModal: () => dispatch(closeModal(MODAL_ID)),
    publish: (id: string) => dispatch(publishCmsRequest(id))
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(PublishContainer)
);
