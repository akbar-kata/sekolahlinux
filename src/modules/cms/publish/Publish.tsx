import React, { Fragment } from 'react';

import { Formik, Form, FormikActions } from 'formik';

import { Button } from 'components/Button';
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'components/Modal';
// import { Field } from 'components/FormikWrapper';

import { PublishContainerProps } from './Publish.Container';

interface Props extends PublishContainerProps {
  onPublish: () => void;
  doPublish: (values: any, actions: FormikActions<any>) => void;
}

const Publish: React.SFC<Props> = props => (
  <Fragment>
    <Button color="primary" onClick={props.onPublish}>
      Publish
    </Button>

    <Modal show={props.isOpen} onClose={props.closeModal}>
      <Formik initialValues={{ changelog: '' }} onSubmit={props.doPublish}>
        {({ submitForm }) => (
          <Form>
            <ModalHeader onClose={props.closeModal}>
              Publish Revision
            </ModalHeader>
            <ModalBody>
              <div className="kata-info__container">
                <div className="kata-info__label">
                  You are publishing a new CMS revision, are you sure?
                </div>
              </div>
            </ModalBody>
            <ModalFooter>
              <Button
                color="primary"
                type="submit"
                loading={props.isSaving}
                onClick={submitForm}
              >
                Publish
              </Button>
              <Button
                color="secondary"
                type="button"
                onClick={props.closeModal}
              >
                Close
              </Button>
            </ModalFooter>
          </Form>
        )}
      </Formik>
    </Modal>
  </Fragment>
);

export default Publish;
