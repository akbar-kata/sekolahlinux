import React, { Fragment } from 'react';

import { Drawer, DrawerBody, DrawerHeader } from 'components/Drawer';
import moment from 'moment';

interface Props {
  isOpen: boolean;
  data: any;
  onClose(): void;
}

class ChannelDetail extends React.Component<Props> {
  render() {
    const { isOpen, data, onClose } = this.props;
    return (
      <Drawer isOpen={isOpen} onClose={onClose}>
        <DrawerHeader title={`View Revision List`} />
        <DrawerBody>
          {data && (
            <Fragment>
              {/* <div className="kata-revision__form-group">
                <div className="kata-info__container kata-revision__form-control mr-1">
                  <div className="kata-info__label">Revision</div>
                  <input
                    className="form-control kata-form__input-text"
                    disabled
                    value={data.revision}
                  />
                </div>
                <div className="kata-info__containe kata-revision__form-control ml-1">
                  <div className="kata-info__label">Date</div>
                  <input
                    className="form-control kata-form__input-text"
                    disabled
                    value={moment(data.created_at).format(
                      'DD MMM YYYY - HH:mm:ss'
                    )}
                  />
                </div>
              </div> */}
              <div className="kata-info__container">
                <div className="kata-info__label">Revision</div>
                <input
                  className="form-control kata-form__input-text"
                  disabled
                  value={data.revision}
                />
              </div>
              <div className="kata-info__container">
                <div className="kata-info__label">Date</div>
                <input
                  className="form-control kata-form__input-text"
                  disabled
                  value={moment(data.created_at).format(
                    'DD MMM YYYY - HH:mm:ss'
                  )}
                />
              </div>
              {/* <div className="kata-info__container">
                <div className="kata-info__label">Published by</div>
                <input
                  className="form-control kata-form__input-text"
                  disabled
                  value={data.name || '-'}
                />
              </div>
              <div className="kata-info__container">
                <div className="kata-info__label">Change log</div>
                <textarea
                  className="form-control kata-form__input-text"
                  disabled
                  value={data.changelog || '-'}
                />
              </div> */}
            </Fragment>
          )}
        </DrawerBody>
      </Drawer>
    );
  }
}

export default ChannelDetail;
