import React from 'react';
import moment from 'moment';

import { EmptyMessage } from 'components/Common';
import { CmsRevision } from 'interfaces/revision';
import IconicButton from 'components/Button/IconicButton';

interface Props {
  isLoading: boolean;
  error: string | null;
  index: string[];
  data: Record<string, CmsRevision>;
  openInfo: Function;
}

class RevisionList extends React.Component<Props, any> {
  renderMessage(child: React.ReactNode) {
    return (
      <tr>
        <td colSpan={6} className="text-center">
          {child}
        </td>
      </tr>
    );
  }

  renderLoading() {
    return this.renderMessage(<h5 className="p-4 text-bold">Loading...</h5>);
  }

  renderError() {
    return this.renderMessage(
      <h5 className="p-4 text-danger text-bold">{this.props.error}</h5>
    );
  }

  renderData() {
    const { index, data } = this.props;
    if (index.length === 0) {
      return this.renderMessage(
        <EmptyMessage title="Revision Lists is empty">
          This CMS don’t have any revision yet. Publish the CMS by clicking the
          publish button on top.
        </EmptyMessage>
      );
    }

    return index.map((revisionId, idx) => {
      const revision = data[revisionId];
      if (!revision) {
        return null;
      }
      return (
        <tr key={idx} onClick={() => this.props.openInfo(revision)}>
          <td>
            <samp>{revision.revision}</samp>
          </td>
          <td>
            {revision.created_at
              ? moment(revision.created_at).format('DD MMM YYYY - HH:mm:ss')
              : '-'}
          </td>
          {/* <td>{revision.name || '-'}</td> */}
          <td className="text-center">
            <div className="kata-version__detail-icon">
              <IconicButton
                onClick={e => {
                  e.stopPropagation();
                  this.props.openInfo(revision);
                }}
              >
                <i className="icon icon-view mr-1" />
                View
              </IconicButton>
            </div>
          </td>
        </tr>
      );
    });
  }

  renderTable() {
    if (this.props.isLoading) {
      return this.renderLoading();
    }

    return this.renderData();
  }

  render() {
    return (
      <div className="kata-revision__table">
        <table className="kata-table table-hover table-striped">
          <thead>
            <tr>
              <th>Revision</th>
              <th>Date</th>
              {/* <th>Published by</th> */}
              <th>Action</th>
            </tr>
          </thead>
          <tbody>{this.renderTable()}</tbody>
        </table>
      </div>
    );
  }
}

export default RevisionList;
