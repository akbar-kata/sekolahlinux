import React from 'react';

import { Dashboard } from 'components/Dashboard';

import CMSRevisionsList from './RevisionList.Container';

class CMSRevisions extends React.Component {
  render() {
    return (
      <Dashboard
        className="kata-cms-pages-dashboard"
        title="Revision List"
        tooltip="Every time you publish your CMS, it will be recorded here."
      >
        <CMSRevisionsList />
      </Dashboard>
    );
  }
}

export default CMSRevisions;
