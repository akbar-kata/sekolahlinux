import React, { Fragment } from 'react';
import { connect } from 'react-redux';

import RootStore from 'interfaces/rootStore';

import { getProjectSelected } from 'stores/project/selectors';
import { openDrawer, closeDrawer } from 'stores/app/drawer/actions';
import { fetchCmsRevisionsRequest } from 'stores/revision/actions';

import {
  getCmsRevisionsData,
  getCmsRevisionsIndex
} from 'stores/revision/cms/selectors';

import { CmsRevision } from 'interfaces/revision';

import RevisionList from './RevisionList';
import RevisionDetail from './RevisionDetail';
import { getLoading } from 'stores/app/loadings/selectors';

import REVISION from 'stores/revision/types';
import { getDrawerData, isDrawerOpen } from 'stores/app/drawer/selectors';
import ErrorBoundary from 'modules/core/ErrorBoundary';

interface PropsFromState {
  selectedProject: string | null;
  index: string[];
  data: Record<string, CmsRevision>;
  isLoading: boolean;
  dataInfo: any;
  isInfoOpen: boolean;
}

interface PropsFromDispatch {
  openInfo: Function;
  fetchRevisions: Function;
  closeInfo();
}

interface Props extends PropsFromDispatch, PropsFromState {}

interface State {}

const infoDrawerId = 'RevisionDetail';

class RevisionListContainer extends React.Component<Props, State> {
  componentDidMount() {
    if (this.props.selectedProject) {
      this.props.fetchRevisions(this.props.selectedProject);
    }
  }

  componentDidUpdate(prev: Props) {
    if (
      this.props.selectedProject &&
      prev.selectedProject !== this.props.selectedProject
    ) {
      this.props.fetchRevisions(this.props.selectedProject);
    }
  }

  render() {
    return (
      <Fragment>
        <RevisionDetail
          isOpen={this.props.isInfoOpen}
          data={this.props.dataInfo}
          onClose={this.props.closeInfo}
        />
        <ErrorBoundary>
          <RevisionList
            isLoading={this.props.isLoading}
            error={''}
            index={this.props.index}
            data={this.props.data}
            openInfo={this.props.openInfo}
          />
        </ErrorBoundary>
      </Fragment>
    );
  }
}

const mapStateToProps = ({
  revision,
  project,
  app: { loadings, drawer }
}: RootStore): PropsFromState => {
  const selectedProject = getProjectSelected(project) as string;
  return {
    selectedProject,
    index: getCmsRevisionsIndex(revision),
    data: getCmsRevisionsData(revision),
    isLoading: getLoading(loadings, REVISION.FETCH_CMS_REVISIONS_REQUEST),
    dataInfo: getDrawerData(drawer, infoDrawerId),
    isInfoOpen: isDrawerOpen(drawer, infoDrawerId)
  };
};

const mapDispatchToProps: PropsFromDispatch = {
  fetchRevisions: fetchCmsRevisionsRequest,
  openInfo: (data: any) => openDrawer(infoDrawerId, data),
  closeInfo: () => closeDrawer(infoDrawerId)
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RevisionListContainer);
