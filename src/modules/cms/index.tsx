import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';

import SelectedUrl from 'containers/SelectedUrl';
import { Robot } from 'components/Loading';

const FormEditor = React.lazy(() => import('./forms-editor'));
const Forms = React.lazy(() => import('./forms'));
const Pages = React.lazy(() => import('./pages'));
const Revisions = React.lazy(() => import('./revisions'));
const Users = React.lazy(() => import('./users'));
const Settings = React.lazy(() => import('./settings'));

export default () => {
  return (
    <SelectedUrl>
      {projectId => {
        const parentPath = `/project/${projectId}/cms`;
        return (
          <React.Suspense fallback={<Robot />}>
            <Switch>
              <Route
                path={`${parentPath}/pages/:pageId/forms/:formId/elements/:revision`}
                component={FormEditor}
              />
              <Route
                path={`${parentPath}/pages/:pageId/forms`}
                component={Forms}
              />
              <Route path={`${parentPath}/pages`} component={Pages} />
              <Route path={`${parentPath}/revisions`} component={Revisions} />
              <Route path={`${parentPath}/users`} component={Users} />
              <Route path={`${parentPath}/settings`} component={Settings} />
              <Route render={() => <Redirect to={`${parentPath}/pages`} />} />
            </Switch>
          </React.Suspense>
        );
      }}
    </SelectedUrl>
  );
};
