import React from 'react';
import { connect } from 'react-redux';

import RootStore from 'interfaces/rootStore';
import { fetchEnvRequest } from 'stores/deployment/environment/actions';
import {
  getEnvironmentData,
  getEnvironmentError,
  getEnvironmentLoading
} from 'stores/deployment/environment/selectors';

interface PropsFromState {
  error: string | null;
  loading: boolean;
  options: any[];
}

interface PropsFromDispatch {
  reqEnvironment: Function;
}

interface Props extends PropsFromState, PropsFromDispatch {
  children: (props: any) => React.ReactElement<any>;
}

interface State {}

class EnvironmentSelector extends React.Component<Props, State> {
  componentWillMount() {
    this.props.reqEnvironment();
  }

  render() {
    const { children, ...rest } = this.props;
    return children(rest);
  }
}
const mapStateToProps = ({
  deployment: { environment }
}: RootStore): PropsFromState => {
  const data = getEnvironmentData(environment);

  return {
    loading: getEnvironmentLoading(environment, 'fetch'),
    error: getEnvironmentError(environment, 'fetch'),
    options: Object.values(data).map(env => ({
      label: `${env.name}-${env.slug}-${env.depVersion}`,
      value: env.slug,
      envName: env.name
    }))
  };
};

const mapDispatchToProps = (dispatch: Function): PropsFromDispatch => {
  return {
    reqEnvironment: () => dispatch(fetchEnvRequest())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EnvironmentSelector);
