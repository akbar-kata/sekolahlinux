import React from 'react';
import { connect } from 'react-redux';
import { push } from 'connected-react-router';

import Store from 'interfaces/rootStore';
import { UserData } from 'interfaces/cms';
import { LoadingState } from 'interfaces/app/loadings';

import * as CommonAction from 'stores/common/actions';
import { isDrawerOpen } from 'stores/app/drawer/selectors';

import Robot from 'components/Loading/Robot';
import { confirm } from 'components/Modal';
import { Button } from 'components/Button';
import { Dashboard } from 'components/Dashboard';

import CMS_USER from 'stores/cms/users/types';
import { openDrawer } from 'stores/app/drawer/actions';
import { getLoading } from 'stores/app/loadings/selectors';

import { default as NewUserForm, DRAWER_ID } from './NewUserForm';

import ErrorBoundary from 'modules/core/ErrorBoundary';

interface PropsFromState {
  loadings: LoadingState;
  selectedCms: string;
  userIndexes: string[];
  userData: Record<string, UserData>;
  newUserDrawerOpen: boolean;
}

interface PropsFromDispatch {
  push: typeof push;
  loadUsers: () => any;
  deleteUser: (user: UserData) => any;
  openDrawer: () => ReturnType<typeof openDrawer>;
}

interface Props extends PropsFromState, PropsFromDispatch {}

class CmsDetailPage extends React.Component<Props> {
  deleteUser = (user: UserData) => {
    confirm({
      title: 'Remove User',
      message: `Are you sure want to remove ${user.email} from current CMS?`,
      okLabel: 'Remove',
      cancelLabel: 'Cancel'
    }).then(res => {
      if (res) {
        this.props.deleteUser(user);
      }
    });
  };

  componentDidMount() {
    this.props.loadUsers();
  }

  renderUsers() {
    if (!this.props.userIndexes || this.props.userIndexes.length === 0) {
      return (
        <tr>
          <td colSpan={5} className="text-center">
            <div className="text-center">
              <div className="empty-message">
                <img
                  src={require('assets/images/form-empty.svg')}
                  alt="Empty Users"
                />
                <div className="empty-message__title">User is empty</div>
                <div className="empty-message__description">
                  You don’t have any user. Add user by click the button on top.
                </div>
              </div>
            </div>
          </td>
        </tr>
      );
    }

    return this.props.userIndexes.map((index, mapIndex) => {
      return (
        <tr key={`${index}_${mapIndex}`}>
          <td>{mapIndex + 1}</td>
          <td>{this.props.userData[index].username}</td>
          <td>{this.props.userData[index].email}</td>
          <td>{this.props.userData[index].namespace}</td>
          <td>{this.props.userData[index].environment}</td>
          {/* <td className="text-center">
            <IconicButton
              onClick={e => {
                e.stopPropagation();
                this.deleteUser(this.props.userData[index]);
              }}
            >
              <span className="text-danger">
                <i className="icon-trash mr-1" />
                Delete
              </span>
            </IconicButton>
          </td> */}
        </tr>
      );
    });
  }

  render() {
    if (this.props.loadings.fetch) {
      return <Robot />;
    }
    return (
      <ErrorBoundary>
        <Dashboard
          title="Users"
          tooltip="You can manage who can access your CMS here."
        >
          <Button
            className="kata-cms-user__add-btn"
            color="primary"
            onClick={this.props.openDrawer}
          >
            <span className="icon-add pr-1" />
            Add User
          </Button>
          <div className="kata-cms-user__table">
            <table className="kata-table kata-table--striped kata-table--hover">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Namespace</th>
                  <th>Environment</th>
                  {/* <th style={{ width: 100 }} className="text-center">
                  Actions
                </th> */}
                </tr>
              </thead>
              <tbody>{this.renderUsers()}</tbody>
            </table>
          </div>
          <NewUserForm routerPush={this.props.push} />
        </Dashboard>
      </ErrorBoundary>
    );
  }
}

const mapStateToProps = (store: Store) => ({
  loadings: {
    fetch: getLoading(store.app.loadings, CMS_USER.FETCH_DATA),
    save: getLoading(store.app.loadings, CMS_USER.SAVE_USER),
    remove: getLoading(store.app.loadings, CMS_USER.DELETE_DATA),
    update: false
  } as LoadingState,
  selectedCms: 'null',
  userIndexes: store.cms.users.index,
  userData: store.cms.users.data,
  newUserDrawerOpen: isDrawerOpen(store.app.drawer, DRAWER_ID)
});

const mapDispatchToProps = {
  push,
  loadUsers: () => CommonAction.request(CMS_USER.FETCH_DATA),
  deleteUser: (user: UserData) =>
    CommonAction.request(CMS_USER.DELETE_DATA, user.userId),
  openDrawer: () => openDrawer(DRAWER_ID)
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CmsDetailPage);
