import React from 'react';
import { connect } from 'react-redux';
import { Formik, Form } from 'formik';
import { push } from 'connected-react-router';
import * as yup from 'yup';

import Store from 'interfaces/rootStore';
import { UserData } from 'interfaces/cms';

import CMS_USER from 'stores/cms/users/types';
import * as CommonAction from 'stores/common/actions';
import { closeDrawer } from 'stores/app/drawer/actions';
import { isDrawerOpen } from 'stores/app/drawer/selectors';
import { getLoading } from 'stores/app/loadings/selectors';

import { Button } from 'components/Button';
import { Field, ReactSelect } from 'components/FormikWrapper';
import {
  Drawer,
  DrawerBody,
  DrawerFooter,
  DrawerHeader
} from 'components/Drawer';

import SelectedUrl from 'containers/SelectedUrl';

import EnvironmentSelector from './EnvironmentSelector';

export const DRAWER_ID = 'drawer-new-user';

interface PropsFromState {
  loading: boolean;
  newUserFormDrawerOpen: boolean;
}

interface PropsFromDispatch {
  closeDrawer: () => ReturnType<typeof closeDrawer>;
  saveNewUser: (data: UserData) => any;
}

interface Props extends PropsFromState, PropsFromDispatch {
  routerPush: typeof push;
}

class NewUserForm extends React.Component<Props> {
  validationSchema = yup.object().shape({
    email: yup
      .string()
      .required('Email is required')
      .email('Email format is not valid'),
    slug: yup.string().required('Environment is required')
  });

  innerForm = formApi => (
    <Form className="kata-form full-size">
      <DrawerHeader title="Add User" />
      <DrawerBody>
        <div className="kata-form__element">
          <label htmlFor="name" className="control-label kata-form__label">
            Email
          </label>
          <Field
            name="email"
            className="kata-form__input-text"
            placeholder="Email"
            type="email"
          />
        </div>
        <div className="kata-form__element">
          <label htmlFor="name" className="control-label kata-form__label">
            Environment
          </label>
          <EnvironmentSelector>
            {params => {
              return params.loading ? (
                <div>Loading...</div>
              ) : !params.options.length ? (
                <div>
                  <SelectedUrl>
                    {projectId => {
                      const parentPath = `/project/${projectId}/deployment`;
                      return (
                        <Button
                          color="primary"
                          block
                          onClick={() =>
                            this.props.routerPush(`${parentPath}/environment`)
                          }
                        >
                          Go to Environment
                        </Button>
                      );
                    }}
                  </SelectedUrl>
                </div>
              ) : (
                <ReactSelect
                  name="slug"
                  clearable={false}
                  options={params.options || []}
                  onChange={val => {
                    formApi.setFieldValue('slug', val.value);
                    formApi.setFieldValue('namespace', val.value);
                    formApi.setFieldValue('environmentName', val.envName);
                  }}
                />
              );
            }}
          </EnvironmentSelector>
        </div>
      </DrawerBody>
      <DrawerFooter>
        <Button
          className="mr-1"
          color="primary"
          type="submit"
          disabled={this.props.loading}
          loading={this.props.loading}
        >
          Add
        </Button>
        <Button onClick={this.props.closeDrawer}>Cancel</Button>
      </DrawerFooter>
    </Form>
  );

  onSubmit = data => {
    this.props.saveNewUser(data);
  };

  render() {
    return (
      <Drawer
        isOpen={this.props.newUserFormDrawerOpen}
        onClose={this.props.closeDrawer}
      >
        <Formik
          onSubmit={this.onSubmit}
          initialValues={null}
          validationSchema={this.validationSchema}
        >
          {this.innerForm}
        </Formik>
      </Drawer>
    );
  }
}

const mapStateToProps = (store: Store) => ({
  loading: getLoading(store.app.loadings, CMS_USER.SAVE_USER),
  newUserFormDrawerOpen: isDrawerOpen(store.app.drawer, DRAWER_ID)
});
const mapDispatchToProps = {
  closeDrawer: () => closeDrawer(DRAWER_ID),
  saveNewUser: (data: UserData) =>
    CommonAction.request(CMS_USER.SAVE_USER, data)
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NewUserForm);
