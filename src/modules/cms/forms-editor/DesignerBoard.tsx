import React from 'react';
import { ConnectDropTarget, DropTarget, DropTargetMonitor } from 'react-dnd';

import { ElementMetadata } from 'interfaces/cms';

import Sortable from 'components/Common/Sortable';

import { DRAG_TYPE } from './elements/CmsWidget';
import FaqElement from './elements/FaqElement';
import TextBoxElement from './elements/TextBoxElement';
import TextAreaElement from './elements/TextAreaElement';
import UnknownElement from './elements/UnknownElement';
import TextListElement from './elements/TextListElement';
import DropdownElement from './elements/DropdownElement';
import ToggleElement from './elements/ToggleElement';
import SliderElement from './elements/SliderElement';
import LocationElement from './elements/LocationElement';
import LocatorElement from './elements/LocatorElement';
import ImageUploadElement from './elements/ImageUploadElement';
import ProductTableElement from './elements/table/product/ProductTableElement';

interface DndProps {
  connectDropTarget?: ConnectDropTarget;
}

interface Props extends DndProps {
  elements: ElementMetadata[];
  addElement(data: ElementMetadata);
  onSortMove(start: number, end: number);
  removeElement(index: number);
}

interface States {}

class DesignerBoard extends React.Component<Props & DndProps, States> {
  getElement(type: string) {
    switch (type) {
      case 'faq':
        return FaqElement;
      case 'text':
        return TextBoxElement;
      case 'textarea':
        return TextAreaElement;
      case 'textlist':
        return TextListElement;
      case 'dropdown':
        return DropdownElement;
      case 'slider':
        return SliderElement;
      case 'toggle':
        return ToggleElement;
      case 'location':
        return LocationElement;
      case 'locator':
        return LocatorElement;
      case 'images':
        return ImageUploadElement;
      case 'product':
        return ProductTableElement;
      default:
        return UnknownElement;
    }
  }

  render() {
    return this.props.connectDropTarget ? (
      this.props.connectDropTarget(
        <div>
          <div className="kata-form-editor__designer">
            {this.renderElements()}
          </div>
        </div>
      )
    ) : (
      <div>
        <div className="kata-form-editor__designer">
          {this.renderElements()}
        </div>
      </div>
    );
  }

  renderElements() {
    if (!this.props.elements || this.props.elements.length === 0) {
      return (
        <div className="text-center">
          <div className="empty-message">
            <img src={require('assets/images/form-empty.svg')} alt="No Field" />
            <div className="empty-message__title">No Field</div>
            <div className="empty-message__description">
              You don't have any field in this form yet. Add a field by dragging
              from the left.
            </div>
          </div>
        </div>
      );
    }

    return this.props.elements.map((el, index) => {
      const Element: any = this.getElement(el.meta ? el.meta.type : 'unknown');
      return (
        Element && (
          <Sortable
            key={index}
            index={index}
            id={el.id || el.tempId || index}
            onMove={this.props.onSortMove}
          >
            <Element
              id={el.id}
              meta={el.meta}
              key={index}
              index={index}
              showPreview
              showOptions
              onRemove={() => this.props.removeElement(index)}
            />
          </Sortable>
        )
      );
    });
  }
}

export default DropTarget<Props>(
  DRAG_TYPE,
  {
    canDrop() {
      return true;
    },
    drop(props: Props, mon: DropTargetMonitor) {
      props.addElement(mon.getItem() as ElementMetadata);
    }
  },
  (con, mon) => ({
    connectDropTarget: con.dropTarget(),
    item: mon.getItem()
  })
)(DesignerBoard);
