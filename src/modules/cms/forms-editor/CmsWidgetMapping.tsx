import React, { Component } from 'react';

import CmsWidget from 'modules/cms/forms-editor/elements/CmsWidget';
import CmsElementDefinitions from 'modules/cms/forms-editor/CmsElementDefinitions';
import { ElementDefinition } from 'interfaces/cms';
import * as env from 'utils/env';

interface Props {
  addElement: Function;
}

class CmsWidgetMapping extends Component<Props> {
  /**
   * CAUTION: THIS METHOD NEED TO REVISIT WHEN COMPONENT ADD NEW PROP / STATE
   */
  shouldComponentUpdate() {
    return false;
  }

  render() {
    return (
      <div className="kata-form-editor__widgets">
        <div className="kata-form-editor__widget-header">Elements</div>
        <div className="kata-cms-widget-container">
          <div className="kata-form-editor__widgets-text-single">Single</div>
          {CmsElementDefinitions.map(
            (elementDef: ElementDefinition, i: number) =>
              elementDef.element === 'single' && (
                <CmsWidget
                  key={i}
                  title={elementDef.title}
                  type={elementDef.type}
                  definition={elementDef}
                  onClick={() => {
                    this.props.addElement(elementDef);
                  }}
                />
              )
          )}

          {env.getRuntimeEnv('REACT_APP_RUNTIME_CMS_NEW_ELEMENTS') ===
            'true' && (
            <>
              <div className="kata-form-editor__widgets-text-single pt-1">
                Table
              </div>
              {CmsElementDefinitions.map(
                (meta: any, i: number) =>
                  meta.element === 'table' && (
                    <CmsWidget
                      key={i}
                      {...meta}
                      onClick={() => {
                        this.props.addElement(meta);
                      }}
                    />
                  )
              )}
            </>
          )}
        </div>
      </div>
    );
  }
}

export default CmsWidgetMapping;
