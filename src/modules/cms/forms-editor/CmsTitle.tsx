import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import { Form as FormData } from 'interfaces/cms';

import Button from 'components/Button/Button';

interface Props {
  overflow: boolean;
  isValid: boolean;
  loading: boolean;
  selectedFormData?: FormData;
}

class CmsTitle extends Component<Props> {
  /**
   * CAUTION: THIS METHOD NEED TO REVISIT WHEN COMPONENT ADD NEW PROP / STATE
   */
  shouldComponentUpdate(nextProps: Props) {
    const { overflow, isValid, loading, selectedFormData } = this.props;
    return (
      overflow !== nextProps.overflow ||
      isValid !== nextProps.isValid ||
      loading !== nextProps.loading ||
      selectedFormData !== nextProps.selectedFormData
    );
  }

  render() {
    return (
      <div
        className={`kata-form-editor__designer-header ${
          this.props.overflow
            ? 'kata-form-editor__designer-header--overflow'
            : ''
        }`}
      >
        <Link
          className="btn kata-btn-float kata-btn-float__primary kata-subpage__back-btn"
          to={`../forms`}
        >
          <i className="icon-arrow-left" />
        </Link>
        <div className="float-left">
          <h1 className="heading1 kata-subpage__title">
            {this.props.selectedFormData
              ? this.props.selectedFormData.name
              : '...'}
          </h1>
          <Link to={`../forms`} className="text-muted">
            CMS Forms
          </Link>
          <span className="kata-subpage__bread-separator">/</span>
          <span className="text-primary">
            {this.props.selectedFormData
              ? this.props.selectedFormData.name
              : '...'}
          </span>
        </div>
        <div className="ml-auto">
          <Button
            type="submit"
            color="primary"
            disabled={!this.props.isValid}
            loading={this.props.loading}
          >
            Save
          </Button>
        </div>
      </div>
    );
  }
}

export default CmsTitle;
