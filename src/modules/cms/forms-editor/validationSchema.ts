import * as yup from 'yup';
import get from 'lodash-es/get';

function validateDefault(values: any, noLabel?: boolean) {
  return yup.object().shape({
    label: yup.lazy(label => {
      return noLabel ? yup.mixed() : yup.string();
    })
  });
}

function validateDropdown(values: any, noLabel?: boolean) {
  return yup.object().shape({
    // label: yup.lazy(label => {
    //   return noLabel ? yup.mixed() : yup.string().required('Label is required');
    // }),
    options: yup
      .array(
        yup.object().shape({
          label: yup.string().required('Label is required'),
          value: yup.string().required('Value is required')
        })
      )
      .min(1, 'Options is required')
      .required('Options is required')
  });
}

function validateSlider(values: any, noLabel: boolean) {
  return yup.object().shape({
    // label: yup.lazy(label => {
    //   return noLabel ? yup.mixed() : yup.string().required('Label is required');
    // }),
    min: yup
      .number()
      .min(0, 'Minimum value is 0')
      .max(values.max, `Minimum value can't exceed maximum value`)
      .required('Minimum value is required'),
    max: yup
      .number()
      .min(values.min, `Minimum value can't be lower than minimum value`)
      .required('Maximum value is required'),
    step: yup
      .number()
      .min(1, `Scale value can't be lower than 1`)
      .max(values.max, `Scale value can't exceed maximum value`)
      .required('Scale value is required'),
    value: yup
      .number()
      .min(values.min, `Default Value can't be lower than minimum value`)
      .max(values.max, `Default value can't exceed maximum value`)
      .required('Default value is required')
  });
}

function validateProduct(values: any) {
  const isNameExist = name => {
    if (name === '') {
      return false;
    }
    const cols = [...get(values, 'columns', []), ...get(values, 'options', [])];
    const found = cols.filter(col => col.name === name);
    return found.length > 1;
  };

  const colSchema = yup.array(
    yup.lazy((col: any) =>
      yup.object().shape({
        label: yup.string().required('Label is required'),
        type: yup.string().required('Type must be selected'),
        name: yup
          .string()
          .required('Name is required')
          .matches(/^[A-Za-z][-A-Za-z0-9_]*$/, {
            message: 'Name only allowed charaters alphanumeric and _',
            excludeEmptyString: true
          })
          .test(
            'isNameExists',
            'Name already exist, use another name',
            value => !isNameExist(value)
          ),
        metadata: yup.lazy(metadata => {
          const metaSchema = schemaMap[col.type] || schemaMap['default'];
          return metaSchema(metadata, true);
        })
      })
    )
  );

  return yup.object().shape({
    label: yup.string().required('Label is required'),
    columns: colSchema,
    options: colSchema
  });
}

const schemaMap = {
  dropdown: validateDropdown,
  slider: validateSlider,
  product: validateProduct,
  default: validateDefault
};

const validationSchema = yup.lazy((values: any) => {
  const isLabelExist = (elements: any[], label: string): boolean => {
    if (label === '') {
      return false;
    }
    const found = elements.filter(ele => ele.label === label);
    return found.length > 1;
  };

  return yup.object().shape({
    elements: yup.array(
      yup.object().shape({
        label: yup
          .string()
          .required('ID is required')
          .max(20, 'ID only allowed 20 characters')
          .matches(/^[A-Za-z\d]+$/, {
            message: 'ID may only contain a combination of strings or number.',
            excludeEmptyString: true
          })
          .test(
            'isNameExists',
            'ID already exist, use another id',
            value => !isLabelExist(values.elements, value)
          ),
        name: yup
          .string()
          .required('Label is required')
          .max(20, 'Name only allowed 20 characters'),
        meta: yup.lazy((meta: any) => {
          const schema = schemaMap[meta.type] || schemaMap['default'];
          return schema(meta);
        })
      })
    )
  });
});

export default validationSchema;
