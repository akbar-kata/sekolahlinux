import React from 'react';
import { DragSource, ConnectDragSource, DragSourceMonitor } from 'react-dnd';

import './CmsWidget.scss';
import { ElementDefinition } from 'interfaces/cms';

interface DndProps {
  connectDragSource?: ConnectDragSource;
}

interface Props extends DndProps {
  title: string;
  type: string;
  definition: ElementDefinition;
  onClick?(meta: ElementDefinition);
  onMove?(data: any);
}

interface States {}

export const DRAG_TYPE = 'CmsWidget';

class CmsWidget extends React.Component<Props & DndProps, States> {
  handleClick = () => {
    if (this.props.onClick) {
      this.props.onClick(this.props.definition);
    }
  };
  render() {
    return this.props.connectDragSource ? (
      this.props.connectDragSource(
        <div className="kata-cms-widget" onClick={this.handleClick}>
          <div className="kata-cms-widget__icon">
            <i className={`icon-drag`} />
          </div>
          <div className="kata-cms-widget__title">{this.props.title}</div>
        </div>
      )
    ) : (
      <div className="kata-cms-widget" onClick={this.handleClick}>
        <div className="kata-cms-widget__icon">
          <i className={`icon-drag`} />
        </div>
        <div className="kata-cms-widget__title">{this.props.title}</div>
      </div>
    );
  }
}

export default DragSource<Props>(
  DRAG_TYPE,
  {
    canDrag(props: Props) {
      // You can disallow drag based on props
      return true;
    },
    beginDrag(props: Props) {
      // Return the data describing the dragged item
      return props.definition;
    },
    endDrag(props: Props, mon: DragSourceMonitor) {
      if (props.onMove) {
        props.onMove(mon.getItem());
      }
    }
  },
  (con, mon) => {
    return {
      // Call this function inside render()
      // to let React DnD handle the drag events:
      connectDragSource: con.dragSource(),
      // You can ask the monitor about the current drag state:
      isDragging: mon.isDragging()
    };
  }
)(CmsWidget);
