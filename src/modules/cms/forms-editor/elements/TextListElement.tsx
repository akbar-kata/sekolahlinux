import React from 'react';

import CmsElement, { Props as ElementProps } from './CmsElement';

interface Props extends ElementProps {}
export default class TextListElement extends React.Component<Props> {
  render() {
    return (
      <CmsElement
        {...this.props}
        renderOptions={() => (
          <React.Fragment>
            <div className="">No Options Available</div>
          </React.Fragment>
        )}
        renderPreview={() => (
          <React.Fragment>
            <div className="kata-form__element">
              <input
                type="text"
                disabled
                className="kata-form__input-text kata-form__input-text__line"
                value="1. Text 1"
              />
            </div>
            <div className="kata-form__element">
              <input
                type="text"
                disabled
                className="kata-form__input-text kata-form__input-text__line"
                value="2. Text 2"
              />
            </div>
            <div className="kata-form__element">
              <input
                type="text"
                disabled
                className="kata-form__input-text kata-form__input-text__line"
                value="3. Text 3"
              />
            </div>
          </React.Fragment>
        )}
      />
    );
  }
}
