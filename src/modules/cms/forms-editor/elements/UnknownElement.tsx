import React from 'react';

import CmsElement, { Props as ElementProps } from './CmsElement';

interface Props extends ElementProps {}

export default class UnknownElement extends React.Component<Props> {
  render() {
    return (
      <CmsElement
        {...this.props}
        renderOptions={() => <React.Fragment>-</React.Fragment>}
        renderPreview={() => (
          <React.Fragment>
            Unknown Element - We can not handle this element, you might want to
            delete it instead.
          </React.Fragment>
        )}
      />
    );
  }
}
