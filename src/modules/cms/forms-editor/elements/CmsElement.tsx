import React from 'react';
import classNames from 'classnames';

import './CmsElement.scss';

import { Field, ReactToggle } from 'components/FormikWrapper';
import FloatingButton from 'components/Button/FloatingButton';

import { confirm } from 'components/Modal';

export interface Props {
  fieldName?: string;
  index: number;
  id: string;
  showOptions?: boolean;
  showPreview?: boolean;
  showHeader?: boolean;
  noRequired?: boolean;
  renderOptions(): any;
  renderPreview(): any;
  onClick?();
  onRemove();
}

interface States {}

export const DRAG_TYPE = 'CmsElement';

/**
 * This class should not be used directly and is intended to be wrapped.
 */
export default class CmsElement extends React.Component<Props, States> {
  static defaultProps = {
    showHeader: true
  };

  deleteElement = () => {
    confirm({
      title: 'Delete Element',
      message: `Are you sure you want to delete element?`,
      okLabel: 'Delete',
      cancelLabel: 'Cancel'
    }).then(res => {
      if (res) {
        this.props.onRemove();
      }
    });
  };

  renderOptions() {
    if (this.props.showOptions) {
      return (
        <div className="kata-cms-element__options">
          {this.props.renderOptions()}
        </div>
      );
    }
    return null;
  }

  renderPreview() {
    if (this.props.showPreview) {
      return (
        <div className="kata-cms-element__preview">
          {this.props.renderPreview()}
        </div>
      );
    }
    return null;
  }

  renderHeader() {
    if (!this.props.showHeader) {
      return null;
    }
    return (
      <div className="kata-cms-element__header">
        <div className="kata-cms-element__header-handle">
          <i className="icon-drag" />
        </div>
        <div className={`kata-cms-element__header-label`}>
          <Field
            name={`elements.${this.props.index}.name`}
            className="kata-form__input-text"
            placeholder="Label here..."
          />
        </div>
        <div className={`kata-cms-element__header-label`}>
          <Field
            name={`elements.${this.props.index}.label`}
            className="kata-form__input-text"
            placeholder="ID"
            disabled={this.props.id ? true : false}
          />
        </div>
        {!this.props.noRequired ? (
          <div className="kata-cms-element__header-optional">
            <ReactToggle
              name={`elements.${this.props.index}.meta.required`}
              label="Required"
            />
          </div>
        ) : null}
        <div className="kata-cms-element__header-actions">
          <FloatingButton
            color="danger"
            className="mr-1"
            icon="trash"
            onClick={this.deleteElement}
          />
        </div>
      </div>
    );
  }

  render() {
    return (
      <div
        className={classNames({
          'kata-cms-element': true,
          'kata-cms-element--no-header': !this.props.showHeader
        })}
      >
        {this.renderHeader()}
        {this.renderPreview()}
      </div>
    );
  }
}
