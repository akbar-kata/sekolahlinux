import React from 'react';

import { Field, ReactToggle } from 'components/FormikWrapper';
import CmsElement, { Props as ElementProps } from './CmsElement';

interface Props extends ElementProps {}

export default class ToggleElement extends React.Component<Props> {
  render() {
    const fieldName =
      this.props.fieldName || `elements.${this.props.index}.meta`;
    return (
      <CmsElement
        {...this.props}
        noRequired
        renderOptions={() => <div className="">No Options Available</div>}
        renderPreview={() => (
          <div className="p-2">
            <div className="kata-form__element kata-cms-element--center">
              <ReactToggle name={`${fieldName}.value`} />
              <Field
                className="kata-form__input-text ml-2"
                style={{ width: 160 }}
                name={`${fieldName}.toggleLabel`}
              />
            </div>
          </div>
        )}
      />
    );
  }
}
