import React from 'react';
import classNames from 'classnames';

import { Field, ReactSelect, ReactToggle } from 'components/FormikWrapper';
import { Button } from 'components/Button';
import ToggleButton from 'components/Button/ToggleButton';

import TextAreaElement from '../../TextAreaElement';
import UnknownElement from '../../UnknownElement';
import TextListElement from '../../TextListElement';
import DropdownElement from '../../DropdownElement';
import ToggleElement from '../../ToggleElement';
import SliderElement from '../../SliderElement';
import LocationElement from '../../LocationElement';
import LocatorElement from '../../LocatorElement';
import ImageUploadElement from '../../ImageUploadElement';
import TextBoxElement from '../../TextBoxElement';

interface States {
  active: boolean;
}
interface Props {
  fieldName: string;
  index: number;
  type: string;
  editable?: boolean;
  onTypeChange?: (name: string, value: string) => void;
}

export default class ProductTableColumn extends React.Component<Props, States> {
  static defaultProps = {
    editable: false
  };

  state = {
    active: true
  };

  handleToggle = (active: boolean) => {
    this.setState({
      active: !active
    });
  };

  getElement(type: string) {
    switch (type) {
      case 'text':
        return TextBoxElement;
      case 'textarea':
        return TextAreaElement;
      case 'textlist':
        return TextListElement;
      case 'dropdown':
        return DropdownElement;
      case 'slider':
        return SliderElement;
      case 'toggle':
        return ToggleElement;
      case 'location':
        return LocationElement;
      case 'locator':
        return LocatorElement;
      case 'images':
        return ImageUploadElement;
      default:
        return UnknownElement;
    }
  }

  renderBody() {
    if (this.state.active) {
      const Element: any = this.getElement(this.props.type);
      return (
        <div className="kata-product-table__column-body">
          {this.props.editable && (
            <div className="kata-form__element p-2 pb-0 mb-0">
              <label className="control-label kata-form__label">
                Column Width
              </label>
              <Field
                name={`${this.props.fieldName}.${this.props.index}.width`}
                type="number"
                className="kata-form__input-text"
                disabled={!this.props.editable}
                placeholder="Width"
              />
            </div>
          )}
          <Element
            fieldName={`${this.props.fieldName}.${this.props.index}.metadata`}
            showPreview
            showOptions
            showHeader={false}
          />
        </div>
      );
    }
    return null;
  }

  render() {
    const { fieldName, index } = this.props;
    return (
      <div
        className={classNames({
          'kata-product-table__column': true,
          'kata-product-table__column--active': this.state.active
        })}
      >
        <div className="kata-product-table__column-header">
          <div className="kata-product-table__column-header-field">
            <Field
              name={`${fieldName}.${index}.name`}
              className="kata-form__input-text"
              disabled={!this.props.editable}
              placeholder="Name..."
            />
          </div>
          {this.props.editable ? (
            <div className="kata-product-table__column-header-field">
              <Field
                name={`${fieldName}.${index}.label`}
                className="kata-form__input-text"
                disabled={!this.props.editable}
                placeholder="Label..."
              />
            </div>
          ) : null}
          <div className="kata-product-table__column-header-field">
            <ReactSelect
              name={`${fieldName}.${index}.type`}
              clearable={false}
              disabled={!this.props.editable}
              options={[
                { value: 'text', label: 'Text' },
                { value: 'textarea', label: 'Text Area' },
                { value: 'textlist', label: 'Text List' },
                { value: 'dropdown', label: 'Dropdown' },
                { value: 'toggle', label: 'Toggle' },
                { value: 'slider', label: 'Slider' },
                { value: 'location', label: 'Location' },
                { value: 'locator', label: 'Locator' },
                { value: 'images', label: 'Images' }
              ]}
              onChange={value => {
                if (typeof this.props.onTypeChange === 'function') {
                  this.props.onTypeChange(
                    `${fieldName}.${index}.metadata`,
                    value
                  );
                }
              }}
              simpleValue
            />
          </div>
          {this.props.editable ? (
            <div className="product-table__column-header-field">
              <ReactToggle
                name={`${fieldName}.${index}.required`}
                label="Required"
              />
            </div>
          ) : null}
          <div className="kata-product-table__column-header-action">
            <ToggleButton
              onToggle={this.handleToggle}
              active={this.state.active}
              renderActive={() => (
                <Button isIcon>
                  <i className="icon icon-arrow-right" />
                </Button>
              )}
              renderInactive={() => (
                <Button isIcon>
                  <i className="icon icon-arrow-left" />
                </Button>
              )}
            />
          </div>
        </div>

        {this.renderBody()}
      </div>
    );
  }
}
