import React, { Fragment } from 'react';
import { FieldArray, connect, FormikContext } from 'formik';
import get from 'lodash-es/get';

import './ProductTable.scss';

import CmsElement, { Props as ElementProps } from '../../CmsElement';
import ProductTableColumn from './ProductTableColumn';
import { FloatingButton } from 'components/Button';
import EmptyMessage from 'components/Common/EmptyMessage';
import CmsElementDefinitions from 'modules/cms/forms-editor/CmsElementDefinitions';

interface Props extends ElementProps {}

class ProductTableElement extends React.Component<
  Props & { formik: FormikContext<any> }
> {
  handleTypeChange = (name: string, value: string) => {
    this.props.formik.setFieldValue(name, this.getOptionsMetadata(value));
  };

  getOptionsMetadata = (eleType: string) => {
    const found = CmsElementDefinitions.filter(data => data.type === eleType);
    if (found.length) {
      const { type, ...meta } = get(found[0], 'metadata.meta', {});
      return meta;
    }
    return {};
  };

  render() {
    return (
      <CmsElement
        {...this.props}
        renderOptions={() => (
          <React.Fragment>
            <div className="">No Options Available</div>
          </React.Fragment>
        )}
        renderPreview={() => (
          <React.Fragment>
            <div className="kata-product-table__predefined">
              {this.renderPredefinedColumns()}
            </div>
            <div className="kata-product-table__options">
              <h2 className="title">Options</h2>
              {this.renderOptionsColumns()}
            </div>
          </React.Fragment>
        )}
      />
    );
  }

  renderPredefinedColumns() {
    const cols = get(
      this.props.formik,
      `values.elements.${this.props.index}.meta.columns`,
      []
    );
    return Array.isArray(cols)
      ? cols.map((col, index) => (
          <ProductTableColumn
            key={index}
            fieldName={`elements.${this.props.index}.meta.columns`}
            index={index}
            type={col.type}
          />
        ))
      : null;
  }

  renderOptionsColumns() {
    return (
      <FieldArray name={`elements.${this.props.index}.meta.options`}>
        {arrayHelpers => {
          const options = get(
            this.props.formik,
            `values.elements.${this.props.index}.meta.options`,
            false
          );
          return (
            <Fragment>
              {Array.isArray(options) && options.length > 0 ? (
                options.map((col, index) => (
                  <div className="row no-gutters mb-1" key={index}>
                    <div className="col mr-1">
                      <ProductTableColumn
                        key={index}
                        fieldName={`elements.${this.props.index}.meta.options`}
                        index={index}
                        onTypeChange={this.handleTypeChange}
                        type={col.type}
                        editable
                      />
                    </div>
                    <div className="align-self-start">
                      <FloatingButton
                        icon="trash"
                        onClick={() => arrayHelpers.remove(index)}
                      />
                    </div>
                  </div>
                ))
              ) : (
                <EmptyMessage title="No optional columns">
                  Click on the plus button below to add column.
                </EmptyMessage>
              )}{' '}
              <div className="kata-product-table__options-action text-center">
                <FloatingButton
                  icon="add"
                  onClick={() =>
                    arrayHelpers.push({
                      name: '',
                      label: '',
                      type: 'text',
                      width: 180,
                      metadata: {}
                    })
                  }
                />
              </div>
            </Fragment>
          );
        }}
      </FieldArray>
    );
  }
}

export default connect<Props>(ProductTableElement);
