import React from 'react';

import { Field } from 'components/FormikWrapper';
import CmsElement, { Props as ElementProps } from './CmsElement';

interface Props extends ElementProps {}

export default class TextAreaElement extends React.Component<Props> {
  render() {
    const fieldName =
      this.props.fieldName || `elements.${this.props.index}.meta`;
    return (
      <CmsElement
        {...this.props}
        renderOptions={() => (
          <React.Fragment>
            <div className="">No Options Available</div>
          </React.Fragment>
        )}
        renderPreview={() => (
          <React.Fragment>
            <div className="p-2">
              <div className="kata-form__element">
                <label className="control-label kata-form__label">
                  Default Value
                </label>
                <Field
                  component="textarea"
                  name={`${fieldName}.value`}
                  className="kata-form__input-text"
                  style={{ height: 100 }}
                />
              </div>
            </div>
          </React.Fragment>
        )}
      />
    );
  }
}
