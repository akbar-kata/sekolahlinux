import React from 'react';

import CmsElement, { Props as ElementProps } from './CmsElement';

interface Props extends ElementProps {}

export default class LocatorElement extends React.Component<Props> {
  render() {
    return (
      <CmsElement
        {...this.props}
        renderOptions={() => <div className="">No Options Available</div>}
        renderPreview={() => (
          <div className="p-2 text-center">
            <div className="kata-location-element">
              <div className="kata-location-element__thumbnail">
                <img
                  src={require('assets/images/preview-map.svg')}
                  alt="Preview"
                />
              </div>
              <div className="kata-location-element__data">
                <div className="kata-location-element__data-title">
                  Location Title
                </div>
                <div className="kata-location-element__data-table">
                  <img src={require('assets/images/table.svg')} alt="" />
                </div>
              </div>
            </div>
          </div>
        )}
      />
    );
  }
}
