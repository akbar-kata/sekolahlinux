import React from 'react';

import CmsElement, { Props as ElementProps } from './CmsElement';

interface Props extends ElementProps {}

export default class FaqElement extends React.Component<Props> {
  render() {
    return (
      <CmsElement
        {...this.props}
        noRequired
        renderOptions={() => (
          <React.Fragment>
            <div className="">No Options Available</div>
          </React.Fragment>
        )}
        renderPreview={() => (
          <React.Fragment>
            <div className="p-2">
              <table className="kata-client-table">
                <thead>
                  <tr>
                    <th>Label</th>
                    <th>Question</th>
                    <th>Answer</th>
                  </tr>
                </thead>
              </table>
            </div>
          </React.Fragment>
        )}
      />
    );
  }
}
