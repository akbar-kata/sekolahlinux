import React from 'react';
import { connect, FormikContext } from 'formik';
import Slider from 'react-rangeslider';
import get from 'lodash-es/get';
import isNumber from 'lodash-es/isNumber';

import { Field } from 'components/FormikWrapper';
import CmsElement, { Props as ElementProps } from './CmsElement';

interface Props extends ElementProps {}

interface States {}

class SliderElement extends React.Component<
  Props & { formik: FormikContext<any> },
  States
> {
  render() {
    const fieldName =
      this.props.fieldName || `elements.${this.props.index}.meta`;
    const { min, max, step, value } = get(
      this.props.formik,
      `values.${fieldName}`,
      {}
    );
    return (
      <CmsElement
        {...this.props}
        renderOptions={() => (
          <React.Fragment>
            <div className="">No Options Available</div>
          </React.Fragment>
        )}
        renderPreview={() => (
          <React.Fragment>
            <div className="p-3">
              <div className="kata-info__container">
                <div className="kata-info__content row no-gutters">
                  <div className="col pr-1">
                    <label className="kata-info__label">Minimum Value</label>
                    <Field
                      type="number"
                      className="kata-form__input-text"
                      name={`${fieldName}.min`}
                    />
                  </div>
                  <div className="col pr-1">
                    <label className="kata-info__label">Maximum Value</label>
                    <Field
                      type="number"
                      className="kata-form__input-text"
                      name={`${fieldName}.max`}
                    />
                  </div>
                  <div className="col pr-1">
                    <label className="kata-info__label">Default Value</label>
                    <Field
                      type="number"
                      className="kata-form__input-text"
                      name={`${fieldName}.value`}
                    />
                  </div>
                  <div className="col pr-1">
                    <label className="kata-info__label">Scale</label>
                    <Field
                      type="number"
                      className="kata-form__input-text"
                      name={`${fieldName}.step`}
                    />
                  </div>
                </div>
              </div>
              <div className="kata-form__element pb-1 pt-1">
                <Slider
                  min={min}
                  max={max}
                  step={step}
                  labels={{
                    0: min,
                    100: max
                  }}
                  value={value}
                  handleLabel={isNumber(value) ? value.toString() : ''}
                  tooltip={false}
                />
              </div>
            </div>
          </React.Fragment>
        )}
      />
    );
  }
}

export default connect<Props>(SliderElement);
