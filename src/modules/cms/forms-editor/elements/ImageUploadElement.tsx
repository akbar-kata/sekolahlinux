import React from 'react';

import { SupportButton } from 'components/Button';

const imageDropdownPlaceholder = require('assets/images/upload.svg');
const imagePreviewPlaceholder = require('assets/images/upload-image.svg');

import CmsElement, { Props as ElementProps } from './CmsElement';

interface Props extends ElementProps {}

interface States {}

export default class ImageUploadElement extends React.Component<Props, States> {
  render() {
    return (
      <CmsElement
        {...this.props}
        renderOptions={() => <div className="">No Options Available</div>}
        renderPreview={() => (
          <div className="p-3">
            <div className="kata-info__container">
              <div className="kata-image-element__container">
                <div className="kata-image-element__container-drop">
                  <div className="kata-image-element__dropdown-placeholder">
                    <img src={imageDropdownPlaceholder} alt="placeholder" />
                  </div>
                  <div className="pt-1">
                    <span>Drag & drop to upload</span>
                  </div>
                  <div>
                    <span>or</span>
                  </div>
                  <div className="pt-1">
                    <SupportButton>Browse Files</SupportButton>
                  </div>
                  <input
                    type="file"
                    name="images[]"
                    id="image"
                    className="d-none"
                  />
                </div>
                <div className="kata-image-element__container-preview">
                  <div className="kata-image-element__preview">
                    <img src={imagePreviewPlaceholder} alt="" />
                  </div>
                </div>
              </div>
            </div>
          </div>
        )}
      />
    );
  }
}
