import React, { Fragment } from 'react';
import { FieldArray } from 'formik';
import get from 'lodash-es/get';

import { Field, FormError } from 'components/FormikWrapper';
import FloatingButton from 'components/Button/FloatingButton';
import CmsElement, { Props as ElementProps } from './CmsElement';

interface Props extends ElementProps {}

export default class DropdownElement extends React.Component<Props> {
  renderOptions = arrayHelper => {
    const { index } = this.props;
    const fieldName = this.props.fieldName || `elements.${index}.meta`;
    const options = get(arrayHelper, `form.values.${fieldName}.options`, []);
    return (
      <div className="p-2">
        <FormError name={`${fieldName}.options`} />
        {Array.isArray(options) &&
          options.map((opt, idx) => (
            <div className="row no-gutters mb-1" key={idx}>
              <div className="col mr-1">
                <label className="control-label kata-form__label">Label</label>
                <Field
                  name={`${fieldName}.options.${idx}.label`}
                  className="kata-form__input-text"
                  placeholder="Label here..."
                />
              </div>
              <div className="col mr-1">
                <label className="control-label kata-form__label">Value</label>
                <Field
                  name={`${fieldName}.options.${idx}.value`}
                  className="kata-form__input-text"
                  placeholder="Value here..."
                />
              </div>
              <div className="align-self-end">
                <FloatingButton
                  icon="trash"
                  onClick={() => arrayHelper.remove(idx)}
                />
              </div>
            </div>
          ))}
        <div className="text-center mt-2">
          <FloatingButton icon="add" onClick={() => arrayHelper.push({})} />
        </div>
      </div>
    );
  };

  render() {
    return (
      <CmsElement
        {...this.props}
        noRequired
        renderOptions={() => (
          <Fragment>
            <div className="">No Options Available</div>
          </Fragment>
        )}
        renderPreview={() => (
          <Fragment>
            <FieldArray name={`elements.${this.props.index}.meta.options`}>
              {this.renderOptions}
            </FieldArray>
          </Fragment>
        )}
      />
    );
  }
}
