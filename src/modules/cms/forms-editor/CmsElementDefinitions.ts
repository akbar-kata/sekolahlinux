import { ElementDefinition } from 'interfaces/cms';
import * as env from 'utils/env';

const CmsElementDefinitions: ElementDefinition[] = [
  {
    type: 'field',
    title: 'Text Box',
    element: 'single',
    meta: {
      type: 'text',
      name: '',
      meta: {
        label: '',
        type: 'text',
        value: ''
      }
    }
  },
  {
    type: 'field',
    title: 'Text Area',
    element: 'single',
    meta: {
      type: 'textarea',
      name: '',
      meta: {
        label: '',
        type: 'textarea',
        value: ''
      }
    }
  },
  {
    type: 'field',
    title: 'Text List',
    element: 'single',
    meta: {
      type: 'textlist',
      name: '',
      meta: {
        label: '',
        type: 'textlist'
      }
    }
  },
  {
    type: 'field',
    title: 'Dropdown',
    element: 'single',
    meta: {
      type: 'dropdown',
      name: '',
      meta: {
        label: '',
        type: 'dropdown',
        options: [{ label: '', value: '' }]
      }
    }
  }
];

if (env.getRuntimeEnv('REACT_APP_RUNTIME_CMS_NEW_ELEMENTS') === 'true') {
  CmsElementDefinitions.push(
    {
      type: 'field',
      title: 'Toggle',
      element: 'single',
      meta: {
        type: 'toggle',
        name: '',
        meta: {
          value: false,
          toggleLabel: '',
          label: '',
          type: 'toggle'
        }
      }
    },
    {
      type: 'field',
      title: 'Slider',
      element: 'single',
      meta: {
        type: 'slider',
        name: '',
        meta: {
          label: '',
          type: 'slider',
          min: 0,
          max: 100,
          step: 1,
          value: 10
        }
      }
    },
    {
      type: 'table',
      title: 'Location',
      element: 'single',
      meta: {
        type: 'location',
        name: '',
        meta: {
          label: '',
          type: 'location'
        }
      }
    },
    {
      type: 'table',
      title: 'Image Uploader',
      element: 'single',
      meta: {
        type: 'images',
        name: '',
        meta: {
          type: 'images',
          label: '',
          value: []
        }
      }
    },
    {
      type: 'table',
      title: 'FAQ',
      element: 'table',
      meta: {
        type: 'faq',
        name: '',
        meta: {
          type: 'faq',
          label: '',
          columns: [
            {
              name: 'label',
              type: 'string',
              options: {
                index: 0,
                required: true
              }
            },
            {
              name: 'questions',
              type: 'array',
              options: {
                index: 1,
                required: true
              }
            },
            {
              name: 'answers',
              type: 'array',
              options: {
                index: 2,
                required: true
              }
            },
            {
              name: 'options',
              type: 'dictionary',
              options: {
                index: 1,
                required: false
              }
            }
          ]
        }
      }
    },
    {
      type: 'table',
      title: 'Locator',
      element: 'table',
      meta: {
        type: 'locator',
        name: '',
        meta: {
          label: '',
          type: 'locator'
        }
      }
    },
    {
      type: 'table',
      title: 'Product Table',
      element: 'table',
      meta: {
        type: 'product',
        name: '',
        meta: {
          type: 'product',
          label: '',
          columns: [
            {
              name: 'name',
              label: 'Name',
              type: 'text',
              width: 180
            },
            {
              name: 'tag',
              label: 'Tag',
              type: 'textlist',
              width: 180
            },
            {
              name: 'description',
              label: 'Description',
              type: 'text',
              width: 250
            },
            {
              name: 'image',
              label: 'Image',
              type: 'images',
              width: 100
            },
            {
              name: 'link',
              label: 'Link',
              type: 'text',
              width: 180
            },
            {
              name: 'availability',
              label: 'Availability',
              type: 'toggle',
              width: 80
            }
          ],
          options: []
        }
      }
    }
  );
}

export default CmsElementDefinitions;
