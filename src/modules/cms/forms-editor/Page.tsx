import React, { PureComponent } from 'react';
import { withFormik, FormikProps, Form, FieldArray } from 'formik';
import { Scrollbars } from 'react-custom-scrollbars';
import NavigationPrompt from 'react-router-navigation-prompt';
import shortid from 'shortid';
import cloneDeep from 'lodash-es/cloneDeep';

import { DataMap } from 'interfaces/common';
import { ElementMetadata } from 'interfaces/cms';
import { LoadingState } from 'interfaces/app/loadings';

import Robot from 'components/Loading/Robot';
import Button from 'components/Button/Button';
import { ConfirmDialogPrompt } from 'components/ConfirmDialogPrompt';

import CmsWidgetMapping from './CmsWidgetMapping';
import CmsTitle from './CmsTitle';
import DesignerBoard from './DesignerBoard';
import validationSchema from './validationSchema';

import ErrorBoundary from 'modules/core/ErrorBoundary';

export interface FormValue {
  elements: ElementMetadata[];
  removed: string[];
}

interface Props {
  elements: ElementMetadata[];
  selectedFormData: any;
  formLoading: boolean;
  loadings: LoadingState;
  saveElements(payload: FormValue);
}

interface States {
  overflow: boolean;
}

class Page extends PureComponent<Props & FormikProps<FormValue>, States> {
  state: States = {
    overflow: false
  };

  componentWillUnmount() {
    this.watchOverflow(0);
  }

  decorateWithId(data: DataMap<any>) {
    return {
      ...cloneDeep(data),
      tempId: shortid.generate()
    };
  }

  watchOverflow = (position: number) => {
    if (position > 0.05) {
      this.setState({ overflow: true });
    } else {
      this.setState({ overflow: false });
    }
  };

  removeElement = (remove: Function, index) => {
    const element = this.props.values.elements[index] || {};
    if (element.id) {
      const removed = this.props.values.removed || [];
      this.props.setFieldValue('removed', [...removed, element.id]);
    }
    remove(index);
  };

  render() {
    const { formLoading, loadings, values } = this.props;
    if (formLoading && loadings.fetch) {
      return <Robot />;
    }
    return (
      <ErrorBoundary>
        <Form className="full-size">
          <NavigationPrompt when={this.props.dirty}>
            {({ isActive, onCancel, onConfirm }) => {
              return (
                <ConfirmDialogPrompt
                  message="If you leave this page your changes will be lost. Leave page anyway?"
                  title="Leave CMS"
                  open={isActive}
                  oncancel={onCancel}
                >
                  <Button color="primary" onClick={onConfirm}>
                    Leave
                  </Button>
                  <Button color="white" onClick={onCancel}>
                    Stay
                  </Button>
                </ConfirmDialogPrompt>
              );
            }}
          </NavigationPrompt>

          <FieldArray name="elements">
            {arrayHelper => (
              <div className="kata-form-editor">
                {loadings.update ||
                loadings.fetch ||
                loadings.save ||
                loadings.remove ? (
                  <div className="kata-form-editor__blocker" />
                ) : null}

                <CmsWidgetMapping
                  addElement={data =>
                    arrayHelper.push(this.decorateWithId(data))
                  }
                />
                <div className="kata-form-editor__designer-container">
                  <CmsTitle
                    overflow={this.state.overflow}
                    isValid={!!(values.elements && values.elements.length)}
                    loading={
                      !!(this.props.loadings.save || this.props.loadings.update)
                    }
                    selectedFormData={this.props.selectedFormData}
                  />
                  <Scrollbars
                    onScrollFrame={scroll => this.watchOverflow(scroll.top)}
                  >
                    <DesignerBoard
                      addElement={data => {
                        arrayHelper.push(this.decorateWithId(data));
                      }}
                      elements={values.elements}
                      removeElement={index =>
                        this.removeElement(arrayHelper.remove, index)
                      }
                      onSortMove={(start, end) => {
                        arrayHelper.swap(start, end);
                      }}
                    />
                  </Scrollbars>
                </div>
              </div>
            )}
          </FieldArray>
        </Form>
      </ErrorBoundary>
    );
  }
}

export default withFormik<Props, FormValue>({
  validationSchema,
  mapPropsToValues: props => ({
    elements: props.elements,
    removed: []
  }),
  enableReinitialize: true,
  handleSubmit: (values, { props, resetForm, setStatus }) => {
    props.saveElements(values);
    // reset form to set `dirty` = false
    setTimeout(() => {
      resetForm({ elements: values.elements, removed: [] });
      setStatus(new Date()); // track last saved
    }, 500);
  }
})(Page);
