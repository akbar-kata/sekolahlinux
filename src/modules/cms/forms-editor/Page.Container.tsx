import React, { Component } from 'react';
import { connect } from 'react-redux';

import Store from 'interfaces/rootStore';
import { ElementMetadata } from 'interfaces/cms';
import { LoadingState } from 'interfaces/app/loadings';

import * as CommonAction from 'stores/common/actions';
import ELEMENT from 'stores/cms/elements/types';
import { getDetailForm } from 'stores/cms/forms/selectors';
import {
  setElement,
  setElementData,
  fetchCmsElementsRequest
} from 'stores/cms/elements/actions';
import { getLoading } from 'stores/app/loadings/selectors';

import withDragDropContext from 'components/Common/WithDragDropContext';

import Page, { FormValue } from './Page';

import './Page.scss';

interface PropsFromStore {
  formLoading: boolean;
  loadings: LoadingState;
  elements: any;
  selectedFormData: any;
  match?: any;
}

interface PropsFromDispatch {
  fetchElements: Function;
  saveElements(payload: FormValue);
  setElements(elements: ElementMetadata[]);
  setElementData(index: number, field: string, data: any);
  deleteElement(element: ElementMetadata);
}

interface OwnProps {}

interface Props extends PropsFromStore, PropsFromDispatch, OwnProps {}

interface States {}

class PageContainer extends Component<Props, States> {
  componentDidMount() {
    const { match } = this.props;
    const data = {
      formId: match.params.formId,
      label: match.params.pageId,
      revision: match.params.revision
    };

    this.props.fetchElements(data);
  }

  render() {
    return (
      <Page
        formLoading={this.props.formLoading}
        loadings={this.props.loadings}
        elements={this.props.elements}
        saveElements={this.props.saveElements}
        selectedFormData={this.props.selectedFormData}
      />
    );
  }
}

const mapStateToProps = (store: Store) => ({
  formLoading: getLoading(store.app.loadings, ELEMENT.FETCH_REQUEST),
  loadings: {
    fetch: getLoading(store.app.loadings, ELEMENT.FETCH_ELEMENTS),
    save: getLoading(store.app.loadings, ELEMENT.SAVE_ELEMENTS),
    update: getLoading(store.app.loadings, ELEMENT.SAVE_ELEMENTS),
    remove: getLoading(store.app.loadings, ELEMENT.DELETE_ELEMENT)
  },
  elements: store.cms.elements.elements,
  selectedFormData: getDetailForm(store.cms, store.cms.forms.label)
});

const mapDispatchToProps = {
  saveElements: (payload: FormValue) =>
    CommonAction.request(ELEMENT.SAVE_ELEMENTS, payload),
  fetchElements: (data: any) => fetchCmsElementsRequest(data),
  setElements: (elements: ElementMetadata[]) => setElement(elements),
  setElementData: (index: number, field: string, data: any) =>
    setElementData(index, field, data),
  deleteElement: (element: ElementMetadata) =>
    CommonAction.request(ELEMENT.DELETE_ELEMENT, element)
};

export default withDragDropContext(
  connect<PropsFromStore, PropsFromDispatch, OwnProps, Store>(
    mapStateToProps,
    mapDispatchToProps
  )(PageContainer)
);
