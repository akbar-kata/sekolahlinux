import React from 'react';
import { connect } from 'react-redux';

import Store from 'interfaces/rootStore';
import { closeDrawer } from 'stores/app/drawer/actions';
import { Page as FormData } from 'interfaces/cms';
import CMS_PAGE from 'stores/cms/pages/types';
import { isDrawerOpen, getDrawerData } from 'stores/app/drawer/selectors';
import { getLoading } from 'stores/app/loadings/selectors';
import Form from './Form';
import { editPageRequest } from 'stores/cms/pages/actions';

export const DRAWER_ID = 'drawer-edit-page';

interface PropsFromState {
  loading: boolean;
  isOpen: boolean;
  editData: any;
}

interface PropsFromDispatch {
  closeDrawer(): void;
  updatePage(data: FormData);
}

interface Props extends PropsFromState, PropsFromDispatch {}

const EditFormContainer = (props: Props) => (
  <Form
    type="update"
    data={props.editData}
    isOpen={props.isOpen}
    loading={props.loading}
    onCancel={props.closeDrawer}
    onSubmit={props.updatePage}
  />
);

const mapStateToProps = (store: Store): PropsFromState => ({
  loading: getLoading(store.app.loadings, CMS_PAGE.FETCH_REQUEST),
  isOpen: isDrawerOpen(store.app.drawer, DRAWER_ID),
  editData: getDrawerData(store.app.drawer, DRAWER_ID)
});

const mapDispatchToProps: PropsFromDispatch = {
  closeDrawer: () => closeDrawer(DRAWER_ID),
  updatePage: (data: FormData) => editPageRequest(data)
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditFormContainer);
