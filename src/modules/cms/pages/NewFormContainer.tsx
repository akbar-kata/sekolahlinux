import React from 'react';
import { connect } from 'react-redux';

import Store from 'interfaces/rootStore';
import { Page as FormData } from 'interfaces/cms';

import CMS_PAGE from 'stores/cms/pages/types';
import { closeDrawer } from 'stores/app/drawer/actions';
import { isDrawerOpen } from 'stores/app/drawer/selectors';
import { getLoading } from 'stores/app/loadings/selectors';
import { addPageRequest } from 'stores/cms/pages/actions';

import Form from './Form';

export const DRAWER_ID = 'drawer-new-page';

interface PropsFromState {
  loading: boolean;
  isOpen: boolean;
  pages: Record<string, FormData>;
}

interface PropsFromDispatch {
  closeDrawer(): void;
  saveNewPage(data: FormData);
}

interface Props extends PropsFromState, PropsFromDispatch {}

const NewFormContainer = (props: Props) => (
  <Form
    type="create"
    isOpen={props.isOpen}
    loading={props.loading}
    onCancel={props.closeDrawer}
    onSubmit={props.saveNewPage}
    data={props.pages}
  />
);

const mapStateToProps = (store: Store) => ({
  loading: getLoading(store.app.loadings, CMS_PAGE.FETCH_REQUEST),
  isOpen: isDrawerOpen(store.app.drawer, DRAWER_ID),
  pages: store.cms.pages.data
});

const mapDispatchToProps = {
  closeDrawer: () => closeDrawer(DRAWER_ID),
  saveNewPage: (data: FormData) => addPageRequest(data)
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NewFormContainer);
