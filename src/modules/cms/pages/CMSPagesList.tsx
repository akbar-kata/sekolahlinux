import React from 'react';

import Card from 'components/Card';
import { Button } from 'components/Button';
import Sortable from 'components/Common/Sortable';
import { confirm } from 'components/Modal';

import { DataMap } from 'interfaces/common';
import * as CMSPages from 'interfaces/cms';

import { closeDrawer, openDrawer } from 'stores/app/drawer/actions';
import { swapPage } from 'stores/cms/pages/actions';

interface CMSPagesListProps {
  index: string[];
  data: DataMap<CMSPages.Page>;
  openDetail: (data: any) => ReturnType<typeof openDrawer>;
  closeDetail: () => ReturnType<typeof closeDrawer>;
  onSwap: (start: number, end: number) => ReturnType<typeof swapPage>;
  onRemove: Function;
  onSelectPage: Function;
}

type AllProps = CMSPagesListProps;

class CMSPagesList extends React.Component<AllProps> {
  render() {
    return <div>{this.props.data && this.renderElements()}</div>;
  }

  public onRemove = (item: any) => {
    return confirm({
      title: 'Delete Page',
      message: `Are you sure you want to delete ${item.name} page?`,
      okLabel: 'Delete',
      cancelLabel: 'Cancel'
    }).then(res => {
      if (res) {
        this.props.onRemove(item);
      }
    });
  };

  private renderElements() {
    const { index, data, onSwap } = this.props;
    return index.map((p, i) => {
      const item = data[p];
      return (
        <Sortable key={i} index={i} id={p} onMove={onSwap}>
          <Card
            key={p}
            className="kata-cms-pages-list-card mb-2"
            onClick={() => this.props.onSelectPage(item)}
          >
            <div className="kata-cms-pages-list-card__drag-icon">
              <i className="icon-drag" />
            </div>
            <div className="kata-cms-pages-list-card__avatar">
              <img
                src={require('assets/images/cms-pages-avatar.svg')}
                alt="Page Icon"
              />
            </div>
            <div className="kata-cms-pages-list-card__name kata-cms-pages-list-card__ellipsis">
              <span className="heading2">{item.name}</span>
            </div>
            <div className="kata-cms-pages-list-card__metadata">
              <div className="kata-cms-pages-list-card__metadata-item">
                <h5 className="text-label">Label</h5>
                <p className="paragraph">{item.label}</p>
              </div>
            </div>
            <div className="kata-cms-pages-list-card__buttons">
              {/* <Button
                color="white"
                onClick={e => {
                  e.stopPropagation();
                  openDetail(item);
                }}
              >
                <i className="icon-edit" /> <span>Update</span>
              </Button>{' '} */}
              <Button
                className="kata-btn--danger"
                color="white"
                onClick={e => {
                  e.stopPropagation();
                  this.onRemove(item);
                }}
              >
                <i className="icon-trash" /> <span>Delete</span>
              </Button>
            </div>
          </Card>
        </Sortable>
      );
    });
  }
}

export default CMSPagesList;
