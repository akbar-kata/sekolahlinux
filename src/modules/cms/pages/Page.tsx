import React from 'react';

import { Dashboard } from 'components/Dashboard';
import CMSPagesList from './CMSPagesList.Container';
import Publish from '../publish';

class CMSPages extends React.Component {
  render() {
    return (
      <Dashboard
        className="kata-cms-pages-dashboard"
        title="Pages"
        tooltip="Page acts as a category for your Forms (i.e. Product Page, FAQ Page.)"
      >
        <Publish />
        <CMSPagesList />
      </Dashboard>
    );
  }
}

export default CMSPages;
