import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';

import Store from 'interfaces/rootStore';
import * as CMSPages from 'interfaces/cms';

import { Robot } from 'components/Loading';
import { FloatingButton } from 'components/Button';
import { Tooltip, TooltipTarget } from 'components/Tooltip';
import { EmptyMessage, withDragDropContext } from 'components/Common';

import PAGE from 'stores/cms/pages/types';
import { closeDrawer, openDrawer } from 'stores/app/drawer/actions';
import { getDrawerData, isDrawerOpen } from 'stores/app/drawer/selectors';
import { getModalData, isModalOpen } from 'stores/app/modal/selectors';
import { getBotSelected } from 'stores/bot/selectors';
import {
  fetchCmsPagesRequest,
  removePageRequest,
  selectPage,
  swapPage
} from 'stores/cms/pages/actions';
import { getCmsPagesData, getCmsPagesIndex } from 'stores/cms/pages/selectors';
import { getProjectSelected } from 'stores/project/selectors';
import { getLoading } from 'stores/app/loadings/selectors';

import ErrorBoundary from 'modules/core/ErrorBoundary';

import CMSPagesList from './CMSPagesList';
import { default as NewFormContainer, DRAWER_ID } from './NewFormContainer';
import {
  default as EditFormContainer,
  DRAWER_ID as EDIT_DRAWER_ID
} from './EditFormContainer';

interface PropsFromState {
  selectedProject: string | null;
  selectedBot: string | null;
  isLoading: boolean;
  index: string[];
  data: Record<string, CMSPages.Page>;
  isCreateOpen: boolean;
  isDetailOpen: boolean;
  isDeleteOpen: boolean;
  detailData: any;
  deleteData: any;
}

interface PropsFromDispatch {
  openCreate: () => ReturnType<typeof openDrawer>;
  closeCreate: () => ReturnType<typeof closeDrawer>;
  openDetail: (data: any) => ReturnType<typeof openDrawer>;
  closeDetail: () => ReturnType<typeof closeDrawer>;
  onSwap: (start: number, end: number) => ReturnType<typeof swapPage>;
  fetchCmsPagesRequest: typeof fetchCmsPagesRequest;
  onRemove: (data: any) => ReturnType<typeof removePageRequest>;
  onSelectPage: (item: any) => ReturnType<typeof selectPage>;
}

export type Props = PropsFromState & PropsFromDispatch;

const deleteModalId = 'DeleteCMS';

class CMSPagesListContainer extends React.Component<Props> {
  renderLoading() {
    return (
      <table>
        <tbody>
          <tr>
            <td colSpan={4} className="text-center">
              <h5 className="pa4 text-bold">
                <Robot />
              </h5>
            </td>
          </tr>
        </tbody>
      </table>
    );
  }

  onSelect = item => {
    this.props.onSelectPage(item);
  };

  renderData() {
    const {
      openDetail,
      closeDetail,
      index,
      data,
      onSwap,
      onRemove
    } = this.props;
    if (this.props.index.length === 0) {
      return (
        <EmptyMessage
          image={require('assets/images/pages-empty.svg')}
          title="No Pages"
        >
          This Page don’t have any form yet. Create Pages by click the floating
          button in below.
        </EmptyMessage>
      );
    }

    return (
      <ErrorBoundary>
        <CMSPagesList
          index={index}
          data={data}
          openDetail={openDetail}
          closeDetail={closeDetail}
          onSwap={onSwap}
          onRemove={onRemove}
          onSelectPage={this.onSelect}
        />
      </ErrorBoundary>
    );
  }

  render() {
    const { openCreate, isLoading } = this.props;
    return (
      <Fragment>
        {(isLoading && this.renderLoading()) || this.renderData()}

        <NewFormContainer />
        <EditFormContainer />

        <TooltipTarget
          placement="top"
          component={<Tooltip>Create Page</Tooltip>}
        >
          <FloatingButton
            color="primary"
            icon="add"
            className="kata-floating-action"
            onClick={() => openCreate()}
          />
        </TooltipTarget>
      </Fragment>
    );
  }
}

const mapStateToProps = ({
  app: { modal, drawer, loadings },
  bot,
  cms,
  project
}: Store) => {
  const selectedProject = getProjectSelected(project);
  return {
    selectedProject,
    selectedBot: getBotSelected(bot),
    isLoading: getLoading(loadings, PAGE.FETCH_REQUEST),
    index: getCmsPagesIndex(cms),
    data: getCmsPagesData(cms),
    isDetailOpen: isDrawerOpen(drawer, EDIT_DRAWER_ID),
    isCreateOpen: isDrawerOpen(drawer, DRAWER_ID),
    isDeleteOpen: isModalOpen(modal, deleteModalId),
    detailData: getDrawerData(drawer, EDIT_DRAWER_ID),
    deleteData: getModalData(modal, deleteModalId)
  };
};

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    openCreate: () => dispatch(openDrawer(DRAWER_ID)),
    closeCreate: () => dispatch(closeDrawer(DRAWER_ID)),
    openDetail: (data?: any) => dispatch(openDrawer(EDIT_DRAWER_ID, data)),
    closeDetail: () => dispatch(closeDrawer(EDIT_DRAWER_ID)),
    onSwap: (start: number, end: number) => dispatch(swapPage(start, end)),
    fetchCmsPagesRequest: () => dispatch(fetchCmsPagesRequest()),
    onRemove: (data: any) => dispatch(removePageRequest(data)),
    onSelectPage: (item: any) => dispatch(selectPage(item))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withDragDropContext(CMSPagesListContainer));
