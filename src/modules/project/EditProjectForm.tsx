import React from 'react';
import { connect } from 'react-redux';
import { Form, Formik, FormikProps } from 'formik';
import * as yup from 'yup';

import {
  Drawer,
  DrawerHeader,
  DrawerBody,
  DrawerFooter
} from 'components/Drawer';
import { Button } from 'components/Button';

import RootStore from 'interfaces/rootStore';
import { Project } from 'interfaces/project';

import { isDrawerOpen } from 'stores/app/drawer/selectors';
import { closeDrawer } from 'stores/app/drawer/actions';

import { updateProjectRequest } from 'stores/project/actions';
import { getLoading } from 'stores/app/loadings/selectors';
import PROJECT_TYPES from 'stores/project/types';
import ProjectForm from './ProjectForm';

interface PropsFromState {
  isDrawerOpen: boolean;
  isSaving: boolean;
}

interface PropsFromDispatch {
  closeDrawer: () => any;
  updateProject: (project: Partial<Project>) => any;
}
interface OwnProps {
  project: Project;
}

type ComponentProps = PropsFromState & PropsFromDispatch & OwnProps;

export const DRAWER_ID = 'EditProjectForm';

class EditProjectForm extends React.Component<ComponentProps> {
  validationSchema = yup.object().shape({
    name: yup
      .string()
      .required('Name is required')
      .max(20, 'Name only allowed 20 characters')
      .matches(
        /^[A-Za-z][-A-Za-z0-9_-]*[A-Za-z0-9]$/,
        'Name must contain only alphanumeric characters (A-Z, a-z, 0-9), `-`, and `_`.'
      ),
    description: yup.string(),
    options: yup.object({
      timezone: yup.number().required('Timezone is required'),
      nluVisibility: yup.string().required('NLU Visibiliy is required'),
      nluLang: yup.string().required('Language is required'),
      bot: yup.boolean(),
      nlu: yup.boolean(),
      cms: yup.boolean()
    })
  });

  onSubmit = (values: Project) => {
    const { name, description, options } = values;

    this.props.updateProject({
      name,
      description,
      options,
      id: this.props.project.id
    });
  };

  innerForm = ({ isValid }: FormikProps<Project>) => (
    <Form className="kata-form full-size">
      <DrawerHeader title="Edit Project" />
      <DrawerBody>
        <ProjectForm isEditing />
      </DrawerBody>
      <DrawerFooter>
        <Button
          color="primary"
          className="mr-1"
          loading={this.props.isSaving}
          disabled={this.props.isSaving || !isValid}
          type="submit"
        >
          Update
        </Button>
        <Button onClick={this.props.closeDrawer}>Cancel</Button>
      </DrawerFooter>
    </Form>
  );

  render() {
    return (
      <Drawer isOpen={this.props.isDrawerOpen} onClose={this.props.closeDrawer}>
        <Formik
          validationSchema={this.validationSchema}
          onSubmit={this.onSubmit}
          initialValues={this.props.project}
        >
          {this.innerForm}
        </Formik>
      </Drawer>
    );
  }
}
const mapStateToProps = (store: RootStore) => ({
  isDrawerOpen: isDrawerOpen(store.app.drawer, DRAWER_ID),
  isSaving: getLoading(store.app.loadings, PROJECT_TYPES.UPDATE_PROJECT_REQUEST)
});
const mapDispatchToProps = {
  closeDrawer: () => closeDrawer(DRAWER_ID),
  updateProject: (project: Project) => updateProjectRequest(project)
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditProjectForm);
