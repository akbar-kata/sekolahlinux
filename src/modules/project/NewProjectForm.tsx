import React from 'react';
import { connect } from 'react-redux';
import { Form, Formik, FormikProps } from 'formik';
import * as yup from 'yup';

import {
  Drawer,
  DrawerBody,
  DrawerFooter,
  DrawerHeader
} from 'components/Drawer';
import { Button } from 'components/Button';

import RootStore from 'interfaces/rootStore';
import { Project } from 'interfaces/project';

import { isDrawerOpen } from 'stores/app/drawer/selectors';
import { closeDrawer } from 'stores/app/drawer/actions';

import { createProjectRequest } from 'stores/project/actions';
import { getLoading } from 'stores/app/loadings/selectors';
import PROJECT_TYPES from 'stores/project/types';

import ProjectForm from './ProjectForm';

interface PropsFromState {
  isDrawerOpen: boolean;
  isSaving: boolean;
}

interface PropsFromDispatch {
  closeDrawer: () => any;
  createProject: (project: Partial<Project>) => any;
}

type ComponentProps = PropsFromState & PropsFromDispatch;

export const DRAWER_ID = 'NewProjectForm';

const formInitialValues: any = {
  name: '',
  description: '',
  options: {
    timezone: 7.0,
    nluLang: 'id',
    nluVisibility: 'private',
    bot: true,
    cms: true,
    nlu: true
  }
};

class NewProjectForm extends React.Component<ComponentProps> {
  validationSchema = yup.object().shape({
    name: yup
      .string()
      .required('Name is required')
      .max(20, 'Name only allowed 20 characters')
      .matches(
        /^[A-Za-z][-A-Za-z0-9_-]*[A-Za-z0-9]$/,
        'Name must contain only alphanumeric characters (A-Z, a-z, 0-9), `-`, and `_`.'
      ),
    description: yup.string(),
    options: yup.object({
      timezone: yup.number().required('Timezone is required'),
      nluVisibility: yup.string().required('NLU Visibiliy is required'),
      nluLang: yup.string().required('Language is required'),
      bot: yup.boolean(),
      nlu: yup.boolean(),
      cms: yup.boolean()
    })
  });

  onSubmit = (values: Partial<Project>) => {
    const { name, description, options } = values;

    if (name && options) {
      this.props.createProject({
        name,
        description,
        options
      });
    }
  };

  innerForm = ({ isValid }: FormikProps<Project>) => (
    <Form className="kata-form full-size">
      <DrawerHeader title="Create Project" />
      <DrawerBody>
        <ProjectForm />
      </DrawerBody>
      <DrawerFooter>
        <Button
          color="primary"
          type="submit"
          className="mr-1"
          loading={this.props.isSaving}
          disabled={this.props.isSaving || !isValid}
        >
          Create
        </Button>
        <Button onClick={this.props.closeDrawer}>Cancel</Button>
      </DrawerFooter>
    </Form>
  );

  render() {
    return (
      <Drawer isOpen={this.props.isDrawerOpen} onClose={this.props.closeDrawer}>
        <Formik
          validationSchema={this.validationSchema}
          onSubmit={this.onSubmit}
          initialValues={formInitialValues}
        >
          {this.innerForm}
        </Formik>
      </Drawer>
    );
  }
}

const mapStateToProps = (store: RootStore) => ({
  isDrawerOpen: isDrawerOpen(store.app.drawer, DRAWER_ID),
  isSaving: getLoading(store.app.loadings, PROJECT_TYPES.CREATE_PROJECT_REQUEST)
});
const mapDispatchToProps = {
  closeDrawer: () => closeDrawer(DRAWER_ID),
  createProject: (project: Partial<Project>) => createProjectRequest(project)
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NewProjectForm);
