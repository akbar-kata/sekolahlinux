import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import classnames from 'classnames';

import Dashboard from 'components/Dashboard/Dashboard';
import * as env from 'utils/env';
import DashboardCards from 'components/Dashboard/DashboardCards';
import Card from 'components/Card/Card';
import CardButton from 'components/Card/CardButton';
import { randomizeAvatarWithSeed } from 'utils/avatars';
import Button from 'components/Button/Button';
import { DataMap } from 'interfaces/common';
import { Project, Template } from 'interfaces/project';
import RootStore from 'interfaces/rootStore';
import {
  getProjects,
  getProjectIndexes,
  getTemplates,
  getTemplateIndexes,
  projectHasNextPage
} from 'stores/project/selectors';
import {
  selectProject,
  deleteProjectRequest,
  loadMoreProjectRequest
} from 'stores/project/actions';
import NewProjectForm, {
  DRAWER_ID as NEW_FORM_DRAWER_ID
} from './NewProjectForm';
import { openDrawer } from 'stores/app/drawer/actions';
import EditProjectForm, {
  DRAWER_ID as EDIT_FORM_DRAWER_ID
} from './EditProjectForm';
import confirm from 'components/Modal/ConfirmDialog';
import {
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from 'components/Dropdown';
import LoadMoreIcon from 'components/Icon/LoadMoreIcon';
import ProfileMenu from 'modules/core/ProfileMenu/Container';
import get from 'lodash-es/get';

import './Page.scss';

const tutorialUrl = env.getRuntimeEnv(
  'REACT_APP_RUNTIME_BOTSTUDIO_TUTORIAL_URL',
  env.defaultEnvs['REACT_APP_RUNTIME_BOTSTUDIO_TUTORIAL_URL']
);
const docsUrl = env.getRuntimeEnv(
  'REACT_APP_RUNTIME_DOCS_URL',
  env.defaultEnvs['REACT_APP_RUNTIME_DOCS_URL']
);

const HeaderContent = (
  <Fragment>
    <p className="kata-dashboard__paragraph mb-2">
      Kata Platform brings together all of the elements to create intelligent
      chatbot in a single, integrated platform. Built with scalability in mind,
      its engine allows you to design, train, and manage chatbot without
      compromise.
    </p>
    <div className="kata-dashboard__icons-container">
      <div className="kata-dashboard__icon">
        <a href={tutorialUrl} target="_blank">
          <i className="icon-video" />
          <span className="kata-dashboard__icon-text">Tutorial</span>
        </a>
      </div>
      <div className="kata-dashboard__icon">
        <a href={docsUrl} target="_blank">
          <i className="icon-docs" />
          <span className="kata-dashboard__icon-text">Documentation</span>
        </a>
      </div>
    </div>
  </Fragment>
);

interface PropsFromStore {
  projects: DataMap<Project>;
  projectIndexes: string[];
  projectHasNextPage: boolean;
  templates: DataMap<Template>;
  templateIndexes: string[];
}
interface PropsFromDispatch {
  selectProject: (projectId: string) => void;
  openNewProjectFormDrawer: () => any;
  openEditProjectFormDrawer: () => any;
  deleteProject: (projectId: string) => any;
  loadMore: () => any;
}
interface OwnProps {}

interface States {
  editProject: Project | null;
}

type ComponentProps = PropsFromStore & PropsFromDispatch & OwnProps;

class Page extends React.Component<ComponentProps, States> {
  state: States = {
    editProject: null
  };

  deleteProject = (projectId: string) => {
    confirm({
      title: 'Delete Project',
      message: 'Are you sure want to delete this project?',
      okLabel: 'Delete'
    }).then(result => {
      if (result) {
        this.props.deleteProject(projectId);
      }
    });
  };
  openNewProjectFormDrawer = () => {
    this.props.openNewProjectFormDrawer();
  };
  editProject = (project: Project) => {
    this.setState(
      {
        editProject: project
      },
      () => this.props.openEditProjectFormDrawer()
    );
  };
  handleLoadMore = () => {
    this.props.loadMore();
  };
  render() {
    return (
      <Fragment>
        <div className="kata-project-selector pt-2">
          <ProfileMenu />
        </div>
        <Dashboard
          title="Welcome to Kata | Platform"
          headerContent={HeaderContent}
          className="kata-botstudio-dashboard"
          image={require('assets/images/kata-platform-small.svg')}
          isStarter
        >
          {/* <h1 className="kata-dashboard__content-header--starter">
            Project Templates
          </h1>
          <DashboardCards>
            {this.props.templateIndexes.map(templateIndex => (
              <Card
                key={templateIndex}
                className=""
                title={this.props.templates[templateIndex].name}
                avatar={randomizeAvatarWithSeed(0)}
                flex
              >
                <div className="">
                  <p className="text-muted">No description specified.</p>
                </div>

                <div className="mb-2 mt-2">
                  <Button color="primary" block>
                    Create with This Template
                  </Button>
                </div>
              </Card>
            ))}
          </DashboardCards> */}

          <h1 className="kata-dashboard__content-header--starter">
            Your Project
          </h1>
          <DashboardCards>
            <Card asButton className="kata-project-card justify-content-center">
              <CardButton
                label={'Create Project'}
                icon="add"
                onClick={this.openNewProjectFormDrawer}
              />
            </Card>
            {/* TODO: `(untitled)` is temporary until we can fix the empty names */}
            {this.props.projectIndexes.map((projectIndex, index) => (
              <Card
                key={`${projectIndex}_${index}`}
                className="kata-project-card"
                title={this.props.projects[projectIndex].name || '(untitled)'}
                avatar={randomizeAvatarWithSeed(
                  this.props.projects[projectIndex].id
                )}
                onClick={() => this.props.selectProject(projectIndex)}
                flex
                action={
                  <Dropdown>
                    <DropdownToggle caret={false}>
                      <Button isIcon>
                        <i className="icon-more" />
                      </Button>
                    </DropdownToggle>
                    <DropdownMenu right>
                      <DropdownItem
                        onClick={() =>
                          this.editProject(this.props.projects[projectIndex])
                        }
                      >
                        Update
                      </DropdownItem>
                      <DropdownItem
                        onClick={() => this.deleteProject(projectIndex)}
                      >
                        Delete
                      </DropdownItem>
                    </DropdownMenu>
                  </Dropdown>
                }
              >
                <div className="kata-card--light-text">
                  {this.props.projects[projectIndex].description ? (
                    <p className="text-muted">
                      {this.props.projects[projectIndex].description}
                    </p>
                  ) : (
                    <p>No description specified.</p>
                  )}
                </div>
                <div
                  className={classnames(
                    get(this.props, `projects.${projectIndex}.environments`, [])
                      .length && 'kata-table--deployment-container'
                  )}
                >
                  <table className="kata-light-table kata-project-card__table">
                    <thead className="kata-project-card__table-header">
                      <tr>
                        <th className="kata-project-card__table-header--deployment">
                          Deployment Version
                        </th>
                        <th className="kata-project-card__table-header--environment">
                          Environment
                        </th>
                      </tr>
                    </thead>
                    <tbody className="kata-project-card__table-body">
                      {/*
                       * HACK: backend gives out dev-stag-prod while it should've been prod-stag-dev, so temporarily we
                       * reverse it.
                       */}
                      {get(
                        this.props,
                        `projects.${projectIndex}.environments`,
                        []
                      ).length ? (
                        this.props.projects[projectIndex].environments
                          .reverse()
                          .map((environtment, environtmentIndex) => (
                            <tr key={environtmentIndex}>
                              <td>{environtment.deploymentVersion}</td>
                              <td title={environtment.name}>
                                {environtment.name}
                              </td>
                            </tr>
                          ))
                      ) : (
                        <tr>
                          <td>-</td>
                          <td>-</td>
                        </tr>
                      )}
                    </tbody>
                  </table>
                </div>
              </Card>
            ))}
          </DashboardCards>
          {this.props.projectHasNextPage ? (
            <div className="text-center">
              <Button
                className="kata-btn-loadmore"
                onClick={this.handleLoadMore}
              >
                <LoadMoreIcon className="icon-spinner mr-1" />
                Load More
              </Button>
            </div>
          ) : null}

          <NewProjectForm />
          {this.state.editProject ? (
            <EditProjectForm project={this.state.editProject} />
          ) : null}
        </Dashboard>
      </Fragment>
    );
  }
}

const mapStateToProps = (store: RootStore): PropsFromStore => ({
  projects: getProjects(store.project),
  projectIndexes: getProjectIndexes(store.project),
  projectHasNextPage: projectHasNextPage(store.project),
  templates: getTemplates(store.project),
  templateIndexes: getTemplateIndexes(store.project)
});

const mapDispatchToProps = {
  selectProject: projectId => selectProject(projectId),
  openNewProjectFormDrawer: () => openDrawer(NEW_FORM_DRAWER_ID),
  openEditProjectFormDrawer: () => openDrawer(EDIT_FORM_DRAWER_ID),
  deleteProject: (projectId: string) => deleteProjectRequest(projectId),
  loadMore: () => loadMoreProjectRequest()
};

export default connect<PropsFromStore, PropsFromDispatch, OwnProps, RootStore>(
  mapStateToProps,
  mapDispatchToProps
)(Page);
