import React from 'react';

import timezones from 'components/utils/timezones';
import ReactSelect from 'components/FormikWrapper/ReactSelect';
import Field from 'components/FormikWrapper/Field';

import { langOpts, visibilityOpts } from './utils/selectValues';

interface ProjectFormProps {
  isEditing?: boolean;
}

const ProjectForm: React.SFC<ProjectFormProps> = ({ isEditing }) => (
  <React.Fragment>
    <div className="mt4 mb-2">
      <label className="control-label kata-form__label">Name</label>
      <Field
        autoFocus
        name="name"
        className="form-control kata-form__input-text"
        disabled={isEditing}
      />
    </div>
    <div className="mt4 mb-2">
      <label className="control-label kata-form__label">Description</label>
      <Field
        component="textarea"
        name="description"
        className="form-control kata-form__input-textarea"
        rows={4}
      />
    </div>
    <div className="mt4 mb-2">
      <label className="control-label kata-form__label">Timezone</label>
      <ReactSelect
        name="options.timezone"
        options={timezones}
        clearable={false}
        disabled={isEditing}
        simpleValue
      />
    </div>
    <div className="mt4 mb-2">
      <label className="control-label kata-form__label">NLU Language</label>
      <ReactSelect
        name="options.nluLang"
        options={langOpts}
        clearable={false}
        disabled={isEditing}
        simpleValue
      />
    </div>
    <div className="mt4 mb-2">
      <label className="control-label kata-form__label">NLU Visibility</label>
      <ReactSelect
        name="options.nluVisibility"
        options={visibilityOpts}
        clearable={false}
        disabled={isEditing}
        simpleValue
      />
    </div>
  </React.Fragment>
);

export default ProjectForm;
