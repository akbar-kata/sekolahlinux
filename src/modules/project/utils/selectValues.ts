export const langOpts = [
  {
    label: 'Bahasa Indonesia',
    value: 'id'
  },
  {
    label: 'English',
    value: 'en'
  }
  // {
  //     label: 'Deutsch',
  //     value: 'de'
  // }
];

export const visibilityOpts = [
  {
    label: 'Private',
    value: 'private'
  },
  {
    label: 'Public',
    value: 'public'
  }
];
