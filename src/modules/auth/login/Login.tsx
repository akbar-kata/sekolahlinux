import React, { Fragment } from 'react';
import { Redirect, Link } from 'react-router-dom';

import { Button } from 'components/Button';
import { Board } from 'components/Board';
import { Banner } from 'components/Banner';
import * as env from 'utils/env';
import { LoginContainerProps } from './LoginContainer';

export const KATA_EMPFANG_URL = env.getRuntimeEnv(
  'REACT_APP_RUNTIME_EMPFANG_URL',
  env.defaultEnvs['REACT_APP_RUNTIME_EMPFANG_URL']
);

const Logo = require('assets/images/logo-platform.png');

interface LoginProps extends LoginContainerProps {}

interface LoginState {
  username: string;
  password: string;
  remember: boolean;
}

const FooterChildren = (
  <Fragment>
    <p className="text-center mx-0 my-0">
      Don't have an account?{' '}
      <a href={KATA_EMPFANG_URL} className="text-bold">
        Sign Up
      </a>
    </p>
  </Fragment>
);

export default class Login extends React.Component<LoginProps, LoginState> {
  constructor(props: LoginProps) {
    super(props);

    this.state = {
      username: '',
      password: '',
      remember: false
    };
  }

  login = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    this.props.loginAction(
      this.state.username,
      this.state.password,
      this.state.remember
    );
  };

  inputChange = (name: string) => {
    return (event: React.ChangeEvent<HTMLInputElement>) => {
      const { target } = event;
      const value = target.type === 'checkbox' ? target.checked : target.value;
      const newState = {};

      newState[name] = value;
      this.setState(newState);
    };
  };

  render() {
    const { from } = this.props.location.state || {
      from: { pathname: '/' }
    };
    const { isLoading, isLoggedIn, error } = this.props;
    const { username, password } = this.state;

    if (isLoggedIn) {
      return <Redirect to={from} />;
    }

    return (
      <div className="auth-wrapper__login">
        <div className="auth-wrapper__inner">
          <Board className="auth-container" footerChildren={FooterChildren}>
            <div className="text-center mb-5">
              <img src={Logo} className="auth-logo" alt="logo" />
            </div>
            {error && <Banner className="mb-2" state="error" message={error} />}
            <form className="kata-form" onSubmit={this.login}>
              <div className="kata-form__element input-group">
                <div className="input-group-prepend">
                  <span className="input-group-text input-group-icon">
                    <i className="icon-account" />
                  </span>
                </div>
                <input
                  type="text"
                  className="form-control kata-form__input-text"
                  placeholder="Username"
                  value={username}
                  onChange={this.inputChange('username')}
                  data-test-id="auth-login-username"
                />
              </div>
              <div className="kata-form__element input-group">
                <div className="input-group-prepend">
                  <span className="input-group-text input-group-icon">
                    <i className="icon-password" />
                  </span>
                </div>
                <input
                  type="password"
                  placeholder="Password"
                  className="form-control kata-form__input-text"
                  value={password}
                  onChange={this.inputChange('password')}
                  data-test-id="auth-login-password"
                />
              </div>
              <div className="clearfix mb-3">
                <div className="form-check float-left">
                  <input
                    type="checkbox"
                    className="form-check-input"
                    onChange={this.inputChange('remember')}
                    id="remember-me"
                  />
                  <label className="input-check-label" htmlFor="remember-me">
                    Remember me
                  </label>
                </div>
                <div className="float-right">
                  <Link to="/forgot">Forgot password?</Link>
                </div>
              </div>
              <Button
                type="submit"
                color="primary"
                block
                disabled={!username || !password}
                loading={isLoading}
                data-test-id="auth-login-submit"
              >
                Login
              </Button>
            </form>
          </Board>
          <div className="auth-footer">
            <p className="text-center">
              Presented by <a href="https://kata.ai/">Kata.ai</a>
            </p>
          </div>
        </div>
      </div>
    );
  }
}
