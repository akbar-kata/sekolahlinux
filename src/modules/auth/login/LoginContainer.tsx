import React from 'react';
import { Dispatch } from 'redux';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router-dom';

// import { auth as authAction } from '../core/stores/actions';
// import RootStore from 'interfaces/rootStore';
import { requestAuth, validateAuth } from 'stores/user/auth/actions';
import {
  getAuthError,
  getAuthLoading,
  getIsAuthenticated
} from 'stores/user/auth/selectors';
import Login from './Login';

export interface LoginContainerProps extends RouteComponentProps<{}> {
  isLoading: boolean;
  isLoggedIn: boolean;
  error: string | false;
  loginAction: (username: string, password: string, remember?: boolean) => any;
  checkLogin: () => any;
}

const LoginContainer = (props: LoginContainerProps) => <Login {...props} />;

const mapStateToProps = ({ auth }) => {
  return {
    isLoggedIn: getIsAuthenticated(auth),
    isLoading: getAuthLoading(auth),
    error: getAuthError(auth)
  };
};

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    checkLogin: () => dispatch(validateAuth()),
    loginAction: (username: string, password: string, remember?: boolean) =>
      dispatch(requestAuth(username, password, remember))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginContainer);
