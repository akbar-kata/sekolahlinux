import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

// import { auth as authAction } from '../core/stores/actions';
import { clearAuthRequest } from 'stores/user/auth/actions';

export const LogoutButton = withRouter(({ history, logout }: any) => (
  <div
    className="dropdown-item kata-dropdown__item"
    onClick={e => {
      e.preventDefault();
      logout();
    }}
  >
    Sign out
  </div>
));

const mapStateToProps = ({ auth }: any) => {
  return {};
};

const mapDispatchToProps = (dispatch: Function) => {
  return {
    logout: () => dispatch(clearAuthRequest())
  };
};

export default connect<any, any, any>(
  mapStateToProps,
  mapDispatchToProps
)(LogoutButton);
