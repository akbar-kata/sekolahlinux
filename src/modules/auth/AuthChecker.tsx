import React from 'react';
import { connect } from 'react-redux';

// import { auth as authAction } from '../core/stores/actions';
import { validateAuth } from 'stores/user/auth/actions';

class AuthChecker extends React.Component<any, any> {
  componentDidMount() {
    this.props.checkLogin();
  }

  render() {
    return null;
  }
}

const mapStateToProps = ({ auth }: any) => {
  return {};
};

const mapDispatchToProps = (dispatch: Function) => {
  return {
    checkLogin: () => dispatch(validateAuth(true))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AuthChecker);
