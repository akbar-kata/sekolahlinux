import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import { getIsAuthenticated } from 'stores/user/auth/selectors';

// Mock of an Auth method, can be replaced with an async call to the backend. Must return true or false
const PrivateRoute = ({ component: Component, isLoggedIn, ...rest }: any) => {
  return (
    <Route
      {...rest}
      render={props =>
        isLoggedIn ? (
          <Component {...props} />
        ) : (
          <Redirect
            to={{
              pathname: '/login',
              state: { from: props.location }
            }}
          />
        )
      }
    />
  );
};

const mapStateToProps = ({ auth }: any) => {
  return {
    isLoggedIn: getIsAuthenticated(auth)
  };
};

const mapDispatchToProps = (dispatch: Function) => {
  return {};
};

export default connect<any, any, any>(
  mapStateToProps,
  mapDispatchToProps
)(PrivateRoute);
