import React from 'react';
import classnames from 'classnames';
import { Route, RouteComponentProps } from 'react-router-dom';

import Login from './login/LoginContainer';
import Forgot from './forgot-password/Forgot';
import ChangePassword from './forgot-password/ChangePassword';

import './auth.scss';

interface AuthProps extends RouteComponentProps<{}> {}

class Auth extends React.Component<AuthProps> {
  isLoginPage(props: AuthProps) {
    return props.location.pathname.match(/^\/login/);
  }

  render() {
    return (
      <div
        className={classnames(
          'auth-wrapper',
          !this.isLoginPage(this.props) && 'auth-wrapper--not-root'
        )}
      >
        <div className="auth-wrapper__welcome">
          <div className="auth-wrapper__welcome-hero">
            <img src={require('assets/images/kata-platform.svg')} />
            <h2 className="auth-wrapper__welcome-title">
              Welcome to Kata Platform
            </h2>
            <h3 className="auth-wrapper__welcome-subtitle">
              Kata | Platform brings together all of the elements to create
              intelligent chatbot in a single, integrated platform. Built with
              scalability in mind, its engine allows you to design, train, and
              manage chatbot without compromise.
            </h3>
            <div className="auth-wrapper__welcome-links">
              <a
                href="https://docs.kata.ai/tutorial/bot-studio/"
                target="_blank"
                className="kata-btn-landing"
              >
                See Tutorial
              </a>
              <a
                href="https://docs.kata.ai/concepts/bot/"
                target="_blank"
                className="kata-btn-landing"
              >
                View Docs
              </a>
            </div>
          </div>
        </div>
        <Route path="/login" component={Login} />
        <Route path="/forgot" component={Forgot} />
        <Route path="/change-password/:token" component={ChangePassword} />
      </div>
    );
  }
}

export default Auth;
