import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { Formik, Form, FormikProps, Field } from 'formik';
import * as Yup from 'yup';
import classnames from 'classnames';
import { Redirect, Link, RouteComponentProps } from 'react-router-dom';
import jwtDecode from 'jwt-decode';

import RootStore from 'interfaces/rootStore';
import { Button } from 'components/Button';
import { Board } from 'components/Board';
import { Banner } from 'components/Banner';
import { requestUpdatePassword } from 'stores/user/account/actions';
import { Data as User } from 'interfaces/user';
import { Bag } from 'interfaces/account';
import { requestAuth } from 'stores/user/auth/actions';
import { getIsAuthenticated } from 'stores/user/auth/selectors';
import { getAuthError } from 'stores/signup/selectors';
import { Circle } from 'components/Loading';

interface FormValues {
  newPassword: string;
  confirmPassword: string;
}

interface RouteParams {
  token: string;
}

interface PropsFromDispatch {
  loginAction: typeof requestAuth;
  updatePassword: typeof requestUpdatePassword;
}

interface PropsFromStore {
  isLoading: boolean;
  isLoggedIn: boolean;
  credentials: User;
  errorBags?: Bag;
  success: any;
  error: string | false;
}

interface OtherProps extends RouteComponentProps<RouteParams> {}

type Props = PropsFromStore & PropsFromDispatch & OtherProps;

interface States {
  showMatch: string;
  confirmPassword: string;
  newPassword: string;
  otp: string;
  isLoading: boolean;
  error: string | null;
  success: string | null;
}

const Logo = require('assets/images/logo-platform.png');
const PasswordWarning = require('../imgs/password-warn.svg');

const validationSchema = Yup.object().shape({
  newPassword: Yup.string()
    .min(8, 'New password must be longer than 8 characters')
    .required('Please enter your new password.'),
  confirmPassword: Yup.string()
    .required('Please enter your new password again.')
    .oneOf([Yup.ref('newPassword')], `Passwords don't match`)
});

class ChangePassword extends React.Component<Props, States> {
  constructor(props: Props) {
    super(props);
    this.state = {
      showMatch: '',
      confirmPassword: '',
      newPassword: '',
      otp: '',
      isLoading: false,
      error: null,
      success: null
    };
  }

  componentDidMount() {
    const { token } = this.props.match.params;
    const data = jwtDecode(token);
    const { username, otp } = data.data;
    this.props.loginAction(username, otp);
    this.setState({ otp });
  }

  componentWillReceiveProps(nextProps: Props) {
    if (nextProps.isLoading) {
      this.setState({ isLoading: false });
    }
    if (nextProps.success) {
      setTimeout(() => {
        return <Redirect to="/" />;
      }, 1000);
    }
  }

  onSubmit = (values: FormValues) => {
    const { newPassword } = values;

    this.setState({ isLoading: true }, () => {
      this.props.updatePassword(
        this.state.otp,
        newPassword,
        this.props.credentials
      );
    });
  };

  innerForm = ({
    errors,
    values,
    touched,
    isSubmitting
  }: FormikProps<FormValues>) => {
    return (
      <Form className="kata-form">
        <div
          className={classnames(
            'kata-form__element',
            !!touched.newPassword && !!errors.newPassword && 'has-error'
          )}
        >
          <div className="input-group">
            <div className="input-group-prepend">
              <span className="input-group-text input-group-icon">
                <i className="icon-password" />
              </span>
            </div>
            <Field
              type="password"
              name="newPassword"
              placeholder="New Password"
              className="form-control kata-form__input-text"
            />
          </div>
          {errors.newPassword && touched.newPassword ? (
            <div className="kata-form__error-label">{errors.newPassword}</div>
          ) : null}
        </div>
        <div
          className={classnames(
            'kata-form__element',
            !!touched.confirmPassword && !!errors.confirmPassword && 'has-error'
          )}
        >
          <div className="input-group">
            <div className="input-group-prepend">
              <span className="input-group-text input-group-icon">
                <i className="icon-password" />
              </span>
            </div>
            <Field
              type="password"
              name="confirmPassword"
              placeholder="Confirm Password"
              className="form-control kata-form__input-text"
            />
          </div>
          {errors.confirmPassword && touched.confirmPassword ? (
            <div className="kata-form__error-label">
              {errors.confirmPassword}
            </div>
          ) : null}
        </div>
        <div className="mt4">
          <Button
            type="submit"
            color="primary"
            disabled={
              !values.newPassword ||
              !values.confirmPassword ||
              !!errors.newPassword ||
              !!errors.confirmPassword
            }
            loading={isSubmitting}
            block
          >
            Change Password
          </Button>
        </div>
      </Form>
    );
  };

  validateInputs = ({ confirmPassword, newPassword }) => {
    return {
      confirmPassword: !confirmPassword
        ? 'Confirm password is required'
        : undefined,
      newPassword: !newPassword ? 'New password is required' : undefined
    };
  };

  renderSuccess = () => {
    return (
      <Fragment>
        <div className="text-center mt-3">
          <img
            src={PasswordWarning}
            className="auth-img"
            alt="Reset Link Delivered"
          />
        </div>
        <div className="text-center mt-3">
          <h4 className="heading4 text-success">New Password Set</h4>
          <p className="paragraph">
            You will be redirected to the login page in 3 seconds.
          </p>
          <Link
            className="btn kata-btn kata-btn__primary btn-primary btn-block"
            to="/login"
          >
            Go to Login
          </Link>
        </div>
      </Fragment>
    );
  };

  renderWarning = () => {
    return (
      <Banner
        className="mb-2"
        state="error"
        message="Change your password now."
      />
    );
  };

  renderError = () => {
    return (
      <Fragment>
        <div className="text-center mt-3">
          <img
            src={PasswordWarning}
            className="auth-img"
            alt="One Time Password Expired"
          />
        </div>
        <div className="text-center mt-3">
          <h4 className="heading4 text-danger">One Time Password Expired</h4>
          <p className="paragraph">
            Your one time password has expired. Please request a new one time
            pasword <Link to="/forgot">here</Link>.
          </p>
        </div>
      </Fragment>
    );
  };

  renderFooter = () => (
    <Fragment>
      <p className="text-center">
        Don't have an account?{' '}
        <a href="https://user.kata.ai/signup" className="text-bold">
          Sign Up
        </a>
      </p>
      <p className="text-center m-0">
        Already have account?{' '}
        <Link to="/login" className="text-bold">
          Sign in
        </Link>
      </p>
    </Fragment>
  );

  render() {
    const { isLoggedIn, error, success } = this.props;
    return (
      <div className="auth-wrapper__change-password">
        <div className="auth-wrapper__inner">
          <Board className="auth-container">
            <div className="text-center mb-5">
              <img src={Logo} className="auth-logo" alt="logo" />
            </div>
            {error ? (
              this.renderError()
            ) : success ? (
              this.renderSuccess()
            ) : (
              <Fragment>
                <div className="text-center mt-3">
                  <h4 className="heading4">Create New Password</h4>
                </div>
                <div className="mt-2">
                  {isLoggedIn && !success ? (
                    <Formik
                      initialValues={{
                        newPassword: '',
                        confirmPassword: ''
                      }}
                      validationSchema={validationSchema}
                      onSubmit={this.onSubmit}
                    >
                      {this.innerForm}
                    </Formik>
                  ) : !error ? (
                    <div className="my-3 text-center">
                      <Circle />
                    </div>
                  ) : null}
                </div>
              </Fragment>
            )}
          </Board>
          <div className="auth-footer">
            <p className="text-center">
              Presented by <a href="https://kata.ai/">Kata.ai</a>
            </p>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: RootStore) => {
  return {
    success: state.account.messages ? state.account.messages.password : '',
    isLoading: state.account.isLoading,
    credentials: state.auth.token ? state.auth.token.detail : null,
    errorBags: state.account.errors,
    isLoggedIn: getIsAuthenticated(state.auth),
    error: getAuthError(state.auth)
  };
};

const mapDispatchToProps = (dispatch: Function) => {
  return {
    loginAction: (username: string, password: string) =>
      dispatch(requestAuth(username, password)),
    updatePassword: (confirmPassword, newPassword, credentials) =>
      dispatch(requestUpdatePassword(confirmPassword, newPassword, credentials))
  };
};

export default connect<
  PropsFromStore,
  PropsFromDispatch,
  OtherProps,
  RootStore
>(
  mapStateToProps,
  mapDispatchToProps
)(ChangePassword);
