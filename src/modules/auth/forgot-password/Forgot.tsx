import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';

import { request } from 'stores/services';

import { Board } from 'components/Board';
import { Button } from 'components/Button';
import { Banner } from 'components/Banner';

interface Props {}

interface States {
  email: string;
  isLoading: boolean;
  error: string | null;
  success: string | null;
}

const Logo = require('assets/images/logo-platform.png');
const ResetLink = require('../imgs/reset-link.svg');

const FooterChildren = (
  <Fragment>
    <p className="auth-back-button text-left my-0">
      <Link to="/login">
        <i className="icon-arrow-left mr-1" /> Back to Login
      </Link>
    </p>
  </Fragment>
);

export default class Forgot extends React.Component<Props, States> {
  constructor(props: Props) {
    super(props);
    this.state = {
      email: '',
      isLoading: false,
      error: null,
      success: null
    };
  }

  forgot = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    if (!this.state.email || this.state.email === '') {
      this.setState({
        isLoading: false,
        error: 'Please enter your username or email.'
      });
    } else {
      this.setState({ isLoading: true, error: null, success: null });
      request('post', 'forgot', {
        username: this.state.email
      })
        .then(response => {
          let error: string | null = null;
          let success: string | null = null;
          let email = '';
          if (response.data) {
            success = 'Check your email to reset password.';
          } else {
            email = this.state.email;
            error = 'Username or Email not registered';
          }
          this.setState({ error, success, email, isLoading: false });
        })
        .catch(err => {
          this.setState({ isLoading: false, error: err.message });
        });
    }
  };

  inputChange = (name: string) => {
    return (event: any) => {
      const newState = {};
      newState[name] = event.target.value;
      this.setState(newState);
    };
  };

  render() {
    const { email, isLoading, error, success } = this.state;

    return (
      <div className="auth-wrapper__forgot-password">
        <div className="auth-wrapper__inner">
          <Board className="auth-container" footerChildren={FooterChildren}>
            <div className="text-center mb-5">
              <img src={Logo} className="auth-logo" alt="logo" />
            </div>
            {!success ? (
              <Fragment>
                <div className="text-center mt-3">
                  <h4 className="heading4">Reset Password</h4>
                  <p className="paragraph">
                    Enter the username or email address associated with your
                    account, and we will email you a link to reset your
                    password.
                  </p>
                </div>

                {error && (
                  <Banner className="mb-2" state="error" message={error} />
                )}

                <form className="kata-form" onSubmit={this.forgot}>
                  <div className="kata-form__element input-group">
                    <div className="input-group-prepend">
                      <span className="input-group-text input-group-icon">
                        <i className="icon-account" />
                      </span>
                    </div>
                    <input
                      type="text"
                      className="form-control kata-form__input-text"
                      placeholder="Username or Email"
                      value={email}
                      onChange={this.inputChange('email')}
                    />
                  </div>
                  <Button
                    type="submit"
                    color="primary"
                    block
                    disabled={!email}
                    loading={isLoading}
                  >
                    Send
                  </Button>
                </form>
              </Fragment>
            ) : (
              <Fragment>
                <div className="text-center mt-3">
                  <img
                    src={ResetLink}
                    className="auth-img"
                    alt="Reset Link Delivered"
                  />
                </div>
                <div className="text-center mt-3">
                  <h4 className="heading4 text-success">
                    Reset Link Delivered
                  </h4>
                  <p className="paragraph">
                    Reset link sent! Please check your email.
                  </p>
                </div>
              </Fragment>
            )}
          </Board>
          <div className="auth-footer">
            <p className="text-center">
              Presented by <a href="https://kata.ai/">Kata.ai</a>
            </p>
          </div>
        </div>
      </div>
    );
  }
}
