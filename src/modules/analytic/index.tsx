import React from 'react';
import { Route, Switch } from 'react-router-dom';

import Maintenance from 'modules/analytic/Maintenance';

export default () => (
  <Switch>
    <Route path="" component={Maintenance} />
  </Switch>
);
