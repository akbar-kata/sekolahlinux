import * as React from 'react';
import EmptyMessage from 'components/Common/EmptyMessage';
import withSidebar, {
  WithSidebarDispatches,
  WithSidebarStates
} from 'stores/app/sidebar/context';

const style = {
  paddingTop: '48px'
};

type MaintenanceProps = WithSidebarStates & WithSidebarDispatches;

class Maintenance extends React.Component<MaintenanceProps> {
  render() {
    return (
      <div className="maintenance-board" style={style}>
        <EmptyMessage
          autoSize
          image={require('assets/images/maintenance-analytic.svg')}
          title="Analytics Report Coming Soon"
        >
          Your data is being recorded, once we have enough we will display it
          here.
        </EmptyMessage>
      </div>
    );
  }
}

export default withSidebar(Maintenance);
