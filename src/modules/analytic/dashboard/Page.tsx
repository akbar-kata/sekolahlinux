import React, { Fragment } from 'react';
import { connect } from 'react-redux';

import RootStore from 'interfaces/rootStore';
import * as Bot from 'interfaces/bot';
import SelectedUrl from 'containers/SelectedUrl';
import { Dashboard, DashboardCards } from 'components/Dashboard';
import { Board } from 'components/Board';

import {
  activateAnalytic,
  checkAnalyticEnabled
} from 'stores/analytic/root/actions';
import { selectBot } from 'stores/bot/actions';
import { getBotIndex, getBotData } from 'stores/bot/selectors';
import { Link } from 'react-router-dom';
import { closeSidebar } from 'stores/app/sidebar/actions';

import './Page.scss';
import { EmptyMessage } from 'components/Common';
import BotList from './BotList';

interface StateFromStore {
  data: Bot.BotMap;
  index: string[];
}

interface DispatchFromStore {
  selectBot(botId: string): void;
  enableAnalytics(
    botId: string,
    deploymentId: string,
    type: 'basic' | 'advanced',
    metrics: string[]
  ): void;
  checkAnalytics(botId: string, deploymentId: string): void;
  closeSidebar(): void;
}

interface Props extends DispatchFromStore, StateFromStore {
  advancedOnly?: boolean;
}

const HeaderContent = (
  <p className="kata-dashboard__paragraph">
    Choose the bot to see the analytics data for that bot. You have to opt-in
    for analytics to see analytics data. Don't worry, you can always change your
    analytics option later.
  </p>
);

class Page extends React.Component<Props> {
  componentDidUpdate() {
    // this.props.checkAnalytics(this.props.botId, this.props.deploymentId);
  }

  renderNoDeploymentRedirection() {
    return (
      <Board className="text-center">
        <EmptyMessage
          image={require('assets/images/no-channel.svg')}
          title="No Deployments"
        >
          <SelectedUrl>
            {selectedId => (
              <Fragment>
                It seems that you have no deployments available.{' '}
                <Link
                  to={`/botstudio/${selectedId}/deployment`}
                  onClick={this.redirectToDeployment}
                >
                  Create a deployment
                </Link>{' '}
                to activate analytics plan.
              </Fragment>
            )}
          </SelectedUrl>
        </EmptyMessage>
      </Board>
    );
  }

  renderNoChannelRedirection = () => {
    return (
      <Board className="text-center">
        <EmptyMessage
          image={require('assets/images/no-channel.svg')}
          title="No Channels"
        >
          <SelectedUrl>
            {selectedId => (
              <Fragment>
                It seems that you have no channels available.{' '}
                <Link
                  to={`/botstudio/${selectedId}/deployment`}
                  onClick={this.redirectToDeployment}
                >
                  Create a channel
                </Link>{' '}
                to activate analytics plan.
              </Fragment>
            )}
          </SelectedUrl>
        </EmptyMessage>
      </Board>
    );
  };

  renderNoBotSelected = () => {
    return (
      <Board className="text-center">
        <EmptyMessage
          image={require('assets/images/no-channel.svg')}
          title="No Bot Selected"
        >
          You have not selected a bot. Please select one from the sidebar on the
          left.
        </EmptyMessage>
      </Board>
    );
  };

  render() {
    const { index, data } = this.props;

    return (
      <Dashboard
        isStarter
        title="Analytics"
        headerContent={HeaderContent}
        className="kata-analytics-dashboard"
        image={require('assets/images/analytics-opt-in.svg')}
      >
        <h1 className="kata-dashboard__content-header--starter">Your Bot</h1>
        <DashboardCards>
          {index.length
            ? Object.keys(data).map((key: string) => {
                const currentData = data[key];

                return (
                  <BotList
                    key={currentData.id}
                    selectBot={this.props.selectBot}
                    {...currentData}
                  />
                );
              })
            : null}
        </DashboardCards>
      </Dashboard>
    );
  }

  redirectToDeployment = () => {
    // this.props.changeToDeployment();
  };
}

const mapStateToProps = ({ bot }: RootStore): StateFromStore => {
  return {
    index: getBotIndex(bot),
    data: getBotData(bot)
  };
};

const mapDispatchToProps = dispatch => {
  return {
    selectBot: (projectId: string, revisionId: string) =>
      dispatch(selectBot(projectId, revisionId)),
    closeSidebar() {
      dispatch(closeSidebar());
    },
    enableAnalytics(
      botId: string,
      deploymentId: string,
      type: 'basic' | 'advanced',
      metrics: string[]
    ) {
      dispatch(activateAnalytic(botId, deploymentId, type, metrics));
    },
    checkAnalytics(botId: string, deploymentId: string) {
      dispatch(checkAnalyticEnabled(botId, deploymentId));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Page);
