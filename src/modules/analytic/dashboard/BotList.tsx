import React from 'react';
import { Button } from 'components/Button';
import Card from 'components/Card';
import { randomizeAvatarWithSeed } from 'utils/avatars';

interface Props {
  id: string;
  name: string;
  desc: string;
  version: string;
  openConfig?: any;
  selectBot(botId: string): void;
}

const BotList: React.SFC<Props> = props => {
  return (
    <Card
      className="kata-botstudio-dashboard__card"
      title={props.name}
      avatar={randomizeAvatarWithSeed(props.id)}
      onClick={() => props.selectBot(`${props.id}`)}
      action={
        <Button isIcon onClick={() => props.openConfig(props)}>
          <i className="icon-settings-cms" />
        </Button>
      }
    >
      <div className="kata-botstudio-dashboard__card-content">
        {props.desc ? (
          <p>{props.desc}</p>
        ) : (
          <p className="text-muted">No description specified.</p>
        )}
      </div>
    </Card>
  );
};

export default BotList;
