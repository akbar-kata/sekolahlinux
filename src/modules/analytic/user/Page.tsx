import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import RootStore from 'interfaces/rootStore';

import SelectedUrl from 'containers/SelectedUrl';

import {
  activateAnalytic,
  checkAnalyticEnabled
} from 'stores/analytic/root/actions';
import { getBotSelected } from 'stores/bot/selectors';
import { selectDeployment } from 'stores/deployment/actions';
import { getDeploymentSelected } from 'stores/deployment/selectors';
import {
  getChannelIndex,
  getChannelLoading
} from 'stores/deployment/channel/selectors';

import { Dashboard } from 'components/Dashboard';
import { Board } from 'components/Board';
import { EmptyMessage } from 'components/Common';
import { Circle } from 'components/Loading';

import UserChart from './Chart.Container';

import DeploymentChannel from '../deployment-channel';
import { DateRangePicker } from '../date';
import '../Root.scss';

interface PropsFromState {
  botId: string | null;
  deploymentId: string | null;
  hasChannel: boolean;
  isReady: boolean;
}

interface PropsFromDispatch {
  changeToDeployment: (id: string) => void;
  enableAnalytics(
    botId: string,
    deploymentId: string,
    type: 'basic' | 'advanced',
    metrics: string[]
  ): void;
  checkAnalytics(botId: string, deploymentId: string): void;
}

type Props = PropsFromState & PropsFromDispatch;

class UserAnalyticsPage extends React.Component<Props> {
  componentDidMount() {
    if (this.props.botId && this.props.deploymentId) {
      this.props.checkAnalytics(this.props.botId, this.props.deploymentId);
    }
  }

  renderNoDeploymentRedirection() {
    return (
      <EmptyMessage
        image={require('assets/images/no-deployments.svg')}
        title="No Deployments"
      >
        <SelectedUrl>
          {selectedId => (
            <Fragment>
              Your bot has no deployments available.{' '}
              <Link to={`/botstudio/${selectedId}/deployment`}>
                Create a deployment
              </Link>{' '}
              to activate analytics plan.
            </Fragment>
          )}
        </SelectedUrl>
      </EmptyMessage>
    );
  }

  renderNoChannelRedirection = () => {
    const { deploymentId } = this.props;

    return (
      <EmptyMessage
        image={require('assets/images/no-channel.svg')}
        title="No Channels"
      >
        <SelectedUrl>
          {selectedId => (
            <Fragment>
              Deployment <strong>{deploymentId}</strong> has no channels
              available.{' '}
              <Link to={`/botstudio/${selectedId}/deployment`}>
                Create a channel
              </Link>{' '}
              to activate analytics plan.
            </Fragment>
          )}
        </SelectedUrl>
      </EmptyMessage>
    );
  };

  renderNoBotSelected = () => {
    return (
      <EmptyMessage
        image={require('assets/images/no-channel.svg')}
        title="No Bot Selected"
      >
        You have not selected a bot. Please select one from the sidebar on the
        left.
      </EmptyMessage>
    );
  };

  renderData() {
    return (
      <Fragment>
        <UserChart />
      </Fragment>
    );
  }

  render() {
    const { botId, deploymentId, hasChannel, isReady } = this.props;

    return (
      <Dashboard
        title="User Analytics"
        tooltip="Understand your user activity."
      >
        <Board
          className="kata-analytics-body"
          headingChildren={
            <div className="kata-environment">
              <DeploymentChannel />
              <DateRangePicker />
            </div>
          }
        >
          {isReady ? (
            <Fragment>
              {!botId
                ? this.renderNoBotSelected()
                : !deploymentId
                ? this.renderNoDeploymentRedirection()
                : !hasChannel
                ? this.renderNoChannelRedirection()
                : this.renderData()}
            </Fragment>
          ) : (
            <div className="text-center py-5">
              <Circle size={30} />
            </div>
          )}
        </Board>
      </Dashboard>
    );
  }
}

const mapStateToProps = ({ bot, deployment }: RootStore): PropsFromState => {
  const selectedBot = getBotSelected(bot);
  const selectedDeployment = getDeploymentSelected(deployment);

  return {
    botId: selectedBot,
    deploymentId: selectedDeployment,
    hasChannel: getChannelIndex(deployment.channel).length > 0,
    isReady: !getChannelLoading(deployment.channel, 'fetch')
  };
};

const mapDispatchToProps = dispatch => {
  return {
    changeToDeployment: (id: string) => dispatch(selectDeployment(id)),
    enableAnalytics(
      botId: string,
      deploymentId: string,
      type: 'basic' | 'advanced',
      metrics: string[]
    ) {
      dispatch(activateAnalytic(botId, deploymentId, type, metrics));
    },
    checkAnalytics(botId: string, deploymentId: string) {
      dispatch(checkAnalyticEnabled(botId, deploymentId));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserAnalyticsPage);
