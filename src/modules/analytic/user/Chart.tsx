import React, { Fragment } from 'react';
import numeral from 'numeral';

import {
  ResponsiveContainer,
  LineChart,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  Label
} from 'recharts';

class CustomizedAxisTick extends React.Component<any, any> {
  render() {
    const { x, y, payload } = this.props;
    return (
      <g transform={`translate(${x},${y})`}>
        <text x={0} y={0} dy={16} textAnchor="middle" fill="#666">
          {payload.value}
        </text>
      </g>
    );
  }
}

class Chart extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
    this.state = {
      metaData: [
        {
          key: 'total',
          name: 'Users',
          color: '#2a90ff',
          visible: true
        },
        {
          key: 'activeUsers',
          name: 'Active Users',
          color: '#484c4f',
          visible: false
        },
        {
          key: 'engagedSessions',
          name: 'Engaged Users',
          color: '#00c853',
          visible: false
        },
        {
          key: 'newUsers',
          name: 'New Users',
          color: '#ffc400',
          visible: false
        }
      ],
      users: true,
      activeUsers: false,
      engagedSessions: false,
      newUsers: false
    };
  }

  legendClick(key: any) {
    const newMeta = [...this.state.metaData];
    const index = newMeta.findIndex(meta => meta.key === key.id);
    newMeta[index].visible = !newMeta[index].visible;
    this.setState({ metaData: newMeta });
  }

  render() {
    const { data } = this.props;
    const { metaData } = this.state;
    return (
      <Fragment>
        <div className="kata-chart-container--user-analytics">
          <ResponsiveContainer height={400}>
            <LineChart data={data} margin={{ top: 24, bottom: 24 }}>
              <XAxis
                dataKey="date"
                padding={{ left: 10, right: 10 }}
                tick={<CustomizedAxisTick />}
                height={60}
              >
                <Label value="Date" offset={0} position="insideBottom" />
              </XAxis>
              <YAxis
                label={
                  // this object is supposed in the typings for the label prop,
                  // but it isn't, so for now we'll `any` it.
                  { value: 'Total', angle: -90, position: 'insideLeft' } as any
                }
                width={60}
                tickFormatter={(val: any) => numeral(val).format('0a')}
              />
              <Tooltip />
              <CartesianGrid strokeDasharray="3 3" vertical={false} />
              {metaData
                .filter((meta: any) => meta.visible)
                .map((meta: any, index: any) => (
                  <Line
                    key={index}
                    name={meta.name}
                    dataKey={meta.key}
                    stroke={meta.color}
                    strokeWidth={2}
                    type="monotone"
                  />
                ))}
              <Legend
                verticalAlign="top"
                height={36}
                iconType="line"
                payload={metaData.map((meta: any) => {
                  return {
                    value: meta.name,
                    type: 'line',
                    id: meta.key,
                    color: meta.visible ? meta.color : '#c2c7c8'
                  };
                })}
                onClick={(obj: any) => this.legendClick(obj)}
              />
            </LineChart>
          </ResponsiveContainer>
        </div>
        <ul className="mt-3">
          <li>
            <strong>Users:</strong> Users who have sent, received or read a
            message
          </li>
          <li>
            <strong>Active Users:</strong> Users who have sent or read a message
          </li>
          <li>
            <strong>Engaged Users:</strong> Users who have sent a message
          </li>
          <li>
            <strong>New Users:</strong> New Users who have sent a message
          </li>
        </ul>
      </Fragment>
    );
  }
}

export default Chart;
