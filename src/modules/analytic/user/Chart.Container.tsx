import React from 'react';
import { connect } from 'react-redux';

// import { analyticUser as action } from '../../core/stores/actions';
import RootStore from 'interfaces/rootStore';
import * as Deployment from 'interfaces/deployment';

import { userRequest } from 'stores/analytic/user/actions';
import { getAnalyticDate } from 'stores/analytic/selectors';
import { checkAnalyticEnabled } from 'stores/analytic/root/actions';
import { getAnalyticMetricsEnabled } from 'stores/analytic/root/selectors';
import { getBotSelected } from 'stores/bot/selectors';
import {
  getAnalyticUserLoading,
  getAnalyticUserData,
  getAnalyticUserError
} from 'stores/analytic/user/selectors';
import {
  getDeploymentSelected,
  getDeploymentData
} from 'stores/deployment/selectors';
import {
  getChannelSelected,
  getChannelLoading
} from 'stores/deployment/channel/selectors';
import { Circle } from 'components/Loading';
import Chart from './Chart';
import Optin from '../optin';

interface PropsFromState {
  selectedBot: string | null;
  isLoading: boolean;
  error: string | null;
  data: any;
  metrics: string[];
  isReady: boolean;
  dataDeployment: Deployment.DataMap;
  deployment: string | null;
  channel: string | null;
  startDate: number;
  endDate: number;
}

interface PropsFromDispatch {
  fetchAction: Function;
  checkAnalytic: Function;
}

type Props = PropsFromState & PropsFromDispatch;

class UsersContainer extends React.Component<Props> {
  componentDidMount() {
    const {
      selectedBot,
      isReady,
      dataDeployment,
      deployment,
      channel,
      startDate,
      endDate
    } = this.props;
    if (isReady && selectedBot && deployment && channel) {
      this.fetchData(
        selectedBot,
        dataDeployment[deployment].name,
        startDate,
        endDate,
        channel
      );
      this.props.checkAnalytic(selectedBot, deployment);
    }
  }

  componentWillReceiveProps(nextProps: Props) {
    const { deployment, startDate, endDate, channel } = this.props;
    if (
      nextProps.isReady &&
      (deployment !== nextProps.deployment ||
        startDate !== nextProps.startDate ||
        endDate !== nextProps.endDate ||
        channel !== nextProps.channel) &&
      nextProps.selectedBot &&
      nextProps.dataDeployment &&
      nextProps.deployment &&
      nextProps.channel
    ) {
      this.fetchData(
        nextProps.selectedBot,
        nextProps.dataDeployment[nextProps.deployment].name,
        nextProps.startDate,
        nextProps.endDate,
        nextProps.channel
      );
    }
  }

  fetchData = (
    botId: string,
    deployment: string,
    startDate: number,
    endDate: number,
    channel?: string
  ) => {
    this.props.fetchAction(botId, deployment, startDate, endDate, channel);
  };

  render() {
    if (this.props.isLoading) {
      return (
        <div className="text-center py-5">
          <Circle size={30} />
        </div>
      );
    }

    if (this.props.metrics.indexOf('user') === -1) {
      return <Optin />;
    }

    if (this.props.error !== null) {
      return <div className="text-danger">{this.props.error}</div>;
    }

    return <Chart type="date" data={this.props.data} />;
  }
}

const mapStateToProps = ({
  analytic,
  bot,
  deployment
}: RootStore): PropsFromState => {
  const selectedBot = getBotSelected(bot);
  const selectedDeployment = getDeploymentSelected(deployment);
  const dateRage = getAnalyticDate(analytic);

  return {
    selectedBot,
    isLoading: getAnalyticUserLoading(analytic.user),
    error: getAnalyticUserError(analytic.user),
    data: getAnalyticUserData(analytic.user),
    metrics: getAnalyticMetricsEnabled(analytic.root),
    isReady:
      selectedBot !== null &&
      selectedDeployment !== null &&
      !getChannelLoading(deployment.channel, 'fetch'),
    dataDeployment: getDeploymentData(deployment),
    deployment: selectedDeployment,
    channel: getChannelSelected(deployment.channel),
    startDate: dateRage.start,
    endDate: dateRage.end
  };
};

const mapDispatchToProps: PropsFromDispatch = {
  fetchAction: (
    botId: string,
    deployment: string,
    start: number,
    end: number,
    channel?: string
  ) => userRequest({ botId, deployment, start, end, channel }),
  checkAnalytic: (botId: string, deploymentId: string) =>
    checkAnalyticEnabled(botId, deploymentId)
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UsersContainer);
