import React from 'react';
import { connect } from 'react-redux';

// import { analytic as analyticAction } from '../core/stores/actions';
import RootStore from 'interfaces/rootStore';
import { getAnalyticDate } from 'stores/analytic/selectors';
import { changeAnalyticDate } from 'stores/analytic/actions';

import { DateRangePicker } from 'components/DateRangePicker';

interface PropsFromState {
  startDate: number;
  endDate: number;
}

interface PropsFromDispatch {
  changeDate: Function;
}

const DateRangePickerContainer = (props: any) => {
  return (
    <div className="kata-channel-selector__container kata-channel-selector__container--daterange">
      <label className="kata-info__label mr-2">Date</label>
      <DateRangePicker {...props} />
    </div>
  );
};

const mapStateToProps = ({ analytic }: RootStore): PropsFromState => {
  const dateRage = getAnalyticDate(analytic);
  return {
    startDate: dateRage.start,
    endDate: dateRage.end
  };
};

const mapDispatchToProps = (dispatch: Function): PropsFromDispatch => {
  return {
    changeDate: (start: number, end: number) =>
      dispatch(changeAnalyticDate(start, end))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DateRangePickerContainer);
