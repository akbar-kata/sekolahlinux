import React from 'react';
import numeral from 'numeral';

const {
  ResponsiveContainer,
  BarChart,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip
} = require('recharts');

class Charts extends React.Component<any, any> {
  render() {
    const { isLoading, error, data, type } = this.props;
    if (isLoading) {
      return <div>Loading...</div>;
    }

    if (error !== null) {
      return <div className="text-danger">{this.props.error}</div>;
    }

    return (
      <ResponsiveContainer>
        <BarChart
          data={data}
          layout="vertical"
          margin={{ top: 5, right: 30, left: 20, bottom: 5 }}
        >
          <XAxis
            type="number"
            tickFormatter={function format(val: any) {
              return numeral(val).format('0a');
            }}
          />
          <YAxis
            dataKey={type}
            type="category"
            width={180}
            padding={{ top: 10 }}
          />
          <CartesianGrid strokeDasharray="3 3" />
          <Tooltip />
          <Bar
            dataKey={(type && 'total') || 'messages'}
            fill="#2a90ff"
            maxBarSize={20}
            label={{
              fontWeight: 'bold',
              fontSize: 11,
              fill: '#fff'
            }}
          />
        </BarChart>
      </ResponsiveContainer>
    );
  }
}

export default Charts;
