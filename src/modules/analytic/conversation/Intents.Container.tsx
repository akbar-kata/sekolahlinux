import React from 'react';
import { connect } from 'react-redux';

import RootStore from 'interfaces/rootStore';

import { intentRequest } from 'stores/analytic/conversation/actions';
import { getBotSelected } from 'stores/bot/selectors';
import {
  getDeploymentSelected,
  getDeploymentData
} from 'stores/deployment/selectors';
import {
  getChannelSelected,
  getChannelLoading
} from 'stores/deployment/channel/selectors';
import { getAnalyticDate } from 'stores/analytic/selectors';
import {
  getAnalyticConvLoading,
  getAnalyticConvIntent,
  getAnalyticConvError
} from 'stores/analytic/conversation/selectors';

import Chart from './Chart';

interface PropsFromState {
  selectedBot: string | null;
  isLoading: boolean;
  error: string | null;
  data: any;
  isReady: boolean;
  dataDeployment: any;
  deployment: string | null;
  channel: string | null;
  startDate: number;
  endDate: number;
}

interface PropsFromDispatch {
  fetchAction: Function;
}

class IntentsContainer extends React.Component<any, any> {
  componentDidMount() {
    const {
      selectedBot,
      isReady,
      dataDeployment,
      deployment,
      channel,
      startDate,
      endDate
    } = this.props;
    if (isReady) {
      this.fetchData(
        selectedBot,
        dataDeployment[deployment].name,
        startDate,
        endDate,
        channel
      );
    }
  }

  componentWillReceiveProps(nextProps: any) {
    const { deployment, startDate, endDate, channel } = this.props;
    if (
      nextProps.isReady &&
      (deployment !== nextProps.deployment ||
        startDate !== nextProps.startDate ||
        endDate !== nextProps.endDate ||
        channel !== nextProps.channel)
    ) {
      this.fetchData(
        nextProps.selectedBot,
        nextProps.dataDeployment[nextProps.deployment].name,
        nextProps.startDate,
        nextProps.endDate,
        nextProps.channel
      );
    }
  }

  fetchData = (
    botId: string,
    deployment: string,
    startDate: number,
    endDate: number,
    channel?: string
  ) => {
    this.props.fetchAction(botId, deployment, startDate, endDate, channel);
  };

  render() {
    return (
      <Chart
        type="intent"
        isLoading={this.props.isLoading}
        error={this.props.error}
        data={this.props.data}
      />
    );
  }
}

const mapStateToProps = ({
  analytic,
  bot,
  deployment
}: RootStore): PropsFromState => {
  const selectedBot = getBotSelected(bot);
  const selectedDeployment = getDeploymentSelected(deployment);
  const dateRage = getAnalyticDate(analytic);

  return {
    selectedBot,
    isLoading: getAnalyticConvLoading(analytic.conversation, 'intent'),
    error: getAnalyticConvError(analytic.conversation, 'intent'),
    data: getAnalyticConvIntent(analytic.conversation),
    isReady:
      selectedBot !== null &&
      selectedDeployment !== null &&
      !getChannelLoading(deployment.channel, 'fetch'),
    dataDeployment: getDeploymentData(deployment),
    deployment: selectedDeployment,
    channel: getChannelSelected(deployment.channel),
    startDate: dateRage.start,
    endDate: dateRage.end
  };
};

const mapDispatchToProps = (dispatch: Function): PropsFromDispatch => {
  return {
    fetchAction: (
      botId: string,
      deployment: string,
      startDate: number,
      endDate: number,
      channel?: string
    ) => dispatch(intentRequest(botId, deployment, startDate, endDate, channel))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(IntentsContainer);
