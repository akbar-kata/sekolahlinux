import React from 'react';
import { connect } from 'react-redux';

import RootStore from 'interfaces/rootStore';
import { flowRequest } from 'stores/analytic/conversation/actions';
import { getBotSelected } from 'stores/bot/selectors';
import {
  getDeploymentSelected,
  getDeploymentData
} from 'stores/deployment/selectors';
import {
  getChannelSelected,
  getChannelLoading
} from 'stores/deployment/channel/selectors';
import { getAnalyticDate } from 'stores/analytic/selectors';
import {
  getAnalyticConvLoading,
  getAnalyticConvFlow,
  getAnalyticConvError
} from 'stores/analytic/conversation/selectors';
import Chart from './Chart';
import { checkAnalyticEnabled } from 'stores/analytic/root/actions';

interface PropsFromState {
  selectedBot: string | null;
  isLoading: boolean;
  error: string | null;
  data: any;
  isReady: boolean;
  dataDeployment: any;
  deployment: string | null;
  channel: string | null;
  startDate: number;
  endDate: number;
}

interface PropsFromDispatch {
  fetchAction: Function;
  checkAnalytic: Function;
}

class FlowsContainer extends React.Component<any, any> {
  componentDidMount() {
    const {
      selectedBot,
      isReady,
      dataDeployment,
      deployment,
      channel,
      startDate,
      endDate
    } = this.props;
    if (isReady) {
      this.fetchData(
        selectedBot,
        dataDeployment[deployment].name,
        startDate,
        endDate,
        channel
      );
    }
  }

  componentWillReceiveProps(nextProps: any) {
    const { deployment, startDate, endDate, channel } = this.props;
    if (
      nextProps.isReady &&
      (deployment !== nextProps.deployment ||
        startDate !== nextProps.startDate ||
        endDate !== nextProps.endDate ||
        channel !== nextProps.channel)
    ) {
      this.fetchData(
        nextProps.selectedBot,
        nextProps.dataDeployment[nextProps.deployment].name,
        nextProps.startDate,
        nextProps.endDate,
        nextProps.channel
      );
    }
  }

  fetchData = (
    botId: string,
    deployment: string,
    startDate: number,
    endDate: number,
    channel?: string
  ) => {
    this.props.fetchAction(botId, deployment, startDate, endDate, channel);
  };

  render() {
    return (
      <Chart
        type="flow"
        isLoading={this.props.isLoading}
        error={this.props.error}
        data={this.props.data}
      />
    );
  }
}

const mapStateToProps = ({
  analytic,
  bot,
  deployment
}: RootStore): PropsFromState => {
  const selectedBot = getBotSelected(bot);
  const selectedDeployment = getDeploymentSelected(deployment);
  const dateRage = getAnalyticDate(analytic);

  return {
    selectedBot,
    isLoading: getAnalyticConvLoading(analytic.conversation, 'flow'),
    error: getAnalyticConvError(analytic.conversation, 'flow'),
    data: getAnalyticConvFlow(analytic.conversation),
    isReady:
      selectedBot !== null &&
      selectedDeployment !== null &&
      !getChannelLoading(deployment.channel, 'fetch'),
    dataDeployment: getDeploymentData(deployment),
    deployment: selectedDeployment,
    channel: getChannelSelected(deployment.channel),
    startDate: dateRage.start,
    endDate: dateRage.end
  };
};

const mapDispatchToProps = (dispatch: Function): PropsFromDispatch => {
  return {
    fetchAction: (
      botId: string,
      deployment: string,
      startDate: number,
      endDate: number,
      channel?: string
    ) => dispatch(flowRequest(botId, deployment, startDate, endDate, channel)),
    checkAnalytic(botId: string, deploymentId: string) {
      dispatch(checkAnalyticEnabled(botId, deploymentId));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FlowsContainer);
