import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import RootStore from 'interfaces/rootStore';
import SelectedUrl from 'containers/SelectedUrl';

import { getBotSelected } from 'stores/bot/selectors';
import { getDeploymentSelected } from 'stores/deployment/selectors';
import {
  getChannelIndex,
  getChannelLoading
} from 'stores/deployment/channel/selectors';
import { checkAnalyticEnabled } from 'stores/analytic/root/actions';
import { getAnalyticMetricsEnabled } from 'stores/analytic/root/selectors';

import DeploymentChannel from '../deployment-channel';
import { DateRangePicker } from '../date';
import Optin from '../optin';

import { Dashboard } from 'components/Dashboard';
import { Board } from 'components/Board';
import { EmptyMessage } from 'components/Common';
import { Circle } from 'components/Loading';

import FlowsChart from './Flows.Container';
import IntentsChart from './Intents.Container';
import MessagesChart from './Messages.Container';

import '../Root.scss';

const HeadingChildren = (
  <div className="kata-environment">
    <DeploymentChannel />
    <DateRangePicker />
  </div>
);

interface PropsFromState {
  botId: string | null;
  deploymentId: string | null;
  hasChannel: boolean;
  metrics: string[];
  isReady: boolean;
}

interface PropsFromDispatch {
  checkAnalytics(botId: string, deploymentId: string): void;
}

type Props = PropsFromState & PropsFromDispatch;

class ConversationAnalyticsPage extends React.Component<Props> {
  componentDidMount() {
    if (this.props.botId && this.props.deploymentId) {
      this.props.checkAnalytics(this.props.botId, this.props.deploymentId);
    }
  }

  renderNoDeploymentRedirection() {
    return (
      <EmptyMessage
        image={require('assets/images/no-channel.svg')}
        title="No Deployments"
      >
        <SelectedUrl>
          {selectedId => (
            <Fragment>
              Your bot has no deployments available.{' '}
              <Link to={`/botstudio/${selectedId}/deployment`}>
                Create a deployment
              </Link>{' '}
              to activate analytics plan.
            </Fragment>
          )}
        </SelectedUrl>
      </EmptyMessage>
    );
  }

  renderNoChannelRedirection = () => {
    const { deploymentId } = this.props;

    return (
      <EmptyMessage
        image={require('assets/images/no-channel.svg')}
        title="No Channels"
      >
        <SelectedUrl>
          {selectedId => (
            <Fragment>
              Deployment <strong>{deploymentId}</strong> has no channels
              available.{' '}
              <Link to={`/botstudio/${selectedId}/deployment`}>
                Create a channel
              </Link>{' '}
              to activate analytics plan.
            </Fragment>
          )}
        </SelectedUrl>
      </EmptyMessage>
    );
  };

  renderNoBotSelected = () => {
    return (
      <EmptyMessage
        image={require('assets/images/no-channel.svg')}
        title="No Bot Selected"
      >
        You have not selected a bot. Please select one from the sidebar on the
        left.
      </EmptyMessage>
    );
  };

  renderData() {
    return (
      <Fragment>
        {this.isAnalyticsDisabled(this.props) ? (
          <Optin />
        ) : (
          <Fragment>
            <h4>Top 5 Flows</h4>
            <h5 className="subtext-gray">Top flows stats is updated hourly</h5>
            <div className="kata-chart-container">
              <FlowsChart />
            </div>
            <hr className="my-5" />
            <h4 className="text-gray">Top 10 Intent</h4>
            <h5 className="subtext-gray">
              Top intents stats is updated hourly
            </h5>
            <div className="kata-chart-container">
              <IntentsChart />
            </div>
            <hr className="my-5" />
            <h4 className="text-gray">Top 10 Messages</h4>
            <h5 className="subtext-gray">
              Top messages stats is updated hourly
            </h5>
            <div className="kata-chart-container">
              <MessagesChart />
            </div>
          </Fragment>
        )}
      </Fragment>
    );
  }

  render() {
    const { botId, deploymentId, hasChannel, isReady } = this.props;

    return (
      <Dashboard
        title="Conversation Statistics"
        tooltip="Get to know which topics attract users the most."
      >
        <Board headingChildren={HeadingChildren}>
          {isReady ? (
            <Fragment>
              {!botId
                ? this.renderNoBotSelected()
                : !deploymentId
                ? this.renderNoDeploymentRedirection()
                : !hasChannel
                ? this.renderNoChannelRedirection()
                : this.renderData()}
            </Fragment>
          ) : (
            <div className="text-center py-5">
              <Circle size={30} />
            </div>
          )}
        </Board>
      </Dashboard>
    );
  }

  private isAnalyticsDisabled({ metrics }: Props) {
    return (
      metrics.indexOf('flow') === -1 ||
      metrics.indexOf('intent') === -1 ||
      metrics.indexOf('text') === -1
    );
  }
}

const mapStateToProps = ({
  bot,
  deployment,
  analytic
}: RootStore): PropsFromState => {
  return {
    botId: getBotSelected(bot),
    deploymentId: getDeploymentSelected(deployment),
    hasChannel: getChannelIndex(deployment.channel).length > 0,
    metrics: getAnalyticMetricsEnabled(analytic.root),
    isReady: !getChannelLoading(deployment.channel, 'fetch')
  };
};

const mapDispatchToProps = dispatch => {
  return {
    checkAnalytics(botId: string, deploymentId: string) {
      dispatch(checkAnalyticEnabled(botId, deploymentId));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ConversationAnalyticsPage);
