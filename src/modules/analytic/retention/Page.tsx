import React from 'react';

import DeploymentChannel from '../deployment-channel/DeploymentChannel.Container';
import { DateRangePicker } from '../date';

export default () => (
  <div>
    <div title="Retention">
      User's bot retention
      <div className="breadcrumb-env">
        <DeploymentChannel />
        <DateRangePicker />
      </div>
    </div>
    <div>
      <div>
        <table className="table table-striped table-hover">
          <thead>
            <tr>
              <th>Date</th>
              <th className="text-right" style={{ width: 80 }}>
                New Users
              </th>
              <th className="text-right">1 Day</th>
              <th className="text-right">2 Day</th>
              <th className="text-right">3 Day</th>
              <th className="text-right">4 Day</th>
              <th className="text-right">5 Day</th>
              <th className="text-right">6 Day</th>
              <th className="text-right">7 Day</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>26 Jan 2017</td>
              <td className="text-right">171</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(203, 229, 242)'
                }}
              >
                25.73 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(221, 238, 247)'
                }}
              >
                16.96 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(220, 238, 247)'
                }}
              >
                17.54 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(228, 242, 249)'
                }}
              >
                13.45 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(235, 245, 250)'
                }}
              >
                9.94 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(246, 251, 253)'
                }}
              >
                4.68 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(244, 250, 253)'
                }}
              >
                5.85 %
              </td>
            </tr>
            <tr>
              <td>27 Jan 2017</td>
              <td className="text-right">208</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(210, 233, 244)'
                }}
              >
                22.12 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(221, 238, 247)'
                }}
              >
                16.83 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(237, 246, 251)'
                }}
              >
                9.13 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(241, 248, 252)'
                }}
              >
                7.21 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(246, 251, 253)'
                }}
              >
                4.81 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(245, 250, 253)'
                }}
              >
                5.29 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(242, 249, 252)'
                }}
              >
                6.73 %
              </td>
            </tr>
            <tr>
              <td>28 Jan 2017</td>
              <td className="text-right">162</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(200, 228, 242)'
                }}
              >
                27.16 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(223, 239, 247)'
                }}
              >
                16.05 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(235, 245, 250)'
                }}
              >
                9.88 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(235, 245, 250)'
                }}
              >
                9.88 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(252, 254, 255)'
                }}
              >
                1.85 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(240, 248, 252)'
                }}
              >
                7.41 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(247, 251, 253)'
                }}
              >
                4.32 %
              </td>
            </tr>
            <tr>
              <td>29 Jan 2017</td>
              <td className="text-right">184</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(195, 225, 240)'
                }}
              >
                29.35 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(218, 237, 246)'
                }}
              >
                18.48 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(235, 245, 250)'
                }}
              >
                9.78 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(241, 248, 252)'
                }}
              >
                7.07 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(235, 245, 250)'
                }}
              >
                9.78 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(245, 250, 253)'
                }}
              >
                4.89 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(230, 243, 249)'
                }}
              >
                12.50 %
              </td>
            </tr>
            <tr>
              <td>30 Jan 2017</td>
              <td className="text-right">177</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(206, 231, 243)'
                }}
              >
                24.29 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(239, 247, 251)'
                }}
              >
                7.91 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(249, 252, 254)'
                }}
              >
                3.39 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(232, 244, 250)'
                }}
              >
                11.30 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(237, 246, 251)'
                }}
              >
                9.04 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(240, 248, 252)'
                }}
              >
                7.34 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(242, 249, 252)'
                }}
              >
                6.78 %
              </td>
            </tr>
            <tr>
              <td>31 Jan 2017</td>
              <td className="text-right">186</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(210, 233, 244)'
                }}
              >
                22.04 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(236, 246, 251)'
                }}
              >
                9.68 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(215, 235, 245)'
                }}
              >
                19.89 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(235, 245, 250)'
                }}
              >
                10.22 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(227, 241, 248)'
                }}
              >
                13.98 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(222, 239, 247)'
                }}
              >
                16.13 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(246, 251, 253)'
                }}
              >
                4.84 %
              </td>
            </tr>
            <tr>
              <td>1 Feb 2017</td>
              <td className="text-right">181</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(213, 234, 245)'
                }}
              >
                20.99 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(219, 237, 246)'
                }}
              >
                17.68 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(235, 245, 250)'
                }}
              >
                9.94 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(237, 246, 251)'
                }}
              >
                8.84 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(226, 241, 248)'
                }}
              >
                14.36 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(237, 246, 251)'
                }}
              >
                8.84 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(249, 252, 254)'
                }}
              >
                3.31 %
              </td>
            </tr>
            <tr>
              <td>2 Feb 2017</td>
              <td className="text-right">206</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(203, 229, 242)'
                }}
              >
                25.73 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(224, 240, 248)'
                }}
              >
                15.53 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(227, 241, 248)'
                }}
              >
                14.08 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(232, 244, 250)'
                }}
              >
                11.65 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(235, 245, 250)'
                }}
              >
                10.19 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(253, 254, 255)'
                }}
              >
                1.46 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(249, 252, 254)'
                }}
              >
                3.40 %
              </td>
            </tr>
            <tr>
              <td>3 Feb 2017</td>
              <td className="text-right">186</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(213, 234, 245)'
                }}
              >
                20.97 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(215, 235, 245)'
                }}
              >
                19.89 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(222, 239, 247)'
                }}
              >
                16.13 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(244, 250, 253)'
                }}
              >
                5.38 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(248, 252, 254)'
                }}
              >
                3.76 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(247, 251, 253)'
                }}
              >
                4.30 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(247, 251, 253)'
                }}
              >
                4.30 %
              </td>
            </tr>
            <tr>
              <td>4 Feb 2017</td>
              <td className="text-right">210</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(190, 223, 239)'
                }}
              >
                31.90 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(212, 234, 245)'
                }}
              >
                21.43 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(233, 244, 250)'
                }}
              >
                10.95 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(242, 249, 252)'
                }}
              >
                6.67 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(245, 250, 253)'
                }}
              >
                5.24 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(244, 250, 253)'
                }}
              >
                5.71 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(242, 249, 252)'
                }}
              >
                6.67 %
              </td>
            </tr>
            <tr>
              <td>5 Feb 2017</td>
              <td className="text-right">184</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(183, 219, 237)'
                }}
              >
                35.33 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(218, 237, 246)'
                }}
              >
                18.48 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(237, 246, 251)'
                }}
              >
                9.24 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(245, 250, 253)'
                }}
              >
                4.89 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(234, 245, 250)'
                }}
              >
                10.33 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(248, 252, 254)'
                }}
              >
                3.80 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(239, 247, 251)'
                }}
              >
                8.15 %
              </td>
            </tr>
            <tr>
              <td>6 Feb 2017</td>
              <td className="text-right">182</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(207, 231, 243)'
                }}
              >
                23.63 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(242, 249, 252)'
                }}
              >
                6.59 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(243, 249, 252)'
                }}
              >
                6.04 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(242, 249, 252)'
                }}
              >
                6.59 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(248, 252, 254)'
                }}
              >
                3.85 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(237, 246, 251)'
                }}
              >
                8.79 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(216, 236, 246)'
                }}
              >
                19.23 %
              </td>
            </tr>
            <tr>
              <td>7 Feb 2017</td>
              <td className="text-right">226</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(207, 231, 243)'
                }}
              >
                23.89 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(234, 245, 250)'
                }}
              >
                10.62 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(241, 248, 252)'
                }}
              >
                7.08 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(242, 249, 252)'
                }}
              >
                6.64 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(236, 246, 251)'
                }}
              >
                9.29 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(198, 227, 241)'
                }}
              >
                27.88 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(228, 242, 249)'
                }}
              >
                13.27 %
              </td>
            </tr>
            <tr>
              <td>8 Feb 2017</td>
              <td className="text-right">206</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(219, 237, 246)'
                }}
              >
                17.96 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(222, 239, 247)'
                }}
              >
                16.50 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(241, 248, 252)'
                }}
              >
                7.28 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(220, 238, 247)'
                }}
              >
                17.48 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(196, 226, 241)'
                }}
              >
                29.13 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(230, 243, 249)'
                }}
              >
                12.62 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(218, 237, 246)'
                }}
              >
                18.45 %
              </td>
            </tr>
            <tr>
              <td>9 Feb 2017</td>
              <td className="text-right">186</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(209, 232, 244)'
                }}
              >
                22.58 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(229, 242, 249)'
                }}
              >
                12.90 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(222, 239, 247)'
                }}
              >
                16.13 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(196, 226, 241)'
                }}
              >
                29.03 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(226, 241, 248)'
                }}
              >
                14.52 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(214, 235, 245)'
                }}
              >
                20.43 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(246, 251, 253)'
                }}
              >
                4.84 %
              </td>
            </tr>
            <tr>
              <td>10 Feb 2017</td>
              <td className="text-right">227</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(218, 237, 246)'
                }}
              >
                18.50 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(218, 237, 246)'
                }}
              >
                18.50 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(182, 219, 237)'
                }}
              >
                35.68 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(212, 234, 245)'
                }}
              >
                21.15 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(195, 225, 240)'
                }}
              >
                29.52 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(238, 247, 251)'
                }}
              >
                8.37 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(240, 248, 252)'
                }}
              >
                7.49 %
              </td>
            </tr>
            <tr>
              <td>11 Feb 2017</td>
              <td className="text-right">206</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(196, 226, 241)'
                }}
              >
                29.13 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(175, 215, 235)'
                }}
              >
                39.32 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(210, 233, 244)'
                }}
              >
                22.33 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(195, 225, 240)'
                }}
              >
                29.61 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(236, 246, 251)'
                }}
              >
                9.71 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(242, 249, 252)'
                }}
              >
                6.80 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(245, 250, 253)'
                }}
              >
                5.34 %
              </td>
            </tr>
            <tr>
              <td>12 Feb 2017</td>
              <td className="text-right">184</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(146, 201, 228)'
                }}
              >
                53.26 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(209, 232, 244)'
                }}
              >
                22.83 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(203, 229, 242)'
                }}
              >
                25.54 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(234, 245, 250)'
                }}
              >
                10.33 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(239, 247, 251)'
                }}
              >
                8.15 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(249, 252, 254)'
                }}
              >
                3.26 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(242, 249, 252)'
                }}
              >
                6.52 %
              </td>
            </tr>
            <tr>
              <td>13 Feb 2017</td>
              <td className="text-right">2983</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(167, 211, 233)'
                }}
              >
                43.45 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(123, 189, 222)'
                }}
              >
                64.67 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(228, 242, 249)'
                }}
              >
                13.48 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(237, 246, 251)'
                }}
              >
                9.08 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(248, 252, 254)'
                }}
              >
                3.89 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(243, 249, 252)'
                }}
              >
                6.20 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(217, 236, 246)'
                }}
              >
                18.97 %
              </td>
            </tr>
            <tr>
              <td>14 Feb 2017</td>
              <td className="text-right">1180</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(113, 184, 220)'
                }}
              >
                69.66 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(222, 239, 247)'
                }}
              >
                16.44 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(233, 244, 250)'
                }}
              >
                11.19 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(245, 250, 253)'
                }}
              >
                5.17 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(241, 248, 252)'
                }}
              >
                7.03 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(228, 242, 249)'
                }}
              >
                13.31 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(245, 250, 253)'
                }}
              >
                5.00 %
              </td>
            </tr>
            <tr>
              <td>15 Feb 2017</td>
              <td className="text-right">4820</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(211, 233, 244)'
                }}
              >
                21.97 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(230, 243, 249)'
                }}
              >
                12.22 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(242, 249, 252)'
                }}
              >
                6.37 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(236, 246, 251)'
                }}
              >
                9.59 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(222, 239, 247)'
                }}
              >
                16.22 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(242, 249, 252)'
                }}
              >
                6.72 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(249, 252, 254)'
                }}
              >
                2.97 %
              </td>
            </tr>
            <tr>
              <td>16 Feb 2017</td>
              <td className="text-right">536</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(201, 228, 242)'
                }}
              >
                26.68 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(232, 244, 250)'
                }}
              >
                11.38 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(229, 242, 249)'
                }}
              >
                12.87 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(203, 229, 242)'
                }}
              >
                25.56 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(235, 245, 250)'
                }}
              >
                9.89 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(245, 250, 253)'
                }}
              >
                5.22 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(247, 251, 253)'
                }}
              >
                4.29 %
              </td>
            </tr>
            <tr>
              <td>17 Feb 2017</td>
              <td className="text-right">426</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(213, 234, 245)'
                }}
              >
                20.66 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(221, 238, 247)'
                }}
              >
                16.90 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(192, 224, 240)'
                }}
              >
                30.99 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(227, 241, 248)'
                }}
              >
                14.08 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(243, 249, 252)'
                }}
              >
                6.10 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(247, 251, 253)'
                }}
              >
                3.99 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(229, 242, 249)'
                }}
              >
                12.91 %
              </td>
            </tr>
            <tr>
              <td>18 Feb 2017</td>
              <td className="text-right">359</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(205, 230, 243)'
                }}
              >
                24.79 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(171, 213, 234)'
                }}
              >
                41.23 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(228, 242, 249)'
                }}
              >
                13.37 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(244, 250, 253)'
                }}
              >
                5.57 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(245, 250, 253)'
                }}
              >
                5.29 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(232, 244, 250)'
                }}
              >
                11.42 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(242, 249, 252)'
                }}
              >
                6.69 %
              </td>
            </tr>
            <tr>
              <td>19 Feb 2017</td>
              <td className="text-right">402</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(173, 214, 235)'
                }}
              >
                40.05 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(219, 237, 246)'
                }}
              >
                17.66 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(237, 246, 251)'
                }}
              >
                9.20 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(245, 250, 253)'
                }}
              >
                4.98 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(226, 241, 248)'
                }}
              >
                14.43 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(238, 247, 251)'
                }}
              >
                8.46 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(234, 245, 250)'
                }}
              >
                10.45 %
              </td>
            </tr>
            <tr>
              <td>20 Feb 2017</td>
              <td className="text-right">464</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(211, 233, 244)'
                }}
              >
                21.55 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(235, 245, 250)'
                }}
              >
                9.91 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(248, 252, 254)'
                }}
              >
                3.66 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(232, 244, 250)'
                }}
              >
                11.64 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(241, 248, 252)'
                }}
              >
                7.11 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(224, 240, 248)'
                }}
              >
                15.30 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(237, 246, 251)'
                }}
              >
                9.05 %
              </td>
            </tr>
            <tr>
              <td>21 Feb 2017</td>
              <td className="text-right">431</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(216, 236, 246)'
                }}
              >
                19.49 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(236, 246, 251)'
                }}
              >
                9.51 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(214, 235, 245)'
                }}
              >
                20.19 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(236, 246, 251)'
                }}
              >
                9.28 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(223, 239, 247)'
                }}
              >
                15.78 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(233, 244, 250)'
                }}
              >
                10.90 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(242, 249, 252)'
                }}
              >
                6.50 %
              </td>
            </tr>
            <tr>
              <td>22 Feb 2017</td>
              <td className="text-right">324</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(218, 237, 246)'
                }}
              >
                18.52 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(218, 237, 246)'
                }}
              >
                18.52 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(238, 247, 251)'
                }}
              >
                8.33 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(235, 245, 250)'
                }}
              >
                9.88 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(228, 242, 249)'
                }}
              >
                13.27 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(242, 249, 252)'
                }}
              >
                6.79 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(249, 252, 254)'
                }}
              >
                3.09 %
              </td>
            </tr>
            <tr>
              <td>23 Feb 2017</td>
              <td className="text-right">321</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(201, 228, 242)'
                }}
              >
                26.48 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(234, 245, 250)'
                }}
              >
                10.28 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(226, 241, 248)'
                }}
              >
                14.64 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(229, 242, 249)'
                }}
              >
                13.08 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(238, 247, 251)'
                }}
              >
                8.41 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(250, 253, 254)'
                }}
              >
                2.80 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(249, 252, 254)'
                }}
              >
                3.12 %
              </td>
            </tr>
            <tr>
              <td>24 Feb 2017</td>
              <td className="text-right">265</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(198, 227, 241)'
                }}
              >
                27.92 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(214, 235, 245)'
                }}
              >
                20.38 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(225, 240, 248)'
                }}
              >
                15.09 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(238, 247, 251)'
                }}
              >
                8.68 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(245, 250, 253)'
                }}
              >
                5.28 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(249, 252, 254)'
                }}
              >
                3.40 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(243, 249, 252)'
                }}
              >
                6.04 %
              </td>
            </tr>
            <tr>
              <td>25 Feb 2017</td>
              <td className="text-right">273</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(204, 230, 243)'
                }}
              >
                24.91 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(218, 237, 246)'
                }}
              >
                18.32 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(240, 248, 252)'
                }}
              >
                7.69 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(243, 249, 252)'
                }}
              >
                6.23 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(243, 249, 252)'
                }}
              >
                5.86 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(240, 248, 252)'
                }}
              >
                7.33 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(240, 248, 252)'
                }}
              >
                7.33 %
              </td>
            </tr>
            <tr>
              <td>26 Feb 2017</td>
              <td className="text-right">191</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(195, 225, 240)'
                }}
              >
                29.32 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(222, 239, 247)'
                }}
              >
                16.23 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(239, 247, 251)'
                }}
              >
                7.85 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(243, 249, 252)'
                }}
              >
                6.28 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(238, 247, 251)'
                }}
              >
                8.38 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(245, 250, 253)'
                }}
              >
                5.24 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(240, 248, 252)'
                }}
              >
                7.33 %
              </td>
            </tr>
            <tr>
              <td>27 Feb 2017</td>
              <td className="text-right">187</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(202, 229, 242)'
                }}
              >
                26.20 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(237, 246, 251)'
                }}
              >
                9.09 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(248, 252, 254)'
                }}
              >
                3.74 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(243, 249, 252)'
                }}
              >
                5.88 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(243, 249, 252)'
                }}
              >
                5.88 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(250, 253, 254)'
                }}
              >
                2.67 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(228, 242, 249)'
                }}
              >
                13.37 %
              </td>
            </tr>
            <tr>
              <td>28 Feb 2017</td>
              <td className="text-right">174</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(212, 234, 245)'
                }}
              >
                21.26 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(243, 249, 252)'
                }}
              >
                6.32 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(225, 240, 248)'
                }}
              >
                14.94 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(238, 247, 251)'
                }}
              >
                8.62 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(244, 250, 253)'
                }}
              >
                5.75 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(217, 236, 246)'
                }}
              >
                18.97 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(240, 248, 252)'
                }}
              >
                7.47 %
              </td>
            </tr>
            <tr>
              <td>1 Mar 2017</td>
              <td className="text-right">160</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(213, 234, 245)'
                }}
              >
                20.63 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(217, 236, 246)'
                }}
              >
                18.75 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(223, 239, 247)'
                }}
              >
                15.63 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(243, 249, 252)'
                }}
              >
                6.25 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(212, 234, 245)'
                }}
              >
                21.25 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(227, 241, 248)'
                }}
              >
                13.75 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(238, 247, 251)'
                }}
              >
                8.75 %
              </td>
            </tr>
            <tr>
              <td>2 Mar 2017</td>
              <td className="text-right">198</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(213, 234, 245)'
                }}
              >
                20.71 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(237, 246, 251)'
                }}
              >
                9.09 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(241, 248, 252)'
                }}
              >
                7.07 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(235, 245, 250)'
                }}
              >
                10.10 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(250, 253, 254)'
                }}
              >
                2.53 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(250, 253, 254)'
                }}
              >
                2.53 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(251, 253, 254)'
                }}
              >
                2.02 %
              </td>
            </tr>
            <tr>
              <td>3 Mar 2017</td>
              <td className="text-right">202</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(208, 232, 244)'
                }}
              >
                23.27 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(236, 246, 251)'
                }}
              >
                9.41 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(245, 250, 253)'
                }}
              >
                4.95 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(238, 247, 251)'
                }}
              >
                8.42 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(243, 249, 252)'
                }}
              >
                5.94 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(251, 253, 254)'
                }}
              >
                1.98 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(242, 249, 252)'
                }}
              >
                6.44 %
              </td>
            </tr>
            <tr>
              <td>4 Mar 2017</td>
              <td className="text-right">168</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(226, 241, 248)'
                }}
              >
                14.29 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(241, 248, 252)'
                }}
              >
                7.14 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(237, 246, 251)'
                }}
              >
                8.93 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(243, 249, 252)'
                }}
              >
                5.95 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(249, 252, 254)'
                }}
              >
                2.98 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(232, 244, 250)'
                }}
              >
                11.31 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(241, 248, 252)'
                }}
              >
                7.14 %
              </td>
            </tr>
            <tr>
              <td>5 Mar 2017</td>
              <td className="text-right">115</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(222, 239, 247)'
                }}
              >
                16.52 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(225, 240, 248)'
                }}
              >
                14.78 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(239, 247, 251)'
                }}
              >
                7.83 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(245, 250, 253)'
                }}
              >
                5.22 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(225, 240, 248)'
                }}
              >
                14.78 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(243, 249, 252)'
                }}
              >
                6.09 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(232, 244, 250)'
                }}
              >
                11.30 %
              </td>
            </tr>
            <tr>
              <td>6 Mar 2017</td>
              <td className="text-right">193</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(209, 232, 244)'
                }}
              >
                22.80 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(239, 247, 251)'
                }}
              >
                8.29 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(246, 251, 253)'
                }}
              >
                4.66 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(227, 241, 248)'
                }}
              >
                13.99 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(248, 252, 254)'
                }}
              >
                3.63 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(228, 242, 249)'
                }}
              >
                13.47 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(231, 243, 249)'
                }}
              >
                11.92 %
              </td>
            </tr>
            <tr>
              <td>7 Mar 2017</td>
              <td className="text-right">170</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(228, 242, 249)'
                }}
              >
                13.53 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(242, 249, 252)'
                }}
              >
                6.47 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(233, 244, 250)'
                }}
              >
                11.18 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(240, 248, 252)'
                }}
              >
                7.65 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(224, 240, 248)'
                }}
              >
                15.29 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(230, 243, 249)'
                }}
              >
                12.35 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(247, 251, 253)'
                }}
              >
                4.12 %
              </td>
            </tr>
            <tr>
              <td>8 Mar 2017</td>
              <td className="text-right">171</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(212, 234, 245)'
                }}
              >
                21.05 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(217, 236, 246)'
                }}
              >
                18.71 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(233, 244, 250)'
                }}
              >
                11.11 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(223, 239, 247)'
                }}
              >
                15.79 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(234, 245, 250)'
                }}
              >
                10.53 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(248, 252, 254)'
                }}
              >
                3.51 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(234, 245, 250)'
                }}
              >
                10.53 %
              </td>
            </tr>
            <tr>
              <td>9 Mar 2017</td>
              <td className="text-right">167</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(208, 232, 244)'
                }}
              >
                23.35 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(235, 245, 250)'
                }}
              >
                10.18 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(227, 241, 248)'
                }}
              >
                13.77 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(221, 238, 247)'
                }}
              >
                16.77 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(248, 252, 254)'
                }}
              >
                3.59 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(215, 235, 245)'
                }}
              >
                19.76 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(222, 239, 247)'
                }}
              >
                16.17 %
              </td>
            </tr>
            <tr>
              <td>10 Mar 2017</td>
              <td className="text-right">142</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(224, 240, 248)'
                }}
              >
                15.49 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(209, 232, 244)'
                }}
              >
                22.54 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(230, 243, 249)'
                }}
              >
                12.68 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(240, 248, 252)'
                }}
              >
                7.75 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(222, 239, 247)'
                }}
              >
                16.20 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(218, 237, 246)'
                }}
              >
                18.31 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(244, 250, 253)'
                }}
              >
                5.63 %
              </td>
            </tr>
            <tr>
              <td>11 Mar 2017</td>
              <td className="text-right">167</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(217, 236, 246)'
                }}
              >
                18.56 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(237, 246, 251)'
                }}
              >
                8.98 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(241, 248, 252)'
                }}
              >
                7.19 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(221, 238, 247)'
                }}
              >
                16.77 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(214, 235, 245)'
                }}
              >
                20.36 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(246, 251, 253)'
                }}
              >
                4.79 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(243, 249, 252)'
                }}
              >
                5.99 %
              </td>
            </tr>
            <tr>
              <td>12 Mar 2017</td>
              <td className="text-right">144</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(220, 238, 247)'
                }}
              >
                17.36 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(236, 246, 251)'
                }}
              >
                9.72 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(216, 236, 246)'
                }}
              >
                19.44 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(209, 232, 244)'
                }}
              >
                22.92 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(238, 247, 251)'
                }}
              >
                8.33 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(247, 251, 253)'
                }}
              >
                4.17 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(230, 243, 249)'
                }}
              >
                12.50 %
              </td>
            </tr>
            <tr>
              <td>13 Mar 2017</td>
              <td className="text-right">141</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(226, 241, 248)'
                }}
              >
                14.18 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(212, 234, 245)'
                }}
              >
                21.28 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(203, 229, 242)'
                }}
              >
                25.53 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(238, 247, 251)'
                }}
              >
                8.51 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(247, 251, 253)'
                }}
              >
                4.26 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(216, 236, 246)'
                }}
              >
                19.15 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(247, 251, 253)'
                }}
              >
                4.26 %
              </td>
            </tr>
            <tr>
              <td>14 Mar 2017</td>
              <td className="text-right">132</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(175, 215, 235)'
                }}
              >
                39.39 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(197, 226, 241)'
                }}
              >
                28.79 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(242, 249, 252)'
                }}
              >
                6.82 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(246, 251, 253)'
                }}
              >
                4.55 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(220, 238, 247)'
                }}
              >
                17.42 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(223, 239, 247)'
                }}
              >
                15.91 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(248, 252, 254)'
                }}
              >
                3.79 %
              </td>
            </tr>
            <tr>
              <td>15 Mar 2017</td>
              <td className="text-right">882</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(204, 230, 243)'
                }}
              >
                25.28 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(244, 250, 253)'
                }}
              >
                5.56 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(248, 252, 254)'
                }}
              >
                3.63 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(214, 235, 245)'
                }}
              >
                20.18 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(233, 244, 250)'
                }}
              >
                11.00 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(248, 252, 254)'
                }}
              >
                3.63 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(249, 252, 254)'
                }}
              >
                2.95 %
              </td>
            </tr>
            <tr>
              <td>16 Mar 2017</td>
              <td className="text-right">1222</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(223, 239, 247)'
                }}
              >
                15.63 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(241, 248, 252)'
                }}
              >
                6.96 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(210, 233, 244)'
                }}
              >
                22.09 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(227, 241, 248)'
                }}
              >
                13.83 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(246, 251, 253)'
                }}
              >
                4.75 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(249, 252, 254)'
                }}
              >
                3.03 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(249, 252, 254)'
                }}
              >
                2.95 %
              </td>
            </tr>
            <tr>
              <td>17 Mar 2017</td>
              <td className="text-right">878</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(218, 237, 246)'
                }}
              >
                18.45 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(203, 229, 242)'
                }}
              >
                25.51 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(222, 239, 247)'
                }}
              >
                16.29 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(245, 250, 253)'
                }}
              >
                5.13 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(247, 251, 253)'
                }}
              >
                4.33 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(247, 251, 253)'
                }}
              >
                4.33 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(239, 247, 251)'
                }}
              >
                8.20 %
              </td>
            </tr>
            <tr>
              <td>18 Mar 2017</td>
              <td className="text-right">669</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(188, 222, 239)'
                }}
              >
                32.88 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(207, 231, 243)'
                }}
              >
                23.77 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(234, 245, 250)'
                }}
              >
                10.31 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(239, 247, 251)'
                }}
              >
                8.07 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(246, 251, 253)'
                }}
              >
                4.48 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(233, 244, 250)'
                }}
              >
                11.06 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(238, 247, 251)'
                }}
              >
                8.67 %
              </td>
            </tr>
            <tr>
              <td>19 Mar 2017</td>
              <td className="text-right">666</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(196, 226, 241)'
                }}
              >
                28.98 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(232, 244, 250)'
                }}
              >
                11.41 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(242, 249, 252)'
                }}
              >
                6.76 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(244, 250, 253)'
                }}
              >
                5.71 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(232, 244, 250)'
                }}
              >
                11.56 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(243, 249, 252)'
                }}
              >
                5.86 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(235, 245, 250)'
                }}
              >
                9.91 %
              </td>
            </tr>
            <tr>
              <td>20 Mar 2017</td>
              <td className="text-right">711</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(220, 238, 247)'
                }}
              >
                17.30 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(236, 246, 251)'
                }}
              >
                9.56 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(245, 250, 253)'
                }}
              >
                5.06 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(233, 244, 250)'
                }}
              >
                10.83 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(244, 250, 253)'
                }}
              >
                5.77 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(229, 242, 249)'
                }}
              >
                12.80 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(222, 239, 247)'
                }}
              >
                16.46 %
              </td>
            </tr>
            <tr>
              <td>21 Mar 2017</td>
              <td className="text-right">962</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(208, 232, 244)'
                }}
              >
                22.97 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(237, 246, 251)'
                }}
              >
                8.84 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(228, 242, 249)'
                }}
              >
                13.31 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(240, 248, 252)'
                }}
              >
                7.59 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(232, 244, 250)'
                }}
              >
                11.64 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(222, 239, 247)'
                }}
              >
                16.53 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(248, 252, 254)'
                }}
              >
                3.64 %
              </td>
            </tr>
            <tr>
              <td>22 Mar 2017</td>
              <td className="text-right">735</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(222, 239, 247)'
                }}
              >
                16.19 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(227, 241, 248)'
                }}
              >
                14.01 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(241, 248, 252)'
                }}
              >
                6.94 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(230, 243, 249)'
                }}
              >
                12.52 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(223, 239, 247)'
                }}
              >
                15.65 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(249, 252, 254)'
                }}
              >
                3.40 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(236, 246, 251)'
                }}
              >
                9.66 %
              </td>
            </tr>
            <tr>
              <td>23 Mar 2017</td>
              <td className="text-right">453</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(199, 227, 241)'
                }}
              >
                27.37 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(228, 242, 249)'
                }}
              >
                13.25 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(219, 237, 246)'
                }}
              >
                17.66 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(221, 238, 247)'
                }}
              >
                17.00 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(245, 250, 253)'
                }}
              >
                5.30 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(236, 246, 251)'
                }}
              >
                9.71 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(218, 237, 246)'
                }}
              >
                18.10 %
              </td>
            </tr>
            <tr>
              <td>24 Mar 2017</td>
              <td className="text-right">393</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(214, 235, 245)'
                }}
              >
                20.10 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(223, 239, 247)'
                }}
              >
                16.03 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(202, 229, 242)'
                }}
              >
                25.95 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(244, 250, 253)'
                }}
              >
                5.60 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(232, 244, 250)'
                }}
              >
                11.70 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(221, 238, 247)'
                }}
              >
                17.05 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(242, 249, 252)'
                }}
              >
                6.36 %
              </td>
            </tr>
            <tr>
              <td>25 Mar 2017</td>
              <td className="text-right">344</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(194, 225, 240)'
                }}
              >
                29.94 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(197, 226, 241)'
                }}
              >
                28.78 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(241, 248, 252)'
                }}
              >
                7.27 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(220, 238, 247)'
                }}
              >
                17.44 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(208, 232, 244)'
                }}
              >
                22.97 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(243, 249, 252)'
                }}
              >
                6.10 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(248, 252, 254)'
                }}
              >
                3.49 %
              </td>
            </tr>
            <tr>
              <td>26 Mar 2017</td>
              <td className="text-right">334</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(182, 219, 237)'
                }}
              >
                35.93 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(236, 246, 251)'
                }}
              >
                9.58 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(222, 239, 247)'
                }}
              >
                16.17 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(208, 232, 244)'
                }}
              >
                23.05 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(241, 248, 252)'
                }}
              >
                7.19 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(244, 250, 253)'
                }}
              >
                5.69 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(230, 243, 249)'
                }}
              >
                12.57 %
              </td>
            </tr>
            <tr>
              <td>27 Mar 2017</td>
              <td className="text-right">434</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(213, 234, 245)'
                }}
              >
                20.51 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(220, 238, 247)'
                }}
              >
                17.51 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(193, 224, 240)'
                }}
              >
                30.41 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(246, 251, 253)'
                }}
              >
                4.61 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(236, 246, 251)'
                }}
              >
                9.45 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(226, 241, 248)'
                }}
              >
                14.29 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(223, 239, 247)'
                }}
              >
                15.90 %
              </td>
            </tr>
            <tr>
              <td>28 Mar 2017</td>
              <td className="text-right">366</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(193, 224, 240)'
                }}
              >
                30.33 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(191, 223, 239)'
                }}
              >
                31.42 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(240, 248, 252)'
                }}
              >
                7.65 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(246, 251, 253)'
                }}
              >
                4.64 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(226, 241, 248)'
                }}
              >
                14.48 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(225, 240, 248)'
                }}
              >
                15.03 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(245, 250, 253)'
                }}
              >
                5.19 %
              </td>
            </tr>
            <tr>
              <td>29 Mar 2017</td>
              <td className="text-right">544</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(188, 222, 239)'
                }}
              >
                32.72 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(237, 246, 251)'
                }}
              >
                9.19 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(245, 250, 253)'
                }}
              >
                5.15 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(223, 239, 247)'
                }}
              >
                15.81 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(224, 240, 248)'
                }}
              >
                15.44 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(248, 252, 254)'
                }}
              >
                3.68 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(249, 252, 254)'
                }}
              >
                3.13 %
              </td>
            </tr>
            <tr>
              <td>30 Mar 2017</td>
              <td className="text-right">627</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(229, 242, 249)'
                }}
              >
                12.92 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(243, 249, 252)'
                }}
              >
                6.06 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(231, 243, 249)'
                }}
              >
                11.96 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(223, 239, 247)'
                }}
              >
                15.79 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(245, 250, 253)'
                }}
              >
                5.10 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(248, 252, 254)'
                }}
              >
                3.51 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(240, 248, 252)'
                }}
              >
                7.81 %
              </td>
            </tr>
            <tr>
              <td>31 Mar 2017</td>
              <td className="text-right">404</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(225, 240, 248)'
                }}
              >
                14.85 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(209, 232, 244)'
                }}
              >
                22.52 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(220, 238, 247)'
                }}
              >
                17.57 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(244, 250, 253)'
                }}
              >
                5.69 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(246, 251, 253)'
                }}
              >
                4.46 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(232, 244, 250)'
                }}
              >
                11.39 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(243, 249, 252)'
                }}
              >
                5.94 %
              </td>
            </tr>
            <tr>
              <td>1 Apr 2017</td>
              <td className="text-right">361</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(201, 228, 242)'
                }}
              >
                26.59 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(206, 231, 243)'
                }}
              >
                24.10 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(246, 251, 253)'
                }}
              >
                4.71 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(249, 252, 254)'
                }}
              >
                3.32 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(236, 246, 251)'
                }}
              >
                9.70 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(236, 246, 251)'
                }}
              >
                9.42 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(246, 251, 253)'
                }}
              >
                4.71 %
              </td>
            </tr>
            <tr>
              <td>2 Apr 2017</td>
              <td className="text-right">305</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(194, 225, 240)'
                }}
              >
                30.16 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(238, 247, 251)'
                }}
              >
                8.52 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(245, 250, 253)'
                }}
              >
                4.92 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(231, 243, 249)'
                }}
              >
                11.80 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(236, 246, 251)'
                }}
              >
                9.51 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(251, 253, 254)'
                }}
              >
                1.97 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(231, 243, 249)'
                }}
              >
                12.13 %
              </td>
            </tr>
            <tr>
              <td>3 Apr 2017</td>
              <td className="text-right">446</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(220, 238, 247)'
                }}
              >
                17.26 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(240, 248, 252)'
                }}
              >
                7.40 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(234, 245, 250)'
                }}
              >
                10.31 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(239, 247, 251)'
                }}
              >
                8.07 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(250, 253, 254)'
                }}
              >
                2.91 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(232, 244, 250)'
                }}
              >
                11.66 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(229, 242, 249)'
                }}
              >
                12.78 %
              </td>
            </tr>
            <tr>
              <td>4 Apr 2017</td>
              <td className="text-right">382</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(222, 239, 247)'
                }}
              >
                16.49 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(214, 235, 245)'
                }}
              >
                20.42 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(243, 249, 252)'
                }}
              >
                6.02 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(244, 250, 253)'
                }}
              >
                5.50 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(226, 241, 248)'
                }}
              >
                14.40 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(224, 240, 248)'
                }}
              >
                15.18 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(246, 251, 253)'
                }}
              >
                4.71 %
              </td>
            </tr>
            <tr>
              <td>5 Apr 2017</td>
              <td className="text-right">326</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(200, 228, 242)'
                }}
              >
                26.99 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(232, 244, 250)'
                }}
              >
                11.66 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(246, 251, 253)'
                }}
              >
                4.60 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(228, 242, 249)'
                }}
              >
                13.19 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(231, 243, 249)'
                }}
              >
                11.96 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(249, 252, 254)'
                }}
              >
                3.07 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(240, 248, 252)'
                }}
              >
                7.67 %
              </td>
            </tr>
            <tr>
              <td>6 Apr 2017</td>
              <td className="text-right">220</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(225, 240, 248)'
                }}
              >
                15.00 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(239, 247, 251)'
                }}
              >
                8.18 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(226, 241, 248)'
                }}
              >
                14.55 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(224, 240, 248)'
                }}
              >
                15.45 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(247, 251, 253)'
                }}
              >
                4.09 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(239, 247, 251)'
                }}
              >
                8.18 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(223, 239, 247)'
                }}
              >
                15.91 %
              </td>
            </tr>
            <tr>
              <td>7 Apr 2017</td>
              <td className="text-right">198</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(214, 235, 245)'
                }}
              >
                20.20 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(208, 232, 244)'
                }}
              >
                23.23 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(209, 232, 244)'
                }}
              >
                22.73 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(235, 245, 250)'
                }}
              >
                10.10 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(228, 242, 249)'
                }}
              >
                13.64 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(216, 236, 246)'
                }}
              >
                19.19 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(242, 249, 252)'
                }}
              >
                6.57 %
              </td>
            </tr>
            <tr>
              <td>8 Apr 2017</td>
              <td className="text-right">175</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(210, 233, 244)'
                }}
              >
                22.29 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(203, 229, 242)'
                }}
              >
                25.71 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(239, 247, 251)'
                }}
              >
                8.00 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(234, 245, 250)'
                }}
              >
                10.29 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(222, 239, 247)'
                }}
              >
                16.57 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(238, 247, 251)'
                }}
              >
                8.57 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(248, 252, 254)'
                }}
              >
                3.43 %
              </td>
            </tr>
            <tr>
              <td>9 Apr 2017</td>
              <td className="text-right">176</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(186, 221, 238)'
                }}
              >
                34.09 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(218, 237, 246)'
                }}
              >
                18.18 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(236, 246, 251)'
                }}
              >
                9.66 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(215, 235, 245)'
                }}
              >
                19.89 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(240, 248, 252)'
                }}
              >
                7.39 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(247, 251, 253)'
                }}
              >
                3.98 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(246, 251, 253)'
                }}
              >
                4.55 %
              </td>
            </tr>
            <tr>
              <td>10 Apr 2017</td>
              <td className="text-right">183</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(221, 238, 247)'
                }}
              >
                16.94 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(231, 243, 249)'
                }}
              >
                12.02 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(207, 231, 243)'
                }}
              >
                23.50 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(247, 251, 253)'
                }}
              >
                4.37 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(241, 248, 252)'
                }}
              >
                7.10 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(240, 248, 252)'
                }}
              >
                7.65 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(224, 240, 248)'
                }}
              >
                15.30 %
              </td>
            </tr>
            <tr>
              <td>11 Apr 2017</td>
              <td className="text-right">143</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(210, 233, 244)'
                }}
              >
                22.38 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(200, 228, 242)'
                }}
              >
                27.27 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(238, 247, 251)'
                }}
              >
                8.39 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(240, 248, 252)'
                }}
              >
                7.69 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(243, 249, 252)'
                }}
              >
                6.29 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(210, 233, 244)'
                }}
              >
                22.38 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(244, 250, 253)'
                }}
              >
                5.59 %
              </td>
            </tr>
            <tr>
              <td>12 Apr 2017</td>
              <td className="text-right">128</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(155, 205, 230)'
                }}
              >
                49.22 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(233, 244, 250)'
                }}
              >
                10.94 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(238, 247, 251)'
                }}
              >
                8.59 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(246, 251, 253)'
                }}
              >
                4.69 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(211, 233, 244)'
                }}
              >
                21.88 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(241, 248, 252)'
                }}
              >
                7.03 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(246, 251, 253)'
                }}
              >
                4.69 %
              </td>
            </tr>
            <tr>
              <td>13 Apr 2017</td>
              <td className="text-right">314</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(227, 241, 248)'
                }}
              >
                13.69 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(255, 255, 255)'
                }}
              >
                0.00 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(237, 246, 251)'
                }}
              >
                8.92 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(222, 239, 247)'
                }}
              >
                16.56 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(244, 250, 253)'
                }}
              >
                5.73 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(248, 252, 254)'
                }}
              >
                3.82 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(248, 252, 254)'
                }}
              >
                3.50 %
              </td>
            </tr>
            <tr>
              <td>14 Apr 2017</td>
              <td className="text-right">210</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(211, 233, 244)'
                }}
              >
                21.90 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(236, 246, 251)'
                }}
              >
                9.52 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(222, 239, 247)'
                }}
              >
                16.19 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(238, 247, 251)'
                }}
              >
                8.57 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(248, 252, 254)'
                }}
              >
                3.81 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(245, 250, 253)'
                }}
              >
                5.24 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(193, 224, 240)'
                }}
              >
                30.48 %
              </td>
            </tr>
            <tr>
              <td>15 Apr 2017</td>
              <td className="text-right">130</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(216, 236, 246)'
                }}
              >
                19.23 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(215, 235, 245)'
                }}
              >
                20.00 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(241, 248, 252)'
                }}
              >
                6.92 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(246, 251, 253)'
                }}
              >
                4.62 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(244, 250, 253)'
                }}
              >
                5.38 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(177, 216, 236)'
                }}
              >
                38.46 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(244, 250, 253)'
                }}
              >
                5.38 %
              </td>
            </tr>
            <tr>
              <td>16 Apr 2017</td>
              <td className="text-right">174</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(199, 227, 241)'
                }}
              >
                27.59 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(234, 245, 250)'
                }}
              >
                10.34 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(245, 250, 253)'
                }}
              >
                5.17 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(241, 248, 252)'
                }}
              >
                6.90 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(188, 222, 239)'
                }}
              >
                32.76 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(243, 249, 252)'
                }}
              >
                6.32 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(232, 244, 250)'
                }}
              >
                11.49 %
              </td>
            </tr>
            <tr>
              <td>17 Apr 2017</td>
              <td className="text-right">126</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(202, 229, 242)'
                }}
              >
                26.19 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(242, 249, 252)'
                }}
              >
                6.35 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(236, 246, 251)'
                }}
              >
                9.52 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(173, 214, 235)'
                }}
              >
                40.48 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(239, 247, 251)'
                }}
              >
                7.94 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(239, 247, 251)'
                }}
              >
                7.94 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(216, 236, 246)'
                }}
              >
                19.05 %
              </td>
            </tr>
            <tr>
              <td>18 Apr 2017</td>
              <td className="text-right">153</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(218, 237, 246)'
                }}
              >
                18.30 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(242, 249, 252)'
                }}
              >
                6.54 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(174, 215, 235)'
                }}
              >
                39.87 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(245, 250, 253)'
                }}
              >
                5.23 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(225, 240, 248)'
                }}
              >
                15.03 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(226, 241, 248)'
                }}
              >
                14.38 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(245, 250, 253)'
                }}
              >
                5.23 %
              </td>
            </tr>
            <tr>
              <td>19 Apr 2017</td>
              <td className="text-right">124</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(211, 233, 244)'
                }}
              >
                21.77 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(183, 219, 237)'
                }}
              >
                35.48 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(246, 251, 253)'
                }}
              >
                4.84 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(234, 245, 250)'
                }}
              >
                10.48 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(224, 240, 248)'
                }}
              >
                15.32 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(249, 252, 254)'
                }}
              >
                3.23 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(254, 255, 255)'
                }}
              >
                0.81 %
              </td>
            </tr>
            <tr>
              <td>20 Apr 2017</td>
              <td className="text-right">163</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(149, 202, 229)'
                }}
              >
                52.15 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(235, 245, 250)'
                }}
              >
                9.82 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(229, 242, 249)'
                }}
              >
                12.88 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(227, 241, 248)'
                }}
              >
                14.11 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(248, 252, 254)'
                }}
              >
                3.68 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(250, 253, 254)'
                }}
              >
                2.45 %
              </td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>21 Apr 2017</td>
              <td className="text-right">495</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(238, 247, 251)'
                }}
              >
                8.48 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(241, 248, 252)'
                }}
              >
                7.07 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(237, 246, 251)'
                }}
              >
                8.89 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(249, 252, 254)'
                }}
              >
                3.03 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(252, 254, 255)'
                }}
              >
                1.62 %
              </td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>22 Apr 2017</td>
              <td className="text-right">176</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(200, 228, 242)'
                }}
              >
                27.27 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(214, 235, 245)'
                }}
              >
                20.45 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(240, 248, 252)'
                }}
              >
                7.39 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(240, 248, 252)'
                }}
              >
                7.39 %
              </td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>23 Apr 2017</td>
              <td className="text-right">165</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(196, 226, 241)'
                }}
              >
                29.09 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(231, 243, 249)'
                }}
              >
                12.12 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(239, 247, 251)'
                }}
              >
                7.88 %
              </td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>24 Apr 2017</td>
              <td className="text-right">171</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(209, 232, 244)'
                }}
              >
                22.81 %
              </td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(235, 245, 250)'
                }}
              >
                9.94 %
              </td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>25 Apr 2017</td>
              <td className="text-right">270</td>
              <td
                className="text-right"
                style={{
                  backgroundColor: 'rgb(223, 239, 247)'
                }}
              >
                15.93 %
              </td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
);
