import React, { Fragment } from 'react';
import classnames from 'classnames';

import socialIcons from 'components/utils/SocialIcons';

import DeploymentContainer from 'containers/Deployment.Container';

import ChannelContainer from 'containers/Channel.Container';

import {
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from 'components/Dropdown';

import './DeploymentChannel.scss';
import { DepChannelContainerProps } from './DeploymentChannel.Container';
import { Circle } from 'components/Loading';

interface Props extends DepChannelContainerProps {
  onSelect?: (id: string) => void;
}

class DeploymentChannel extends React.Component<Props> {
  render() {
    return (
      <Fragment>
        <DeploymentContainer>
          {params => {
            const detail = params.selected
              ? params.getDetail(params.selected)
              : null;
            return (
              <Fragment>
                <div className="kata-channel-selector__container">
                  <label className="kata-info__label mr-2">Deployment</label>
                  {params.loading ? (
                    <div className={'kata-dropdown-selector__button'}>
                      Loading...
                      <Circle
                        size={25}
                        className="kata-dropdown-selector__loading"
                      />
                    </div>
                  ) : (
                    <Dropdown
                      block
                      onSelect={this.props.onSelect || params.selectDeployment}
                    >
                      <DropdownToggle
                        className={classnames(
                          'kata-dropdown-selector__button',
                          detail
                            ? 'kata-dropdown-selector__button--filled'
                            : null
                        )}
                      >
                        {detail ? detail.name : 'Select...'}
                      </DropdownToggle>
                      <DropdownMenu className="kata-dropdown-selector__menu">
                        {params.data && params.index.length !== 0 ? (
                          params.index.map(key => (
                            <DropdownItem
                              key={key}
                              value={params.data[key].name}
                            >
                              {params.data[key].name}
                            </DropdownItem>
                          ))
                        ) : (
                          <DropdownItem disabled className="text-muted">
                            No deployments.
                          </DropdownItem>
                        )}
                      </DropdownMenu>
                    </Dropdown>
                  )}
                </div>
              </Fragment>
            );
          }}
        </DeploymentContainer>
        <ChannelContainer>
          {({ index, data, loading, selected, selectChannel, getDetail }) => {
            const detail =
              index.length > 0 && selected ? getDetail(selected) : null;
            return (
              <div className="kata-channel-selector__container">
                <label className="kata-info__label mr-2">Channel</label>
                {loading ? (
                  <div className={'kata-dropdown-selector__button'}>
                    Loading...
                    <Circle
                      size={25}
                      className="kata-dropdown-selector__loading"
                    />
                  </div>
                ) : (
                  <Dropdown block onSelect={selectChannel}>
                    <DropdownToggle
                      className={classnames(
                        'kata-dropdown-selector__button',
                        detail ? 'kata-dropdown-selector__button--filled' : null
                      )}
                    >
                      {detail ? (
                        <Fragment>
                          <img
                            src={
                              socialIcons[detail.type] || socialIcons['generic']
                            }
                            width="15"
                            alt={detail.type}
                            className="mr-2"
                          />
                          {detail.name}
                        </Fragment>
                      ) : (
                        'Select...'
                      )}
                    </DropdownToggle>
                    <DropdownMenu className="kata-dropdown-selector__menu">
                      {data && index.length !== 0 ? (
                        index.map(key => (
                          <DropdownItem key={key} value={data[key].id}>
                            <img
                              src={
                                socialIcons[data[key].type] ||
                                socialIcons['generic']
                              }
                              width="15"
                              alt={data[key].type}
                              className="mr-2"
                            />
                            {data[key].name}
                          </DropdownItem>
                        ))
                      ) : (
                        <DropdownItem disabled className="text-muted">
                          No deployments.
                        </DropdownItem>
                      )}
                    </DropdownMenu>
                  </Dropdown>
                )}
              </div>
            );
          }}
        </ChannelContainer>
      </Fragment>
    );
  }
}

export default DeploymentChannel;
