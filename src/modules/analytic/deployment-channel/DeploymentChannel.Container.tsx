import React from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';

import RootStore from 'interfaces/rootStore';
import { getBotSelected } from 'stores/bot/selectors';
import { getDeploymentSelected } from 'stores/deployment/selectors';
import { selectDeployment } from 'stores/deployment/actions';
import { clearChannelList } from 'stores/deployment/channel/actions';

import DeploymentChannel from './DeploymentChannel';

interface PropsFromState {
  bot: string | null;
  deployment: string | null;
  hasChannel: boolean;
}

interface PropsFromDispatch {
  selectDeployment: (id: string) => void;
  clearChannel: () => void;
}

export interface DepChannelContainerProps
  extends PropsFromDispatch,
    PropsFromState {
  hideChannel?: boolean;
}

class DepChannelContainer extends React.Component<DepChannelContainerProps> {
  componentDidMount() {
    // When no selected deployment exists, we're 100% sure that
    // there's no listed channels as well, so clear it.
    if (!this.props.deployment) {
      this.props.clearChannel();
    }
  }

  render() {
    const { ...rest } = this.props;

    return <DeploymentChannel {...rest} />;
  }
}

const mapStateToProps = ({ bot, deployment }: RootStore): PropsFromState => {
  return {
    bot: getBotSelected(bot),
    deployment: getDeploymentSelected(deployment),
    hasChannel: deployment.channel.index.length > 0
  };
};

const mapDispatchToProps = (dispatch: Dispatch): PropsFromDispatch => {
  return {
    clearChannel: () => dispatch(clearChannelList()),
    selectDeployment: (id: string) => dispatch(selectDeployment(id))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DepChannelContainer);
