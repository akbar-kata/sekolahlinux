import React, { Fragment } from 'react';
import { Dispatch } from 'redux';
import { connect } from 'react-redux';

import { Button } from 'components/Button';
import { EmptyMessage } from 'components/Common';

import RootStore from 'interfaces/rootStore';
import {
  activateAnalytic,
  checkAnalyticEnabled
} from 'stores/analytic/root/actions';
import { getBotSelected } from 'stores/bot/selectors';
import { getChannelIndex } from 'stores/deployment/channel/selectors';
import { getDeploymentSelected } from 'stores/deployment/selectors';

import './Optin.scss';

interface PropsFromState {
  botId: string | null;
  deploymentId: string | null;
  hasChannel: boolean;
}

interface PropsFromDispatch {
  enableAnalytics(
    botId: string,
    deploymentId: string,
    type: 'basic' | 'advanced',
    metrics: string[]
  ): void;
  checkAnalytics(botId: string, deploymentId: string): void;
}

interface Props extends PropsFromState, PropsFromDispatch {
  isAdvanced?: boolean;
}

class Page extends React.Component<Props> {
  onEnableCompleteAnalyticsClick = () => {
    if (this.props.botId && this.props.deploymentId) {
      this.props.enableAnalytics(
        this.props.botId,
        this.props.deploymentId,
        'advanced',
        ['user', 'flow', 'intent', 'text', 'transcript']
      );
    }
  };

  onEnableBasicAnalyticsClick = () => {
    if (this.props.botId && this.props.deploymentId) {
      this.props.enableAnalytics(
        this.props.botId,
        this.props.deploymentId,
        'basic',
        ['user', 'flow', 'intent', 'text']
      );
    }
  };

  componentDidMount() {
    if (this.props.botId && this.props.deploymentId) {
      this.props.checkAnalytics(this.props.botId, this.props.deploymentId);
    }
  }
  componentDidUpdate() {
    if (this.props.botId && this.props.deploymentId) {
      this.props.checkAnalytics(this.props.botId, this.props.deploymentId);
    }
  }

  render() {
    const { isAdvanced } = this.props;
    return (
      <Fragment>
        {isAdvanced ? (
          <EmptyMessage
            image={require('assets/images/enable-analytics.svg')}
            title="Enable Advanced Analytics"
          >
            <p>
              Advanced Analytics allows you to see <strong>User</strong>,{' '}
              <strong>Conversation</strong>, and <strong>Transcript</strong>{' '}
              analytics data.
            </p>
            <Button
              color="primary"
              onClick={this.onEnableCompleteAnalyticsClick}
            >
              Enable Advanced Analytics
            </Button>
          </EmptyMessage>
        ) : (
          <EmptyMessage
            image={require('assets/images/enable-analytics.svg')}
            title="Enable Basic Analytics"
          >
            <p>
              Basic Analytics allows you to see <strong>User</strong> and{' '}
              <strong>Conversation</strong> analytics data.
            </p>
            <Button color="primary" onClick={this.onEnableBasicAnalyticsClick}>
              Enable Basic Analytics
            </Button>
          </EmptyMessage>
        )}
      </Fragment>
    );
  }

  redirectToDeployment = () => {
    // this.props.changeToDeployment();
  };
}

const mapStateToProps = ({ bot, deployment }: RootStore): PropsFromState => {
  return {
    botId: getBotSelected(bot),
    deploymentId: getDeploymentSelected(deployment),
    hasChannel: getChannelIndex(deployment.channel).length > 0
  };
};

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    enableAnalytics(
      botId: string,
      deploymentId: string,
      type: 'basic' | 'advanced',
      metrics: string[]
    ) {
      dispatch(activateAnalytic(botId, deploymentId, type, metrics));
    },
    checkAnalytics(botId: string, deploymentId: string) {
      dispatch(checkAnalyticEnabled(botId, deploymentId));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Page);
