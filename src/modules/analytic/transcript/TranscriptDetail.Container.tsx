import React from 'react';
import { connect } from 'react-redux';

import RootStore from 'interfaces/rootStore';

import {
  conversationRequest,
  clearConversation
} from 'stores/analytic/transcripts/actions';
import { selectBot } from 'stores/bot/actions';
import { selectDeployment } from 'stores/deployment/actions';
import { getBotSelected } from 'stores/bot/selectors';
import { getDeploymentSelected } from 'stores/deployment/selectors';
import {
  getAnalyticTranscriptLoading,
  getAnalyticTranscriptConversation,
  getAnalyticTranscriptError
} from 'stores/analytic/transcripts/selectors';

import TranscriptDetail from './TranscriptDetail';

interface PropsFromState {
  isLoading: boolean;
  error: string | null;
  data: any;
  selectedBot: string | null;
  selectedDeployment: string | null;
}

interface PropsFromDispatch {
  clearAction: Function;
  fetchAction: Function;
  selectBot: Function;
  selectDeployment: Function;
}

interface Props extends PropsFromState, PropsFromDispatch {
  [key: string]: any;
}

class TranscriptContainer extends React.Component<Props, any> {
  componentDidMount() {
    this.props.clearAction();
    this.fetchData(-1, Date.now());
  }

  componentWillReceiveProps(nextProps: Props) {
    // if (
    //   nextProps.selectedProject != null &&
    //   nextProps.selectedProject !== this.props.match.params.botId
    // ) {
    //   this.props.selectBot(this.props.match.params.botId);
    // }
    if (
      nextProps.selectedDeployment != null &&
      nextProps.selectedDeployment !== this.props.match.params.deployment
    ) {
      this.props.selectDeployment(this.props.match.params.deployment);
    }
  }

  componentWillUnmount() {
    this.props.clearAction();
  }

  fetchData = (startDate: number, endDate: number) => {
    this.props.fetchAction(startDate, endDate);
  };

  render() {
    return (
      <TranscriptDetail
        isLoading={this.props.isLoading}
        error={this.props.error}
        data={this.props.data}
        fetchAction={this.fetchData}
        clearAction={this.props.clearAction}
      />
    );
  }
}

const mapStateToProps = ({
  analytic,
  bot,
  deployment
}: RootStore): PropsFromState => {
  return {
    isLoading: getAnalyticTranscriptLoading(
      analytic.transcript,
      'conversation'
    ),
    error: getAnalyticTranscriptError(analytic.transcript, 'conversation'),
    data: getAnalyticTranscriptConversation(analytic.transcript),
    selectedBot: getBotSelected(bot),
    selectedDeployment: getDeploymentSelected(deployment)
  };
};

const mapDispatchToProps = (
  dispatch: Function,
  ownProps: any
): PropsFromDispatch => {
  return {
    clearAction: () => dispatch(clearConversation()),
    fetchAction: (startDate: number, endDate: number) =>
      dispatch(
        conversationRequest(
          ownProps.match.params.botId,
          ownProps.match.params.deployment,
          startDate,
          endDate,
          ownProps.match.params.transcriptId
        )
      ),
    selectBot: (projectId: string, revisionId: string) =>
      dispatch(selectBot(projectId, revisionId)),
    selectDeployment: (deployment: string) =>
      dispatch(selectDeployment(deployment))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TranscriptContainer);
