import React from 'react';
import moment from 'moment';
import DateTime from 'react-datetime';

import { Button } from 'components/Button';
import { DropdownSelector, DropdownItem } from 'components/Dropdown';
// import { Modal, ModalHeader, ModalBody, ModalFooter } from 'components/Modal';

import 'react-datetime/css/react-datetime.css';
import './JumpTo.scss';

interface Props {
  onJump?: (time: number) => any;
}

interface States {
  viewState: 'normal' | 'custom';
  isCustomDateModalOpen: boolean;
  value: moment.Moment;
}

const defaultState: States = {
  viewState: 'normal',
  isCustomDateModalOpen: false,
  value: moment()
};

class JumpTo extends React.PureComponent<Props, States> {
  constructor(props: Props) {
    super(props);
    this.state = Object.assign({}, defaultState);
  }

  closeModal = () => {
    this.setState({
      isCustomDateModalOpen: false
    });
  };

  resetState = () => {
    this.setState(defaultState);
  };

  changeViewState = (state: 'normal' | 'custom' = 'normal') => {
    this.setState({ viewState: state });
  };

  onDateTimeChange = date => {
    this.setState({ value: date });
  };

  handleJump = (type: string, time?: any) => {
    const { onJump } = this.props;
    if (onJump && typeof onJump === 'function') {
      let curTime = moment(time);
      if (type === 'yesterday') {
        curTime = curTime.subtract(1, 'days');
      } else if (type === '3day') {
        curTime = curTime.subtract(3, 'days');
      } else if (type === 'week') {
        curTime = curTime.subtract(1, 'weeks');
      } else if (type === 'month') {
        curTime = curTime.subtract(1, 'months');
      }
      onJump(curTime.valueOf());
    }
  };

  isValidDate = current => {
    return current.isBefore(moment());
  };

  handleSelect = (value: string) => {
    if (value !== 'custom') {
      this.handleJump(value);
    } else {
      this.changeViewState('custom');
    }
  };

  handleDateSelect = () => {
    this.handleJump('custom', this.state.value);
    this.changeViewState('normal');
  };

  render() {
    return (
      <div className="kata-channel-selector__container">
        <label className="kata-info__label mr-2">Jump to</label>
        <DropdownSelector onSelect={this.handleSelect}>
          <DropdownItem value="yesterday">Yesterday</DropdownItem>
          <DropdownItem value="3day">Last 3 Days</DropdownItem>
          <DropdownItem value="week">Last Week</DropdownItem>
          <DropdownItem value="month">Last Month</DropdownItem>
          <DropdownItem divider />
          <DropdownItem value="custom">Custom Time</DropdownItem>
        </DropdownSelector>
        {this.state.viewState === 'custom' && (
          <div className="dropdown-menu kata-dropdown-selector__menu kata-dropdown__menu custom-time show">
            <h6 className="heading6 text-center">Custom Time</h6>
            <DateTime
              className="custom-time__picker"
              value={this.state.value}
              input={false}
              dateFormat="YYYY-MM-DD"
              timeFormat="HH:mm:ss"
              onChange={this.onDateTimeChange}
              isValidDate={this.isValidDate}
            />
            <Button block color="primary" onClick={this.handleDateSelect}>
              Use time
            </Button>
          </div>
        )}
      </div>
    );
  }
}

export default JumpTo;
