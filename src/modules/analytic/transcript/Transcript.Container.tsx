import React from 'react';
import { connect } from 'react-redux';

import RootStore from 'interfaces/rootStore';
import { listRequest, clearList } from 'stores/analytic/transcripts/actions';
import { getBotSelected } from 'stores/bot/selectors';
import { getDeploymentSelected } from 'stores/deployment/selectors';
import {
  getChannelSelected,
  getChannelLoading
} from 'stores/deployment/channel/selectors';
import {
  getAnalyticTranscriptLoading,
  getAnalyticTranscriptList,
  getAnalyticTranscriptError
} from 'stores/analytic/transcripts/selectors';
import { checkAnalyticEnabled } from 'stores/analytic/root/actions';
import { getAnalyticMetricsEnabled } from 'stores/analytic/root/selectors';

import Transcript from './Transcript';
import { Dashboard } from 'components/Dashboard';
import { Board } from 'components/Board';
import DeploymentChannel from '../deployment-channel';
import JumpTo from './JumpTo';
import Optin from '../optin';

interface PropsFromState {
  selectedBot: string | null;
  metrics: string[];
  isLoading: boolean;
  error: string | null;
  data: any;
  isReady: boolean;
  deployment: string | null;
  channel: string | null;
}

interface PropsFromDispatch {
  clearAction: () => Function;
  fetchAction: (
    botId: string,
    deployment: string,
    startDate: number,
    endDate: number,
    channel?: string
  ) => Function;
  checkAnalytic: Function;
}

export interface TranscriptContainerProps
  extends PropsFromState,
    PropsFromDispatch {}

interface HeadingChildrenProps {
  botId: string | null;
  deployment: string | null;
  jumpTo: (time: number) => void;
}

const HeadingChildren = (props: HeadingChildrenProps) => (
  <div className="kata-environment">
    <DeploymentChannel />
    {props.botId && props.deployment ? <JumpTo onJump={props.jumpTo} /> : null}
  </div>
);

class TranscriptContainer extends React.Component<TranscriptContainerProps> {
  jumpTo = (time: number) => {
    this.props.clearAction();
    this.fetchData(-1, time);
  };

  fetchData = (
    startDate: number,
    endDate: number,
    botId: string = this.props.selectedBot as string,
    deployment: string = this.props.deployment as string,
    channel: string = this.props.channel as string
  ) => {
    this.props.fetchAction(botId, deployment, startDate, endDate, channel);
  };

  componentDidMount() {
    const { isReady, selectedBot, deployment, channel } = this.props;

    if (isReady) {
      this.props.clearAction();
      this.props.checkAnalytic(this.props.selectedBot, this.props.deployment);

      if (selectedBot && deployment && channel) {
        this.props.fetchAction(
          selectedBot,
          deployment,
          -1,
          Date.now(),
          channel
        );
      }
    }
  }

  componentWillReceiveProps(nextProps: TranscriptContainerProps) {
    const { deployment, channel } = this.props;

    if (
      nextProps.isReady &&
      (deployment !== nextProps.deployment || channel !== nextProps.channel)
    ) {
      if (nextProps.selectedBot && nextProps.deployment && nextProps.channel) {
        this.props.clearAction();
        this.props.fetchAction(
          nextProps.selectedBot,
          nextProps.deployment,
          -1,
          Date.now(),
          nextProps.channel
        );
      }
    }
  }

  componentWillUnmount() {
    this.props.clearAction();
  }

  render() {
    const { selectedBot, deployment } = this.props;

    return (
      <Dashboard
        title="Conversation Transcripts"
        tooltip="Take a look on what your users are talking about with your bot."
      >
        <Board
          headingChildren={
            <HeadingChildren
              botId={selectedBot}
              deployment={deployment}
              jumpTo={this.jumpTo}
            />
          }
        >
          {this.isAnalyticsDisabled(this.props) ? (
            <Optin isAdvanced />
          ) : (
            <Transcript {...this.props} />
          )}
        </Board>
      </Dashboard>
    );
  }

  private isAnalyticsDisabled({ metrics }: TranscriptContainerProps) {
    return metrics.indexOf('transcript') === -1;
  }
}

const mapStateToProps = ({
  analytic,
  bot,
  deployment
}: RootStore): PropsFromState => {
  const selectedBot = getBotSelected(bot);
  const selectedDeployment = getDeploymentSelected(deployment) as string;
  const metrics = getAnalyticMetricsEnabled(analytic.root);

  return {
    selectedBot,
    metrics,
    isLoading: getAnalyticTranscriptLoading(analytic.transcript, 'list'),
    error: getAnalyticTranscriptError(analytic.transcript, 'list'),
    data: getAnalyticTranscriptList(analytic.transcript),
    isReady:
      selectedBot !== null &&
      selectedDeployment !== null &&
      metrics.indexOf('transcript') !== -1 &&
      !getChannelLoading(deployment.channel, 'fetch'),
    deployment: selectedDeployment,
    channel: getChannelSelected(deployment.channel)
  };
};

const mapDispatchToProps = (dispatch: Function): PropsFromDispatch => {
  return {
    clearAction: () => dispatch(clearList()),
    fetchAction: (
      botId: string,
      deployment: string,
      startDate: number,
      endDate: number,
      channel?: string
    ) => dispatch(listRequest(botId, deployment, startDate, endDate, channel)),
    checkAnalytic(botId: string, deploymentId: string) {
      dispatch(checkAnalyticEnabled(botId, deploymentId));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TranscriptContainer);
