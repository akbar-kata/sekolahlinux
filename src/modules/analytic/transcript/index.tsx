import React from 'react';
import { Switch, Route } from 'react-router-dom';

import Transcript from './Transcript.Container';
import TranscriptDetail from './TranscriptDetail.Container';

export default () => (
  <Switch>
    <Route
      path="/analytic/:botId/transcript/:deployment/:transcriptId"
      component={TranscriptDetail}
    />
    <Route path="/analytic/:botId/transcript" component={Transcript} />
  </Switch>
);
