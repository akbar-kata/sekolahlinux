import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';

import SelectedUrl from 'containers/SelectedUrl';
import { Dashboard } from 'components/Dashboard';
import { Board } from 'components/Board';
import Message from './Message';

import './TranscriptDetail.scss';
import { Button, FloatingButton } from 'components/Button';
import { LoadMoreIcon } from 'components/Icon';

interface Props {
  isLoading: boolean;
  error: string | null;
  data: any[];
  fetchAction: Function;
  clearAction: Function;
}

interface States {
  showButton: boolean;
  previousScroll: number;
}

const messageId = 'transcript-conv-';

class TranscriptDetail extends React.PureComponent<Props, States> {
  messageRef: HTMLDivElement | null;
  constructor(props: Props) {
    super(props);
    this.state = {
      showButton: false,
      previousScroll: 0
    };
  }

  handleScroll = (event: any) => {
    const positionTop = event.target.scrollTop;
    // const positionTop = event.currentTarget.scrollTop;
    if (this.state.previousScroll > positionTop) {
      this.setState({ showButton: true });
    } else {
      this.setState({ showButton: false });
    }

    this.setState({
      previousScroll: positionTop
    });
  };

  renderHeaderContent = (props: Props) => (
    <Fragment>
      <SelectedUrl>
        {selectedId => (
          <Link
            className="btn kata-btn-float kata-btn-float__primary kata-subpage__back-btn"
            to={`/analytic/${selectedId}/transcript/`}
          >
            <i className="icon-arrow-left" />
          </Link>
        )}
      </SelectedUrl>
      <div className="float-left">
        <h1 className="heading1 kata-subpage__title">Transcript Detail</h1>
        <SelectedUrl>
          {selectedId => (
            <Fragment>
              <Link
                to={`/analytic/${selectedId}/transcript/`}
                className="text-muted"
              >
                Transcripts
              </Link>
              <span className="kata-subpage__bread-separator">/</span>
              <span className="text-primary">{`${selectedId}`}</span>
            </Fragment>
          )}
        </SelectedUrl>
      </div>
    </Fragment>
  );

  componentDidUpdate(prevProps: Props) {
    if (prevProps.data.length === 0) {
      const root = document.getElementById('root');
      if (root) {
        const index = this.props.data.length - 1;

        const el = document.getElementById(messageId + index);
        if (el) {
          el.scrollIntoView();
        }
      }
    } else if (prevProps.data !== this.props.data) {
      const el = document.getElementById(`${messageId}0`);

      if (el) {
        window.scrollTo(0, el.offsetTop + el.offsetHeight);
      }
    }
  }

  jumpTo = (time: number) => {
    this.props.clearAction();
    this.props.fetchAction(-1, time);
  };

  jumpToBottom = () => {
    const index = this.props.data.length - 1;

    const el = document.getElementById(messageId + index);
    if (el) {
      el.scrollIntoView({ behavior: 'smooth' });
    }
  };

  renderArrowDown = () => {
    return (
      <FloatingButton
        className="kata-floating-transcript__arrow-down"
        icon="arrow-down"
        onClick={this.jumpToBottom}
      />
    );
  };

  render() {
    return (
      <Dashboard headerContent={this.renderHeaderContent(this.props)}>
        <Board className="kata-chat-transcript" onScroll={this.handleScroll}>
          <div
            ref={el => (this.messageRef = el)}
            className="kata-floating-transcript__load-more"
          >
            {this.props.data && (
              <Button
                className="kata-btn-loadmore"
                onClick={() =>
                  this.props.fetchAction(
                    -1,
                    this.props.data[0].created_at ||
                      this.props.data[0].updated_at
                  )
                }
              >
                <LoadMoreIcon className="icon-spinner mr-1" />
                {/* <i className="icon-refresh mr-1" /> */}
                Load More
              </Button>
            )}
            {this.props.data.map((data, index) => (
              <div key={index} id={messageId + index}>
                {(data.messages &&
                  data.messages.length &&
                  data.messages.map((message, idx) => (
                    <Message
                      key={idx}
                      type="in"
                      data={message}
                      date={data.dateStr}
                    />
                  ))) ||
                  null}
                {(data.responses &&
                  data.responses.length &&
                  data.responses.map((response, idx) => (
                    <Message
                      key={idx}
                      type="out"
                      data={response}
                      date={data.dateStr}
                    />
                  ))) ||
                  null}
              </div>
            ))}
          </div>
        </Board>
        {this.state.showButton && this.renderArrowDown()}
      </Dashboard>
    );
  }
}

export default TranscriptDetail;
