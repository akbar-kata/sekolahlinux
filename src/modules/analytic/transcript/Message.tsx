import React from 'react';

const msgStyle = {
  padding: '15px 20px',
  borderRadius: 20,
  width: '100%'
};

const msgBlue = Object.assign({}, msgStyle, {
  background: '#318bf3',
  color: '#eee'
});

const msgGrey = Object.assign({}, msgStyle, {
  background: '#ddd',
  color: '#555'
});

const msgContainer = {
  maxWidth: '60%',
  margin: '20px 0'
};

const msgIn = Object.assign({}, msgContainer, {
  float: 'right'
});

const msgOut = Object.assign({}, msgContainer, {
  float: 'left'
});

interface Props {
  type: string;
  data: any;
  date: string;
}

class Message extends React.Component<Props, any> {
  constructor(props: any) {
    super(props);
    this.state = {
      showJson: false
    };
  }

  toggleJSON() {
    this.setState({
      showJson: !this.state.showJson
    });
  }

  renderMessage(message: any) {
    if (message) {
      if (message.type === 'data') {
        if (message.payload && message.payload.template_type === 'image') {
          return (
            <a href={message.payload.items.originalContentUrl} target="_blank">
              <img src={message.payload.items.previewImageUrl} alt="Image" />
            </a>
          );
        }

        return <strong>[DATA]</strong>;
      }

      return <span>{message.content || ''}</span>;
    }

    return <span>&nbsp;</span>;
  }

  render() {
    const { data, type, date } = this.props;
    return (
      <div className="clearfix">
        <div style={type === 'in' ? msgIn : msgOut}>
          <div style={type === 'in' ? msgBlue : msgGrey}>
            {this.renderMessage(data)}
          </div>
          <div>
            <small style={{ marginLeft: 15 }}>
              {date} WIB
              <i className="icon-eye v-btm" onClick={() => this.toggleJSON()} />
            </small>
          </div>
          {(this.state.showJson && (
            <figure className="highlight">
              <pre style={{ fontSize: 11 }}>
                {JSON.stringify(data, null, 2)}
              </pre>
            </figure>
          )) ||
            null}
        </div>
      </div>
    );
  }
}

export default Message;
