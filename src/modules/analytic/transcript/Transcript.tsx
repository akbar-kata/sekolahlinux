import React from 'react';
import { Link } from 'react-router-dom';

import { InfiniteScrollSpinner } from 'components/Loading';
import InfiniteScroll from 'components/InfiniteScroll';
import { TranscriptContainerProps } from './Transcript.Container';

import '../Root.scss';

interface Props extends TranscriptContainerProps {}

const width110 = {
  width: 110
};

const colWidth = {
  width: 150
};

class Transcript extends React.PureComponent<Props, any> {
  fetchData = (
    startDate: number,
    endDate: number,
    botId: string = this.props.selectedBot as string,
    deployment: string = this.props.deployment as string,
    channel: string = this.props.channel as string
  ) => {
    this.props.fetchAction(botId, deployment, startDate, endDate, channel);
  };

  renderMessages(message: any) {
    return (
      <tbody>
        <tr>
          <td colSpan={4} className="text-center pv5">
            {message}
          </td>
        </tr>
      </tbody>
    );
  }

  renderLoading() {
    return this.renderMessages(<InfiniteScrollSpinner bsStyle="gray" />);
  }

  renderError() {
    return this.renderMessages(
      <span className="text-danger">{this.props.error}</span>
    );
  }

  renderData() {
    const { selectedBot, deployment, data } = this.props;
    return data && data.length ? (
      <InfiniteScroll
        element="tbody"
        pageStart={0}
        loadMore={() => {
          const startTime = -1;
          const endTime =
            data[data.length - 1].updated_at ||
            data[data.length - 1].created_at;
          this.fetchData(startTime, endTime);
        }}
        hasMore={true || false}
        loader={
          <tr>
            <td colSpan={4} className="text-center">
              <InfiniteScrollSpinner bsStyle="gray" />
            </td>
          </tr>
        }
        initialLoad={false}
        useWindow
      >
        {data.map((item, index) => (
          <tr key={index}>
            <td className="text-center">
              <small>{item.startTime} WIB</small>
            </td>
            <td>
              <strong>{item.name || '-'}</strong>
            </td>
            <td>
              <small>{item.sessionid}</small>
            </td>
            <td className="text-center">
              <Link
                to={`/analytic/${selectedBot}/transcript/${deployment}/${
                  item.sessionid
                }`}
              >
                <i className="icon-view text-primary" />
              </Link>
            </td>
          </tr>
        ))}
      </InfiniteScroll>
    ) : (
      this.renderMessages('Transcript is empty')
    );
  }

  render() {
    return (
      <table className="kata-table table table-striped table-hover">
        <thead>
          <tr>
            <th style={width110}>Start Time</th>
            <th style={colWidth}>Name</th>
            <th>Session</th>
            <th>Action</th>
          </tr>
        </thead>
        {this.props.isLoading && this.props.data.length === 0
          ? this.renderLoading()
          : this.renderData()}
      </table>
    );
  }
}

export default Transcript;
