import * as env from 'utils/env';

const menus = {
  project: [
    {
      title: 'Bot',
      path: 'bot',
      icon: 'bot'
    },
    {
      title: 'NLU',
      path: 'nlu',
      icon: 'nlu'
    },
    {
      title: 'Deploy',
      path: 'deployment',
      icon: 'building'
    },
    {
      title: 'Analytics',
      path: 'analytic',
      icon: 'analytics'
    }
  ],
  root: [
    {
      title: 'Account',
      path: 'setting/account',
      icon: 'account'
    },
    {
      title: 'Team',
      path: 'setting/teams',
      icon: 'users'
    }
  ]
};

if (env.getRuntimeEnv('REACT_APP_RUNTIME_WITH_CMS') === 'true') {
  menus['project'].splice(2, 0, {
    title: 'CMS',
    path: 'cms',
    icon: 'cms'
  });
}

const submenus = {
  admin: [
    {
      title: 'Overal analytic',
      path: 'analytic',
      icon: 'analytics'
    },
    {
      title: 'User management',
      path: 'users',
      icon: 'analytics'
    },
    {
      title: 'Pricing',
      path: 'pricing',
      icon: 'analytics'
    },
    {
      title: 'Subscription',
      path: 'subscription',
      icon: 'analytics'
    },
    {
      title: 'Invoice',
      path: 'invoice',
      icon: 'analytics'
    }
  ],
  deployment: [
    {
      title: 'Overview',
      path: 'overview',
      icon: 'cms'
    },
    {
      title: 'Environment',
      path: 'environments',
      icon: 'building'
    }
  ],
  bot: [
    {
      title: 'Conversation Flows',
      path: 'flow',
      icon: 'entity'
    },
    {
      title: 'NLUs',
      path: 'nlu',
      icon: 'nlus'
    },
    {
      title: 'Methods',
      path: 'method',
      icon: 'methods'
    },
    {
      title: 'Revision List',
      path: 'version',
      icon: 'version-lists'
    },
    // {
    //   title: 'Emulator',
    //   path: 'emulator',
    //   icon: 'bot'
    // },
    {
      title: 'Error Logs',
      path: 'logs-error',
      icon: 'error-log'
    },
    {
      title: 'Options',
      path: 'config',
      icon: 'settings-cms'
    }
  ],
  nlu: [
    {
      title: 'Entities',
      path: 'entity',
      icon: 'entity'
    },
    {
      title: 'Training',
      path: 'training',
      icon: 'training'
    },
    {
      title: 'Logs',
      path: 'log',
      icon: 'log'
    },
    {
      title: 'Revision List',
      path: 'version',
      icon: 'version-lists'
    },
    {
      title: 'Settings',
      path: 'settings',
      icon: 'settings-cms'
    }
  ],
  cms: [
    {
      title: 'Pages',
      path: 'pages',
      icon: 'cms'
    },
    {
      title: 'Revision Lists',
      path: 'revisions',
      icon: 'forms'
    },
    {
      title: 'Users',
      path: 'users',
      icon: 'users'
    },
    {
      title: 'Setting',
      path: 'settings',
      icon: 'settings-cms'
    }
  ]
};

export default { menus, submenus };
