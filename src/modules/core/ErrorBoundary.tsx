import React from 'react';
import * as Sentry from '@sentry/browser';

interface Props {
  component?: string;
}
interface States {
  hasError: boolean;
}

class ErrorBoundary extends React.Component<Props, States> {
  constructor(props: Props) {
    super(props);
    this.state = { hasError: false };
  }

  componentDidCatch(error: Error, errorInfo: React.ErrorInfo) {
    // Display fallback UI
    this.setState({ hasError: true });
    Sentry.withScope(scope => {
      Object.keys(errorInfo).forEach(key => {
        scope.setExtra(key, errorInfo[key]);
      });
      Sentry.captureException(error);
    });
  }

  render() {
    return this.props.children;
  }
}

export default ErrorBoundary;
