import React from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps, withRouter } from 'react-router-dom';

import { Robot } from 'components/Loading';

import RootStore from 'interfaces/rootStore';
import { getLoading } from 'stores/app/loadings/selectors';
import MODULE from 'stores/module/types';
import { fetchModuleRequest } from 'stores/module/actions';

interface PropsFromState {
  isLoading: boolean;
}

interface PropsFromDispatch {
  fetchModules: () => any;
}

interface Props
  extends PropsFromState,
    PropsFromDispatch,
    RouteComponentProps<any> {}

const style = {
  position: 'absolute' as 'absolute',
  top: '50%',
  marginTop: -80,
  left: 0,
  right: 0
};

class ModuleContainer extends React.Component<Props> {
  componentDidMount() {
    this.props.fetchModules();
  }

  render() {
    if (this.props.isLoading) {
      return (
        <div className="text-center" style={style}>
          <Robot />
        </div>
      );
    }

    return <React.Fragment>{this.props.children}</React.Fragment>;
  }
}

const mapStateToProps = (store: RootStore): PropsFromState => {
  return {
    isLoading: getLoading(store.app.loadings, MODULE.FETCH_MODULE_REQUEST)
  };
};

const mapDispatchToProps = {
  fetchModules: fetchModuleRequest
};

export default withRouter<any>(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(ModuleContainer)
);
