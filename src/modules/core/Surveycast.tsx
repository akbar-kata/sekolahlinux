import React from 'react';
import Cookies from 'js-cookie';

import { Modal, ModalFooter, ModalBody } from 'components/Modal';
import { Button } from 'components/Button';

import './Newscast.scss';
import * as env from 'utils/env';
const CK_SURVEY_READ = 'ktnews.survey_read';

interface States {
  open: boolean;
}

export default class Surveycast extends React.Component<{}, States> {
  state = {
    open: true
  };
  onCloseModal = () => {
    this.setState({
      open: false
    });
  };
  onOpenSurvey = () => {
    const newsUrl = env.getRuntimeEnv(
      'REACT_APP_RUNTIME_SURVEY_URL',
      env.defaultEnvs['REACT_APP_RUNTIME_SURVEY_URL']
    );
    window.open(newsUrl, '_blank');
  };

  componentDidMount() {
    // The newscast is only visible for the first time
    if (Cookies.get(CK_SURVEY_READ) !== 'true') {
      Cookies.set(CK_SURVEY_READ, 'true');
    } else {
      this.setState({
        open: false
      });
    }
  }

  render() {
    return (
      <div className="kata-newscast">
        <Modal
          show={this.state.open}
          onClose={this.onCloseModal}
          className="kata-newscast__modal"
        >
          <ModalBody className="kata-newscast__modal-body">
            <div className="kata-newscast__header">
              <img
                className="kata-newscast__header-image"
                src={require('assets/images/survey-illustration.svg')}
                alt="Kata Platform 2.5 Survey"
              />
            </div>
            <div className="kata-newscast__content">
              <h3 className="kata-newscast__content-heading">
                Kata Platform 2.5 Survey
              </h3>
              <p className="kata-newscast__content-text">
                On July Friday The 13, we released the new version of Kata
                Platform that we've been working on since early this year. Now,
                it's time to hear from you. Let us know what you think of this
                new version by taking this short survey.
              </p>
            </div>
          </ModalBody>
          <ModalFooter>
            <Button onClick={this.onOpenSurvey} color="primary">
              Take Survey
            </Button>
            <Button color="secondary" onClick={this.onCloseModal}>
              Try Now
            </Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}
