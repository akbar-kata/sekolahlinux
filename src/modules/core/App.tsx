import React, { Fragment } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

import * as env from 'utils/env';
import AnalyticsBotSelector from './AnalyticsBotSelector/Container';
import ProjectSelector from './ProjectSelector/Container';
import { BotSelector } from './BotSelector';
import { CmsSelector } from './CmsSelector';
import { DeploymentsSelector } from './DeploymentsSelector';
import { NluSelector } from './NluSelector';
import {
  Container,
  Content,
  Sidebar,
  SidebarMain,
  SidebarMainMenu,
  SidebarSub,
  SidebarSubMenu,
  Topbar
} from 'components/Layout';
import AppRoot from 'components/Layout/AppRoot';
import { Robot } from 'components/Loading';
import { HollowButton } from 'components/Button';
import ModuleContainer from 'modules/core/ModuleContainer';

import ProfileMenu from './ProfileMenu/Container';
import AnalyticsContainer from './AnalyticsContainer';
import ProjectContainer from './ProjectContainer';
import sidebarData from './sidebarData';
import Newscast from './Newscast';
import Newsbar from './Newsbar';
import ModuleName from './moduleName';

const Admin = React.lazy(() => import('../admin'));
const BotStudio = React.lazy(() => import('../botstudio'));
const NLStudio = React.lazy(() => import('../nlstudio'));
const Cms = React.lazy(() => import('../cms'));
const Deployments = React.lazy(() => import('../deployments'));
// const Analytic = React.lazy(() => import('../analytic'));
const Settings = React.lazy(() => import('../setting'));
const Project = React.lazy(() => import('../project'));
const Module = React.lazy(() => import('../module'));

const Logo = require('assets/images/logo.svg');

interface Props {
  isAdmin: boolean;
  prevLoc: string;
  currLoc: string;
  isSidebarCollapsed: boolean;
  selectedProject: string | null;
  goBack: Function;
}

class App extends React.Component<Props> {
  getPath = (path: string) => {
    if (this.props.currLoc === 'setting') {
      return `/${path}`;
    }
    return `/project/${this.props.selectedProject}/${path}`;
  };

  getSubPath = (subpath: string) => {
    if (this.props.currLoc === 'setting') {
      return `/${this.props.currLoc}/${subpath}`;
    }
    return `/project/${this.props.selectedProject}/${
      this.props.currLoc
    }/${subpath}`;
  };

  getContainerEl(loc: string) {
    switch (loc) {
      case 'analytic':
        return AnalyticsContainer;
      case 'module':
        return ModuleContainer;
      default:
        return React.Fragment;
    }
  }

  getSelector(loc: string) {
    switch (loc) {
      case 'bot':
        return BotSelector;
      case 'nlu':
        return NluSelector;
      case 'analytic':
        return AnalyticsBotSelector;
      case 'cms':
        return CmsSelector;
      case 'deployment':
        return DeploymentsSelector;
      default:
        return null;
    }
  }

  render() {
    const {
      currLoc,
      prevLoc,
      isAdmin,
      isSidebarCollapsed,
      selectedProject,
      goBack
    } = this.props;
    const submenus = sidebarData.submenus[currLoc] || [];

    const ContainerEl = this.getContainerEl(currLoc);
    const Title = this.getSelector(currLoc);

    const sideBarRoot = currLoc === 'setting' ? 'root' : 'project';
    const sideBarHidden = currLoc === 'project';
    const hasTopBar = currLoc === 'setting' || !sideBarHidden;

    const isSidebarMaintenance = currLoc === 'analytic';

    const isSidebarSetting = currLoc === 'setting';

    return (
      <AppRoot>
        {hasTopBar ? (
          <Topbar logo={Logo}>
            <div className="kata-topbar__left">
              <div className="kata-topbar__selector">
                {currLoc === 'setting' ? (
                  <HollowButton onClick={goBack}>
                    <i className="icon-arrow-left mr-1" />
                    Back{' '}
                    {ModuleName[prevLoc] ? `to ${ModuleName[prevLoc]}` : ''}
                  </HollowButton>
                ) : !!selectedProject ? (
                  <ProjectSelector />
                ) : (
                  <HollowButton block loading>
                    Loading...
                  </HollowButton>
                )}
              </div>
            </div>
            <div className="kata-topbar__right">
              <ProfileMenu />
            </div>
          </Topbar>
        ) : null}
        <Container hasTop={hasTopBar}>
          <Sidebar
            isHidden={sideBarHidden}
            collapsed={
              isSidebarCollapsed || isSidebarMaintenance || isSidebarSetting
            }
            hasTop={hasTopBar}
          >
            <SidebarMain>
              <Fragment>
                {sidebarData.menus[sideBarRoot].map((menu, index) => (
                  <SidebarMainMenu
                    key={index}
                    to={this.getPath(menu.path)}
                    icon={menu.icon}
                  >
                    {menu.title}
                  </SidebarMainMenu>
                ))}
              </Fragment>
              {isAdmin && (
                <SidebarMainMenu to="/admin" icon="settings">
                  Admin
                </SidebarMainMenu>
              )}
            </SidebarMain>
            <SidebarSub titleElement={Title ? <Title /> : null}>
              <Fragment>
                {submenus.map((menu, index) => (
                  <SidebarSubMenu
                    key={index}
                    to={this.getSubPath(menu.path)}
                    icon={menu.icon}
                  >
                    {menu.title}
                  </SidebarSubMenu>
                ))}
              </Fragment>
            </SidebarSub>
          </Sidebar>
          <Content>
            <ProjectContainer>
              <ContainerEl>
                <React.Suspense fallback={<Robot />}>
                  <Switch>
                    {isAdmin && <Route path="/admin" component={Admin} />}
                    <Route path="/project" exact component={Project} />
                    <Route
                      path="/project/:projectId/bot"
                      component={BotStudio}
                    />
                    <Route
                      path="/project/:projectId/nlu"
                      component={NLStudio}
                    />
                    <Route
                      path="/project/:projectId/module"
                      component={Module}
                    />
                    <Route path="/project/:projectId/cms" component={Cms} />
                    <Route
                      path="/project/:projectId/deployment"
                      component={Deployments}
                    />
                    {/* <Route
                      path="/project/:projectId/analytic"
                      component={Analytic}
                    /> */}
                    <Route path="/setting" component={Settings} />
                    <Route render={() => <Redirect to="/project" />} />
                  </Switch>
                </React.Suspense>
              </ContainerEl>
            </ProjectContainer>
            {env.getRuntimeEnv('REACT_APP_RUNTIME_NEWS_25_BAR') === 'true' ? (
              <Newsbar />
            ) : null}
            {env.getRuntimeEnv('REACT_APP_RUNTIME_NEWS_30_CAST') === 'true' ? (
              <Newscast />
            ) : null}
          </Content>
        </Container>
      </AppRoot>
    );
  }
}

export default App;
