import React from 'react';
import Cookies from 'js-cookie';

import './Newsbar.scss';
import * as env from 'utils/env';

const CK_NEWSBAR_READ = 'ktnews.bar_read';

interface States {
  open: boolean;
}

export default class Newsbar extends React.Component<{}, States> {
  state = {
    open: true
  };
  onClose = () => {
    this.setState({
      open: false
    });

    Cookies.set(CK_NEWSBAR_READ, 'true');
  };

  componentDidMount() {
    // The newsbar is only visible until the user clicked close
    if (Cookies.get(CK_NEWSBAR_READ) === 'true') {
      this.setState({
        open: false
      });
    }
  }
  render() {
    if (!this.state.open) {
      return null;
    }
    const newsUrl = env.getRuntimeEnv(
      'KATA_NEWS_URL',
      env.defaultEnvs['REACT_APP_RUNTIME_NEWS_URL']
    );
    return (
      <div className="kata-newsbar">
        <div className="kata-newsbar__icon">
          <i className="icon-info" />
        </div>
        <div className="kata-newsbar__text">
          Meet the new Kata Platform 2.5. Better UX, Error Logs, and general
          overhaul on navigation based on our Design System.
        </div>
        <div className="kata-newsbar__text">
          <a href={newsUrl} target="_blank">
            What's New
          </a>
        </div>
        <div className="kata-newsbar__close" onClick={this.onClose}>
          <i className="icon-close" />
        </div>
      </div>
    );
  }
}
