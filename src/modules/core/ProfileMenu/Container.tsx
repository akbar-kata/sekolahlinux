import React from 'react';
import { connect } from 'react-redux';

import { getFromLocalStorage } from 'stores/user/auth/api';
import { reqSwitchUser } from 'stores/user/auth/actions';
import { getAuthToken, getAuthSelected } from 'stores/user/auth/selectors';
import ProfileMenu from './ProfileMenu';

interface PropsFromState {
  teams: any;
  userDetail: any;
  selected: any;
  menus: string[];
}

interface PropsFromDispatch {
  reqSwitchUser: Function;
}

interface Props extends PropsFromState, PropsFromDispatch {}

interface States {}

class ProfileSectionContainer extends React.Component<Props, States> {
  render() {
    return <ProfileMenu {...this.props} />;
  }
}

const mapStateToProps = ({ app, auth }: any): PropsFromState => {
  const token: any = getAuthToken(auth) || {};
  const teams = getFromLocalStorage('teams');

  return {
    teams,
    userDetail: token.detail,
    selected: getAuthSelected(auth),
    menus: app.menus
  };
};

const mapDispatchToProps = (dispatch: Function): PropsFromDispatch => {
  return {
    reqSwitchUser: (id: string, type: 'team' | 'user', username: string) =>
      dispatch(reqSwitchUser(id, type, username))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProfileSectionContainer);
