import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { Scrollbars } from 'react-custom-scrollbars';

import {
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from 'components/Dropdown';
import LogoutButton from 'modules/auth/LogoutButton';
import { randomizeAvatarWithSeed } from 'utils/avatars';

import './ProfileMenu.scss';

interface Props {
  userDetail: any;
  teams: any;
  selected: any;
  menus: string[];
  reqSwitchUser: Function;
}

interface States {
  isOpen: boolean;
}

class ProfileMenu extends Component<Props, States> {
  state = {
    isOpen: false
  };

  onSwitchUser = (
    id: string,
    type: 'team' | 'user',
    username: string
  ) => () => {
    if (this.props.selected && id !== this.props.selected.id) {
      this.props.reqSwitchUser(id, type, username);
    }
  };

  render() {
    const { userDetail, teams, selected } = this.props;
    return (
      <div
        className="kata-profile"
        title={`Logged in as ${selected.username}. Click to switch user/teams.`}
      >
        <div className="kata-profile__container">
          <div className="kata-profile__left">
            <img
              src={randomizeAvatarWithSeed(
                selected.username,
                selected.type || 'user'
              )}
              alt="Bot"
              className="kata-profile__avatar"
            />
            <span className="kata-profile__text">{selected.username}</span>
          </div>
          <div className="kata-profile__right">
            <Dropdown>
              <DropdownToggle className="kata-profile__toggle" />
              <DropdownMenu className="kata-profile__menu">
                {userDetail && (
                  <DropdownItem
                    className={
                      userDetail.id === selected.id
                        ? 'kata-profile__team--active'
                        : undefined
                    }
                    onClick={this.onSwitchUser(
                      userDetail.id,
                      'user',
                      userDetail.username
                    )}
                  >
                    <img
                      src={randomizeAvatarWithSeed(
                        userDetail.username,
                        userDetail.type || 'user'
                      )}
                      className="kata-profile__team-avatar"
                    />{' '}
                    as {userDetail.username}
                  </DropdownItem>
                )}
                <DropdownItem divider />
                <DropdownItem header>Switch Team</DropdownItem>
                <Scrollbars autoHeight autoHeightMax={200}>
                  {teams && teams.length
                    ? teams.map((team, index) => (
                        <DropdownItem
                          key={index}
                          className={
                            team.id === selected.id
                              ? 'kata-profile__team--active'
                              : undefined
                          }
                          onClick={this.onSwitchUser(
                            team.id,
                            'team',
                            team.username
                          )}
                        >
                          <img
                            src={
                              team.id
                                ? randomizeAvatarWithSeed(
                                    team.username,
                                    team.type || 'team'
                                  )
                                : require('assets/images/no-avatar.png')
                            }
                            className="kata-profile__team-avatar"
                          />{' '}
                          {team.username}
                        </DropdownItem>
                      ))
                    : null}
                </Scrollbars>
                <DropdownItem divider />
                <NavLink
                  to="/setting/account"
                  className="dropdown-item kata-dropdown__item"
                  activeClassName=""
                >
                  Account
                </NavLink>
                <NavLink
                  to="/setting/teams"
                  className="dropdown-item kata-dropdown__item"
                  activeClassName=""
                >
                  Manage Team
                </NavLink>
                <DropdownItem>
                  <LogoutButton />
                </DropdownItem>
              </DropdownMenu>
            </Dropdown>
          </div>
        </div>
      </div>
    );
  }
}

export default ProfileMenu;
