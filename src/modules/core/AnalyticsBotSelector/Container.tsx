import React from 'react';
import { connect } from 'react-redux';
import { withRouter, NavLink, RouteComponentProps } from 'react-router-dom';

import { fetchBotRequest, selectBot } from 'stores/bot/actions';
import { getBotData, getBotIndex, getBotSelected } from 'stores/bot/selectors';
import { DropdownSelector, DropdownItem } from 'components/Dropdown';
import { isDrawerOpen } from 'stores/app/drawer/selectors';

interface PropsFromState {
  index: string[];
  data: any;
  selected: string | null;
  isOpen: boolean;
}

interface PropsFromDispatch {
  fetchBotRequest: Function;
  selectBot: Function;
}

interface Props
  extends PropsFromState,
    PropsFromDispatch,
    RouteComponentProps<any> {}

class AnalyticsBotSelectorContainer extends React.Component<Props, any> {
  handleSelect = (val: any) => {
    this.props.selectBot(val as string);
  };

  render() {
    const { selected, index, data } = this.props;
    return (
      <>
        <DropdownSelector
          value={selected && data[selected] ? data[selected].name : ''}
          onSelect={this.handleSelect}
          // loading={isLoading}
          block
          className="mb-3"
        >
          {index && index.length
            ? index.slice(0, 4).map(botId => (
                <DropdownItem key={botId} value={botId}>
                  {data[botId].name}
                </DropdownItem>
              ))
            : null}
          <DropdownItem divider />
          <DropdownItem>
            <NavLink to="/analytic" activeClassName="">
              See all bots
            </NavLink>
          </DropdownItem>
        </DropdownSelector>
      </>
    );
  }
}

const mapStateToProps = ({ bot, app }: any): PropsFromState => {
  return {
    index: getBotIndex(bot),
    data: getBotData(bot),
    selected: getBotSelected(bot),
    isOpen: isDrawerOpen(app.drawer, 'NewBotForm')
  };
};

const mapDispatchToProps = (dispatch: Function): PropsFromDispatch => {
  return {
    fetchBotRequest: () => dispatch(fetchBotRequest()),
    selectBot: (projectId: string, revisionId: string) =>
      dispatch(selectBot(projectId, revisionId))
  };
};

export default withRouter<any>(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(AnalyticsBotSelectorContainer)
);
