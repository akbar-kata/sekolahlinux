import React from 'react';
import Cookies from 'js-cookie';

import { Modal, ModalFooter, ModalBody } from 'components/Modal';
import { Button } from 'components/Button';

import './Newscast.scss';
import * as env from 'utils/env';

// IMPORTANT: change this every time we publish a new announcement
const CK_NEWSCAST_READ = 'ktnews.cast_30_read';

interface States {
  open: boolean;
}

export default class Newscast extends React.Component<{}, States> {
  state = {
    open: true
  };
  onCloseModal = () => {
    this.setState({
      open: false
    });
  };
  onOpenDocumentation = () => {
    const newsUrl = env.getRuntimeEnv(
      'REACT_APP_RUNTIME_NEWS_URL',
      env.defaultEnvs['REACT_APP_RUNTIME_NEWS_URL']
    );
    window.open(newsUrl, '_blank');
  };

  componentDidMount() {
    // The newscast is only visible for the first time
    if (Cookies.get(CK_NEWSCAST_READ) !== 'true') {
      Cookies.set(CK_NEWSCAST_READ, 'true', {
        expires: 365 // one year should be enough
      });
    } else {
      this.setState({
        open: false
      });
    }
  }

  render() {
    return (
      <div className="kata-newscast">
        <Modal
          show={this.state.open}
          onClose={this.onCloseModal}
          className="kata-newscast__modal"
        >
          <ModalBody className="kata-newscast__modal-body">
            <div className="kata-newscast__header">
              <img
                className="kata-newscast__header-image"
                src={require('assets/images/release-3.svg')}
                alt="What's New on Platform 2.5"
              />
            </div>
            <div className="kata-newscast__content">
              <h3 className="kata-newscast__content-heading">
                Say Hello to Kata Platform 3.0
              </h3>
              <p className="kata-newscast__content-text">
                We released several new features to help you build a more
                comprehensive chatbot solution.
              </p>
            </div>
          </ModalBody>
          <ModalFooter>
            <Button onClick={this.onOpenDocumentation} color="primary">
              What's New
            </Button>
            <Button color="secondary" onClick={this.onCloseModal}>
              Try Now
            </Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}
