import React from 'react';

const BotSelector: React.SFC = () => (
  <h2 className="kata-sidebar-container-title--bot">Bot Studio</h2>
);

export default BotSelector;
