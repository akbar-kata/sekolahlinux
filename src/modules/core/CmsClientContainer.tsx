import React from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps, withRouter } from 'react-router-dom';

import Store from 'interfaces/rootStore';

import CmsClient from 'modules/cms';

import CMS_CLIENT from 'stores/cms/types';
import { request } from 'stores/common/actions';
import { getLoading } from 'stores/app/loadings/selectors';

import { Robot } from 'components/Loading';

const CONTAINER_ID = 'cms-container';

interface PropsFromState {
  isLoading: boolean;
}

interface PropsFromDispatch {
  loadInvitedCms();
}

interface Props
  extends PropsFromState,
    PropsFromDispatch,
    RouteComponentProps<any> {
  children: any;
}

const style = {
  position: 'absolute' as 'absolute',
  top: '50%',
  marginTop: -80,
  width: '100%'
};

class CmsContainer extends React.Component<Props> {
  componentDidMount() {
    this.props.loadInvitedCms();
  }
  render() {
    return this.props.isLoading ? (
      <div className="text-center" style={style}>
        <Robot />
      </div>
    ) : (
      <CmsClient />
    );
  }
}

const mapStateToProps = ({ nlu, app }: Store): PropsFromState => {
  return {
    isLoading: getLoading(app.loadings, CONTAINER_ID)
  };
};

const mapDispatchToProps: PropsFromDispatch = {
  loadInvitedCms: () => request(CMS_CLIENT.FETCH_REQUEST)
};

export default withRouter<any>(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(CmsContainer)
);
