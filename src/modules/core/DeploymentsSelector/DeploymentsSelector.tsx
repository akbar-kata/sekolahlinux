import React from 'react';

const DeploymentsSelector: React.SFC = () => (
  <h2 className="kata-sidebar-container-title--deployment">Deployments</h2>
);

export default DeploymentsSelector;
