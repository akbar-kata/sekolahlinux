import React, { ReactNode } from 'react';
import { connect } from 'react-redux';
import { withRouter, RouteComponentProps } from 'react-router-dom';

import { Robot } from 'components/Loading';
import { isRootURL } from 'utils/url';
import PROJECT_TYPES from 'stores/project/types';
import { getLoading } from 'stores/app/loadings/selectors';
import RootStore from 'interfaces/rootStore';
import {
  fetchProjectsRequest,
  fetchTemplatesRequest
} from 'stores/project/actions';

interface PropsFromState {
  isLoading: boolean;
}

interface PropsFromDispatch {
  fetchProjects: (opts?: { withSelect: boolean }) => any;
  fetchTemplates: () => any;
}

interface Props
  extends PropsFromState,
    PropsFromDispatch,
    RouteComponentProps<any> {
  children: ReactNode;
}

const style = {
  position: 'absolute' as 'absolute',
  top: '50%',
  marginTop: -80,
  left: 0,
  right: 0
};

class ProjectContainer extends React.Component<Props> {
  componentDidMount() {
    const currentLocation = this.props.history.location.pathname;
    if (isRootURL(currentLocation)) {
      this.props.fetchProjects({ withSelect: false });
      this.props.fetchTemplates();
    } else {
      this.props.fetchProjects();
    }
  }

  render() {
    if (this.props.isLoading) {
      return (
        <div className="text-center" style={style}>
          <Robot />
        </div>
      );
    }

    return this.props.children;
  }
}

const mapStateToProps = (store: RootStore): PropsFromState => {
  return {
    isLoading: getLoading(
      store.app.loadings,
      PROJECT_TYPES.FETCH_PROJECT_REQUEST
    )
  };
};

const mapDispatchToProps = {
  fetchProjects: fetchProjectsRequest,
  fetchTemplates: fetchTemplatesRequest
};

export default withRouter<any>(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(ProjectContainer)
);
