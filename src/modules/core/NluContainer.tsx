import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { withRouter, RouteComponentProps } from 'react-router-dom';

import RootStore from 'interfaces/rootStore';
import { getLoading } from 'stores/app/loadings/selectors';
import { Robot } from 'components/Loading';

const CONTAINER_ID = 'nlu-container';

interface PropsFromState {
  isLoading: boolean;
}

interface PropsFromDispatch {
  fetchNluRequest: Function;
}

interface Props
  extends PropsFromState,
    PropsFromDispatch,
    RouteComponentProps<any> {
  children: any;
}

const style = {
  position: 'absolute' as 'absolute',
  top: '50%',
  marginTop: -80,
  left: 0,
  right: 0
};

const NluContainer: React.SFC<Props> = ({ isLoading, children }) => {
  return isLoading ? (
    <div className="text-center" style={style}>
      <Robot />
    </div>
  ) : (
    <Fragment>{children}</Fragment>
  );
};

const mapStateToProps = ({ app }: RootStore): PropsFromState => {
  return {
    isLoading: getLoading(app.loadings, CONTAINER_ID)
  };
};

export default withRouter(connect(mapStateToProps)(NluContainer));
