import React from 'react';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import { connect } from 'react-redux';
import Cookies from 'js-cookie';

import { CK_LAST_PATH } from 'stores/app/sagas';
import { getProjectSelected } from 'stores/project/selectors';
import RootStore from 'interfaces/rootStore';

import App from './App';

interface PropsFromState {
  isSidebarCollapsed: boolean;
  isOtp: boolean;
  isAdmin: boolean;
  selectedProject: string | null;
}

interface PropsFromDispatch {}

interface Props
  extends RouteComponentProps<any>,
    PropsFromState,
    PropsFromDispatch {}

class AppContainer extends React.Component<Props> {
  extractLocation(location: string) {
    const rx = /\/project\/[A-Za-z0-9-_]+\/([A-Za-z0-9-_]+)/;
    const match = rx.exec(location);
    if (match) {
      return match[1] || 'project';
    }

    const arr = location.split('/');
    return arr.length > 1 ? arr[1] : '';
  }

  render() {
    const { location, history, isAdmin, isSidebarCollapsed } = this.props;
    const prevPath = Cookies.get(CK_LAST_PATH) || '/';
    const prevLoc = this.extractLocation(prevPath);
    const currLoc = this.extractLocation(
      location && location.pathname ? location.pathname : ''
    );
    return (
      <App
        isAdmin={isAdmin}
        currLoc={currLoc}
        prevLoc={prevLoc}
        isSidebarCollapsed={isSidebarCollapsed}
        selectedProject={this.props.selectedProject}
        goBack={() => history.push(prevPath)}
      />
    );
  }
}

const mapStateToProps = ({ auth, app, project }: RootStore): PropsFromState => {
  const isOtp = auth.token ? auth.token.isOtp : false;
  const isSidebarCollapsed = !app.sidebar.isOpen;

  return {
    isSidebarCollapsed,
    isOtp,
    isAdmin:
      auth.token && auth.token.detail && auth.token.detail.username === 'admin',
    selectedProject: getProjectSelected(project)
  };
};

const mapDispatchToProps = (dispatch: Function): PropsFromDispatch => ({});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(AppContainer)
);
