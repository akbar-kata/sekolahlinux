const moduleName = {
  analytic: 'Analytic',
  bot: 'Bot Studio',
  cms: 'CMS',
  nlstudio: 'NL Studio',
  project: 'Project'
};

export default moduleName;
