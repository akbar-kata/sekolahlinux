import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { withRouter, NavLink, RouteComponentProps } from 'react-router-dom';

import RootStore from 'interfaces/rootStore';
import PROJECT_TYPES from 'stores/project/types';
import NewProjectForm, {
  DRAWER_ID as NEW_FORM_DRAWER_ID
} from 'modules/project/NewProjectForm';

import { selectProject } from 'stores/project/actions';
import {
  getProjects,
  getProjectIndexes,
  getProjectSelected
} from 'stores/project/selectors';
import { getLoading } from 'stores/app/loadings/selectors';

import { DropdownSelector, DropdownItem } from 'components/Dropdown';
import { isDrawerOpen } from 'stores/app/drawer/selectors';
import { openDrawer } from 'stores/app/drawer/actions';
import { isRootURL } from 'utils/url';

interface PropsFromState {
  isLoading: boolean;
  index: string[];
  data: any;
  selected: string | null;
  isOpen: boolean;
}

interface PropsFromDispatch {
  selectProject: Function;
  openDrawer(): void;
}

interface Props
  extends PropsFromState,
    PropsFromDispatch,
    RouteComponentProps<any> {}

class ProjectSelectorContainer extends React.Component<Props, any> {
  handleSelect = (val: any) => {
    this.props.selectProject(val as string);
  };

  render() {
    const { selected, index, data, isLoading } = this.props;
    return (
      <Fragment>
        <DropdownSelector
          value={selected && data[selected] ? data[selected].name : ''}
          onSelect={this.handleSelect}
          loading={isLoading}
          block
        >
          {index && index.length
            ? index.slice(0, 4).map((projectId, projectIndex) => (
                <DropdownItem
                  key={`${projectId}_${projectIndex}`}
                  value={projectId}
                >
                  {data[projectId].name}
                </DropdownItem>
              ))
            : null}
          <DropdownItem divider />
          <DropdownItem>
            <NavLink to="/project" activeClassName="">
              See all projects
            </NavLink>
          </DropdownItem>
          <DropdownItem onClick={this.props.openDrawer}>
            Create new project
          </DropdownItem>
        </DropdownSelector>
        {!isRootURL(this.props.location.pathname) && <NewProjectForm />}
      </Fragment>
    );
  }
}

const mapStateToProps = ({ project, app }: RootStore): PropsFromState => {
  return {
    isLoading: getLoading(app.loadings, PROJECT_TYPES.FETCH_PROJECT_REQUEST),
    index: getProjectIndexes(project),
    data: getProjects(project),
    selected: getProjectSelected(project),
    isOpen: isDrawerOpen(app.drawer, NEW_FORM_DRAWER_ID)
  };
};

const mapDispatchToProps: PropsFromDispatch = {
  selectProject: (id: string) => selectProject(id),
  openDrawer: () => openDrawer(NEW_FORM_DRAWER_ID)
};

export default withRouter<any>(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(ProjectSelectorContainer)
);
