import React from 'react';

const CMSSelector: React.SFC = () => (
  <h2 className="kata-sidebar-container-title--cms">CMS Studio</h2>
);

export default CMSSelector;
