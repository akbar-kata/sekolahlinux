import React from 'react';
import GoogleAnalytics from 'react-ga';

import * as env from 'utils/env';

export const GA_TRACKING_CODE = env.getRuntimeEnv(
  'REACT_APP_RUNTIME_GA_TRACKING_CODE',
  env.defaultEnvs['REACT_APP_RUNTIME_GA_TRACKING_CODE']
);

const options = {
  testMode: process.env.NODE_ENV !== 'production'
};

GoogleAnalytics.initialize(GA_TRACKING_CODE, options);

const Analytics = WrappedComponent => {
  const trackPage = page => {
    GoogleAnalytics.set({ page });
    GoogleAnalytics.pageview(page);
  };

  const HOC = props => {
    const page = props.location.pathname;
    trackPage(page);

    return <WrappedComponent {...props} />;
  };

  return HOC;
};

export default Analytics;
