import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { withRouter, RouteComponentProps } from 'react-router-dom';

import { getBotData, getBotIndex, getBotSelected } from 'stores/bot/selectors';
import { fetchBotRequest } from 'stores/bot/actions';

import { isRootURL } from 'utils/url';

interface PropsFromState {
  index: string[];
  data: any;
  selected: string | null;
}

interface PropsFromDispatch {
  fetchBotRequest: Function;
}

interface Props
  extends PropsFromState,
    PropsFromDispatch,
    RouteComponentProps<any> {
  children: any;
}

class BotContainer extends React.Component<Props> {
  componentDidMount() {
    const currentLocation = this.props.history.location.pathname;
    this.props.fetchBotRequest({ withSelect: !isRootURL(currentLocation) });
  }

  render() {
    return <Fragment>{this.props.children}</Fragment>;
  }
}

const mapStateToProps = ({ bot }: any): PropsFromState => {
  return {
    index: getBotIndex(bot),
    data: getBotData(bot),
    selected: getBotSelected(bot)
  };
};

const mapDispatchToProps = (dispatch: Function): PropsFromDispatch => {
  return {
    fetchBotRequest: (args: any) => dispatch(fetchBotRequest(args))
  };
};

export default withRouter<any>(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(BotContainer)
);
