import React from 'react';
import { Dispatch } from 'redux';
import { connect } from 'react-redux';
import { withRouter, RouteComponentProps } from 'react-router-dom';

import RootStore from 'interfaces/rootStore';
import { fetchBotRequest } from 'stores/bot/actions';
import { getBotData, getBotIndex, getBotSelected } from 'stores/bot/selectors';

import Maintenance from 'modules/analytic/Maintenance';

interface PropsFromState {
  index: string[];
  data: any;
  selected: string | null;
}

interface PropsFromDispatch {
  fetchBotRequest: Function;
}

interface Props
  extends PropsFromState,
    PropsFromDispatch,
    RouteComponentProps<any> {}

class AnalyticsContainer extends React.Component<Props> {
  render() {
    return <Maintenance />;
  }
}

const mapStateToProps = ({ bot }: RootStore): PropsFromState => {
  return {
    index: getBotIndex(bot),
    data: getBotData(bot),
    selected: getBotSelected(bot)
  };
};

const mapDispatchToProps = (dispatch: Dispatch): PropsFromDispatch => {
  return {
    fetchBotRequest: (args: any) => dispatch(fetchBotRequest(args))
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(AnalyticsContainer)
);
