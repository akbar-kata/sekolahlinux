import React, { Component, Fragment } from 'react';
import classnames from 'classnames';

import ProfilesSelector from './ProfilesSelector';
import RootSelector from './RootSelector';
import InheritSelector from './InheritSelector';

import { JsonObject } from 'interfaces';

import {
  Drawer,
  DrawerBody,
  DrawerHeader,
  DrawerFooter
} from 'components/Drawer';
import { Button, SupportButton } from 'components/Button';

import { Formik, Form, FieldArray } from 'formik';
import * as yup from 'yup';
import {
  FormError,
  Field,
  ReactSelect,
  ReactSelectAsync,
  ReactTagsInput
} from 'components/FormikWrapper';
import debounce from 'debounce-promise';

interface Props {
  isOpen: boolean;
  type: string;
  id?: string;
  data?: JsonObject;
  entityIds: string[];
  isSubmitting: boolean;
  onCancel: Function;
  isExist: Function;
  getDetailInherit: Function;
  onCloseDrawer(): void;
  onSubmit(data: any);
}

interface States {}

const inputId = 'form-input-entity';

const entityType = [
  {
    label: 'Trait',
    value: 'trait'
  },
  {
    label: 'Dictionary',
    value: 'dict'
  },
  {
    label: 'Phrase',
    value: 'phrase'
  }
];

class EntityForm extends Component<Props, States> {
  validationSchema = yup.lazy((values: any) => {
    return yup.object().shape({
      name: yup
        .string()
        .required('Name is required')
        .max(20, 'Entity name must not be longer than 20 characters')
        .matches(/^[A-Za-z][-A-Za-z0-9_-]*$/, {
          message: 'Name only allowed charaters alphanumeric, _ and -'
        })
        .test(
          'isNameExists',
          'Name already in use, pick another name',
          value => !this.props.isExist(value)
        ),
      type: yup.string().required('Type must be selected'),
      profile: yup.string().required('Profile must be selected'),
      labels:
        values.type === 'trait' && (!values.labels || !values.labels.length)
          ? yup.string().required('Labels is required')
          : yup.mixed(),
      dictionary:
        values.type === 'dict'
          ? yup
              .array()
              .required('Dictionary is required')
              .of(
                yup.object().shape({
                  key: yup.string().required('Key is required'),
                  words: yup.string().required('Words is required')
                })
              )
          : yup.mixed()
    });
  });

  componentDidMount() {
    const el = document.getElementById(inputId);
    if (el && el.focus) {
      el.focus();
    }
  }

  composeBelongsOpts = () => {
    const { entityIds } = this.props;
    return entityIds && entityIds.length
      ? entityIds.map(id => ({
          label: id,
          value: id
        }))
      : [];
  };

  onClose = () => {
    this.props.onCancel();
  };

  innerForm = formApi => (
    <Form className="kata-form full-size">
      <DrawerHeader title="Create Entity" />
      <DrawerBody>
        <div className="kata-form__element">
          <label className="kata-form__label">Name</label>
          <Field
            id={inputId}
            name="name"
            placeholder="Name"
            className="form-control kata-form__input-text"
          />
        </div>
        <div className="kata-form__element">
          <label className="kata-form__label">Inherit</label>
          <InheritSelector>
            {params => {
              return params.loading ? (
                <div>Loading...</div>
              ) : params.error ? (
                <div>{params.error}</div>
              ) : (
                <ReactSelectAsync
                  name="inherit"
                  options={params.options}
                  loadOptions={debounce(params.search, 500, {
                    leading: true
                  })}
                  simpleValue
                  onChange={val => {
                    if (val) {
                      const detail = this.props.getDetailInherit(val);
                      if (detail) {
                        formApi.setFieldValue('type', detail.type);
                        formApi.setFieldValue('profile', detail.profile);
                        formApi.setFieldValue('inherit', detail.inherit);
                        formApi.setFieldValue('root', detail.root);
                        formApi.setFieldValue('belongsTo', detail.belongsTo);
                        formApi.setFieldValue('labels', detail.labels);
                        // set dictionary value
                        if (detail.dictionary) {
                          const dictionary = Object.keys(detail.dictionary);
                          Object.values(detail.dictionary).map((dict, key) => {
                            formApi.setFieldValue(
                              `dictionary.${key}.key`,
                              dictionary[key]
                            );
                            formApi.setFieldValue(
                              `dictionary.${key}.words`,
                              dict
                            );
                          });
                        }
                      }
                    } else {
                      formApi.setFieldValue('type', '');
                      formApi.setFieldValue('profile', '');
                      formApi.setFieldValue('inherit', '');
                      formApi.setFieldValue('root', '');
                      formApi.setFieldValue('belongsTo', '');
                      formApi.setFieldValue('labels', '');
                      formApi.setFieldValue('dictionary', '');
                    }
                  }}
                />
              );
            }}
          </InheritSelector>
        </div>
        <div className="kata-form__element">
          <label className="kata-form__label">Type</label>
          <ReactSelect
            name="type"
            simpleValue
            options={entityType}
            onChange={() => {
              formApi.setFieldValue('dictionary', undefined);
              formApi.setFieldValue('profile', 'default');
            }}
            disabled={formApi.values.inherit ? true : false}
          />
        </div>
        <div className="kata-form__element">
          <label className="kata-form__label">Profile</label>
          <ProfilesSelector>
            {(params, generate) => {
              return params.loading ? (
                <div>Loading...</div>
              ) : params.error ? (
                <div>{params.error}</div>
              ) : (
                <ReactSelect
                  name="profile"
                  clearable
                  simpleValue
                  options={generate(formApi.values.type)}
                  disabled={formApi.values.inherit ? true : false}
                />
              );
            }}
          </ProfilesSelector>
        </div>
        <div className="kata-form__element">
          <label className="kata-form__label">Root</label>
          <RootSelector>
            {params => {
              return params.loading ? (
                <div>Loading...</div>
              ) : params.error ? (
                <div>{params.error}</div>
              ) : (
                <ReactSelectAsync
                  options={params.getOptions(formApi.values.type)}
                  loadOptions={debounce(params.search, 500, {
                    leading: true
                  })}
                  name="root"
                  simpleValue
                  disabled={formApi.values.inherit ? true : false}
                />
              );
            }}
          </RootSelector>
        </div>

        {formApi.values.type !== 'trait' && (
          <div className="kata-form__element">
            <label className="kata-form__label">Belongs to</label>
            <ReactSelect
              name="belongsTo"
              simpleValue
              options={this.composeBelongsOpts()}
              disabled={formApi.values.inherit ? true : false}
            />
          </div>
        )}
        {formApi.values.type !== 'dict' && (
          <div className="kata-form__element">
            <label className="kata-form__label">Labels</label>
            <ReactTagsInput
              name="labels"
              className={`form-control kata-form__input-textarea ${
                formApi.values.inherit
                  ? 'kata-form__input-textarea disabled'
                  : ''
              }`}
              inputProps={
                formApi.values.inherit
                  ? {
                      placeholder: ''
                    }
                  : {
                      placeholder: 'Add a word'
                    }
              }
              disabled={formApi.values.inherit ? true : false}
            />
          </div>
        )}

        {formApi.values.type === 'dict' && (
          <Fragment>
            <label className="kata-form__label">Dictionary</label>
            <FormError name="dictionary" />
            <FieldArray
              name="dictionary"
              render={arrayHelpers => (
                <div>
                  {formApi.values.dictionary && formApi.values.dictionary.length
                    ? formApi.values.dictionary.map((dict, i) => (
                        <div
                          key={i}
                          className="kata-entity__create--dictionary-wrapper"
                        >
                          <div className="kata-entity__create--dictionary-wrapper-content">
                            <div className="kata-form__element">
                              <Field
                                name={`dictionary.${i}.key`}
                                placeholder="Key"
                                className="form-control kata-form__input-text"
                                disabled={formApi.values.inherit ? true : false}
                              />
                            </div>
                            <div className="kata-form__element">
                              <ReactTagsInput
                                name={`dictionary.${i}.words`}
                                className={classnames(
                                  'form-control',
                                  'kata-form__input-textarea',
                                  formApi.values.inherit &&
                                    'kata-form__input-select--disabled'
                                )}
                                inputProps={
                                  formApi.values.inherit
                                    ? {
                                        placeholder: ''
                                      }
                                    : {
                                        placeholder: 'Add a word'
                                      }
                                }
                                disabled={formApi.values.inherit ? true : false}
                              />
                            </div>
                          </div>
                          <div className="kata-entity__create--dictionary-wrapper-footer">
                            <Button
                              color="danger"
                              size="sm"
                              onClick={() => arrayHelpers.remove(i)}
                            >
                              Delete
                            </Button>
                          </div>
                        </div>
                      ))
                    : null}
                  {!formApi.values.inherit ? (
                    <SupportButton
                      className="d-block mt-1"
                      onClick={() => arrayHelpers.push({})}
                    >
                      Add dictionary
                    </SupportButton>
                  ) : null}
                </div>
              )}
            />
          </Fragment>
        )}
      </DrawerBody>
      <DrawerFooter>
        <Button
          color="primary"
          className="mr-1"
          type="submit"
          loading={this.props.isSubmitting}
        >
          <i
            className={`mr2 icon-${
              this.props.type === 'create' ? 'plus' : 'check'
            }`}
          />{' '}
          {this.props.type}
        </Button>
        <Button onClick={this.onClose}>Cancel</Button>
      </DrawerFooter>
    </Form>
  );

  render() {
    const { data } = this.props;
    const defaultVal = Object.assign(
      { type: 'trait', profile: 'default' },
      data
    );
    return (
      <Drawer isOpen={this.props.isOpen} onClose={this.props.onCloseDrawer}>
        <Formik
          onSubmit={this.props.onSubmit}
          initialValues={defaultVal}
          validationSchema={this.validationSchema}
        >
          {this.innerForm}
        </Formik>
      </Drawer>
    );
  }
}

export default EntityForm;
