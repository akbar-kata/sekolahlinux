import React, { Fragment } from 'react';

import { DashboardCards } from 'components/Dashboard';
import { Robot } from 'components/Loading';
import { confirm } from 'components/Modal';
import { Entity } from 'interfaces/nlu/entity';
import { DataMap } from 'interfaces/common';

import EntityCreate from './EntityCreate.Container';
import EntityUpdate from './EntityUpdate.Container';
import EntityCard from './EntityCard';

interface Props {
  isLoading: boolean;
  error: string | null;
  index?: string[];
  data?: DataMap<Entity>;
  onUpdate: Function;
  onRemove: Function;
}

interface States {
  hover?: string;
}

// TODO: Split EntityCard to separate component, maintain hover state
class EntityList extends React.Component<Props, States> {
  state = {
    hover: undefined
  };

  public onUpdate = (data: any) => {
    return () => this.props.onUpdate(data);
  };

  public onRemove = (name: string) => {
    return () => {
      confirm({
        title: 'Delete Entity',
        message: `Are you sure you want to delete this entity?`,
        okLabel: 'Delete',
        cancelLabel: 'Cancel'
      }).then(res => {
        if (res) {
          this.props.onRemove(name);
        }
      });
    };
  };

  renderMessage(child: any) {
    return <div>{child}</div>;
  }

  renderLoading() {
    return this.renderMessage(
      <h5 className="text-center py-5">
        <Robot />
      </h5>
    );
  }

  renderError() {
    return this.renderMessage(
      <h5 className="pa4 text-danger text-bold">{this.props.error}</h5>
    );
  }

  enterBelongsTo = (hover: string) => () => {
    this.setState({ hover });
  };

  leaveBelongsTo = () => {
    this.setState({ hover: undefined });
  };

  renderData() {
    const { index, data } = this.props;
    return (
      <DashboardCards>
        <EntityCreate />
        {index &&
          !!index.length &&
          data &&
          index.map(key => {
            return (
              <EntityCard
                key={key}
                isHover={key === this.state.hover}
                title={key}
                entity={data[key]}
                onUpdate={this.onUpdate(data[key])}
                onRemove={this.onRemove(key)}
                enterBelongsTo={this.enterBelongsTo(data[key].belongsTo)}
                leaveBelongsTo={this.leaveBelongsTo}
              />
            );
          })}
      </DashboardCards>
    );
  }

  render() {
    const { isLoading, error } = this.props;
    return (
      <Fragment>
        <EntityUpdate />
        {!isLoading
          ? error !== null
            ? this.renderError()
            : this.renderData()
          : this.renderLoading()}
      </Fragment>
    );
  }
}

export default EntityList;
