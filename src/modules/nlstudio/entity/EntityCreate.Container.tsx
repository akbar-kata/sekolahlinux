import React from 'react';
import { connect } from 'react-redux';

import Card from 'components/Card';
import CardButton from 'components/Card/CardButton';

import RootStore from 'interfaces/rootStore';

import { getCurrentNlu } from 'stores/nlu/nlu/selectors';
import { createEntityRequest } from 'stores/nlu/entity/actions';
import {
  getEntityIndexCustomType,
  getDetailEntityInherit,
  isExist
} from 'stores/nlu/entity/selectors';
import { openDrawer, closeDrawer } from 'stores/app/drawer/actions';
import { isDrawerOpen } from 'stores/app/drawer/selectors';
import { getProjectSelected } from 'stores/project/selectors';
import { getLoading } from 'stores/app/loadings/selectors';
import NLU_ENTITY from 'stores/nlu/entity/types';

import EntityForm from './EntityForm';

interface PropsFromState {
  error: string;
  currentNlu: string | null;
  isOpen: boolean;
  isSubmitting: boolean;
  selectedProject: string | null;
  entityIds: string[];
  isExist: Function;
  getDetailInherit: Function;
}

interface PropsFromDispatch {
  createAction: typeof createEntityRequest;
  openDrawer: () => void;
  closeDrawer: () => void;
}

interface Props extends PropsFromState, PropsFromDispatch {}

interface State {}

const drawerId = 'EntityCreate';

class EntityCreate extends React.Component<Props, State> {
  onCreate = (data: any) => {
    if (this.props.selectedProject && this.props.currentNlu) {
      this.props.createAction(
        this.props.selectedProject,
        this.props.currentNlu,
        data
      );
    }
  };

  render() {
    return (
      <>
        <Card
          asButton
          className="kata-entity__add-btn"
          onClick={this.props.openDrawer}
        >
          <CardButton label={'Create Entity'} icon="add" />
        </Card>
        <EntityForm
          type="Create"
          entityIds={this.props.entityIds}
          isOpen={this.props.isOpen}
          onCancel={this.props.closeDrawer}
          onSubmit={this.onCreate}
          isExist={this.props.isExist}
          getDetailInherit={this.props.getDetailInherit}
          isSubmitting={this.props.isSubmitting}
          onCloseDrawer={this.props.closeDrawer}
        />
      </>
    );
  }
}
const mapStateToProps = ({
  app,
  nlu: { entity, nlu },
  project,
  app: { drawer }
}: RootStore): PropsFromState => {
  const currentNlu = getCurrentNlu(nlu);

  return {
    isOpen: isDrawerOpen(drawer, drawerId),
    currentNlu: currentNlu ? currentNlu.id : null,
    error: '',
    isSubmitting: getLoading(app.loadings, NLU_ENTITY.CREATE_REQUEST),
    selectedProject: getProjectSelected(project),
    entityIds: getEntityIndexCustomType(entity, ['dict', 'phrase']),
    isExist: (name: string) => isExist(entity, name),
    getDetailInherit: (name: string) => getDetailEntityInherit(entity, name)
  };
};

const mapDispatchToProps = (dispatch: Function): PropsFromDispatch => {
  return {
    createAction: (project: string, nlu: string, data: any) =>
      dispatch(createEntityRequest(project, nlu, data)),
    openDrawer: () => dispatch(openDrawer(drawerId)),
    closeDrawer: () => dispatch(closeDrawer(drawerId))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EntityCreate);
