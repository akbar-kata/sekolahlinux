import React, { PureComponent, Fragment } from 'react';

import DetailLoader from './DetailLoader';
import {
  Drawer,
  DrawerBody,
  DrawerHeader,
  DrawerFooter
} from 'components/Drawer';

import { Button, SupportButton } from 'components/Button';
import { Circle } from 'components/Loading';
import EntityInfo from './EntityInfo';

import { Formik, Form, FieldArray } from 'formik';
import * as yup from 'yup';
import { FormError, Field, ReactTagsInput } from 'components/FormikWrapper';

interface Props {
  isOpen: boolean;
  name: string;
  isSubmitting: boolean;
  onCancel: Function;
  onSubmit(data: any);
}

interface States {}

class EntityForm extends PureComponent<Props, States> {
  validationSchema = yup.lazy((values: any) => {
    return yup.object().shape({
      newLabels:
        values.type === 'trait' &&
        (!values.newLabels || !values.newLabels.length)
          ? yup.string().required('Labels is required')
          : yup.mixed(),
      newDictionary:
        values.type === 'dict'
          ? yup
              .array()
              .required('Dictionary is required')
              .of(
                yup.object().shape({
                  key: yup.string().required('Key is required'),
                  words: yup.string().required('Words is required')
                })
              )
          : yup.mixed()
    });
  });

  onClose = () => {
    this.props.onCancel();
  };

  renderMessage(message: React.ReactElement<any>) {
    return (
      <>
        <p>Edit Entity</p>
        <p>{message}</p>
      </>
    );
  }

  innerForm = formApi => (
    <Form className="kata-form full-size">
      <DrawerHeader
        title={`Add Entity ${
          formApi.values.type === 'dict' ? 'Dictionary' : 'Label'
        }`}
      />
      <DrawerBody>
        {formApi.values.entityInfo && (
          <EntityInfo entity={formApi.values.entityInfo} />
        )}
        <Fragment>
          {formApi.values.type === 'dict' && formApi.values.dictionary ? (
            <div className="kata-form__element">
              <label className="kata-form__label">Current dictionary</label>
              <FormError name="dictionary" />
              {formApi.values.dictionary && formApi.values.dictionary.length
                ? formApi.values.dictionary.map((dict, i) => (
                    <div
                      className="kata-form__element kata-entity__dictionary"
                      key={i}
                    >
                      <label className="kata-form__label kata-entity__dictionary-label">
                        <i className="icon-dict" /> {dict.key}
                      </label>
                      {dict.words && dict.words.length
                        ? dict.words.map((word, idx) => (
                            <Fragment key={idx}>
                              <label
                                key={idx}
                                className="badge badge-secondary kata-entity__badge"
                              >
                                {word}
                              </label>{' '}
                            </Fragment>
                          ))
                        : null}
                    </div>
                  ))
                : null}
            </div>
          ) : formApi.values.labels ? (
            <div className="kata-form__element">
              <label className="kata-form__label">Current labels</label>
              <div className="kata-entity__labels">
                {formApi.values.labels && formApi.values.labels.length
                  ? formApi.values.labels.map((label, idx) => (
                      <Fragment key={idx}>
                        <label
                          key={idx}
                          className="badge badge-secondary kata-entity__badge"
                        >
                          {label}
                        </label>{' '}
                      </Fragment>
                    ))
                  : null}
              </div>
            </div>
          ) : null}
        </Fragment>
        <Fragment>
          {(formApi.values.type === 'dict' && (
            <div>
              <div className="kata-form__element">
                <label className="kata-form__label">New dictionary</label>
              </div>
              <FormError name="newDictionary" />
              <FieldArray
                name="newDictionary"
                render={arrayHelpers => (
                  <div>
                    {formApi.values.newDictionary &&
                    formApi.values.newDictionary.length
                      ? formApi.values.newDictionary.map((dict, i) => (
                          <div
                            key={i}
                            className="kata-entity__create--dictionary-wrapper"
                          >
                            <div className="kata-entity__create--dictionary-wrapper-content">
                              <div className="kata-form__element">
                                <label className="kata-form__label">Name</label>
                                <Field
                                  name={`newDictionary.${i}.key`}
                                  placeholder="Key"
                                  className="kata-form__input-text"
                                />
                              </div>
                              <div className="kata-form__element">
                                <ReactTagsInput
                                  name={`newDictionary.${i}.words`}
                                  className="kata-form__input-textarea"
                                  inputProps={{
                                    placeholder: 'Add a word'
                                  }}
                                />
                              </div>
                            </div>
                            <div className="kata-entity__create--dictionary-wrapper-footer">
                              <Button
                                color="danger"
                                size="sm"
                                onClick={() => arrayHelpers.remove(i)}
                              >
                                Delete
                              </Button>
                            </div>
                          </div>
                        ))
                      : null}
                    <SupportButton
                      className="d-block mt-1"
                      onClick={() => arrayHelpers.push({})}
                    >
                      Add dictionary
                    </SupportButton>
                  </div>
                )}
              />
            </div>
          )) || (
            <div className="kata-form__element">
              <label className="kata-form__label">New labels</label>
              <div>
                <ReactTagsInput
                  name="newLabels"
                  className="form-control kata-form__input-textarea"
                  inputProps={{
                    placeholder: 'Add a word'
                  }}
                />
              </div>
            </div>
          )}
        </Fragment>
      </DrawerBody>
      <DrawerFooter>
        <Button
          color="primary"
          className="mr-1"
          type="submit"
          loading={this.props.isSubmitting}
        >
          Update
        </Button>
        <Button type="button" onClick={this.onClose}>
          Cancel
        </Button>
      </DrawerFooter>
    </Form>
  );

  render() {
    return (
      <Drawer isOpen={this.props.isOpen} onClose={this.onClose}>
        <DetailLoader name={this.props.name}>
          {({ loading, name, data, error }) => {
            const entityInfo = data;
            if (loading) {
              return (
                <div className="kata-form full-size">
                  <DrawerHeader title="Loading..." />
                  <DrawerBody>
                    <div className="my-3 text-center">
                      <Circle />
                    </div>
                  </DrawerBody>
                </div>
              );
            }
            return (
              <Formik
                onSubmit={this.props.onSubmit}
                initialValues={{ ...data, entityInfo, loading, name, error }}
                validationSchema={this.validationSchema}
              >
                {this.innerForm}
              </Formik>
            );
          }}
        </DetailLoader>
      </Drawer>
    );
  }
}

export default EntityForm;
