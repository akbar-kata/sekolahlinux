import React from 'react';
import { connect } from 'react-redux';
import isArray from 'lodash-es/isArray';

import RootStore from 'interfaces/rootStore';
import { getProjectSelected } from 'stores/project/selectors';
import { getCurrentNlu } from 'stores/nlu/nlu/selectors';
import { updateEntityRequest } from 'stores/nlu/entity/actions';
import { closeDrawer } from 'stores/app/drawer/actions';
import { isDrawerOpen, getDrawerData } from 'stores/app/drawer/selectors';
import { getLoading } from 'stores/app/loadings/selectors';
import NLU_ENTITY from 'stores/nlu/entity/types';

import EntityUpdate from './EntityUpdate';

interface PropsFromState {
  error: string | null;
  currentNlu: string | null;
  isUpdating: boolean;
  isOpen: boolean;
  selectedProject: string | null;
  dataToUpdate: any;
}

interface PropsFromDispatch {
  updateAction: (
    project: string,
    nlu: string,
    name: string,
    data: any
  ) => Promise<any>;
  closeDrawer: () => void;
}

interface Props extends PropsFromState, PropsFromDispatch {}

interface State {}

const drawerId = 'EntityUpdate';

class EntityUpdateContainer extends React.Component<Props, State> {
  onUpdate = ({
    type,
    newLabels,
    newDictionary,
    labels,
    dictionary,
    ...rest
  }) => {
    const data: any = { type, ...rest };

    if (type === 'dict') {
      const arr = isArray(dictionary) ? dictionary : [];
      data.dictionary = arr.concat(isArray(newDictionary) ? newDictionary : []);
    } else {
      const arr = isArray(labels) ? labels : [];
      data.labels = arr.concat(isArray(newLabels) ? newLabels : []);
    }
    if (this.props.selectedProject && this.props.currentNlu) {
      this.props.updateAction(
        this.props.selectedProject,
        this.props.currentNlu,
        data.name,
        data
      );
    }
  };

  render() {
    return (
      <EntityUpdate
        isOpen={this.props.isOpen}
        name={
          this.props.dataToUpdate && this.props.dataToUpdate.name
            ? this.props.dataToUpdate.name
            : ''
        }
        isSubmitting={this.props.isUpdating}
        onSubmit={this.onUpdate}
        onCancel={this.props.closeDrawer}
      />
    );
  }
}

const mapStateToProps = ({
  app,
  nlu: { nlu },
  project,
  app: { drawer }
}: RootStore): PropsFromState => {
  const currentNlu = getCurrentNlu(nlu);

  return {
    isOpen: isDrawerOpen(drawer, drawerId),
    error: '',
    currentNlu: currentNlu ? currentNlu.id : null,
    isUpdating: getLoading(app.loadings, NLU_ENTITY.UPDATE_REQUEST),
    selectedProject: getProjectSelected(project),
    dataToUpdate: getDrawerData(drawer, drawerId)
  };
};

const mapDispatchToProps = (dispatch: Function): PropsFromDispatch => {
  return {
    updateAction: (project: string, nlu: string, name: string, data: any) =>
      dispatch(updateEntityRequest(project, nlu, name, data)),
    closeDrawer: () => dispatch(closeDrawer(drawerId))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EntityUpdateContainer);
