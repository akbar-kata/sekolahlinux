import React, { Fragment } from 'react';
import { connect } from 'react-redux';

import RootStore from 'interfaces/rootStore';
import { EntityDataMap } from 'interfaces/nlu/entity';

import { getCurrentNlu, getNluLoadings } from 'stores/nlu/nlu/selectors';
import {
  removeEntityRequest,
  updateEntityRequest,
  fetchEntityRequest
} from 'stores/nlu/entity/actions';
import { getEntityIndex, getEntityData } from 'stores/nlu/entity/selectors';
import { getProjectSelected } from 'stores/project/selectors';
import { getAuthSelected } from 'stores/user/auth/selectors';
import { openDrawer, closeDrawer } from 'stores/app/drawer/actions';
import { isDrawerOpen, getDrawerData } from 'stores/app/drawer/selectors';
import { getLoading } from 'stores/app/loadings/selectors';
import NLU_ENTITY from 'stores/nlu/entity/types';

import EntityList from './EntityList';
import { Robot } from 'components/Loading';

interface PropsFromState {
  currentUser: string;
  currentNlu: string | null;
  selectedProject: string | null;
  nluIsLoading: boolean;
  isLoading: boolean;
  index: string[];
  data: EntityDataMap;
  isUpdateOpen: boolean;
  dataToUpdate: any;
}

interface PropsFromDispatch {
  fetchEntityRequest: (
    projectId: string,
    nluId: string
  ) => ReturnType<typeof fetchEntityRequest>;
  openUpdate: Function;
  closeUpdate: Function;
  onUpdate: typeof updateEntityRequest;
  onRemove: typeof removeEntityRequest;
}

interface Props extends PropsFromState, PropsFromDispatch {}

interface State {}

const updateDrawerId = 'EntityUpdate';

class EntityListContainer extends React.Component<Props, State> {
  onUpdate = (name, data) => {
    if (this.props.selectedProject && this.props.currentNlu) {
      this.props.onUpdate(
        this.props.selectedProject,
        this.props.currentNlu,
        name,
        data
      );
    }
  };

  onRemove = name => {
    if (this.props.selectedProject && this.props.currentNlu) {
      this.props.onRemove(
        this.props.selectedProject,
        this.props.currentNlu,
        name
      );
    }
  };

  componentDidMount() {
    if (this.props.selectedProject && this.props.currentNlu) {
      this.props.fetchEntityRequest(
        this.props.selectedProject,
        this.props.currentNlu
      );
    }
  }

  renderLoading() {
    return <Robot />;
  }

  renderData() {
    return (
      <EntityList
        isLoading={false}
        error={null}
        index={this.props.index}
        data={this.props.data}
        onUpdate={this.props.openUpdate}
        onRemove={this.onRemove}
      />
    );
  }

  render() {
    const { isLoading, nluIsLoading } = this.props;

    return (
      <Fragment>
        {isLoading || nluIsLoading ? this.renderLoading() : this.renderData()}
      </Fragment>
    );
  }
}

const mapStateToProps = ({
  auth,
  nlu: { nlu, entity },
  project,
  app: { drawer, loadings }
}: RootStore): PropsFromState => {
  const token = getAuthSelected(auth);
  const currentNlu = getCurrentNlu(nlu);

  return {
    currentUser: token ? token.username : '',
    currentNlu: currentNlu ? currentNlu.id : null,
    selectedProject: getProjectSelected(project),
    nluIsLoading: getNluLoadings(nlu, 'fetch'),
    isLoading: getLoading(loadings, NLU_ENTITY.FETCH_REQUEST),
    index: getEntityIndex(entity),
    data: getEntityData(entity),
    isUpdateOpen: isDrawerOpen(drawer, updateDrawerId),
    dataToUpdate: getDrawerData(drawer, updateDrawerId)
  };
};

const mapDispatchToProps = (dispatch: Function): PropsFromDispatch => {
  return {
    fetchEntityRequest: (projectId: string, nluId: string) =>
      dispatch(fetchEntityRequest(projectId, nluId)),
    openUpdate: (data: any) => dispatch(openDrawer(updateDrawerId, data)),
    closeUpdate: () => dispatch(closeDrawer(updateDrawerId)),
    onUpdate: (project: string, nlu: string, name: string, data: any) =>
      dispatch(updateEntityRequest(project, nlu, name, data)),
    onRemove: (project: string, nlu: string, name: string) =>
      dispatch(removeEntityRequest(project, nlu, name))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EntityListContainer);
