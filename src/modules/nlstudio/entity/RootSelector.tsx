import React from 'react';
import { connect } from 'react-redux';

import RootStore from 'interfaces/rootStore';
import {
  getEntityRoot,
  getEntityError,
  getEntityLoading
} from 'stores/nlu/entity/selectors';
import { searchRootRequest } from 'stores/nlu/entity/actions';

interface PropsFromState {
  error: string | null;
  loading: boolean;
  data: any;
}

interface PropsFromDispatch {
  search: Function;
}

interface Props extends PropsFromState, PropsFromDispatch {
  children: (props: any) => React.ReactElement<any>;
}

interface State {}

class RootSelector extends React.Component<Props, State> {
  getOptions = (type: string) => {
    const { data } = this.props;
    return Object.keys(data)
      .filter(key => data[key].type === type)
      .map(key => ({ label: key, value: key }));
  };

  render() {
    const { children, ...rest } = this.props;
    return children({ ...rest, getOptions: this.getOptions });
  }
}
const mapStateToProps = ({ nlu: { entity } }: RootStore): PropsFromState => {
  const results = getEntityRoot(entity);

  return {
    loading: getEntityLoading(entity, 'root'),
    error: getEntityError(entity, 'root'),
    data: results
  };
};

const mapDispatchToProps: PropsFromDispatch = {
  search: (keyword: string) => searchRootRequest(keyword)
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RootSelector);
