import React from 'react';
import { connect } from 'react-redux';

import RootStore from 'interfaces/rootStore';
import {
  getEntityProfiles,
  getEntityError,
  getEntityLoading
} from 'stores/nlu/entity/selectors';
import { profilesEntityRequest } from 'stores/nlu/entity/actions';
import { getCurrentNlu } from 'stores/nlu/nlu/selectors';
import {
  getProjectOptions,
  getProjectSelected
} from 'stores/project/selectors';

interface PropsFromState {
  error: string | null;
  loading: boolean;
  data: any[];
  lang: string | null;
}

interface PropsFromDispatch {
  reqProfiles: Function;
}

interface Props extends PropsFromState, PropsFromDispatch {
  children: (props: any, generate: Function) => React.ReactElement<any>;
}

interface State {}

class ProfileSelector extends React.Component<Props, State> {
  componentWillMount() {
    this.props.reqProfiles(this.props.lang);
  }

  generateOpts = (type: string) => {
    return this.props.data && this.props.data.length
      ? this.props.data
          .filter(profiles => profiles.type === type)
          .map(profiles => ({
            label: `${profiles.name} - ${profiles.desc}`,
            value: profiles.name
          }))
      : [];
  };

  render() {
    const { children, ...rest } = this.props;
    return children(rest, this.generateOpts);
  }
}
const mapStateToProps = ({
  project,
  nlu: { entity, nlu }
}: RootStore): PropsFromState => {
  const currentNlu = getCurrentNlu(nlu);
  const selectedProject = getProjectSelected(project);
  const projectOptions = selectedProject
    ? getProjectOptions(project, selectedProject)
    : null;

  return {
    lang: currentNlu
      ? currentNlu.lang
      : projectOptions
      ? projectOptions.nluLang || null
      : null,
    loading: getEntityLoading(entity, 'profiles'),
    error: getEntityError(entity, 'profiles'),
    data: getEntityProfiles(entity)
  };
};

const mapDispatchToProps = (dispatch: Function): PropsFromDispatch => {
  return {
    reqProfiles: (lang: string) => dispatch(profilesEntityRequest(lang))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProfileSelector);
