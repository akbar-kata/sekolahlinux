import React from 'react';
import { connect } from 'react-redux';

import RootStore from 'interfaces/rootStore';
import { getCurrentNlu } from 'stores/nlu/nlu/selectors';
import {
  getEntityDetailUpdate,
  getEntityError,
  getEntityLoading
} from 'stores/nlu/entity/selectors';
import {
  detailEntityRequest,
  detailEntityClear
} from 'stores/nlu/entity/actions';
import { getProjectSelected } from 'stores/project/selectors';

interface PropsFromState {
  currentNlu: string | null;
  error: string | null;
  loading: boolean;
  data: any;
  selectedProject: string | null;
}

interface PropsFromDispatch {
  fetchDetail: typeof detailEntityRequest;
  clearData: Function;
}

interface OtherProps extends PropsFromState, PropsFromDispatch {
  name: string;
}

type RenderProps = PropsFromState & PropsFromDispatch & OtherProps;

interface Props extends PropsFromState, PropsFromDispatch, OtherProps {
  name: string;
  children: (api: RenderProps) => JSX.Element;
}

interface State {}

class DetailLoader extends React.Component<Props, State> {
  componentDidMount() {
    if (this.props.selectedProject && this.props.currentNlu) {
      this.props.fetchDetail(
        this.props.selectedProject,
        this.props.currentNlu,
        this.props.name
      );
    }
  }

  componentWillUnmount() {
    this.props.clearData();
  }

  render() {
    const { children, ...rest } = this.props;
    return children(rest);
  }
}
const mapStateToProps = ({
  nlu: { nlu, entity },
  project
}: RootStore): PropsFromState => {
  const currentNlu = getCurrentNlu(nlu);

  return {
    currentNlu: currentNlu ? currentNlu.id : null,
    loading: getEntityLoading(entity, 'detail'),
    error: getEntityError(entity, 'detail'),
    data: getEntityDetailUpdate(entity),
    selectedProject: getProjectSelected(project)
  };
};

const mapDispatchToProps = (dispatch: Function): PropsFromDispatch => {
  return {
    fetchDetail: (project: string, nlu: string, name: string) =>
      dispatch(detailEntityRequest(project, nlu, name)),
    clearData: () => dispatch(detailEntityClear())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DetailLoader);
