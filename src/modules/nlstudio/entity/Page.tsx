import React from 'react';

import EntityList from './EntityList.Container';
import { Dashboard } from 'components/Dashboard';
import './style.scss';

interface Props {}
interface State {
  isOpen: boolean;
}
class Page extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      isOpen: false
    };
  }

  onOpenDrawer = () => {
    this.setState({ isOpen: true });
  };

  render() {
    return (
      <Dashboard
        title={'Entities'}
        tooltip={`
        Entity is used to identify and extract specific information from user inputs.
        `}
      >
        <EntityList />
      </Dashboard>
    );
  }
}

export default Page;
