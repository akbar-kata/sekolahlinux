import React, { Fragment } from 'react';
import classnames from 'classnames';

import { Entity } from 'interfaces/nlu/entity';

import { Button } from 'components/Button';
import Card from 'components/Card';
import {
  Dropdown,
  DropdownItem,
  DropdownToggle,
  DropdownMenu
} from 'components/Dropdown';

import './EntityCard.scss';

interface EntityCardProps {
  entity: Entity;
  title: string;
  isHover?: boolean;
  onUpdate: () => void;
  onRemove: () => void;
  enterBelongsTo?: () => void;
  leaveBelongsTo?: () => void;
}

interface EntityCardState {
  currentPage: number;
}

class EntityCard extends React.Component<EntityCardProps, EntityCardState> {
  constructor(props: EntityCardProps) {
    super(props);

    this.state = {
      currentPage: 1
    };
  }

  preventBubbleUp(e: React.MouseEvent) {
    e.preventDefault();
    e.stopPropagation();
  }

  handlePageChange(page: number) {
    return (e: React.MouseEvent<HTMLButtonElement>) => {
      this.preventBubbleUp(e);
      this.setState({
        currentPage: page
      });
    };
  }

  render() {
    const {
      entity,
      isHover,
      title,
      onUpdate,
      onRemove,
      enterBelongsTo,
      leaveBelongsTo
    } = this.props;
    const { currentPage } = this.state;
    const { type, profile, belongsTo, inherit, root, labels } = entity;

    return (
      <Card
        className={classnames(
          'kata-entity__card',
          isHover && 'kata-entity__card-focus-hover'
        )}
        title={title}
        onClick={onUpdate}
        action={
          <Dropdown>
            <DropdownToggle caret={false}>
              <Button isIcon>
                <i className="icon-more" />
              </Button>
            </DropdownToggle>
            <DropdownMenu right>
              <DropdownItem onClick={onUpdate}>Manage</DropdownItem>
              <DropdownItem onClick={onRemove}>Delete</DropdownItem>
            </DropdownMenu>
          </Dropdown>
        }
      >
        <div className="kata-entity__body">
          {currentPage === 1 && (
            <Fragment>
              <div className="kata-entity__card-info">
                <div className="kata-entity__card-info--key">Profile</div>
                <div className="kata-entity__card-info--value">
                  {profile || 'default'}
                </div>
              </div>
              <div className="kata-entity__card-info">
                <div className="kata-entity__card-info--key">Root</div>
                <div className="kata-entity__card-info--value">
                  {root
                    ? root.substring(0, 14) + (root.length > 14 ? '...' : '')
                    : '-'}
                </div>
              </div>
            </Fragment>
          )}
          {currentPage === 2 && (
            <div className="kata-entity__belongsto-container">
              {type === 'trait' && (
                <div className="kata-entity__card-info">
                  <div className="kata-entity__card-info--key">Inherit</div>
                  <div className="kata-entity__card-info--value">
                    {inherit
                      ? inherit.substring(0, 14) +
                        (inherit.length > 14 ? '...' : '')
                      : '-'}
                  </div>
                </div>
              )}
              {type !== 'trait' && (
                <div className="kata-entity__belongs-to">
                  <div className="key">
                    <span className="text-muted">Belongs to</span>
                  </div>
                  <div className="value">
                    {belongsTo ? (
                      <span
                        className="kata-entity__belongs-to-type"
                        onMouseEnter={enterBelongsTo}
                        onMouseLeave={leaveBelongsTo}
                      >
                        {belongsTo}
                      </span>
                    ) : (
                      <span className="kata-entity__belongs-to-type kata-entity__belongs-to-type--none">
                        none
                      </span>
                    )}
                  </div>
                </div>
              )}
              {labels && labels.length ? (
                <div className="kata-entity__labels-container">
                  {labels.map((label, idx) => (
                    <Fragment key={idx}>
                      <label className="badge badge-secondary kata-entity__badge">
                        {label}
                      </label>{' '}
                    </Fragment>
                  ))}
                </div>
              ) : null}
            </div>
          )}
        </div>
        <div
          className="kata-entity__footer"
          onClick={e => this.preventBubbleUp(e)}
        >
          <div className="kata-entity__footer-item">
            <i className={`icon-${type ? type : 'trait'} kata-entity__icon`} />{' '}
            {type}
          </div>
          <div className="kata-entity__footer-item">
            <button
              className={classnames(
                'kata-entity__page-button',
                currentPage === 1 && 'kata-entity__page-button--active'
              )}
              onClick={this.handlePageChange(1)}
            >
              <span>1</span>
            </button>
            <button
              className={classnames(
                'kata-entity__page-button',
                currentPage === 2 && 'kata-entity__page-button--active'
              )}
              onClick={this.handlePageChange(2)}
            >
              <span>2</span>
            </button>
          </div>
        </div>
      </Card>
    );
  }
}

export default EntityCard;
