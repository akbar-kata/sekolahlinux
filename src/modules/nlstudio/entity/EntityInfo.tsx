import React from 'react';
import { Entity } from 'interfaces/nlu/entity';

import './EntityInfo.scss';
import { SupportButton } from 'components/Button';

interface EntityInfoProps {
  entity: Entity;
}

interface EntityInfoState {
  show?: boolean;
}

class EntityInfo extends React.Component<EntityInfoProps, EntityInfoState> {
  constructor(props: EntityInfoProps) {
    super(props);

    this.state = {
      show: false
    };
  }

  toggleInfo(isVisible: boolean = false) {
    return (e: React.MouseEvent) => {
      e.preventDefault();

      this.setState({
        show: isVisible
      });
    };
  }

  render() {
    const { entity } = this.props;
    const { show } = this.state;

    if (!show) {
      return (
        <div className="kata-entity-info kata-form__element">
          <label className="kata-form__label kata-entity-info__label">
            Entity Info
          </label>
          <SupportButton
            className="d-inline-block text-right"
            onClick={this.toggleInfo(true)}
          >
            Show Entity Info
          </SupportButton>
        </div>
      );
    }

    return (
      <div className="kata-entity-info kata-form__element">
        <label className="kata-form__label kata-entity-info__label">
          Entity Info
        </label>
        <div className="kata-entity-info__inner">
          <div className="kata-entity-info__content">
            <h2 className="kata-entity-info__title kata-entity-info--ellipsis heading2">
              {entity.name}
            </h2>
            <div className="kata-entity-info__type">
              <div className="key">
                <span className="text-muted">Type</span>
              </div>
              <div className="value">
                <i className={`icon-${entity.type} text-primary`} />{' '}
                <span>{entity.type}</span>
              </div>
            </div>
            <div className="kata-entity-info__card-info">
              <div className="kata-entity-info__card-info--key">Profile</div>
              <div className="kata-entity-info__card-info--value">
                {entity.profile || '-'}
              </div>
            </div>
            <div className="kata-entity-info__card-info">
              <div className="kata-entity-info__card-info--key">Root</div>
              <div
                className="kata-entity-info__card-info--value"
                title={entity.root || undefined}
              >
                {entity.root
                  ? entity.root.substring(0, 19) +
                    (entity.root.length > 19 ? '...' : '')
                  : '-'}
              </div>
            </div>
            <div className="kata-entity-info__card-info">
              <div className="kata-entity-info__card-info--key">Inherit</div>
              <div
                className="kata-entity-info__card-info--value"
                title={entity.inherit || undefined}
              >
                {entity.inherit
                  ? entity.inherit.substring(0, 19) +
                    (entity.inherit.length > 19 ? '...' : '')
                  : '-'}
              </div>
            </div>
            {entity.type !== 'trait' && (
              <div className="kata-entity__belongs-to">
                <div className="key">
                  <span className="text-muted">Belongs to</span>
                </div>
                <div className="value">
                  {entity.belongsTo ? (
                    <span className="kata-entity__belongs-to-type">
                      {entity.belongsTo}
                    </span>
                  ) : (
                    <span className="kata-entity__belongs-to-type kata-entity__belongs-to-type--none">
                      none
                    </span>
                  )}
                </div>
              </div>
            )}
          </div>
          <div className="kata-entity-info__footer text-right">
            <SupportButton
              className="d-inline-block"
              onClick={this.toggleInfo(false)}
            >
              Hide Entity Info
            </SupportButton>
          </div>
        </div>
      </div>
    );
  }
}

export default EntityInfo;
