import React from 'react';
import { connect } from 'react-redux';

import RootStore from 'interfaces/rootStore';
import {
  getEntityError,
  getEntityLoading,
  getInheritRoot
} from 'stores/nlu/entity/selectors';
import { searchEntitiesRequest } from 'stores/nlu/entity/actions';

interface PropsFromState {
  data: any[];
  error: string | null;
  loading: boolean;
  options: any[];
}

interface PropsFromDispatch {
  search: Function;
}

interface Props extends PropsFromState, PropsFromDispatch {
  children: (props: any) => React.ReactElement<any>;
}

interface State {}

class InheritSelector extends React.Component<Props, State> {
  render() {
    const { children, ...rest } = this.props;
    return children(rest);
  }
}
const mapStateToProps = ({ nlu: { entity } }: RootStore): PropsFromState => {
  const data = getInheritRoot(entity);
  return {
    data,
    loading: getEntityLoading(entity, 'inherit'),
    error: getEntityError(entity, 'inherit'),
    options:
      data && data.length ? data.map(idx => ({ label: idx, value: idx })) : []
  };
};

const mapDispatchToProps: PropsFromDispatch = {
  search: (keyword: string) => searchEntitiesRequest(keyword)
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(InheritSelector);
