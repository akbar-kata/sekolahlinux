import React from 'react';

import { Button } from 'components/Button';

import './Snapshot.scss';

interface Props {
  isLoading: boolean;
  action: Function;
}

const Snapshot: React.SFC<Props> = props => {
  return (
    <div className="kata-snapshot">
      <Button
        color="primary"
        loading={props.isLoading}
        onClick={() => props.action()}
      >
        Publish
      </Button>
    </div>
  );
};

export default Snapshot;
