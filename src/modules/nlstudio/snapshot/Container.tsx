import React from 'react';
import { connect } from 'react-redux';
import { withRouter, RouteComponentProps } from 'react-router-dom';

import RootStore from 'interfaces/rootStore';
import { isRootURL } from 'utils/url';
import { snapshotNluRequest } from 'stores/nlu/nlu/actions';
import { getNluLoadings, getCurrentNlu } from 'stores/nlu/nlu/selectors';

import Snapshot from './Snapshot';
import { getProjectSelected } from 'stores/project/selectors';

interface PropsFromState {
  selectedNlu: string | null;
  selectedProject: string | null;
  isLoading: boolean;
}

interface PropsFromDispatch {
  action: typeof snapshotNluRequest;
}

interface Props
  extends PropsFromState,
    PropsFromDispatch,
    RouteComponentProps<any> {}

interface State {}

class SnapshotContainer extends React.Component<Props, State> {
  action = () => {
    if (this.props.selectedProject && this.props.selectedNlu) {
      this.props.action(this.props.selectedProject, this.props.selectedNlu);
    }
  };

  render() {
    return (
      !isRootURL(this.props.location.pathname) && (
        <Snapshot isLoading={this.props.isLoading} action={this.action} />
      )
    );
  }
}

const mapStateToProps = ({
  nlu: { nlu },
  project
}: RootStore): PropsFromState => {
  const currentNlu = getCurrentNlu(nlu);

  return {
    selectedNlu: currentNlu ? currentNlu.id : null,
    selectedProject: getProjectSelected(project),
    isLoading: getNluLoadings(nlu, 'snapshot')
  };
};

const mapDispatchToProps = (dispatch: Function): PropsFromDispatch => {
  return {
    action: (project: string, name: string) =>
      dispatch(snapshotNluRequest(project, name))
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(SnapshotContainer)
);
