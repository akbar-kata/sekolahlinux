import * as React from 'react';
import { connect } from 'react-redux';
import { withRouter, RouteComponentProps } from 'react-router';

import RootStore from 'interfaces/rootStore';
import { Nlu } from 'interfaces/nlu';

import CMS from 'stores/cms/types';
import { getLoading } from 'stores/app/loadings/selectors';
import { getCurrentNlu } from 'stores/nlu/nlu/selectors';
import { reIssueToken } from 'stores/nlu/nlu/actions';
import { addNotification } from 'stores/app/notification/actions';
import { getProjectSelected } from 'stores/project/selectors';

import Robot from 'components/Loading/Robot';
import Board from 'components/Board/Board';
import Dashboard from 'components/Dashboard/Dashboard';

import ManageNlu from './ManageNLU';

interface PropsFromStore {
  isLoading: boolean;
  selectedProject: string | null;
  currentNlu: Nlu | null;
}

interface PropsFromDispatch {
  doReIssueToken: typeof reIssueToken;
  notify: typeof addNotification;
}

interface OwnProps extends RouteComponentProps<{}> {}

interface Props extends PropsFromStore, PropsFromDispatch, OwnProps {}

interface States {}

class Page extends React.Component<Props, States> {
  constructor(props: Props) {
    super(props);

    this.copyToClipboard = this.copyToClipboard.bind(this);
  }

  copyToClipboard(id: string) {
    const ref = document.getElementById(id) as HTMLInputElement;

    let messageType = '';
    if (id === 'token') {
      messageType = 'Token';
    } else {
      messageType = 'Model ID';
    }

    if (ref) {
      // Temporarily enable text input so we can select it.
      ref.removeAttribute('disabled');
      ref.select();
      const successful = document.execCommand('copy');
      ref.setAttribute('disabled', 'disabled');

      if (successful) {
        this.props.notify({
          title: 'Success',
          message: `${messageType} successfully copied to clipboard.`,
          status: 'success',
          dismissible: true,
          dismissAfter: 5000
        });
      } else {
        throw Error('Unable to copy.');
      }
    }
  }

  redirect = (url: any) => {
    this.props.history.push(url);
  };

  render() {
    if (this.props.isLoading) {
      return <Robot />;
    }

    return (
      <Dashboard title="Settings" tooltip="Manage your NLU." isSettings>
        <Board>
          <ManageNlu
            manageEntities={this.redirect}
            reIssueToken={this.props.doReIssueToken}
            nluData={this.props.currentNlu}
            project={this.props.selectedProject}
            copy={this.copyToClipboard}
          />
        </Board>
      </Dashboard>
    );
  }
}

const mapStateToProps = ({ nlu: { nlu }, app, project }: RootStore) => ({
  isLoading: getLoading(app.loadings, CMS.FETCH_REQUEST),
  selectedProject: getProjectSelected(project),
  currentNlu: getCurrentNlu(nlu)
});

const mapDispatchToProps = (dispatch: Function): PropsFromDispatch => {
  return {
    doReIssueToken: (project: string, nluId: string) =>
      dispatch(reIssueToken(project, nluId)),
    notify: (opts: any) => dispatch(addNotification(opts))
  };
};

export default withRouter(
  connect<PropsFromStore, PropsFromDispatch, OwnProps, RootStore>(
    mapStateToProps,
    mapDispatchToProps
  )(Page)
);
