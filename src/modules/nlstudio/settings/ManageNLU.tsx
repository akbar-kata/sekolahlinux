import React, { Fragment } from 'react';

import { FloatingButton, SupportButton } from 'components/Button';
import { Nlu } from 'interfaces/nlu';
import { reIssueToken } from 'stores/nlu/nlu/actions';

interface Props {
  nluData: Nlu | null;
  project: string | null;
  manageEntities: (url: any) => void;
  reIssueToken: typeof reIssueToken;
  copy: (id: string) => void;
}

const renderLang = (lang: string) => {
  switch (lang) {
    case 'ID':
      return 'Bahasa Indonesia';
    case 'EN':
      return 'English';
    default:
      return 'Bahasa Indonesia';
  }
};

const ManageNlu: React.SFC<Props> = props => {
  const { nluData, project } = props;
  return (
    <Fragment>
      <div className="kata-info__container">
        <label className="kata-info__label">Model Name</label>
        <input
          className="kata-form__input-text"
          disabled
          value={nluData ? nluData.name : '...'}
        />
      </div>

      <div className="kata-info__container">
        <label className="kata-info__label">Model ID</label>
        <div className="d-flex flex-direction-row">
          <input
            id="model"
            className="kata-form__input-text d-inline"
            disabled
            value={nluData ? nluData.id : '...'}
          />
          <FloatingButton
            icon="copy"
            className="ml-2"
            disabled={!nluData}
            onClick={() => props.copy('model')}
          />
        </div>
      </div>

      <div className="kata-info__container">
        <div className="kata-info__label">Language</div>
        <input
          className="kata-form__input-text"
          disabled
          value={nluData ? renderLang(nluData.lang) : '...'}
        />
      </div>

      <div className="kata-info__container">
        <div className="kata-info__label">Entities</div>
        <div className="kata-info__content">
          {nluData && nluData.entities ? (
            <>
              {Object.keys(nluData.entities).map((label, idx) => (
                <React.Fragment key={idx}>
                  <label
                    key={idx}
                    className="badge badge-secondary kata-nlu-settings__entity-badge"
                  >
                    {label}
                  </label>{' '}
                </React.Fragment>
              ))}
            </>
          ) : (
            <label className="badge badge-secondary kata-nlu-settings__entity-badge">
              ...
            </label>
          )}
        </div>
        <SupportButton
          disabled={!nluData}
          onClick={nluData ? () => props.manageEntities(nluData.name) : null}
        >
          Manage Entities
        </SupportButton>
      </div>

      <div className="kata-info__container">
        <div className="kata-info__label">Token</div>
        <div className="d-flex flex-direction-row">
          <input
            id="token"
            className="kata-form__input-text"
            disabled
            value={nluData && nluData.token ? nluData.token : 'No Token'}
          />
          <FloatingButton
            icon="copy"
            disabled={!nluData || !nluData.token}
            className="ml-2"
            onClick={() => props.copy('token')}
          />
        </div>
        <SupportButton
          className="d-block mt-1"
          disabled={!nluData}
          onClick={
            nluData && project
              ? () => props.reIssueToken(project, nluData.id)
              : null
          }
        >
          Re-issue Token
        </SupportButton>
      </div>
    </Fragment>
  );
};

export default ManageNlu;
