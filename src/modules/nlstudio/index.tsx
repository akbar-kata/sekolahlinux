import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

import SelectedUrl from 'containers/SelectedUrl';
import { Robot } from 'components/Loading';

import TestingContainer from './testing/Testing.Container';
import Snapshot from './snapshot/Container';

const Entity = React.lazy(() => import('./entity'));
const Log = React.lazy(() => import('./log'));
const Training = React.lazy(() => import('./training'));
const Versions = React.lazy(() => import('./versions'));
const Settings = React.lazy(() => import('./settings'));

export default () => {
  return (
    <SelectedUrl>
      {projectId => {
        const parentPath = `/project/${projectId}/nlu`;

        return (
          <React.Suspense fallback={<Robot />}>
            <Switch>
              <Route path={`${parentPath}/version`} component={Versions} />
              <Route path={`${parentPath}/entity`} component={Entity} />
              <Route path={`${parentPath}/training`} component={Training} />
              <Route path={`${parentPath}/log`} component={Log} />
              <Route path={`${parentPath}/settings`} component={Settings} />
              <Route render={() => <Redirect to={`${parentPath}/entity`} />} />
            </Switch>
            <TestingContainer />
            <Snapshot />
          </React.Suspense>
        );
      }}
    </SelectedUrl>
  );
};
