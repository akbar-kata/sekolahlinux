import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { withRouter, RouteComponentProps } from 'react-router';

import { Button } from 'components/Button';
import { isRootURL } from 'utils/url';
import RootStore from 'interfaces/rootStore';
import { getCurrentNlu } from 'stores/nlu/nlu/selectors';
import { fetchTestingRequest, clearTesting } from 'stores/nlu/testing/actions';

import NlTesting from './NlTesting';
import { getProjectSelected } from 'stores/project/selectors';

interface PropsFromState {
  nluName: string | null;
  selectedProject: string | null;
  data: any;
  loading: boolean;
  error: string | null;
}

interface PropsFromDispatch {
  fetchAction: typeof fetchTestingRequest;
  clearAction: () => ReturnType<typeof clearTesting>;
}

interface Props
  extends PropsFromState,
    PropsFromDispatch,
    RouteComponentProps<any> {}

interface State {
  showDialog: boolean;
}

class TestingContainer extends React.Component<Props, State> {
  state = { showDialog: false };

  showDialog = () => {
    this.setState({ showDialog: true });
  };

  onHide = () => {
    this.setState({ showDialog: false });
    this.props.clearAction();
  };

  onFetch = sentence => {
    if (this.props.selectedProject && this.props.nluName) {
      this.props.fetchAction(
        this.props.selectedProject,
        this.props.nluName,
        sentence
      );
    }
  };

  render() {
    if (isRootURL(this.props.location.pathname)) {
      return null;
    }
    return (
      <Fragment>
        <div
          className={`${
            this.state.showDialog ? 'kata-test-nlu__panel-overlay' : ''
          }`}
          onClick={this.onHide}
        />
        <div className="row">
          {this.state.showDialog && (
            <NlTesting
              onHide={this.onHide}
              onSubmit={this.onFetch}
              data={this.props.data}
              loading={this.props.loading}
              error={this.props.error}
            />
          )}
          {!this.state.showDialog && (
            <Button
              onClick={this.showDialog}
              className="kata-test-nlu__btn-testing"
            >
              <i className="icon-test-nlu kata-test-nlu__icon-testing" /> Test
              NLU
            </Button>
          )}
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = ({
  nlu: { nlu, testing },
  project
}: RootStore): PropsFromState => {
  const currentNlu = getCurrentNlu(nlu);

  return {
    nluName: currentNlu ? currentNlu.id : null,
    selectedProject: getProjectSelected(project),
    data: testing.data,
    loading: testing.loadings.fetch,
    error: testing.errors.fetch || null
  };
};

const mapDispatchToProps = (dispatch: Function): PropsFromDispatch => {
  return {
    fetchAction: (projectId: string, nluName: string, sentence: string) =>
      dispatch(fetchTestingRequest(projectId, nluName, sentence)),
    clearAction: () => dispatch(clearTesting('clear'))
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(TestingContainer)
);
