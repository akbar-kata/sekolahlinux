import React from 'react';
import './styles.scss';
import { Button } from 'components/Button';
import { Circle } from 'components/Loading';

import { Formik, Form } from 'formik';

interface Props {
  onHide: Function;
  data: any;
  loading: boolean;
  error: string | null;
  onSubmit(data: any);
}

class NlTesting extends React.Component<Props, any> {
  state = {
    sentence: ''
  };

  onSubmit = (event: any) => {
    this.props.onSubmit(this.state.sentence);
  };

  onHideDialog = (event: any) => {
    this.props.onHide();
  };

  onRemove = () => {
    this.setState({ sentence: '' });
  };

  onChange = (event: any) => {
    this.setState({ sentence: event.target.value });
  };

  innerForm = formApi => (
    <div className="kata-test-nlu__widget">
      <div className="kata-test-nlu__panel">
        <div className="kata-test-nlu__panel-heading">
          <div className="kata-test-nlu__panel-title">
            <h2 className="heading2 kata-test-nlu__panel-text">Test NLU</h2>
            <Button
              isIcon
              onClick={this.onHideDialog}
              className="kata-test-nlu__btn-close"
            >
              <i className="icon-close" />
            </Button>
          </div>
        </div>
        <div className="kata-test-nlu__panel-body">
          <Form>
            <input
              type="text"
              autoFocus
              className="kata-form__input-text"
              placeholder="Write test sentence in here..."
              value={this.state.sentence}
              onChange={this.onChange}
            />
            {this.props.loading && (
              <Circle className="kata-test-nlu__loading" size={20} />
            )}
            {this.state.sentence && !this.props.loading && (
              <div className="kata-test-nlu__remove" onClick={this.onRemove}>
                <i className="icon-remove" />
              </div>
            )}
          </Form>
          {this.props.error ? 'There is no training data' : null}
          {this.props.data.id && !this.props.loading && (
            <div className="kata-test-nlu__result-box">
              <pre>
                <code>{JSON.stringify(this.props.data, null, 2)}</code>
              </pre>
            </div>
          )}
        </div>
      </div>
    </div>
  );

  render() {
    return (
      <Formik initialValues={null} onSubmit={this.onSubmit}>
        {this.innerForm}
      </Formik>
    );
  }
}

export default NlTesting;
