import React from 'react';
import { connect } from 'react-redux';

import RootStore from 'interfaces/rootStore';
import { correctLogRequest, fetchLogRequest } from 'stores/nlu/log/actions';
import {
  getLogData,
  getLogIndex,
  getLogError,
  getLogLoading,
  getLogPage
} from 'stores/nlu/log/selectors';
import { getCurrentNlu, getNluLoadings } from 'stores/nlu/nlu/selectors';
import { getEntityLoading, getEntityDetail } from 'stores/nlu/entity/selectors';

import LogList from './LogList';
import { getProjectSelected } from 'stores/project/selectors';

interface PropsFromState {
  nluId: string | null;
  selectedProject: string | null;
  nluIsLoading: boolean;
  isLoading: boolean;
  error: string | null;
  index: string[];
  data: any;
  page: any;
  getEntityDetail: Function;
}

interface PropsFromDispatch {
  onCorrection: typeof correctLogRequest;
  onFetch: typeof fetchLogRequest;
}

interface Props extends PropsFromState, PropsFromDispatch {}

interface State {
  currentPage: number;
}

class LogListContainer extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = {
      currentPage: 1
    };
  }

  componentDidMount() {
    this.onFetch();
  }

  componentWillReceiveProps(nextProps: Props) {
    if (nextProps.selectedProject !== this.props.selectedProject) {
      this.onFetch();
    }
  }

  onFetch = (page: number = 1) => {
    if (this.props.selectedProject && this.props.nluId) {
      this.props.onFetch(this.props.selectedProject, this.props.nluId, page);
    }
  };

  onCorrection = (id, data) => {
    if (this.props.selectedProject && this.props.nluId) {
      this.props.onCorrection(
        this.props.selectedProject,
        this.props.nluId,
        id,
        data
      );
    }
  };

  // NLU Log's pagination is weird so we keep current page number in a local state.
  onPaginationChanged = (page: number) => {
    this.setState({
      currentPage: page
    });

    this.onFetch(page);
  };

  render() {
    return (
      <LogList
        nluIsLoading={this.props.nluIsLoading}
        isLoading={this.props.isLoading}
        error={this.props.error}
        index={this.props.index}
        data={this.props.data}
        page={this.props.page}
        onFetch={this.onFetch}
        onCorrection={this.onCorrection}
        getEntityDetail={this.props.getEntityDetail}
        currentPage={this.state.currentPage}
        onPaginationChanged={this.onPaginationChanged}
      />
    );
  }
}

const mapStateToProps = ({
  nlu: { nlu, entity, log },
  project
}: RootStore): PropsFromState => {
  const currentNlu = getCurrentNlu(nlu);

  return {
    nluId: currentNlu ? currentNlu.id : null,
    selectedProject: getProjectSelected(project),
    nluIsLoading: getNluLoadings(nlu, 'fetch'),
    isLoading: getLogLoading(log, 'fetch') || getEntityLoading(entity, 'fetch'),
    error: getLogError(log, 'fetch'),
    index: getLogIndex(log),
    data: getLogData(log),
    page: getLogPage(log),
    getEntityDetail: (name: string) => getEntityDetail(entity, name)
  };
};

const mapDispatchToProps: PropsFromDispatch = {
  onCorrection: correctLogRequest,
  onFetch: fetchLogRequest
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LogListContainer);
