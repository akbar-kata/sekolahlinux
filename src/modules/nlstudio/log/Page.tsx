import React, { Component } from 'react';

import { Dashboard } from 'components/Dashboard';
import LogList from './LogList.Container';

interface Props {}

interface States {}

class Page extends Component<Props, States> {
  render() {
    return (
      <Dashboard
        className="kata-nlstudio-dashboard"
        title={'Logs'}
        tooltip="All NL prediction from test NLU & message from users will be recorded here."
      >
        <LogList />
      </Dashboard>
    );
  }
}

export default Page;
