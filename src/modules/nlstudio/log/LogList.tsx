import React from 'react';

import { Robot } from 'components/Loading';
import { Pagination } from 'components/Pagination';
import Item from '../training/Item';
import { EmptyMessage } from 'components/Common';
import { NluLogPagination } from 'interfaces/nlu/log';

const logEmptyImg = require('assets/images/log-empty.svg');

interface Props {
  nluIsLoading: boolean;
  isLoading: boolean;
  error: string | null;
  data: any;
  index: string[];
  page: NluLogPagination;
  onFetch: (page?: number) => void;
  onCorrection: (id: string, data: any) => void;
  getEntityDetail: Function;

  // NLU Log's pagination is weird so we keep current page number in a local state.
  currentPage: number;
  onPaginationChanged: (page: number) => void;
}

interface States {}

class LogList extends React.PureComponent<Props, States> {
  renderLoading = () => {
    return (
      <div className="kata-train-page__loading-container">
        <Robot />
      </div>
    );
  };

  renderError = () => {
    return (
      <EmptyMessage image={logEmptyImg} title="Log is empty!">
        All actions from emulator and entity train will be recorded in the log.
      </EmptyMessage>
    );
  };

  renderEmpty = () => {
    return (
      <div className="text-center py-5">
        <EmptyMessage image={logEmptyImg} title="Log is empty!">
          All incoming text from emulator and training will be recorded in the
          log.
        </EmptyMessage>
      </div>
    );
  };

  renderData = () => {
    const { index, data } = this.props;
    if (this.props.isLoading || this.props.nluIsLoading) {
      return this.renderLoading();
    }

    if (this.props.error) {
      return this.renderError();
    }

    if (!index.length) {
      return this.renderEmpty();
    }

    return index.map((id, idx) => (
      <Item
        key={idx}
        type="log"
        text={data[id].input}
        prediction={data[id]}
        getEntityDetail={this.props.getEntityDetail}
        onSubmit={args => this.props.onCorrection(id, args)}
        readOnly={data[id].trained}
      />
    ));
  };

  render() {
    const { page } = this.props;
    return (
      <div className="kata-train-page">
        {this.renderData()}
        {this.props.index && this.props.index.length > 0 ? (
          <div className={this.props.isLoading ? 'mt-8' : 'mt-3'}>
            <Pagination
              current={Number(this.props.currentPage)}
              total={page.total}
              onSelect={selected => this.props.onPaginationChanged(selected)}
            />
            <div className="float-right">
              Page <strong>{this.props.currentPage}</strong> of{' '}
              <strong>{page.total}</strong>
            </div>
          </div>
        ) : null}
      </div>
    );
  }
}

export default LogList;
