import React from 'react';
import { connect } from 'react-redux';

import { fetchVersionRequest } from 'stores/bot/version/actions';

import VersionRefresh from './VersionRefresh';

interface PropsFromState {
  selectedNlu: string;
  latest: string;
}

interface PropsFromDispatch {
  refreshAction: Function;
}

const VersionRefresContainer = (props: any) => <VersionRefresh {...props} />;

const mapStateToProps = ({ nlu, version }: any): PropsFromState => {
  return {
    selectedNlu: nlu.selected,
    latest: version.latest
  };
};

const mapDispatchToProps = (dispatch: Function): PropsFromDispatch => {
  return {
    refreshAction: (nluId: string) => dispatch(fetchVersionRequest(nluId))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(VersionRefresContainer);
