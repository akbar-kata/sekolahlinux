import React from 'react';
import moment from 'moment';

import VersionInfo from './VersionInfo';
import { EmptyMessage } from 'components/Common';
import { NluRevision } from 'interfaces/revision';
import IconicButton from 'components/Button/IconicButton';
import { InfiniteScrollSpinner } from 'components/Loading';

interface Props {
  isLoading: boolean;
  index: string[];
  data: Record<string, NluRevision>;
  selectedProject: string | null;
  isVersionListOpen: boolean;
  versionData: any;
  openDrawer(data: any): void;
  closeDrawer(): void;
  setSelectedVersion(version: string): any;
  showNotif(title: string, message: string): void;
}

const colWidth = {
  width: 150
};

class VersionList extends React.Component<Props, any> {
  static defaultProps = {
    isVersionListOpen: false
  };

  renderMessage(child: React.ReactNode) {
    return (
      <tr>
        <td colSpan={6} className="text-center">
          {child}
        </td>
      </tr>
    );
  }

  renderLoading() {
    return (
      <tr>
        <td colSpan={4} className="text-center">
          <h5 className="pa4 text-bold">
            <InfiniteScrollSpinner />
          </h5>
        </td>
      </tr>
    );
  }

  renderData() {
    const { data, index } = this.props;

    if (index.length === 0) {
      return this.renderMessage(
        <EmptyMessage title="Version list is empty">
          This NLU don’t have any version yet.
        </EmptyMessage>
      );
    }

    return index.map((revisionId, idx) => {
      const revision = data[revisionId];
      if (!revision) {
        return null;
      }
      return (
        <tr key={idx} className={false ? 'kata-version--latest' : ''}>
          <td>
            <samp>{revision.revision}</samp>
          </td>
          <td>
            {revision.created_at
              ? moment(revision.created_at).format('DD MMM YYYY - HH:mm:ss')
              : '-'}
          </td>
          <td className="text-center">
            <div
              className="kata-version__detail-icon"
              onClick={() => {
                this.props.openDrawer({
                  version: revision.revision,
                  username: '',
                  timestamp: moment(revision.created_at).format(
                    'DD MMM YYYY - HH:mm:ss'
                  ),
                  changelog: ''
                });
                this.props.setSelectedVersion(revision.revision!);
              }}
            >
              <IconicButton>
                <i className="icon icon-view mr-1" />
                View
              </IconicButton>
            </div>
          </td>
        </tr>
      );
    });
  }

  render() {
    const { isLoading, selectedProject } = this.props;

    return (
      <div className="kata-version__table">
        <table className="kata-table kata-table--striped kata-table--hover">
          <thead>
            <tr>
              <th>Snapshot</th>
              <th>Date</th>
              <th style={colWidth} className="text-center">
                Details
              </th>
            </tr>
          </thead>
          <tbody>
            {((isLoading || !selectedProject) && this.renderLoading()) ||
              this.renderData()}
          </tbody>
        </table>
        <VersionInfo {...this.props} />
      </div>
    );
  }
}

export default VersionList;
