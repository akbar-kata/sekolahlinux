import React from 'react';
import { connect } from 'react-redux';

import RootStore from 'interfaces/rootStore';
import { NluRevision } from 'interfaces/revision';

import REVISION from 'stores/revision/types';
import { selectVersion } from 'stores/bot/version/actions';
import { addNotification } from 'stores/app/notification/actions';
import { closeDrawer, openDrawer } from 'stores/app/drawer/actions';
import { isDrawerOpen, getDrawerData } from 'stores/app/drawer/selectors';
import { getProjectSelected } from 'stores/project/selectors';
import { fetchNluRevisionsRequest } from 'stores/revision/actions';
import { getLoading } from 'stores/app/loadings/selectors';
import {
  getNluRevisionsData,
  getNluRevisionsIndex
} from 'stores/revision/nlu/selectors';

import VersionList from './VersionList';

interface PropsFromState {
  selectedProject: string | null;
  isLoading: boolean;
  data: Record<string, NluRevision>;
  index: string[];
  isVersionListOpen: boolean;
  versionData: any;
  // botDetail: any;
}

interface PropsFromDispatch {
  fetchRevisions(projectId: string): any;
  openDrawer(data: any): void;
  closeDrawer(): void;
  setSelectedVersion(version: string): any;
  showSuccessNotification(title: string, message: string): void;
}

interface Props extends PropsFromState, PropsFromDispatch {}

interface State {
  isOpen: boolean;
  key: string;
}

class VersionListContainer extends React.Component<Props, State> {
  public state = {
    isOpen: false,
    key: ''
  };

  componentDidMount() {
    if (this.props.selectedProject) {
      this.props.fetchRevisions(this.props.selectedProject);
    }
  }

  componentDidUpdate(prev: Props) {
    if (
      this.props.selectedProject &&
      prev.selectedProject !== this.props.selectedProject
    ) {
      this.props.fetchRevisions(this.props.selectedProject);
    }
  }

  render() {
    return (
      <VersionList
        {...this.props}
        showNotif={this.props.showSuccessNotification}
      />
    );
  }
}

const mapStateToProps = ({
  project,
  revision,
  app: { drawer, loadings }
}: RootStore): PropsFromState => {
  const selectedBot = getProjectSelected(project) as string;
  return {
    selectedProject: selectedBot,
    isLoading: getLoading(loadings, REVISION.FETCH_NLU_REVISIONS_REQUEST),
    data: getNluRevisionsData(revision),
    index: getNluRevisionsIndex(revision),
    isVersionListOpen: isDrawerOpen(drawer, 'VersionList'),
    versionData: getDrawerData(drawer, 'VersionList')
  };
};

const mapDispatchToProps: PropsFromDispatch = {
  fetchRevisions: fetchNluRevisionsRequest,
  openDrawer: (data: any) => openDrawer('VersionList', data),
  closeDrawer: () => closeDrawer('VersionList'),
  setSelectedVersion: (version: string) => selectVersion(version),
  showSuccessNotification: (title: string, message: string) =>
    addNotification({
      title,
      message,
      status: 'success',
      dismissible: true,
      dismissAfter: 5000
    })
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(VersionListContainer);
