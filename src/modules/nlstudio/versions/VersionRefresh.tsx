import React from 'react';
import { FloatingButton } from 'components/Button';

interface Props {
  selectedBot: string;
  isLoading: boolean;
  latest: string;
  refreshAction: Function;
}

class VersionList extends React.Component<Props, any> {
  onRefresh = () => {
    this.props.refreshAction(this.props.selectedBot);
  };

  render() {
    const { latest } = this.props;
    return (
      <div className="kata-version__heading-children">
        <input
          type="text"
          className="form-control"
          value={`Latest Version: ${latest}`}
          disabled
        />
        <FloatingButton icon="refresh" onClick={this.onRefresh} />
      </div>
    );
  }
}

export default VersionList;
