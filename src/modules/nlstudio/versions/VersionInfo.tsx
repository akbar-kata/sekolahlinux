import React from 'react';

import {
  Drawer,
  DrawerBody,
  DrawerHeader,
  DrawerFooter
} from 'components/Drawer';
// import { DropdownSelector, DropdownItem } from 'components/Dropdown';
import { Button } from 'components/Button';

import 'brace';
import 'brace/mode/yaml';
import { NluRevision } from 'interfaces/revision';

interface Props {
  isLoading: boolean;
  data: Record<string, NluRevision>;
  selectedProject: string | null;
  isVersionListOpen: boolean;
  versionData: any;
  openDrawer(data: any): void;
  closeDrawer(): void;
  showNotif(title: string, message: string): void;
}

interface State {
  mode: string;
  isShow: boolean;
  readyToDownload: boolean;
}

const defaultState = {
  mode: 'yaml',
  isShow: false,
  readyToDownload: false
};

class VersionInfo extends React.Component<Props, State> {
  state = defaultState;

  modeChange = (mode: string) => {
    this.setState({
      mode
    });
  };

  onCloseDrawer = () => {
    this.setState(defaultState);
    this.props.closeDrawer();
  };

  render() {
    const { isVersionListOpen, versionData = {} } = this.props;

    return (
      <Drawer isOpen={isVersionListOpen} onClose={this.onCloseDrawer}>
        <DrawerHeader title="Version List Details" />
        <DrawerBody>
          <div className="row">
            <div className="col">
              <div className="kata-info__container">
                <div className="kata-info__label">Version</div>
                <div className="kata-info__content">
                  <input
                    type="text"
                    className="form-control"
                    value={versionData.version}
                    disabled
                  />
                </div>
              </div>
            </div>

            <div className="col">
              <div className="kata-info__container">
                <div className="kata-info__label">Date</div>
                <div className="kata-info__content">
                  <input
                    type="text"
                    className="form-control"
                    value={versionData.timestamp}
                    disabled
                  />
                </div>
              </div>
            </div>
          </div>

          {/*
          <div className="kata-info__container">
            <div className="kata-info__label">Deployed By</div>
            <div className="kata-info__content">
              <input
                type="text"
                className="form-control"
                value={versionData.username}
                disabled
              />
            </div>
          </div>

          <div className="kata-info__container">
            <div className="kata-info__label">KataML</div>
          </div>

          {this.state.isShow && (
            <Fragment>
              <div className="kata-info__container">
                <div className="kata-info__label">Mode</div>
                <div className="kata-info__content">
                  <DropdownSelector
                    value={this.state.mode.toUpperCase()}
                    onSelect={(val: string) => this.modeChange(val)}
                    block
                  >
                    <DropdownItem value="yaml">YAML</DropdownItem>
                    <DropdownItem value="json">JSON</DropdownItem>
                  </DropdownSelector>
                </div>
              </div>
            </Fragment>
          )}
          */}
        </DrawerBody>
        <DrawerFooter>
          <Button onClick={this.props.closeDrawer}>Close</Button>
        </DrawerFooter>
      </Drawer>
    );
  }
}

export default VersionInfo;
