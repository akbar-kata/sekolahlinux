import React from 'react';

import { colorGenerator } from 'utils/hexGenerator';

interface Props {
  text: string;
  entities: any[];
}

interface States {
  showCollided: boolean;
}

const highlightStyle = {
  background: 'white',
  border: '1px solid #aaa'
};

const errorStyle = {
  color: '#d67070',
  border: '1px solid #d08d8d'
};

class Annotated extends React.PureComponent<Props, States> {
  constructor(props: Props) {
    super(props);
    this.state = {
      showCollided: false
    };
  }

  toggleCollided = event => {
    if (event) {
      event.preventDefault(); // Let's stop this event.
      event.stopPropagation(); // Really this time.
      event.nativeEvent.preventDefault();
      event.nativeEvent.stopImmediatePropagation();
    }
    this.setState({
      showCollided: !this.state.showCollided
    });
  };

  renderAnnotation = (text: string, arr: any[]) => {
    const formatted: any[] = text.split('');
    arr.reduce((offset, item, index) => {
      const style = item.highlight
        ? highlightStyle
        : item.collided
        ? errorStyle
        : {
            background: colorGenerator(item.entity.toLowerCase())
          };
      const length = item.end - item.start;
      const el = (
        <span
          className="label noselect kata-train-item__label"
          style={style}
          key={index}
        >
          {text.substr(item.start, length)}
        </span>
      );
      formatted.splice(item.start - offset, length, el);
      return offset + (item.end - item.start - 1);
    }, 0);
    return formatted;
  };

  render() {
    const collided: any[] = [];
    const sortedEntities = this.props.entities.concat().sort((a, b) => {
      if (!a) {
        return 1;
      }

      if (!b) {
        return -1;
      }

      return a.start - b.start;
    });
    const formatted = this.renderAnnotation(
      this.props.text,
      sortedEntities.filter((tag, index) => {
        if (!tag) {
          return false;
        }
        if (
          sortedEntities
            .slice(0, index)
            .some(item => item && tag.start < item.end)
        ) {
          collided.push({ ...tag, collided: true });
          return false;
        }
        return true;
      })
    );
    return (
      <div
        className="d-inline-block kata-train-annotated__container"
        contentEditable
        suppressContentEditableWarning
        onKeyDown={e => e.preventDefault()}
      >
        {formatted}
        {collided.length ? (
          <div className="noselect my-3">
            {!this.state.showCollided ? (
              <div className="text-danger">
                There are <strong>{collided.length} entity</strong> that
                overlaped{' '}
                <button
                  className="btn btn-default btn-sm"
                  onClick={this.toggleCollided}
                >
                  Show them
                </button>
              </div>
            ) : (
              <div className="botstudio-item">
                {this.renderAnnotation(this.props.text, collided)}{' '}
                <button
                  className="btn btn-default btn-sm"
                  onClick={this.toggleCollided}
                >
                  Hide
                </button>
              </div>
            )}
          </div>
        ) : null}
      </div>
    );
  }
}

export default Annotated;
