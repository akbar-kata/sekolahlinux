import React, { Component } from 'react';
import classnames from 'classnames';

import { Robot } from 'components/Loading';
import { Pagination } from 'components/Pagination';
import Item from './Item';
import { EmptyMessage } from 'components/Common';
import { NluTrainPage, NluTrainDataMap } from 'interfaces/nlu/train';

interface Props {
  loading: boolean;
  nluIsLoading: boolean;
  error: string | null;
  nluId: string | null;
  index: string[];
  page: NluTrainPage;
  data: NluTrainDataMap;
  getEntityDetail: Function;
  onFetch: Function;
}

interface States {}

const botTrainImg = require('assets/images/train-bot.svg');

class TrainList extends Component<Props, States> {
  renderLoading = () => {
    return (
      <div className="kata-train-page__loading-container">
        <Robot />
      </div>
    );
  };

  renderError = () => {
    return (
      <EmptyMessage image={botTrainImg} title="Train your bot!">
        Train your bot by adding sentences and tag them with entities and
        intents.
      </EmptyMessage>
    );
  };

  renderEmpty = () => {
    return (
      <div className="text-center pt-7">
        <EmptyMessage image={botTrainImg} title="Train your bot!">
          Train your bot by adding sentences and tag them with entities and
          intents.
        </EmptyMessage>
      </div>
    );
  };

  renderData = () => {
    const { index, data } = this.props;
    if (this.props.loading || this.props.nluIsLoading) {
      return this.renderLoading();
    }
    if (this.props.error) {
      return this.renderError();
    }
    if (!this.props.index.length) {
      return this.renderEmpty();
    }
    return index.map((id, idx) => (
      <Item
        key={idx}
        text={data[id].input}
        prediction={data[id]}
        getEntityDetail={this.props.getEntityDetail}
        readOnly
      />
    ));
  };

  render() {
    const { nluId, page } = this.props;
    return (
      <div className="kata-train-page">
        {this.renderData()}
        {this.props.index && this.props.index.length > 0 ? (
          <div
            className={classnames(
              'kata-train-list',
              this.props.loading ? 'mt-8' : 'mt-3'
            )}
          >
            <Pagination
              current={Number(page.page)}
              total={Math.ceil(page.total / page.limit)}
              onSelect={selected => this.props.onFetch(nluId, selected)}
            />
            <div className="kata-train-list__page-numbers">
              Page <strong>{page.page}</strong> of{' '}
              <strong>{Math.ceil(page.total / page.limit)}</strong>
            </div>
          </div>
        ) : null}
      </div>
    );
  }
}

export default TrainList;
