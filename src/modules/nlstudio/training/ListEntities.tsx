import React from 'react';

interface Props {
  onSelect: Function;
  onRemove: Function;
  value?: any;
  entities?: any;
}
class ListEntities extends React.Component<Props, any> {
  state = {
    value: '',
    isOpen: '',
    list: '',
    type: '',
    entity: '',
    isMenuOpen: false
  };

  componentWillReceiveProps(nextProps: Props) {
    if (nextProps.value) {
      this.setState({ value: nextProps.value });
    }
  }

  renderEntities = () => {
    function* listEntity(entities: any) {
      for (const key in entities) {
        if (entities.hasOwnProperty(key)) {
          if (entities[key].type !== 'trait') {
            yield {
              label: entities[key].name,
              value: entities[key].name
            };
          }
        }
      }
    }

    return Array.from(listEntity(this.props.entities));
  };
  toggle = () => {
    this.setState({ isMenuOpen: !this.state.isMenuOpen });
  };

  close = () => {
    this.setState({ isMenuOpen: false });
  };

  onRemove = () => {
    this.setState({ value: '' });
    this.props.onRemove();
  };

  select = () => {
    this.props.onSelect(this.state.entity, this.state.type);
    this.setState({ value: '' });
  };

  renderEntity = (val, idx) => {
    if (val.labels) {
      return (
        <li className="dropdown-submenu" key={idx}>
          <a tabIndex={-1} href="#" role="menuitem">
            {val.name}
          </a>
          <ul className="dropdown-menu">
            {val.labels.map((element, index) => {
              return (
                <li key={index}>
                  <a
                    tabIndex={-1}
                    href="#"
                    role="menuitem"
                    onClick={e => {
                      e.preventDefault();
                      this.setState(
                        {
                          entity: `${val.name}:${element}`,
                          type: val.type,
                          isOpen: ''
                        },
                        () => {
                          this.select();
                        }
                      );
                    }}
                  >
                    {element}
                  </a>
                </li>
              );
            })}
          </ul>
        </li>
      );
    }

    return (
      <li key={idx}>
        <a
          tabIndex={-1}
          href="#"
          role="menuitem"
          onClick={e => {
            e.preventDefault();
            this.setState(
              { entity: val.name, type: val.type, isOpen: '' },
              () => {
                this.select();
              }
            );
          }}
        >
          {val.name}
        </a>
      </li>
    );
  };

  render() {
    let { entities } = this.props;
    entities = Object.values(entities);
    const { isOpen } = this.state;
    return (
      <div className="wrap-entity">
        <table className="table">
          <tbody>
            <tr>
              <td style={{ width: 320 }}>
                <div className="entity-item">
                  <div
                    className={`dropdown ${isOpen} btn-group btn-group-sm btn-group-default`}
                  >
                    <button
                      type="button"
                      className="tagbox btn btn-sm btn-default"
                    >
                      Add Trait
                    </button>
                    <button
                      aria-label="Add Trait"
                      id="split-button-basic-bsjkf7"
                      role="button"
                      aria-haspopup="true"
                      aria-expanded="true"
                      type="button"
                      className="dropdown-toggle btn btn-sm btn-default"
                      onClick={() => {
                        this.setState({ isOpen: 'open' });
                      }}
                    >
                      <span className="caret" />
                    </button>
                    <ul
                      role="menu"
                      className="dropdown-menu"
                      aria-labelledby="split-button-basic-bsjkf7"
                      onMouseLeave={() => {
                        this.setState({ isOpen: '' });
                      }}
                    >
                      {entities
                        .filter(item => {
                          if (
                            this.state.value &&
                            this.state.value.trim() !== ''
                          ) {
                            return ['dict', 'phrase'].indexOf(item.type) !== -1;
                          }

                          return item.type === 'trait';
                        })
                        .map((val, index) => this.renderEntity(val, index))}
                    </ul>
                  </div>
                </div>
              </td>
              <td style={{ width: 220 }}>
                <div className="entity-item">&nbsp;</div>
              </td>
              <td style={{ width: 220 }}>
                <div className="entity-item">{this.state.value}</div>
              </td>
              <td style={{ width: 220 }}>
                <div className="entity-item">&nbsp;</div>
              </td>
              <td style={{ width: 220 }}>
                {this.state.value && (
                  <div className="action-close pull-right">
                    <i className="icon-close" onClick={this.onRemove} />
                  </div>
                )}
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
}

export default ListEntities;
