import React, { Component } from 'react';

import { Dashboard } from 'components/Dashboard';
import PredictBox from './PredictBox.Container';
import TrainList from './TrainList.Container';

interface Props {}

interface States {
  predictActive: boolean;
}

class Page extends Component<Props, States> {
  state = {
    predictActive: false
  };

  enablePredict = () => {
    this.setState({
      predictActive: true
    });
  };

  disablePredict = () => {
    this.setState({
      predictActive: false
    });
  };

  render() {
    return (
      <Dashboard
        className="kata-nlstudio-dashboard"
        title={'Training'}
        tooltip="We recommend training at least 10 phrases per intent/entity."
      >
        <PredictBox
          predictActive={this.state.predictActive}
          enablePredict={this.enablePredict}
          disablePredict={this.disablePredict}
        />
        <TrainList predictActive={this.state.predictActive} />
      </Dashboard>
    );
  }
}

export default Page;
