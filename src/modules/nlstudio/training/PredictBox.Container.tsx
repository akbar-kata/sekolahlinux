import React from 'react';
import { connect } from 'react-redux';

import RootStore from 'interfaces/rootStore';
import { confirm } from 'components/Modal';
import PredictBox from './PredictBox';
import { fetchPredictRequest } from 'stores/nlu/predict/actions';
import { addTrainRequest } from 'stores/nlu/train/actions';
import { getEntityDetail } from 'stores/nlu/entity/selectors';
import {
  getPredictData,
  getPredictLoading
} from 'stores/nlu/predict/selectors';
import { addNotification } from 'stores/app/notification/actions';
import { getCurrentNlu } from 'stores/nlu/nlu/selectors';
import { getProjectSelected } from 'stores/project/selectors';

interface PropsFromState {
  prediction: any;
  predictLoading: boolean;
  trainLoading: boolean;
  nlu: string | null;
  selectedProject: string | null;
  getEntityDetail: Function;
}

interface PropsFromDispatch {
  onPredict: typeof fetchPredictRequest;
  onTrain: typeof addTrainRequest;
  showNotif(title: string, message: string): void;
}

interface State {
  sentence: string;
}

interface Props extends PropsFromState, PropsFromDispatch {
  predictActive: boolean;
  enablePredict(): void;
  disablePredict(): void;
}

class PredictBoxContainer extends React.Component<Props, State> {
  state = {
    sentence: ''
  };

  componentWillReceiveProps(next: Props) {
    if (this.props.predictLoading && !next.predictLoading) {
      this.props.enablePredict();
    }
  }

  onPredict = data => {
    if (this.props.selectedProject && this.props.nlu) {
      this.props.onPredict(this.props.selectedProject, this.props.nlu, data);
      this.setState({ sentence: data.sentence });
    }
  };

  onRemovePredict = () => {
    confirm({
      title: 'Remove Prediction',
      message: 'Are you sure you want to remove this dataset?',
      okLabel: 'Remove',
      cancelLabel: 'Cancel'
    }).then(response => {
      if (response) {
        this.props.disablePredict();
        this.props.showNotif('Success', 'Dataset has been removed.');
      }
    });
  };

  onTrain = data => {
    if (this.props.selectedProject && this.props.nlu) {
      this.props.onTrain(this.props.selectedProject, this.props.nlu, data);
      this.setState({ sentence: '' });
      this.props.disablePredict();
    }
  };

  render() {
    return (
      <PredictBox
        text={this.state.sentence}
        predictActive={this.props.predictActive}
        prediction={this.props.prediction}
        predictLoading={this.props.predictLoading}
        trainLoading={false}
        onPredict={this.onPredict}
        onRemovePredict={this.onRemovePredict}
        onTrain={this.onTrain}
        getEntityDetail={this.props.getEntityDetail}
      />
    );
  }
}

const mapStateToProps = ({
  nlu: { nlu, predict, entity },
  project
}: RootStore): PropsFromState => {
  const currentNlu = getCurrentNlu(nlu);

  return {
    nlu: currentNlu ? currentNlu.id : null,
    selectedProject: getProjectSelected(project),
    prediction: getPredictData(predict),
    predictLoading: getPredictLoading(predict, 'fetch'),
    trainLoading: false,
    getEntityDetail: (name: string) => getEntityDetail(entity, name)
  };
};

const mapDispatchToProps: PropsFromDispatch = {
  onPredict: fetchPredictRequest,
  onTrain: addTrainRequest,
  showNotif: (title: string, message: string) =>
    addNotification({
      title,
      message,
      status: 'success',
      dismissible: true,
      dismissAfter: 5000
    })
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PredictBoxContainer);
