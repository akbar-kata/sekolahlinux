function removeCollision(
  text: string,
  entities: any[],
  start: number,
  end: number
): {
  start: number;
  end: number;
  text: string;
} {
  const startCollations = entities
    .filter(item => item.end >= start && item.end <= end)
    .sort((a, b) => {
      if (!a) {
        return 1;
      }
      if (!b) {
        return -1;
      }
      return a.start - b.start;
    });
  let newStart = startCollations.length ? startCollations[0].end : start;
  const endCollations = entities
    .filter(item => item.start <= end && item.start >= newStart)
    .sort((a, b) => {
      if (!a) {
        return 1;
      }
      if (!b) {
        return -1;
      }
      return a.start - b.start;
    });
  let newEnd = endCollations.length ? endCollations[0].start : end;

  const subText = text.substr(newStart, newEnd - newStart);
  const spaceStartCount = subText.search(/\S|$/);
  newStart = newStart + spaceStartCount;
  newEnd = newStart + subText.search(/\s+$|$/) - spaceStartCount;
  return {
    start: newStart,
    end: newEnd,
    text: text.substr(newStart, newEnd - newStart)
  };
}

type ResultSelection =
  | {
      start: number;
      end: number;
    }
  | false;

function getSelectionRange(text: string): ResultSelection {
  let sel: any = null;
  let result: ResultSelection = false;

  if (typeof window.getSelection === 'function') {
    // modern browsers
    sel = window.getSelection();
  } else if (typeof document.getSelection === 'function') {
    sel = document.getSelection();
  }

  if (sel.toString() !== '') {
    const range = sel.getRangeAt(0);
    const selected = sel.toString().trim();
    const parentElement = range.startContainer.parentElement;
    if (parentElement !== null && selected && selected !== '') {
      const priorRange = range.cloneRange();
      priorRange.selectNodeContents(parentElement);
      priorRange.setEnd(range.startContainer, range.startOffset);
      let start = priorRange.toString().length;
      let end = start + range.toString().length;
      const regex = /[a-zA-Z0-9]/;
      while (start > 0 && text[start - 1] && text[start - 1].match(regex)) {
        // tslint:disable-next-line:no-increment-decrement - legacy code
        start--;
      }
      while (text[end] && text[end].match(regex)) {
        // tslint:disable-next-line:no-increment-decrement - legacy code
        end++;
      }
      result = {
        start,
        end
      };
    }
  }
  sel.removeAllRanges();
  return result;
}

function highlightSelection(sentence: string, entities: any[]) {
  const range = getSelectionRange(sentence);
  if (!range) {
    return false;
  }
  const selected = removeCollision(sentence, entities, range.start, range.end);
  if (selected.text === '') {
    return false;
  }
  return selected;
}

export default highlightSelection;
