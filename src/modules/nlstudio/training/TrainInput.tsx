import React from 'react';
import { Formik, Form } from 'formik';
import { Circle } from 'components/Loading';

interface Props {
  loading: boolean;
  onSubmit(data: any);
}

interface States {
  text: string;
  filter: string;
}

class TrainInput extends React.Component<Props, States> {
  state = {
    text: '',
    filter: 'all'
  };

  componentWillReceiveProps(next: Props) {
    if (!next.loading && next.loading !== this.props.loading) {
      this.setState({ text: '' });
    }
  }

  onInputChange = event => {
    this.setState({ text: event.target.value });
  };

  onFilterChange = (value: string) => {
    this.setState({ filter: value });
  };

  onSubmit = (event: any) => {
    this.props.onSubmit({
      sentence: this.state.text,
      filter: this.state.filter,
      log: false
    });
  };

  innerForm = formApi => (
    <div className="kata-train-input">
      <Form>
        <input
          type="text"
          className="form-control kata-form__input-text kata-train-input__textbox"
          placeholder="Type your sentence in here…"
          value={this.state.text}
          onChange={this.onInputChange}
        />
        <div className="kata-train-input__floating">
          {this.props.loading && (
            <Circle className="kata-train-input__loading" size={30} />
          )}
        </div>
      </Form>
    </div>
  );

  render() {
    return (
      <Formik initialValues={null} onSubmit={this.onSubmit}>
        {this.innerForm}
      </Formik>
    );
  }
}

export default TrainInput;
