import React, { Fragment } from 'react';
import numeral from 'numeral';
import shortid from 'shortid';

import { colorGenerator } from 'utils/hexGenerator';
import highlightSelection from './utils';
import EntityContainer from 'containers/EntityContainer';
import { Button } from 'components/Button';
import { SplitButton } from 'components/SplitButton';
import { DropdownItem } from 'components/Dropdown';
import Annotated from './Annotated';

const width200 = {
  width: 200
};

const width150 = {
  width: 150
};

const width18 = {
  width: 18
};

interface Props {
  readOnly?: boolean;
  text: string;
  entities: any[];
  traits: any[];
  cancelText?: string;
  getEntityDetail: Function;
  onSubmit: Function;
  onCancel: Function;
}

interface States {
  entities: any[];
  traits: any[];
  selected: {
    text: string;
    start: number;
    end: number;
    highlight: true;
  } | null;
}

const Wrapper = (props: { className?: string; children: any }) => (
  <div className={`kata-train-form__item ${props.className || ''}`}>
    <table className="w-100">
      <tbody>
        <tr>{props.children}</tr>
      </tbody>
    </table>
  </div>
);

export default class ItemForm extends React.Component<Props, States> {
  static defaultProps = {
    cancelText: 'Cancel'
  };

  state: States = {
    entities: [],
    traits: [],
    selected: null
  };

  componentDidMount() {
    this.setState({
      entities: this.props.entities,
      traits: this.props.traits
    });
  }

  componentWillReceiveProps(next: Props) {
    if (
      next.entities !== this.props.entities ||
      next.traits !== this.props.traits
    ) {
      this.setState({
        entities: next.entities,
        traits: next.traits
      });
    }
  }

  highlight = () => {
    const selected = highlightSelection(this.props.text, this.state.entities);
    if (selected) {
      this.setState({
        selected: {
          ...selected,
          highlight: true
        }
      });
    }
  };

  getBelongsToDetail = (entities: any[], id) => {
    const arr = entities.filter(item => item.tempId === id);
    return arr.length ? arr[0].value : '';
  };

  changeBelongsTo = (item, index, newId) => {
    const { entities } = this.state;
    this.setState({
      entities: [
        ...entities.slice(0, index),
        {
          ...item,
          belongsTo: newId
        },
        ...entities.slice(index + 1)
      ]
    });
  };

  removeEntity = (tempId, index) => {
    const newEntities = this.state.entities.map(({ belongsTo, ...rest }) => ({
      ...rest,
      belongsTo: belongsTo === tempId ? undefined : belongsTo
    }));
    this.setState({
      entities: [
        ...newEntities.slice(0, index),
        ...newEntities.slice(index + 1)
      ]
    });
  };

  removeSelected = () => {
    this.setState({ selected: null });
  };

  removeTrait = index => {
    this.setState({
      traits: [
        ...this.state.traits.slice(0, index),
        ...this.state.traits.slice(index + 1)
      ]
    });
  };

  addEntity = (selected, entity) => {
    this.setState({
      entities: [
        ...this.state.entities,
        {
          tempId: shortid.generate(),
          entity: entity.name,
          start: selected.start,
          end: selected.end,
          value: selected.text,
          label: entity.label,
          detail: this.props.getEntityDetail(entity.name)
        }
      ]
    });
    this.removeSelected();
  };

  addTrait = entity => {
    const arr = entity.split(':');
    this.setState({
      traits: [
        ...this.state.traits,
        {
          entity: arr[0],
          start: 0,
          end: this.props.text.length,
          value: arr[1],
          label: arr[1]
        }
      ]
    });
  };

  renderEntity = item => {
    return (
      <Fragment>
        <span
          className="label kata-train-form__label font-weight-bold"
          style={{
            background: colorGenerator(item.entity.toLowerCase())
          }}
        >
          {item.entity}
          {item.label && `:${item.label.toLowerCase()}`}
        </span>
        {item.score && (
          <span className="label kata-train-form__label kata-train-form__label--white ml-1">
            {numeral(item.score).format('0.000')}
          </span>
        )}
      </Fragment>
    );
  };

  renderBelongsTo = (item, index) => {
    if (!item.detail || !item.detail.belongsTo) {
      return null;
    }
    return !this.props.readOnly ? (
      <SplitButton
        color="white"
        size="small"
        title="Belongs to"
        subtitle={this.getBelongsToDetail(this.state.entities, item.belongsTo)}
        className="kata-train-form__splitbutton"
      >
        {this.state.entities
          .filter(val => val.entity === item.detail.belongsTo)
          .sort((a, b) => {
            if (!a) {
              return 1;
            }

            if (!b) {
              return -1;
            }

            return a.start - b.start;
          })
          .map((val, idx) => (
            <DropdownItem
              key={idx}
              onClick={() => this.changeBelongsTo(item, index, val.tempId)}
            >
              {val.value}
            </DropdownItem>
          ))}
      </SplitButton>
    ) : item.belongsTo ? (
      <div>
        Belongs to{' '}
        <span className="label kata-train-form__label kata-train-form__label--trait">
          {this.getBelongsToDetail(this.state.entities, item.belongsTo)}
        </span>
      </div>
    ) : null;
  };

  renderSelected = () => {
    const { selected } = this.state;
    return selected ? (
      <Wrapper>
        <td style={width200}>
          <EntityContainer>
            {({ loading, error, entities }) => {
              return loading ? (
                <div>Loading...</div>
              ) : error ? (
                <div>{error}</div>
              ) : (
                <SplitButton
                  color="white"
                  size="small"
                  title="Add Entity"
                  className="kata-train-form__splitbutton"
                >
                  {entities
                    .reduce((arr, item, idx) => {
                      const newArr: { name: string; label?: string }[] = [];
                      if (item.labels && item.labels.length) {
                        item.labels.forEach(label =>
                          newArr.push({
                            label,
                            name: item.name
                          })
                        );
                      } else {
                        newArr.push({ name: item.name });
                      }
                      return [...arr, ...newArr];
                    }, [])
                    .map((entity, idx) => (
                      <DropdownItem
                        key={idx}
                        onClick={() => this.addEntity(selected, entity)}
                      >
                        {entity.name}
                        {entity.label ? `:${entity.label}` : ''}
                      </DropdownItem>
                    ))}
                </SplitButton>
              );
            }}
          </EntityContainer>
        </td>
        <td className="kata-train-form__text" style={width150}>
          {this.props.text.substr(
            selected.start,
            selected.end - selected.start
          )}
        </td>
        <td>&nbsp;</td>
        <td style={width18}>
          <i
            className="icon-remove icon-middle kata-train-form__close"
            onClick={() => this.removeSelected()}
          />
        </td>
      </Wrapper>
    ) : null;
  };

  renderTraits = () => {
    return this.state.traits.map((item, index) => (
      <Wrapper key={index} className="kata-train-form__item-block">
        <td style={width200}>
          {item.entity}{' '}
          <span className="label kata-train-form__label kata-train-form__label--dark font-weight-bold ">
            {item.value}
          </span>
          {item.score && (
            <span className="label kata-train-form__label kata-train-form__label--white ml-1">
              {numeral(item.score).format('0.000')}
            </span>
          )}
        </td>
        <td>&nbsp;</td>
        <td style={width18}>
          <i
            className="icon-remove icon-middle kata-train-form__close"
            onClick={() => this.removeTrait(index)}
          />
        </td>
      </Wrapper>
    ));
  };

  renderGroupedTraits = () => {
    const traits = this.state.traits.reduce((obj, item) => {
      let arr = obj[item.entity];
      if (arr && arr.length) {
        arr.push(item.value);
      } else {
        arr = [item.value];
      }
      return {
        ...obj,
        [item.entity]: arr
      };
    }, {});
    return Object.keys(traits).map((item, index) => (
      <Wrapper key={index} className="kata-train-form__item-block">
        <td>
          <span className="mr-1">{item}</span>{' '}
          {traits[item] && traits[item].length
            ? traits[item].map((sub, subIndex) => (
                <Fragment key={subIndex}>
                  <span className="label kata-train-form__label kata-train-form__label--dark font-weight-bold">
                    {sub}
                  </span>{' '}
                </Fragment>
              ))
            : null}
        </td>
      </Wrapper>
    ));
  };

  renderNewTrait = () => {
    return (
      <Wrapper className="kata-train-form__item-block">
        <td style={width200}>
          <EntityContainer>
            {({ loading, error, traits }) => {
              return loading ? (
                <div>Loading...</div>
              ) : error ? (
                <div>{error}</div>
              ) : (
                <SplitButton
                  color="white"
                  size="small"
                  title="Add Trait"
                  className="kata-train-form__splitbutton"
                >
                  {traits
                    .reduce((arr, itm, idx) => {
                      const newArr: string[] = [];
                      if (itm.labels && itm.labels.length) {
                        itm.labels.forEach(label =>
                          newArr.push(`${itm.name}:${label}`)
                        );
                      } else {
                        newArr.push(itm.name);
                      }
                      return [...arr, ...newArr];
                    }, [])
                    .filter(
                      entity =>
                        !this.state.traits.some(
                          trait => entity === `${trait.entity}:${trait.value}`
                        )
                    )
                    .map((entity, idx) => (
                      <DropdownItem
                        key={idx}
                        onClick={() => this.addTrait(entity)}
                      >
                        {entity}
                      </DropdownItem>
                    ))}
                </SplitButton>
              );
            }}
          </EntityContainer>
        </td>
      </Wrapper>
    );
  };

  render() {
    return (
      <div className="kata-train-form">
        <div
          className="kata-train-form__header"
          onMouseUp={!this.props.readOnly ? this.highlight : undefined}
        >
          <Annotated
            text={this.props.text}
            entities={[...this.state.entities, this.state.selected]}
          />
        </div>
        <div className="kata-train-form__body noselect">
          <div className="kata-train-form__title">Entities</div>
          {this.state.entities.map((item, index) => (
            <Wrapper key={index}>
              <td style={width200}>{this.renderEntity(item)}</td>
              <td className="kata-train-form__text" style={width150}>
                {this.props.text.substr(item.start, item.end - item.start)}
              </td>
              <td>{this.renderBelongsTo(item, index)}</td>
              <td style={width18}>
                {!this.props.readOnly && (
                  <i
                    className="icon-remove icon-middle kata-train-form__close"
                    onClick={() => this.removeEntity(item.tempId, index)}
                  />
                )}
              </td>
            </Wrapper>
          ))}
          {this.renderSelected()}
          {this.props.readOnly
            ? this.renderGroupedTraits()
            : this.renderTraits()}
          {!this.props.readOnly && this.renderNewTrait()}
          <div className="text-right mt-3">
            {!this.props.readOnly && (
              <Button
                color="primary"
                onClick={() =>
                  this.props.onSubmit({
                    sentence: this.props.text,
                    entities: this.state.entities,
                    traits: this.state.traits
                  })
                }
              >
                Train
              </Button>
            )}
            <Button className="ml-1" onClick={this.props.onCancel}>
              {this.props.cancelText}
            </Button>
          </div>
        </div>
      </div>
    );
  }
}
