import React, { Fragment, Component } from 'react';

import Annotated from './Annotated';

interface Props {
  text: string;
  entities: any[];
  traits: any[];
  className?: string;
  children?: any;
}

interface States {}

export default class ItemDisplay extends Component<Props, States> {
  renderGroupedTraits = () => {
    const traits = this.props.traits.reduce((obj, item) => {
      let arr = obj[item.entity];
      if (Array.isArray(arr)) {
        arr.push(item.value);
      } else {
        arr = [item.value];
      }
      return {
        ...obj,
        [item.entity]: arr
      };
    }, {});
    return Object.keys(traits).map((item, index) => (
      <div key={index} className="d-inline-block mr-2">
        <span className="mr-1">{item}</span>
        {traits[item] && traits[item].length
          ? traits[item].map((sub, subIndex) => (
              <Fragment key={subIndex}>
                <span className="label kata-train-form__label kata-train-form__label--dark font-weight-bold">
                  {sub}
                </span>{' '}
              </Fragment>
            ))
          : null}
      </div>
    ));
  };

  render() {
    return (
      <div className={`kata-train-item ${this.props.className || ''}`}>
        <div className="title">
          <Annotated text={this.props.text} entities={this.props.entities} />
        </div>
        <div className="mt-1">{this.renderGroupedTraits()}</div>
        {this.props.children && (
          <div className="kata-train-item__action">{this.props.children}</div>
        )}
      </div>
    );
  }
}
