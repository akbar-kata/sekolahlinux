import React from 'react';
import { connect } from 'react-redux';

import RootStore from 'interfaces/rootStore';
import { fetchTrainRequest } from 'stores/nlu/train/actions';
import {
  getTrainIndex,
  getTrainLoading,
  getTrainError,
  getTrainData,
  getTrainPage
} from 'stores/nlu/train/selectors';
import { getEntityLoading, getEntityDetail } from 'stores/nlu/entity/selectors';
import TrainList from './TrainList';
import { NluTrainDataMap, NluTrainPage } from 'interfaces/nlu/train';
import { getCurrentNlu, getNluLoadings } from 'stores/nlu/nlu/selectors';
import { getProjectSelected } from 'stores/project/selectors';

interface PropsFromState {
  nluId: string | null;
  selectedProject: string | null;
  nluIsLoading: boolean;
  loading: boolean;
  error: string | null;
  index: string[];
  page: NluTrainPage;
  data: NluTrainDataMap;
  getEntityDetail: Function;
}

interface PropsFromDispatch {
  fetchAction: Function;
}

interface Props extends PropsFromState, PropsFromDispatch {
  predictActive: boolean;
}

interface State {}

class TrainListContainer extends React.Component<Props, State> {
  state = {
    isOpen: false
  };

  componentDidMount() {
    if (this.props.nluId) {
      this.onFetch(this.props.nluId);
    }
  }

  componentWillReceiveProps(nextProps: Props) {
    // if (nextProps.nlu !== this.props.nlu) {
    //   this.onFetch(1);
    // }
  }

  onFetch = (nluId: string, page?: number) => {
    if (this.props.selectedProject) {
      this.props.fetchAction(this.props.selectedProject, nluId, page);
    }
  };

  render() {
    return !this.props.predictActive ? (
      <TrainList
        nluIsLoading={this.props.nluIsLoading}
        loading={this.props.loading}
        index={this.props.index}
        data={this.props.data}
        page={this.props.page}
        nluId={this.props.nluId}
        onFetch={this.onFetch}
        error={this.props.error}
        getEntityDetail={this.props.getEntityDetail}
      />
    ) : null;
  }
}

const mapStateToProps = ({
  nlu: { nlu, train, entity },
  project
}: RootStore): PropsFromState => {
  const currentNlu = getCurrentNlu(nlu);

  return {
    nluId: currentNlu ? currentNlu.id : null,
    selectedProject: getProjectSelected(project),
    nluIsLoading: getNluLoadings(nlu, 'fetch'),
    loading:
      getTrainLoading(train, 'fetch') || getEntityLoading(entity, 'fetch'),
    error: getTrainError(train, 'fetch'),
    index: getTrainIndex(train),
    data: getTrainData(train),
    page: getTrainPage(train),
    getEntityDetail: (name: string) => getEntityDetail(entity, name)
  };
};

const mapDispatchToProps: PropsFromDispatch = {
  fetchAction: fetchTrainRequest
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TrainListContainer);
