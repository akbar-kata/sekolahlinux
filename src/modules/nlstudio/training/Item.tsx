import React, { Component, Fragment } from 'react';
import shortid from 'shortid';

import { FloatingButton } from 'components/Button';
import { Tooltip, TooltipTarget } from 'components/Tooltip';
import Card from 'components/Card';
import ItemDisplay from './ItemDisplay';
import ItemForm from './ItemForm';
import { NluTrainData } from 'interfaces/nlu/train';

interface Props {
  noDisplay?: boolean;
  readOnly?: boolean;
  text: string;
  prediction: NluTrainData;
  cancelText?: string;
  type?: 'train' | 'log';
  getEntityDetail: Function;
  onSubmit?: Function;
  onCancel?: Function;
}

interface States {
  isEdit: boolean;
  entities: any[];
  traits: any[];
}

class Item extends Component<Props, States> {
  state = {
    isEdit: false,
    entities: [],
    traits: []
  };

  componentDidMount() {
    this.transformData(this.props.prediction);
  }

  componentWillReceiveProps(next: Props) {
    if (this.props.prediction !== next.prediction) {
      this.transformData(next.prediction);
    }
  }

  transformData = (prediction: any) => {
    const raw =
      prediction && prediction.entities && prediction.entities.length
        ? prediction.entities.map(item => {
            const detail = this.props.getEntityDetail(item.entity);
            let type = undefined;
            if (item && item.type) {
              type = item.type;
            } else if (detail && detail.type) {
              type = detail.type;
            }
            return {
              ...item,
              type,
              detail,
              tempId: shortid.generate()
            };
          })
        : [];
    const entities = raw
      .filter(item => item.type !== 'trait')
      .map(({ belongsTo, ...rest }) => {
        const newItem = { ...rest };
        if (belongsTo) {
          const arr = raw
            .filter(el => el.entity === (belongsTo.name || belongsTo.entity))
            .sort((a, b) => {
              if (!a) {
                return 1;
              }

              if (!b) {
                return -1;
              }

              return a.start - b.start;
            });
          const belongsToId =
            belongsTo.index === undefined || belongsTo.index === null
              ? belongsTo.id
              : belongsTo.index;
          newItem.belongsTo = arr[belongsToId]
            ? arr[belongsToId].tempId
            : undefined;
        }
        return newItem;
      })
      .sort((a, b) => {
        if (!a) {
          return 1;
        }

        if (!b) {
          return -1;
        }

        return a.start - b.start;
      });
    const traits = raw
      .filter(item => item.type === 'trait')
      .map(item => ({ ...item, label: item.value }));
    this.setState({
      entities,
      traits
    });
  };

  openEdit = () => {
    this.setState({
      isEdit: true
    });
  };

  onSubmit = ({ id, sentence, entities, traits }: any) => {
    const newEntities = (entities || []).map(
      ({ belongsTo, tempId, ...rest }) => {
        const newItem: any = { ...rest, id: tempId };
        if (belongsTo) {
          const owner =
            entities.filter(item => item.tempId === belongsTo)[0] || {};
          const foundId = entities
            .filter(item => item.entity === owner.entity)
            .sort((a, b) => {
              if (!a) {
                return 1;
              }

              if (!b) {
                return -1;
              }

              return a.start - b.start;
            })
            .findIndex(item => item.tempId === belongsTo);
          newItem.belongsTo = {
            // for that use {name, index}
            name: owner.entity,
            index: foundId,
            // for that use {entity, id}
            entity: owner.entiy,
            id: owner.tempId
          };
        }
        return newItem;
      }
    );
    const newTraits = traits || [];
    this.setState({ isEdit: false });
    if (this.props.onSubmit) {
      this.props.onSubmit({
        id,
        sentence,
        entities: newEntities,
        traits: newTraits
      });
    }
  };

  onCancel = () => {
    this.setState({ isEdit: false });
    if (this.props.onCancel) {
      this.props.onCancel();
    }
  };

  render() {
    const { type, text, prediction, noDisplay } = this.props;
    const { isEdit, entities, traits } = this.state;
    return (
      <Card noWrap className="mb-1" disabled={prediction.trained}>
        {(noDisplay || isEdit) && !prediction.trained ? (
          <ItemForm
            text={text}
            entities={entities}
            traits={traits}
            readOnly={this.props.readOnly}
            cancelText={this.props.cancelText}
            getEntityDetail={this.props.getEntityDetail}
            onSubmit={this.onSubmit}
            onCancel={this.onCancel}
          />
        ) : (
          <div onClick={type !== 'log' ? this.openEdit : undefined}>
            <ItemDisplay
              text={text}
              entities={entities}
              traits={traits}
              className={
                type !== 'log'
                  ? 'kata-train-item--training'
                  : 'kata-train-item--log'
              }
            >
              {type !== 'log' ? (
                <i className="icon-arrow icon-middle kata-train-item__action-icon" />
              ) : prediction.trained ? (
                <i className="icon-train icon-middle kata-train-item__action-icon text-success" />
              ) : (
                <Fragment>
                  <TooltipTarget
                    placement="top"
                    component={<Tooltip>Edit</Tooltip>}
                  >
                    <FloatingButton
                      className="mr-1"
                      icon="training"
                      onClick={this.openEdit}
                    />
                  </TooltipTarget>
                  <TooltipTarget
                    placement="top"
                    component={<Tooltip>Submit</Tooltip>}
                  >
                    <FloatingButton
                      icon="train"
                      color="success"
                      onClick={() =>
                        this.onSubmit({ entities, traits, sentence: text })
                      }
                    />
                  </TooltipTarget>
                </Fragment>
              )}
            </ItemDisplay>
          </div>
        )}
      </Card>
    );
  }
}

export default Item;
