import React, { Component, Fragment } from 'react';

import TrainInput from './TrainInput';
import Item from './Item';

interface Props {
  text: string;
  predictActive: boolean;
  prediction: any;
  predictLoading: boolean;
  trainLoading: boolean;
  onPredict: (text: string) => void;
  onRemovePredict: () => void;
  onTrain: (data: any) => void;
  getEntityDetail: Function;
}

interface States {}

export default class PredictBox extends Component<Props, States> {
  render() {
    return (
      <Fragment>
        <TrainInput
          loading={this.props.predictLoading}
          onSubmit={this.props.onPredict}
        />
        {this.props.predictActive && (
          <Item
            text={this.props.text}
            prediction={this.props.prediction}
            cancelText="Remove"
            getEntityDetail={this.props.getEntityDetail}
            onSubmit={this.props.onTrain}
            onCancel={this.props.onRemovePredict}
            noDisplay
          />
        )}
      </Fragment>
    );
  }
}
