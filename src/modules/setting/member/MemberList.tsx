import React from 'react';

import * as Member from 'interfaces/member';
import MemberItem from './MemberItem';
import { EmptyMessage } from 'components/Common';

interface Props {
  isLoading?: boolean;
  error?: string | null;
  index: string[];
  data: Member.DataMap;
  removeAction: Function;
}

const LoadingList = (
  <tr>
    <td colSpan={5} className="text-center">
      <h5 className="pa4 text-bold">Loading...</h5>
    </td>
  </tr>
);

const EmptyList = (
  <tr>
    <td colSpan={5} className="text-center">
      <EmptyMessage title="Team is empty">
        You don’t have any member in this team. Add members by clicking the
        floating button below.
      </EmptyMessage>
    </td>
  </tr>
);

const MemberList: React.SFC<Props> = ({
  isLoading,
  error,
  index,
  data,
  removeAction
}) => {
  return (
    <div className="kata-member__table">
      <table className="kata-table table-hover table-striped">
        <thead>
          <tr>
            <th>Username</th>
            <th>Role</th>
            {/* <th>Actions</th> */}
          </tr>
        </thead>
        <tbody>
          {(isLoading && LoadingList) ||
            ((index.length === 0 && EmptyList) ||
              index.map(id => (
                <MemberItem
                  key={id}
                  userId={id}
                  username={data[id].username}
                  roleName={data[id].roleName}
                  removeAction={removeAction(id, data[id].username)}
                />
              )))}
        </tbody>
      </table>
    </div>
  );
};

export default MemberList;
