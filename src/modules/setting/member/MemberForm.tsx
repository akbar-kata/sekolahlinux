import React, { Component } from 'react';

import { JsonObject } from 'interfaces';

import {
  Drawer,
  DrawerHeader,
  DrawerBody,
  DrawerFooter
} from 'components/Drawer';
import { Button } from 'components/Button';

import MemberSelector from './MemberSelector';
import { randomizeAvatarWithSeed } from 'utils/avatars';

import { Formik, Form } from 'formik';
import * as yup from 'yup';
import { ReactSelectAsync } from 'components/FormikWrapper';
import debounce from 'debounce-promise';

interface Props {
  isOpen: boolean;
  type: string;
  id?: string;
  data?: JsonObject;
  onCancel: Function;
  onCloseDrawer(): void;
  onSubmit(data: any);
}

interface States {
  username: string;
  userId: string;
  loading: boolean;
}

class MemberForm extends Component<Props, States> {
  validationSchema = yup.lazy((values: any) => {
    return yup.object().shape({
      username: yup.string().required('Team name is required')
    });
  });

  constructor(props: Props) {
    super(props);
    this.state = {
      loading: false,
      username: '',
      userId: ''
    };
  }

  onClose = () => {
    this.props.onCancel();
    this.setState({
      loading: true,
      username: '',
      userId: ''
    });
  };

  onSubmit = values => {
    if (values.data) {
      const data = {
        username: values.data.label,
        userId: values.data.value
      };
      this.props.onSubmit(data);
    }
  };

  innerForm = formApi => (
    <Form className="kata-form full-size">
      <DrawerHeader title="Invite Member" />
      <DrawerBody>
        <div className="kata-form__element">
          <label className="kata-form__label">Username</label>
          <MemberSelector>
            {params => {
              return params.loading ? (
                <div>Loading...</div>
              ) : params.error ? (
                <div>{params.error}</div>
              ) : (
                <ReactSelectAsync
                  name="username"
                  options={params.options}
                  loadOptions={debounce(params.search, 500, {
                    leading: true
                  })}
                  onChange={values => {
                    if (values) {
                      formApi.setFieldValue('data', values);
                    }
                  }}
                  optionComponent={GravatarOption}
                  valueComponent={GravatarValue}
                />
              );
            }}
          </MemberSelector>
        </div>
      </DrawerBody>
      <DrawerFooter>
        <Button color="primary" type="submit" className="mr-1">
          {this.props.type}
        </Button>
        <Button onClick={this.onClose}>Cancel</Button>
      </DrawerFooter>
    </Form>
  );

  render() {
    const defaultVal = { username: this.state.username };
    return (
      <Drawer isOpen={this.props.isOpen} onClose={this.props.onCloseDrawer}>
        <Formik
          onSubmit={this.onSubmit}
          initialValues={defaultVal}
          validationSchema={this.validationSchema}
        >
          {this.innerForm}
        </Formik>
      </Drawer>
    );
  }
}

class GravatarOption extends React.PureComponent<any, any> {
  handleMouseDown = (event: any) => {
    event.preventDefault();
    event.stopPropagation();
    this.props.onSelect(this.props.option, event);
  };

  handleMouseEnter = (event: any) => {
    this.props.onFocus(this.props.option, event);
  };

  handleMouseMove = (event: any) => {
    if (this.props.isFocused) {
      return;
    }
    this.props.onFocus(this.props.option, event);
  };

  render() {
    return (
      <div
        className={this.props.className}
        onMouseDown={this.handleMouseDown}
        onMouseEnter={this.handleMouseEnter}
        onMouseMove={this.handleMouseMove}
        title={this.props.option.label}
      >
        <img
          src={randomizeAvatarWithSeed(this.props.option.label)}
          width={15}
          alt=""
          className="mr-2"
        />
        {this.props.children}
      </div>
    );
  }
}

class GravatarValue extends React.PureComponent<any, any> {
  render() {
    return (
      <div className="Select-value" title={this.props.value.label}>
        <span className="Select-value-label">
          <img
            src={randomizeAvatarWithSeed(this.props.value.label)}
            width={15}
            alt=""
            className="mr-2"
          />
          {this.props.children}
        </span>
      </div>
    );
  }
}

export default MemberForm;
