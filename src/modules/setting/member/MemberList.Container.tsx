import React, { Fragment } from 'react';
import { connect } from 'react-redux';

import RootStore from 'interfaces/rootStore';
import {
  fetchMemberRequest,
  updateMemberRequest,
  removeMemberRequest
} from 'stores/user/member/actions';
import { getMemberData, getMemberIndex } from 'stores/user/member/selectors';
import { openModal, closeModal } from 'stores/app/modal/actions';
import { isModalOpen, getModalData } from 'stores/app/modal/selectors';
import MemberList from './MemberList';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import { searchUserRequest } from 'stores/user/check/actions';
import { confirm } from 'components/Modal';

interface PropsFromState {
  index: string[];
  data: any;
  isUpdateOpen: boolean;
  dataToUpdate: any;
}

interface PropsFromDispatch {
  fetchAction: typeof fetchMemberRequest;
  checkAction: typeof searchUserRequest;
  editAction: (id: string, data: any) => ReturnType<typeof updateMemberRequest>;
  removeAction: (data: any) => ReturnType<typeof removeMemberRequest>;
  openUpdate: (data: any) => ReturnType<typeof openModal>;
  closeUpdate: () => ReturnType<typeof closeModal>;
}

interface RouteParams {
  teamId: string;
  username: string;
}

interface OwnProps extends RouteComponentProps<RouteParams> {}

type Props = PropsFromState & PropsFromDispatch & OwnProps;

interface State {}

const updateModalId = 'MemberUpdate';

class MemberListContainer extends React.Component<Props, State> {
  componentDidMount() {
    this.props.fetchAction(this.props.match.params.teamId);
  }

  public openEdit = (selected: any) => () => {
    this.props.openUpdate(selected);
  };

  public checkUser = (username: any) => () => {
    this.props.checkAction(username);
  };

  public closeEdit = () => {
    this.props.closeUpdate();
  };

  onUpdate = data => {
    this.props.editAction(data.userId, data);
  };

  removeMember = (userId: string, username: string) => {
    const data = {
      userId,
      teamId: this.props.match.params.teamId,
      teamname: this.props.match.params.username
    };
    return () => {
      confirm({
        title: 'Delete Member',
        message: `Are you sure want to delete ${username}`,
        okLabel: 'Delete',
        cancelLabel: 'Cancel'
      }).then(res => {
        if (res) {
          this.props.removeAction(data);
        }
      });
    };
  };

  render() {
    return (
      <Fragment>
        <MemberList
          index={this.props.index}
          data={this.props.data}
          removeAction={this.removeMember}
        />
      </Fragment>
    );
  }
}

const mapStateToProps = ({ member, app: { modal } }: RootStore) => {
  return {
    index: getMemberIndex(member),
    data: getMemberData(member),
    isUpdateOpen: isModalOpen(modal, updateModalId),
    dataToUpdate: getModalData(modal, updateModalId)
  };
};

const mapDispatchToProps = (dispatch: Function): PropsFromDispatch => {
  return {
    fetchAction: (teamId: string) => dispatch(fetchMemberRequest(teamId)),
    checkAction: (username: string) => dispatch(searchUserRequest(username)),
    editAction: (id: string, data: any) =>
      dispatch(updateMemberRequest(id, data)),
    removeAction: (data: any) => dispatch(removeMemberRequest(data)),
    openUpdate: (data: any) => dispatch(openModal(updateModalId, data)),
    closeUpdate: () => dispatch(closeModal(updateModalId))
  };
};

export default withRouter(
  connect<PropsFromState, PropsFromDispatch, OwnProps, RootStore>(
    mapStateToProps,
    mapDispatchToProps
  )(MemberListContainer)
);
