import React from 'react';
import { connect } from 'react-redux';

import RootStore from 'interfaces/rootStore';

import {
  getDataUserCheck,
  getCheckUserLoading,
  getCheckUserError
} from 'stores/user/check/selectors';
import { searchUserRequest } from 'stores/user/check/actions';

interface PropsFromState {
  data: any[];
  error: string | null;
  loading: boolean;
  options: any[];
}

interface PropsFromDispatch {
  search: Function;
}

interface Props extends PropsFromState, PropsFromDispatch {
  children: (props: any) => React.ReactElement<any>;
}

interface State {}

class MemberSelector extends React.Component<Props, State> {
  render() {
    const { children, ...rest } = this.props;
    return children(rest);
  }
}
const mapStateToProps = ({ check }: RootStore): PropsFromState => {
  const data: any = getDataUserCheck(check);
  return {
    data,
    loading: getCheckUserLoading(check, 'fetch'),
    error: getCheckUserError(check, 'fetch'),
    options: data
      ? Object.values(data).map((user: any) => ({
          label: user.username,
          value: user.username
        }))
      : []
  };
};

const mapDispatchToProps: PropsFromDispatch = {
  search: (keyword: string) => searchUserRequest(keyword)
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MemberSelector);
