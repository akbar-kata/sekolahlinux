import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { withRouter, RouteComponentProps } from 'react-router-dom';

import RootStore from 'interfaces/rootStore';

import { addMemberRequest } from 'stores/user/member/actions';
import { openDrawer, closeDrawer } from 'stores/app/drawer/actions';
import { isDrawerOpen } from 'stores/app/drawer/selectors';

import { Button } from 'components/Button';

import MemberForm from './MemberForm';

interface PropsFromState {
  error: string;
  isOpen: boolean;
}

interface PropsFromDispatch {
  createAction: Function;
  openDrawer: Function;
  closeDrawer: Function;
}

interface Props extends PropsFromState, PropsFromDispatch, RouteComponentProps {
  match: any;
  data: any;
  closeDrawer(): void;
}

interface State {}

const drawerId = 'MemberCreate';

class MemberCreate extends React.Component<Props, State> {
  onCreate = (data: any) => {
    if (data) {
      data.teamId = this.props.match.params.teamId;
      data.teamname = this.props.match.params.username;
      this.props.createAction(data);
    }
  };

  openAdd = () => {
    this.props.openDrawer();
  };

  render() {
    return (
      <Fragment>
        <Button
          className="kata-member-btn-invite__create-button"
          color="primary"
          onClick={this.openAdd}
        >
          <i className="icon-add" /> <span>Invite Member</span>
        </Button>
        <MemberForm
          type="Invite"
          isOpen={this.props.isOpen}
          onCancel={this.props.closeDrawer}
          onCloseDrawer={this.props.closeDrawer}
          onSubmit={this.onCreate}
          data={this.props.data}
        />
      </Fragment>
    );
  }
}

const mapStateToProps = ({ member, check, app: { drawer } }: RootStore) => {
  return {
    isOpen: isDrawerOpen(drawer, drawerId),
    data: check.data,
    error: ''
  };
};

const mapDispatchToProps = (dispatch: Function): PropsFromDispatch => {
  return {
    createAction: (data: any) => dispatch(addMemberRequest(data)),
    openDrawer: () => dispatch(openDrawer(drawerId)),
    closeDrawer: () => dispatch(closeDrawer(drawerId))
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(MemberCreate)
);
