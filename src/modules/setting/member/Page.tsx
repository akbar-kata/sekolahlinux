import React, { Fragment } from 'react';

import MemberList from './MemberList.Container';
import MemberCreate from './MemberCreate.Container';
import { Dashboard } from 'components/Dashboard';
import { Link } from 'react-router-dom';
import './style.scss';

export default props => (
  <div className="pl-2 pr-2">
    <Dashboard
      className="kata-member__member-container"
      headerContent={
        <Fragment>
          <Link
            className="btn kata-btn-float kata-btn-float__primary text-small mr-2 float-left d-flex"
            to={`/setting/teams`}
          >
            <i className="icon-arrow-left" />
          </Link>
          <div className="float-left">
            <h1 className="heading1 m-0">{`${
              props.match.params.username
            } Team`}</h1>
            <Link to={`/setting/teams`} className="text-muted">
              Teams
            </Link>{' '}
            /{' '}
            <span className="text-primary">{`${
              props.match.params.username
            } Team`}</span>
          </div>
          <MemberCreate />
        </Fragment>
      }
      title={'Team Name'}
    >
      <MemberList />
    </Dashboard>
  </div>
);
