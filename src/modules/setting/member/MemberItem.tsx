import React from 'react';

import * as Member from 'interfaces/member';
// import { Button } from 'components/Button';

interface Props extends Partial<Member.Data> {
  userId: string;
  removeAction: any;
}

const MemberItem: React.SFC<Props> = ({
  userId,
  username,
  roleName,
  removeAction
}) => {
  return (
    <tr>
      <td>{username}</td>
      <td>{roleName}</td>
      {/* <td>
        {roleName !== 'teamadmin' && (
          <Button color="danger" onClick={removeAction}>
            <i className="text-white icon-trash" /> <span>Delete</span>
          </Button>
        )}
      </td> */}
    </tr>
  );
};

export default MemberItem;
