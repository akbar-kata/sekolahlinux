import React, { Fragment } from 'react';
import { connect } from 'react-redux';

import RootStore from 'interfaces/rootStore';
import { addTeamRequest } from 'stores/user/team/actions';
import { openDrawer, closeDrawer } from 'stores/app/drawer/actions';
import { isDrawerOpen } from 'stores/app/drawer/selectors';
import TeamForm from './TeamForm';
import Card from 'components/Card';
import CardButton from 'components/Card/CardButton';

// interface Props {

// }

interface PropsFromState {
  error: string;
  isOpen: boolean;
  userId?: string;
}

interface PropsFromDispatch {
  createAction: Function;
  openDrawer: Function;
  closeDrawer: Function;
}
interface Props extends PropsFromState, PropsFromDispatch {
  closeDrawer(): void;
}

interface State {
  // isOpen: boolean;
}

const drawerId = 'TeamCreate';

class TeamCreate extends React.Component<Props, State> {
  onCreate = data => {
    const team = { username: data.username, userId: this.props.userId };
    this.props.createAction(team);
  };

  render() {
    return (
      <Fragment>
        <Card
          asButton
          className="kata-team__add-btn"
          onClick={this.props.openDrawer}
        >
          <CardButton label={'Create Team'} icon="add" />
        </Card>
        <TeamForm
          type="create"
          isOpen={this.props.isOpen}
          onCancel={this.props.closeDrawer}
          onSubmit={this.onCreate}
          onCloseDrawer={this.props.closeDrawer}
        />
      </Fragment>
    );
  }
}

const mapStateToProps = ({
  auth,
  app: { drawer }
}: RootStore): PropsFromState => {
  return {
    isOpen: isDrawerOpen(drawer, drawerId),
    error: '',
    userId: auth.token ? auth.token.detail.id : null
  };
};

const mapDispatchToProps = (dispatch: Function): PropsFromDispatch => {
  return {
    createAction: (data: any) => dispatch(addTeamRequest(data)),
    openDrawer: () => dispatch(openDrawer(drawerId)),
    closeDrawer: () => dispatch(closeDrawer(drawerId))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TeamCreate);
