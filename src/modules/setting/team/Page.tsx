import React from 'react';

import { Dashboard } from 'components/Dashboard';
import TeamList from './TeamList.Container';
import './style.scss';

export default () => (
  <Dashboard title={'Team'} tooltip="Manage your team members here">
    <TeamList />
  </Dashboard>
);
