import React, { Component } from 'react';

import { JsonObject } from 'interfaces';
import { Button } from 'components/Button';
import {
  Drawer,
  DrawerBody,
  DrawerHeader,
  DrawerFooter
} from 'components/Drawer';

import { Formik, Form } from 'formik';
import * as yup from 'yup';
import { Field } from 'components/FormikWrapper';

interface Props {
  isOpen: boolean;
  type: string;
  id?: string;
  data?: JsonObject;
  onCancel: Function;
  onCloseDrawer(): void;
  onSubmit(data: any);
}

interface States {}

class TeamForm extends Component<Props, States> {
  validationSchema = yup.lazy((values: any) => {
    return yup.object().shape({
      username: yup
        .string()
        .required('Team name is required')
        .matches(/^[a-z0-9-_]+$/, {
          message:
            'Team name can only contains lowercase character, number, dash, and underscore'
        })
    });
  });

  onClose = () => {
    this.props.onCancel();
  };

  innerForm = formApi => (
    <Form className="kata-form full-size">
      <DrawerHeader title="Create Team" />
      <DrawerBody>
        <div className="kata-form__element">
          <label className="kata-form__label">Team Name</label>
          <Field
            name="username"
            placeholder="Team Name"
            className="form-control kata-form__input-text"
          />
        </div>
      </DrawerBody>
      <DrawerFooter>
        <Button color="primary" type="submit" className="mr-1 text-capitalize">
          <i
            className={`mr2 icon-${
              this.props.type === 'create' ? 'plus' : 'check'
            }`}
          />{' '}
          {this.props.type}
        </Button>
        <Button onClick={this.onClose}>Cancel</Button>
      </DrawerFooter>
    </Form>
  );

  render() {
    return (
      <Drawer isOpen={this.props.isOpen} onClose={this.props.onCloseDrawer}>
        <Formik
          onSubmit={this.props.onSubmit}
          initialValues={null}
          validationSchema={this.validationSchema}
        >
          {this.innerForm}
        </Formik>
      </Drawer>
    );
  }
}

export default TeamForm;
