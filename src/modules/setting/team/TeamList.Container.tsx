import React from 'react';
import { connect } from 'react-redux';

import RootStore from 'interfaces/rootStore';
import {
  fetchTeamRequest,
  updateTeamRequest,
  removeTeamRequest
} from 'stores/user/team/actions';
import {
  getTeamIndex,
  getTeamLoading,
  getTeamError,
  getTeamData
} from 'stores/user/team/selectors';
import { getAuthToken } from 'stores/user/auth/selectors';
import { openDrawer, closeDrawer } from 'stores/app/drawer/actions';
import { isDrawerOpen, getDrawerData } from 'stores/app/drawer/selectors';
import TeamList from './TeamList';
// import TeamForm from './TeamForm';

interface PropsFromState {
  isLoading: boolean;
  error: string | null;
  index: string[];
  data: any;
  username: any;
  isUpdateOpen: boolean;
  dataToUpdate: any;
}

interface PropsFromDispatch {
  fetchAction: Function;
  editAction: Function;
  removeAction: Function;
  openUpdate: Function;
  closeUpdate: Function;
}

interface Props extends PropsFromState, PropsFromDispatch {}

interface State {}

const updateDrawerId = 'TeamUpdate';

class TeamListContainer extends React.Component<Props, State> {
  componentDidMount() {
    this.props.fetchAction(this.props.username);
  }

  public openEdit = (selected: any) => () => {
    this.props.openUpdate(selected);
  };

  public closeEdit = () => {
    this.props.closeUpdate();
  };

  onUpdate = data => {
    this.props.editAction(data.id, data);
  };

  render() {
    return (
      <>
        {/* <TeamForm
            type="update"
            isOpen={this.props.isUpdateOpen}
            data={this.props.dataToUpdate}
            onCancel={this.closeEdit}
            onSubmit={this.onUpdate}
          /> */}
        <TeamList
          isLoading={this.props.isLoading}
          index={this.props.index}
          data={this.props.data}
          editAction={this.openEdit}
          onRemove={this.props.removeAction}
        />
      </>
    );
  }
}

const mapStateToProps = ({
  team,
  auth,
  app: { drawer }
}: RootStore): PropsFromState => {
  const token: any = getAuthToken(auth) || {};

  return {
    isLoading: getTeamLoading(team, 'fetch'),
    error: getTeamError(team, 'fetch'),
    index: getTeamIndex(team),
    username: token.detail.username,
    data: getTeamData(team),
    isUpdateOpen: isDrawerOpen(drawer, updateDrawerId),
    dataToUpdate: getDrawerData(drawer, updateDrawerId)
  };
};

const mapDispatchToProps = (dispatch: Function): PropsFromDispatch => {
  return {
    fetchAction: (username: string) => dispatch(fetchTeamRequest(username)),
    editAction: (id: string, data: any) =>
      dispatch(updateTeamRequest(id, data)),
    removeAction: (id: string, name: string) =>
      dispatch(removeTeamRequest(id, name)),
    openUpdate: (data: any) => dispatch(openDrawer(updateDrawerId, data)),
    closeUpdate: () => dispatch(closeDrawer(updateDrawerId))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TeamListContainer);
