import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';

import * as Team from 'interfaces/team';
import TeamCreate from './TeamCreate.Container';
import Card from 'components/Card';
import { SupportButton } from 'components/Button';
import { DashboardCards } from 'components/Dashboard';
import { confirm } from 'components/Modal';

interface Props {
  isLoading: boolean;
  index: string[];
  data: Team.DataMap;
  editAction: Function;
  onRemove: Function;
}

class TeamList extends React.Component<Props> {
  onRemove = (id: string, name: string) => {
    return () => {
      confirm({
        title: 'Delete Team',
        message: `Are you sure want to delete ${name}`,
        okLabel: 'Delete',
        cancelLabel: 'Cancel'
      }).then(res => {
        if (res) {
          this.props.onRemove(id, name);
        }
      });
    };
  };
  render() {
    const { isLoading, index, data } = this.props;
    return (
      <>
        <DashboardCards>
          <TeamCreate />
          {!isLoading &&
            index.map(id => (
              <Fragment key={id}>
                <Card title={data[id].username} className="kata-team__card">
                  <div className="kata-team__body">
                    <div className="kata-team__member-container">
                      <label className="kata-form__label">Members</label>
                      <p>{data[id].members}</p>
                    </div>
                    <div className="kata-team__bot-container">
                      <label className="kata-form__label">Projects</label>
                      <p>{data[id].bots}</p>
                    </div>
                  </div>
                  <Link
                    to={`/setting/teams/${data[id].username}/members/${id}`}
                    className="kata-team__btn-link"
                  >
                    <SupportButton size="sm">Manage Team</SupportButton>
                  </Link>
                </Card>
              </Fragment>
            ))}
        </DashboardCards>
      </>
    );
  }
}

export default TeamList;
