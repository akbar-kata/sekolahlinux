import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';

import { Robot } from 'components/Loading';

const Members = React.lazy(() => import('./member'));
const Teams = React.lazy(() => import('./team'));
const Account = React.lazy(() => import('./account'));

const UsageDummy: React.SFC = () => <div>UsageDummy component</div>;
const BillingDummy: React.SFC = () => <div>BillingDummy component</div>;

const parentPath = 'setting';

export default () => {
  return (
    <React.Suspense fallback={<Robot />}>
      <Switch>
        <Route path={`/${parentPath}/account`} component={Account} />
        <Route exact path={`/${parentPath}/teams`} component={Teams} />
        <Route
          path={`/${parentPath}/teams/:username/members/:teamId`}
          component={Members}
        />
        <Route path={`/${parentPath}/usage`} component={UsageDummy} />
        <Route path={`/${parentPath}/billing`} component={BillingDummy} />
        <Route render={() => <Redirect to={`/${parentPath}/account`} />} />
      </Switch>
    </React.Suspense>
  );
};
