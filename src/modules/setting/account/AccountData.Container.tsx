import React, { Fragment } from 'react';
import { connect } from 'react-redux';

import RootStore from 'interfaces/rootStore';
import { Bag } from 'interfaces/account';
import { Data as User } from 'interfaces/user';

import { FloatingButton, Button } from 'components/Button';
import { Board } from 'components/Board';
import {
  requestUpdateEmail,
  requestUpdatePassword
} from 'stores/user/account/actions';
import { Circle } from 'components/Loading';

import { Formik, Form } from 'formik';
import * as yup from 'yup';
import { Field } from 'components/FormikWrapper';

interface PropsFromDispatch {
  updateEmail: Function;
  updatePassword: Function;
}

interface PropsFromStore {
  currentEmail: string;
  currentType: string;
  currentName: string;
  errorBags?: Bag;
  messageBags?: Bag;
  credentials: User;
  isLoading: boolean;
}

interface State {
  isLoading: boolean;
  isEmailOpen: boolean;
  isPasswordOpen: boolean;
  newPassword: string;
  oldPassword: string;
  confirmPassword: string;
  showOldPassword: boolean;
  showNewPassword: boolean;
  showConfirmPassword: boolean;
}

interface PropsFromDispatch {}

interface Props extends PropsFromStore, PropsFromDispatch {}

class AccountData extends React.Component<Props, State> {
  static defaultProps = {
    isLoading: false
  };

  validationSchema = yup.lazy((values: any) => {
    const { oldPassword, newPassword, confirmPassword } = values;
    const { isPasswordOpen } = this.state;
    this.setState({ oldPassword, newPassword, confirmPassword });
    return yup.object().shape({
      email: yup
        .string()
        .required('An email is required')
        .email('Email format is not valid'),
      oldPassword: isPasswordOpen
        ? yup.string().required('Please type your old password.')
        : yup.mixed(),
      newPassword: isPasswordOpen
        ? yup
            .string()
            .required('Please type a new password.')
            .min(8, 'New password must be longer than 8 characters.')
        : yup.mixed(),
      confirmPassword: isPasswordOpen
        ? yup
            .string()
            .required('Please type your password again')
            .test(
              'is-password',
              'Passwords dont match',
              value => value === values.newPassword
            )
        : yup.mixed()
    });
  });

  constructor(props: Props) {
    super(props);
    this.state = {
      isLoading: props.isLoading,
      isEmailOpen: false,
      isPasswordOpen: false,
      newPassword: '',
      oldPassword: '',
      confirmPassword: '',
      showOldPassword: true,
      showNewPassword: true,
      showConfirmPassword: true
    };
  }

  static getDerivedStateFromProps(props: Props) {
    return {
      isLoading: props.isLoading
    };
  }

  onClickEmail = () => {
    this.setState({ isEmailOpen: true, isPasswordOpen: false });
  };

  closeEmailField = () => {
    this.setState({ isEmailOpen: false });
  };

  onClickPassword = () => {
    this.setState({ isPasswordOpen: true, isEmailOpen: false });
  };

  onClose = () => {
    this.setState({ isPasswordOpen: false, isEmailOpen: false });
  };

  closePasswordField = () => {
    this.setState({ isPasswordOpen: false });
  };

  showOldPassword = () => {
    this.setState({ showOldPassword: !this.state.showOldPassword });
  };

  showNewPassword = () => {
    this.setState({ showNewPassword: !this.state.showNewPassword });
  };

  showConfirmPassword = () => {
    this.setState({ showConfirmPassword: !this.state.showConfirmPassword });
  };

  onSubmit = values => {
    const { isPasswordOpen, isLoading } = this.state;
    if (isPasswordOpen) {
      this.props.updatePassword(
        values.oldPassword,
        values.newPassword,
        this.props.credentials
      );
    } else {
      this.props.updateEmail(values.email, this.props.credentials);
    }
    if (!isLoading) {
      setTimeout(() => {
        this.onClose();
      }, 2000);
    }
  };

  renderFooter = formApi => (
    <Fragment>
      <div className="kata-settings__footer-action">
        {this.state.isLoading && <Circle className="mr-1" size={30} />}
        <Button
          color="primary"
          className="mr-1"
          type="submit"
          disabled={this.state.isLoading ? true : false}
        >
          Update
        </Button>
        <Button
          color="white"
          className="mr-1"
          onClick={() => {
            formApi.resetForm();
            this.onClose();
          }}
        >
          Cancel
        </Button>
      </div>
    </Fragment>
  );

  innerForm = formApi => {
    const { isPasswordOpen, isEmailOpen } = this.state;
    return (
      <Form>
        <Board
          footerChildren={
            isEmailOpen || isPasswordOpen
              ? this.renderFooter(formApi)
              : undefined
          }
        >
          <div className="kata-info__container">
            <div className="kata-info__label">Username</div>
            <div className="kata-info__content">
              <Field
                name="username"
                placeholder="username"
                className="form-control kata-form__input-text"
                disabled
              />
            </div>
          </div>
          <div className="kata-info__container">
            <div className="kata-info__label">Account Type</div>
            <div className="kata-info__content">
              <Field
                name="type"
                className="form-control kata-form__input-text"
                disabled
              />
            </div>
          </div>
          <div className="kata-info__container">
            <div className="kata-info__label">Email</div>
            <div className="kata-info__content kata-settings__wrapper-email">
              {!isEmailOpen ? (
                <Fragment>
                  <Field
                    name="email"
                    className="form-control kata-form__input-text kata-settings__input--text"
                    disabled
                  />
                  <FloatingButton
                    icon="edit"
                    color="primary"
                    className="kata-settings__edit-email"
                    onClick={this.onClickEmail}
                  />
                </Fragment>
              ) : (
                <Fragment>
                  <Field
                    autoFocus
                    name="email"
                    placeholder="Email"
                    className="form-control kata-form__input-text kata-settings__input--text"
                  />
                  <FloatingButton
                    icon="close"
                    color="danger"
                    className="kata-settings__edit-email"
                    onClick={() => {
                      formApi.resetForm();
                      this.closeEmailField();
                    }}
                  />
                </Fragment>
              )}
            </div>
          </div>
          {isPasswordOpen && (
            <Fragment>
              <div className="kata-settings__wrapper-password">
                <div className="kata-info__container">
                  <div className="kata-info__label">Old Password</div>
                  <div className="kata-info__content kata-settings__wrapper-password">
                    <Field
                      className="kata-form__input-text kata-settings__input--text"
                      name="oldPassword"
                      type={this.state.showOldPassword ? 'password' : 'text'}
                      placeholder="Old Password"
                      disabled={this.props.isLoading ? true : false}
                    />
                    <i
                      className="icon-view text-primary kata-settings__icon-view"
                      onClick={this.showOldPassword}
                    />
                  </div>
                </div>
                <div className="kata-info__container">
                  <div className="kata-info__label">New Password</div>
                  <div className="kata-info__content kata-settings__wrapper-password">
                    <Field
                      className="kata-form__input-text kata-settings__input--text"
                      name="newPassword"
                      type={this.state.showNewPassword ? 'password' : 'text'}
                      placeholder="New Password"
                      disabled={this.props.isLoading ? true : false}
                    />
                    <i
                      className="icon-view text-primary kata-settings__icon-view"
                      onClick={this.showNewPassword}
                    />
                  </div>
                </div>
                <div className="kata-info__container">
                  <div className="kata-info__label">Confirm Password</div>
                  <div className="kata-info__content kata-settings__wrapper-password">
                    <Field
                      className="kata-form__input-text kata-settings__input--text"
                      name="confirmPassword"
                      type={
                        this.state.showConfirmPassword ? 'password' : 'text'
                      }
                      placeholder="Confirmation Password"
                      disabled={this.props.isLoading ? true : false}
                    />
                    <i
                      className="icon-view text-primary kata-settings__icon-view"
                      onClick={this.showConfirmPassword}
                    />
                  </div>
                </div>
                <FloatingButton
                  icon="close"
                  color="danger"
                  className="kata-settings__ok-password"
                  onClick={() => {
                    formApi.resetForm();
                    this.closePasswordField();
                  }}
                />
              </div>
            </Fragment>
          )}
          {!isPasswordOpen && (
            <div className="kata-info__container">
              <div className="kata-info__label">Password</div>
              <div className="kata-info__content kata-settings__dots--container">
                <div className="kata-settings__dots mt-2 mr-1" />
                <div className="kata-settings__dots mt-2 mr-1" />
                <div className="kata-settings__dots mt-2 mr-1" />
                <div className="kata-settings__dots mt-2 mr-1" />
                <div className="kata-settings__dots mt-2 mr-1" />
                <div className="kata-settings__dots mt-2 mr-1" />
                <FloatingButton
                  icon="edit"
                  color="primary"
                  className="kata-settings__edit-password"
                  onClick={this.onClickPassword}
                />
              </div>
            </div>
          )}
        </Board>
      </Form>
    );
  };

  render() {
    const credentials = this.props.credentials;
    return (
      <Fragment>
        <div className="kata-settings__wrapper">
          <div className="kata-settings__wrapper-inner">
            <Formik
              onSubmit={this.onSubmit}
              initialValues={{
                ...credentials,
                oldPassword: '',
                newPassword: '',
                confirmPassword: ''
              }}
              validationSchema={this.validationSchema}
            >
              {this.innerForm}
            </Formik>
          </div>
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = (state: RootStore) => {
  return {
    isLoading: state.account.isLoading,
    credentials: state.auth.token ? state.auth.token.detail : null,
    currentEmail:
      state.auth.token && state.auth.token.detail.email
        ? state.auth.token.detail.email
        : '(not set)',
    currentType:
      state.auth.token && state.auth.token.detail.type
        ? state.auth.token.detail.type
        : '(not set)',
    currentName:
      state.auth.token && state.auth.token.detail.username
        ? state.auth.token.detail.username
        : '(not set)'
  };
};

const mapDispatchToProps = (dispatch: Function): PropsFromDispatch => {
  return {
    updateEmail: (email, credentials) =>
      dispatch(requestUpdateEmail(email, credentials)),
    updatePassword: (oldPassword, newPassword, credentials) =>
      dispatch(requestUpdatePassword(oldPassword, newPassword, credentials))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AccountData);
