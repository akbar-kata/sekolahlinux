import React, { Fragment } from 'react';
import AccountData from './AccountData.Container';
import { Dashboard } from 'components/Dashboard';
import './style.scss';

interface Props {}

export default class Page extends React.Component<Props> {
  constructor(props: Props) {
    super(props);
  }
  render() {
    return (
      <Fragment>
        <div className="kata-settings__container">
          <Dashboard
            title="Account"
            tooltip="Manage your account info."
            isSettings
          >
            <AccountData />
          </Dashboard>
        </div>
      </Fragment>
    );
  }
}
