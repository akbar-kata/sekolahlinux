import React, { Fragment } from 'react';

import { Formik, Form, FormikActions } from 'formik';

import { Button } from 'components/Button';
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'components/Modal';
import { Circle } from 'components/Loading';
import { SavingIndicatorContainerProps } from './SavingIndicator.Container';

interface Props extends SavingIndicatorContainerProps {
  onDiscard: () => void;
  doDiscard: (values: any, actions: FormikActions<any>) => void;
}

const Component: React.SFC<Props> = props => (
  <Fragment>
    <div className="kata-loading__message--saved">
      {props.isSavingDraft ? (
        <Fragment>
          <Circle size={16} className="kata-loading__message--loading" /> Saving
          ...
        </Fragment>
      ) : (
        props.config.draft && (
          <Fragment>
            <i className="kata-loading__message--icon icon-ok" /> Saved
            <a
              className="kata-loading__message--discard-btn"
              onClick={props.onDiscard}
            >
              Discard Draft
            </a>
          </Fragment>
        )
      )}
    </div>
    <Modal show={props.isOpen} onClose={props.closeModal}>
      <Formik initialValues={{ discard: '' }} onSubmit={props.doDiscard}>
        {({ submitForm }) => (
          <Form>
            <ModalHeader onClose={props.closeModal}>Discard Draft</ModalHeader>
            <ModalBody>
              <div className="kata-info__container">
                Are you sure want to discard draft and back to the previous
                revision list?
              </div>
            </ModalBody>
            <ModalFooter>
              <Button
                color="primary"
                type="submit"
                loading={props.isDiscard}
                onClick={submitForm}
              >
                Discard
              </Button>
              <Button
                color="secondary"
                type="button"
                onClick={props.closeModal}
              >
                Close
              </Button>
            </ModalFooter>
          </Form>
        )}
      </Formik>
    </Modal>
  </Fragment>
);

export default Component;
