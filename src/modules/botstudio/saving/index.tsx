import React from 'react';
import SavingIndicatorContainer from './SavingIndicator.Container';

import './style.scss';

const SavingIndicator: React.SFC = () => (
  <div className="kata-loading">
    <SavingIndicatorContainer />
  </div>
);

export default SavingIndicator;
