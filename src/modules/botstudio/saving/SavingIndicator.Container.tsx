import React from 'react';
import { connect } from 'react-redux';
import RootStore from 'interfaces/rootStore';
import { FormikActions } from 'formik';
import { discardDraft } from 'stores/bot/actions';

import { closeModal, openModal } from 'stores/app/modal/actions';
import { isModalOpen } from 'stores/app/modal/selectors';
import SavingIndicator from './SavingIndicator';

import BOT from 'stores/bot/types';
import { getLoading } from 'stores/app/loadings/selectors';

interface PropsFromState {
  isOpen: boolean;
  isDiscard: boolean;
  isSavingDraft: boolean;
  config: any;
}

interface PropsFromDispatch {
  openModal: () => ReturnType<typeof openModal>;
  closeModal: () => ReturnType<typeof closeModal>;
  discard: () => void;
}

export interface SavingIndicatorContainerProps
  extends PropsFromState,
    PropsFromDispatch {}

export const MODAL_ID = 'bot-discard-draft';

class SavingIndicatorContainer extends React.Component<
  SavingIndicatorContainerProps,
  PropsFromState
> {
  onDiscard = () => {
    this.props.openModal();
  };

  doDiscard = (values: any, actions: FormikActions<any>) => {
    this.props.discard();
  };

  render() {
    return (
      <SavingIndicator
        onDiscard={this.onDiscard}
        doDiscard={this.doDiscard}
        {...this.props}
      />
    );
  }
}

const mapStateToProps = ({ app, bot }: RootStore): PropsFromState => {
  return {
    isOpen: isModalOpen(app.modal, MODAL_ID),
    isDiscard: getLoading(app.loadings, BOT.DISCARD_DRAFT_REQUEST),
    isSavingDraft: getLoading(app.loadings, BOT.SAVE_DRAFT_REQUEST),
    config: bot
  };
};

const mapDispatchToProps = (dispatch: Function) => {
  return {
    openModal: () => dispatch(openModal(MODAL_ID)),
    closeModal: () => dispatch(closeModal(MODAL_ID)),
    discard: () => dispatch(discardDraft())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SavingIndicatorContainer);
