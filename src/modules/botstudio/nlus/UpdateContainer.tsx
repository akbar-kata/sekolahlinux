import React from 'react';
import { connect } from 'react-redux';

import Store from 'interfaces/rootStore';
import * as NluActions from 'stores/bot/nlu/actions';
import { isExist } from 'stores/bot/nlu/selectors';
import NLUForm from './NLUForm';

interface PropsFromState {
  isExist: Function;
}

interface PropsFromDispatch {
  onUpdate: Function;
}

interface Props extends PropsFromState, PropsFromDispatch {
  nluId: string;
  isFormOpen: boolean;
  data: { [key: string]: any };
  onCancel: Function;
  closeUpdateDrawer(): void;
  showNotif(title: string, message: string): void;
  [key: string]: any;
}

interface States {}

class UpdateContainer extends React.Component<Props, States> {
  onSubmit = data => {
    this.props.onUpdate(this.props.data.id, data);
    this.props.closeUpdateDrawer();
    this.props.showNotif('NLUs Updated', 'You have successfully updated NLUs.');
  };

  render() {
    const { data, onCancel } = this.props;

    return (
      <NLUForm
        data={data.data}
        isExist={this.props.isExist}
        onSubmit={this.onSubmit}
        onCancel={onCancel}
        isFormOpen={this.props.isFormOpen}
        onCloseForm={this.props.closeUpdateDrawer}
        formType="update"
      />
    );
  }
}

const mapStateToProps = ({ bot: { nlu } }: Store, ownProps): PropsFromState => {
  return {
    isExist: (name: string) => isExist(nlu, name, ownProps.nluId)
  };
};

const mapDispatchToProps = {
  onUpdate: NluActions.update
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UpdateContainer);
