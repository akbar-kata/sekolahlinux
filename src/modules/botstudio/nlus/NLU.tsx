import React from 'react';

import { BotNLU as NLUInterface } from 'interfaces/bot';
import { confirm } from 'components/Modal';
import Card from 'components/Card';
import {
  Dropdown,
  DropdownItem,
  DropdownToggle,
  DropdownMenu
} from 'components/Dropdown';
import { Button, SupportButton } from 'components/Button';

interface Props extends Partial<NLUInterface> {
  children?: any;
  nluId: string;
  data: NLU;
  onRemove: Function;
  onUpdate: Function;
  selectNlu(id: string, data: NLU, isUpdate: boolean): void;
  showNotif(title: string, message: string): void;
}

interface States {}

class NLU extends React.PureComponent<Props, States> {
  remove = (event: any) => {
    event.stopPropagation();

    confirm({
      title: 'Delete NLUs',
      message: `Are you sure you want to delete this NLUs?`,
      okLabel: 'Delete',
      cancelLabel: 'Cancel'
    }).then(res => {
      if (res) {
        this.props.onRemove(this.props.nluId);
        this.props.showNotif('NLUs Deleted', 'NLUs has been deleted.');
      }
    });
  };

  render() {
    return (
      <Card
        key={this.props.nluId}
        className="kata-bot-nlu__card"
        title={this.props.name}
        action={
          <Dropdown>
            <DropdownToggle caret={false}>
              <Button isIcon>
                <i className="icon-more" />
              </Button>
            </DropdownToggle>
            <DropdownMenu right>
              <DropdownItem
                onClick={() =>
                  this.props.selectNlu(this.props.nluId, this.props.data, true)
                }
              >
                Update
              </DropdownItem>
              <DropdownItem onClick={this.remove}>Delete</DropdownItem>
            </DropdownMenu>
          </Dropdown>
        }
        onClick={() =>
          this.props.selectNlu(this.props.nluId, this.props.data, true)
        }
      >
        <div className="row mb-2">
          <div className="col-7">
            <div className="text-label">Type</div>
            {this.props.type}
          </div>
        </div>
        <SupportButton
          onClick={() =>
            this.props.selectNlu(this.props.nluId, this.props.data, true)
          }
        >
          View NLU
        </SupportButton>
      </Card>
    );
  }
}

export default NLU;
