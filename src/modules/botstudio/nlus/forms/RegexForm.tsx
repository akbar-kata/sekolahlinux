import React, { Fragment } from 'react';
import { FieldArray } from 'formik';
import isArray from 'lodash-es/isArray';

import { Field } from 'components/FormikWrapper';
import { Button, FloatingButton, SupportButton } from 'components/Button';

export const regexValidate = values => {
  if (!values) {
    return {
      regex: 'Regex is required'
    };
  }

  const { regex, index } = values;

  return {
    regex: !regex ? 'Regex is required' : undefined,
    index:
      index && isArray(index)
        ? index.map(idx => {
            const { key, number: indexNumber } = idx;
            return {
              key: !key
                ? 'Key is required'
                : !/^[A-Za-z][-A-Za-z0-9_-]*$/.test(key)
                ? 'Key only allowed charaters alphanumeric, _ and -'
                : undefined,
              number: !indexNumber ? 'Number is required' : undefined
            };
          })
        : undefined
  };
};

const renderSwitchButton = (
  isView: boolean = false,
  { values, setFieldValue },
  { push }
) => (
  <div className="my-2">
    {(!isView && values.options && !values.options.multiple && (
      <SupportButton
        onClick={async () => {
          await setFieldValue('options.multiple', true);
          await push({});
        }}
      >
        Use Multiple Index
      </SupportButton>
    )) ||
      (!isView && (
        <SupportButton
          onClick={async () => {
            await setFieldValue('options.multiple', false);
            await setFieldValue('options.index', '');
          }}
        >
          Use Single Index
        </SupportButton>
      ))}
  </div>
);

const RegexForm = (formApi, isView: boolean = false) => {
  const { values } = formApi;
  return (
    <Fragment>
      <div className="mb-2">
        <label className="control-label kata-form__label">Regex</label>
        <Field
          name="options.regex"
          className="form-control kata-form__input-text"
          disabled={isView}
        />
      </div>
      <div>
        <label className="control-label kata-form__label">Index</label>
        <div>
          {values.options &&
            (!values.options.multiple || values.options.multiple === false) && (
              <Field
                name="options.index"
                type="number"
                placeholder="Number"
                className="form-control kata-form__input-text"
                disabled={isView}
              />
            )}
          <FieldArray
            name="options.index"
            render={arrayHelpers => (
              <div>
                {values.options &&
                  values.options.multiple &&
                  isArray(values.options.index) &&
                  values.options.index.map((value, index) => (
                    <div className="kata-bot-nlu--regex-wrapper" key={index}>
                      <div className="kata-form__element">
                        <Field
                          name={`options.index.${index}.key`}
                          className="form-control kata-form__input-text"
                          placeholder="Key"
                          disabled={isView}
                        />
                      </div>
                      <div className="kata-form__element">
                        <Field
                          name={`options.index.${index}.number`}
                          type="number"
                          className="form-control kata-form__input-text"
                          placeholder="Number"
                          disabled={isView}
                        />
                      </div>
                      {!isView && (
                        <FloatingButton
                          className="kata-bot-nlu--remove-btn"
                          color="danger"
                          icon="trash"
                          onClick={() => {
                            if (isArray(values.options.index)) {
                              arrayHelpers.remove(index);
                            }
                          }}
                        />
                      )}
                    </div>
                  ))}

                {!isView && values.options && values.options.multiple && (
                  <Button color="primary" onClick={() => arrayHelpers.push({})}>
                    Add Index
                  </Button>
                )}

                {renderSwitchButton(isView, formApi, arrayHelpers)}
              </div>
            )}
          />
        </div>
      </div>
    </Fragment>
  );
};

export default RegexForm;
