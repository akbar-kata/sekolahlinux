import React from 'react';

import { Field } from 'components/FormikWrapper';

export const verstandRetrievalValidate = values => {
  if (!values) {
    return {
      model: 'Model Id is required'
    };
  }

  const { model } = values;

  return {
    model: !model
      ? 'Model Id is required'
      : !/^[A-Za-z][-A-Za-z0-9_-]*$/.test(model)
      ? 'Model Id only allowed charaters alphanumeric, _ and -'
      : undefined
  };
};

const VerstandRetrievalForm = (formApi: any, isView: boolean = false) => (
  <div>
    <label className="control-label kata-form__label">Model Id</label>
    <Field
      name="options.model"
      className="form-control kata-form__input-text"
      disabled={isView}
    />
  </div>
);

export default VerstandRetrievalForm;
