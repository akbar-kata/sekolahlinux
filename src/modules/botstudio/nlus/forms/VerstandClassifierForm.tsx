import React, { Fragment } from 'react';

import { Field, ReactToggle } from 'components/FormikWrapper';

export const verstandClassfierValidate = values => {
  if (!values) {
    return {
      model: 'Model Id is required'
    };
  }

  const { model } = values;

  return {
    model: !model
      ? 'Model Id is required'
      : !/^[A-Za-z][-A-Za-z0-9_-]*$/.test(model)
      ? 'Model Id only allowed charaters alphanumeric, _ and -'
      : undefined
  };
};

const VerstandClassifierFrom = (formApi: any, isView: boolean = false) => (
  <Fragment>
    <div>
      <label className="control-label kata-form__label">Model Id</label>
      <Field
        name="options.model"
        className="form-control kata-form__input-text"
        disabled={isView}
      />
    </div>
    <div className="my-2">
      <label className="control-label kata-form__label">Threshold</label>
      <div className="d-flex align-items-center">
        <Field
          name="options.threshold"
          type="number"
          className="form-control kata-form__input-text mr-1"
          step={0.1}
          disabled={isView}
          style={{ width: 120 }}
        />
        <ReactToggle
          name="options.asAttribute"
          label="Use as attributes"
          noFalse
          disabled={isView}
          innerClassName="mb-0"
        />
      </div>
    </div>
  </Fragment>
);

export default VerstandClassifierFrom;
