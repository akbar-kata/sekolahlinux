import React from 'react';

import MethodSelector from 'containers/MethodSelector';

export const methodFormValidate = values => {
  if (!values) {
    return {
      method: 'Method is required'
    };
  }

  const { method } = values;

  return {
    method: !method ? 'Method is required' : undefined
  };
};

const MethodForm = (formApi: any, isView: boolean = false) => (
  <div className="mb-2">
    <label className="control-label kata-form__label">Method</label>
    <MethodSelector field="options.method" root disabled={isView} />
  </div>
);

export default MethodForm;
