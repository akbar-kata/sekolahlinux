import React from 'react';

import { Field } from 'components/FormikWrapper';

export const witIntentValidate = values => {
  if (!values) {
    return {
      apitoken: 'API token is required',
      threshold: 'Threshold is required'
    };
  }

  const { apitoken, threshold } = values;

  return {
    apitoken: !apitoken
      ? 'API Token is required'
      : !/^[A-Za-z][-A-Za-z0-9_-]*$/.test(apitoken)
      ? 'Wrong token format'
      : undefined,
    threshold: !threshold ? 'Threshold is required' : undefined
  };
};

const WitIntentForm = (formApi: any, isView: boolean = false) => (
  <div className="mb-2">
    <div className="mb-2">
      <label className="control-label kata-form__label">API Token</label>
      <Field
        name="options.apitoken"
        className="form-control kata-form__input-text"
        disabled={isView}
      />
    </div>
    <div>
      <label className="control-label kata-form__label">Threshold</label>
      <Field
        name="options.threshold"
        type="number"
        className="form-control kata-form__input-text"
        step={0.1}
        disabled={isView}
        style={{ width: 120 }}
      />
    </div>
  </div>
);

export default WitIntentForm;
