import React, { Fragment } from 'react';

import { Field, ReactSelect, ReactToggle } from 'components/FormikWrapper';
import NLUSearch from 'containers/NLUSearch';

export const nluValidate = values => {
  if (!values) {
    return {
      nluId: 'NLU Id is required',
      threshold: 'Threshold is required',
      output: 'Output is required'
    };
  }

  const { nluId, threshold, output } = values;

  return {
    nluId: !nluId
      ? 'NLU Id is required'
      : !/^[-A-Za-z0-9_:-]*$/.test(nluId)
      ? 'NLU Id only allowed charaters alphanumeric, _ and -'
      : undefined,
    threshold: !threshold ? 'Threshold is required' : undefined,
    output: !output ? 'Output is required' : undefined
  };
};

const outputOptions = [
  {
    label: 'Raw',
    value: 'raw'
  },
  {
    label: 'Dictionary',
    value: 'dict'
  },
  {
    label: 'Value',
    value: 'value'
  }
];

const NLForm = (formApi: any, isView: boolean = false) => (
  <Fragment>
    <div>
      <label className="control-label kata-form__label">NLU Id</label>
      <NLUSearch>
        {args => {
          return (
            <ReactSelect
              async
              name="options.nluId"
              options={args.options}
              valueKey="nluId"
              labelKey="nluId"
              loadOptions={args.search}
              disabled={isView}
              simpleValue
            />
          );
        }}
      </NLUSearch>
    </div>
    <div className="d-flex my-2">
      <div style={{ width: 120 }}>
        <label className="control-label kata-form__label">Threshold</label>
        <Field
          name="options.threshold"
          type="number"
          className="form-control kata-form__input-text"
          step={0.1}
          disabled={isView}
        />
      </div>
      <div className="kata-bot-nlu--flex-one ml-1">
        <label className="control-label kata-form__label">As Intent</label>
        <Field
          name="options.asIntent"
          className="form-control kata-form__input-text"
          disabled={isView}
        />
      </div>
    </div>

    <div className="d-flex my-2">
      <div style={{ width: 120 }}>
        <label className="control-label kata-form__label">Output</label>
        <ReactSelect
          name="options.output"
          options={outputOptions}
          disabled={isView}
          clearable
          simpleValue
        />
      </div>
      <div className="kata-bot-nlu--flex-one ml-1">
        <label className="control-label kata-form__label">Path</label>
        <Field
          name="options.path"
          className="form-control kata-form__input-text"
          disabled={isView}
        />
      </div>
    </div>

    <div className="kata-bot-nlu--flex-one ml-1">
      <ReactToggle name="options.flatten" label="Flatten" disabled={isView} />
    </div>
  </Fragment>
);

export default NLForm;
