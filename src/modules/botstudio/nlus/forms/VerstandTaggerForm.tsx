import React, { Fragment } from 'react';

import { Field, ReactToggle } from 'components/FormikWrapper';

export const verstandTaggerValidate = values => {
  if (!values) {
    return {
      model: 'Model Id is required'
    };
  }

  const { model } = values;

  return {
    model: !model
      ? 'Model Id is required'
      : !/^[A-Za-z][-A-Za-z0-9_-]*$/.test(model)
      ? 'Model Id only allowed charaters alphanumeric, _ and -'
      : undefined
  };
};

const VerstandTaggerForm = (formApi: any, isView: boolean = false) => (
  <Fragment>
    <div className="mb-2">
      <label className="control-label kata-form__label">Model Id</label>
      <Field
        name="options.model"
        className="form-control kata-form__input-text"
        disabled={isView}
      />
    </div>
    <div className="mb-2">
      <ReactToggle
        name="options.labels"
        label="Use as labels"
        disabled={isView}
      />
    </div>
  </Fragment>
);

export default VerstandTaggerForm;
