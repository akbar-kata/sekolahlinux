import React, { Fragment } from 'react';
import isArray from 'lodash-es/isArray';
import { FieldArray } from 'formik';

import { Field, FormError } from 'components/FormikWrapper';
import { FloatingButton, SupportButton } from 'components/Button';

export const multipleRegexValidate = values => {
  if (!values) {
    return {
      regex: 'Regex is required'
    };
  }

  const { regex } = values;

  return {
    regex:
      regex && isArray(regex)
        ? regex.map(item => ({
            key: !item.key
              ? 'Key is required'
              : !/^[A-Za-z][-A-Za-z0-9_-]*$/.test(item.key)
              ? 'Key only allowed charaters alphanumeric, _ and -'
              : undefined,
            value: !item.value ? 'Regex is required' : undefined
          }))
        : undefined
  };
};

const MultipleRegexForm = ({ values }, isView: boolean = false) => {
  return (
    <div className="mb-2">
      <label className="control-label kata-form__label">Regex</label>
      <FormError name="options.regex" />
      <FieldArray
        name="options.regex"
        render={({ push, remove }) => (
          <Fragment>
            {values.options &&
              values.options.regex &&
              isArray(values.options.regex) &&
              values.options.regex.map((regex, i) => (
                <div className="kata-bot-nlu--regex-wrapper" key={i}>
                  <div className="kata-form__element">
                    <Field
                      name={`options.regex.${i}.key`}
                      className="form-control kata-form__input-text"
                      placeholder="Key"
                      disabled={isView}
                    />
                  </div>
                  <div className="kata-form__element">
                    <Field
                      name={`options.regex.${i}.value`}
                      className="form-control kata-form__input-text"
                      placeholder="Regex"
                      disabled={isView}
                    />
                  </div>
                  {!isView && (
                    <FloatingButton
                      color="danger"
                      icon="trash"
                      className="kata-bot-nlu--remove-btn"
                      onClick={() => remove(i)}
                    />
                  )}
                </div>
              ))}
            {!isView && (
              <SupportButton onClick={() => push({})}>Add Regex</SupportButton>
            )}
          </Fragment>
        )}
      />
    </div>
  );
};

export default MultipleRegexForm;
