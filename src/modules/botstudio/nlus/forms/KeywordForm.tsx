import React, { Fragment } from 'react';
import { FieldArray } from 'formik';
import isEmpty from 'lodash-es/isEmpty';
import get from 'lodash-es/get';

import { FloatingButton, SupportButton } from 'components/Button';
import {
  Field,
  ReactTagsInput,
  ReactToggle,
  FormError
} from 'components/FormikWrapper';

export const keywordValidate = values => {
  if (!values) {
    return {
      keywords: 'Need at least one keyword'
    };
  }

  const { keywords } = values;

  return {
    keywords:
      !keywords || !keywords.length
        ? 'Need at least one keyword'
        : keywords.map(keyword => {
            const { key, words } = keyword;
            return {
              key: !key
                ? 'A key is required'
                : !/^[A-Za-z][-A-Za-z0-9_-]*$/.test(key)
                ? 'Key only allowed charaters alphanumeric, _ and -'
                : undefined,
              words:
                !words || !words.length
                  ? 'Need at least one word defined'
                  : undefined
            };
          })
  };
};

const KeywordForm = ({ values }, isView: boolean = false) => {
  const isEmptyKeywords = isEmpty(
    get(values, ['options', 'keywords'], undefined)
  );

  return (
    <Fragment>
      <div>
        <label className="control-label kata-form__label">Keywords</label>
        <FormError name="options.keywords" />
        <FieldArray
          name="options.keywords"
          render={({ remove, push }) => (
            <Fragment>
              {!values.options || isEmptyKeywords ? (
                <div className="mb-2">
                  <em>No keywords have been added yet</em>
                </div>
              ) : values.options &&
                values.options.keywords &&
                values.options.keywords.length ? (
                values.options.keywords.map((keyword, index) => (
                  <div
                    key={index}
                    className="kata-bot-nlu__create--keyword-wrapper"
                  >
                    <div className="kata-form__element">
                      <Field
                        name={`options.keywords.${index}.key`}
                        placeholder="Key"
                        className="form-control kata-form__input-text"
                        disabled={isView}
                      />
                    </div>
                    <div className="kata-form__element">
                      <ReactTagsInput
                        name={`options.keywords.${index}.words`}
                        className="form-control kata-form__input-textarea"
                        placeholder="Add a word"
                        disabled={isView}
                      />
                    </div>

                    {!isView && (
                      <FloatingButton
                        icon="trash"
                        color="danger"
                        className="kata-bot-nlu--remove-btn"
                        onClick={() => remove(index)}
                      />
                    )}
                  </div>
                ))
              ) : null}

              {!isView && (
                <SupportButton onClick={() => push({})}>
                  Add Keyword
                </SupportButton>
              )}
            </Fragment>
          )}
        />
      </div>

      <div className="mt-2">
        <label className="control-label kata-form__label">Default</label>
        <Field
          name="options.default"
          className="form-control"
          disabled={isView}
        />
      </div>

      <div className="mt-3 mb-3 d-flex">
        <ReactToggle
          name="options.case"
          label="Case sensitive"
          disabled={isView}
        />
        <ReactToggle
          name="options.exact"
          label="Exact matching"
          className="ml-2"
          disabled={isView}
        />
      </div>
    </Fragment>
  );
};

export default KeywordForm;
