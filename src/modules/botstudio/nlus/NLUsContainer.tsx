import React from 'react';
import { connect } from 'react-redux';

import Store from 'interfaces/rootStore';
import * as NluActions from 'stores/bot/nlu/actions';
import { getData, getIndex } from 'stores/bot/nlu/selectors';
import { isDrawerOpen } from 'stores/app/drawer/selectors';
import { closeDrawer, openDrawer } from 'stores/app/drawer/actions';
import { addNotification } from 'stores/app/notification/actions';

import NLUs from './NLUs';
import { BotNLU } from 'interfaces/bot';

interface PropsFromState {
  isCreateFormOpen: boolean;
  isUpdateFormOpen: boolean;
  isDetailFormOpen: boolean;
  index: string[];
  data: any;
}

interface PropsFromDispatch {
  onRemove: Function;
  openCreateDrawer: () => ReturnType<typeof openDrawer>;
  closeCreateDrawer: () => ReturnType<typeof closeDrawer>;
  openUpdateDrawer: () => ReturnType<typeof openDrawer>;
  closeUpdateDrawer: () => ReturnType<typeof closeDrawer>;
  openDetailDrawer: () => ReturnType<typeof openDrawer>;
  closeDetailDrawer: () => ReturnType<typeof closeDrawer>;
  showSuccessNotification(title: string, message: string): void;
}

export interface NLUsContainerProps extends PropsFromState, PropsFromDispatch {}

interface States {
  selected: {
    id: string;
    data: object;
  };
}

class NLUsContainer extends React.Component<NLUsContainerProps, States> {
  state = {
    selected: {
      id: '',
      data: {}
    }
  };

  onSelectNlu = (id: string, data: BotNLU, isUpdate: boolean = false) => {
    this.setState(
      {
        selected: {
          id,
          data
        }
      },
      () => {
        if (isUpdate) {
          this.props.openUpdateDrawer();
        } else {
          this.props.openDetailDrawer();
        }
      }
    );
  };

  render() {
    return (
      <NLUs
        {...this.props}
        selectNlu={this.onSelectNlu}
        selectedNlu={this.state.selected}
      />
    );
  }
}

const mapStateToProps = ({ app, bot: { nlu } }: Store): PropsFromState => {
  return {
    data: getData(nlu),
    index: getIndex(nlu),
    isCreateFormOpen: isDrawerOpen(app.drawer, 'NewNluForm'),
    isUpdateFormOpen: isDrawerOpen(app.drawer, 'UpdateNluForm'),
    isDetailFormOpen: isDrawerOpen(app.drawer, 'DetailNluForm')
  };
};

const mapDispatchToProps: PropsFromDispatch = {
  onRemove: NluActions.remove,
  openCreateDrawer: () => openDrawer('NewNluForm'),
  closeCreateDrawer: () => closeDrawer('NewNluForm'),
  openUpdateDrawer: () => openDrawer('UpdateNluForm'),
  closeUpdateDrawer: () => closeDrawer('UpdateNluForm'),
  openDetailDrawer: () => openDrawer('DetailNluForm'),
  closeDetailDrawer: () => closeDrawer('DetailNluForm'),
  showSuccessNotification: (title: string, message: string) =>
    addNotification({
      title,
      message,
      status: 'success',
      dismissible: true,
      dismissAfter: 5000
    })
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NLUsContainer);
