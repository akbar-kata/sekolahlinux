import React from 'react';

import { Dashboard } from 'components/Dashboard';

import NLUs from './NLUsContainer';

class Page extends React.Component<any, any> {
  render() {
    return (
      <Dashboard
        title="NLUs"
        tooltip="You can create and manage NL to be used by your chatbot here."
      >
        <NLUs />
      </Dashboard>
    );
  }
}

export default Page;
