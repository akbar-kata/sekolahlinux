import React from 'react';

import NLUForm from './NLUForm';

interface Props {
  isFormOpen: boolean;
  data: { [key: string]: any };
  closeDetailDrawer(): void;
  [key: string]: any;
}

interface States {}

class ViewContainer extends React.PureComponent<Props, States> {
  render() {
    const { data } = this.props;

    return (
      <NLUForm
        data={data.data}
        isFormOpen={this.props.isFormOpen}
        onCloseForm={this.props.closeDetailDrawer}
        formType="detail"
        isExist={() => null}
      />
    );
  }
}

export default ViewContainer;
