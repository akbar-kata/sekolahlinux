import React, { Fragment } from 'react';
import isEmpty from 'lodash-es/isEmpty';
import get from 'lodash-es/get';
import { Formik, Form, FieldArray } from 'formik';

import { Field, ReactSelect, utils } from 'components/FormikWrapper';
import {
  Drawer,
  DrawerBody,
  DrawerHeader,
  DrawerFooter
} from 'components/Drawer';
import { Button, SupportButton, FloatingButton } from 'components/Button';

import MethodSelector from 'containers/MethodSelector';

import KeywordForm, { keywordValidate } from './forms/KeywordForm';
import RegexForm, { regexValidate } from './forms/RegexForm';
import MultipleRegexForm, {
  multipleRegexValidate
} from './forms/MultipleRegexForm';
import VerstandTaggerForm, {
  verstandTaggerValidate
} from './forms/VerstandTaggerForm';
import VerstandClassifierForm, {
  verstandClassfierValidate
} from './forms/VerstandClassifierForm';
import VerstandRetrievalForm, {
  verstandRetrievalValidate
} from './forms/VerstandRetrievalForm';
import WitIntentForm, { witIntentValidate } from './forms/WitIntentForm';
import WitEntitiesForm, { witEntitiesValidate } from './forms/WitEntitiesForm';
import MethodForm, { methodFormValidate } from './forms/MethodForm';
import NlForm, { nluValidate } from './forms/NlForm';

interface Props {
  formType: 'create' | 'update' | 'detail';
  id?: string;
  data?: { [key: string]: any };
  isFormOpen: boolean;
  onCancel?: Function;
  isExist: Function;
  onSubmit?(data: any);
  onCloseForm(): void;
}

interface States {}

const inputId = 'input-nlu-create';

export const typeOptions = [
  {
    label: 'Keyword',
    value: 'keyword'
  },
  {
    label: 'NL',
    value: 'nl'
  },
  {
    label: 'Regex',
    value: 'regex'
  },
  {
    label: 'Multiple Regex',
    value: 'multipleRegex'
  },
  {
    label: 'Verstand tagger',
    value: 'verstandTagger'
  },
  {
    label: 'Verstand classifier',
    value: 'verstandClassifier'
  },
  {
    label: 'Verstand retrieval',
    value: 'verstandRetrieval'
  },
  {
    label: 'Wit Intent',
    value: 'witIntent'
  },
  {
    label: 'Wit Entity',
    value: 'witEntity'
  },
  {
    label: 'Method',
    value: 'method'
  }
];

class NLUForm extends React.PureComponent<Props, States> {
  private isViewDetail: boolean;

  constructor(props: Props) {
    super(props);

    this.isViewDetail = this.props.formType === 'detail';
  }

  componentDidMount() {
    const el = document.getElementById(inputId);
    if (el && el.focus) {
      el.focus();
    }
  }

  validateOptions = (type: string, options: any) => {
    switch (type) {
      case 'keyword':
        return keywordValidate(options);
      case 'nl':
        return nluValidate(options);
      case 'regex':
        return regexValidate(options);
      case 'multipleRegex':
        return multipleRegexValidate(options);
      case 'verstandTagger':
        return verstandTaggerValidate(options);
      case 'verstandClassifier':
        return verstandClassfierValidate(options);
      case 'verstandRetrieval':
        return verstandRetrievalValidate(options);
      case 'witIntent':
        return witIntentValidate(options);
      case 'witEntity':
        return witEntitiesValidate(options);
      case 'method':
        return methodFormValidate(options);
      default:
        return null;
    }
  };

  onValidate = (values, state) => {
    const { name, type, options, process } = values;

    const validate = {
      name: !name
        ? 'A name is required'
        : !/^[A-Za-z][-A-Za-z0-9_-]*$/.test(name)
        ? 'Name only allowed charaters alphanumeric, _ and -'
        : this.props.isExist(name, this.props.id)
        ? 'Name already used, pick another name'
        : undefined,
      type: !type ? 'Type must be selected' : undefined,
      options: this.validateOptions(type, options),
      process:
        process && process.length
          ? process.map(proc => {
              return !proc ? 'Processor is required' : undefined;
            })
          : undefined
    };

    return utils.cleanDeep(validate);
  };

  renderFormType = (type: string, formApi: any) => {
    switch (type) {
      case 'nl':
        return NlForm(formApi, this.isViewDetail);
      case 'regex':
        return RegexForm(formApi, this.isViewDetail);
      case 'multipleRegex':
        return MultipleRegexForm(formApi, this.isViewDetail);
      case 'verstandTagger':
        return VerstandTaggerForm(formApi, this.isViewDetail);
      case 'verstandClassifier':
        return VerstandClassifierForm(formApi, this.isViewDetail);
      case 'verstandRetrieval':
        return VerstandRetrievalForm(formApi, this.isViewDetail);
      case 'witIntent':
        return WitIntentForm(formApi, this.isViewDetail);
      case 'witEntity':
        return WitEntitiesForm(formApi, this.isViewDetail);
      case 'method':
        return MethodForm(formApi, this.isViewDetail);
      case 'keyword':
        return KeywordForm(formApi, this.isViewDetail);
      default:
        return null;
    }
  };

  renderHeader = () => {
    if (this.props.formType === 'create') {
      return 'Create NLUs';
    }

    if (this.props.formType === 'update') {
      return 'Update NLUs';
    }

    return 'NLUs Detail';
  };

  renderInnerForm = formApi => {
    const { values, setFieldValue } = formApi;

    return (
      <Form className="kata-form full-size">
        <DrawerHeader title={this.renderHeader()} />
        <DrawerBody>
          <div className="mb-2">
            <label className="control-label kata-form__label">Name</label>
            <div>
              <Field
                id={inputId}
                name="name"
                className="form-control kata-form__input-text"
                disabled={this.isViewDetail}
              />
            </div>
          </div>

          <div className="mb-2">
            <label className="control-label kata-form__label">Type</label>
            <div>
              <ReactSelect
                name="type"
                clearable={false}
                simpleValue
                options={typeOptions}
                onChange={() => setFieldValue('options', {})}
                disabled={this.isViewDetail}
              />
            </div>
          </div>

          {this.renderFormType(values.type, formApi)}

          {!isEmpty(values.type) ? (
            <label className="control-label kata-form__label mb-2">
              Processors
            </label>
          ) : null}

          <FieldArray
            name="process"
            render={({ remove, push }) => (
              <Fragment>
                {!isEmpty(values.process)
                  ? values.process.map((process, index) => (
                      <div key={index} className="d-flex mb-1">
                        <div className="kata-bot-nlu--flex-one">
                          <MethodSelector
                            field={`process.${index}`}
                            root
                            disabled={this.isViewDetail}
                          />
                        </div>
                        {!this.isViewDetail && (
                          <div>
                            <FloatingButton
                              icon="trash"
                              className="ml-1"
                              onClick={() => remove(index)}
                            />
                          </div>
                        )}
                      </div>
                    ))
                  : null}

                {!this.isViewDetail && values.type ? (
                  <div className="mt-2">
                    <SupportButton onClick={() => push('')}>
                      Add Processor
                    </SupportButton>
                  </div>
                ) : null}
              </Fragment>
            )}
          />
        </DrawerBody>

        <DrawerFooter>
          {!this.isViewDetail && (
            <Button type="submit" color="primary" className="mr-1">
              {this.props.formType === 'create' ? 'Create' : 'Update'}
            </Button>
          )}
          <Button onClick={this.props.onCloseForm}>Cancel</Button>
        </DrawerFooter>
      </Form>
    );
  };

  constructFormProps = (defaultProps: any) => {
    if (this.props.formType === 'detail') {
      const { validate, ...props } = defaultProps;
      return props;
    }

    return defaultProps;
  };

  render() {
    const defaultProps = {
      initialValues: get(this.props, 'data', {}),
      validate: this.onValidate,
      onSubmit: this.props.onSubmit
    };

    return (
      <Drawer isOpen={this.props.isFormOpen} onClose={this.props.onCloseForm}>
        <Formik {...this.constructFormProps(defaultProps)}>
          {this.renderInnerForm}
        </Formik>
      </Drawer>
    );
  }
}

export default NLUForm;
