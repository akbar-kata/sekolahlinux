import React, { Fragment } from 'react';

import { DashboardCards } from 'components/Dashboard';
import Card from 'components/Card';
import CardButton from 'components/Card/CardButton';
import { BotNLU } from 'interfaces/bot';

import CreateNLU from './CreateContainer';
import UpdateNLU from './UpdateContainer';
import DetailNLU from './ViewContainer';
import NLU from './NLU';
import { NLUsContainerProps } from './NLUsContainer';

interface Props extends NLUsContainerProps {
  selectedNlu: any;
  selectNlu(id: string, data: BotNLU, isUpdate: boolean): void;
}

const NLUs: React.SFC<Props> = ({
  index,
  data,
  onRemove,
  isCreateFormOpen,
  isUpdateFormOpen,
  isDetailFormOpen,
  openCreateDrawer,
  closeCreateDrawer,
  openUpdateDrawer,
  closeUpdateDrawer,
  closeDetailDrawer,
  selectedNlu,
  selectNlu,
  showSuccessNotification
}) => {
  return (
    <Fragment>
      <DashboardCards>
        <Fragment>
          <Card
            asButton
            className="kata-bot-nlu__add-btn"
            onClick={openCreateDrawer}
          >
            <CardButton label={'Create NLU'} icon="add" />
          </Card>
        </Fragment>
        {index.map(key => (
          <NLU
            key={key}
            nluId={key}
            {...data[key]}
            data={data[key]}
            onRemove={onRemove}
            onUpdate={openUpdateDrawer}
            selectNlu={selectNlu}
            showNotif={showSuccessNotification}
          />
        ))}
      </DashboardCards>
      <CreateNLU
        isFormOpen={isCreateFormOpen}
        closeCreateDrawer={closeCreateDrawer}
        showNotif={showSuccessNotification}
      />
      {selectedNlu && (
        <Fragment>
          <UpdateNLU
            nluId={selectedNlu.id}
            data={selectedNlu}
            isFormOpen={isUpdateFormOpen}
            closeUpdateDrawer={closeUpdateDrawer}
            showNotif={showSuccessNotification}
          />
          <DetailNLU
            data={selectedNlu}
            isFormOpen={isDetailFormOpen}
            closeDetailDrawer={closeDetailDrawer}
          />
        </Fragment>
      )}
    </Fragment>
  );
};

export default NLUs;
