import React from 'react';
import { connect } from 'react-redux';

import Store from 'interfaces/rootStore';
import * as NluActions from 'stores/bot/nlu/actions';
import { isExist } from 'stores/bot/nlu/selectors';
import NLUForm from './NLUForm';

interface PropsFromState {
  isExist: (name: string) => ReturnType<typeof isExist>;
}

interface PropsFromDispatch {
  onAdd: typeof NluActions.create;
}

interface Props extends PropsFromState, PropsFromDispatch {
  isFormOpen: boolean;
  onCancel?: () => any;
  closeCreateDrawer: () => any;
  showNotif(title: string, message: string): void;
}

interface States {}

class CreateContainer extends React.Component<Props, States> {
  onSubmit = data => {
    this.props.onAdd(data);
    this.props.closeCreateDrawer();
    this.props.showNotif('NLUs Created', 'You have successfully created NLUs.');
  };

  render() {
    return (
      <NLUForm
        isExist={this.props.isExist}
        onSubmit={this.onSubmit}
        onCancel={this.props.onCancel}
        isFormOpen={this.props.isFormOpen}
        onCloseForm={this.props.closeCreateDrawer}
        formType="create"
      />
    );
  }
}

const mapStateToProps = ({ bot: { nlu } }: Store): PropsFromState => {
  return {
    isExist: (name: string) => isExist(nlu, name)
  };
};

const mapDispatchToProps = {
  onAdd: NluActions.create
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CreateContainer);
