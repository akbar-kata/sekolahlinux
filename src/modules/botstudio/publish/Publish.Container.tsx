import React from 'react';
import { connect } from 'react-redux';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import { FormikActions } from 'formik';

import RootStore from 'interfaces/rootStore';
import * as Auth from 'interfaces/auth';

import BOT from 'stores/bot/types';
import { getPreparedBot } from 'stores/bot/selectors';
import { getAuthSelected } from 'stores/user/auth/selectors';
import { updateBotRequest } from 'stores/bot/actions';
import { isModalOpen } from 'stores/app/modal/selectors';
import { closeModal, openModal } from 'stores/app/modal/actions';
import { getProjectSelected } from 'stores/project/selectors';
import { getLoading } from 'stores/app/loadings/selectors';

import { isRootURL } from 'utils/url';

import Publish from './Publish';

interface PropsFromState {
  isOpen: boolean;
  selectedBot: string | null;
  getPreparedData: Function;
  isSaving: boolean;
  user: Auth.Selected | null;
}

interface PropsFromDispatch {
  openModal: () => ReturnType<typeof openModal>;
  closeModal: () => ReturnType<typeof closeModal>;
  publish: (id: string, increment: string, data: any) => void;
}

export interface PublishContainerProps
  extends PropsFromState,
    PropsFromDispatch,
    RouteComponentProps<any> {}

export interface PublishContainerState {
  increment: string;
  changelog: string;
}

export const MODAL_ID = 'bot-publish-revision';

class PublishContainer extends React.Component<
  PublishContainerProps,
  PublishContainerState
> {
  state = {
    increment: 'patch',
    changelog: ''
  };

  doPublish = (values: any, actions: FormikActions<any>) => {
    const preparedValues = { ...this.props.getPreparedData(), ...values };

    if (this.props.selectedBot) {
      this.props.publish(
        this.props.selectedBot,
        this.state.increment,
        preparedValues
      );
    }
  };

  onPublish = () => {
    this.props.openModal();
  };

  shouldBeHidden() {
    if (/bot\/version$/.test(this.props.location.pathname)) {
      return true;
    }
    return false;
  }

  render() {
    return (
      !isRootURL(this.props.location.pathname) &&
      !this.shouldBeHidden() && (
        <Publish
          onPublish={this.onPublish}
          doPublish={this.doPublish}
          {...this.props}
        />
      )
    );
  }
}

const mapStateToProps = ({
  app,
  project,
  bot,
  auth
}: RootStore): PropsFromState => {
  return {
    isOpen: isModalOpen(app.modal, MODAL_ID),
    selectedBot: getProjectSelected(project),
    user: getAuthSelected(auth),
    isSaving: getLoading(app.loadings, BOT.UPDATE_REQUEST),
    getPreparedData: () => getPreparedBot(bot)
  };
};

const mapDispatchToProps = (dispatch: Function): PropsFromDispatch => {
  return {
    openModal: () => dispatch(openModal(MODAL_ID)),
    closeModal: () => dispatch(closeModal(MODAL_ID)),
    publish: (id: string, increment: string, data: any) =>
      dispatch(updateBotRequest(id, increment, data))
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(PublishContainer)
);
