import React from 'react';
import PublishContainer from './Publish.Container';

import './style.scss';

const PublishBot: React.SFC = () => (
  <div className="kata-publish">
    <PublishContainer />
  </div>
);

export default PublishBot;
