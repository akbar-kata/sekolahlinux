import React, { Fragment } from 'react';

import { Formik, Form, FormikActions } from 'formik';

import { Button } from 'components/Button';
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'components/Modal';
import { Field } from 'components/FormikWrapper';
import SavingIndicator from 'modules/botstudio/saving';

import { PublishContainerProps } from './Publish.Container';

interface Props extends PublishContainerProps {
  onPublish: () => void;
  doPublish: (values: any, actions: FormikActions<any>) => void;
}

const Publish: React.SFC<Props> = props => (
  <Fragment>
    <div className="kata-publish__save-container">
      <SavingIndicator />
    </div>
    <div>
      <Button color="primary" onClick={props.onPublish}>
        Publish
      </Button>
    </div>

    <Modal show={props.isOpen} onClose={props.closeModal}>
      <Formik initialValues={{ changelog: '' }} onSubmit={props.doPublish}>
        {({ submitForm }) => (
          <Form>
            <ModalHeader onClose={props.closeModal}>
              Publish Version
            </ModalHeader>
            <ModalBody>
              <div className="kata-info__container">
                <div className="kata-info__label">
                  You are publishing a new revision, what's new on this
                  revision?
                </div>
              </div>

              <div className="kata-info__container">
                <div className="kata-info__label">Changelog</div>
                <div className="kata-info__content">
                  <Field
                    component="textarea"
                    name="changelog"
                    rows={10}
                    className="form-control"
                  />
                </div>
              </div>
            </ModalBody>
            <ModalFooter>
              <Button
                color="primary"
                type="submit"
                loading={props.isSaving}
                onClick={submitForm}
              >
                Publish
              </Button>
              <Button
                color="secondary"
                type="button"
                onClick={props.closeModal}
              >
                Close
              </Button>
            </ModalFooter>
          </Form>
        )}
      </Formik>
    </Modal>
  </Fragment>
);

export default Publish;
