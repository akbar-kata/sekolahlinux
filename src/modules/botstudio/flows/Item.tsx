import React, { Component } from 'react';

import { Button } from 'components/Button';
import {
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from 'components/Dropdown';

interface Props {
  selected: string;
  id: string;
  data: any;
  onSelect(id: string);
  onEdit(id: string, data: any);
  onDelete(id: string, name: string);
}

interface States {}

class Item extends Component<Props, States> {
  render() {
    const { selected, id, data, onSelect, onEdit, onDelete } = this.props;
    return (
      <div
        className={`kata-flow-item ${
          selected === id ? 'kata-flow-item--selected' : ''
        }`}
        key={id}
        onClick={onSelect(id)}
      >
        <div className="kata-flow-item__draggable">
          <i className="icon-drag icon-middle" />
        </div>
        <div className="kata-flow-item__content">{data.name} </div>
        <div className="kata-flow-item__action">
          <Dropdown>
            <DropdownToggle caret={false}>
              <Button isIcon>
                <i className="icon-more" />
              </Button>
            </DropdownToggle>
            <DropdownMenu right>
              <DropdownItem onClick={onEdit(id, data)}>Update</DropdownItem>
              <DropdownItem onClick={onDelete(id, data.name)}>
                Delete
              </DropdownItem>
            </DropdownMenu>
          </Dropdown>
        </div>
      </div>
    );
  }
}

export default Item;
