import React, { ReactElement } from 'react';
import { connect } from 'react-redux';

import Store from 'interfaces/rootStore';
import * as FlowAction from 'stores/bot/flow/actions';
import {
  getData,
  getIndex,
  getSelected,
  isExist
} from 'stores/bot/flow/selectors';
import { addNotification } from 'stores/app/notification/actions';

interface PropsFromState {
  index: string[];
  data: any;
  selected?: string | null;
  isExist: Function;
}

interface PropsFromDispatch {
  onSelect: Function;
  onSwap: Function;
  onCreate: Function;
  onUpdate: Function;
  onRemove: Function;
  showNotif(title: string, message: string): void;
}

interface Props extends PropsFromDispatch, PropsFromState {
  children(api: PropsFromDispatch & PropsFromState): ReactElement<any>;
}

interface States {}

class FlowContainer extends React.Component<Props, States> {
  render() {
    const { children, ...rest } = this.props;
    return children(rest);
  }
}

const mapStateToProps = ({ bot: { flow } }: Store): PropsFromState => {
  return {
    data: getData(flow),
    index: getIndex(flow),
    selected: getSelected(flow),
    isExist: (name: string, id?: string) => isExist(flow, name, id)
  };
};

const mapDispatchToProps: PropsFromDispatch = {
  onSelect: FlowAction.select,
  onSwap: FlowAction.swap,
  onCreate: FlowAction.create,
  onUpdate: FlowAction.update,
  onRemove: FlowAction.remove,
  showNotif: (title: string, message: string) =>
    addNotification({
      title,
      message,
      status: 'success',
      dismissible: true,
      dismissAfter: 5000
    })
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FlowContainer);
