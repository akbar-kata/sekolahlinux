import React, { Fragment } from 'react';
import { Scrollbars } from 'react-custom-scrollbars';

import { withDragDropContext, Sortable } from 'components/Common';
import { Button } from 'components/Button';
import { confirm } from 'components/Modal';
import FlowForm from './FlowForm';
import Item from './Item';

interface Props {
  data: any;
  index: string[];
  selected?: string | null;
  isExist: Function;
  onSelect: Function;
  onSwap: Function;
  onCreate: Function;
  onUpdate: Function;
  onRemove: Function;
  showNotif(title: string, message: string): void;
}

interface States {
  newOpen: boolean;
  editOpen: boolean;
  selectedId?: string;
  selectedData?: any;
}

class Flow extends React.Component<Props, States> {
  constructor(props: Props) {
    super(props);
    this.state = {
      newOpen: false,
      editOpen: false
    };
  }

  onMove = (dragIndex, hoverIndex) => {
    this.props.onSwap(dragIndex, hoverIndex);
  };

  openNew = () => {
    this.setState({ newOpen: true });
  };

  closeNew = () => {
    this.setState({ newOpen: false });
  };

  openEdit = (id?: string, data?: any) => event => {
    event.preventDefault(); // Let's stop this event.
    event.stopPropagation(); // Really this time.
    this.setState(
      {
        selectedId: id,
        selectedData: data
      },
      () => {
        this.setState({ editOpen: true });
      }
    );
  };

  closeEdit = () => {
    this.setState(
      {
        selectedId: undefined,
        selectedData: undefined
      },
      () => {
        this.setState({ editOpen: false });
      }
    );
  };

  onAdd = data => {
    const newAction = this.props.onCreate(data);
    if (newAction && newAction.payload) {
      this.props.onSelect(Object.keys(newAction.payload)[0]);
    }
    this.closeNew();
    this.props.showNotif(
      `${data.name} Flow Created`,
      `You have successfully created ${data.name} flow.`
    );
  };

  onEdit = id => data => {
    this.props.onUpdate(id, data);
    this.closeEdit();
    this.props.showNotif(
      `${data.name} Flow Updated`,
      `You have successfully updated ${data.name} flow.`
    );
  };

  onDelete = (id, name) => event => {
    event.preventDefault(); // Let's stop this event.
    event.stopPropagation(); // Really this time.
    confirm({
      title: 'Delete Flow',
      message: `Are you sure you want to delete this flow?`,
      okLabel: 'Delete',
      cancelLabel: 'Cancel'
    }).then(res => {
      if (res) {
        this.props.onRemove(id);
        this.props.showNotif(
          `${name} Flow Deleted`,
          `${name} flow has been deleted.`
        );
      }
    });
  };

  onSelect = (id: string) => () => {
    this.props.onSelect(id);
  };

  render() {
    const { selected, index, data } = this.props;
    return (
      <Fragment>
        <Button color="primary" block className="mb-2" onClick={this.openNew}>
          Create Flow
        </Button>
        <div className="kata-flow__side-content">
          <Scrollbars autoHide>
            {index.map((id, idx) => (
              <Sortable key={idx} index={idx} id={id} onMove={this.onMove}>
                <Item
                  selected={selected as string}
                  id={id}
                  data={data[id]}
                  onSelect={this.onSelect}
                  onEdit={this.openEdit}
                  onDelete={this.onDelete}
                />
              </Sortable>
            ))}
          </Scrollbars>
        </div>
        <FlowForm
          isOpen={this.state.newOpen}
          isExist={this.props.isExist}
          onSubmit={this.onAdd}
          onCancel={this.closeNew}
        />
        <FlowForm
          flowId={this.state.selectedId}
          data={this.state.selectedData}
          isOpen={this.state.editOpen}
          isExist={this.props.isExist}
          onSubmit={this.onEdit(this.state.selectedId)}
          onCancel={this.closeEdit}
        />
      </Fragment>
    );
  }
}

export default withDragDropContext(Flow);
