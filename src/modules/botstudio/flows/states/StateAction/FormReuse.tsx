import React, { Component, Fragment } from 'react';

import { Text, FormError } from 'components/Form';
import { FloatingButton } from 'components/Button';
import ActionSelect from 'modules/botstudio/flows/actions/ActionSelect';
import { SubForms } from 'modules/botstudio/flows/actions/forms';

interface Props {
  type: 'create' | 'update';
  api: any;
  options: any[];
  getAction(name: string): any;
}

class FormReuse extends Component<Props> {
  renderOptions = (type: string, formApi) => {
    if (!SubForms[type]) {
      return null;
    }
    return SubForms[type](formApi, this.props.type === 'create' ? true : false);
  };

  render() {
    const { values, setValue } = this.props.api;
    return (
      <Fragment>
        <div className="kata-info__container">
          <div className="kata-info__label">Action Name</div>
          <div className="kata-info__content">
            <div className="row no-gutters">
              <div className="col">
                <ActionSelect
                  field={`name`}
                  options={this.props.options}
                  onChange={val => {
                    const action = this.props.getAction(val);
                    setValue('id', action.id);
                    setValue('type', action.type);
                    setValue('options', action.options);
                  }}
                />
              </div>
              <div className="ml-1">
                <FloatingButton
                  icon="add"
                  onClick={() => {
                    setValue('formType', 'new');
                    setValue('type', false);
                  }}
                />
              </div>
            </div>
          </div>
        </div>
        <div className="kata-info__container">
          <div className="kata-info__label">Condition</div>
          <div className="kata-info__content">
            <Text
              className="form-control kata-form__input-text"
              field={`condition`}
            />
          </div>
        </div>
        <div className="kata-info__container">
          <FormError field="options" />
          {this.renderOptions(values.type, this.props.api)}
        </div>
      </Fragment>
    );
  }
}

export default FormReuse;
