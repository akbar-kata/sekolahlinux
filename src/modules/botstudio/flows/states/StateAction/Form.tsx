import React, { Component } from 'react';

import { Form } from 'components/Form';
import { SupportButton } from 'components/Button';
import actionIcons from 'modules/botstudio/flows/actions/actionIcons';
import { SubValidate } from 'modules/botstudio/flows/actions/forms';
import { confirm } from 'components/Modal';

import FormNew from './FormNew';
import FormReuse from './FormReuse';

interface Props {
  type: 'create' | 'update';
  isOpen?: boolean;
  data?: any;
  actionIndex: any[];
  actionData: any;
  isExist: Function;
  onOpen?();
  onSubmit?(data: any);
  onCancel?();
  onRemove?();
  onPermanentDelete?(id: string);
}

interface States {
  action: string | null;
}

class StateActionForm extends Component<Props, States> {
  formRef: Form | null;

  constructor(props: Props) {
    super(props);
    this.state = {
      action: null
    };
  }

  onDeletePermanently = () => {
    confirm({
      title: 'Delete Action',
      message: 'Are you sure you want to delete this action?',
      okLabel: 'Delete',
      cancelLabel: 'Cancel'
    }).then(res => {
      if (this.formRef) {
        const id = this.formRef.getValue('id', null);
        if (id && this.props.onPermanentDelete && this.props.onRemove) {
          this.props.onRemove();
          this.props.onPermanentDelete(id);
        }
      }
    });
  };

  getAction = (name: string) => {
    const { actionIndex, actionData } = this.props;
    const actIdx = actionIndex.findIndex(idx => actionData[idx].name === name);
    return {
      id: actionIndex[actIdx],
      ...(actionData[actionIndex[actIdx]] || {})
    };
  };

  getDefaultValue = () => {
    const action = this.getAction((this.props.data || {}).name);
    return {
      ...this.props.data,
      id: action.id,
      type: action.type,
      options: { ...action.options }
    };
  };

  onValidate = ({ formType, name, type, options }) => {
    function validateOptions() {
      if (!SubValidate[type]) {
        return undefined;
      }
      return SubValidate[type](options);
    }
    return {
      name: !name
        ? 'A name is required'
        : !/^[A-Za-z][-A-Za-z0-9_-]*$/.test(name)
        ? 'Name only allowed charaters alphanumeric, _ and -'
        : formType === 'new' && this.props.isExist(name)
        ? 'Name already used, pick another name'
        : undefined,
      options: !options ? 'Options must be defined' : validateOptions()
    };
  };

  onSubmit = data => {
    if (this.props.onSubmit) {
      this.props.onSubmit(data);
    }
    if (this.props.onCancel) {
      this.props.onCancel();
    }
  };

  onRemove = e => {
    e.preventDefault();

    if (this.props.onRemove) {
      this.props.onRemove();
    }
  };

  renderClosed = ({ values }) => {
    return (
      <div className="kata-action__new-wrapper" onClick={this.props.onOpen}>
        <i className="icon-drag icon-middle kata-action__draggable" />
        <i
          className={`kata-action-header__icon icon-${actionIcons[
            values.type
          ] || 'text'} mr-2`}
        />
        {values.name}
        <i
          className="icon-remove kata-action__delete-btn"
          onClick={this.onRemove}
        />
      </div>
    );
  };

  renderForm = (formApi: any) => {
    if (!this.props.isOpen) {
      return this.renderClosed(formApi);
    }
    const { actionIndex, actionData } = this.props;
    const options = actionIndex.map(idx => ({
      label: actionData[idx].name,
      value: actionData[idx].name,
      type: actionData[idx].type
    }));
    return (
      <div
        className={`kata-action__wrapper ${
          formApi.values.formType === 'new' || formApi.values.name
            ? 'kata-action__wrapper--blue'
            : ''
        }`}
      >
        {formApi.values.formType === 'new' ? (
          <FormNew api={formApi} />
        ) : (
          <FormReuse
            type={this.props.type}
            api={formApi}
            options={options}
            getAction={this.getAction}
          />
        )}
        <div className="kata-action__footer">
          {formApi.values.id ? (
            <SupportButton
              className="kata-action__footer-delete kata-btn__danger"
              onClick={this.onDeletePermanently}
            >
              Delete Action
            </SupportButton>
          ) : null}
          {formApi.values.type && (
            <SupportButton
              className="text-capitalize kata-btn__primary mr-1"
              onClick={formApi.submitForm}
            >
              {this.props.type} Action
            </SupportButton>
          )}
          <SupportButton
            onClick={() => {
              if (this.props.onCancel) {
                this.props.onCancel();
              } else {
                if (this.props.onOpen) {
                  this.props.onOpen();
                }
              }
              formApi.setAllValues(this.getDefaultValue());
            }}
          >
            Cancel
          </SupportButton>
        </div>
      </div>
    );
  };

  render() {
    return (
      <Form
        ref={r => (this.formRef = r)}
        defaultValues={this.getDefaultValue()}
        validate={this.onValidate}
        onSubmit={this.onSubmit}
      >
        {this.renderForm}
      </Form>
    );
  }
}

export default StateActionForm;
