import React, { Component, Fragment } from 'react';

import { Text, FormError } from 'components/Form';
import ActionTypeSelect from 'modules/botstudio/flows/actions/ActionTypeSelect';
import actionTypes from 'modules/botstudio/flows/actions/actionTypes';
import { SubForms } from 'modules/botstudio/flows/actions/forms';

interface Props {
  api: any;
}

class FormNew extends Component<Props> {
  renderHeader = (type: string) => {
    const index = actionTypes.findIndex(item => item.value === type);
    if (index === -1) {
      return null;
    }
    return (
      <div className="kata-action-header">
        <i
          className={`kata-action-header__icon icon-${
            actionTypes[index].icon
          } mr-2`}
        />
        {actionTypes[index].fulltext || actionTypes[index].text}
      </div>
    );
  };

  renderOptions = (type: string, formApi) => {
    if (!SubForms[type]) {
      return null;
    }
    return SubForms[type](formApi);
  };

  render() {
    const { values, setValue } = this.props.api;
    return (
      <Fragment>
        {values.type ? (
          <div>
            <div onClick={() => setValue('type', undefined)}>
              {this.renderHeader(values.type)}
            </div>
            <div className="kata-info__container">
              <label className="kata-info__label">Name</label>
              <div className="kata-info__content">
                <Text
                  field="name"
                  className="form-control kata-form__input-text"
                />
              </div>
            </div>
            <div className="kata-info__container">
              <div className="kata-info__label">Condition</div>
              <div className="kata-info__content">
                <Text
                  className="form-control kata-form__input-text"
                  field={`condition`}
                />
              </div>
            </div>
            <div className="kata-info__container">
              <FormError field="options" />
              {this.renderOptions(values.type, this.props.api)}
            </div>
          </div>
        ) : (
          <div className="mb-3">
            <ActionTypeSelect
              onSelect={(value: string) => {
                setValue('type', value);
                setValue('name', '');
                setValue('condition', '');
                setValue('options', {});
              }}
            />
          </div>
        )}
      </Fragment>
    );
  }
}

export default FormNew;
