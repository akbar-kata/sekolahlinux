import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import shortid from 'shortid';
import get from 'lodash-es/get';

import Store from 'interfaces/rootStore';
import { getSelected as getSelectedFlow } from 'stores/bot/flow/selectors';
import { isExist } from 'stores/bot/action/selectors';
import * as Actions from 'stores/bot/action/actions';

import ActionContainer from 'modules/botstudio/flows/actions/ActionsContainer';
import { SupportButton } from 'components/Button';
import withDragDropContext from 'components/Common/WithDragDropContext';
import Sortable from 'components/Common/Sortable';
import Form from './Form';

interface PropsFromState {
  activeFlow?: string | null;
  isExist: Function;
}

interface PropsFromDispatch {
  addAction: Function;
  editAction: Function;
  permanentDeleteAction: (id: string) => void;
}

interface Props extends PropsFromState, PropsFromDispatch {
  api: any;
  data?: any[];
}

interface States {
  openId: number | 'new' | null;
}

class StateAction extends Component<Props, States> {
  state = {
    openId: null
  };

  onOpen = id => {
    this.setState({
      openId: id
    });
  };

  onSwap = (from, to) => {
    const actions = get(this.props.api, 'values.action', []);
    if (actions[from] === undefined || actions[to] === undefined) {
      return;
    }
    const a = from > to ? to : from;
    const b = from > to ? from : to;
    this.props.api.setValue('action', [
      ...actions.slice(0, a),
      actions[b],
      ...actions.slice(a + 1, b),
      actions[a],
      ...actions.slice(b + 1)
    ]);
  };

  onCreate = data => {
    this.props.api.addValue('action', {
      id: shortid.generate(),
      name: data.name,
      condition: data.condition
    });
    const newData = {
      name: data.name,
      type: data.type,
      options: data.options
    };
    if (data.formType === 'new') {
      this.props.addAction(this.props.activeFlow, newData);
    }
  };

  onEdit = (index: number, data) => {
    const actions = this.props.data || [];
    this.props.api.setValue('action', [
      ...actions.slice(0, index),
      {
        name: data.name,
        condition: data.condition
      },
      ...actions.slice(index + 1)
    ]);
    const newData = {
      name: data.name,
      type: data.type,
      options: data.options
    };
    if (data.formType === 'new') {
      this.props.addAction(this.props.activeFlow, newData);
    } else {
      this.props.editAction(this.props.activeFlow, data.id, newData);
    }
  };

  render() {
    const { data, api } = this.props;
    return (
      <ActionContainer>
        {props => (
          <Fragment>
            {data && data.length
              ? data.map((action, index) => (
                  <Sortable
                    key={index}
                    index={index}
                    id={action.id}
                    onMove={this.onSwap}
                  >
                    <Form
                      type="update"
                      data={action}
                      isOpen={this.state.openId === index}
                      actionIndex={props.index}
                      actionData={props.data}
                      isExist={this.props.isExist}
                      onSubmit={args => this.onEdit(index, args)}
                      onRemove={() => api.removeValue('action', index)}
                      onPermanentDelete={this.props.permanentDeleteAction}
                      onOpen={() => this.onOpen(index)}
                      onCancel={() => this.onOpen(null)}
                    />
                  </Sortable>
                ))
              : null}
            {this.state.openId === 'new' ? (
              <Form
                type="create"
                isOpen
                actionIndex={props.index}
                actionData={props.data}
                isExist={this.props.isExist}
                onCancel={() => this.onOpen(null)}
                onSubmit={this.onCreate}
              />
            ) : (
              <SupportButton
                className="d-block mt-1"
                onClick={() => this.onOpen('new')}
              >
                Add actions
              </SupportButton>
            )}
          </Fragment>
        )}
      </ActionContainer>
    );
  }
}

const mapStateToProps = ({ bot: { action, flow } }: Store): PropsFromState => {
  const activeFlow = getSelectedFlow(flow);
  return {
    activeFlow,
    isExist: (name: string, id?: string) =>
      isExist(action, activeFlow as string, name, id)
  };
};

const mapDispatchToProps: PropsFromDispatch = {
  addAction: Actions.create,
  editAction: Actions.update,
  permanentDeleteAction: Actions.remove
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withDragDropContext(StateAction));
