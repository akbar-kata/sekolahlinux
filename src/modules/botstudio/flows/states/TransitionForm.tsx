import React, { Fragment } from 'react';

import {
  Drawer,
  DrawerHeader,
  DrawerBody,
  DrawerFooter
} from 'components/Drawer';
import { Button, FloatingButton, SupportButton } from 'components/Button';
import { Form, Text, ReactToggle } from 'components/Form';
import MethodSelector from 'containers/MethodSelector';

interface Props {
  type: 'create' | 'update';
  isOpen: boolean;
  nodes: {
    [key: string]: any;
  };
  value?: {
    [key: string]: any;
  };
  onSubmit: Function;
  onCancel: Function;
  onDelete?: Function;
}

interface States {
  activeTab: string;
  value: { [key: string]: any };
}

class TransitionForm extends React.Component<Props, States> {
  public static defaultProps: Partial<Props> = {
    value: {
      transition: {},
      _opts: {
        mappingMode: 'simple'
      }
    }
  };

  constructor(props: Props) {
    super(props);
    this.state = {
      activeTab: '1',
      value: {
        transition: {}
      }
    };
  }

  componentWillReceiveProps(next: Props) {
    if (next.isOpen) {
      const { value = {}, nodes } = next;
      this.setState({
        value: {
          ...value,
          stateFrom: value.from ? nodes[value.from].name : '',
          stateTo: value.to ? nodes[value.to].name : ''
        }
      });
    }
  }

  selectTab = key => {
    this.setState({
      activeTab: key
    });
  };

  closeForm = () => {
    this.props.onCancel();
  };

  onValidate = values => {
    const {
      transition: { priority }
    } = values;
    return {
      transition: {
        priority:
          priority && priority < 0 ? 'Priority must greater than 0!' : undefined
      }
    };
  };

  onSubmit = data => {
    this.props.onSubmit(
      Object.assign({}, this.props.value, {
        transition: data.transition
      })
    );
  };

  innerForm = ({
    values,
    submitForm,
    setValue,
    addValue,
    editValue,
    removeValue,
    getError
  }) => (
    <Fragment>
      <DrawerHeader title={`${this.props.type} Transition`} />
      <DrawerBody>
        <form className="full-size" onSubmit={submitForm}>
          <div className="kata-info__container row no-gutters">
            <div className="col mr-1">
              <div className="kata-info__label">From</div>
              <div className="kata-info__content">
                <Text
                  field="stateFrom"
                  className="form-control kata-form__input-text"
                  disabled
                />
              </div>
            </div>
            <div className="col">
              <div className="kata-info__label">To</div>
              <div className="kata-info__content">
                <Text
                  field="stateTo"
                  className="form-control kata-form__input-text"
                  disabled
                />
              </div>
            </div>
          </div>
          <div className="kata-info__container">
            <div className="kata-info__label">Condition</div>
            <div className="kata-info__content">
              <Text
                autoFocus
                field="transition.condition"
                placeholder="Transition Condition"
                className="form-control kata-form__input-text"
              />
            </div>
          </div>
          <div className="kata-info__container">
            <div className="d-inline-block mr-5">
              <div className="kata-info__label">Priority</div>
              <div className="kata-info__content">
                <Text
                  field="transition.priority"
                  type="number"
                  placeholder="Priority"
                  className="form-control kata-form__input-text kata-form__input-number"
                />
              </div>
            </div>
            <div className="d-inline-block">
              <ReactToggle field="transition.fallback" label="Default" />
            </div>
          </div>
          <div className="kata-info__container">
            <label className="kata-info__label">Mapping mode</label>
            <div className="btn-group kata-btn-group d-flex mb-2">
              <button
                type="button"
                className={`btn col kata-btn-group__item ${
                  values._opts.mappingMode === 'simple'
                    ? 'kata-btn-group__item--selected'
                    : ''
                }`}
                onClick={() => {
                  setValue('_opts.mappingMode', 'simple');
                  // [PLATFORM-1490]
                  // This might not be an optimal solution.
                  // We need to keep this temporarily in the `_opts` property
                  // so we don't lose this option when switching tabs.
                  setValue(
                    '_opts.tempMethodMapping',
                    values.transition.mapping
                  );
                  setValue(
                    'transition.mapping',
                    values._opts.tempSimpleMapping || undefined
                  );
                }}
              >
                Simple Mapping
              </button>
              <button
                type="button"
                className={`btn col kata-btn-group__item ${
                  values._opts.mappingMode === 'method'
                    ? 'kata-btn-group__item--selected'
                    : ''
                }`}
                onClick={() => {
                  setValue('_opts.mappingMode', 'method');
                  // [PLATFORM-1490]
                  // This might not be an optimal solution.
                  // We need to keep this temporarily in the `_opts` property
                  // so we don't lose this option when switching tabs.
                  setValue(
                    '_opts.tempSimpleMapping',
                    values.transition.mapping
                  );
                  setValue(
                    'transition.mapping',
                    values._opts.tempMethodMapping || undefined
                  );
                }}
              >
                Method Mapping
              </button>
            </div>
          </div>
          {(values._opts.mappingMode === 'simple' && (
            <Fragment>
              {values.transition &&
              values.transition.mapping &&
              values.transition.mapping.length ? (
                <Fragment>
                  <div className="d-flex">
                    <div className="kata-info__label" style={{ width: 180 }}>
                      Key
                    </div>
                    <div className="kata-info__label">Value</div>
                  </div>
                  {values.transition.mapping.map((map, index) => (
                    <div key={index} className="d-flex mb-2">
                      <div>
                        <Text
                          field={`transition.mapping.${index}.key`}
                          className="form-control kata-form__input-text"
                        />
                      </div>
                      <div className="px-1 pt-1 font-weight-bold">=</div>
                      <div>
                        <Text
                          field={`transition.mapping.${index}.value`}
                          className="form-control kata-form__input-text"
                        />
                      </div>
                      <div className="pl-1">
                        <FloatingButton
                          icon="trash"
                          color="danger"
                          onClick={() =>
                            removeValue('transition.mapping', index)
                          }
                        />
                      </div>
                    </div>
                  ))}
                </Fragment>
              ) : null}
              <SupportButton onClick={() => addValue('transition.mapping', {})}>
                Add mapping
              </SupportButton>
            </Fragment>
          )) || (
            <div className="kata-info__container">
              <label className="kata-info__label">Select method</label>
              <MethodSelector field="transition.mapping" useRoot />
            </div>
          )}
        </form>
      </DrawerBody>
      <DrawerFooter>
        <Button
          color="primary"
          className="text-capitalize mr-1"
          onClick={submitForm}
        >
          {this.props.type}
        </Button>
        {this.props.type === 'update' ? (
          <Button color="danger" onClick={this.props.onDelete}>
            Delete
          </Button>
        ) : (
          <Button color="secondary" onClick={this.closeForm}>
            Cancel
          </Button>
        )}
      </DrawerFooter>
    </Fragment>
  );

  render() {
    return (
      <Drawer isOpen={this.props.isOpen} onClose={this.closeForm}>
        <Form
          onSubmit={this.onSubmit}
          defaultValues={this.state.value}
          validate={this.onValidate}
        >
          {this.innerForm}
        </Form>
      </Drawer>
    );
  }
}

export default TransitionForm;
