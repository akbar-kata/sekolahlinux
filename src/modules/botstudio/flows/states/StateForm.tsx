import React, { Fragment } from 'react';
import isArray from 'lodash-es/isArray';

import { Form, Text, ReactToggle, FormError } from 'components/Form';
import {
  Drawer,
  DrawerBody,
  DrawerHeader,
  DrawerFooter
} from 'components/Drawer';
import { Banner } from 'components/Banner';
import { Button, SupportButton, FloatingButton } from 'components/Button';
import MethodSelector from 'containers/MethodSelector';
import StateAction from './StateAction/Container';

import { confirm } from 'components/Modal';
import cloneDeep from 'lodash-es/cloneDeep';

interface Props {
  type: 'create' | 'update';
  isOpen: boolean;
  nodeId?: string;
  value?: {
    [key: string]: any;
  };
  isExist: Function;
  onSubmit: Function;
  onCancel: Function;
  onDelete?: Function;
}

interface States {
  activeTab: number;
  touched: boolean;
  previousData: any;
}

const tabs = ['Overview', 'onEnter', 'onTransit', 'onExit', 'Transitions'];

class NodeForm extends React.Component<Props, States> {
  public static defaultProps: Partial<Props> = {
    value: {
      action: [],
      style: {
        top: 150,
        left: 150
      },
      _opts: {
        enterMappingMode: 'simple',
        transitMappingMode: 'simple',
        exitMappingMode: 'simple',
        transitionTab: 'fallback',
        floatMappingMode: 'simple',
        fallbackMappingMode: 'simple'
      }
    }
  };

  formRef: Form | null;

  constructor(props: Props) {
    super(props);
    this.state = {
      activeTab: 0,
      touched: false,
      previousData: null
    };
  }
  onChange = e => {
    if (this.state.previousData === null) {
      this.setState({
        previousData: cloneDeep(e.values)
      });
      return;
    }

    // Lodash isEqual does not work :D
    if (JSON.stringify(e.values) !== JSON.stringify(this.state.previousData)) {
      this.setState({
        touched: true
      });
    }
    this.setState({
      previousData: cloneDeep(e.values)
    });
  };

  componentWillUpdate(nextProps: Props) {
    if (nextProps.isOpen !== this.props.isOpen && nextProps.isOpen) {
      this.setState({
        activeTab: 0
      });
    }
  }

  selectTab = key => () => {
    this.setState({
      activeTab: key
    });
  };

  closeForm = () => {
    if (!this.state.touched) {
      this.props.onCancel();
      return;
    }
    confirm({
      title: 'Changes Not Saved Yet....',
      message:
        'Are you sure you want to leave this page? Some changes will not be saved yet... Click Update to save it.',
      okLabel: 'Update',
      cancelLabel: 'Leave'
    }).then(result => {
      if (!result) {
        this.props.onCancel();
      } else {
        if (this.formRef) {
          // We manually trigger the submit form event
          this.props.onSubmit(this.state.previousData);
        }
      }
    });
  };

  validateMap = value => {
    return value && isArray(value)
      ? value.map(e => {
          return {
            key:
              !e.key && e.value ? 'A Key to map need to be defined' : undefined,
            value: !e.value && e.key ? 'Value need to be defined' : undefined
          };
        })
      : undefined;
  };

  validateTransition = (options, value) => {
    return options[`enable${value}`] && value
      ? {
          priority:
            value.priority && value.priority < 0
              ? 'Priority must greater than 0!'
              : undefined,
          mapping: this.validateMap(value.mapping)
        }
      : undefined;
  };

  onValidate = values => {
    const {
      name,
      action,
      onEnter,
      onTransit,
      onExit,
      float,
      initial,
      fallback,
      _opts
    } = values;
    return {
      name: !name
        ? 'A name is required'
        : !/^[A-Za-z][-A-Za-z0-9_-]*$/.test(name)
        ? 'Name only allowed charaters alphanumeric, _ and -'
        : this.props.isExist(name, this.props.nodeId)
        ? 'Name already used, pick another name'
        : undefined,
      action:
        !initial && (!action || !action.length)
          ? 'Action is required'
          : undefined,
      onEnter: this.validateMap(onEnter),
      onTransit: this.validateMap(onTransit),
      onExit: this.validateMap(onExit),
      float: this.validateTransition(_opts, float),
      nofloat:
        _opts.enablefloat &&
        (!float || (!float.condition && !float.priority && !float.fallback))
          ? 'At least fill one floating field'
          : undefined,
      fallback: this.validateTransition(_opts, fallback)
    };
  };

  onValidationFail = (values, state) => {
    const {
      name,
      action,
      onEnter,
      onTransit,
      onExit,
      float,
      noFloat
    } = state.errors;
    if (name || action) {
      this.selectTab(0)();
    } else if (onEnter !== undefined) {
      this.selectTab(1)();
    } else if (onTransit !== undefined) {
      this.selectTab(2)();
    } else if (onExit !== undefined) {
      this.selectTab(3)();
    } else if (float !== undefined || noFloat !== undefined) {
      this.selectTab(4)();
    }
  };

  onSubmit = values => {
    const clearMapValue = (mode, map) => {
      if (mode === 'simple' && map && map.length) {
        const newMap = map.filter(item => item.key && item.value);
        return newMap.length > 0 ? newMap : undefined;
      }
      return map;
    };

    const newVal = Object.assign({}, values);
    newVal.onEnter = clearMapValue(
      newVal._opts.enterMappingMode,
      newVal.onEnter
    );
    newVal.onTransit = clearMapValue(
      newVal._opts.transitMappingMode,
      newVal.onTransit
    );
    newVal.onExit = clearMapValue(newVal._opts.exitMappingMode, newVal.onExit);
    if (newVal.float && newVal.float.mapping) {
      newVal.float.mapping = clearMapValue(
        newVal._opts.floatMappingMode,
        newVal.float.mapping
      );
    }
    this.props.onSubmit(newVal);
  };

  renderMappingMode = (formApi, field, mapField, className = 'text-center') => {
    function mappingModeClick(value: string) {
      formApi.setValue(`_opts.${field}`, value);
      formApi.setValue(mapField, undefined);
    }

    const _opts = formApi.values._opts;
    return (
      <div className="btn-group kata-btn-group d-flex mb-2">
        <button
          type="button"
          className={`btn col kata-btn-group__item ${
            _opts[field] === 'simple' ? 'kata-btn-group__item--selected' : ''
          }`}
          onClick={() => mappingModeClick('simple')}
        >
          Simple Mapping
        </button>
        <button
          type="button"
          className={`btn col kata-btn-group__item ${
            _opts[field] === 'method' ? 'kata-btn-group__item--selected' : ''
          }`}
          onClick={() => mappingModeClick('method')}
        >
          Method Mapping
        </button>
      </div>
    );
  };

  renderFormMap = (formApi, field, val) => (
    <Fragment>
      {val && val.length ? (
        <Fragment>
          <div className="d-flex">
            <div className="kata-info__label" style={{ width: 180 }}>
              Key
            </div>
            <div className="kata-info__label">Value</div>
          </div>
          {val.map((map, index) => (
            <div key={index} className="d-flex mb-2">
              <div>
                <Text
                  field={`${field}.${index}.key`}
                  className="form-control kata-form__input-text"
                />
              </div>
              <div className="px-1 pt-1 font-weight-bold">=</div>
              <div>
                <Text
                  field={`${field}.${index}.value`}
                  className="form-control kata-form__input-text"
                />
              </div>
              <div className="pl-1">
                <FloatingButton
                  icon="trash"
                  color="danger"
                  onClick={() => formApi.removeValue(field, index)}
                />
              </div>
            </div>
          ))}
        </Fragment>
      ) : null}
      <SupportButton onClick={() => formApi.addValue(field, {})}>
        Add mapping
      </SupportButton>
    </Fragment>
  );

  renderTransitionTab = formApi => {
    const { values, setValue } = formApi;
    return (
      <div className="btn-group kata-btn-group d-flex mb-2">
        <button
          type="button"
          className={`btn col kata-btn-group__item ${
            values._opts.transitionTab === 'fallback'
              ? 'kata-btn-group__item--selected'
              : ''
          }`}
          onClick={() => setValue(`_opts.transitionTab`, 'fallback')}
        >
          Self Transition
        </button>
        <button
          type="button"
          className={`btn col kata-btn-group__item ${
            values._opts.transitionTab === 'float'
              ? 'kata-btn-group__item--selected'
              : ''
          }`}
          onClick={() => setValue(`_opts.transitionTab`, 'float')}
        >
          Floating Transition
        </button>
      </div>
    );
  };

  renderTransitionForm = (formApi, field, label, isDefault?: boolean) => {
    const { values, setValue } = formApi;
    return (
      <Fragment>
        <div className="kata-info__container">
          <ReactToggle
            field={`_opts.enable${field}`}
            label={label}
            onChange={(e: any, handler: Function) => {
              setValue(field, undefined);
              if (isDefault && e.target.checked) {
                setValue(field, { fallback: true });
              }
              handler(e);
            }}
          />
        </div>
        {values._opts[`enable${field}`] && (
          <Fragment>
            <FormError field={`no${field}`} />
            <div className="kata-info__container d-flex">
              <div style={{ width: 256 }} className="pr-1">
                <div className="kata-info__label">Condition</div>
                <div className="kata-info__content">
                  <Text
                    field={`${field}.condition`}
                    placeholder="Condition"
                    className="form-control kata-form__input-text"
                  />
                </div>
              </div>
              <div>
                <div className="kata-info__label">Priority</div>
                <div className="kata-info__content">
                  <Text
                    type="number"
                    field={`${field}.priority`}
                    placeholder="Priority"
                    className="form-control kata-form__input-text kata-form__input-number"
                  />
                </div>
              </div>
            </div>
            <div className="kata-info__container">
              <ReactToggle field={`${field}.fallback`} label="Default" />
            </div>
            <div className="kata-info__container">
              <div className="kata-info__label">Mapping mode</div>
              <div className="kata-info__content">
                {this.renderMappingMode(
                  formApi,
                  `${field}MappingMode`,
                  `${field}.mapping`,
                  ''
                )}
                {(values._opts[`${field}MappingMode`] === 'simple' &&
                  this.renderFormMap(
                    formApi,
                    `${field}.mapping`,
                    values[field] ? values[field]['mapping'] : undefined
                  )) || (
                  <div className="mb4">
                    <label>Select method</label>
                    <MethodSelector field={`${field}.mapping`} useRoot />
                  </div>
                )}
              </div>
            </div>
          </Fragment>
        )}
      </Fragment>
    );
  };

  renderInnerForm = formApi => {
    const { values, errors, submitForm } = formApi;
    const { activeTab } = this.state;

    return (
      <Fragment>
        <DrawerHeader title={`${this.props.type} State`} />
        <DrawerBody className="pt-0">
          <form className="full-size" onSubmit={submitForm}>
            <ul className="kata-simple-tab">
              {tabs.map((tab, index) => (
                <li key={index} role="presentation">
                  <a
                    href="#"
                    className={`kata-simple-tab__item ${
                      index === activeTab ? 'kata-simple-tab__item--active' : ''
                    }`}
                    onClick={this.selectTab(index)}
                  >
                    {tab}
                  </a>
                </li>
              ))}
            </ul>
            {activeTab === 0 && (
              <Fragment>
                <div className="kata-info__container">
                  <div className="kata-info__label">Name</div>
                  <div className="kata-info__content">
                    <Text
                      autoFocus
                      field="name"
                      placeholder="State name"
                      className="form-control kata-form__input-text"
                    />
                  </div>
                </div>
                <div className="kata-info__container">
                  <div className="d-inline-block mr-5">
                    <ReactToggle field="initial" label="Initial State" />
                  </div>
                  <div className="d-inline-block">
                    <ReactToggle field="end" label="End State" />
                  </div>
                </div>
                <div className="kata-info__container">
                  <div className="kata-info__label">Action</div>
                  {errors && errors.action && (
                    <Banner
                      className="mb-2"
                      state="error"
                      message="Action is required"
                    />
                  )}
                  <StateAction api={formApi} data={values.action} />
                </div>
              </Fragment>
            )}
            {activeTab === 1 && (
              <Fragment>
                {this.renderMappingMode(formApi, 'enterMappingMode', 'onEnter')}
                {(values._opts.enterMappingMode === 'simple' &&
                  this.renderFormMap(
                    formApi,
                    'onEnter',
                    values['onEnter']
                  )) || (
                  <div>
                    <div className="kata-info__label">Select Method</div>
                    <MethodSelector field="onEnter" useRoot />
                  </div>
                )}
              </Fragment>
            )}
            {activeTab === 2 && (
              <Fragment>
                {this.renderMappingMode(
                  formApi,
                  'transitMappingMode',
                  'onTransit'
                )}
                {(values._opts.transitMappingMode === 'simple' &&
                  this.renderFormMap(
                    formApi,
                    'onTransit',
                    values['onTransit']
                  )) || (
                  <div>
                    <div className="kata-info__label">Select Method</div>
                    <MethodSelector field="onTransit" useRoot />
                  </div>
                )}
              </Fragment>
            )}
            {activeTab === 3 && (
              <Fragment>
                {this.renderMappingMode(formApi, 'exitMappingMode', 'onExit')}
                {(values._opts.exitMappingMode === 'simple' &&
                  this.renderFormMap(formApi, 'onExit', values['onExit'])) || (
                  <div>
                    <div className="kata-info__label">Select Method</div>
                    <MethodSelector field="onExit" useRoot />
                  </div>
                )}
              </Fragment>
            )}
            {activeTab === 4 && (
              <Fragment>
                {this.renderTransitionTab(formApi)}
                {values._opts.transitionTab === 'fallback'
                  ? this.renderTransitionForm(
                      formApi,
                      'fallback',
                      'Self Transition',
                      true
                    )
                  : this.renderTransitionForm(
                      formApi,
                      'float',
                      'Floating Transition'
                    )}
              </Fragment>
            )}
          </form>
        </DrawerBody>
        <DrawerFooter>
          <Button
            color="primary"
            className="text-capitalize mr-1"
            onClick={submitForm}
          >
            {this.props.type}
          </Button>
          {this.props.type === 'update' ? (
            <Button color="danger" onClick={this.props.onDelete}>
              Delete
            </Button>
          ) : (
            <Button color="secondary" onClick={this.closeForm}>
              Cancel
            </Button>
          )}
        </DrawerFooter>
      </Fragment>
    );
  };

  render() {
    return (
      <Drawer isOpen={this.props.isOpen} onClose={this.closeForm}>
        <Form
          ref={e => (this.formRef = e)}
          onChange={this.onChange}
          onSubmit={this.onSubmit}
          defaultValues={this.props.value}
          validate={this.onValidate}
          onValidationFail={this.onValidationFail}
        >
          {this.renderInnerForm}
        </Form>
      </Drawer>
    );
  }
}

export default NodeForm;
