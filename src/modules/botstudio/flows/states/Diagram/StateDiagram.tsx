import React, { Component, Fragment } from 'react';
import { jsPlumb } from 'jsplumb';
import { DraggableCore } from 'react-draggable';
import Toggle from 'react-toggle';

import { Diagram, Connection } from './interface';
import DiagramConfig from './DiagramConfig';
import { SupportButton } from 'components/Button';
import { Drawer, DrawerHeader, DrawerBody } from 'components/Drawer';
import Node from './Node';
import { getLayout } from './layoutHelper';
import legends from './legends';

import './jsPlumb.css';

interface Props {
  flow?: string | null;
  lastUpdated: number;
  nodesIdx: string[];
  nodes: any;
  connsIdx: string[];
  conns: any;
  onNodeArrage: Function;
  onNodeChangePos(data: any);
  onNodeUpdate(data: any);
  onConnCreate(data: any);
  onConnUpdate(data: any);
}

interface States {
  // diagram state
  legend: boolean;
  floating: boolean;
  priority: boolean;
  position: {
    x: number;
    y: number;
  };
  diagram: Diagram;
  nodeHovered: boolean;

  // connection state
  conns: Connection[];
}

const containerId = 'kata-state-diagram';

class StateDiagram extends Component<Props, States> {
  instance: any;
  state: States = {
    legend: false,
    floating: false,
    priority: true,
    position: {
      x: 0,
      y: 0
    },
    diagram: {
      isLoading: true
    },
    nodeHovered: false,

    conns: []
  };

  componentWillMount() {
    const { nodesIdx, nodes } = this.props;
    if (nodesIdx.length > 0 && !nodes[nodesIdx[0]].style) {
      this.arrageLayout();
    }
  }

  componentDidMount() {
    // tslint:disable-next-line:no-this-assignment - legacy code
    const that = this;
    (jsPlumb as any).ready(() => {
      const container = document.querySelector(`#${containerId}`);
      (jsPlumb as any).setContainer(container);
      that.instance = jsPlumb.getInstance(DiagramConfig.default as any);
      that.instance.registerConnectionType('floating', DiagramConfig.floating);
    });
    this.renderGraph();
  }

  componentWillReceiveProps(next: Props) {
    const { nodes, nodesIdx, conns, connsIdx, flow } = next;
    if (nodesIdx.length > 0 && !nodes[nodesIdx[0]].style) {
      this.arrageLayout({ nodes, nodesIdx, conns, connsIdx });
    }
    if (this.props.flow !== flow) {
      this.setState({
        position: {
          x: 0,
          y: 0
        }
      });
    }
  }

  componentDidUpdate(prev: Props) {
    const { flow, lastUpdated } = this.props;
    if (prev.lastUpdated !== lastUpdated || prev.flow !== flow) {
      this.renderGraph();
    }
  }

  onDrag = (e: any, position: any) => {
    const { x, y } = this.state.position;
    this.setState({
      position: {
        x: x + position.deltaX,
        y: y + position.deltaY
      }
    });
  };

  onDragStop = (e: any, position: any) => {
    this.onDrag(e, position);
  };

  getPosition = () => {
    const { position } = this.state;
    return {
      transform: `translate(${position.x}px, ${position.y}px)`
    };
  };

  toggleShow = (name: 'floating' | 'priority') => {
    const newState = {};
    newState[name] = !this.state[name];
    this.setState(newState, () => {
      this.renderGraph();
    });
  };

  openLegend = () => {
    try {
      const root = document.getElementById('root');
      if (root) {
        root.classList.add('pushed-legend-right');
      }
    } catch (err) {
      // do nothing
    }
    this.setState({ legend: true });
  };

  closeLegend = () => {
    try {
      const root = document.getElementById('root');
      if (root) {
        root.classList.remove('pushed-legend-right');
      }
    } catch (err) {
      // do nothing
    }
    this.setState({ legend: false });
  };

  renderNodes() {
    const { nodesIdx, nodes } = this.props;
    return nodesIdx.map(id => (
      <Node
        key={id}
        nodeId={id}
        data={nodes[id]}
        style={nodes[id].style}
        onMouseEnter={this.nodeMouseEnter}
        onMouseLeave={this.nodeMouseLeave}
        onUpdate={this.props.onNodeUpdate}
      />
    ));
  }

  renderLegend() {
    return (
      <Drawer
        isOpen={this.state.legend}
        onClose={this.closeLegend}
        className="kata-state-legend__drawer"
        backdrop={false}
      >
        <DrawerHeader title="Legend" />
        <DrawerBody>
          {legends.map((item, index) => (
            <div className="kata-state-legend__item" key={index}>
              <div className="kata-state-legend__image">
                <img
                  src={require(`./images/${item.image}.svg`)}
                  alt={item.title}
                />
              </div>
              <div className="kata-state-legend__content">
                <div className="kata-state-legend__title">{item.title}</div>
                <div className="kata-state-legend__text">{item.text}</div>
              </div>
            </div>
          ))}
        </DrawerBody>
      </Drawer>
    );
  }

  render() {
    return (
      <Fragment>
        <DraggableCore
          cancel=".kata-state-node"
          onDrag={this.onDrag}
          onStop={this.onDragStop}
        >
          <div
            className={`kata-state ${
              this.state.nodeHovered ? 'kata-state--node-hovered' : ''
            }`}
          >
            <div className="kata-state__action-bar">
              <label className="mr-3">
                <Toggle
                  defaultChecked={this.state.priority}
                  onChange={() => this.toggleShow('priority')}
                />
                <span className="ml-1">Priority</span>
              </label>
              <label className="mr-3">
                <Toggle
                  defaultChecked={this.state.floating}
                  onChange={() => this.toggleShow('floating')}
                />
                <span className="ml-1">Floating</span>
              </label>
              <SupportButton onClick={this.openLegend}>Legend</SupportButton>
            </div>
            <div
              id={containerId}
              className="kata-state__diagram"
              style={this.getPosition()}
            >
              {this.renderNodes()}
            </div>
          </div>
        </DraggableCore>
        {this.renderLegend()}
      </Fragment>
    );
  }

  /**
   *  GRAPH FUNCTIONS
   */

  resetConnections() {
    this.instance.deleteEveryConnection();
  }

  renderGraph() {
    this.instance.setSuspendDrawing(true);

    // reset graph
    this.instance.unbind('connection');
    this.resetConnections();
    this.instance.deleteEveryEndpoint();

    // re-render everything
    this.prepareNodes();
    this.renderConnections();
    this.instance.setSuspendDrawing(false, true);
    this.instance.repaintEverything();
  }

  arrageLayout = (params?: any) => {
    const { nodes, nodesIdx, conns, connsIdx } = params || this.props;
    const layout = getLayout(100, {
      nodes,
      nodesIndex: nodesIdx,
      connections: conns,
      connectionsIndex: connsIdx
    });
    const newPositions = nodesIdx.map(id => ({
      id,
      top: layout._nodes[id].y,
      left: layout._nodes[id].x
    }));
    this.props.onNodeArrage(newPositions);
  };

  findConnId = (source, target) => {
    const { connsIdx, conns } = this.props;
    const filtered = connsIdx.filter(
      id =>
        source.replace('node-', '') === conns[id].from &&
        target.replace('node-', '') === conns[id].to
    );
    if (filtered.length > 0) {
      return filtered[0];
    }

    return undefined;
  };

  createConnection(info: any, originalEvent: MouseEvent) {
    if (!originalEvent) {
      return;
    }
    const arr = this.instance.select({
      source: info.sourceId,
      target: info.targetId
    });
    this.instance.deleteConnection(info.connection);
    if (arr.length > 1 || info.sourceId === info.targetId) {
      // check if connection already exist or if self transition
      // then delete new connection to prevent duplicate
      return;
    }
    this.props.onConnCreate({
      from: info.sourceId.replace('node-', ''),
      to: info.targetId.replace('node-', ''),
      transition: {},
      _opts: {
        mappingMode: 'simple'
      }
    });
  }

  renderConnections() {
    const { connsIdx, conns, nodes, nodesIdx } = this.props;
    connsIdx.forEach(id => {
      if (conns[id].from === conns[id].to) {
        return;
      }
      const newConn: any = {
        source: `node-${conns[id].from}`,
        target: `node-${conns[id].to}`,
        detachable: true,
        type: 'basic',
        cssClass: `kata-state__connection kata-state__connection-${
          conns[id].from
        }`
      };
      if (
        this.state.priority &&
        conns[id].transition &&
        conns[id].transition.priority
      ) {
        newConn.overlays = [
          [
            'Label',
            {
              label: conns[id].transition.priority,
              id: `connection-label-${id}`,
              cssClass: `kata-state-label kata-state-label-${conns[id].from}`
            }
          ]
        ];
      }
      this.instance.connect(newConn);
    });
    if (this.state.floating) {
      nodesIdx.forEach(id => {
        if (!nodes[id].float) {
          return;
        }
        nodesIdx
          .filter(nodeId => nodeId !== id)
          .forEach(nodeId => {
            this.instance.connect({
              source: `node-${nodeId}`,
              target: `node-${id}`,
              detachable: true,
              type: 'floating',
              connector: 'StateMachine',
              cssClass: `kata-state__connection kata-state__connection--floating kata-state__connection-${nodeId}`
            });
          });
      });
    }
    this.instance.bind('click', (conn: any, event: MouseEvent) => {
      event.stopImmediatePropagation();
      const connId = this.findConnId(conn.sourceId, conn.targetId);
      if (connId) {
        this.props.onConnUpdate(conns[connId]);
      }
    });
    this.instance.bind('connection', this.createConnection.bind(this));
  }

  prepareNodes() {
    const nodes = document.querySelectorAll(`#${containerId} .kata-state-node`);
    this.instance.draggable(nodes, {
      handle: '.kata-state-node__drag',
      stop: dragEndEvent => {
        const nodeId = dragEndEvent.el.id.replace('node-', '');
        if (this.props.onNodeChangePos) {
          this.props.onNodeChangePos({
            ...this.props.nodes[nodeId],
            style: {
              top: dragEndEvent.el.style.top,
              left: dragEndEvent.el.style.left
            }
          });
        }
        this.instance.repaintEverything();
      }
    });
    // tslint:disable-next-line:no-increment-decrement - legacy code
    for (let i = 0; i < nodes.length; i++) {
      this.instance.makeSource(nodes[i], DiagramConfig.source);
      this.instance.makeTarget(nodes[i], DiagramConfig.source);
    }
  }

  getFocusedElement = (id: string): any[] => {
    try {
      const ids = this.props.nodes[id].relatedIds;
      return Array.from(
        document.querySelectorAll(
          `.kata-state__connection-${id}, .kata-state-node-${id}, .kata-state-label-${id}${
            ids && ids.length
              ? `, .kata-state-node-${ids.join(', .kata-state-node-')}`
              : ''
          }`
        )
      );
    } catch (error) {
      return [];
    }
  };

  nodeMouseEnter = (id: string) => {
    this.setState({ nodeHovered: true });
    try {
      this.getFocusedElement(id).forEach(el => {
        el.classList.add('focused');
      });
    } catch (error) {
      // do nothing
    }
  };

  nodeMouseLeave = (id: string) => {
    this.setState({ nodeHovered: false });
    try {
      this.getFocusedElement(id).forEach(el => {
        el.classList.remove('focused');
      });
    } catch (error) {
      // do nothing
    }
  };
}

export default StateDiagram;
