export interface Diagram {
  isLoading: boolean;
  scale?: number;
  translate?: string;
}

export interface Node {
  id: string;
  data: any;
  style: {
    left: number;
    top: number;
  };
}

export interface Connection {
  from: string;
  to: string;
}
