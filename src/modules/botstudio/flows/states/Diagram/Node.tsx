import React, { SyntheticEvent } from 'react';
import classnames from 'classnames';

interface Props {
  nodeId: string;
  data?: any;
  style?: {
    top: number | string;
    left: number | string;
  };
  onMouseEnter(id: string);
  onMouseLeave(id: string);
  onUpdate(data: any);
}

interface State {}

class Node extends React.PureComponent<Props, State> {
  onDragClick = (e: SyntheticEvent<any>) => {
    e.preventDefault();
    e.stopPropagation();
  };

  render() {
    const {
      style,
      data,
      nodeId,
      onMouseEnter,
      onMouseLeave,
      onUpdate
    } = this.props;
    return (
      <div
        id={`node-${nodeId}`}
        className={classnames(`kata-state-node kata-state-node-${nodeId}`, {
          'kata-state-node--initial': data.initial,
          'kata-state-node--end': data.end
        })}
        style={style}
        onMouseEnter={() => onMouseEnter(nodeId)}
        onMouseLeave={() => onMouseLeave(nodeId)}
        onClick={() => onUpdate(data)}
      >
        <div className="kata-state-node__content">
          <i
            className="icon-drag icon-middle kata-state-node__drag"
            onClick={this.onDragClick}
          />
          {data.name}
          {data.fallback && (
            <i className="icon-fallback icon-middle kata-state-node__fallback" />
          )}
        </div>
        {!data.end && <div className="kata-state-node__enpoint" />}
      </div>
    );
  }
}

export default Node;
