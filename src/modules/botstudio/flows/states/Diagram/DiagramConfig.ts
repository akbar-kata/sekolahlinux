const settings = {
  source: {
    filter: '.kata-state-node__enpoint',
    anchor: 'AutoDefault',
    connectorStyle: {
      stroke: '#2a90ff',
      strokeWidth: 2,
      outlineStroke: 'transparent',
      outlineWidth: 4,
      joinstyle: 'round'
    },
    connectionType: 'basic',
    extract: {
      action: 'the-action'
    },
    maxConnections: -1,
    onMaxConnections(info: any, e: any) {
      alert(`Maximum connections (${info.maxConnections}) reached`);
    }
  },
  target: {
    dropOptions: {
      hoverClass: 'dragHover'
    },
    anchor: 'AutoDefault',
    allowLoopback: true
  },
  default: {
    Endpoint: [
      'Dot',
      {
        radius: 0.1
      }
    ],
    Connector: [
      'Flowchart',
      {
        gap: 2,
        stub: [10, 15],
        alwaysRespectStubs: true,
        cornerRadius: 5
      }
    ],
    ConnectionsDetachable: true,
    HoverPaintStyle: {
      stroke: '#484c4f',
      strokeWidth: 2
    },
    ConnectionOverlays: [
      [
        'Arrow',
        {
          location: 1,
          id: 'arrow',
          width: 11,
          length: 11
        }
      ]
    ],
    Container: 'canvas'
  },
  floating: {
    anchor: 'AutoDefault',
    Endpoint: [
      'Dot',
      {
        radius: 0.1
      }
    ],
    Connector: 'StateMachine',
    ConnectionsDetachable: true,
    paintStyle: {
      stroke: '#949a9d',
      strokeWidth: 1.2,
      dashstyle: '3 3',
      outlineStroke: 'transparent',
      outlineWidth: 4,
      joinstyle: 'round'
    },
    hoverPaintStyle: { stroke: '#676b6d' },
    ConnectionOverlays: [
      [
        'Arrow',
        {
          location: 1,
          id: 'arrow',
          length: 11,
          width: 12,
          foldback: 1
        }
      ]
    ]
  }
};

export default settings;
