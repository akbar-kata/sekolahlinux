const dagre = require('dagre');

export const getLayout = (
  separation = 10,
  { nodes, nodesIndex, connections, connectionsIndex }
) => {
  const graph = new dagre.graphlib.Graph();
  graph.setGraph({
    nodesep: 10,
    ranksep: separation,
    rankdir: 'LR',
    marginx: 0,
    marginy: 0
  });
  graph.setDefaultEdgeLabel(() => {
    return {};
  });

  nodesIndex.forEach((id: string) => {
    graph.setNode(id, {
      label: nodes[id].name,
      width: 74 + nodes[id].name.length * 5,
      height: 42
    });
  });

  connectionsIndex.forEach((id: string) => {
    graph.setEdge(connections[id].from, connections[id].to);
  });
  dagre.layout(graph);
  return graph;
};

// const getDimension = (selector) => {
//     const parent = document.querySelector(selector);
//     const parentDimension = {
//       height: parent.getBoundingClientRect().height,
//       width: parent.getBoundingClientRect().width
//     };
//     return parentDimension;
// };

// export function fitToScreen(nodes: any, connections: any, parentSelector: string) {
//   const {width, height} = getDimension(parentSelector);
//   let layout = getLayout(100, {nodes, connections});
//   let widthScale = (width - 100) / layout.graph().width;
//   let heightScale = (height - 100) / layout.graph().height;
//   let scale = Math.min(widthScale, heightScale);
//   scale = scale > 1 ? 1 : scale;
//   let scaledDownGraphWidth = layout.graph().width * scale;
//   let scaledDownGraphHeight = layout.graph().height * scale;

//   let translateX = ((width - 100) > scaledDownGraphWidth ? ((width - 100) - scaledDownGraphWidth) / 2 : 0);
//   let translateY = ((height - 100) > scaledDownGraphHeight ? ((height - 100) - scaledDownGraphHeight) / 2 : 0);
//   return {
//     scale,
//     translate: `${translateX}px , ${translateY.toString()}px`
//   };
// }

export function zoomIn(scale: number) {
  return scale + 0.1;
}

export function zoomOut(scale: number) {
  return scale - 0.1;
}
