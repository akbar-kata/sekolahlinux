const legends = [
  {
    image: 'state-initial',
    title: 'Start State',
    text: 'The beginning of selected flow..'
  },
  {
    image: 'state-end',
    title: 'End State',
    text: 'The end of selected flow.'
  },
  {
    image: 'state',
    title: 'Default State',
    text: 'Default state of selected flow.'
  },
  {
    image: 'connection-priority',
    title: 'Priority Number',
    text: `Arrange what's the priority of your state. The higher the number, the higher the priority.`
  },
  {
    image: 'fallback',
    title: 'Fallback Icon',
    text: 'Mark the fallback state.'
  },
  {
    image: 'connection',
    title: 'Transition Path',
    text: 'Shows the connection between two states.'
  },
  {
    image: 'connection-floating',
    title: 'Floating Transition',
    text: 'Shows the floating condition between two states.'
  }
];

export default legends;
