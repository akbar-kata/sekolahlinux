import React, { Fragment } from 'react';
import { connect } from 'react-redux';

import Store from 'interfaces/rootStore';
import * as NodeActions from 'stores/bot/node/actions';
import * as ConnActions from 'stores/bot/connection/actions';
import { getSelected as getSelectedFlow } from 'stores/bot/flow/selectors';
import {
  isExist,
  getData as getNodes,
  getByFlow as getNodesByFlow,
  getLastUpdated
} from 'stores/bot/node/selectors';
import {
  getData as getConns,
  getByFlow as getConnsByFlow
} from 'stores/bot/connection/selectors';
import { addNotification } from 'stores/app/notification/actions';

import { confirm } from 'components/Modal';
import { FloatingButton } from 'components/Button';
import { Tooltip, TooltipTarget } from 'components/Tooltip';
import StateDiagram from './Diagram/StateDiagram';
import StateForm from './StateForm';
import TransitionForm from './TransitionForm';

export interface PropsFromState {
  flow?: string | null;
  lastUpdated: number;
  nodesIdx: string[];
  nodes: any;
  connsIdx: string[];
  conns: any;
  isExist: Function;
}

export interface PropsFromDispatch {
  onNodeArrage: Function;
  onNodeCreate: Function;
  onNodeUpdate: Function;
  onNodeChangePos: Function;
  onNodeRemove: Function;
  onConnCreate: Function;
  onConnUpdate: Function;
  onConnRemove: Function;
  removeRelatedConn: Function;
  showNotif(title: string, message: string): void;
}

interface Props extends PropsFromState, PropsFromDispatch {}

interface Selected {
  id: string;
  name: string;
  [key: string]: any;
}

interface States {
  isOpen: {
    [key: string]: boolean;
  };
  nodeSelected?: Selected;
  connSelected?: Selected;
  [key: string]: any;
}

class PageContainer extends React.Component<Props, States> {
  state: States = {
    isOpen: {}
  };

  shouldComponentUpdate(nextProps: Props, nextState: States) {
    const { lastUpdated, flow } = this.props;
    if (
      nextProps.lastUpdated !== lastUpdated ||
      nextProps.flow !== flow ||
      this.state.isOpen !== nextState.isOpen
    ) {
      return true;
    }

    return false;
  }

  openCreate = (key, data?: any) => {
    this.setState({
      isOpen: {
        ...this.state.isOpen,
        [`${key}Create`]: true
      },
      [`${key}Selected`]: data
    });
  };

  closeCreate = key => {
    this.setState({
      isOpen: { ...this.state.isOpen, [`${key}Create`]: false },
      [`${key}Selected`]: undefined
    });
  };

  openUpdate = (key, data) => {
    this.setState({
      isOpen: { ...this.state.isOpen, [`${key}Update`]: true },
      [`${key}Selected`]: data
    });
  };

  closeUpdate = key => {
    this.setState({
      isOpen: { ...this.state.isOpen, [`${key}Update`]: false },
      [`${key}Selected`]: undefined
    });
  };

  onNodeCreate = (data: Selected) => {
    this.props.onNodeCreate(this.props.flow, data);
    this.closeCreate('node');
    this.props.showNotif(
      `${data.name} State Created`,
      `You have successfully created ${data.name} state.`
    );
  };

  onNodeUpdate = (data: Selected) => {
    this.props.onNodeUpdate(this.props.flow, data.id, data);
    this.closeUpdate('node');
    this.props.showNotif(
      `${data.name} State Updated`,
      `You have successfully updated ${data.name} state.`
    );
  };

  onNodeRemove = () => {
    if (this.state.nodeSelected) {
      const name = this.state.nodeSelected.name;
      confirm({
        title: 'Delete State',
        message: 'Are you sure you want to delete this state?',
        okLabel: 'Delete',
        cancelLabel: 'Cancel'
      }).then(res => {
        if (res) {
          this.props.removeRelatedConn(
            this.props.flow,
            this.state.nodeSelected!.id
          );
          this.props.onNodeRemove(this.state.nodeSelected!.id);
          this.closeUpdate('node');
          this.props.showNotif(
            `${name} State Deleted`,
            `${name} state has been deleted.`
          );
        }
      });
    }
  };

  onNodeChangePos = (data: Selected) => {
    this.props.onNodeChangePos(this.props.flow, data.id, data);
  };

  onConnCreate = data => {
    this.props.onConnCreate(this.props.flow, data);
    const node = this.props.nodes[data.from];
    if (node) {
      this.props.onNodeUpdate(this.props.flow, node.id, {
        ...node,
        fallback: data.from === data.to ? data : undefined,
        relatedIds: [...node.relatedIds, data.to]
      });
    }
    this.closeCreate('conn');
    this.props.showNotif(
      `Transition Created`,
      `You have successfully created transition.`
    );
  };

  onConnUpdate = data => {
    this.props.onConnUpdate(this.props.flow, data.id, data);
    this.closeUpdate('conn');
    this.props.showNotif(
      `Transition Updated`,
      `You have successfully updated transition.`
    );
  };

  onConnRemove = () => {
    const selected = this.state.connSelected;
    if (selected) {
      confirm({
        title: 'Delete State',
        message: `Are you sure you want to delete this transition?`,
        okLabel: 'Delete',
        cancelLabel: 'Cancel'
      }).then(res => {
        if (res) {
          this.props.onConnRemove(selected.id);
          const node = this.props.nodes[selected.from];
          if (node) {
            const ids = node.relatedIds || [];
            const deleteIdx = ids.indexOf(selected.to);
            if (deleteIdx !== -1) {
              this.props.onNodeUpdate(this.props.flow, node.id, {
                ...node,
                fallback:
                  !selected.from === selected.to ? node.fallback : undefined,
                relatedIds: [
                  ...ids.slice(0, deleteIdx),
                  ...ids.slice(deleteIdx + 1)
                ]
              });
            }
          }
          this.closeUpdate('conn');
          this.props.showNotif(
            `Transition Deleted`,
            `Transition has been deleted.`
          );
        }
      });
    }
  };

  render() {
    const { onNodeUpdate, onNodeChangePos, ...props } = this.props;
    return (
      <Fragment>
        <StateDiagram
          {...props}
          onNodeChangePos={this.onNodeChangePos}
          onNodeUpdate={data => this.openUpdate('node', data)}
          onConnCreate={data => this.openCreate('conn', data)}
          onConnUpdate={data => this.openUpdate('conn', data)}
        />
        <TooltipTarget
          placement="top"
          component={<Tooltip>Create State</Tooltip>}
        >
          <FloatingButton
            color="primary"
            icon="add"
            className="kata-floating-action"
            onClick={() => this.openCreate('node')}
          />
        </TooltipTarget>
        <StateForm
          type="create"
          isOpen={this.state.isOpen['nodeCreate']}
          isExist={props.isExist}
          onCancel={() => this.closeCreate('node')}
          onDelete={this.onNodeRemove}
          onSubmit={this.onNodeCreate}
        />
        <StateForm
          type="update"
          nodeId={
            this.state.nodeSelected ? this.state.nodeSelected.id : undefined
          }
          value={this.state.nodeSelected}
          isOpen={this.state.isOpen['nodeUpdate']}
          isExist={props.isExist}
          onCancel={() => this.closeUpdate('node')}
          onDelete={this.onNodeRemove}
          onSubmit={this.onNodeUpdate}
        />
        <TransitionForm
          type="create"
          value={this.state.connSelected}
          isOpen={this.state.isOpen['connCreate']}
          nodes={props.nodes}
          onCancel={() => this.closeCreate('conn')}
          onSubmit={this.onConnCreate}
        />
        <TransitionForm
          type="update"
          value={this.state.connSelected}
          isOpen={this.state.isOpen['connUpdate']}
          nodes={props.nodes}
          onCancel={() => this.closeUpdate('conn')}
          onDelete={this.onConnRemove}
          onSubmit={this.onConnUpdate}
        />
      </Fragment>
    );
  }
}

const mapStateToProps = ({
  bot: { flow, node, connection }
}: Store): PropsFromState => {
  const selectedFlow = getSelectedFlow(flow);
  return {
    flow: selectedFlow,
    lastUpdated: getLastUpdated(node),
    nodesIdx: getNodesByFlow(node, selectedFlow),
    nodes: getNodes(node),
    connsIdx: getConnsByFlow(connection, selectedFlow),
    conns: getConns(connection),
    isExist: (name: string, id?: string) =>
      isExist(node, selectedFlow as string, name, id)
  };
};

const mapDispatchToProps: PropsFromDispatch = {
  onNodeArrage: NodeActions.arrage,
  onNodeCreate: NodeActions.create,
  onNodeUpdate: NodeActions.update,
  onNodeChangePos: NodeActions.changePosition,
  onNodeRemove: NodeActions.remove,
  onConnCreate: ConnActions.create,
  onConnUpdate: ConnActions.update,
  onConnRemove: ConnActions.remove,
  removeRelatedConn: ConnActions.remove,
  showNotif: (title: string, message: string) =>
    addNotification({
      title,
      message,
      status: 'success',
      dismissible: true,
      dismissAfter: 5000
    })
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PageContainer);
