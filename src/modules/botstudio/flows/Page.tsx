import React, { Component } from 'react';
import { Switch, Route, NavLink, Redirect } from 'react-router-dom';

import SelectedUrl from 'containers/SelectedUrl';
import FlowContainer from './Flow.Container';
import Flow from './Flow';

import Intents from './intents/IntentsContainer';
import States from './states';
import { TooltipTarget, Tooltip } from 'components/Tooltip';
import { EmptyMessage } from 'components/Common';

const flowEmptyImage = require('assets/images/flow-empty.svg');

class Page extends Component {
  renderFlowContent = () => {
    return (
      <SelectedUrl>
        {projectId => {
          const parentPath = `/project/${projectId}/bot`;
          return (
            <div className="full-size position-relative">
              <div className="kata-flow__header">
                <div className="btn-group kata-btn-group">
                  <NavLink
                    to={`${parentPath}/flow/states`}
                    className="btn kata-btn-group__item"
                    activeClassName="kata-btn-group__item--selected"
                  >
                    States
                  </NavLink>
                  <NavLink
                    to={`${parentPath}/flow/intents`}
                    exact
                    className="btn kata-btn-group__item"
                    activeClassName="kata-btn-group__item--selected"
                  >
                    Intents
                  </NavLink>
                </div>
              </div>
              <div className="kata-flow__content">
                <Switch>
                  <Route
                    path="/project/:projectId/bot/flow/states"
                    component={States}
                  />
                  <Route
                    path="/project/:projectId/bot/flow/intents"
                    component={Intents}
                  />
                  <Route
                    render={() => <Redirect to={`${parentPath}/flow/states`} />}
                  />
                </Switch>
              </div>
            </div>
          );
        }}
      </SelectedUrl>
    );
  };

  renderEmptyFlow = (noSelect = false) => {
    return (
      <div className="pt-10">
        {noSelect ? (
          <EmptyMessage image={flowEmptyImage} title="No flow selected!">
            Select a flow by clicking the button on the left.
          </EmptyMessage>
        ) : (
          <EmptyMessage image={flowEmptyImage} title="No flow!">
            This bot has no flow. Create your first flow now by clicking the
            button on the left.
          </EmptyMessage>
        )}
      </div>
    );
  };

  render() {
    return (
      <FlowContainer>
        {flowProps => (
          <div className="kata-flow__wrapper">
            <div className="kata-flow__side">
              <div className="kata-flow__side-wrapper">
                <div>
                  <h1 className="kata-flow__title heading2">
                    Conversation Flows
                  </h1>
                  <TooltipTarget
                    component={
                      <Tooltip>Bot flows & actions are defined here.</Tooltip>
                    }
                  >
                    <i className="icon-info kata-dashboard__tooltip" />
                  </TooltipTarget>
                </div>
                <Flow {...flowProps} />
              </div>
            </div>
            <div className="kata-flow__main">
              {flowProps.index && flowProps.index.length
                ? flowProps.selected
                  ? this.renderFlowContent()
                  : this.renderEmptyFlow(true)
                : this.renderEmptyFlow()}
            </div>
          </div>
        )}
      </FlowContainer>
    );
  }
}

export default Page;
