import React from 'react';

import actionTypes from './actionTypes';

interface Props {
  onSelect?: Function;
}

const ActionTypeSelect: React.SFC<Props> = props => {
  const onSelect = (value: string) => () => {
    if (props.onSelect) {
      props.onSelect(value);
    }
  };

  return (
    <div className="action-type-select">
      {actionTypes.map((type, index) => (
        <div
          key={index}
          className="action-type-select__item"
          onClick={onSelect(type.value)}
        >
          <i className={`icon-${type.icon} action-type-select__icon`} />
          <div className="action-type-select__text">{type.text}</div>
        </div>
      ))}
    </div>
  );
};

export default ActionTypeSelect;
