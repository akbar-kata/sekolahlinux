import React, { Component, Fragment } from 'react';

import { JsonObject } from 'interfaces';
import { Form, Text, FormError } from 'components/Form';
import { SupportButton } from 'components/Button';
import actionTypes from './actionTypes';
import ActionTypeSelect from './ActionTypeSelect';
import { SubForms, SubValidate } from './forms';

interface Props {
  id?: string;
  data?: JsonObject;
  subForm?: boolean;
  isExist: Function;
  submitAction: Function;
  cancelAction: Function;
}

interface States {}

const inputId = 'form-input-action';

class ActionForm extends Component<Props, States> {
  onValidate = ({ name, type, options }, state) => {
    function validateOptions() {
      if (!SubValidate[type]) {
        return undefined;
      }
      return SubValidate[type](options);
    }
    return {
      name: !name
        ? 'A name is required'
        : !/^[A-Za-z][-A-Za-z0-9_-]*$/.test(name)
        ? 'Name only allowed charaters alphanumeric, _ and -'
        : this.props.isExist(name, this.props.id)
        ? 'Name already used, pick another name'
        : undefined,
      type: !type ? 'Type must be selected' : undefined,
      options: !options ? 'Options must be defined' : validateOptions()
    };
  };

  renderOptions = (type: string, formApi) => {
    if (!SubForms[type]) {
      return null;
    }
    return SubForms[type](formApi);
  };

  renderHeader = (type: string) => {
    const index = actionTypes.findIndex(item => item.value === type);
    if (index === -1) {
      return null;
    }
    return (
      <div className="kata-action-header">
        <i
          className={`kata-action-header__icon icon-${
            actionTypes[index].icon
          } mr-2`}
        />
        {actionTypes[index].fulltext || actionTypes[index].text}
      </div>
    );
  };

  innerForm = onCancel => formApi => {
    const { values, submitForm, setValue } = formApi;
    const content = (
      <Fragment>
        {values.type ? (
          <div>
            <div onClick={() => setValue('type', undefined)}>
              {this.renderHeader(values.type)}
            </div>
            <div className="kata-info__container">
              <label className="kata-info__label">Name</label>
              <div className="kata-info__content">
                <Text
                  id={inputId}
                  field="name"
                  className="form-control kata-form__input-text"
                />
              </div>
            </div>
            <div className="kata-info__container">
              <FormError field="options" />
              {this.renderOptions(values.type, formApi)}
            </div>
          </div>
        ) : (
          <div className="mb-3">
            <ActionTypeSelect
              onSelect={(value: string) => {
                setValue('type', value);
                setValue('options', {});
              }}
            />
          </div>
        )}
        <div className="kata-action__footer">
          {values.type && (
            <SupportButton
              className="text-capitalize kata-btn__primary mr-1"
              onClick={submitForm}
            >
              <i className="icon-check" /> Create Action
            </SupportButton>
          )}
          <SupportButton onClick={onCancel}>Cancel</SupportButton>
        </div>
      </Fragment>
    );
    return this.props.subForm ? (
      content
    ) : (
      <form onSubmit={submitForm}>{content}</form>
    );
  };

  render() {
    const { data, submitAction, cancelAction } = this.props;
    return (
      <Form
        defaultValues={data}
        validate={this.onValidate}
        onSubmit={submitAction}
      >
        {this.innerForm(cancelAction)}
      </Form>
    );
  }
}

export default ActionForm;
