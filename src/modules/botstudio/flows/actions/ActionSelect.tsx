import React from 'react';

import { ReactSelect } from 'components/Form';

const imageMap = {
  text: 'text',
  data: 'text-data',
  image: 'image',
  button: 'button',
  carousel: 'carousel',
  dynamicCarousel: 'dynamic-carousel',
  imageMap: 'image-map',
  sticker: 'sticker',
  video: 'videos',
  audio: 'audio',
  location: 'location',
  schedule: 'schedule',
  api: 'api',
  command: 'command',
  method: 'method'
};

class GravatarOption extends React.PureComponent<any, any> {
  handleMouseDown = (event: any) => {
    event.preventDefault();
    event.stopPropagation();
    this.props.onSelect(this.props.option, event);
  };

  handleMouseEnter = (event: any) => {
    this.props.onFocus(this.props.option, event);
  };

  handleMouseMove = (event: any) => {
    if (this.props.isFocused) {
      return;
    }
    this.props.onFocus(this.props.option, event);
  };

  render() {
    const option = this.props.option || {};
    return (
      <div
        className={this.props.className}
        onMouseDown={this.handleMouseDown}
        onMouseEnter={this.handleMouseEnter}
        onMouseMove={this.handleMouseMove}
        title={this.props.option.title}
      >
        <i className={`icon-${imageMap[option.type]} icon-middle mr-1`} />{' '}
        {this.props.children}
      </div>
    );
  }
}

class GravatarValue extends React.PureComponent<any, any> {
  render() {
    const value = this.props.value || {};
    return (
      <div className="Select-value" title={value.label}>
        <span className="Select-value-label">
          <i className={`icon-${imageMap[value.type]} icon-middle mr-1`} />{' '}
          {this.props.children}
        </span>
      </div>
    );
  }
}

interface Props {
  field: string;
  options: any;
  onChange?: Function;
}

const ActionSelect = (props: Props) => {
  return (
    <ReactSelect
      field={props.field}
      clearable={false}
      simpleValue
      options={props.options}
      optionComponent={GravatarOption}
      valueComponent={GravatarValue}
      onChange={props.onChange}
    />
  );
};

export default ActionSelect;
