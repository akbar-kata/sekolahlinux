export default {
  text: 'text',
  data: 'text-data',
  image: 'image',
  button: 'button',
  carousel: 'carousel',
  dynamicCarousel: 'dynamic-carousel',
  imageMap: 'image-map',
  sticker: 'sticker',
  video: 'videos',
  audio: 'audio',
  location: 'location',
  schedule: 'schedule',
  api: 'api',
  command: 'command',
  method: 'method'
};
