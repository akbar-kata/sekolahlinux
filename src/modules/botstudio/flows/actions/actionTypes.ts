const types = [
  {
    text: 'Text',
    icon: 'text',
    value: 'text'
  },
  {
    text: 'Text w/ Data',
    fulltext: 'Text with Data',
    icon: 'text-data',
    value: 'data'
  },
  {
    text: 'Image',
    icon: 'image',
    value: 'image'
  },
  {
    text: 'Button',
    icon: 'button',
    value: 'button'
  },
  {
    text: 'Carousel',
    icon: 'carousel',
    value: 'carousel'
  },
  {
    text: 'D. Carousel',
    fulltext: 'Dynamic Carousel',
    icon: 'dynamic-carousel',
    value: 'dynamicCarousel'
  },
  {
    text: 'Image Map',
    icon: 'image-map',
    value: 'imageMap'
  },
  {
    text: 'Sticker',
    icon: 'sticker',
    value: 'sticker'
  },
  {
    text: 'Video',
    icon: 'videos',
    value: 'video'
  },
  {
    text: 'Audio',
    icon: 'audio',
    value: 'audio'
  },
  {
    text: 'Location',
    icon: 'location',
    value: 'location'
  },
  {
    text: 'Schedule',
    icon: 'schedule',
    value: 'schedule'
  },
  {
    text: 'API',
    icon: 'api',
    value: 'api'
  },
  {
    text: 'Command',
    icon: 'command',
    value: 'command'
  },
  {
    text: 'Method',
    icon: 'method',
    value: 'method'
  }
];

export default types;
