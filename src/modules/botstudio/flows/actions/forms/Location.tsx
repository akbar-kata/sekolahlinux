import React from 'react';

import { Text } from 'components/Form';

export const locationValidate = ({ title, address, longitude, latitude }) => {
  return {
    title: !title ? 'Title is required' : undefined,
    address: !address ? 'Address is required' : undefined,
    longitude: !longitude ? 'Longitude is required' : undefined,
    latitude: !latitude ? 'Latitude is required' : undefined
  };
};

const LocationForm = (formApi, disabled?: boolean) => (
  <div>
    <div className="kata-info__container">
      <label className="kata-info__label">Title</label>
      <Text
        field="options.title"
        className="form-control kata-form__input-text"
        disabled={disabled}
      />
    </div>
    <div className="kata-info__container">
      <label className="kata-info__label">Address</label>
      <Text
        field="options.address"
        className="form-control kata-form__input-text"
        disabled={disabled}
      />
    </div>
    <div className="kata-info__container">
      <label className="kata-info__label">Latitude</label>
      <Text
        field="options.latitude"
        className="form-control kata-form__input-text"
        disabled={disabled}
      />
    </div>
    <div className="kata-info__container">
      <label className="kata-info__label">Longitude</label>
      <Text
        field="options.longitude"
        className="form-control kata-form__input-text"
        disabled={disabled}
      />
    </div>
    <div className="kata-info__container">
      <label className="kata-info__label">Location Image URL (optional)</label>
      <Text
        field="options.locationImageUrl"
        className="form-control kata-form__input-text"
        disabled={disabled}
      />
    </div>
  </div>
);

export default LocationForm;
