import React, { Fragment } from 'react';

import { Text } from 'components/Form';

export const dataValidate = ({ data, path, template }) => {
  return {
    data: !data ? 'Data is required' : undefined,
    path: !path ? 'Path is required' : undefined,
    template: !template ? 'Template is required' : undefined
  };
};

const DataForm = (formApi, disabled?: boolean) => (
  <Fragment>
    <div className="kata-info__container">
      <label className="kata-info__label">Data</label>
      <Text
        field="options.data"
        className="form-control kata-form__input-text"
        disabled={disabled}
      />
    </div>
    <div className="kata-info__container">
      <label className="kata-info__label">Path</label>
      <Text
        field="options.path"
        className="form-control kata-form__input-text"
        disabled={disabled}
      />
    </div>
    <div className="kata-info__container">
      <label className="kata-info__label">Template</label>
      <Text
        field="options.template"
        className="form-control kata-form__input-text"
        disabled={disabled}
      />
    </div>
  </Fragment>
);

export default DataForm;
