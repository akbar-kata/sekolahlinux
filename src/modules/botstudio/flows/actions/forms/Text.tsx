import React from 'react';
import isEmpty from 'lodash-es/isEmpty';

import { Text, FormError } from 'components/Form';
import { FloatingButton, SupportButton } from 'components/Button';

export const textValidate = ({ text }) => {
  return {
    text:
      text && text.length
        ? text.map(t => (isEmpty(t) ? 'Text is required' : undefined))
        : 'At least need one text'
  };
};

const TextForm = (
  { values, setValue, addValue, editValue, removeValue },
  disabled?: boolean
) => (
  <div className="kata-info__container">
    <label className="kata-info__label">Text</label>
    <FormError field="options.text" />
    {values.options &&
      values.options.text &&
      values.options.text.length &&
      values.options.text.map((text, index) => (
        <div className="row no-gutters mb-2" key={index}>
          <div className="col mr-1">
            <Text
              field={`options.text.${index}`}
              className="form-control kata-form__input-text"
              disabled={disabled}
            />
          </div>
          <div>
            <FloatingButton
              icon="trash"
              onClick={() => removeValue(`options.text`, index)}
              disabled={disabled}
            />
          </div>
        </div>
      ))}
    <div>
      <SupportButton
        onClick={() => addValue('options.text', '')}
        disabled={disabled}
      >
        Add text
      </SupportButton>
    </div>
  </div>
);

export default TextForm;
