import React, { Fragment } from 'react';

import { Text, ReactSelect, FormError } from 'components/Form';
import { Button, SupportButton } from 'components/Button';
import DateTime from 'components/Form/inputs/reactDateTime';

export const scheduleValidate = ({
  id,
  command,
  start,
  end,
  freqInterval,
  freqType,
  message
}) => {
  let errObject: any = {
    id: !id
      ? 'ID is required'
      : !/^[A-Za-z][-A-Za-z0-9_-]*$/.test(id)
      ? 'ID only allowed charaters alphanumeric, _ and -'
      : undefined,
    command: !command ? 'Command Must be selected' : undefined
  };
  if (command !== 'remove') {
    errObject = Object.assign({}, errObject, {
      start: !start ? 'Start is required' : undefined,
      end: !end ? 'End is required' : undefined,
      freqInterval: !freqInterval
        ? 'Interval is required'
        : !freqType
        ? 'Type must be selected'
        : undefined,
      message: !message
        ? 'Message is required'
        : {
            type: !message.type ? 'Message type must be selected' : undefined,
            content: !message.content
              ? 'Message content is required'
              : undefined,
            metadata:
              message.metadata && message.metadata.length
                ? message.metadata.map((meta, i) => ({
                    key: !message.metadata[i].key
                      ? 'Key is required'
                      : undefined,
                    value: !message.metadata[i].value
                      ? 'Value is required'
                      : undefined
                  }))
                : undefined
          }
    });
  }
  return errObject;
};

const dateTimeProps = {
  className: 'form-control kata-form__input-text'
};

const ScheduleForm = (
  { values, setValue, addValue, editValue, removeValue },
  disabled?: boolean
) => {
  const metadata =
    values.options && values.options.message && values.options.message.metadata
      ? values.options.message.metadata
      : undefined;
  return (
    <Fragment>
      <div className="kata-info__container">
        <label className="kata-info__label">ID</label>
        <Text
          field="options.id"
          className="form-control kata-form__input-text"
          disabled={disabled}
        />
      </div>
      <div className="kata-info__container">
        <label className="kata-info__label">Command</label>
        <ReactSelect
          field="options.command"
          clearable={false}
          simpleValue
          options={[
            {
              label: 'Add',
              value: 'add'
            },
            {
              label: 'Remove',
              value: 'remove'
            }
          ]}
          disabled={disabled}
        />
      </div>
      <div className="kata-info__container row no-gutters">
        <div className="col mr-1">
          <label className="kata-info__label">Start</label>
          <DateTime
            field="options.start"
            inputProps={{
              ...dateTimeProps,
              disabled
            }}
            dateFormat="YYYY-MM-DD"
            timeFormat="HH:mm:ss"
            disabled={disabled}
          />
        </div>
        <div className="col">
          <label className="kata-info__label">End</label>
          <DateTime
            field="options.end"
            inputProps={{
              ...dateTimeProps,
              disabled
            }}
            dateFormat="YYYY-MM-DD"
            timeFormat="HH:mm:ss"
          />
        </div>
      </div>
      <div className="kata-info__container">
        <label className="kata-info__label">Frequent</label>
        <div className="row no-gutters">
          <div className="col mr-1">
            <Text
              field="options.freqInterval"
              type="number"
              placeholder="Interval"
              className="form-control kata-form__input-text kata-form__input-number"
              disabled={disabled}
            />
          </div>
          <div className="col">
            <ReactSelect
              field="options.freqType"
              clearable={false}
              simpleValue
              options={[
                {
                  label: 'Second',
                  value: 'second'
                },
                {
                  label: 'Minute',
                  value: 'minute'
                },
                {
                  label: 'Hour',
                  value: 'hour'
                },
                {
                  label: 'Day',
                  value: 'day'
                },
                {
                  label: 'Week',
                  value: 'week'
                },
                {
                  label: 'Month',
                  value: 'month'
                },
                {
                  label: 'Year',
                  value: 'year'
                }
              ]}
              disabled={disabled}
            />
          </div>
        </div>
      </div>
      <div className="kata-info__container">
        <label className="kata-info__label">Message type</label>
        <ReactSelect
          field="options.message.type"
          clearable={false}
          simpleValue
          options={[
            {
              label: 'Text',
              value: 'text'
            },
            {
              label: 'Command',
              value: 'command'
            },
            {
              label: 'Data',
              value: 'data'
            }
          ]}
          disabled={disabled}
        />
        <FormError field="options.message" />
      </div>
      <div className="kata-info__container">
        <label className="kata-info__label">Message content</label>
        <Text
          field="options.message.content"
          className="form-control kata-form__input-text"
          disabled={disabled}
        />
      </div>
      <div className="kata-info__container">
        {(metadata && metadata.length && (
          <Fragment>
            <label className="kata-info__label">Message metadata</label>
            {metadata.map((meta, index) => (
              <div
                className="bg-white rounded p-2 mb-2 position-relative"
                key={index}
              >
                <div className="mb-2">
                  <Text
                    field={`options.message.metadata.${index}.key`}
                    className="form-control kata-form__input-text"
                    placeholder="Key"
                    disabled={disabled}
                  />
                </div>
                <div>
                  <Text
                    field={`options.message.metadata.${index}.value`}
                    className="form-control kata-form__input-text"
                    placeholder="Value"
                    disabled={disabled}
                  />
                </div>
                <Button
                  isIcon
                  size="sm"
                  className="kata-action__remove-btn"
                  onClick={() => removeValue('options.message.metadata', index)}
                  disabled={disabled}
                >
                  <i className="icon-trash" />
                </Button>
              </div>
            ))}
          </Fragment>
        )) ||
          false}
        <SupportButton
          onClick={() => addValue('options.message.metadata', {})}
          disabled={disabled}
        >
          Add message metadata
        </SupportButton>
      </div>
    </Fragment>
  );
};

export default ScheduleForm;
