import React, { Fragment } from 'react';
import get from 'lodash-es/get';
import isArray from 'lodash-es/isArray';

import { Text, ReactSelect, FormError } from 'components/Form';
import { Button, SupportButton, FloatingButton } from 'components/Button';

const validateMap = value => {
  return value && isArray(value)
    ? value.map(e => {
        return {
          key: !e.key && e.value ? 'Key is not set' : undefined,
          value: !e.value && e.key ? 'Value is not set' : undefined
        };
      })
    : undefined;
};

export const carouselValidate = ({ items }) => {
  return {
    items:
      items && items.length
        ? items.map(item => ({
            title: !item.title ? 'Title is required' : undefined,
            text: !item.text ? 'Text is required' : undefined,
            thumbnailImageUrl: !item.thumbnailImageUrl
              ? 'Thumbnail url is required'
              : undefined,
            actions:
              item.actions && item.actions.length
                ? item.actions.map(action => ({
                    label: !action.label
                      ? 'Action label is required'
                      : undefined,
                    type: !action.type
                      ? 'Action type must be selected'
                      : undefined,
                    payload:
                      action.type === 'postback' && !action.payload
                        ? 'Action payload is required'
                        : validateMap(action.payload),
                    url:
                      action.type === 'url' && !action.url
                        ? 'Action url is required'
                        : undefined
                  }))
                : 'Actions is required'
          }))
        : 'Item must be defined'
  };
};

const renderPayload = (formApi, parentIndex, itemIndex, disabled) => {
  const data = get(
    formApi,
    `values.options.items.${parentIndex}.actions.${itemIndex}.payload`,
    []
  );
  return (
    <div className="kata-info__container">
      <label className="kata-info__label">Payload</label>
      <FormError
        field={`options.items.${parentIndex}.actions.${itemIndex}.payload`}
      />
      {data &&
        data.map((item, i) => (
          <div key={i} className="row no-gutters mb-2">
            <div className="col mr-1">
              <Text
                field={`options.items.${parentIndex}.actions.${itemIndex}.payload.${i}.key`}
                className="form-control kata-form__input-text"
                placeholder="Key"
                disabled={disabled}
              />
            </div>
            <div className="col mr-1">
              <Text
                field={`options.items.${parentIndex}.actions.${itemIndex}.payload.${i}.value`}
                className="form-control kata-form__input-text"
                placeholder="Value"
                disabled={disabled}
              />
            </div>
            <div>
              <FloatingButton
                icon="trash"
                onClick={() =>
                  formApi.removeValue(
                    `options.items.${parentIndex}.actions.${itemIndex}.payload`,
                    i
                  )
                }
                disabled={disabled}
              />
            </div>
          </div>
        ))}
      <div>
        <SupportButton
          onClick={() =>
            formApi.addValue(
              `options.items.${parentIndex}.actions.${itemIndex}.payload`,
              {}
            )
          }
          disabled={disabled}
        >
          Add Payload
        </SupportButton>
      </div>
    </div>
  );
};

const CarouselForm = (formApi, disabled?: boolean) => {
  const { values, setValue, addValue, removeValue } = formApi;
  return (
    <Fragment>
      <FormError field="options.items" />
      {values.options &&
        values.options.items &&
        values.options.items.length &&
        values.options.items.map((item, index) => (
          <div
            key={index}
            className="bg-white p-2 rounded mb-2 position-relative"
          >
            <Button
              isIcon
              size="sm"
              className="kata-action__remove-btn"
              onClick={() => removeValue(`options.items`, index)}
              disabled={disabled}
            >
              <i className="icon-trash" />
            </Button>
            <div className="kata-info__container">
              <label className="kata-info__label">Title</label>
              <Text
                field={`options.items.${index}.title`}
                className="form-control kata-form__input-text"
                disabled={disabled}
              />
            </div>
            <div className="kata-info__container">
              <label className="kata-info__label">Text</label>
              <Text
                field={`options.items.${index}.text`}
                className="form-control kata-form__input-text"
                disabled={disabled}
              />
            </div>
            <div className="kata-info__container">
              <label className="kata-info__label">Thumbnail Image Url</label>
              <Text
                field={`options.items.${index}.thumbnailImageUrl`}
                className="form-control kata-form__input-text"
                disabled={disabled}
              />
            </div>
            <div className="kata-info__container">
              <label className="kata-info__label">Actions</label>
              <FormError field="options.items.${index}.actions" />
              {item.actions &&
                item.actions.length &&
                item.actions.map((action, i) => (
                  <div
                    key={i}
                    className="bg-light mb-2 rounded p-2 position-relative"
                  >
                    <div className="mb-2">
                      <Text
                        field={`options.items.${index}.actions.${i}.label`}
                        className="form-control kata-form__input-text"
                        placeholder="Label"
                        disabled={disabled}
                      />
                    </div>
                    <div className="mb-2">
                      <ReactSelect
                        field={`options.items.${index}.actions.${i}.type`}
                        simpleValue
                        options={[
                          {
                            label: 'Postback',
                            value: 'postback'
                          },
                          {
                            label: 'URL',
                            value: 'url'
                          }
                        ]}
                        onChange={() => {
                          setValue(
                            `options.items.${index}.actions.${i}.payload`,
                            ''
                          );
                          setValue(
                            `options.items.${index}.actions.${i}.url`,
                            ''
                          );
                        }}
                        disabled={disabled}
                      />
                    </div>
                    <div>
                      {action.type === 'postback' ? (
                        renderPayload(formApi, index, i, disabled)
                      ) : (
                        <Text
                          field={`options.items.${index}.actions.${i}.url`}
                          className="form-control kata-form__input-text"
                          placeholder="URL"
                          disabled={disabled}
                        />
                      )}
                    </div>
                    <Button
                      isIcon
                      size="sm"
                      className="kata-action__remove-btn"
                      onClick={() =>
                        removeValue(`options.items.${index}.actions`, i)
                      }
                      disabled={disabled}
                    >
                      <i className="icon-trash" />
                    </Button>
                  </div>
                ))}
              <div>
                <SupportButton
                  onClick={() =>
                    addValue(`options.items.${index}.actions`, {
                      type: 'postback'
                    })
                  }
                  disabled={disabled}
                >
                  Add action
                </SupportButton>
              </div>
            </div>
          </div>
        ))}
      <div>
        <SupportButton
          onClick={() => addValue('options.items', {})}
          disabled={disabled}
        >
          Add carousel
        </SupportButton>
      </div>
    </Fragment>
  );
};

export default CarouselForm;
