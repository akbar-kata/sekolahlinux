import React, { Fragment } from 'react';
import isArray from 'lodash-es/isArray';

import { Text, FormError } from 'components/Form';
import { FloatingButton, SupportButton } from 'components/Button';

const validateMap = value => {
  return value && isArray(value)
    ? value.map(e => {
        return {
          key: !e.key && e.value ? 'Key is not set' : undefined,
          value: !e.value && e.key ? 'Value is not set' : undefined
        };
      })
    : undefined;
};

export const commandValidate = ({ command, payload }) => {
  return {
    command: !command ? 'Command is required' : undefined,
    payload: validateMap(payload)
  };
};

const CommandForm = (
  { values, setValue, addValue, editValue, removeValue },
  disabled?: boolean
) => {
  function renderMap(name: string, title?: string) {
    const data =
      values.options && values.options[name] && values.options[name].length
        ? values.options[name]
        : undefined;

    return (
      <div className="kata-info__container">
        <label className="kata-info__label">
          {title || name.replace('_', ' ')}
        </label>
        <FormError field={`options.${name}`} />
        {data &&
          data.map((item, i) => (
            <div key={i} className="row no-gutters mb-2">
              <div className="col mr-1">
                <Text
                  field={`options.${name}.${i}.key`}
                  className="form-control kata-form__input-text"
                  placeholder="Key"
                  disabled={disabled}
                />
              </div>
              <div className="col mr-1">
                <Text
                  field={`options.${name}.${i}.value`}
                  className="form-control kata-form__input-text"
                  placeholder="Value"
                  disabled={disabled}
                />
              </div>
              <div>
                <FloatingButton
                  icon="trash"
                  onClick={() => removeValue(`options.${name}`, i)}
                  disabled={disabled}
                />
              </div>
            </div>
          ))}
        <div>
          <SupportButton
            onClick={() => addValue(`options.${name}`, {})}
            disabled={disabled}
          >
            Add {name.replace('_', ' ')}
          </SupportButton>
        </div>
      </div>
    );
  }

  return (
    <Fragment>
      <div className="kata-info__container">
        <label className="kata-info__label">Command</label>
        <Text
          field="options.command"
          className="form-control kata-form__input-text"
          disabled={disabled}
        />
      </div>
      {renderMap('payload', 'Payload')}
    </Fragment>
  );
};

export default CommandForm;
