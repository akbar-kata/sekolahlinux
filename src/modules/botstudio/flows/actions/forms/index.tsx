import TextForm, { textValidate } from './Text';
import DataForm, { dataValidate } from './Data';
import ImageForm, { imageValidate } from './Image';
import ButtonForm, { buttonValidate } from './Button';
import CarouselForm, { carouselValidate } from './Carousel';
import DynamicCarouselForm, {
  dynamicCarouselValidate
} from './DynamicCarousel';
import ImageMapForm, { imageMapValidate } from './ImageMap';
import StickerForm, { stickerValidate } from './Sticker';
import AudioForm, { audioValidate } from './Audio';
import VideoForm, { videoValidate } from './Video';
import LocationForm, { locationValidate } from './Location';
import ScheduleForm, { scheduleValidate } from './Schedule';
import ApiForm, { apiValidate } from './Api';
import CommandForm, { commandValidate } from './Command';
import MethodForm, { methodValidate } from './Method';

export const SubForms = {
  text: TextForm,
  data: DataForm,
  image: ImageForm,
  button: ButtonForm,
  carousel: CarouselForm,
  dynamicCarousel: DynamicCarouselForm,
  imageMap: ImageMapForm,
  sticker: StickerForm,
  video: VideoForm,
  audio: AudioForm,
  location: LocationForm,
  schedule: ScheduleForm,
  api: ApiForm,
  command: CommandForm,
  method: MethodForm
};

export const SubValidate = {
  text: textValidate,
  data: dataValidate,
  image: imageValidate,
  button: buttonValidate,
  carousel: carouselValidate,
  dynamicCarousel: dynamicCarouselValidate,
  imageMap: imageMapValidate,
  sticker: stickerValidate,
  video: videoValidate,
  audio: audioValidate,
  location: locationValidate,
  schedule: scheduleValidate,
  api: apiValidate,
  command: commandValidate,
  method: methodValidate
};
