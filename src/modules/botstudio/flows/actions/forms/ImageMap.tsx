import React from 'react';

import { Text, ReactSelect, FormError } from 'components/Form';
import { Button, SupportButton } from 'components/Button';

export const imageMapValidate = ({ baseUrl, altText, baseSize, actions }) => {
  return {
    baseUrl: !baseUrl ? 'Image url is required' : undefined,
    altText: !altText ? 'Alternative text is required' : undefined,
    baseSize: !baseSize
      ? 'Width is required'
      : {
          width: !baseSize.width ? 'Width is required' : undefined,
          height: !baseSize.height ? 'Height is required' : undefined
        },
    actions:
      !actions || !actions.length
        ? 'Actions is required'
        : actions.map((action, index) => ({
            type: !action.type ? 'Type is required' : undefined,
            text: !action.text ? 'Text is required' : undefined,
            area: !action.area
              ? 'Area X is required'
              : {
                  x: !action.area.x ? 'Area X is required' : undefined,
                  y: !action.area.y ? 'Area Y is required' : undefined,
                  width: !action.area.width
                    ? 'Area width is required'
                    : undefined,
                  height: !action.area.height
                    ? 'Area height is required'
                    : undefined
                }
          }))
  };
};

const ImageMapForm = (
  { values, setValue, addValue, editValue, removeValue },
  disabled?: boolean
) => (
  <div>
    <div className="kata-info__container">
      <label className="kata-info__label">Image URL</label>
      <Text
        field="options.baseUrl"
        className="form-control kata-form__input-text"
        disabled={disabled}
      />
    </div>
    <div className="kata-info__container">
      <label className="kata-info__label">Alternative Text</label>
      <Text
        field="options.altText"
        className="form-control kata-form__input-text"
        disabled={disabled}
      />
    </div>
    <div className="row no-gutters kata-info__container">
      <div className="col mr-1">
        <label className="kata-info__label">Width</label>
        <Text
          field="options.baseSize.width"
          type="number"
          className="form-control kata-form__input-text kata-form__input-number"
          disabled={disabled}
        />
        <FormError field="options.baseSize" />
      </div>
      <div className="col">
        <label className="kata-info__label">Height</label>
        <Text
          field="options.baseSize.height"
          type="number"
          className="form-control kata-form__input-text kata-form__input-number"
          disabled={disabled}
        />
      </div>
    </div>
    <div className="kata-info__container">
      <label className="kata-info__label">Actions</label>
      <FormError field="options.actions" />
      {values.options &&
        values.options.actions &&
        values.options.actions.length &&
        values.options.actions.map((action, i) => (
          <div key={i} className="bg-white rounded p-2 mb-2 position-relative">
            <div className="kata-info__container">
              <label className="kata-info__label">Type</label>
              <ReactSelect
                field={`options.actions.${i}.type`}
                clearable={false}
                simpleValue
                options={[
                  {
                    label: 'Message',
                    value: 'message'
                  }
                ]}
                disabled={disabled}
              />
            </div>
            <div className="kata-info__container">
              <label className="kata-info__label">Text</label>
              <Text
                field={`options.actions.${i}.text`}
                className="form-control kata-form__input-text"
                placeholder="Text"
                disabled={disabled}
              />
            </div>
            <div className="kata-info__container row no-gutters">
              <div className="col mr-1">
                <label className="kata-info__label">Area X</label>
                <Text
                  type="number"
                  field={`options.actions.${i}.area.x`}
                  className="form-control kata-form__input-text kata-form__input-number"
                  placeholder="X"
                  disabled={disabled}
                />
                <FormError field={`options.actions.${i}.area`} />
              </div>
              <div className="col">
                <label className="kata-info__label">Area Y</label>
                <Text
                  type="number"
                  field={`options.actions.${i}.area.y`}
                  className="form-control kata-form__input-text kata-form__input-number"
                  placeholder="Y"
                  disabled={disabled}
                />
              </div>
            </div>
            <div className="kata-info__container row no-gutters">
              <div className="col mr-1">
                <label className="kata-info__label">Area width</label>
                <Text
                  type="number"
                  field={`options.actions.${i}.area.width`}
                  className="form-control kata-form__input-text kata-form__input-number"
                  placeholder="Width"
                  disabled={disabled}
                />
              </div>
              <div className="col">
                <label className="kata-info__label">Area height</label>
                <Text
                  type="number"
                  field={`options.actions.${i}.area.height`}
                  className="form-control kata-form__input-text kata-form__input-number"
                  placeholder="Height"
                  disabled={disabled}
                />
              </div>
            </div>
            <Button
              isIcon
              size="sm"
              className="kata-action__remove-btn"
              onClick={() => removeValue(`options.actions`, i)}
              disabled={disabled}
            >
              <i className="icon-trash" />
            </Button>
          </div>
        ))}
      <div>
        <SupportButton
          onClick={() => addValue(`options.actions`, { type: 'postback' })}
          disabled={disabled}
        >
          Add action
        </SupportButton>
      </div>
    </div>
  </div>
);

export default ImageMapForm;
