import React, { Fragment } from 'react';

import { Text } from 'components/Form';

export const audioValidate = ({ originalContentUrl, duration }) => {
  return {
    originalContentUrl: !originalContentUrl
      ? 'Sticker ID is required'
      : undefined,
    duration: !duration ? 'Package ID is required' : undefined
  };
};

const AudioForm = (formApi, disabled?: boolean) => (
  <Fragment>
    <div className="kata-info__container">
      <label className="kata-info__label">Original Content URL</label>
      <Text
        field="options.originalContentUrl"
        className="form-control kata-form__input-text"
        disabled={disabled}
      />
    </div>
    <div className="kata-info__container">
      <label className="kata-info__label">Duration</label>
      <Text
        field="options.duration"
        className="form-control kata-form__input-text"
        disabled={disabled}
      />
    </div>
  </Fragment>
);

export default AudioForm;
