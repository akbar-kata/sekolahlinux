import React from 'react';

import { FormError, ReactSelect, Text } from 'components/Form';
import { FloatingButton, SupportButton } from 'components/Button';

export const apiValidate = ({
  uri,
  method,
  body,
  headers,
  query,
  form_data,
  resultPath
}) => {
  function validateMap(data: any) {
    if (!data || !data.length) {
      return undefined;
    }

    return data.map((row, i) => ({
      key: !row.key
        ? 'Key is required'
        : !/^[A-Za-z][-A-Za-z0-9_-]*$/.test(row.key)
        ? 'Key only allowed charaters alphanumeric, _ and -'
        : undefined,
      value: !row.value ? 'Value is required' : undefined
    }));
  }

  return {
    uri: !uri ? 'URI is required' : undefined,
    method: !method ? 'Method must be selected' : undefined,
    // resultPath: !resultPath ? 'Result path is required' : undefined,
    body: validateMap(body),
    headers: validateMap(headers),
    query: validateMap(query),
    form_data: validateMap(form_data)
  };
};

const ApiForm = (
  { values, setValue, addValue, editValue, removeValue },
  disabled?: boolean
) => {
  function renderMap(name: string) {
    const data =
      values.options && values.options[name] && values.options[name].length
        ? values.options[name]
        : undefined;

    return (
      <div className="kata-info__container">
        <label className="kata-info__label">{name.replace('_', ' ')}</label>
        <FormError field={`options.${name}`} />
        {data &&
          data.map((item, i) => (
            <div key={i} className="row no-gutters mb-2">
              <div className="col mr-1">
                <Text
                  field={`options.${name}.${i}.key`}
                  className="form-control kata-form__input-text"
                  placeholder="Key"
                  disabled={disabled}
                />
              </div>
              <div className="col mr-1">
                <Text
                  field={`options.${name}.${i}.value`}
                  className="form-control kata-form__input-text"
                  placeholder="Value"
                  disabled={disabled}
                />
              </div>
              <div>
                <FloatingButton
                  icon="trash"
                  onClick={() => removeValue(`options.${name}`, i)}
                  disabled={disabled}
                />
              </div>
            </div>
          ))}
        <div>
          <SupportButton
            onClick={() => addValue(`options.${name}`, {})}
            disabled={disabled}
          >
            Add {name.replace('_', ' ')}
          </SupportButton>
        </div>
      </div>
    );
  }

  return (
    <div>
      <div className="kata-info__container">
        <label className="kata-info__label">URI</label>
        <Text
          field="options.uri"
          className="form-control kata-form__input-text"
          disabled={disabled}
        />
      </div>
      <div className="kata-info__container">
        <label className="kata-info__label">Method</label>
        <ReactSelect
          field={`options.method`}
          clearable={false}
          simpleValue
          options={[
            {
              label: 'GET',
              value: 'GET'
            },
            {
              label: 'POST',
              value: 'POST'
            }
          ]}
          disabled={disabled}
        />
      </div>
      {renderMap('body')}
      {renderMap('headers')}
      {renderMap('query')}
      {renderMap('form_data')}
      <div className="kata-info__container">
        <label className="kata-info__label">Result Path</label>
        <Text
          field="options.resultPath"
          className="form-control kata-form__input-text"
          disabled={disabled}
        />
      </div>
    </div>
  );
};

export default ApiForm;
