import React, { Fragment } from 'react';
import isArray from 'lodash-es/isArray';
import get from 'lodash-es/get';

import { Text, ReactSelect, FormError } from 'components/Form';
import { Button, SupportButton, FloatingButton } from 'components/Button';

const validateMap = value => {
  return value && isArray(value)
    ? value.map(e => {
        return {
          key: !e.key && e.value ? 'Key is not set' : undefined,
          value: !e.value && e.key ? 'Value is not set' : undefined
        };
      })
    : undefined;
};

export const dynamicCarouselValidate = ({
  data,
  title,
  text,
  thumbnailImageUrl,
  actions
}) => {
  return {
    data: !data ? 'Data is required' : undefined,
    title: !title ? 'Title is required' : undefined,
    text: !text ? 'Text is required' : undefined,
    thumbnailImageUrl: !thumbnailImageUrl
      ? 'Thumbnail url is required'
      : undefined,
    actions:
      actions && actions.length
        ? actions.map(action => ({
            label: !action.label ? 'Action label is required' : undefined,
            type: !action.type ? 'Action type must be selected' : undefined,
            payload:
              action.type === 'postback' && !action.payload
                ? 'Action payload is required'
                : validateMap(action.payload),
            url:
              action.type === 'url' && !action.url
                ? 'Action url is required'
                : undefined
          }))
        : 'Actions is required'
  };
};

const renderPayload = (formApi, index, disabled) => {
  const data = get(formApi, `values.options.actions.${index}.payload`, []);
  return (
    <div className="kata-info__container">
      <label className="kata-info__label">Payload</label>
      <FormError field={`options.actions.${index}.payload`} />
      {data &&
        data.map((item, i) => (
          <div key={i} className="row no-gutters mb-2">
            <div className="col mr-1">
              <Text
                field={`options.actions.${index}.payload.${i}.key`}
                className="form-control kata-form__input-text"
                placeholder="Key"
                disabled={disabled}
              />
            </div>
            <div className="col mr-1">
              <Text
                field={`options.actions.${index}.payload.${i}.value`}
                className="form-control kata-form__input-text"
                placeholder="Value"
                disabled={disabled}
              />
            </div>
            <div>
              <FloatingButton
                icon="trash"
                onClick={() =>
                  formApi.removeValue(`options.actions.${index}.payload`, i)
                }
                disabled={disabled}
              />
            </div>
          </div>
        ))}
      <div>
        <SupportButton
          onClick={() =>
            formApi.addValue(`options.actions.${index}.payload`, {})
          }
          disabled={disabled}
        >
          Add Payload
        </SupportButton>
      </div>
    </div>
  );
};

const DynamicCarouselForm = (formApi, disabled?: boolean) => {
  const { values, setValue, addValue, removeValue } = formApi;
  return (
    <Fragment>
      <div className="kata-info__container">
        <label className="kata-info__label">Data</label>
        <Text
          field="options.data"
          className="form-control kata-form__input-text"
          disabled={disabled}
        />
      </div>
      <div className="kata-info__container">
        <label className="kata-info__label">Text</label>
        <Text
          field="options.text"
          className="form-control kata-form__input-text"
          disabled={disabled}
        />
      </div>
      <div className="kata-info__container">
        <label className="kata-info__label">Title</label>
        <Text
          field="options.title"
          className="form-control kata-form__input-text"
          disabled={disabled}
        />
      </div>
      <div className="kata-info__container">
        <label className="kata-info__label">Thumbnail Image Url</label>
        <Text
          field="options.thumbnailImageUrl"
          className="form-control kata-form__input-text"
          disabled={disabled}
        />
      </div>
      <div className="kata-info__container">
        <label className="kata-info__label">Actions</label>
        <FormError field="options.actions" />
        {values.options &&
          values.options.actions &&
          values.options.actions.length &&
          values.options.actions.map((action, i) => (
            <div
              key={i}
              className="bg-white rounded p-2 mb-2 position-relative"
            >
              <div className="mb-2">
                <Text
                  field={`options.actions.${i}.label`}
                  className="form-control kata-form__input-text"
                  placeholder="Label"
                  disabled={disabled}
                />
              </div>
              <div className="mb-2">
                <ReactSelect
                  field={`options.actions.${i}.type`}
                  clearable={false}
                  simpleValue
                  options={[
                    {
                      label: 'Postback',
                      value: 'postback'
                    },
                    {
                      label: 'URL',
                      value: 'url'
                    }
                  ]}
                  onChange={() => {
                    setValue(`options.actions.${i}.payload`, '');
                    setValue(`options.actions.${i}.url`, '');
                  }}
                  disabled={disabled}
                />
              </div>
              <div className="mb-2">
                {(action.type === 'postback' &&
                  renderPayload(formApi, i, disabled)) || (
                  <Text
                    field={`options.actions.${i}.url`}
                    className="form-control kata-form__input-text"
                    placeholder="URL"
                    disabled={disabled}
                  />
                )}
              </div>
              <Button
                isIcon
                size="sm"
                className="kata-action__remove-btn"
                onClick={() => removeValue(`options.actions`, i)}
                disabled={disabled}
              >
                <i className="icon-trash" />
              </Button>
            </div>
          ))}
        <div>
          <SupportButton
            onClick={() => addValue(`options.actions`, { type: 'postback' })}
            disabled={disabled}
          >
            Add action
          </SupportButton>
        </div>
      </div>
    </Fragment>
  );
};

export default DynamicCarouselForm;
