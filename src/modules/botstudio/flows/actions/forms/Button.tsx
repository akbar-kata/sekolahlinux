import React, { Fragment } from 'react';
import get from 'lodash-es/get';
import isArray from 'lodash-es/isArray';

import { Text, ReactSelect, FormError } from 'components/Form';
import { Button, SupportButton, FloatingButton } from 'components/Button';

const validateMap = value => {
  return value && isArray(value)
    ? value.map(e => {
        return {
          key: !e.key && e.value ? 'Key is not set' : undefined,
          value: !e.value && e.key ? 'Value is not set' : undefined
        };
      })
    : undefined;
};

export const buttonValidate = ({ title, text, actions }) => {
  return {
    title: !title ? 'Title is required' : undefined,
    text: !text ? 'Text is required' : undefined,
    actions:
      actions && actions.length
        ? actions.map(action => ({
            label: !action.label ? 'Action label is required' : undefined,
            type: !action.type ? 'Action type must be selected' : undefined,
            payload:
              action.type === 'postback' && !action.payload
                ? 'Action payload is required'
                : validateMap(action.payload),
            url:
              action.type === 'url' && !action.url
                ? 'Action url is required'
                : undefined,
            text:
              action.type === 'message' && !action.text
                ? 'Action text is required'
                : undefined
          }))
        : 'Action must defined'
  };
};

const renderActionField = (
  formApi: any,
  type: string,
  index: number,
  disabled?: boolean
) => {
  switch (type) {
    case 'postback': {
      const data = get(formApi, `values.options.actions.${index}.payload`, []);
      return (
        <div className="kata-info__container">
          <label className="kata-info__label">Payload</label>
          <FormError field={`options.actions.${index}.payload`} />
          {data &&
            data.map((item, i) => (
              <div key={i} className="row no-gutters mb-2">
                <div className="col mr-1">
                  <Text
                    field={`options.actions.${index}.payload.${i}.key`}
                    className="form-control kata-form__input-text"
                    placeholder="Key"
                    disabled={disabled}
                  />
                </div>
                <div className="col mr-1">
                  <Text
                    field={`options.actions.${index}.payload.${i}.value`}
                    className="form-control kata-form__input-text"
                    placeholder="Value"
                    disabled={disabled}
                  />
                </div>
                <div>
                  <FloatingButton
                    icon="trash"
                    onClick={() =>
                      formApi.removeValue(`options.actions.${index}.payload`, i)
                    }
                    disabled={disabled}
                  />
                </div>
              </div>
            ))}
          <div>
            <SupportButton
              onClick={() =>
                formApi.addValue(`options.actions.${index}.payload`, {})
              }
              disabled={disabled}
            >
              Add Payload
            </SupportButton>
          </div>
        </div>
      );
    }
    case 'message': {
      return (
        <Fragment>
          <label className="kata-info__label">Text</label>
          <Text
            field={`options.actions.${index}.text`}
            className="form-control kata-form__input-text"
            placeholder="Text"
            disabled={disabled}
          />
        </Fragment>
      );
    }
    case 'url': {
      return (
        <Fragment>
          <label className="kata-info__label">URL</label>
          <Text
            field={`options.actions.${index}.url`}
            className="form-control kata-form__input-text"
            placeholder="URL"
            disabled={disabled}
          />
        </Fragment>
      );
    }
    default: {
      return null;
    }
  }
};

const ButtonForm = (formApi, disabled?: boolean) => {
  const { values, setValue, addValue, removeValue } = formApi;
  return (
    <Fragment>
      <div className="kata-info__container">
        <label className="kata-info__label">Title</label>
        <Text
          field="options.title"
          className="form-control kata-form__input-text"
          disabled={disabled}
        />
      </div>
      <div className="kata-info__container">
        <label className="kata-info__label">Text</label>
        <Text
          field="options.text"
          className="form-control kata-form__input-text"
          disabled={disabled}
        />
      </div>
      <div className="kata-info__container">
        <label className="kata-info__label">Actions</label>
        <FormError field="actions" />
        {values.options &&
          values.options.actions &&
          values.options.actions.length &&
          values.options.actions.map((action, index) => (
            <div
              className="bg-white rounded p-2 mb-2 position-relative"
              key={index}
            >
              <div className="mb-1">
                <label className="kata-info__label">Label</label>
                <Text
                  field={`options.actions.${index}.label`}
                  className="form-control kata-form__input-text"
                  placeholder="Label"
                  disabled={disabled}
                />
              </div>
              <div className="mb-1">
                <label className="kata-info__label">Type</label>
                <ReactSelect
                  field={`options.actions.${index}.type`}
                  clearable={false}
                  simpleValue
                  options={[
                    {
                      label: 'Postback',
                      value: 'postback'
                    },
                    {
                      label: 'URL',
                      value: 'url'
                    },
                    {
                      label: 'Message',
                      value: 'message'
                    }
                  ]}
                  onChange={() => {
                    setValue(`options.actions.${index}.payload`, '');
                    setValue(`options.actions.${index}.url`, '');
                    setValue(`options.actions.${index}.text`, '');
                  }}
                  disabled={disabled}
                />
              </div>
              <div>
                {renderActionField(formApi, action.type, index, disabled)}
              </div>
              <Button
                isIcon
                size="sm"
                className="kata-action__remove-btn"
                onClick={() => removeValue('options.actions', index)}
                disabled={disabled}
              >
                <i className="icon-trash" />
              </Button>
            </div>
          ))}
        <div>
          <SupportButton
            onClick={() => addValue('options.actions', { type: 'postback' })}
            disabled={disabled}
          >
            Add action
          </SupportButton>
        </div>
      </div>
    </Fragment>
  );
};

export default ButtonForm;
