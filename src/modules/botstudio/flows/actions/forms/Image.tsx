import React, { Fragment } from 'react';

import { Text } from 'components/Form';

export const imageValidate = ({ imageUrl, thumbnailUrl }) => {
  return {
    imageUrl: !imageUrl ? 'Image url is required' : undefined,
    thumbnailUrl: !thumbnailUrl ? 'Thumbnail url is required' : undefined
  };
};

const ImageForm = (formApi, disabled?: boolean) => (
  <Fragment>
    <div className="kata-info__container">
      <label className="kata-info__label">Image URL</label>
      <Text
        field="options.imageUrl"
        className="form-control kata-form__input-text"
        disabled={disabled}
      />
    </div>
    <div className="kata-info__container">
      <label className="kata-info__label">Thumbnail URL</label>
      <Text
        field="options.thumbnailUrl"
        className="form-control kata-form__input-text"
        disabled={disabled}
      />
    </div>
  </Fragment>
);

export default ImageForm;
