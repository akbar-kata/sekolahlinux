import React from 'react';

import { Text } from 'components/Form';

export const videoValidate = ({ originalContentUrl, previewImageUrl }) => {
  return {
    originalContentUrl: !originalContentUrl
      ? 'Sticker ID is required'
      : undefined,
    previewImageUrl: !previewImageUrl ? 'Package ID is required' : undefined
  };
};

const VideoForm = (formApi, disabled?: boolean) => (
  <div>
    <div className="kata-info__container">
      <label className="kata-info__label">Original Content URL</label>
      <Text
        field="options.originalContentUrl"
        className="form-control kata-form__input-text"
        disabled={disabled}
      />
    </div>
    <div className="kata-info__container">
      <label className="kata-info__label">Preview Image URL</label>
      <Text
        field="options.previewImageUrl"
        className="form-control kata-form__input-text"
        disabled={disabled}
      />
    </div>
  </div>
);

export default VideoForm;
