import React, { Fragment } from 'react';

import { Text } from 'components/Form';

export const stickerValidate = ({ stickerId, packageId }) => {
  return {
    stickerId: !stickerId ? 'Sticker ID is required' : undefined,
    packageId: !packageId ? 'Package ID is required' : undefined
  };
};

const StickerForm = (formApi, disabled?: boolean) => (
  <Fragment>
    <div className="kata-info__container">
      <label className="kata-info__label">Sticker ID</label>
      <Text
        field="options.stickerId"
        className="form-control kata-form__input-text"
        disabled={disabled}
      />
    </div>
    <div className="kata-info__container">
      <label className="kata-info__label">Package Id</label>
      <Text
        field="options.packageId"
        className="form-control kata-form__input-text"
        disabled={disabled}
      />
    </div>
  </Fragment>
);

export default StickerForm;
