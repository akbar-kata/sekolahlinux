import React from 'react';
import { connect } from 'react-redux';

import Store from 'interfaces/rootStore';
import { getSelected as getSelectedFlow } from 'stores/bot/flow/selectors';
import { isExist } from 'stores/bot/action/selectors';
import * as Actions from 'stores/bot/action/actions';
import ActionForm from './ActionForm';

interface PropsFromState {
  activeFlow?: string | null;
  isExist: Function;
}

interface PropsFromDispatch {
  addAction: typeof Actions.create;
}

interface Props extends PropsFromState, PropsFromDispatch {
  cancelAction: Function;
  submitAction?: Function;
  subForm?: boolean;
  [key: string]: any;
}

interface States {}

class CreateContainer extends React.Component<Props, States> {
  submitAction = data => {
    const addAction = this.props.addAction(this.props.activeFlow!, data);
    if (this.props.submitAction) {
      this.props.submitAction(addAction);
    }
    this.props.cancelAction();
  };

  render() {
    return (
      <ActionForm
        subForm={this.props.subForm}
        isExist={this.props.isExist}
        submitAction={this.submitAction}
        cancelAction={this.props.cancelAction}
      />
    );
  }
}

const mapStateToProps = ({ bot: { action, flow } }: Store): PropsFromState => {
  const activeFlow = getSelectedFlow(flow);
  return {
    activeFlow,
    isExist: (name: string) => isExist(action, activeFlow as string, name)
  };
};

const mapDispatchToProps = {
  addAction: Actions.create
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CreateContainer);
