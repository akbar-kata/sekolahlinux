import React, { ReactElement } from 'react';
import { connect } from 'react-redux';

import Store from 'interfaces/rootStore';
import * as Action from 'stores/bot/action/actions';
import { getSelected as getSelectedFlow } from 'stores/bot/flow/selectors';
import { getData, getByFlow } from 'stores/bot/action/selectors';

interface PropsFromState {
  data: any;
  index: string[];
}

interface PropsFromDispatch {
  removeAction: typeof Action.remove;
}

interface Props extends PropsFromState, PropsFromDispatch {
  children(props: any): ReactElement<any>;
}

interface States {}

class ActionsContainer extends React.Component<Props, States> {
  render() {
    const { children, ...props } = this.props;
    return children(props);
  }
}

const mapStateToProps = ({ bot: { action, flow } }: Store): PropsFromState => {
  return {
    data: getData(action),
    index: getByFlow(action, getSelectedFlow(flow))
  };
};

const mapDispatchToProps = {
  removeAction: Action.remove
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ActionsContainer);
