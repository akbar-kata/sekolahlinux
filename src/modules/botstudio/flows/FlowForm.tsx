import React from 'react';
import { Formik, Form } from 'formik';
import * as yup from 'yup';

import {
  Drawer,
  DrawerBody,
  DrawerHeader,
  DrawerFooter
} from 'components/Drawer';
import { Button } from 'components/Button';
import { Field, FormError, ReactToggle } from 'components/FormikWrapper';

interface Props {
  isOpen: boolean;
  flowId?: string;
  data?: any;
  isExist: Function;
  onSubmit: Function;
  onCancel: Function;
}

interface States {}

class FlowForm extends React.Component<Props, States> {
  validationSchema = yup.object().shape({
    name: yup
      .string()
      .required('A name is required')
      .matches(/^[A-Za-z][-A-Za-z0-9_-]*$/, {
        message: 'Name only allowed charaters alphanumeric, _ and -'
      })
      .test(
        'isNameExists',
        'Name already used, pick another name',
        name => !this.props.isExist(name, this.props.flowId)
      )
  });

  closeForm = () => {
    this.props.onCancel();
  };

  extractExpired = (expire?: number) => {
    const obj = {};
    if (expire) {
      let num = expire;
      const hour = Math.floor(num / 3600000);
      if (hour >= 1) {
        obj['hour'] = hour;
        num = num % 3600000;
      }
      const minute = Math.floor(num / 60000);
      if (minute >= 1) {
        obj['minute'] = minute;
        num = num % 60000;
      }
      const second = Math.floor(num / 1000);
      if (second >= 1) {
        obj['second'] = second;
      }
    }
    return obj;
  };

  onSubmit = data => {
    const { hour, minute, second, ...rest } = data;
    let expire: undefined | number;
    if (hour || minute || second) {
      expire = 0;
      if (hour) {
        expire += hour * 3600000;
      }
      if (minute) {
        expire += minute * 60000;
      }
      if (second) {
        expire += second * 1000;
      }
    }
    this.props.onSubmit({
      ...rest,
      expire
    });
  };

  renderInnerForm = ({ values, setFieldValue }) => {
    const type = this.props.flowId ? 'update' : 'create';
    return (
      <Form className="kata-form full-size">
        <DrawerHeader title={`${type} Flow`} />
        <DrawerBody>
          <div className="kata-info__container">
            <div className="kata-info__label">Name</div>
            <div className="kata-info__content">
              <Field
                autoFocus
                name="name"
                placeholder="Flow name"
                className="form-control kata-form__input-text"
              />
            </div>
          </div>
          <div className="kata-info__container">
            <div className="kata-info__label">Expires in</div>
            <div className="kata-info__content row no-gutters">
              <div className="col pr-1">
                <Field
                  type="number"
                  name="hour"
                  min={0}
                  className="form-control kata-form__input-text kata-form__input-number px-1"
                  prependElement={
                    <span className="input-group-text text-small px-1">
                      Hour
                    </span>
                  }
                />
              </div>
              <div className="col pr-1">
                <Field
                  type="number"
                  name="minute"
                  min={0}
                  className="form-control kata-form__input-text kata-form__input-number px-1"
                  prependElement={
                    <span className="input-group-text text-small px-1">
                      Minute
                    </span>
                  }
                />
              </div>
              <div className="col">
                <Field
                  type="number"
                  name="second"
                  className="form-control kata-form__input-text kata-form__input-number px-1"
                  min={0}
                  prependElement={
                    <span className="input-group-text text-small px-1">
                      Second
                    </span>
                  }
                />
              </div>
            </div>
            <FormError name="hour" />
          </div>
          <div className="kata-info__container">
            <div className="d-inline-block mr-5">
              <ReactToggle name="active" label="Active" />
            </div>
            <div className="d-inline-block mr-5">
              <ReactToggle name="volatile" label="Persistent" />
            </div>
          </div>
          <div className="kata-info__container">
            <ReactToggle name="fallback" label="Default" />
          </div>
        </DrawerBody>
        <DrawerFooter>
          <Button
            type="submit"
            color="primary"
            className="text-capitalize mr-1"
          >
            {type}
          </Button>
          <Button color="secondary" onClick={this.closeForm}>
            Cancel
          </Button>
        </DrawerFooter>
      </Form>
    );
  };

  render() {
    const { isOpen, data } = this.props;
    return (
      <Drawer isOpen={isOpen} onClose={this.closeForm}>
        <Formik
          validationSchema={this.validationSchema}
          initialValues={{
            active: true,
            ...data,
            ...this.extractExpired(data ? data.expire : undefined)
          }}
          onSubmit={this.onSubmit}
        >
          {this.renderInnerForm}
        </Formik>
      </Drawer>
    );
  }
}

export default FlowForm;
