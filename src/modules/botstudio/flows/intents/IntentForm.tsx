import React, { Fragment } from 'react';
import { Formik, Form, FieldArray } from 'formik';
import * as yup from 'yup';

import {
  Field,
  ReactSelect,
  ReactToggle,
  ReactTagsInput,
  ReactAce
} from 'components/FormikWrapper';
import {
  Drawer,
  DrawerBody,
  DrawerHeader,
  DrawerFooter
} from 'components/Drawer';
import { Button, SupportButton, FloatingButton } from 'components/Button';
import {
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from 'components/Dropdown';

import MethodSelector from 'containers/MethodSelector';
import NLUSelector from 'containers/NLUSelector';

interface Props {
  type?: string;
  isOpen: boolean;
  intentId?: string;
  data?: { [key: string]: any };
  isExist: Function;
  onCancel: Function;
  onSubmit(data: any);
}

interface States {}

const inputId = 'form-input-intent';

const typeOptions = [
  {
    label: 'Text',
    value: 'text'
  },
  {
    label: 'Command',
    value: 'command'
  },
  {
    label: 'Data',
    value: 'data'
  }
];

class IntentForm extends React.PureComponent<Props, States> {
  validationSchema = yup.lazy((values: any) => {
    return yup.object().shape({
      name: yup
        .string()
        .required('A name is required')
        .matches(
          /^[A-Za-z][-A-Za-z0-9_-]*$/,
          'Name only allowed charaters alphanumeric, _ and -'
        )
        .test(
          'isNameExist',
          'Name already used, pick another name',
          intentId => !this.props.isExist(values.name, intentId)
        ),
      type: yup.string().required('Type must be selected'),
      condition: yup.string().required('Condition is required'),
      classifier: yup.object().shape({
        nlu: yup.string().required('NLU must be selected'),
        hint: yup
          .string()
          .oneOf(['Hint'], 'Hint must be specified')
          .required('Hint is required'),
        options: yup
          .string()
          .oneOf(['//option use yaml'], 'Options must be specified'),
        process: yup.string().required('Process must be selected'),
        dict: yup.object().shape({
          key: yup.string().required('A key is required'),
          words: yup.string().required('Need at least one word defined')
        })
      }),
      attributes: yup.object().shape({
        name: yup
          .string()
          .required('A name must be specific')
          .matches(
            /^[A-Za-z][-A-Za-z0-9_-]*$/,
            'Name only allowed charaters alphanumeric, _ and -'
          ),
        nlu: yup.string().required('NLU must be selected'),
        options: yup
          .string()
          .oneOf(['//option use yaml'], 'Options must be specified'),
        process: yup.string().required('Process must be selected'),
        dict: yup.object().shape({
          key: yup
            .string()
            .required('A key is required')
            .matches(
              /^[A-Za-z][-A-Za-z0-9_-]*$/,
              'Key only allowed charaters alphanumeric, _ and -'
            ),
          words: yup.string().required('Need at least one word defined')
        })
      })
    });
  });

  componentDidMount() {
    const el = document.getElementById(inputId);
    if (el && el.focus) {
      el.focus();
    }
  }

  onClose = () => {
    this.props.onCancel();
  };

  renderClassifierForm = ({ values, setFieldValue, setValues }) => {
    return (
      <Fragment>
        <div className="pt-2">
          <label className="kata-form__label">Classifier</label>
          <FieldArray
            name="classifier"
            render={arrayHelpers => (
              <Fragment>
                {values.classifier &&
                  values.classifier.map((classifier, index) => (
                    <Fragment key={index}>
                      <div className="kata-intents__attributes--container">
                        <div className="kata-intents__attributes--group">
                          <div className="kata-info__container kata-intents__attributes-row1--wrapper pr-1">
                            <div className="kata-info__label">NLU</div>
                            <div className="kata-info__content">
                              <NLUSelector field={`classifier.${index}.nlu`} />
                            </div>
                          </div>
                          <div className="kata-info__container kata-intents__attributes-row2--wrapper pl-1">
                            <div className="kata-info__label">Match</div>
                            <div className="kata-info__content">
                              <Field
                                name={`classifier.${index}.match`}
                                className="form-control kata-form__input-text"
                              />
                            </div>
                          </div>
                          <div className="kata-info__container kata-intents__attributes-row3--wrapper pl-1">
                            <div className="kata-info__content">
                              <Dropdown key={index}>
                                <DropdownToggle caret={false}>
                                  <Button isIcon>
                                    <i className="icon-more" />
                                  </Button>
                                </DropdownToggle>
                                <DropdownMenu right>
                                  <DropdownItem
                                    onClick={() =>
                                      setFieldValue(
                                        `classifier.${index}.hint`,
                                        'Hint'
                                      )
                                    }
                                  >
                                    Add hint
                                  </DropdownItem>
                                  <DropdownItem
                                    onClick={() =>
                                      setFieldValue(
                                        `classifier.${index}.options`,
                                        '//option use yaml'
                                      )
                                    }
                                  >
                                    Add custom option
                                  </DropdownItem>
                                  <DropdownItem
                                    onClick={() =>
                                      setFieldValue(
                                        `classifier.${index}.process`,
                                        values.classifier[index].process
                                          ? [
                                              ...values.classifier[index]
                                                .process,
                                              ''
                                            ]
                                          : ['']
                                      )
                                    }
                                  >
                                    Add processor
                                  </DropdownItem>
                                  <DropdownItem
                                    onClick={() =>
                                      setFieldValue(
                                        `classifier.${index}.dict`,
                                        values.classifier[index].dict
                                          ? [
                                              ...values.classifier[index].dict,
                                              ''
                                            ]
                                          : ['']
                                      )
                                    }
                                  >
                                    Add dictionary
                                  </DropdownItem>
                                  <DropdownItem
                                    onClick={() =>
                                      setFieldValue(
                                        `classifier.${index}.expression`,
                                        ''
                                      )
                                    }
                                  >
                                    Add Expression
                                  </DropdownItem>
                                </DropdownMenu>
                              </Dropdown>
                            </div>
                          </div>
                        </div>

                        {values.classifier[index].hint && (
                          <div className="kata-info__container">
                            <div className="kata-info__label">Hint</div>
                            <div className="kata-info__content">
                              <Field
                                name={`classifier.${index}.hint`}
                                className="form-control kata-form__input-text"
                              />
                            </div>
                          </div>
                        )}
                        {values.classifier[index].options && (
                          <div className="kata-info__container">
                            <div className="kata-info__label">Options</div>
                            <div className="kata-info__content">
                              <ReactAce
                                name={`classifier.${index}.options`}
                                mode="yaml"
                                fontSize={13}
                                height={100}
                              />
                            </div>
                          </div>
                        )}
                        {values.classifier[index].expression && (
                          <div className="kata-info__container">
                            <div className="kata-info__label">Expression</div>
                            <div className="kata-info__content">
                              <Field
                                name={`classifier.${index}.expression`}
                                className="form-control kata-form__input-text"
                              />
                            </div>
                          </div>
                        )}
                        {values.classifier[index].process && (
                          <Fragment>
                            <div className="kata-info__label">Processor</div>
                            {values.classifier[index].process.map(
                              (process, i) => (
                                <div key={i} className="kata-info__container">
                                  <div className="kata-intents__attributes--group">
                                    <div className="kata-info__content kata-intents__attributes-row4--wrapper pr-1">
                                      <MethodSelector
                                        field={`classifier.${index}.process.${i}`}
                                        useRoot
                                      />
                                    </div>
                                    <div className="kata-info__content kata-intents__attributes-row5--wrapper pr-1">
                                      <FloatingButton
                                        type="button"
                                        icon="trash"
                                        color="danger"
                                        className="kata-intents__dictionary--remove-btn"
                                        onClick={() =>
                                          values.classifier[
                                            index
                                          ].process.splice(i, 1)
                                        }
                                      />
                                    </div>
                                  </div>
                                </div>
                              )
                            )}
                          </Fragment>
                        )}
                        {values.classifier[index].dict && (
                          <Fragment>
                            <div className="kata-info__label">Dictionary</div>
                            {values.classifier[index].dict.map((dict, i) => (
                              <div key={i} className="kata-info__container">
                                <div className="kata-intents__attributes--group">
                                  <div className="kata-info__content kata-intents__attributes-row4--wrapper pr-1">
                                    <Field
                                      name={`classifier.${index}.dict.${i}.key`}
                                      placeholder="Key"
                                      className="form-control kata-form__input-text"
                                    />
                                  </div>
                                  <div className="kata-info__content kata-intents__attributes-row5--wrapper pr-1">
                                    <FloatingButton
                                      type="button"
                                      icon="trash"
                                      color="danger"
                                      className="kata-intents__dictionary--remove-btn"
                                      onClick={() =>
                                        values.classifier[index].dict.splice(
                                          i,
                                          1
                                        )
                                      }
                                    />
                                  </div>
                                </div>
                                <div className="kata-info__content">
                                  <ReactTagsInput
                                    name={`classifier.${index}.dict.${i}.words`}
                                    className="react-tagsinput-sm kata-form__input-textarea"
                                    placeholder="Add a word"
                                  />
                                </div>
                              </div>
                            ))}
                          </Fragment>
                        )}
                      </div>
                      <div className="kata-intents__attributes-action-footer text-right">
                        <Button
                          color="danger"
                          size="sm"
                          className="text-right"
                          onClick={() => arrayHelpers.remove(index)}
                        >
                          Delete
                        </Button>
                      </div>
                    </Fragment>
                  ))}

                <SupportButton
                  className="d-block mt-1"
                  onClick={() => arrayHelpers.push({})}
                >
                  Add classifier
                </SupportButton>
              </Fragment>
            )}
          />
        </div>
      </Fragment>
    );
  };

  renderAttributesForm = ({ values, setFieldValue }) => {
    return (
      <Fragment>
        <div className="pt-2">
          <div className="kata-info__label">Attributes</div>
          <FieldArray
            name="attributes"
            render={arrayHelpers => (
              <div>
                {values.attributes &&
                  values.attributes.map((attribute, index) => (
                    <Fragment key={index}>
                      <div className="kata-intents__attributes--container">
                        <div className="kata-intents__attributes--group">
                          <div className="kata-info__container kata-intents__attributes-row1--wrapper pr-1">
                            <div className="kata-info__label">Name</div>
                            <div className="kata-info__content">
                              <Field
                                name={`attributes.${index}.name`}
                                className="form-control kata-form__input-text"
                              />
                            </div>
                          </div>
                          <div className="kata-info__container kata-intents__attributes-row2--wrapper pl-1">
                            <div className="kata-info__label">NLU</div>
                            <div className="kata-info__content">
                              <NLUSelector field={`attributes.${index}.nlu`} />
                            </div>
                          </div>
                          <div className="kata-info__container kata-intents__attributes-row3--wrapper pl-1">
                            <div className="kata-info__content">
                              <Dropdown key={index}>
                                <DropdownToggle caret={false}>
                                  <Button isIcon>
                                    <i className="icon-more" />
                                  </Button>
                                </DropdownToggle>
                                <DropdownMenu right>
                                  <DropdownItem
                                    onClick={() =>
                                      setFieldValue(
                                        `attributes.${index}.options`,
                                        '//option use yaml'
                                      )
                                    }
                                  >
                                    Add custom option
                                  </DropdownItem>
                                  <DropdownItem
                                    onClick={() =>
                                      setFieldValue(
                                        `attributes.${index}.process`,
                                        values.attributes[index].process
                                          ? [
                                              ...values.attributes[index]
                                                .process,
                                              ''
                                            ]
                                          : ['']
                                      )
                                    }
                                  >
                                    Add processor
                                  </DropdownItem>
                                  <DropdownItem
                                    onClick={() =>
                                      setFieldValue(
                                        `attributes.${index}.dict`,
                                        values.attributes[index].dict
                                          ? [
                                              ...values.attributes[index].dict,
                                              ''
                                            ]
                                          : ['']
                                      )
                                    }
                                  >
                                    Add dictionary
                                  </DropdownItem>
                                </DropdownMenu>
                              </Dropdown>
                            </div>
                          </div>
                        </div>
                        <div className="kata-info__container">
                          <div className="kata-info__label">Path</div>
                          <div className="kata-info__content">
                            <Field
                              name={`attributes.${index}.path`}
                              className="form-control kata-form__input-text"
                            />
                          </div>
                        </div>

                        {values.attributes[index].options && (
                          <div className="kata-info__container">
                            <div className="kata-info__label">Options</div>
                            <div className="kata-info__content">
                              <ReactAce
                                name={`classifier.${index}.options`}
                                mode="yaml"
                                fontSize={13}
                                height={100}
                              />
                            </div>
                          </div>
                        )}
                        {values.attributes[index].process && (
                          <div className="kata-info__container">
                            <div className="kata-info__label">Processor</div>
                            {values.attributes[index].process.map(
                              (process, i) => (
                                <div key={i} className="kata-info__container">
                                  <div className="kata-intents__attributes--group">
                                    <div className="kata-info__content kata-intents__attributes-row4--wrapper pr-1">
                                      <MethodSelector
                                        field={`attributes.${index}.process.${i}`}
                                        useRoot
                                      />
                                    </div>
                                    <div className="kata-info__content kata-intents__attributes-row5--wrapper pr-1">
                                      <FloatingButton
                                        type="button"
                                        icon="trash"
                                        color="danger"
                                        className="kata-intents__dictionary--remove-btn"
                                        onClick={() =>
                                          values.attributes[
                                            index
                                          ].process.splice(i, 1)
                                        }
                                      />
                                    </div>
                                  </div>
                                </div>
                              )
                            )}
                          </div>
                        )}
                        {values.attributes[index].dict && (
                          <Fragment>
                            <div className="kata-info__label">Dictionary</div>
                            {values.attributes[index].dict.map((dict, i) => (
                              <div key={i} className="kata-info__container">
                                <div className="kata-intents__attributes--group">
                                  <div className="kata-info__content kata-intents__attributes-row4--wrapper pr-1">
                                    <Field
                                      name={`attributes.${index}.dict.${i}.key`}
                                      placeholder="Key"
                                      className="form-control kata-form__input-text"
                                    />
                                  </div>
                                  <div className="kata-info__content kata-intents__attributes-row5--wrapper pr-1">
                                    <FloatingButton
                                      type="button"
                                      icon="trash"
                                      color="danger"
                                      className="kata-intents__dictionary--remove-btn"
                                      onClick={() =>
                                        values.attributes[index].dict.splice(
                                          i,
                                          1
                                        )
                                      }
                                    />
                                  </div>
                                </div>
                                <div className="kata-info__content">
                                  <ReactTagsInput
                                    name={`attributes.${index}.dict.${i}.words`}
                                    className="react-tagsinput-sm kata-form__input-textarea"
                                    inputProps={{
                                      placeholder: 'Add a word'
                                    }}
                                  />
                                </div>
                              </div>
                            ))}
                          </Fragment>
                        )}
                      </div>
                      <div className="kata-intents__attributes-action-footer text-right">
                        <Button
                          color="danger"
                          size="sm"
                          className="text-right"
                          onClick={() => arrayHelpers.remove(index)}
                        >
                          Delete
                        </Button>
                      </div>
                    </Fragment>
                  ))}
                <SupportButton
                  className="d-block mt-1"
                  onClick={() => arrayHelpers.push({})}
                >
                  Add attributes
                </SupportButton>
              </div>
            )}
          />
        </div>
      </Fragment>
    );
  };

  renderInnerForm = formApi => {
    const { values, submitForm } = formApi;
    return (
      <Form className="kata-form full-size">
        <DrawerHeader title={`${this.props.type} Intent`} />
        <DrawerBody>
          <div className="kata-info__container">
            <div className="kata-info__label">Name</div>
            <div className="kata-info__content">
              <Field
                id={inputId}
                name="name"
                className="form-control kata-form__input-text"
              />
            </div>
          </div>
          <div className="kata-intents__options--container">
            <div className="kata-info__container kata-intents__options--group">
              <div className="kata-info__label">Options</div>
              <div className="kata-info__content">
                <div className="kata-intents__options--wrapper">
                  <div className="kata-intents__options--initial">
                    <ReactToggle name="initial" label="As initial" />
                  </div>
                  <div className="kata-intents__options--fallback">
                    <ReactToggle name="fallback" label="As fallback" noFalse />
                  </div>
                </div>
              </div>
            </div>
            <div className="kata-info__container">
              <div className="kata-info__label">Priority</div>
              <div className="kata-info__content">
                <div className="kata-intents__options--priority">
                  <Field
                    type="number"
                    name="priority"
                    min={0}
                    className="form-control kata-form__input-text botstudio-input"
                  />
                </div>
              </div>
            </div>
          </div>
          {(!values.fallback && (
            <div className="kata-info__container">
              <div className="kata-info__label">Type</div>
              <div className="kata-info__content">
                <ReactSelect
                  name="type"
                  clearable={false}
                  simpleValue
                  options={typeOptions}
                />
              </div>
            </div>
          )) ||
            null}

          <div className="kata-info__container">
            <div className="kata-info__label">Condition</div>
            <FieldArray
              name="condition"
              render={arrayHelpers => (
                <Fragment>
                  {values.condition &&
                    values.condition.map((condition, index) => (
                      <div
                        className="kata-intents__condition--wrapper pt-1"
                        key={index}
                      >
                        <div className="kata-info__content kata-intents__condition-text">
                          <Field
                            name={`condition.${index}`}
                            className="form-control kata-form__input-text"
                          />
                        </div>
                        <FloatingButton
                          icon="trash"
                          color="danger"
                          className="kata-intents__condition--remove-btn"
                          onClick={() => arrayHelpers.remove(index)}
                        />
                      </div>
                    ))}
                  <SupportButton
                    className="d-block mt-1"
                    onClick={() => arrayHelpers.push('')}
                  >
                    Add condition
                  </SupportButton>

                  {values.type === 'text' && this.renderClassifierForm(formApi)}
                  {values.type === 'text' && this.renderAttributesForm(formApi)}
                </Fragment>
              )}
            />
          </div>
        </DrawerBody>
        <DrawerFooter>
          <Button
            color="primary"
            onClick={submitForm}
            className="mr-1 text-capitalize"
          >
            {this.props.type}
          </Button>
          <Button onClick={this.onClose}>Cancel</Button>
        </DrawerFooter>
      </Form>
    );
  };

  render() {
    const { data, onSubmit } = this.props;
    return (
      <Drawer isOpen={this.props.isOpen} onClose={this.onClose}>
        <Formik
          initialValues={data}
          validationSchema={this.validationSchema}
          onSubmit={onSubmit}
        >
          {this.renderInnerForm}
        </Formik>
      </Drawer>
    );
  }
}

export default IntentForm;
