import React, { Fragment } from 'react';
import { connect } from 'react-redux';

import Store from 'interfaces/rootStore';
import { getSelected as getSelectedFlow } from 'stores/bot/flow/selectors';
import { isExist } from 'stores/bot/intent/selectors';
import { openDrawer, closeDrawer } from 'stores/app/drawer/actions';
import { isDrawerOpen } from 'stores/app/drawer/selectors';
import { addNotification } from 'stores/app/notification/actions';
import * as IntentAction from 'stores/bot/intent/actions';

import IntentForm from './IntentForm';
import Card from 'components/Card';
import CardButton from 'components/Card/CardButton';

interface PropsFromState {
  isOpen: boolean;
  activeFlow?: string | null;
  isExist: Function;
}

interface PropsFromDispatch {
  openDrawer: () => ReturnType<typeof openDrawer>;
  closeDrawer: () => ReturnType<typeof closeDrawer>;
  onAdd: typeof IntentAction.create;
  showSuccessNotification: (
    title: string,
    message: string
  ) => ReturnType<typeof addNotification>;
}

interface Props extends PropsFromState, PropsFromDispatch {
  onCancel: Function;
  [key: string]: any;
}

interface States {}

const drawerId = 'IntentCreate';

class IntentCreateContainer extends React.Component<Props, States> {
  submitAction = data => {
    // FIXME: don't type coerce.
    this.props.onAdd(this.props.activeFlow!, data);
    this.props.closeDrawer();
    this.props.showSuccessNotification(
      `${data.name} Intent Created`,
      `You have successfully created ${data.name} intent.`
    );
  };

  render() {
    return (
      <Fragment>
        <Card
          asButton
          className="kata-intents__card kata-intents__card-action"
          onClick={this.props.openDrawer}
        >
          <CardButton label={'Create Intent'} icon="add" />
        </Card>
        <IntentForm
          isExist={this.props.isExist}
          type="create"
          isOpen={this.props.isOpen}
          onCancel={this.props.closeDrawer}
          onSubmit={this.submitAction}
        />
      </Fragment>
    );
  }
}

const mapStateToProps = ({
  bot: { flow, intent, method, nlu },
  app: { drawer }
}: Store): PropsFromState => {
  const activeFlow = getSelectedFlow(flow);
  return {
    activeFlow,
    isOpen: isDrawerOpen(drawer, drawerId),
    isExist: (name: string) => isExist(intent, activeFlow as string, name)
  };
};

const mapDispatchToProps = {
  onAdd: IntentAction.create,
  openDrawer: () => openDrawer(drawerId),
  closeDrawer: () => closeDrawer(drawerId),
  showSuccessNotification: (title: string, message: string) =>
    addNotification({
      title,
      message,
      status: 'success',
      dismissible: true,
      dismissAfter: 5000
    })
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(IntentCreateContainer);
