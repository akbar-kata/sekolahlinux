import React, { Fragment } from 'react';
import { connect } from 'react-redux';

import Store from 'interfaces/rootStore';
import * as IntentActions from 'stores/bot/intent/actions';
import { getSelected as getSelectedFlow } from 'stores/bot/flow/selectors';
import { getData, getByFlow, isExist } from 'stores/bot/intent/selectors';
import { isDrawerOpen, getDrawerData } from 'stores/app/drawer/selectors';
import { openDrawer, closeDrawer } from 'stores/app/drawer/actions';
import { addNotification } from 'stores/app/notification/actions';

import IntentForm from './IntentForm';
import Intents from './Intents';

import './style.scss';

interface PropsFromState {
  // isLoading: boolean;
  activeFlow?: string | null;
  index: string[];
  data: any;
  isExist: Function;
  isUpdateOpen: boolean;
  dataToUpdate: any;
}

interface PropsFromDispatch {
  onRemove: Function;
  onEdit: Function;
  openUpdate: Function;
  closeUpdate: Function;
  showSuccessNotification(title: string, message: string): void;
}

interface Props extends PropsFromState, PropsFromDispatch {}

interface States {
  intentId: string;
}

const updateDrawerId = 'IntentUpdate';

class IntentsContainer extends React.Component<Props, States> {
  constructor(props: Props) {
    super(props);
    this.state = {
      intentId: ''
    };
  }

  openEdit = (selected: any, key: string) => {
    this.setState({ intentId: key });
    this.props.openUpdate(selected);
  };

  closeEdit = () => {
    this.props.closeUpdate();
  };

  submitAction = data => {
    this.props.onEdit(this.props.activeFlow, this.state.intentId, data);
    this.props.showSuccessNotification(
      `${data.name} Intent Updated`,
      `You have successfully updated ${data.name} intent.`
    );

    this.props.closeUpdate();
  };

  render() {
    return (
      <Fragment>
        <IntentForm
          type="update"
          isOpen={this.props.isUpdateOpen}
          onSubmit={this.submitAction}
          onCancel={this.closeEdit}
          intentId={this.state.intentId}
          data={this.props.dataToUpdate}
          isExist={this.props.isExist}
        />
        <Intents
          data={this.props.data}
          index={this.props.index}
          onRemove={this.props.onRemove}
          onUpdate={this.openEdit}
          showNotif={this.props.showSuccessNotification}
        />
      </Fragment>
    );
  }
}

const mapStateToProps = (
  { bot: { flow, intent }, app: { drawer } }: Store,
  ownProps
): PropsFromState => {
  const activeFlow = getSelectedFlow(flow);
  return {
    activeFlow,
    data: getData(intent),
    index: getByFlow(intent, getSelectedFlow(flow)),
    isUpdateOpen: isDrawerOpen(drawer, updateDrawerId),
    dataToUpdate: getDrawerData(drawer, updateDrawerId),
    isExist: (name: string, id: string) =>
      isExist(intent, activeFlow as string, name, id)
  };
};

const mapDispatchToProps = {
  onRemove: IntentActions.remove,
  onEdit: IntentActions.update,
  openUpdate: (data: any) => openDrawer(updateDrawerId, data),
  closeUpdate: () => closeDrawer(updateDrawerId),
  showSuccessNotification: (title: string, message: string) =>
    addNotification({
      title,
      message,
      status: 'success',
      dismissible: true,
      dismissAfter: 5000
    })
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(IntentsContainer);
