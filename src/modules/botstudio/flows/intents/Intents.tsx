import React from 'react';

import IntentCreate from './IntentCreateContainer';
import { DashboardCards } from 'components/Dashboard';
import Card from 'components/Card';
import { Button } from 'components/Button';
import {
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from 'components/Dropdown';
import { confirm } from 'components/Modal';

interface Props {
  data: { [key: string]: any };
  index: string[];
  onRemove: Function;
  onUpdate: Function;
  showNotif(title: string, message: string): void;
}

interface States {}

class Intents extends React.Component<Props, States> {
  onUpdate = (data: any, key: string) => {
    return () => this.props.onUpdate(data, key);
  };
  onRemove = (name: string, id: string) => {
    return () => {
      confirm({
        title: 'Delete Intent',
        message: `Are you sure you want to delete this intent?`,
        okLabel: 'Delete',
        cancelLabel: 'Cancel'
      }).then(res => {
        if (res) {
          this.props.onRemove(id);
          this.props.showNotif(
            `${name} Intent Deleted`,
            `${name} intent has been deleted.`
          );
        }
      });
    };
  };
  render() {
    const { index, data } = this.props;
    return (
      <div className="kata-intents__container">
        <DashboardCards className="kata-intents__wrapper">
          <IntentCreate />
          {!!index.length &&
            index.map(key => {
              const { name } = data[key];
              return (
                <Card
                  key={key}
                  className={`kata-intents__card`}
                  title={name}
                  onClick={this.onUpdate(data[key], key)}
                  action={
                    <Dropdown>
                      <DropdownToggle caret={false}>
                        <Button isIcon>
                          <i className="icon-more" />
                        </Button>
                      </DropdownToggle>
                      <DropdownMenu right>
                        <DropdownItem onClick={this.onUpdate(data[key], key)}>
                          Update
                        </DropdownItem>
                        <DropdownItem onClick={this.onRemove(name, key)}>
                          Delete
                        </DropdownItem>
                      </DropdownMenu>
                    </Dropdown>
                  }
                >
                  <div className="row mb-2">
                    <div className="col-7">
                      <div className="text-label">Type</div>
                      {data[key].type}
                    </div>
                  </div>
                </Card>
              );
            })}
        </DashboardCards>
      </div>
    );
  }
}

export default Intents;
