import React from 'react';
import { connect } from 'react-redux';

import { fetchVersionRequest } from 'stores/bot/version/actions';

import VersionRefresh from './VersionRefresh';

interface PropsFromState {
  selectedBot: string;
  latest: string;
}

interface PropsFromDispatch {
  refreshAction: Function;
}

const VersionRefresContainer = (props: any) => <VersionRefresh {...props} />;

const mapStateToProps = ({ bot, version }: any): PropsFromState => {
  return {
    selectedBot: bot.selected,
    latest: version.latest
  };
};

const mapDispatchToProps = (dispatch: Function): PropsFromDispatch => {
  return {
    refreshAction: (botId: string) => dispatch(fetchVersionRequest(botId))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(VersionRefresContainer);
