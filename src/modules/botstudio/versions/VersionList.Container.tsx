import React from 'react';
import { connect } from 'react-redux';

import RootStore from 'interfaces/rootStore';

import {
  fetchKataMLRequest,
  clearVersionKataML,
  selectVersion
} from 'stores/bot/version/actions';
import {
  getVersionLoading,
  getVersionKataML
} from 'stores/bot/version/selectors';
import { isDrawerOpen, getDrawerData } from 'stores/app/drawer/selectors';
import { closeDrawer, openDrawer } from 'stores/app/drawer/actions';
import { addNotification } from 'stores/app/notification/actions';
import { getProjectSelected } from 'stores/project/selectors';
import { getLoading } from 'stores/app/loadings/selectors';
import { isModalOpen } from 'stores/app/modal/selectors';
import { BotRevision } from 'interfaces/revision';
import REVISION from 'stores/revision/types';
import { fetchBotRevisionsRequest } from 'stores/revision/actions';
import {
  getBotRevisionsData,
  getBotRevisionsIndex
} from 'stores/revision/bot/selectors';

import VersionList from './VersionList';

interface PropsFromState {
  selectedProject: string | null;
  isLoading: boolean;
  isFetchingKataML: boolean;
  data: Record<string, BotRevision>;
  index: string[];
  isKatamlOpen: boolean;
  kataml?: {
    yaml: string;
    json: string;
  };
  isVersionListOpen: boolean;
  versionData: any;
  // botDetail: any;
}

interface PropsFromDispatch {
  fetchKataML: Function;
  fetchRevisions(projectId: string): any;
  openDrawer(data: any): void;
  closeDrawer(): void;
  clearVersionKataML(botId: string, version: string): void;
  setSelectedVersion(version: string): any;
  showSuccessNotification(title: string, message: string): void;
}

interface Props extends PropsFromState, PropsFromDispatch {}

interface State {
  isOpen: boolean;
  key: string;
}

const modalId = 'VersionKataML';

class VersionListContainer extends React.Component<Props, State> {
  public state = {
    isOpen: false,
    key: ''
  };

  componentDidMount() {
    if (this.props.selectedProject) {
      this.props.fetchRevisions(this.props.selectedProject);
    }
  }

  componentDidUpdate(prev: Props) {
    if (
      this.props.selectedProject &&
      prev.selectedProject !== this.props.selectedProject
    ) {
      this.props.fetchRevisions(this.props.selectedProject);
    }
  }

  render() {
    return (
      <VersionList
        {...this.props}
        kataml={this.props.kataml}
        fetchKataML={this.props.fetchKataML}
        showNotif={this.props.showSuccessNotification}
      />
    );
  }
}

const mapStateToProps = ({
  project,
  revision,
  bot: { version },
  app: { modal, drawer, loadings }
}: RootStore): PropsFromState => {
  const selectedBot = getProjectSelected(project) as string;
  return {
    selectedProject: selectedBot,
    isLoading: getLoading(loadings, REVISION.FETCH_BOT_REVISIONS_REQUEST),
    isFetchingKataML: getVersionLoading(version, 'kataml'),
    data: getBotRevisionsData(revision),
    index: getBotRevisionsIndex(revision),
    kataml: getVersionKataML(version, selectedBot),
    isKatamlOpen: isModalOpen(modal, modalId),
    isVersionListOpen: isDrawerOpen(drawer, 'VersionList'),
    versionData: getDrawerData(drawer, 'VersionList')
  };
};

const mapDispatchToProps: PropsFromDispatch = {
  fetchRevisions: fetchBotRevisionsRequest,
  openDrawer: (data: any) => openDrawer('VersionList', data),
  closeDrawer: () => closeDrawer('VersionList'),
  clearVersionKataML: (botId: string, version: string) =>
    clearVersionKataML(botId, version),
  setSelectedVersion: (version: string) => selectVersion(version),
  fetchKataML: (botId: string, version: string) =>
    fetchKataMLRequest(botId, version),
  showSuccessNotification: (title: string, message: string) =>
    addNotification({
      title,
      message,
      status: 'success',
      dismissible: true,
      dismissAfter: 5000
    })
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(VersionListContainer);
