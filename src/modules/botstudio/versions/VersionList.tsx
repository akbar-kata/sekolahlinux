import React from 'react';
import moment from 'moment';

import VersionInfo from './VersionInfo';
import { EmptyMessage } from 'components/Common';
import { BotRevision } from 'interfaces/revision';
import IconicButton from 'components/Button/IconicButton';
import { InfiniteScrollSpinner } from 'components/Loading';

interface Props {
  isLoading: boolean;
  index: string[];
  data: Record<string, BotRevision>;
  selectedProject: string | null;
  isVersionListOpen: boolean;
  isFetchingKataML: boolean;
  versionData: any;
  kataml: any;
  fetchKataML: Function;
  openDrawer(data: any): void;
  closeDrawer(): void;
  clearVersionKataML(botId: string, version: string): void;
  setSelectedVersion(version: string): any;
  showNotif(title: string, message: string): void;
}

const colWidth = {
  width: 150
};

class VersionList extends React.Component<Props, any> {
  static defaultProps = {
    isVersionListOpen: false
  };

  renderMessage(child: React.ReactNode) {
    return (
      <tr>
        <td colSpan={6} className="text-center">
          {child}
        </td>
      </tr>
    );
  }

  renderLoading() {
    return (
      <tr>
        <td colSpan={4} className="text-center">
          <div className="py-3">
            <InfiniteScrollSpinner />
          </div>
        </td>
      </tr>
    );
  }

  renderData() {
    const { data, index } = this.props;

    if (index.length === 0) {
      return this.renderMessage(
        <EmptyMessage title="Version list is empty">
          This bot don’t have any version yet. Publish the bot by clicking the
          publish button on top.
        </EmptyMessage>
      );
    }

    return index.map((revisionId, idx) => {
      const revision = data[revisionId];
      if (!revision) {
        return null;
      }
      return (
        <tr key={idx} className={false ? 'kata-version--latest' : ''}>
          <td>
            <samp>{revision.revision}</samp>
          </td>
          <td>
            {revision.created_at
              ? moment(revision.created_at).format('DD MMM YYYY - HH:mm:ss')
              : '-'}
          </td>
          <td>{revision.author || '-'}</td>
          <td className="text-center">
            <div
              className="kata-version__detail-icon"
              onClick={() => {
                this.props.openDrawer({
                  version: revision.revision,
                  username: revision.author || '-',
                  timestamp: moment(revision.created_at).format(
                    'DD MMM YYYY - HH:mm:ss'
                  ),
                  changelog: revision.changelog
                });
                this.props.setSelectedVersion(revision.id);
              }}
            >
              <IconicButton>
                <i className="icon icon-view mr-1" />
                View
              </IconicButton>
            </div>
          </td>
        </tr>
      );
    });
  }

  render() {
    const { isLoading, selectedProject } = this.props;

    return (
      <div className="kata-version__table">
        <table className="kata-table kata-table--striped kata-table--hover">
          <thead>
            <tr>
              <th style={colWidth}>Revision</th>
              <th>Date</th>
              <th>Published by</th>
              <th style={colWidth} className="text-center">
                Action
              </th>
            </tr>
          </thead>
          <tbody>
            {((isLoading || !selectedProject) && this.renderLoading()) ||
              this.renderData()}
          </tbody>
        </table>
        <VersionInfo {...this.props} />
      </div>
    );
  }
}

export default VersionList;
