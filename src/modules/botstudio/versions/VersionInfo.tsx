import React, { Fragment } from 'react';
import isEmpty from 'lodash-es/isEmpty';
import AceEditor from 'react-ace';

import {
  Drawer,
  DrawerBody,
  DrawerHeader,
  DrawerFooter
} from 'components/Drawer';
import { DropdownSelector, DropdownItem } from 'components/Dropdown';
import { Button, SupportButton } from 'components/Button';

import 'brace';
import 'brace/mode/yaml';
import { BotRevision } from 'interfaces/revision';

interface Props {
  isLoading: boolean;
  data: Record<string, BotRevision>;
  selectedProject: string | null;
  isVersionListOpen: boolean;
  isFetchingKataML: boolean;
  versionData: any;
  kataml: any;
  fetchKataML: Function;
  openDrawer(data: any): void;
  closeDrawer(): void;
  clearVersionKataML(botId: string | null, version: string): void;
  showNotif(title: string, message: string): void;
}

interface State {
  mode: string;
  isShow: boolean;
  readyToDownload: boolean;
}

const defaultState = {
  mode: 'yaml',
  isShow: false,
  readyToDownload: false
};

class VersionInfo extends React.Component<Props, State> {
  state = defaultState;

  modeChange = (mode: string) => {
    this.setState({
      mode
    });
  };

  fetchKataML = () => {
    return this.props.fetchKataML(
      this.props.selectedProject,
      this.props.versionData.version
    );
  };

  toggleKataML = () => {
    if (isEmpty(this.props.kataml)) {
      this.fetchKataML();
    }

    this.setState({ isShow: !this.state.isShow });
  };

  prepareDownload = (kataml: any) => {
    const ele = document.createElement('a');
    const file = new Blob([kataml[this.state.mode] || ''], {
      type: `text/plain`
    });
    ele.href = URL.createObjectURL(file);
    ele.download = `kataml.${this.state.mode}`;
    ele.click();
  };

  downloadKataML = () => {
    const promise = Promise.resolve();

    if (isEmpty(this.props.kataml)) {
      return promise
        .then(() => this.fetchKataML())
        .then((resp: any) => this.prepareDownload(resp.kataml))
        .then(() =>
          this.props.showNotif(
            'KataML Downloaded',
            'You have successfully downloaded the KataML.'
          )
        );
    }

    return promise
      .then(() => this.prepareDownload(this.props.kataml))
      .then(() =>
        this.props.showNotif(
          'KataML Downloaded',
          'You have successfully downloaded the KataML.'
        )
      );
  };

  onCloseDrawer = () => {
    this.setState(defaultState);
    this.props.clearVersionKataML(
      this.props.selectedProject,
      this.props.versionData.version
    );
    this.props.closeDrawer();
  };

  render() {
    const { isVersionListOpen, versionData = {}, kataml = {} } = this.props;

    return (
      <Drawer isOpen={isVersionListOpen} onClose={this.onCloseDrawer}>
        <DrawerHeader title="View Revision List" />
        <DrawerBody>
          <div className="row">
            <div className="col">
              <div className="kata-info__container">
                <div className="kata-info__label">Revision</div>
                <div className="kata-info__content">
                  <input
                    type="text"
                    className="form-control"
                    value={versionData.version}
                    disabled
                  />
                </div>
              </div>
            </div>

            <div className="col">
              <div className="kata-info__container">
                <div className="kata-info__label">Date</div>
                <div className="kata-info__content">
                  <input
                    type="text"
                    className="form-control"
                    value={versionData.timestamp}
                    disabled
                  />
                </div>
              </div>
            </div>
          </div>

          <div className="kata-info__container">
            <div className="kata-info__label">Published by</div>
            <div className="kata-info__content">
              <input
                type="text"
                className="form-control"
                value={versionData.username}
                disabled
              />
            </div>
          </div>

          <div className="kata-info__container">
            <div className="kata-info__label">Change log</div>
            <div className="kata-info__content">
              <textarea
                name="changelog"
                id="changelog"
                cols={30}
                rows={10}
                className="form-control"
                value={versionData.changelog}
                disabled
              />
            </div>
          </div>

          <div className="kata-info__container">
            <div className="kata-info__label">KataML</div>
          </div>

          {this.state.isShow && (
            <Fragment>
              <div className="kata-info__container">
                <div className="kata-info__label">Mode</div>
                <div className="kata-info__content">
                  <DropdownSelector
                    value={this.state.mode.toUpperCase()}
                    onSelect={(val: string) => this.modeChange(val)}
                    block
                  >
                    <DropdownItem value="yaml">YAML</DropdownItem>
                    <DropdownItem value="json">JSON</DropdownItem>
                  </DropdownSelector>
                </div>
              </div>

              <div className="kata-info__container">
                <div className="kata-info__content">
                  {!isEmpty(kataml[this.state.mode]) && (
                    <AceEditor
                      mode="yaml"
                      value={kataml[this.state.mode]}
                      name="kataML"
                      editorProps={{ $blockScrolling: true }}
                      width={'384px'}
                      height={'350px'}
                      className="kata-version__kataml ace-kata"
                      readOnly
                      onLoad={(editor: any) => editor.renderer.setPadding(8)}
                    />
                  )}
                </div>
              </div>
            </Fragment>
          )}

          <div className="kata-info__container">
            <div className="kata-info__content">
              <SupportButton
                className="mr-1"
                onClick={this.toggleKataML}
                loading={this.props.isFetchingKataML}
              >
                {this.state.isShow ? 'Hide' : 'Show'} KataML
              </SupportButton>

              <SupportButton
                onClick={this.downloadKataML}
                loading={this.props.isFetchingKataML}
              >
                Download KataML
              </SupportButton>
            </div>
          </div>
        </DrawerBody>
        <DrawerFooter>
          <Button onClick={this.props.closeDrawer}>Close</Button>
        </DrawerFooter>
      </Drawer>
    );
  }
}

export default VersionInfo;
