import React from 'react';
import { Dashboard } from 'components/Dashboard';
import VersionList from './VersionList.Container';

export default () => (
  <Dashboard
    title="Revision List"
    tooltip="Every time you publish your bot, it will be recorded here."
  >
    <VersionList />
  </Dashboard>
);
