import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';

import SelectedUrl from 'containers/SelectedUrl';
import { Robot } from 'components/Loading';
import LoadingIndicator from './LoadingIndicator';
import Publish from './publish';
import Emulator from './emulator';

const Configs = React.lazy(() => import('./configs'));
const Flows = React.lazy(() => import('./flows'));
const NLUs = React.lazy(() => import('./nlus'));
const Methods = React.lazy(() => import('./methods'));
const Versions = React.lazy(() => import('./versions'));
const ErrorLogs = React.lazy(() => import('./logerrors'));

export default () => {
  return (
    <SelectedUrl>
      {projectId => {
        const parentPath = `/project/${projectId}/bot`;
        return (
          <React.Suspense fallback={<Robot />}>
            <LoadingIndicator>
              <Switch>
                <Route path={`${parentPath}/config`} component={Configs} />
                <Route path={`${parentPath}/flow`} component={Flows} />
                <Route path={`${parentPath}/nlu`} component={NLUs} />
                <Route path={`${parentPath}/method`} component={Methods} />
                <Route path={`${parentPath}/version`} component={Versions} />
                <Route
                  path={`${parentPath}/logs-error`}
                  component={ErrorLogs}
                />
                <Route render={() => <Redirect to={`${parentPath}/flow`} />} />
              </Switch>
              <Publish />
              <Emulator />
            </LoadingIndicator>
          </React.Suspense>
        );
      }}
    </SelectedUrl>
  );
};
