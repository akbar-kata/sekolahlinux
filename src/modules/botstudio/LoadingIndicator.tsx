import React from 'react';
import { connect } from 'react-redux';

import Store from 'interfaces/rootStore';
import { getLoading } from 'stores/app/loadings/selectors';
import BOT_TYPES from 'stores/bot/types';

import { Robot } from 'components/Loading';

interface PropsFromState {
  loading: boolean;
}

interface PropsFromDispatch {}

interface Props extends PropsFromState, PropsFromDispatch {
  children: any;
}

const Container: React.SFC<Props> = props =>
  props.loading ? <Robot /> : props.children;

const mapStateToProps = ({ app }: Store) => {
  return {
    loading: getLoading(app.loadings, BOT_TYPES.DETAIL)
  };
};

const mapDispatchToProps = (dispatch: Function) => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Container);
