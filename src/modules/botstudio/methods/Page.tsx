import React from 'react';

import MethodList from './MethodList.Container';
import { Dashboard } from 'components/Dashboard';
import './style.scss';

class Page extends React.Component<any, any> {
  render() {
    return (
      <Dashboard
        title={'Methods'}
        tooltip="JavaScript functions accessible from various message processing stages."
      >
        <MethodList root />
      </Dashboard>
    );
  }
}

export default Page;
