import React, { Fragment } from 'react';

import Card from 'components/Card';
import { DashboardCards } from 'components/Dashboard';
import { Button } from 'components/Button';
import { Robot } from 'components/Loading';
import {
  Dropdown,
  DropdownItem,
  DropdownToggle,
  DropdownMenu
} from 'components/Dropdown';
import { confirm } from 'components/Modal';
import MethodCreate from './MethodCreate.Container';

interface Props {
  isLoading: boolean;
  error: string | null;
  index: string[];
  data: any;
  onUpdate: Function;
  onRemove: Function;
}

interface States {
  hover?: string;
}

class MethodList extends React.Component<Props, States> {
  state = {
    hover: undefined
  };

  public onUpdate = (id: string, data: any) => {
    return () => this.props.onUpdate({ id, ...data });
  };

  public onRemove = (id: string, name: string) => {
    return () => {
      confirm({
        title: 'Delete Method',
        message: `Are you sure you want to delete this method?`,
        okLabel: 'Delete',
        cancelLabel: 'Cancel'
      }).then(res => {
        if (res) {
          this.props.onRemove(id);
        }
      });
    };
  };

  renderMessage(child: any) {
    return <div>{child}</div>;
  }

  renderLoading() {
    return this.renderMessage(
      <h5 className="text-center py-5">
        <Robot />
      </h5>
    );
  }

  renderError() {
    return this.renderMessage(
      <h5 className="p-4 text-danger text-bold">{this.props.error}</h5>
    );
  }

  renderData() {
    const { index, data } = this.props;
    return (
      <DashboardCards>
        <MethodCreate root />
        {!!index.length &&
          index.map(key => {
            const { name } = data[key];
            return (
              <Card
                key={key}
                className={`kata-method__card`}
                title={name}
                onClick={this.onUpdate(key, data[key])}
                action={
                  <Dropdown>
                    <DropdownToggle caret={false}>
                      <Button isIcon>
                        <i className="icon-more" />
                      </Button>
                    </DropdownToggle>
                    <DropdownMenu right>
                      <DropdownItem onClick={this.onUpdate(key, data[key])}>
                        Update
                      </DropdownItem>
                      <DropdownItem onClick={this.onRemove(key, name)}>
                        Delete
                      </DropdownItem>
                    </DropdownMenu>
                  </Dropdown>
                }
              >
                <div className="row mb-2">
                  <div className="col-7">
                    <div className="text-label">Mode</div>
                    {data[key].mode}
                  </div>
                </div>
                <div className="row mb-2">
                  <div className="col-7">
                    <div className="text-label">Parameters</div>
                    {data[key].params && data[key].params.length ? (
                      <div className="kata-method__labels-container">
                        {data[key].params.map((label, idx) => (
                          <Fragment key={idx}>
                            <label className="badge badge-secondary kata-entity__badge">
                              {label}
                            </label>{' '}
                          </Fragment>
                        ))}
                      </div>
                    ) : null}
                  </div>
                </div>
              </Card>
            );
          })}
      </DashboardCards>
    );
  }

  render() {
    const { isLoading } = this.props;
    return <Fragment>{!isLoading && this.renderData()}</Fragment>;
  }
}

export default MethodList;
