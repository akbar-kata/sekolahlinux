import React, { Fragment } from 'react';
import { connect } from 'react-redux';

import Store from 'interfaces/rootStore';
import { openDrawer, closeDrawer } from 'stores/app/drawer/actions';
import { isDrawerOpen } from 'stores/app/drawer/selectors';
import { getSelected as getSelectedFlow } from 'stores/bot/flow/selectors';
import * as MethodAction from 'stores/bot/method/actions';
import { getData, getByFlow, isExist } from 'stores/bot/method/selectors';
import { addNotification } from 'stores/app/notification/actions';

import Card from 'components/Card';
import CardButton from 'components/Card/CardButton';
import MethodForm from './MethodForm';

interface PropsFromState {
  isOpen: boolean;
  activeFlow?: string | null;
  index: string[];
  data: any;
  isExist: Function;
}

interface PropsFromDispatch {
  openDrawer: Function;
  closeDrawer: Function;
  createAction: Function;
  editAction: Function;
  showSuccessNotification(title: string, message: string): void;
}

interface Props extends PropsFromState, PropsFromDispatch {}

interface State {}

const drawerId = 'MethodCreate';

class MethodCreate extends React.Component<Props, State> {
  createAction = data => {
    this.props.createAction('__ROOT__', data);
    this.props.closeDrawer();
    this.props.showSuccessNotification(
      `Method Created`,
      `You have successfully created method.`
    );
  };
  render() {
    return (
      <Fragment>
        <Card
          asButton
          className="kata-method__add-btn"
          onClick={this.props.openDrawer}
        >
          <CardButton label={'Create Method'} icon="add" />
        </Card>
        <MethodForm
          type="create"
          isOpen={this.props.isOpen}
          closeAction={this.props.closeDrawer}
          submitAction={this.createAction}
          isExist={this.props.isExist}
        />
      </Fragment>
    );
  }
}

const mapStateToProps = (
  { bot: { flow, method }, app: { drawer } }: Store,
  ownProps
): PropsFromState => {
  const activeFlow = ownProps.root ? '__ROOT__' : getSelectedFlow(flow);
  return {
    activeFlow,
    isOpen: isDrawerOpen(drawer, drawerId),
    data: getData(method),
    index: getByFlow(method, activeFlow),
    isExist: (name: string, id?: string) =>
      isExist(method, activeFlow as string, name, id)
  };
};

const mapDispatchToProps: PropsFromDispatch = {
  createAction: MethodAction.create,
  editAction: MethodAction.update,
  openDrawer: () => openDrawer(drawerId),
  closeDrawer: () => closeDrawer(drawerId),
  showSuccessNotification: (title: string, message: string) =>
    addNotification({
      title,
      message,
      status: 'success',
      dismissible: true,
      dismissAfter: 5000
    })
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MethodCreate);
