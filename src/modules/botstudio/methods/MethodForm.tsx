import React from 'react';
import isArray from 'lodash-es/isArray';
import { Formik, Form } from 'formik';
import * as yup from 'yup';

import { JsonObject } from 'interfaces';

import { Button } from 'components/Button';
import {
  Drawer,
  DrawerBody,
  DrawerHeader,
  DrawerFooter
} from 'components/Drawer';
import {
  Field,
  ReactSelect,
  ReactTagsInput,
  ReactAce
} from 'components/FormikWrapper';

interface Props {
  isOpen: boolean;
  id?: string;
  data?: JsonObject;
  isExist: Function;
  submitAction: Function;
  [key: string]: any;
}

interface State {}

const modeOptions = [
  {
    label: 'Simple',
    value: 'simple'
  },
  {
    label: 'Advanced',
    value: 'advanced'
  }
];

class MethodForm extends React.Component<Props, State> {
  validationSchema = yup.object().shape({
    mode: yup.string().required('Mode must be selected'),
    name: yup
      .string()
      .required('A name is required')
      .matches(/^[A-Za-z][-A-Za-z0-9_-]*$/, {
        message: 'Name only allowed charaters alphanumeric, _ and -'
      })
      .test(
        'isNameExists',
        'Name already used, pick another name',
        name => !this.props.isExist(name, this.props.id)
      ),
    entry: yup
      .string()
      .when('mode', {
        is: 'advanced',
        then: yup.string().required('Entry must be selected'),
        otherwise: yup.string().matches(/^[A-Za-z][-A-Za-z0-9_]*$/, {
          message: 'Name only allowed charaters alphanumeric and _'
        })
      })
      .test(
        'isNameExists',
        'Name already used, pick another name',
        name => !this.props.isExist(name, this.props.id)
      ),
    definition: yup
      .string()
      .required('Definition is required')
      .when('mode', {
        is: 'simple',
        then: yup
          .string()
          .test(
            'isValidFunction',
            'Simple mode can not use "function" keyword',
            definition => this.isValidFunction(definition)
          )
      })
      .when('mode', {
        is: 'advance',
        then: yup
          .string()
          .test(
            'isValidFunction',
            'Advanced mode must contain "function"',
            definition => !this.isValidFunction(definition)
          )
      }),
    params: yup.string().required('Parameters is required')
  });

  constructor(props: Props) {
    super(props);
  }

  isValidFunction(value: string) {
    return value.match(/function/g) === null;
  }

  submitAction = data => {
    this.props.submitAction(data);
  };

  renderEntryOptions = values => {
    const matched = (values || '').match(/function(.*?)\(/g);
    return isArray(matched)
      ? matched
          .map(str => str.replace(/function|\(/g, '').trim())
          .map(str => ({
            label: str,
            value: str
          }))
      : [];
  };

  innerForm = ({ values }) => {
    return (
      <Form className="kata-form full-size">
        <DrawerHeader title={`${this.props.type} Method`} />
        <DrawerBody>
          <div className="kata-info__container">
            <div className="kata-info__label">Name</div>
            <div className="kata-info__content">
              <Field
                name="name"
                className="form-control kata-form__input-text"
              />
            </div>
          </div>
          <div className="kata-info__container">
            <div className="kata-info__label">Mode</div>
            <div className="kata-info__content">
              <ReactSelect
                name="mode"
                clearable={false}
                simpleValue
                options={modeOptions}
                className="kata-form__input-select"
              />
            </div>
          </div>
          <div className="kata-info__container">
            <div className="kata-info__label">Parameters</div>
            <div className="kata-info__content">
              <ReactTagsInput
                name="params"
                placeholder="Add parameter"
                className="form-control kata-form__input-textarea"
              />
            </div>
          </div>
          <div className="kata-info__container">
            <div className="kata-info__label">Definition</div>
            <div className="kata-info__content">
              <ReactAce
                name="definition"
                mode="javascript"
                fontSize={13}
                autoFormat
              />
            </div>
          </div>
          {values.mode === 'advanced' && (
            <div className="kata-info__container">
              <div className="kata-info__label">Entry</div>
              <div className="kata-info__content">
                <ReactSelect
                  name="entry"
                  clearable={false}
                  simpleValue
                  options={this.renderEntryOptions(values.definition)}
                />
              </div>
            </div>
          )}
        </DrawerBody>
        <DrawerFooter>
          <Button
            type="submit"
            color="primary"
            className="mr-1 text-capitalize"
          >
            {this.props.type}
          </Button>
          <Button onClick={this.props.closeAction}>Cancel</Button>
        </DrawerFooter>
      </Form>
    );
  };

  render() {
    return (
      <Drawer isOpen={this.props.isOpen} onClose={this.props.closeAction}>
        <Formik
          initialValues={this.props.data}
          validationSchema={this.validationSchema}
          onSubmit={this.submitAction}
        >
          {this.innerForm}
        </Formik>
      </Drawer>
    );
  }
}

export default MethodForm;
