import React, { Fragment } from 'react';
import { connect } from 'react-redux';

import Store from 'interfaces/rootStore';
import * as MethodActions from 'stores/bot/method/actions';
import { getSelected as getSelectedFlow } from 'stores/bot/flow/selectors';
import { getData, getByFlow, isExist } from 'stores/bot/method/selectors';
import { openDrawer, closeDrawer } from 'stores/app/drawer/actions';
import { isDrawerOpen, getDrawerData } from 'stores/app/drawer/selectors';
import { addNotification } from 'stores/app/notification/actions';

import MethodList from './MethodList';
import MethodForm from './MethodForm';

interface PropsFromState {
  activeFlow?: string | null;
  index: string[];
  data: any;
  isExist: Function;
  isUpdateOpen: boolean;
  dataToUpdate: any;
}

interface PropsFromDispatch {
  editAction: Function;
  removeAction: Function;
  openUpdate: Function;
  closeUpdate: Function;
  showSuccessNotification(title: string, message: string): void;
}

interface Props extends PropsFromState, PropsFromDispatch {
  flow?: string;
  [key: string]: any;
}

interface States {
  selected: string | false;
  blocked: boolean;
  newMethod: boolean;
}

const updateDrawerId = 'MethodUpdate';

class MethodListContainer extends React.Component<Props, States> {
  constructor(props: any) {
    super(props);
    this.state = {
      selected: false,
      blocked: false,
      newMethod: false
    };
  }

  onRemove = id => {
    if (this.state.selected !== 'new') {
      this.props.removeAction(id);
    }
    const filteredArr = this.props.index.filter(el => el !== id);
    this.setState({
      blocked: false,
      newMethod: false,
      selected: filteredArr[0] || false
    });
    this.props.showSuccessNotification(
      `Method Deleted`,
      `Method has been deleted.`
    );
  };

  editAction = data => {
    this.props.editAction('__ROOT__', data.id, data);
    this.props.closeUpdate();
    this.props.showSuccessNotification(
      `Method Updated`,
      `You have successfully updated method.`
    );
  };

  openEdit = (selected: any) => {
    this.props.openUpdate(selected);
  };

  closeEdit = () => {
    this.props.closeUpdate();
  };

  render() {
    return (
      <Fragment>
        <MethodForm
          type="update"
          isOpen={this.props.isUpdateOpen}
          closeAction={this.closeEdit}
          submitAction={this.editAction}
          id={this.props.dataToUpdate ? this.props.dataToUpdate.id : undefined}
          data={this.props.dataToUpdate}
          isExist={this.props.isExist}
        />
        <MethodList
          isLoading={this.props.isLoading}
          error={this.props.error}
          index={this.props.index}
          data={this.props.data}
          onUpdate={this.openEdit}
          onRemove={this.onRemove}
        />
      </Fragment>
    );
  }
}

const mapStateToProps = (
  { bot: { flow, method }, app: { drawer } }: Store,
  ownProps
): PropsFromState => {
  const activeFlow = ownProps.root ? '__ROOT__' : getSelectedFlow(flow);
  return {
    activeFlow,
    data: getData(method),
    index: getByFlow(method, activeFlow),
    isExist: (name: string, id?: string) =>
      isExist(method, activeFlow as string, name, id),
    isUpdateOpen: isDrawerOpen(drawer, updateDrawerId),
    dataToUpdate: getDrawerData(drawer, updateDrawerId)
  };
};

const mapDispatchToProps = {
  editAction: MethodActions.update,
  removeAction: MethodActions.remove,
  openUpdate: (data: any) => openDrawer(updateDrawerId, data),
  closeUpdate: () => closeDrawer(updateDrawerId),
  showSuccessNotification: (title: string, message: string) =>
    addNotification({
      title,
      message,
      status: 'success',
      dismissible: true,
      dismissAfter: 5000
    })
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MethodListContainer);
