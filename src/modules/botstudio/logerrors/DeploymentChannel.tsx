import React, { Fragment } from 'react';
import classnames from 'classnames';
import isEmpty from 'lodash-es/isEmpty';

import socialIcons from 'components/utils/SocialIcons';

import EnvironmentContainer from 'containers/Environment.Container';
import ChannelContainer from 'containers/Channel.Container';

import {
  Dropdown,
  DropdownItem,
  DropdownMenu,
  DropdownSelector,
  DropdownToggle
} from 'components/Dropdown';

import { DeploymentChannelContainerProps } from './DeploymentChannelContainer';

const DeploymentChannel: React.SFC<DeploymentChannelContainerProps> = props => {
  return (
    <Fragment>
      <EnvironmentContainer>
        {params => {
          return (
            <div className="kata-logs__selector--container">
              <label className="kata-info__label mr-2">Environment</label>
              <DropdownSelector
                onSelect={(env: string) => {
                  const environmentData = Object.values(params.data).filter(
                    data => data.name === env
                  );
                  params.setEnvironment(environmentData[0].id);
                }}
                value={
                  params.selected && params.data[params.selected]
                    ? params.data[params.selected].name
                    : undefined
                }
              >
                {params.index.map(key => (
                  <DropdownItem key={key} value={params.data[key].name}>
                    {params.data[key].name}
                  </DropdownItem>
                ))}
              </DropdownSelector>
            </div>
          );
        }}
      </EnvironmentContainer>
      {props.hideChannel ? null : (
        <ChannelContainer>
          {params => {
            const detail = params.selected
              ? params.getDetail(params.selected)
              : null;
            return (
              <div className="kata-logs__selector--container">
                {isEmpty(params.index) || !detail ? (
                  <Fragment>
                    <label className="kata-info__label mr-2">Channel</label>
                    <Dropdown block disabled>
                      <DropdownToggle
                        className={classnames(
                          'kata-dropdown-selector__button',
                          detail
                            ? 'kata-dropdown-selector__button--filled'
                            : null
                        )}
                      >
                        No Channel
                      </DropdownToggle>
                    </Dropdown>
                  </Fragment>
                ) : (
                  <Fragment>
                    <label className="kata-info__label mr-2">Channel</label>
                    <Dropdown block onSelect={params.selectChannel}>
                      <DropdownToggle
                        className={classnames(
                          'kata-dropdown-selector__button',
                          detail
                            ? 'kata-dropdown-selector__button--filled'
                            : null
                        )}
                      >
                        <img
                          src={socialIcons[detail!.type]}
                          width="15"
                          alt={detail!.type}
                          className="mr-2"
                        />
                        {detail!.name}
                      </DropdownToggle>
                      <DropdownMenu className="kata-dropdown-selector__menu">
                        {params.index.map(key => (
                          <DropdownItem key={key} value={params.data[key].id}>
                            <img
                              src={socialIcons[params.data[key].type]}
                              width="15"
                              alt={params.data[key].type}
                              className="mr-2"
                            />
                            {params.data[key].name}
                          </DropdownItem>
                        ))}
                      </DropdownMenu>
                    </Dropdown>
                  </Fragment>
                )}
              </div>
            );
          }}
        </ChannelContainer>
      )}
    </Fragment>
  );
};

export default DeploymentChannel;
