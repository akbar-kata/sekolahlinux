import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import isEmpty from 'lodash-es/isEmpty';

import RootStore from 'interfaces/rootStore';
import { LogError } from 'interfaces/bot';

import { requestGetData } from 'stores/bot/logs/errors/actions';
import { openDrawer, closeDrawer } from 'stores/app/drawer/actions';
import { isDrawerOpen, getDrawerData } from 'stores/app/drawer/selectors';
import { getData } from 'stores/bot/logs/errors/selectors';

import LogErrorTable from './LogErrorTable';
import DetailError from './DetailError';

interface PropsFromState {
  selectedChannel: string | null;
  isOpen: boolean;
  data: LogError[];
  hasNextPage: boolean;
  errorData: any;
}

interface PropsFromDispatch {
  openDrawer: Function;
  closeDrawer: () => void;
  requestGetData();
}

interface ComponentProps {}

const drawerId = 'ErrorDetail';

interface Props extends PropsFromState, PropsFromDispatch, ComponentProps {}

class LogErrorTableContainer extends React.Component<Props> {
  render() {
    return (
      <Fragment>
        <LogErrorTable
          hasNextPage={this.props.hasNextPage}
          onLoadMore={this.props.requestGetData}
          data={this.props.data}
          onOpenDrawer={this.props.openDrawer}
          onCloseDrawer={this.props.closeDrawer}
        />
        {this.props.errorData && (
          <DetailError
            isOpen={this.props.isOpen}
            onCancel={this.props.closeDrawer}
            onCloseDrawer={this.props.closeDrawer}
            errorData={this.props.errorData}
            selectedChannelName={this.props.selectedChannel}
          />
        )}
      </Fragment>
    );
  }

  componentDidMount() {
    this.props.requestGetData();
  }
}

const mapStateToProps = ({
  bot: { log },
  app: { drawer },
  deployment
}: RootStore) => {
  const selectedChannel = deployment.channel.selected;

  return {
    selectedChannel,
    isOpen: isDrawerOpen(drawer, drawerId),
    data: getData(log.errors),
    hasNextPage: !isEmpty(log.errors.paginationToken),
    errorData: getDrawerData(drawer, drawerId)
  };
};

const mapDispatchToProps = (dispatch: Function) => {
  return {
    requestGetData() {
      dispatch(requestGetData());
    },
    openDrawer: (data: any) => dispatch(openDrawer(drawerId, data)),
    closeDrawer: () => dispatch(closeDrawer(drawerId))
  };
};

export default connect<
  PropsFromState,
  PropsFromDispatch,
  ComponentProps,
  RootStore
>(
  mapStateToProps,
  mapDispatchToProps
)(LogErrorTableContainer);
