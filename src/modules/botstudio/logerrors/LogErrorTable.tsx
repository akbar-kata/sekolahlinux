import React from 'react';
import moment from 'moment';

import { LogError } from 'interfaces/bot/log';
import { EmptyMessage } from 'components/Common';
import Button from 'components/Button/Button';

interface Props {
  data: LogError[];
  hasNextPage?: boolean;

  onOpenDrawer: Function;
  onCloseDrawer(): void;

  onLoadMore(): void;
}

export default class LogErrorTable extends React.Component<Props> {
  renderMessage(child: React.ReactNode) {
    return (
      <tr>
        <td colSpan={8} className="text-center">
          {child}
        </td>
      </tr>
    );
  }

  renderData() {
    if (this.props.data.length === 0) {
      return this.renderMessage(
        <EmptyMessage title="Error log is empty">
          No errors, you're doing great!
        </EmptyMessage>
      );
    }

    return this.props.data.map((data, index) => (
      <tr key={index}>
        <td>{moment(data.timestamp).format('DD-MM-YYYY HH:mm:ss')}</td>
        <td>{data.errorCode}</td>
        <td className="kata-logs--force-wrap">{data.errorMessage}</td>
        <td>
          <i
            className="icon-view text-primary"
            onClick={() => this.props.onOpenDrawer(data)}
          />
        </td>
      </tr>
    ));
  }

  render() {
    return (
      <table className="kata-table kata-table--card table table-striped table-hover">
        <thead>
          <tr>
            <th style={{ width: 250 }}>Time</th>
            <th style={{ width: 140 }}>Error Code</th>
            <th>Error Message</th>
            <th />
          </tr>
        </thead>
        <tbody>
          {this.renderData()}
          {this.props.hasNextPage ? (
            <tr>
              <td colSpan={8} className="text-center">
                <Button color="primary" onClick={this.props.onLoadMore}>
                  Load More
                </Button>
              </td>
            </tr>
          ) : null}
        </tbody>
      </table>
    );
  }
}
