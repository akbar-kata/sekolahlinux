import React, { Fragment } from 'react';
import { DropdownSelector, DropdownItem } from 'components/Dropdown';

interface Props {
  groups: string[];
  codes: {
    [groupCode: string]: string[];
  };
  selectedGroup: string;
  selectedCode: string;
  onGroupSelected(key: string): void;
}

export default class ErrorGroup extends React.Component<Props> {
  render() {
    return (
      <Fragment>
        <div className="kata-logs__selector--container">
          <label className="kata-info__label mr-2">Error Group</label>
          <DropdownSelector
            onSelect={this.props.onGroupSelected}
            value={this.props.groups[this.props.selectedGroup]}
          >
            {this.props.groups &&
              Object.keys(this.props.groups).map((errGroup, key) => (
                <DropdownItem value={errGroup} key={errGroup}>
                  {this.props.groups[errGroup]}
                </DropdownItem>
              ))}
          </DropdownSelector>
        </div>
      </Fragment>
    );
  }
}
