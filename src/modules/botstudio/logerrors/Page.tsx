import React, { Fragment } from 'react';

import DeploymentChannelContainer from './DeploymentChannelContainer';
import DateRangePickerContainer from './DateRangePicker.Container';
import LogErrorTableContainer from './LogErrorTable.Container';
import ErrorGroupContainer from './ErrorGroupContainer';
import { Board } from 'components/Board';

import './style.scss';

export default class Page extends React.Component {
  render() {
    return (
      <Fragment>
        <Board className="mb-2">
          <div className="kata-logs__filters">
            <DateRangePickerContainer />
            <DeploymentChannelContainer />
            <ErrorGroupContainer />
          </div>
        </Board>
        <LogErrorTableContainer />
      </Fragment>
    );
  }
}
