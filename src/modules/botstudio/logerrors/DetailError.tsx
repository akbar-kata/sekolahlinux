import React, { Component } from 'react';
import moment from 'moment';

import {
  Drawer,
  DrawerBody,
  DrawerHeader,
  DrawerFooter
} from 'components/Drawer';
import { Button } from 'components/Button';
import { Form } from 'components/Form';

interface Props {
  onCancel: Function;
  isOpen: boolean;
  errorData?: any;
  selectedChannelName: string | null;
  onCloseDrawer(): void;
}

interface States {}

class DetailError extends Component<Props, States> {
  onClose = () => {
    this.props.onCancel();
  };

  render() {
    return (
      <Drawer isOpen={this.props.isOpen} onClose={this.props.onCloseDrawer}>
        <Form>
          <DrawerHeader title="Error Log Details" />
          <DrawerBody>
            <div className="form-row pb-2">
              <div className="col">
                <label className="kata-form__label">Deployment Name</label>
                <input
                  type="text"
                  className="kata-form__input-text"
                  value={this.props.errorData.environmentId}
                  disabled
                />
              </div>
            </div>
            <div className="form-row pb-2">
              {this.props.selectedChannelName && (
                <div className="col">
                  <label className="kata-form__label">Channel Name</label>
                  <input
                    type="text"
                    className="kata-form__input-text"
                    value={this.props.selectedChannelName}
                    disabled
                  />
                </div>
              )}
              <div className="col">
                <label className="kata-form__label">Time</label>
                <input
                  type="text"
                  className="kata-form__input-text"
                  value={moment(this.props.errorData.timestamp).format(
                    'DD-MM-YYYY HH:mm:ss'
                  )}
                  disabled
                />
              </div>
            </div>
            <div className="form-row pb-2">
              <div className="col">
                <label className="kata-form__label">Error Code</label>
                <input
                  type="text"
                  className="kata-form__input-text"
                  value={this.props.errorData.errorCode}
                  disabled
                />
              </div>
            </div>
            <div className="kata-form__element">
              <textarea cols={30} rows={10} className="form-control" disabled>
                {this.props.errorData.errorMessage}
              </textarea>
            </div>
          </DrawerBody>
          <DrawerFooter>
            <Button onClick={this.onClose}>Cancel</Button>
          </DrawerFooter>
        </Form>
      </Drawer>
    );
  }
}

export default DetailError;
