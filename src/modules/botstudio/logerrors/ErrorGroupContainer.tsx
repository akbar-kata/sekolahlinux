import React from 'react';
import { connect } from 'react-redux';

import Store from 'interfaces/rootStore';
import ErrorGroup from './ErrorGroup';
import {
  getCodes,
  getGroups,
  getSelectedCode,
  getSelectedGroup
} from 'stores/bot/logs/errors/selectors';
import {
  setSelectedCode,
  setSelectedGroup
} from 'stores/bot/logs/errors/actions';

interface PropsFromStates {
  groups: string[];
  codes: {
    [groupCode: string]: string[];
  };
  selectedGroup: string;
  selectedCode: string;
}

interface PropsFromDispatches {
  setSelectedGroup: (group: string) => void;
  setSelectedCode: (code: string) => void;
}

interface Props extends PropsFromStates, PropsFromDispatches {}

class ErrorGroupContainer extends React.Component<Props> {
  render() {
    return (
      <ErrorGroup
        groups={this.props.groups}
        codes={this.props.codes}
        selectedGroup={this.props.selectedGroup}
        selectedCode={this.props.selectedCode}
        onGroupSelected={this.handleGroupSelected}
      />
    );
  }

  handleGroupSelected = (key: any) => {
    this.props.setSelectedGroup(key);
  };
  handleCodeSelected = (key: any) => {
    this.props.setSelectedCode(key);
  };
}

const mapStateToProps = ({ bot: { log } }: Store): PropsFromStates => {
  return {
    selectedCode: getSelectedCode(log.errors),
    selectedGroup: getSelectedGroup(log.errors),
    groups: getGroups(log.errors),
    codes: getCodes(log.errors)
  };
};
const mapDispatchToProps = (dispatch: Function): PropsFromDispatches => {
  return {
    setSelectedCode(code: string) {
      dispatch(setSelectedCode(code));
    },
    setSelectedGroup(group: string) {
      dispatch(setSelectedGroup(group));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ErrorGroupContainer);
