import React, { Fragment } from 'react';

import RevisionContainer from 'containers/Revision.Container';

import { DropdownItem, DropdownSelector } from 'components/Dropdown';

const RevisionSelector: React.SFC = () => {
  return (
    <Fragment>
      <RevisionContainer>
        {params => (
          <div className="kata-logs__selector--container">
            <label className="kata-info__label mr-2">Version</label>
            <DropdownSelector
              onSelect={params.setDeployment}
              value={params.selected || undefined}
            >
              {params.index.map(idx => {
                const data = params.data[idx];

                return (
                  <DropdownItem key={data.version} value={data.version}>
                    {data.version}
                  </DropdownItem>
                );
              })}
            </DropdownSelector>
          </div>
        )}
      </RevisionContainer>
    </Fragment>
  );
};

export default RevisionSelector;
