import React, { Fragment } from 'react';
import isEmpty from 'lodash-es/isEmpty';

import { connect } from 'react-redux';
import Page from './Page';
import Store from 'interfaces/rootStore';
import { Banner } from 'components/Banner';
import { Dashboard } from 'components/Dashboard';
import { getProjectSelected } from 'stores/project/selectors';
import { getChannelIndex } from 'stores/deployment/channel/selectors';
import { getEnvironmentIndex } from 'stores/deployment/environment/selectors';
import { fetchEnvRequest } from 'stores/deployment/environment/actions';
import { fetchDeploymentRequest } from 'stores/deployment/actions';
import { clearChannelList } from 'stores/deployment/channel/actions';

interface PropsFromStates {
  project: string | null;
  deployments: any[];
  channels: any[];
}

interface PropsFromDispatches {
  loadDeployments: typeof fetchDeploymentRequest;
  loadEnv: typeof fetchEnvRequest;
  clearChannels: typeof clearChannelList;
}

interface ComponentProps {}

interface Props extends PropsFromStates, PropsFromDispatches, ComponentProps {}

class PageContainer extends React.Component<Props> {
  canDisplayErrors = () => {
    return (
      this.props.project === null &&
      isEmpty(this.props.deployments) &&
      isEmpty(this.props.channels)
    );
  };

  componentDidMount() {
    if (this.props.project) {
      this.props.loadDeployments();
      this.props.loadEnv();
    }
  }

  componentWillUnmount() {
    this.props.clearChannels();
  }

  render() {
    return (
      <Fragment>
        <Dashboard
          title="Error Logs"
          tooltip="Every time an error occured, it will be shown here."
        >
          {!this.canDisplayErrors() ? (
            <Page />
          ) : (
            <Banner
              state="warning"
              message="Please select a bot that contains deployment and already attached to
          certain channel."
            />
          )}
        </Dashboard>
      </Fragment>
    );
  }
}

const mapStateToProps = ({ project, deployment }: Store): PropsFromStates => {
  return {
    project: getProjectSelected(project),
    deployments: getEnvironmentIndex(deployment.environment),
    channels: getChannelIndex(deployment.channel)
  };
};

const mapDispatchToProps = {
  loadDeployments: fetchDeploymentRequest,
  loadEnv: fetchEnvRequest,
  clearChannels: clearChannelList
};

export default connect<
  PropsFromStates,
  PropsFromDispatches,
  ComponentProps,
  Store
>(
  mapStateToProps,
  mapDispatchToProps
)(PageContainer);
