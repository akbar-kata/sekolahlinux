import React, { Fragment } from 'react';
import moment from 'moment';
import { DateRangePicker } from 'react-dates';

interface Props {
  startDate: any;
  endDate: any;
  changeDate: Function;
}

interface State {
  startDate: any;
  endDate: any;
  focusedInput: any;
}

class Picker extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      startDate: moment(props.startDate),
      endDate: moment(props.endDate),
      focusedInput: null
    };
  }

  onDatesChange = ({ startDate, endDate }: any) => {
    this.setState({ startDate, endDate });
    if (startDate !== null && endDate !== null) {
      this.props.changeDate(startDate.valueOf(), endDate.valueOf());
    }
  };

  onFocusChange = (focusedInput: any) => this.setState({ focusedInput });

  isOutsideRange = (data: any) => false;

  render() {
    return (
      <Fragment>
        <div className="kata-logs__selector--date-picker">
          <label className="kata-form__label">Date</label>
          <DateRangePicker
            startDate={this.state.startDate}
            endDate={this.state.endDate}
            onDatesChange={this.onDatesChange}
            focusedInput={this.state.focusedInput}
            onFocusChange={this.onFocusChange}
            displayFormat="DD MMM YYYY"
            isOutsideRange={this.isOutsideRange}
            showDefaultInputIcon
          />
        </div>
      </Fragment>
    );
  }
}

export default Picker;
