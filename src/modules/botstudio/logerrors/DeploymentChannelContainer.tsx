import React from 'react';
import { connect } from 'react-redux';

import RootStore from 'interfaces/rootStore';

import { getProjectSelected } from 'stores/project/selectors';
import { selectEnv } from 'stores/deployment/environment/actions';
import { getDeploymentSelected } from 'stores/deployment/selectors';
import { getEnvironmentSelected } from 'stores/deployment/environment/selectors';

import DeploymentChannel from './DeploymentChannel';

interface PropsFromState {
  selectedProject: string | null;
  deployment: string | null;
  hasChannel: boolean;
  selectedEnv: string | null;
}

interface PropsFromDispatch {
  setEnv: (env: string) => ReturnType<typeof selectEnv>;
}

export interface DeploymentChannelContainerProps
  extends PropsFromDispatch,
    PropsFromState {
  hideChannel?: boolean;
}

class DeploymentChannelContainer extends React.Component<
  DeploymentChannelContainerProps
> {
  render() {
    return <DeploymentChannel {...this.props} />;
  }
}

const mapStateToProps = ({
  project,
  deployment
}: RootStore): PropsFromState => {
  return {
    selectedProject: getProjectSelected(project),
    deployment: getDeploymentSelected(deployment),
    selectedEnv: getEnvironmentSelected(deployment.environment),
    hasChannel: deployment.channel.index.length > 0
  };
};

const mapDispatchToProps = (dispatch: Function): PropsFromDispatch => {
  return {
    setEnv: (env: string) => dispatch(selectEnv(env))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DeploymentChannelContainer);
