import React from 'react';
import { connect } from 'react-redux';

import RootStore from 'interfaces/rootStore';
import DateRangePicker from './DateRangePicker';
import { getEndDate, getStartDate } from 'stores/bot/logs/errors/selectors';
import { setEndDate, setStartDate } from 'stores/bot/logs/errors/actions';

interface PropsFromState {
  startDate: number;
  endDate: number;
}

interface PropsFromDispatch {
  changeDate: Function;
}

const DateRangePickerContainer = (props: any) => {
  return <DateRangePicker {...props} />;
};

const mapStateToProps = ({ bot: { log } }: RootStore): PropsFromState => {
  const startDate = getStartDate(log.errors);
  const endDate = getEndDate(log.errors);
  const startDateNumeric = new Date(startDate).getTime();
  const endDateNumeric = new Date(endDate).getTime();
  return {
    startDate: startDateNumeric,
    endDate: endDateNumeric
  };
};

const mapDispatchToProps = (dispatch: Function): PropsFromDispatch => {
  return {
    changeDate: (start: number, end: number) => {
      const startDate = new Date(start).toISOString();
      const endDate = new Date(end).toISOString();
      dispatch(setStartDate(startDate));
      dispatch(setEndDate(endDate));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DateRangePickerContainer);
