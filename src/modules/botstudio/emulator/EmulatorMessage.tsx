import React from 'react';
import classnames from 'classnames';

import {
  RichMessageAction,
  Message,
  MessageType,
  MessageOwner
} from 'interfaces/bot/emulator';

import RichMessageActionButton from './RichMessageActionButton';

interface Props {
  message: Message;
  onAction?: any;
}
interface States {
  showJson: boolean;
}

const EmulatorMessageRenderers = {
  // tslint:disable-next-line:function-name
  [MessageType.text](
    message: Message,
    className: string,
    context: EmulatorMessage
  ): JSX.Element {
    return (
      <li className={className}>
        <div className="kata-emulator__message-content">{message.message}</div>
        <div className="kata-emulator__message-meta">
          <span className="timestamp">{message.date.toISOString()}</span>
          <a className="action" onClick={context.toggleShowJson}>
            <span className="icon-view" />
          </a>
          {context.state.showJson ? (
            <pre className="json">
              {JSON.stringify(message.metadata, null, 2)}
            </pre>
          ) : null}
        </div>
      </li>
    );
  },
  // tslint:disable-next-line:function-name
  [MessageType.data](
    message: Message,
    className: string,
    context: EmulatorMessage
  ): JSX.Element {
    return (
      <li className={classnames(className, 'rich')}>
        <div className="kata-emulator__message-content">
          {message.message.items.description}
          <div className="kata-emulator__message-image">
            <a href={message.message.items.url}>
              <img
                src={message.message.items.url}
                alt={message.message.items.description}
              />
            </a>
          </div>
          <ul className="kata-emulator__actions">
            {context.renderActions(message.message.items.actions)}
          </ul>
        </div>
        <div className="kata-emulator__message-meta">
          <span className="timestamp">{message.date.toISOString()}</span>
          <a className="action" onClick={context.toggleShowJson}>
            <span className="icon-view" />
          </a>
          {context.state.showJson ? (
            <pre className="json">
              {JSON.stringify(message.metadata, null, 2)}
            </pre>
          ) : null}
        </div>
      </li>
    );
  },
  // tslint:disable-next-line:function-name
  [MessageType.unsupported](
    message: Message,
    className: string,
    context: EmulatorMessage
  ): JSX.Element {
    return (
      <li className={classnames(className, 'rich')}>
        <div className="kata-emulator__message-content">
          This action type is not supported on our emulator. Please check on
          messaging app.
        </div>
        <div className="kata-emulator__message-meta">
          <span className="timestamp">{message.date.toISOString()}</span>
          <a className="action" onClick={context.toggleShowJson}>
            <span className="icon-view" />
          </a>
          {context.state.showJson ? (
            <pre className="json">
              {JSON.stringify(message.metadata, null, 2)}
            </pre>
          ) : null}
        </div>
      </li>
    );
  }
};

export default class EmulatorMessage extends React.Component<Props, States> {
  state = {
    showJson: false
  };

  toggleShowJson = () => {
    this.setState({
      showJson: !this.state.showJson
    });
  };

  render() {
    const message = this.props.message;
    let className = 'kata-emulator__message';
    if (message.from === MessageOwner.bot) {
      className += ' kata-emulator__message--left';
    } else {
      className += ' kata-emulator__message--right';
    }

    if (
      message &&
      message.message &&
      EmulatorMessageRenderers.hasOwnProperty(message.type)
    ) {
      return EmulatorMessageRenderers[message.type](message, className, this);
    }

    return EmulatorMessageRenderers[MessageType.unsupported](
      message,
      className,
      this
    );
  }

  renderActions(items: RichMessageAction[]) {
    return items.map((item, index) => {
      return (
        <li key={index}>
          <RichMessageActionButton
            onAction={this.props.onAction}
            action={item}
          />
        </li>
      );
    });
  }
}
