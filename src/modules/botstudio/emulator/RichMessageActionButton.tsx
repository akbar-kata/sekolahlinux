import React from 'react';

import { RichMessageAction } from 'interfaces/bot/emulator';

interface Props {
  action: RichMessageAction;
  onAction: any;
}

export default class RichMessageActionButton extends React.Component<Props> {
  onAction = () => {
    this.props.onAction(this.props.action);
  };

  render() {
    return (
      <button className="btn btn-default btn-block" onClick={this.onAction}>
        {this.props.action.text}
      </button>
    );
  }
}
