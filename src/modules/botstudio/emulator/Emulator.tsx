import React from 'react';

import { Message, Bags, RichMessageAction } from 'interfaces/bot/emulator';

import { Banner } from 'components/Banner';
import { Button, FloatingButton } from 'components/Button';

import EmulatorMessage from './EmulatorMessage';

interface Props {
  activeDeployment?: string | null;
  activeBot?: string | null;
  activeSession?: string | null;
  messages?: Message[];
  bags?: Bags;
  deployments?: any;
  selectedDeployment?: any;
  onHide: Function;
  sendMessage?: any;
  setSession?: any;
  clearBags?: any;
  clearMessages?: any;
  reset: Function;
  selectDeployment(name: string): void;
}

interface States {
  message: string;
}

class Emulator extends React.Component<Props, States> {
  private elMessageBox: HTMLDivElement | null;

  constructor(props: Props) {
    super(props);

    this.state = {
      message: ''
    };
  }

  componentDidUpdate() {
    if (this.elMessageBox) {
      this.elMessageBox.scrollTop = this.elMessageBox.scrollHeight;
    }
  }

  render() {
    return (
      <div className="kata-emulator__widget">
        <div className="kata-emulator__panel">
          <div className="kata-emulator__panel-heading">
            <div className="kata-emulator__panel-title d-block">
              <h2 className="heading2 kata-emulator__panel-text mb-0 d-inline-block">
                Test Chatbot
              </h2>
              <Button
                isIcon
                onClick={this.props.onHide}
                className="kata-emulator__btn-close"
              >
                <i className="icon-close" />
              </Button>
            </div>
          </div>
          <div className="kata-emulator__panel-body">
            <div
              className="kata-emulator__message-container"
              ref={el => {
                this.elMessageBox = el;
              }}
            >
              {this.renderMessages()}
            </div>
            {this.containsErrors() && (
              <div className="kata-emulator__banner">
                <Banner
                  state="error"
                  onClose={this.onReinitiateSession}
                  message={
                    <>
                      <span>
                        Your bot has an error. Please check your error log
                      </span>
                      <span
                        className="kata-emulator__reinit-text"
                        onClick={this.onReinitiateSession}
                      >
                        Reinit Session
                      </span>
                    </>
                  }
                />
              </div>
            )}
          </div>
          <div className="kata-emulator__panel-footer">
            <form className="d-flex" onSubmit={this.sendMessage}>
              <input
                value={this.state.message}
                type="text"
                className="kata-form__input-text"
                placeholder="Type your message"
                onInput={this.onMessageInput}
              />
              <FloatingButton
                icon="refresh"
                className="ml-2"
                onClick={this.onReinitiateSession}
              />
            </form>
          </div>
        </div>
      </div>
    );
  }

  containsErrors(): any {
    return (
      this.props.bags &&
      this.props.bags.errors &&
      this.props.bags.errors.message
    );
  }

  renderMessages() {
    if (this.props.messages) {
      return this.props.messages.map((message: Message, index) => {
        return (
          <EmulatorMessage
            onAction={this.onAction}
            key={index}
            message={message}
          />
        );
      });
    }
    return null;
  }

  onAction = (item: RichMessageAction) => {
    this.props.sendMessage(
      this.props.activeBot,
      this.props.activeSession,
      item.text
    );
    this.setState({
      message: ''
    });
  };

  onReinitiateSession = event => {
    this.props.clearBags();
    this.props.clearMessages();
    this.props.reset();
  };

  onMessageInput = event => {
    this.setState({
      message: event.target.value
    });
  };

  sendMessage = event => {
    event.preventDefault();
    this.props.sendMessage(
      this.props.activeBot,
      this.props.activeSession,
      this.state.message
    );
    this.setState({
      message: ''
    });
  };
}

export default Emulator;
