import React from 'react';
import { connect } from 'react-redux';
import { withRouter, RouteComponentProps } from 'react-router';

import RootStore from 'interfaces/rootStore';
import * as Nlu2 from 'interfaces/nlu/nlu';
import { Message, Bags } from 'interfaces/bot/emulator';

import { fetchTestingRequest, clearTesting } from 'stores/nlu/testing/actions';
import { selectDeployment } from 'stores/deployment/actions';

import { Button } from 'components/Button';

import {
  getDeploymentData,
  getDeploymentSelected
} from 'stores/deployment/selectors';
import {
  clearMessages,
  setSession,
  sendMessage,
  clearBags,
  reset
} from 'stores/bot/emulator/actions';
import { getCurrentNlu } from 'stores/nlu/nlu/selectors';
import { getProjectSelected } from 'stores/project/selectors';

import { isRootURL } from 'utils/url';

import Emulator from './Emulator';

interface PropsFromState {
  nluName?: string;
  selectedProject: string | null;
  nluData: Nlu2.Nlu | null;
  data: any;
  loading: boolean;
  error: string;
  activeDeployment?: string | null;
  activeBot?: string | null;
  activeSession?: string | null;
  messages?: Message[];
  bags?: Bags;
  deployments?: any;
  selectedDeployment?: any;
}

interface PropsFromDispatch {
  fetchAction: typeof fetchTestingRequest;
  clearAction: () => ReturnType<typeof clearTesting>;
  sendMessage: typeof sendMessage;
  setSession: (sessionId: string) => ReturnType<typeof setSession>;
  clearBags: typeof clearBags;
  clearMessages: typeof clearMessages;
  reset: typeof reset;
  selectDeployment: typeof selectDeployment;
}

interface Props
  extends PropsFromState,
    PropsFromDispatch,
    RouteComponentProps<any> {}

interface State {
  // isOpen: boolean;
}

class TestingContainer extends React.Component<Props, State> {
  state = { showDialog: false };

  showDialog = () => {
    this.setState({ showDialog: true });
  };
  onHide = () => {
    this.setState({ showDialog: false });
    this.props.clearAction();
  };
  onFetch = sentence => {
    if (this.props.selectedProject && this.props.nluData) {
      this.props.fetchAction(
        this.props.selectedProject,
        this.props.nluData.name,
        sentence
      );
    }
  };

  render() {
    if (isRootURL(this.props.location.pathname)) {
      return null;
    }

    return (
      <>
        <div
          className={`${
            this.state.showDialog ? 'kata-emulator__panel-overlay' : ''
          }`}
          onClick={this.onHide}
        />
        <div className="row">
          {this.state.showDialog && (
            <Emulator onHide={this.onHide} {...this.props} />
          )}
          {!this.state.showDialog && (
            <Button
              onClick={this.showDialog}
              className="kata-emulator__btn-testing"
            >
              <i className="icon-test-chatbot kata-emulator__icon-testing" />{' '}
              Test Chatbot
            </Button>
          )}
        </div>
      </>
    );
  }
}

const mapStateToProps = ({
  project,
  deployment,
  nlu: { entity, nlu, testing },
  bot: { selected: activeBot, emulator }
}: RootStore): PropsFromState => {
  return {
    activeBot,
    nluName: entity.selected ? entity.selected : '',
    selectedProject: getProjectSelected(project),
    nluData: getCurrentNlu(nlu),
    data: testing.data,
    loading: testing.loadings.fetch,
    error: testing.errors.fetch ? testing.errors.fetch : '',
    activeDeployment: deployment.selected,
    activeSession: emulator.activeSession,
    messages: emulator.messages,
    bags: emulator.bags,
    deployments: getDeploymentData(deployment),
    selectedDeployment: getDeploymentSelected(deployment)
  };
};

const mapDispatchToProps = {
  fetchAction: (projectId: string, nluName: string, sentence: string) =>
    fetchTestingRequest(projectId, nluName, sentence),
  clearAction: () => clearTesting('clear'),
  selectDeployment: (name: string) => selectDeployment(name),
  sendMessage: (botId: string, sessionId: string, message: string) =>
    sendMessage(botId, sessionId, message),
  setSession: (sessionId: string) => setSession(sessionId),
  reset: () => reset(),
  clearBags: () => clearBags(),
  clearMessages: () => clearMessages()
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(TestingContainer)
);
