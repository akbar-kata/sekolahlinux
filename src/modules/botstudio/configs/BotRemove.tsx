import React from 'react';
import { connect } from 'react-redux';

import { removeBotRequest } from 'stores/bot/actions';
import { getBotSelected } from 'stores/bot/selectors';
import { Button } from 'components/Button';
import { confirm } from 'components/Modal';

interface PropsFromState {
  selectedBot: string | null;
}

interface PropsFromDispatch {
  onRemove: Function;
}

interface Props extends PropsFromState, PropsFromDispatch {}

interface State {}

class BotRemove extends React.Component<Props, State> {
  public onRemove = () => {
    if (!this.props.selectedBot) {
      return;
    }
    confirm({
      title: 'Delete Bot',
      message: 'Are you sure you want to delete this Bot?',
      okLabel: 'Delete',
      cancelLabel: 'Cancel'
    }).then(res => {
      if (res) {
        // this.props.onRemove(this.props.selectedProject, this.props.selectedBotName);
      }
    });
  };

  render() {
    return (
      <Button color="danger" onClick={this.onRemove}>
        Delete
      </Button>
    );
  }
}

const mapStateToProps = ({ bot }: any): PropsFromState => {
  const selected = getBotSelected(bot);
  return {
    selectedBot: selected
  };
};

const mapDispatchToProps = (dispatch: Function): PropsFromDispatch => {
  return {
    onRemove: (id: string, name: string) => dispatch(removeBotRequest(id, name))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BotRemove);
