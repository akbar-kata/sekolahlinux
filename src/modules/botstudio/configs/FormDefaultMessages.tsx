import React from 'react';

import { DataMap } from 'interfaces/common';
import { Form, Text } from 'components/Form';

interface Props {
  data: DataMap<any>;
  onAssign: Function;
}

interface States {}

class FormDefaultMessages extends React.Component<Props, States> {
  onValidate = values => {
    return {};
  };

  renderInnerForm = ({ submitForm }) => (
    <form>
      <div>
        <label>RPM Limit Reponse</label>
        <Text
          field="defaultMessages.rpmLimitResponse"
          className="form-control"
          placeholder="-"
        />
      </div>
      <div className="mt4">
        <label>Burst Message Reponse</label>
        <Text
          field="defaultMessages.burstMessageResponse"
          className="form-control"
          placeholder="-"
        />
      </div>
      <div className="mt4">
        <label>Channel Disabled Response</label>
        <Text
          field="defaultMessages.channelDisabledResponse"
          className="form-control"
          placeholder="-"
        />
      </div>
      <div className="mt4">
        <label>Maintanance Reponse</label>
        <Text
          field="defaultMessages.maintenanceResponse"
          className="form-control"
          placeholder="-"
        />
      </div>
      <div className="mt4">
        <button
          className="btn btn-primary br2"
          type="button"
          onClick={submitForm}
        >
          Save Default Messages
        </button>
        <button className="btn btn-default br2" type="button">
          Cancel
        </button>
      </div>
    </form>
  );

  render() {
    return (
      <Form
        onSubmit={this.props.onAssign}
        defaultValues={this.props.data}
        validate={this.onValidate}
      >
        {this.renderInnerForm}
      </Form>
    );
  }
}

export default FormDefaultMessages;
