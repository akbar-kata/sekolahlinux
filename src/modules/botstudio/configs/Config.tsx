import React, { SFC, Fragment } from 'react';

import { DataMap } from 'interfaces/common';
import FormOptions from './FormOptions';
import Dashboard from 'components/Dashboard/Dashboard';
import { Board } from 'components/Board';

interface Props {
  data: DataMap<any>;
  isBotExist: Function;
  onSave: Function;
  showNotif(title: string, message: string): void;
}

const Config: SFC<Props> = (props: Props) => (
  <Fragment>
    <Dashboard
      className="kata-configs__wrapper"
      title="Options"
      tooltip="Advanced configurations for your bot."
      isSettings
    >
      <Board>
        <div className="kata-configs__item">
          <FormOptions {...props} />
        </div>
      </Board>
    </Dashboard>
  </Fragment>
);

export default Config;
