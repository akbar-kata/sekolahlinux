import React from 'react';
import { connect } from 'react-redux';

import Store from 'interfaces/rootStore';
import { DataMap } from 'interfaces/common';
import { save } from 'stores/bot/config/actions';
import { getData } from 'stores/bot/config/selectors';
import { isExist } from 'stores/bot/selectors';
import { addNotification } from 'stores/app/notification/actions';

import Config from './Config';

interface PropsFromState {
  data: DataMap<any>;
  isExist: Function;
}

interface PropsFromDispatch {
  onSave: typeof save;
  showSuccessNotification: (
    title: string,
    message: string
  ) => ReturnType<typeof addNotification>;
}

interface Props extends PropsFromDispatch, PropsFromState {}

interface States {}

class ConfigContainer extends React.Component<Props, States> {
  render() {
    return (
      <Config
        {...this.props}
        isBotExist={this.props.isExist}
        showNotif={this.props.showSuccessNotification}
      />
    );
  }
}

const mapStateToProps = ({ bot }: Store): PropsFromState => {
  return {
    data: getData(bot),
    isExist: (name: string, id?: string) => isExist(bot, name, id)
  };
};

const mapDispatchToProps = {
  onSave: save,
  showSuccessNotification: (title: string, message: string) =>
    addNotification({
      title,
      message,
      status: 'success',
      dismissible: true,
      dismissAfter: 5000
    })
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ConfigContainer);
