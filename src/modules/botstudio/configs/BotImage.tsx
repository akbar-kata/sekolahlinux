import React from 'react';

interface Props {
  imgUrl?: string;
}

const BotImage: React.SFC<Props> = props => {
  return (
    <div className="bot-img-container">
      <div className="bot-img bg-gray">
        {(props.imgUrl && <img src={props.imgUrl} alt="Bot Image" />) || (
          <i className="icon-picture bot-img-icon text-muted" />
        )}
        <div className="bot-img-changer">
          <i className="icon-camera bot-img-icon" />
        </div>
      </div>
    </div>
  );
};

export default BotImage;
