import React from 'react';

import { DataMap } from 'interfaces/common';
import timezones from 'components/utils/timezones';
import { Form, Text, ReactSelect } from 'components/Form';
import { Button } from 'components/Button';

interface Props {
  data: DataMap<any>;
  isBotExist: Function;
  onAssign: Function;
  showNotif(title: string, message: string): void;
}

interface States {}

const inputId = 'form-config-name';

class FormProfile extends React.Component<Props, States> {
  componentDidMount() {
    const el = document.getElementById(inputId);
    if (el && el.focus) {
      el.focus();
    }
  }

  onUpdate = (data: any) => {
    this.props.onAssign(data);
  };

  onValidate = values => {
    const { id, name, desc, timezone } = values;
    return {
      name: !name
        ? 'A name is required'
        : this.props.isBotExist(name, id)
        ? 'Name already used, pick another name'
        : undefined,
      desc: !desc ? 'A description i required' : undefined,
      timezone: !timezone ? 'Timezone must be selected' : undefined
    };
  };

  renderInnerForm = ({ submitForm }) => (
    <form>
      <div className="kata-form__element">
        <div className="form-row pb-2">
          <div className="col pr-2">
            <label className="kata-form__label">Name</label>
            <Text
              field="name"
              placeholder="Name"
              className="kata-form__input-text"
            />
          </div>
          <div className="col pl-1">
            <label className="kata-form__label">Project Id</label>
            <Text
              field="id"
              placeholder="Bot Id"
              className="kata-form__input-text"
              disabled
            />
          </div>
        </div>
        <div className="form-row pb-1">
          <div className="col pr-2">
            <label className="kata-form__label">Description</label>
            <Text
              field="desc"
              placeholder="Description"
              className="kata-form__input-text"
            />
          </div>
          <div className="col pl-1">
            <label className="kata-form__label">Timezone</label>
            <ReactSelect
              className="kata-form__input-select"
              field="timezone"
              options={timezones}
              simpleValue
            />
          </div>
        </div>
      </div>
      <div className="kata-form__element pb-1">
        <div className="float-right">
          <Button color="primary" className="mr-1" onClick={submitForm}>
            Update
          </Button>
        </div>
      </div>
    </form>
  );

  render() {
    return (
      <Form
        onSubmit={this.onUpdate}
        defaultValues={this.props.data}
        validate={this.onValidate}
      >
        {this.renderInnerForm}
      </Form>
    );
  }
}

export default FormProfile;
