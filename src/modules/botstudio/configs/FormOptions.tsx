import React from 'react';
import { Formik, Form } from 'formik';

import { DataMap } from 'interfaces/common';

import { ReactAce } from 'components/FormikWrapper';
import { Button } from 'components/Button';

interface Props {
  data: DataMap<any>;
  onSave: Function;
  showNotif(title: string, message: string): void;
}

interface States {}

class FormOptions extends React.Component<Props, States> {
  onUpdate = (data: any) => {
    this.props.onSave({
      options: data.options
    });
    this.props.showNotif(
      'Bot Config Updated',
      'You have successfully updated bot configuration.'
    );
  };
  renderInnerForm = () => (
    <Form>
      <div className="kata-form__element pb-1">
        <ReactAce mode="yaml" fontSize={13} name="options" />
      </div>
      <div className="kata-form__element pb-3">
        <div className="float-right">
          <Button type="submit" color="primary">
            Update
          </Button>
        </div>
      </div>
    </Form>
  );

  render() {
    return (
      <Formik onSubmit={this.onUpdate} initialValues={this.props.data}>
        {this.renderInnerForm}
      </Formik>
    );
  }
}

export default FormOptions;
