import React from 'react';

import { Module, ModuleSetting } from 'interfaces/module';
import { Modal, ModalBody, ModalFooter, ModalHeader } from 'components/Modal';
import { Button } from 'components/Button';
import { Field, Form, Formik } from 'formik';

interface Props {
  module: Module;
  isOpen: boolean;
  onClose: () => void;
  onSaveSettings: (module: Module) => void;
  isEnabling: boolean;
}

export default class ModuleSettingModal extends React.Component<Props> {
  constructor(props: Props) {
    super(props);
    this.handleSave = this.handleSave.bind(this);
  }

  handleSave() {
    this.props.onSaveSettings(this.props.module);
  }

  render() {
    return (
      <Modal show={this.props.isOpen} onClose={this.props.onClose}>
        <ModalHeader>Module Settings</ModalHeader>
        <ModalBody>
          <Formik initialValues={{}} onSubmit={data => console.dir(data)}>
            {this.renderFields}
          </Formik>
        </ModalBody>
        <ModalFooter>
          <Button
            color="primary"
            onClick={this.handleSave}
            loading={this.props.isEnabling}
          >
            Enable
          </Button>
          <Button onClick={this.props.onClose}>Cancel</Button>
        </ModalFooter>
      </Modal>
    );
  }

  getSettingsFields() {
    const settings = this.props.module.settings;
    if (!settings) {
      return null;
    }
    const fields: ModuleSetting[] = [];

    for (const prop in settings) {
      if (settings.hasOwnProperty(prop)) {
        const setting = settings[prop];
        fields.push(setting);
      }
    }
    return fields;
  }

  renderFields = () => {
    const fields = this.getSettingsFields();
    if (!fields) {
      return null;
    }
    return (
      <Form>
        {fields.map(field => (
          <div className="kata-form__element">
            <label
              htmlFor={field.name}
              className="control-label kata-form__label"
            >
              {field.label}
            </label>
            <Field
              className="kata-form__input-text"
              key={field.id}
              name={field.name}
              type={field.type}
              placeholder={field.label}
            />
          </div>
        ))}
      </Form>
    );
  };
}
