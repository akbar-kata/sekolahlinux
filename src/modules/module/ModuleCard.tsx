import React from 'react';

import Card from 'components/Card';
import {
  Dropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle
} from 'components/Dropdown';
import { Button } from 'components/Button';
import { Module } from 'interfaces/module';
import UpdateModuleModal from './UpdateModuleModal';

interface Props {
  module: Module;
  onDisable: (module: Module) => any;
  onUpdate: (module: Module) => any;
  onEnable: (module: Module) => any;
  onClick: (module: Module) => any;
  isUpdating: boolean;
}

interface States {
  isUpdateInfoOpen: boolean;
}

export default class ModuleCard extends React.Component<Props, States> {
  state: States = {
    isUpdateInfoOpen: false
  };

  constructor(props: Props) {
    super(props);
    this.handleDisable = this.handleDisable.bind(this);
    this.handleUpdate = this.handleUpdate.bind(this);
    this.handleOpenUpdateInfo = this.handleOpenUpdateInfo.bind(this);
    this.handleCloseUpdateInfo = this.handleCloseUpdateInfo.bind(this);
    this.handleEnable = this.handleEnable.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  handleDisable() {
    this.props.onDisable(this.props.module);
  }

  handleUpdate() {
    this.props.onUpdate(this.props.module);
  }

  handleOpenUpdateInfo() {
    this.setState({
      isUpdateInfoOpen: true
    });
  }

  handleCloseUpdateInfo() {
    this.setState({
      isUpdateInfoOpen: false
    });
  }

  handleEnable() {
    this.props.onEnable(this.props.module);
  }

  handleClick() {
    this.props.onClick(this.props.module);
  }

  render() {
    return (
      <React.Fragment>
        <Card
          onClick={this.handleClick}
          avatar={require('assets/images/avatars/avatar-module-1.svg')}
          flex
          title={this.props.module.name}
          action={
            this.props.module.hasUpdate && this.props.module.enabled ? (
              <Dropdown>
                <DropdownToggle caret={false}>
                  <Button isIcon>
                    <i className="icon-more" />
                  </Button>
                </DropdownToggle>
                <DropdownMenu right>
                  <DropdownItem onClick={this.handleOpenUpdateInfo}>
                    Update Info
                  </DropdownItem>
                  <DropdownItem onClick={this.handleDisable}>
                    Disable
                  </DropdownItem>
                </DropdownMenu>
              </Dropdown>
            ) : null
          }
        >
          <div className="kata-card--light-text elliptrim--wk">
            {this.props.module.description}
          </div>

          <table className="kata-light-table mt-2 mb-2">
            <thead>
              <tr>
                <th>Latest Version</th>
                <th>Author</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>{this.props.module.version}</td>
                <td>{this.props.module.author}</td>
              </tr>
            </tbody>
          </table>

          {this.renderButtons()}
        </Card>
        <UpdateModuleModal
          module={this.props.module}
          isOpen={this.state.isUpdateInfoOpen && this.props.module.hasUpdate}
          onClose={this.handleCloseUpdateInfo}
          onUpdate={this.props.onUpdate}
          isUpdating={this.props.isUpdating}
        />
      </React.Fragment>
    );
  }

  renderButtons() {
    if (!this.props.module.enabled) {
      return (
        <Button
          color="primary"
          className="mt-auto mb-2"
          block
          onClick={this.handleEnable}
        >
          Enable
        </Button>
      );
    }

    if (this.props.module.hasUpdate) {
      return (
        <Button
          color="success"
          className="mt-auto mb-2"
          block
          loading={this.props.isUpdating}
          onClick={this.handleUpdate}
        >
          Update
        </Button>
      );
    }

    return (
      <Button
        color="danger"
        className="mt-auto mb-2"
        block
        onClick={this.handleDisable}
      >
        Disable
      </Button>
    );
  }
}
