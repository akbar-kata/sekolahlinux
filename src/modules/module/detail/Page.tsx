import React from 'react';
import { Route, Switch } from 'react-router';

import ModuleManager, {
  ComponentProps as ManagerProps
} from 'modules/module/ModuleManager';

import './Page.scss';

import ModuleOverview from 'modules/module/detail/ModuleOverview';
import ModuleSettings from 'modules/module/detail/ModuleSettings';
import SelectedUrl from 'containers/SelectedUrl';

interface Props {
  manager: ManagerProps;
}

export default class Page extends React.Component<Props> {
  renderOverview() {
    return <ModuleManager actor={api => <ModuleOverview manager={api} />} />;
  }

  renderSettings() {
    return <ModuleManager actor={api => <ModuleSettings manager={api} />} />;
  }

  render() {
    return (
      <SelectedUrl>
        {projectId => {
          const parentPath = `/project/${projectId}`;
          return (
            <Switch>
              <Route
                path={`${parentPath}/module/:id/overview`}
                render={this.renderOverview}
              />
              <Route
                path={`${parentPath}/module/:id/settings`}
                component={this.renderSettings}
              />
            </Switch>
          );
        }}
      </SelectedUrl>
    );
  }
}
