import React, { Fragment } from 'react';
import { Link, NavLink } from 'react-router-dom';

import { ComponentProps as ManagerApi } from 'modules/module/ModuleManager';
import { Dashboard } from 'components/Dashboard';
import { Board } from 'components/Board';
import { Button } from 'components/Button';
import confirm from 'components/Modal/ConfirmDialog';

import { Robot } from 'components/Loading';
import { ModuleSetting } from 'interfaces/module';
import { Field, Form, Formik } from 'formik';
import EmptyMessage from 'components/Common/EmptyMessage';

interface Props {
  manager: ManagerApi;
}

interface States {
  showSettings: boolean;
}

export default class ModuleSettings extends React.Component<Props, States> {
  state = {
    showSettings: false
  };

  handleEnable = () => {
    this.props.manager.enableModule(this.props.manager.selected);
  };
  handleUpdate = () => {
    this.props.manager.updateModule(this.props.manager.selected);
  };
  handleDisable = () => {
    confirm({
      title: 'Disable Module',
      message: `Are you sure want to disable ${
        this.props.manager.selected.name
      } module?`,
      okLabel: 'Disable'
    }).then(result => {
      if (result) {
        this.props.manager.disableModule(this.props.manager.selected);
      }
    });
  };
  showSettingsModal = () => {
    this.setState({
      showSettings: true
    });
  };
  hideSettingsModal = () => {
    this.setState({
      showSettings: false
    });
  };

  renderButtons() {
    const module = this.props.manager.selected;
    if (module) {
      if (!module.enabled) {
        return (
          <Button color="primary" onClick={this.showSettingsModal}>
            Enable
          </Button>
        );
      }
      if (module.hasUpdate) {
        return (
          <React.Fragment>
            <Button color="success" onClick={this.handleUpdate}>
              Update
            </Button>
            <Button
              className="ml-2"
              color="danger"
              onClick={this.handleDisable}
            >
              Disable
            </Button>
          </React.Fragment>
        );
      }
    }
    return null;
  }

  render() {
    const module = this.props.manager.selected;
    if (!module) {
      return <Robot />;
    }
    if (!module.enabled) {
      return (
        <EmptyMessage title="Please enable the module first before configuring it" />
      );
    }
    return (
      <Dashboard
        headerContent={
          <Fragment>
            <Link
              className="btn kata-btn-float kata-subpage__back-btn kata-btn-float__primary"
              to={`/module/all`}
            >
              <i className="icon-arrow-left" />
            </Link>
            <div className="float-left flex-fill">
              <h1 className="heading1 kata-subpage__title">{`${
                module.name
              } Module`}</h1>
              <Link to={`/module/all`} className="text-muted">
                Module
              </Link>
              <span className="kata-subpage__bread-separator">/</span>
              <span className="text-primary">{`${module.name} Module`}</span>
            </div>
            <div>{this.renderButtons()}</div>
          </Fragment>
        }
      >
        <Board
          bodyClassName="kata-module-board-body"
          footerChildren={
            <div className="text-right">
              <Button color="primary">Update</Button>
            </div>
          }
          headingChildren={
            <div className="text-center">
              <div className="btn-group kata-btn-group">
                <NavLink
                  to={`/module/${module.id}/overview`}
                  exact
                  className="btn kata-btn-group__item"
                  activeClassName="kata-btn-group__item--selected"
                >
                  Overview
                </NavLink>
                <NavLink
                  to={`/module/${module.id}/settings`}
                  exact
                  className="btn kata-btn-group__item"
                  activeClassName="kata-btn-group__item--selected"
                >
                  Settings
                </NavLink>
              </div>
            </div>
          }
        >
          <Formik initialValues={{}} onSubmit={data => console.dir(data)}>
            {this.renderFields()}
          </Formik>
        </Board>
      </Dashboard>
    );
  }

  getSettingsFields() {
    const settings = this.props.manager.selected.settings;
    if (!settings) {
      return null;
    }
    const fields: ModuleSetting[] = [];

    for (const prop in settings) {
      if (settings.hasOwnProperty(prop)) {
        const setting = settings[prop];
        fields.push(setting);
      }
    }
    return fields;
  }

  renderFields = () => {
    const fields = this.getSettingsFields();
    if (!fields) {
      return null;
    }
    return (
      <Form>
        {fields.map(field => (
          <div className="kata-form__element">
            <label
              htmlFor={field.name}
              className="control-label kata-form__label"
            >
              {field.label}
            </label>
            <Field
              className="kata-form__input-text"
              key={field.id}
              name={field.name}
              type={field.type}
              placeholder={field.label}
            />
          </div>
        ))}
      </Form>
    );
  };
}
