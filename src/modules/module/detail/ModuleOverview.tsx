import React, { Fragment } from 'react';
import { Link, NavLink } from 'react-router-dom';

import { ComponentProps as ManagerApi } from 'modules/module/ModuleManager';
import { Dashboard } from 'components/Dashboard';
import { Board } from 'components/Board';
import { Button } from 'components/Button';
import confirm from 'components/Modal/ConfirmDialog';

import { Robot } from 'components/Loading';
import ModuleSettingModal from 'modules/module/ModuleSettingModal';

interface Props {
  manager: ManagerApi;
}

interface States {
  showSettings: boolean;
}

export default class ModuleOverview extends React.Component<Props, States> {
  state = {
    showSettings: false
  };

  handleEnable = () => {
    this.props.manager.enableModule(this.props.manager.selected);
  };
  handleUpdate = () => {
    this.props.manager.updateModule(this.props.manager.selected);
  };
  handleDisable = () => {
    confirm({
      title: 'Disable Module',
      message: `Are you sure want to disable ${
        this.props.manager.selected.name
      } module?`,
      okLabel: 'Disable'
    }).then(result => {
      if (result) {
        this.props.manager.disableModule(this.props.manager.selected);
      }
    });
  };
  showSettingsModal = () => {
    this.setState({
      showSettings: true
    });
  };
  hideSettingsModal = () => {
    this.setState({
      showSettings: false
    });
  };

  renderButtons() {
    const module = this.props.manager.selected;
    if (module) {
      if (!module.enabled) {
        return (
          <Button color="primary" onClick={this.showSettingsModal}>
            Enable
          </Button>
        );
      }
      if (module.hasUpdate) {
        return (
          <React.Fragment>
            <Button color="success" onClick={this.handleUpdate}>
              Update
            </Button>
            <Button
              className="ml-2"
              color="danger"
              onClick={this.handleDisable}
            >
              Disable
            </Button>
          </React.Fragment>
        );
      }
    }
    return null;
  }

  render() {
    const module = this.props.manager.selected;
    if (!module) {
      return <Robot />;
    }
    return (
      <Dashboard
        headerContent={
          <Fragment>
            <Link
              className="btn kata-btn-float kata-subpage__back-btn kata-btn-float__primary"
              to={`/module/all`}
            >
              <i className="icon-arrow-left" />
            </Link>
            <div className="float-left flex-fill">
              <h1 className="heading1 kata-subpage__title">{`${
                module.name
              } Module`}</h1>
              <Link to={`/module/all`} className="text-muted">
                Module
              </Link>
              <span className="kata-subpage__bread-separator">/</span>
              <span className="text-primary">{`${module.name} Module`}</span>
            </div>
            <div>{this.renderButtons()}</div>
          </Fragment>
        }
      >
        <Board
          bodyClassName="kata-module-board-body"
          headingChildren={
            module.enabled ? (
              <div className="text-center">
                <div className="btn-group kata-btn-group">
                  <NavLink
                    exact
                    to={`/module/${module.id}/overview`}
                    className="btn kata-btn-group__item"
                    activeClassName="kata-btn-group__item--selected"
                  >
                    Overview
                  </NavLink>
                  <NavLink
                    to={`/module/${module.id}/settings`}
                    exact
                    className="btn kata-btn-group__item"
                    activeClassName="kata-btn-group__item--selected"
                  >
                    Settings
                  </NavLink>
                </div>
              </div>
            ) : (
              undefined
            )
          }
        >
          <div className="kata-module-info">
            <h3 className="subtitle">Author</h3>
            <p className="body-text">{module.author}</p>
          </div>
          <div className="kata-module-info">
            <h3 className="subtitle">Date</h3>
            <table className="kata-light-table kata-light-table--compact">
              <thead>
                <tr>
                  <th>Publish Date</th>
                  <th>Update Date</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>January 17, 2018</td>
                  <td>August 17, 2018</td>
                </tr>
              </tbody>
            </table>
          </div>
          <div className="kata-module-info">
            <h3 className="subtitle">Description</h3>
            <p className="body-text">{module.description}</p>
          </div>
          <div className="kata-module-info">
            <h3 className="subtitle">Screenshots</h3>
            <div className="kata-module-info__screenshots">
              <img
                className="kata-module-info__screenshot"
                src="https://via.placeholder.com/350x250"
                alt=""
              />
              <img
                className="kata-module-info__screenshot"
                src="https://via.placeholder.com/350x250"
                alt=""
              />
              <img
                className="kata-module-info__screenshot"
                src="https://via.placeholder.com/350x250"
                alt=""
              />
            </div>
          </div>
          <div className="kata-module-info">
            <h3 className="subtitle">Changelogs</h3>
            <table className="kata-light-table kata-light-table--striped">
              <thead>
                <tr>
                  <th style={{ width: '20%' }}>Version</th>
                  <th style={{ width: '20%' }}>Release Date</th>
                  <th>Change Log</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>0.1.1</td>
                  <td>August 17, 2018</td>
                  <td>
                    Lorem, ipsum dolor sit amet consectetur adipisicing elit.
                    Assumenda, exercitationem facilis! Rem, officiis aut fugit
                    ipsum sed soluta optio perspiciatis magni. Placeat, dolore
                    illum. Adipisci error nam iusto magni explicabo!
                  </td>
                </tr>
                <tr>
                  <td>0.1.0</td>
                  <td>August 15, 2018</td>
                  <td>
                    Lorem, ipsum dolor sit amet consectetur adipisicing elit.
                    Assumenda, exercitationem facilis! Rem, officiis aut fugit
                    ipsum sed soluta optio perspiciatis magni. Placeat, dolore
                    illum. Adipisci error nam iusto magni explicabo!
                  </td>
                </tr>
                <tr>
                  <td>0.0.9</td>
                  <td>August 1, 2018</td>
                  <td>
                    Lorem, ipsum dolor sit amet consectetur adipisicing elit.
                    Assumenda, exercitationem facilis! Rem, officiis aut fugit
                    ipsum sed soluta optio perspiciatis magni. Placeat, dolore
                    illum. Adipisci error nam iusto magni explicabo!
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </Board>

        {this.props.manager.selected && this.props.manager.selected.settings ? (
          <ModuleSettingModal
            module={this.props.manager.selected}
            isOpen={this.state.showSettings}
            onClose={this.hideSettingsModal}
            onSaveSettings={this.handleEnable}
            isEnabling={this.props.manager.isModuleEnabling(
              this.props.manager.selected
            )}
          />
        ) : null}
      </Dashboard>
    );
  }
}
