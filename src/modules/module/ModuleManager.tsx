import React from 'react';
import { connect } from 'react-redux';

import Robot from 'components/Loading/Robot';
import confirm from 'components/Modal/ConfirmDialog';

import RootStore from 'interfaces/rootStore';
import { DataMap } from 'interfaces/common';
import { Module } from 'interfaces/module';
import MODULE, {
  DisableModuleRequestAction,
  EnableModuleRequestAction,
  FetchModuleRequestAction,
  SetSelectedModuleAction,
  UpdateModuleRequestAction
} from 'stores/module/types';
import {
  getModuleData,
  getModuleIndex,
  getModuleLoadingState,
  getSelectedModule
} from 'stores/module/selectors';
import { getLoading } from 'stores/app/loadings/selectors';
import {
  disableModuleRequest,
  enableModuleRequest,
  fetchModuleRequest,
  setSelectedModule,
  updateModuleRequest
} from 'stores/module/actions';

interface PropsFromState {
  selected: Module;
  isFetching: boolean;
  data: DataMap<Module>;
  index: string[];

  isModuleUpdating(module: Module): boolean;
  isModuleEnabling(module: Module): boolean;
}

interface PropsFromDispatch {
  fetchModule: () => FetchModuleRequestAction;
  disableModule: (module: Module) => DisableModuleRequestAction;
  updateModule: (module: Module) => UpdateModuleRequestAction;
  enableModule: (module: Module) => EnableModuleRequestAction;
  selectModule: (moduleId: string) => SetSelectedModuleAction;
}

interface OwnProps {
  actor: (props: ComponentProps) => any;
}

export type ComponentProps = OwnProps & PropsFromState & PropsFromDispatch;
export type ModuleManagerApi = PropsFromState & PropsFromDispatch;

class ModuleManager extends React.Component<ComponentProps> {
  handleDisable = (module: Module) => {
    confirm({
      title: 'Disable Module',
      message: `Are you sure want to disable ${module.name} module?`,
      okLabel: 'Disable'
    }).then(result => {
      if (result) {
        this.props.disableModule(module);
      }
    });
  };
  handleUpdate = (module: Module) => {
    this.props.updateModule(module);
  };
  handleEnable = (module: Module) => {
    this.props.enableModule(module);
  };
  handleOpenModule = (module: Module) => {
    this.props.selectModule(module.id);
  };

  render() {
    if (this.props.isFetching) {
      return <Robot />;
    }
    return this.props.actor(this.props);
  }
}

const mapStateToProps = (store: RootStore) => ({
  selected: getSelectedModule(store),
  isFetching: getLoading(store.app.loadings, MODULE.FETCH_MODULE_REQUEST),
  data: getModuleData(store.module),
  index: getModuleIndex(store.module),
  isModuleUpdating: (module: Module) =>
    getModuleLoadingState(
      store.app.loadings,
      MODULE.UPDATE_MODULE_REQUEST,
      module
    ),
  isModuleEnabling: (module: Module) =>
    getModuleLoadingState(
      store.app.loadings,
      MODULE.ENABLE_MODULE_REQUEST,
      module
    )
});
const mapDispatchToProps = {
  fetchModule: fetchModuleRequest,
  disableModule: disableModuleRequest,
  updateModule: updateModuleRequest,
  enableModule: enableModuleRequest,
  selectModule: setSelectedModule
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ModuleManager);
