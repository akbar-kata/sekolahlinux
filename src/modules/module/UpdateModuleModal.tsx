import React from 'react';

import { Module } from 'interfaces/module';
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'components/Modal';
import { Button } from 'components/Button';

interface Props {
  module: Module;
  isOpen: boolean;
  onClose: () => void;
  onUpdate: (module: Module) => void;
  isUpdating: boolean;
}

export default class UpdateModuleModal extends React.Component<Props> {
  constructor(props: Props) {
    super(props);
    this.handleUpdate = this.handleUpdate.bind(this);
  }
  handleUpdate() {
    this.props.onUpdate(this.props.module);
  }
  render() {
    return (
      <Modal show={this.props.isOpen} onClose={this.props.onClose}>
        <ModalHeader>Update Info</ModalHeader>
        <ModalBody>
          <table className="kata-light-table kata-light-table--horizontal mt-2 mb-2">
            <tbody>
              <tr className="--bottomless">
                <th>Module</th>
                <td>{this.props.module.name}</td>
              </tr>
              <tr className="--bottomless">
                <th>Author</th>
                <td>{this.props.module.author}</td>
              </tr>
              <tr className="--bottomless">
                <th>Version</th>
                <td>{this.props.module.version}</td>
              </tr>
              <tr className="--bottomless">
                <th>Date</th>
                <td>
                  {this.props.module.updatedAt
                    ? this.props.module.updatedAt.toDateString()
                    : '-'}
                </td>
              </tr>
              <tr>
                <th>Change Log</th>
                <td>
                  {this.props.module.changeLog
                    ? this.props.module.changeLog
                    : '-'}
                </td>
              </tr>
            </tbody>
          </table>
        </ModalBody>
        <ModalFooter>
          <Button
            color="primary"
            onClick={this.handleUpdate}
            loading={this.props.isUpdating}
          >
            Update
          </Button>
          <Button onClick={this.props.onClose}>Cancel</Button>
        </ModalFooter>
      </Modal>
    );
  }
}
