import React from 'react';

import Dashboard from 'components/Dashboard/Dashboard';
import { DashboardCards } from 'components/Dashboard/index';
import confirm from 'components/Modal/ConfirmDialog';
import { EmptyMessage } from 'components/Common/index';
import { Module } from 'interfaces/module';
import ModuleCard from 'modules/module/ModuleCard';
import { ModuleManagerApi } from 'modules/module/ModuleManager';

interface PropsFromState {
  manager: ModuleManagerApi;
}

interface OwnProps {
  showDisabled: boolean;
}

type ComponentProps = PropsFromState & OwnProps;

export default class ModuleDashboard extends React.Component<ComponentProps> {
  static defaultProps = {
    showDisabled: true
  };
  handleDisable = (module: Module) => {
    confirm({
      title: 'Disable Module',
      message: `Are you sure want to disable ${module.name} module?`,
      okLabel: 'Disable'
    }).then(result => {
      if (result) {
        this.props.manager.disableModule(module);
      }
    });
  };
  handleUpdate = (module: Module) => {
    this.props.manager.updateModule(module);
  };
  handleEnable = (module: Module) => {
    this.props.manager.enableModule(module);
  };
  handleOpenModule = (module: Module) => {
    this.props.manager.selectModule(module.id);
  };

  render() {
    return (
      <Dashboard title="Modules" tooltip="Modules is a collection of feature ">
        {this.props.manager.index.length === 0 ? (
          <EmptyMessage
            image={require('assets/images/no-channel.svg')}
            title="No Modules Available"
          >
            No modules available.
          </EmptyMessage>
        ) : null}
        <DashboardCards>{this.renderModulesCard()}</DashboardCards>
      </Dashboard>
    );
  }

  renderModulesCard() {
    return this.props.manager.index.map(index => {
      if (!this.props.showDisabled && !this.props.manager.data[index].enabled) {
        return null;
      }
      return (
        <ModuleCard
          onClick={this.handleOpenModule}
          key={index}
          isUpdating={this.props.manager.isModuleUpdating(
            this.props.manager.data[index]
          )}
          module={this.props.manager.data[index]}
          onDisable={this.handleDisable}
          onUpdate={this.handleUpdate}
          onEnable={this.handleEnable}
        />
      );
    });
  }
}
