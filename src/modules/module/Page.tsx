import React from 'react';
import { Redirect, Route, Switch } from 'react-router';

import EnabledModulePage from './enabled/Page';
import AllModulePage from './all/Page';
import ModuleDetailPage from './detail/Page';
import SelectedUrl from 'containers/SelectedUrl';

export default class Page extends React.Component {
  render() {
    return (
      <SelectedUrl>
        {projectId => {
          const parentPath = `/project/${projectId}`;
          return (
            <Switch>
              <Route
                path={`${parentPath}/module`}
                render={() => <Redirect to={`${parentPath}/module/enabled`} />}
                exact
              />
              <Route
                path={`${parentPath}/module/enabled`}
                component={EnabledModulePage}
                exact
              />
              <Route
                path={`${parentPath}/module/all`}
                component={AllModulePage}
                exact
              />
              <Route
                path={`${parentPath}/module/:id/overview`}
                component={ModuleDetailPage}
              />
              <Route
                path={`${parentPath}/module/:id`}
                component={ModuleDetailPage}
              />
            </Switch>
          );
        }}
      </SelectedUrl>
    );
  }
}
