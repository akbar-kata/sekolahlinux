import React from 'react';
import ModuleManager from 'modules/module/ModuleManager';
import ModuleDashboard from 'modules/module/ModuleDashboard';

export default class Page extends React.Component {
  render() {
    return (
      <ModuleManager
        actor={api => <ModuleDashboard manager={api} showDisabled={false} />}
      />
    );
  }
}
