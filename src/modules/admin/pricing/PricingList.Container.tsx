import React from 'react';
import { connect } from 'react-redux';

import RootStore from 'interfaces/rootStore';
import {
  fetchPricingRequest,
  updatePricingRequest,
  removePricingRequest
} from 'stores/pricing/actions';
import {
  getPricingData,
  getPricingIndex,
  getPricingLoading,
  getPricingError,
  getPricingMeta
} from 'stores/pricing/selectors';
import { openModal, closeModal } from 'stores/app/modal/actions';
import { isModalOpen, getModalData } from 'stores/app/modal/selectors';
import PricingList from './PricingList';
import PricingForm from './PricingForm';
import PricingPagination from './PricingPagination';

interface PropsFromState {
  isLoading: boolean;
  error: string | null;
  index: string[];
  data: any;
  isUpdateOpen: boolean;
  dataToUpdate: any;
  metadata: any;
}

interface PropsFromDispatch {
  fetchAction: Function;
  editAction: Function;
  removeAction: Function;
  openUpdate: Function;
  closeUpdate: Function;
}

interface Props extends PropsFromState, PropsFromDispatch {}

interface State {}

const updateModalId = 'PricingUpdate';

class PricingListContainer extends React.Component<Props, State> {
  componentDidMount() {
    this.props.fetchAction();
  }

  public openEdit = (selected: any) => () => {
    this.props.openUpdate(selected);
  };

  public closeEdit = () => {
    this.props.closeUpdate();
  };

  onUpdate = data => {
    this.props.editAction(data.id, data);
  };

  public removePricing = (id: string, name: string) => {
    return () => {
      if (confirm(`Are you sure want to delete deployment ${name}?`)) {
        this.props.removeAction(id, name);
      }
    };
  };

  render() {
    return (
      <div>
        <PricingForm
          type="update"
          isReadOnly
          isOpen={this.props.isUpdateOpen}
          data={this.props.dataToUpdate}
          onCancel={this.closeEdit}
          onSubmit={this.onUpdate}
        />
        <PricingList
          isLoading={this.props.isLoading}
          index={this.props.index}
          data={this.props.data}
          editAction={this.openEdit}
          removeAction={this.removePricing}
        />
        <PricingPagination />
      </div>
    );
  }
}

const mapStateToProps = ({
  pricing,
  app: { modal }
}: RootStore): PropsFromState => {
  return {
    isLoading: getPricingLoading(pricing, 'fetch'),
    error: getPricingError(pricing, 'fetch'),
    index: getPricingIndex(pricing),
    data: getPricingData(pricing),
    metadata: getPricingMeta(pricing),
    isUpdateOpen: isModalOpen(modal, updateModalId),
    dataToUpdate: getModalData(modal, updateModalId)
  };
};

const mapDispatchToProps = (dispatch: Function): PropsFromDispatch => {
  return {
    fetchAction: () => dispatch(fetchPricingRequest()),
    editAction: (id: string, data: any) =>
      dispatch(updatePricingRequest(id, data)),
    removeAction: (id: string, name: string) =>
      dispatch(removePricingRequest(id, name)),
    openUpdate: (data: any) => dispatch(openModal(updateModalId, data)),
    closeUpdate: () => dispatch(closeModal(updateModalId))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PricingListContainer);
