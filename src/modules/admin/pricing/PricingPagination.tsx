import React from 'react';
import { connect } from 'react-redux';
import { Metadata } from 'interfaces/metadata';
import RootStore from 'interfaces/rootStore';
import { getPricingMeta } from 'stores/pricing/selectors';
import { fetchPricingRequestWithPayload } from 'stores/pricing/actions';

interface PropsFromDispatch {
  requestsPage: Function;
}
interface PropsFromState {
  metadata: Metadata;
}
interface Props extends PropsFromDispatch, PropsFromState {}

class PricingPagination extends React.Component<Props, {}> {
  getLastPage() {
    return Math.ceil(this.props.metadata.total / this.props.metadata.limit);
  }
  getFirstPage() {
    return 1;
  }
  getPageCount() {
    return this.getLastPage();
  }
  getCurrentPage() {
    return parseInt(this.props.metadata.page.toString(), 10);
  }
  getPreviousPage() {
    return this.getCurrentPage() - 1 < this.getFirstPage()
      ? null
      : this.getCurrentPage() - 1;
  }
  getNextPage() {
    return this.getCurrentPage() + 1 > this.getLastPage()
      ? null
      : this.getCurrentPage() + 1;
  }

  public requestPage = page => () => {
    this.props.requestsPage(page);
  };

  ifPageShouldHaveActiveClass(page: number): any {
    if (page === this.props.metadata.page) {
      return 'active';
    }
  }
  ifPreviousPageShouldHaveDisabledClass(): string {
    if (this.getPreviousPage() === null) {
      return 'disabled';
    }
    return '';
  }
  ifNextPageShouldHaveDisabledClass(): string {
    if (this.getNextPage() === null) {
      return 'disabled';
    }
    return '';
  }

  ifShouldDisplayPagination(): JSX.Element {
    if (this.getFirstPage() === this.getLastPage()) {
      return (
        <ul className="pagination">
          <li className="disabled">
            <a>&laquo;</a>
          </li>
          <li className={this.ifPageShouldHaveActiveClass(this.getFirstPage())}>
            <a>{this.getFirstPage()}</a>
          </li>
          <li className="disabled">
            <a>&raquo;</a>
          </li>
        </ul>
      );
    }
    return (
      <ul className="pagination">
        <li className={this.ifPreviousPageShouldHaveDisabledClass()}>
          <a onClick={this.requestPage(this.getPreviousPage())}>&laquo;</a>
        </li>
        <li className={this.ifPageShouldHaveActiveClass(this.getFirstPage())}>
          <a onClick={this.requestPage(this.getFirstPage())}>
            {this.getFirstPage()}
          </a>
        </li>
        <li className={this.ifPageShouldHaveActiveClass(this.getLastPage())}>
          <a
            onClick={this.requestPage(this.getLastPage())}
            className={this.ifPageShouldHaveActiveClass(this.getFirstPage())}
          >
            {this.getLastPage()}
          </a>
        </li>
        <li className={this.ifNextPageShouldHaveDisabledClass()}>
          <a onClick={this.requestPage(this.getNextPage())}>&raquo;</a>
        </li>
      </ul>
    );
  }

  render() {
    return (
      <nav className="page-pagination">
        <div>{this.ifShouldDisplayPagination()}</div>
        <div>
          <select
            name="pricing_pagination_jump"
            id=""
            className="form-control"
            onChange={this.onPaginationJumpChange}
          >
            {this.renderPaginationJump()}
          </select>
        </div>
      </nav>
    );
  }

  renderPaginationJump(): any {
    const options: JSX.Element[] = [];
    // tslint:disable-next-line:no-increment-decrement
    for (let i = this.getFirstPage(); i < this.getLastPage() + 1; i++) {
      options.push(
        <option key={i} value={i}>
          {i}
        </option>
      );
    }
    return options;
  }

  private onPaginationJumpChange = event => {
    this.props.requestsPage(event.target.value);
  };
}

const mapDispatches = (dispatch: Function): PropsFromDispatch => {
  return {
    requestsPage: page => dispatch(fetchPricingRequestWithPayload(page))
  };
};
const mapStates = ({ pricing, app: { modal } }: RootStore): PropsFromState => {
  return {
    metadata: getPricingMeta(pricing)
  };
};

export default connect(
  mapStates,
  mapDispatches
)(PricingPagination);
