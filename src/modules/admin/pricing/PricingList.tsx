import React from 'react';

import * as Pricing from 'interfaces/pricing';
import PricingItem from './PricingItem';

interface Props {
  isLoading: boolean;
  index: string[];
  data: Pricing.DataMap;
  editAction: Function;
  removeAction: Function;
}

const LoadingList = (
  <tr>
    <td colSpan={5} className="text-center">
      <h5 className="pa4 text-bold">Loading...</h5>
    </td>
  </tr>
);

const EmptyList = (
  <tr>
    <td colSpan={5} className="text-center">
      <h5 className="pa4 text-bold">No data found</h5>
    </td>
  </tr>
);

const PricingList: React.SFC<Props> = ({
  isLoading,
  index,
  data,
  editAction,
  removeAction
}) => {
  return (
    <table className="table table-striped table-hover">
      <thead>
        <tr>
          <th>Name</th>
          <th>Description</th>
          <th>Tierings</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>
        {(isLoading && LoadingList) ||
          ((index.length === 0 && EmptyList) ||
            index.map(id => (
              <PricingItem
                key={id}
                pricingId={id}
                name={data[id].name}
                description={data[id].description}
                currency={data[id].currency}
                tierings={data[id].tierings}
                editAction={editAction(data[id])}
                removeAction={removeAction(id, data[id].name)}
              />
            )))}
      </tbody>
    </table>
  );
};

export default PricingList;
