import React, { Component } from 'react';

import { JsonObject } from 'interfaces';
import { Form, Text, ReactSelect, FormError } from 'components/Form';
import { Button } from 'components/Button';
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'components/Modal';

interface Props {
  isOpen: boolean;
  type: string;
  id?: string;
  data?: JsonObject;
  onSubmit: Function;
  onCancel: Function;
  isReadOnly?: boolean;
}

interface States {}

const inputId = 'form-input-pricing';
const width150 = {
  width: 150
};

const currencyOpts = [
  {
    label: 'IDR',
    value: 'IDR'
  },
  {
    label: 'USD',
    value: 'USD'
  }
];

const tieringTypeOpts = [
  {
    label: 'Message',
    value: 'message'
  },
  {
    label: 'Session',
    value: 'session'
  },
  {
    label: 'MAU',
    value: 'mau'
  },
  {
    label: 'User',
    value: 'user'
  }
];

class PricingForm extends Component<Props, States> {
  componentDidMount() {
    const el = document.getElementById(inputId);
    if (el && el.focus) {
      el.focus();
    }
  }

  onClose = () => {
    this.props.onCancel();
  };

  onValidate = ({ name, description, currency, tierings }) => ({
    name: !name ? 'Name is required' : undefined,
    description: !description ? 'Description is required' : undefined,
    currency: !currency ? 'Currency must be selected' : undefined,
    tierings:
      !tierings || !tierings.length
        ? 'Tierings is required'
        : tierings.map(tiering => ({
            name: !tiering.name ? 'Name is required' : undefined,
            type: !tiering.type ? 'Type must be selected' : undefined,
            startCount: !tiering.startCount
              ? 'Start count is required'
              : undefined,
            endCount: !tiering.endCount
              ? 'End count is required'
              : parseInt(tiering.endCount, 10) <=
                parseInt(tiering.startCount, 10)
              ? 'End count must be larger than start count'
              : undefined,
            commitment: !tiering.commitment
              ? 'Commitment is required'
              : undefined,
            unitPrice: !tiering.unitPrice ? 'Unit price is required' : undefined
          }))
  });

  innerForm = ({
    values,
    setValue,
    addValue,
    editValue,
    removeValue,
    submitForm
  }) => (
    <form onSubmit={submitForm}>
      <ModalHeader>{this.props.type} Tiering</ModalHeader>
      <ModalBody>
        <table className="table table-noborder">
          <tbody>
            <tr>
              <td className="v-mid" style={width150}>
                <label className="control-label">Name</label>
              </td>
              <td>
                <Text
                  readOnly={this.props.isReadOnly}
                  id={inputId}
                  field="name"
                  className="form-control"
                />
              </td>
            </tr>
            <tr>
              <td className="v-mid">
                <label className="control-label">Description</label>
              </td>
              <td>
                <Text
                  readOnly={this.props.isReadOnly}
                  field="description"
                  className="form-control"
                />
              </td>
            </tr>
            <tr>
              <td className="v-mid">
                <label className="control-label">Currency</label>
              </td>
              <td>
                <ReactSelect
                  disabled={this.props.isReadOnly}
                  field="currency"
                  clearable={false}
                  simpleValue
                  options={currencyOpts}
                />
              </td>
            </tr>
            <tr>
              <td colSpan={2}>
                <label className="control-label">Tierings</label>
                <FormError field="tierings" />
                {values.tierings &&
                  values.tierings.length &&
                  values.tierings.map((tiering, index) => (
                    <div className="mb3 botstudio-item" key={index}>
                      {this.props.isReadOnly ? null : (
                        <button
                          type="button"
                          className="btn btn-danger pv2 ph3 br2 mb0 ml2 pull-right"
                          onClick={() => removeValue('tierings', index)}
                        >
                          <i className="icon-trash" />
                        </button>
                      )}
                      <div className="mb3 mt4">
                        <label className="botstudio-label">Name</label>
                        <Text
                          readOnly={this.props.isReadOnly}
                          field={`tierings.${index}.name`}
                          className="form-control"
                          placeholder="Name"
                        />
                      </div>
                      <div className="mb3">
                        <label className="botstudio-label">Type</label>
                        <ReactSelect
                          disabled={this.props.isReadOnly}
                          field={`tierings.${index}.type`}
                          clearable={false}
                          simpleValue
                          options={tieringTypeOpts}
                        />
                      </div>
                      <div className="mb3 row">
                        <div className="col-md-6">
                          <label className="botstudio-label">Start count</label>
                          <Text
                            readOnly={this.props.isReadOnly}
                            field={`tierings.${index}.startCount`}
                            className="form-control"
                            type="number"
                          />
                        </div>
                        <div className="col-md-6">
                          <label className="botstudio-label">End count</label>
                          <Text
                            readOnly={this.props.isReadOnly}
                            field={`tierings.${index}.endCount`}
                            className="form-control"
                            type="number"
                          />
                        </div>
                      </div>
                      <div className="mb3">
                        <label className="botstudio-label">Commitment</label>
                        <Text
                          readOnly={this.props.isReadOnly}
                          field={`tierings.${index}.commitment`}
                          className="form-control"
                          type="number"
                        />
                      </div>
                      <div className="mb3">
                        <label className="botstudio-label">Unit Price</label>
                        <Text
                          readOnly={this.props.isReadOnly}
                          field={`tierings.${index}.unitPrice`}
                          className="form-control"
                          type="number"
                        />
                      </div>
                    </div>
                  ))}
                <div>
                  {this.props.isReadOnly ? null : (
                    <button
                      type="button"
                      className="btn btn-primary btn-sm br2"
                      onClick={() => addValue('tierings', {})}
                    >
                      <i className="icon-plus" /> Add tiering
                    </button>
                  )}
                </div>
              </td>
            </tr>
          </tbody>
        </table>
      </ModalBody>
      <ModalFooter>
        {this.props.isReadOnly ? null : (
          <Button color="primary" onClick={submitForm}>
            <i
              className={`mr2 icon-${
                this.props.type === 'create' ? 'plus' : 'check'
              }`}
            />{' '}
            {this.props.type} Pricing
          </Button>
        )}
        <Button
          type="button"
          color="secondary"
          className="br2"
          onClick={this.onClose}
        >
          Cancel
        </Button>
      </ModalFooter>
    </form>
  );

  render() {
    const { isOpen, data } = this.props;
    return (
      <Modal show={isOpen} onClose={this.onClose}>
        <Form
          defaultValues={data}
          validate={this.onValidate}
          onSubmit={this.props.onSubmit}
        >
          {this.innerForm}
        </Form>
      </Modal>
    );
  }
}

export default PricingForm;
