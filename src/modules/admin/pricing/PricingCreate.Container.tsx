import React from 'react';
import { connect } from 'react-redux';

import RootStore from 'interfaces/rootStore';
import { Button } from 'components/Button';
import { addPricingRequest } from 'stores/pricing/actions';
import { openModal, closeModal } from 'stores/app/modal/actions';
import { isModalOpen } from 'stores/app/modal/selectors';
import PricingForm from './PricingForm';

// interface Props {

// }

interface PropsFromState {
  error: string;
  isOpen: boolean;
}

interface PropsFromDispatch {
  createAction: Function;
  openModal: Function;
  closeModal: Function;
}

interface State {
  // isOpen: boolean;
}

const modalId = 'PricingCreate';

class PricingCreate extends React.Component<any, State> {
  onCreate = data => {
    this.props.createAction(data);
  };

  render() {
    return (
      <div className="dib">
        <Button color="primary" onClick={this.props.openModal}>
          <i className="icon-plus mr2" /> Create Pricing
        </Button>
        <PricingForm
          type="create"
          isOpen={this.props.isOpen}
          onCancel={this.props.closeModal}
          onSubmit={this.onCreate}
        />
      </div>
    );
  }
}

const mapStateToProps = ({
  pricing,
  app: { modal }
}: RootStore): PropsFromState => {
  return {
    isOpen: isModalOpen(modal, modalId),
    error: ''
  };
};

const mapDispatchToProps = (dispatch: Function): PropsFromDispatch => {
  return {
    createAction: (data: any) => dispatch(addPricingRequest(data)),
    openModal: () => dispatch(openModal(modalId)),
    closeModal: () => dispatch(closeModal(modalId))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PricingCreate);
