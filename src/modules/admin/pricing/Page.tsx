import React from 'react';

import PricingCreate from './PricingCreate.Container';
import PricingList from './PricingList.Container';

export default () => (
  <div>
    <div>
      Pricing plan
      <p>Plaform pricing manager</p>
      <div className="breadcrumb-env">
        <PricingCreate />
      </div>
    </div>
    <div>
      <div>
        <PricingList />
      </div>
    </div>
  </div>
);
