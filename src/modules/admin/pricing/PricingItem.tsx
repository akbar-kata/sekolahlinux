import React from 'react';

import * as Pricing from 'interfaces/pricing';
import { Button } from 'components/Button';

interface Props extends Partial<Pricing.Data> {
  pricingId: string;
  editAction: any;
  removeAction: any;
}

const PricingItem: React.SFC<Props> = ({
  pricingId,
  name,
  currency,
  description,
  tierings,
  editAction,
  removeAction
}) => {
  return (
    <tr>
      <td>{name}</td>
      <td>{description}</td>
      <td>
        {tierings &&
          tierings.map((tiering, index) => (
            <div key={index}>{tiering.name}</div>
          ))}
      </td>
      <td>
        <Button color="primary" onClick={editAction}>
          <i className="icon-settings v-mid" />
        </Button>
        {/* <Button bsStyle="danger" className="pv2 ph3 br3" onClick={removeAction}>
          <i className="icon-trash v-mid" />
        </Button> */}
      </td>
    </tr>
  );
};

export default PricingItem;
