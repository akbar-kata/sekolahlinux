import React from 'react';
import { Form } from 'components/Form';

export default class SearchBar extends React.Component<any> {
  render() {
    return (
      <div className="dib">
        <Form>
          {/* <Text field="search" className="form-control" /> */}
          <div className="input-group" style={{ paddingBottom: 10 }}>
            <span
              className="input-group-addon"
              style={{
                padding: 0,
                border: 0,
                position: 'absolute',
                zIndex: 9,
                marginTop: 10,
                marginLeft: 10,
                background: '#ffffff'
              }}
            >
              <i className="fa fa-search" />
            </span>
            <input
              type="text"
              className="form-control"
              placeholder="Search"
              style={{ borderRadius: 28, paddingLeft: 30 }}
            />
          </div>
        </Form>
      </div>
    );
  }
}
