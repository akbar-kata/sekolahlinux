import React from 'react';
import { connect } from 'react-redux';

import RootStore from 'interfaces/rootStore';

import { openModal, closeModal } from 'stores/app/modal/actions';
import { isModalOpen, getModalData } from 'stores/app/modal/selectors';
import { getBotData, getBotIndex } from 'stores/bot/selectors';
import {
  fetchSubscriptionRequest,
  updateSubscriptionRequest
} from 'stores/subscription/actions';
import {
  getSubscriptionData,
  getSubscriptionIndex,
  getSubscriptionLoading
} from 'stores/subscription/selectors';

import SubcriptionList from './SubscriptionList';
import SubscriptionForm from './SubscriptionForm';

interface PropsFromState {
  isLoading: boolean;
  index: string[];
  indexBot: string[];
  data: any;
  bots: any;
  isUpdateOpen: boolean;
  dataToUpdate: any;
}

interface PropsFromDispatch {
  fetchAction: Function;
  createAction: Function;
  editAction: Function;
  openUpdate: Function;
  openAction: Function;
  openCreate: Function;
  closeUpdate: Function;
}

interface Props extends PropsFromState, PropsFromDispatch {}

interface State {}

const updateModalId = 'SubscriptionCreate';

class SubscriptionListContainer extends React.Component<Props, State> {
  componentDidMount() {
    this.props.fetchAction();
  }

  public openEdit = (selected: any) => () => {
    this.props.openUpdate(selected);
  };

  public openCreate = (selected: any) => () => {
    this.props.openCreate(selected);
  };

  public closeEdit = () => {
    this.props.closeUpdate();
  };

  onUpdate = data => {
    this.props.editAction(data.id, data);
  };

  render() {
    return (
      <div>
        <SubscriptionForm
          type="create"
          isOpen={this.props.isUpdateOpen}
          data={this.props.dataToUpdate}
          onCancel={this.closeEdit}
          onSubmit={this.onUpdate}
        />
        <SubcriptionList
          // isLoading={this.props.isLoading}
          index={this.props.index}
          indexBot={this.props.indexBot}
          data={this.props.data}
          bots={this.props.bots}
          editAction={this.openEdit}
          createAction={this.openCreate}
        />
      </div>
    );
  }
}

const mapStateToProps = ({
  bot,
  subscription,
  app: { modal }
}: RootStore): PropsFromState => {
  return {
    isLoading: getSubscriptionLoading(subscription, 'fetch'),
    index: getSubscriptionIndex(subscription),
    indexBot: getBotIndex(bot),
    data: getSubscriptionData(subscription),
    bots: getBotData(bot),
    isUpdateOpen: isModalOpen(modal, updateModalId),
    dataToUpdate: getModalData(modal, updateModalId)
  };
};

const mapDispatchToProps = (dispatch: Function): PropsFromDispatch => {
  return {
    fetchAction: () => dispatch(fetchSubscriptionRequest()),
    editAction: (id: string, data: any) =>
      dispatch(updateSubscriptionRequest(id, data)),
    openUpdate: (data: any) => dispatch(openModal(updateModalId, data)),
    openAction: (id: string, data: any) =>
      dispatch(updateSubscriptionRequest(id, data)),
    createAction: (id: string, data: any) =>
      dispatch(updateSubscriptionRequest(id, data)),
    openCreate: (data: any) => dispatch(openModal(updateModalId, data)),
    closeUpdate: () => dispatch(closeModal(updateModalId))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SubscriptionListContainer);
