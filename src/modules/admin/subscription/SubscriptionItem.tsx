import React from 'react';

import { Button } from 'components/Button';

interface Props {
  id: string;
  name: string;
  data: any;
  editAction: any;
  createAction: any;
}

const SubscriptionItem: React.SFC<Props> = ({
  id,
  name,
  data,
  editAction,
  createAction
}) => {
  return (
    <tr>
      <td>{name}</td>
      <td>
        <Button color="primary" className="btn-xs" onClick={createAction(data)}>
          <i className="icon-plus" />
        </Button>
      </td>
      <td />
    </tr>
  );
};

export default SubscriptionItem;
