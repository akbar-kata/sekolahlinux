import React, { Component } from 'react';
import { SingleDatePicker } from 'react-dates';

import { JsonObject } from 'interfaces';
import { Form, Text, Textarea } from 'components/Form';
import { Button } from 'components/Button';
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'components/Modal';

interface Props {
  isOpen: boolean;
  type: string;
  id?: string;
  data?: JsonObject;
  onSubmit: Function;
  onCancel: Function;
}

interface States {
  dateStart: null;
  dateEnd: null;
  focusedStart: boolean;
  focusedEnd: boolean;
}

const inputId = 'form-input-subscription';
const width150 = {
  width: 150
};

class SubscriptionForm extends Component<Props, States> {
  constructor(props: Props) {
    super(props);
    this.state = {
      focusedStart: false,
      dateStart: null,
      focusedEnd: false,
      dateEnd: null
    };

    this.onDateChange = this.onDateChange.bind(this);
    this.onFocusChange = this.onFocusChange.bind(this);
  }

  componentDidMount() {
    const el = document.getElementById(inputId);
    if (el && el.focus) {
      el.focus();
    }
  }

  onDateChange = date => {
    this.setState({ dateStart: date });
  };

  onFocusChange = ({ focused }) => {
    this.setState({ focusedStart: focused });
  };

  onDateChangeEnd = date => {
    this.setState({ dateEnd: date });
  };

  onFocusChangeEnd = ({ focused }) => {
    this.setState({ focusedEnd: focused });
  };

  onClose = () => {
    this.props.onCancel();
  };

  onValidate = ({ plans, rpmLimit, startDate, endDate, kpi }) => ({
    plans: !plans ? 'Pricing Plan is required' : undefined,
    rpmLimit: !rpmLimit ? 'RPM Limit is required' : undefined,
    startDate: !startDate ? 'Start Date is required' : undefined,
    endDate: !endDate ? 'End Date is required' : undefined,
    kpi: !kpi ? 'KPI is required' : undefined
  });

  innerForm = ({
    values,
    setValue,
    addValue,
    editValue,
    removeValue,
    submitForm
  }) => (
    <form onSubmit={submitForm}>
      <ModalHeader>{this.props.type} New Subscription Plan</ModalHeader>
      <ModalBody>
        <table className="table table-noborder">
          <tbody>
            <tr>
              <td className="v-mid" style={width150}>
                <label className="control-label">Pricing Plan</label>
              </td>
              <td>
                <Text id={inputId} field="plans" className="form-control" />
              </td>
            </tr>
            <tr>
              <td className="v-mid">
                <label className="control-label">RPM</label>
              </td>
              <td>
                <Text field="rpmLimit" className="form-control" />
              </td>
            </tr>
            <tr>
              <td className="v-mid">
                <label className="control-label">Start Date</label>
              </td>
              <td>
                <SingleDatePicker
                  id={'startDate'}
                  date={this.state.dateStart}
                  onDateChange={this.onDateChange}
                  focused={this.state.focusedStart}
                  onFocusChange={this.onFocusChange}
                  placeholder={'Start Date'}
                  numberOfMonths={1}
                  showDefaultInputIcon
                />
              </td>
            </tr>
            <tr>
              <td className="v-mid">
                <label className="control-label">End Date</label>
              </td>
              <td>
                <SingleDatePicker
                  id={'endDate'}
                  date={this.state.dateEnd}
                  onDateChange={this.onDateChangeEnd}
                  focused={this.state.focusedEnd}
                  onFocusChange={this.onFocusChangeEnd}
                  placeholder={'End Date'}
                  numberOfMonths={1}
                  showDefaultInputIcon
                />
              </td>
            </tr>
            <tr>
              <td className="v-mid">
                <label className="control-label">KPI</label>
              </td>
              <td>
                <Textarea field="kpi" className="form-control" />
              </td>
            </tr>
          </tbody>
        </table>
      </ModalBody>
      <ModalFooter>
        <Button color="primary" onClick={submitForm}>
          <i
            className={`mr2 icon-${
              this.props.type === 'create' ? 'plus' : 'check'
            }`}
          />{' '}
          {this.props.type} Subscription
        </Button>
        <Button
          type="button"
          color="secondary"
          className="br2"
          onClick={this.onClose}
        >
          Cancel
        </Button>
      </ModalFooter>
    </form>
  );

  render() {
    const { isOpen, data } = this.props;
    return (
      <Modal show={isOpen} onClose={this.onClose}>
        <Form
          defaultValues={data}
          validate={this.onValidate}
          onSubmit={this.props.onSubmit}
        >
          {this.innerForm}
        </Form>
      </Modal>
    );
  }
}

export default SubscriptionForm;
