import React from 'react';
import { connect } from 'react-redux';

import RootStore from 'interfaces/rootStore';
import { addPricingRequest } from 'stores/pricing/actions';
import { openModal, closeModal } from 'stores/app/modal/actions';
import { isModalOpen } from 'stores/app/modal/selectors';
import SubscriptionForm from './SubscriptionForm';
// import Subscription from 'interfaces/subscription';

// interface Props {

// }

interface PropsFromState {
  error: string;
  isOpen: boolean;
}

interface PropsFromDispatch {
  createAction: Function;
  openModal: Function;
  closeModal: Function;
}

interface State {
  // isOpen: boolean;
}

const modalId = 'SubscriptionCreate';

class SubscriptionCreate extends React.Component<any, State> {
  onCreate = data => {
    this.props.createAction(data);
  };

  render() {
    return (
      <div className="dib">
        <SubscriptionForm
          type="create"
          isOpen={this.props.isOpen}
          onCancel={this.props.closeModal}
          onSubmit={this.onCreate}
        />
      </div>
    );
  }
}

const mapStateToProps = ({
  pricing,
  app: { modal }
}: RootStore): PropsFromState => {
  return {
    isOpen: isModalOpen(modal, modalId),
    error: ''
  };
};

const mapDispatchToProps = (dispatch: Function): PropsFromDispatch => {
  return {
    createAction: (data: any) => dispatch(addPricingRequest(data)),
    openModal: () => dispatch(openModal(modalId)),
    closeModal: () => dispatch(closeModal(modalId))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SubscriptionCreate);
