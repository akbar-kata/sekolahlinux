import React from 'react';

import SearchBar from './SearchBar.Container';
import SubscriptionList from './SubscriptionList.Container';

export default () => (
  <div>
    <div>
      Subscription plan
      <p>Manage Subscription Plan for Bots</p>
    </div>
    <SearchBar />
    <div>
      <div>
        <SubscriptionList />
      </div>
    </div>
  </div>
);
