import React from 'react';

import * as Subscription from 'interfaces/subscription';
import * as Bot from 'interfaces/bot';
import SubscriptionItem from './SubscriptionItem';
import { Pagination } from 'components/Pagination';

const Pagination2: any = Pagination;

interface Props {
  isLoading?: boolean | false;
  index: string[];
  indexBot: string[];
  data: Subscription.DataMap;
  bots: Bot.BotMap;
  editAction: Function;
  createAction: Function;
}

interface States {
  activePage: number;
}

const LoadingList = (
  <tr>
    <td colSpan={5} className="text-center">
      <h5 className="pa4 text-bold">Loading...</h5>
    </td>
  </tr>
);

const EmptyList = (
  <tr>
    <td colSpan={5} className="text-center">
      <h5 className="pa4 text-bold">No data found</h5>
    </td>
  </tr>
);

class SubcriptionList extends React.Component<Props, States> {
  state: States = {
    activePage: 1
  };
  renderCurrentPage(): JSX.Element[] {
    const pageStart = this.state.activePage * this.getItemPerPage();
    const pageEnd =
      pageStart + 10 > this.props.indexBot.length
        ? this.props.indexBot.length
        : pageStart + 10;
    const displayedSlice = this.props.indexBot.slice(pageStart, pageEnd);
    return displayedSlice.map(id => (
      <SubscriptionItem
        key={id}
        id={id}
        name={this.props.bots[id].name}
        data={this.props.bots[id]}
        editAction={this.props.editAction}
        createAction={this.props.createAction}
      />
    ));
  }

  getItemPerPage() {
    return 10;
  }
  getPageSize() {
    return this.props.indexBot
      ? Math.floor(this.props.indexBot.length / this.getItemPerPage())
      : 1;
  }

  handleSelect = page => {
    this.setState({
      activePage: page
    });
  };

  render() {
    return (
      <div>
        <table className="table table-striped table-hover">
          <thead>
            <tr>
              <th>Bot Name</th>
              <th>Active Subscription Plan</th>
              <th>Deactivated Subscription Plan</th>
            </tr>
          </thead>
          <tbody>
            {(this.props.isLoading && LoadingList) ||
              ((this.props.indexBot.length === 0 && EmptyList) ||
                this.renderCurrentPage())}
          </tbody>
        </table>
        <nav className="page-pagination">
          <div>
            <Pagination2
              first
              last
              ellipsis
              boundaryLinks
              items={this.getPageSize()}
              maxButtons={5}
              activePage={this.state.activePage}
              onSelect={this.handleSelect}
            />
          </div>
          <div>
            <select
              id=""
              className="form-control"
              onChange={this.onPaginationJumpChange}
            >
              {this.renderPaginationJump()}
            </select>
          </div>
        </nav>
      </div>
    );
  }

  renderPaginationJump(): any {
    const options: JSX.Element[] = [];
    // tslint:disable-next-line:no-increment-decrement
    for (let i = 1; i < this.getPageSize() + 1; i++) {
      options.push(
        <option key={i} value={i}>
          {i}
        </option>
      );
    }
    return options;
  }
  private onPaginationJumpChange = event => {
    this.handleSelect(parseInt(event.target.value, 10));
  };
}

export default SubcriptionList;
