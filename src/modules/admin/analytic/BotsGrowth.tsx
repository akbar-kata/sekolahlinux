import React from 'react';

import {
  ResponsiveContainer,
  LineChart,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip
} from 'recharts';

class CustomizedAxisTick extends React.Component<any, any> {
  render() {
    const { x, y, payload } = this.props;

    return (
      <g transform={`translate(${x},${y})`}>
        <text
          x={0}
          y={0}
          dy={16}
          textAnchor="end"
          fill="#666"
          transform="rotate(-35)"
        >
          {payload.value}
        </text>
      </g>
    );
  }
}

class BotGrowth extends React.Component<any, any> {
  render() {
    const { isLoading, error, data } = this.props;
    if (isLoading) {
      return <div>Loading...</div>;
    }

    if (error !== null) {
      return <div className="text-danger">{this.props.error}</div>;
    }

    return (
      <ResponsiveContainer>
        <LineChart
          data={data}
          margin={{ top: 5, right: 30, left: 20, bottom: 20 }}
        >
          <XAxis
            dataKey="shortDate"
            padding={{ left: 10, right: 10 }}
            tick={<CustomizedAxisTick />}
          />
          <YAxis />
          <Tooltip viewBox={{ x: 0, y: 0, width: 400, height: 400 }} />
          <CartesianGrid stroke="#eee" vertical={false} />
          <Line
            dataKey="totalBots"
            stroke="#64B5F6"
            strokeWidth="2"
            dot={false}
          />
          <Line
            dataKey="activeBots"
            stroke="#F06292"
            strokeWidth="2"
            dot={false}
          />
        </LineChart>
      </ResponsiveContainer>
    );
  }
}

export default BotGrowth;
