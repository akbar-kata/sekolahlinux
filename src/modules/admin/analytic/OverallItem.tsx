import React from 'react';

interface Props {
  title: string;
  icon: string;
  value: number;
  isLoading: boolean;
  error: string | null;
}

interface States {}

const iconStyle = {
  fontSize: '50px',
  float: 'left' as 'left',
  marginRight: '20px'
};

const titleStyle = {
  fontWeight: 400 as 400
};

const valueStyle = {
  color: '#328af3',
  fontWeight: 100 as 100,
  fontSize: 30
};

class OverallItem extends React.PureComponent<Props, States> {
  renderValue() {
    if (this.props.isLoading) {
      return <div>Loading...</div>;
    }

    if (this.props.error !== null) {
      return <div className="text-danger">{this.props.error}</div>;
    }

    return <div style={valueStyle}>{this.props.value}</div>;
  }

  render() {
    const { title, icon } = this.props;
    return (
      <div>
        <div className="pt0">
          <div style={iconStyle}>
            {icon.length > 1 && <i className={icon} />}
          </div>
          <div style={titleStyle}>{title}</div>
          {this.renderValue()}
        </div>
      </div>
    );
  }
}

export default OverallItem;
