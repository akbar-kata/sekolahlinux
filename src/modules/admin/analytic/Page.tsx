import React from 'react';

import { DateRangePicker } from 'modules/analytic/date';
import Overall from './OverallContainer';
import BotsGrowth from './BotsGrowthContainer';
import UsersGrowth from './UsersGrowthContainer';

export default () => (
  <div>
    <div>
      Overall analytic
      <p>One eye seeing all</p>
      <div className="breadcrumb-env">
        <DateRangePicker />
      </div>
    </div>
    <Overall />
    <div>
      <div>
        <h4 className="text-gray">Bots Growth</h4>
      </div>
      <div>
        <div style={{ height: 300, width: '100%', fontSize: 13 }}>
          <BotsGrowth />
        </div>
      </div>
    </div>
    <div>
      <div>
        <h4 className="text-gray">Users Growth</h4>
      </div>
      <div>
        <div style={{ height: 300, width: '100%', fontSize: 13 }}>
          <UsersGrowth />
        </div>
      </div>
    </div>
  </div>
);
