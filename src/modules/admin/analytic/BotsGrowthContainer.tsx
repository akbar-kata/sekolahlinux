import React from 'react';
import { connect } from 'react-redux';

import RootStore from 'interfaces/rootStore';

import { botsGrowthRequest } from 'stores/analytic/overall/actions';
import { getAnalyticDate } from 'stores/analytic/selectors';
import {
  getAnalyticAllLoading,
  getAnalyticAllBotsGrowth,
  getAnalyticAllError
} from 'stores/analytic/overall/selectors';

import BotGrowth from './BotsGrowth';

interface PropsFromState {
  isLoading: boolean;
  error: string | null;
  data: any;
  startDate: number;
  endDate: number;
}

interface PropsFromDispatch {
  fetchAction: Function;
}

class BotsGrowthContainer extends React.Component<any, any> {
  componentDidMount() {
    const { startDate, endDate } = this.props;
    this.fetchData(startDate, endDate);
  }

  componentWillReceiveProps(nextProps: any) {
    const { startDate, endDate } = this.props;
    if (startDate !== nextProps.startDate || endDate !== nextProps.endDate) {
      this.fetchData(nextProps.startDate, nextProps.endDate);
    }
  }

  fetchData = (startDate: number, endDate: number) => {
    this.props.fetchAction(startDate, endDate);
  };

  render() {
    return (
      <BotGrowth
        isLoading={this.props.isLoading}
        error={this.props.error}
        data={this.props.data}
      />
    );
  }
}

const mapStateToProps = ({ analytic }: RootStore): PropsFromState => {
  const dateRage = getAnalyticDate(analytic);
  return {
    isLoading: getAnalyticAllLoading(analytic.overall, 'overall'),
    error: getAnalyticAllError(analytic.overall, 'overall'),
    data: getAnalyticAllBotsGrowth(analytic.overall),
    startDate: dateRage.start,
    endDate: dateRage.end
  };
};

const mapDispatchToProps = (dispatch: Function): PropsFromDispatch => {
  return {
    fetchAction: (startDate: number, endDate: number) =>
      dispatch(botsGrowthRequest(startDate, endDate))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BotsGrowthContainer);
