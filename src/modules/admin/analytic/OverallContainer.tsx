import React from 'react';
import { connect } from 'react-redux';

import RootStore from 'interfaces/rootStore';

import { overallRequest } from 'stores/analytic/overall/actions';
import { getAnalyticDate } from 'stores/analytic/selectors';
import {
  getAnalyticAllLoading,
  getAnalyticAllOverall,
  getAnalyticAllError
} from 'stores/analytic/overall/selectors';

import OverallItem from './OverallItem';

interface PropsFromState {
  isLoading: boolean;
  error: string | null;
  data: any;
  startDate: number;
  endDate: number;
}

interface PropsFromDispatch {
  fetchAction: Function;
}

class BotsGrowthContainer extends React.Component<any, any> {
  componentDidMount() {
    const { startDate, endDate } = this.props;
    this.fetchData(startDate, endDate);
  }

  componentWillReceiveProps(nextProps: any) {
    const { startDate, endDate } = this.props;
    if (startDate !== nextProps.startDate || endDate !== nextProps.endDate) {
      this.fetchData(nextProps.startDate, nextProps.endDate);
    }
  }

  fetchData = (startDate: number, endDate: number) => {
    this.props.fetchAction(startDate, endDate);
  };

  render() {
    return (
      <div className="row">
        <div className="col-md-3">
          <OverallItem
            isLoading={this.props.isLoading}
            error={this.props.error}
            title="Total Bots"
            icon="fa fa-android"
            value={this.props.data.botsTotal || 0}
          />
        </div>
        <div className="col-md-3">
          <OverallItem
            isLoading={this.props.isLoading}
            error={this.props.error}
            title="Total Users"
            icon="fa fa-users"
            value={this.props.data.usersTotal || 0}
          />
        </div>
        <div className="col-md-3">
          <OverallItem
            isLoading={this.props.isLoading}
            error={this.props.error}
            title="Users built Bots"
            icon="fa fa-user-md"
            value={this.props.data.usersBuiltBot || 0}
          />
        </div>
        <div className="col-md-3">
          <OverallItem
            isLoading={this.props.isLoading}
            error={this.props.error}
            title="Bots per Users"
            icon="fa fa-percent"
            value={this.props.data.botsPerUser || 0}
          />
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ analytic }: RootStore): PropsFromState => {
  const dateRage = getAnalyticDate(analytic);
  return {
    isLoading: getAnalyticAllLoading(analytic.overall, 'overall'),
    error: getAnalyticAllError(analytic.overall, 'overall'),
    data: getAnalyticAllOverall(analytic.overall),
    startDate: dateRage.start,
    endDate: dateRage.end
  };
};

const mapDispatchToProps = (dispatch: Function): PropsFromDispatch => {
  return {
    fetchAction: (startDate: number, endDate: number) =>
      dispatch(overallRequest(startDate, endDate))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BotsGrowthContainer);
