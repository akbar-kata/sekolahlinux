import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';

import CurrentUser from 'containers/CurrentUserContainer';
import { Robot } from 'components/Loading';

const Analytic = React.lazy(() => import('./analytic'));
const Pricing = React.lazy(() => import('./pricing'));
const Subscription = React.lazy(() => import('./subscription'));

const Users: React.SFC = () => <div>User component</div>;
const InvoiceDummy: React.SFC = () => <div>Invoice component</div>;

const parentPath = 'admin';

export default () => {
  return (
    <CurrentUser>
      {(params: any) =>
        params && params.user && params.user !== 'admin' ? (
          <Route render={() => <Redirect to={`/`} />} />
        ) : (
          <React.Suspense fallback={<Robot />}>
            <Switch>
              <Route path={`/${parentPath}/analytic`} component={Analytic} />
              <Route path={`/${parentPath}/users`} component={Users} />
              <Route path={`/${parentPath}/pricing`} component={Pricing} />
              <Route
                path={`/${parentPath}/subscription`}
                component={Subscription}
              />
              <Route path={`/${parentPath}/invoice`} component={InvoiceDummy} />
              <Route
                render={() => <Redirect to={`/${parentPath}/analytic`} />}
              />
            </Switch>
          </React.Suspense>
        )
      }
    </CurrentUser>
  );
};
