import * as React from 'react';
import EmptyMessage from 'components/Common/EmptyMessage';
import withSidebar, {
  WithSidebarDispatches,
  WithSidebarStates
} from 'stores/app/sidebar/context';

const style = {
  paddingTop: '48px'
};

import './style.scss';

const Logo = require('assets/images/logo-platform.png');

type MaintenanceProps = WithSidebarStates & WithSidebarDispatches;

class Maintenance extends React.Component<MaintenanceProps> {
  render() {
    return (
      <div className="maintenance-wrapper">
        <div className="text-left mr-2">
          <img src={Logo} className="maintenance-logo" alt="logo" />
        </div>
        <div className="maintenance-board" style={style}>
          <EmptyMessage
            autoSize
            image={require('assets/images/maintenance.svg')}
            title="Great Things Are On The Way"
          >
            We are currently upgrading our system to serve you better. Thank you
            for your patience.
            <br />
            Need help? Please reach out to{' '}
            <a href="mailto:support@kata.ai">
              <strong>support@kata.ai</strong>
            </a>
          </EmptyMessage>
        </div>
      </div>
    );
  }
}

export default withSidebar(Maintenance);
