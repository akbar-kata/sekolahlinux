const gulp = require('gulp');
const sonarqubeScanner = require('sonarqube-scanner');

const serverUrl = process.env.SONAR_URL;
const token = process.env.SONAR_TOKEN;
const repoSlug = process.env.BITBUCKET_REPO_SLUG;
const accountName = process.env.BITBUCKET_REPO_OWNER;
const oauthClientKey = process.env.OAUTH_CLIENT_KEY;
const oauthClientSecret = process.env.OAUTH_CLIENT_SECRET;
const branchName = process.env.BITBUCKET_BRANCH;

gulp.task('sonar', function(callback) {
  if (serverUrl) {
    sonarqubeScanner(
      {
        serverUrl,
        token,
        options: {
          'sonar.bitbucket.repoSlug': repoSlug,
          'sonar.bitbucket.accountName': accountName,
          'sonar.bitbucket.oauthClientKey': oauthClientKey,
          'sonar.bitbucket.oauthClientSecret': oauthClientSecret,
          'sonar.bitbucket.branchName': branchName,
          'sonar.host.url': serverUrl,
          'sonar.login': token,
          'sonar.analysis.mode': 'issues'
        }
      },
      callback
    );
  } else {
    console.log('No SONAR_URL provided, quitting...');
  }
});
