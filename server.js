const path = require('path');
const express = require('express');
const compression = require('compression');
const cookieParser = require('cookie-parser');

const port = normalizePort(process.env.SERVICE_PORT || 8080);

const securityHeaders = () => (req, res, next) => {
  res.header('Strict-Transport-Security', 7776000000);
  res.header('X-Frame-Options', 'DENY');
  res.header('X-XSS-Protection', 0);
  res.header('X-Content-Type-Options', 'nosniff');
  next();
};

let app = express();
app.use(compression());
app.use(cookieParser());
app.use(securityHeaders());

function normalizePort(val) {
  const port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}

// Only environment variables with `REACT_APP_RUNTIME` prefix can be included.
const REACT_APP_RUNTIME = /^REACT_APP_RUNTIME_/i;

function getEnv() {
  const raw = Object.keys(process.env)
    .filter(key => REACT_APP_RUNTIME.test(key))
    .reduce((env, key) => {
      env[key] = process.env[key];
      return env;
    }, {});

  return raw;
}

app.use(express.static(path.join(process.cwd(), 'build')));

app.use('/runtime-envs.js', (req, res) => {
  const env = getEnv();
  const response = `window.__KATA_RUNTIME__=${JSON.stringify(env)};`;
  res.setHeader('content-type', 'application/javascript');
  res.end(response);
});

app.get('*', (req, res) => {
  res.sendFile(path.resolve(__dirname, 'build/index.html'));
});

app.listen(port, () => {
  console.log(`Kata Platform is running at http://localhost:${port}/`);
  console.log('Open your browser and enter that url');
});
