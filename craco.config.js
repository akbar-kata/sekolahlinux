const path = require('path');
const reactHotReloadPlugin = require('craco-plugin-react-hot-reload');
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');

module.exports = {
  plugins: [
    {
      plugin: reactHotReloadPlugin
    }
  ],
  webpack: {
    alias: {
      assets: path.resolve(__dirname, 'src/assets/'),
      components: path.resolve(__dirname, 'src/components/'),
      containers: path.resolve(__dirname, 'src/containers/'),
      interfaces: path.resolve(__dirname, 'src/interfaces/'),
      modules: path.resolve(__dirname, 'src/modules/'),
      stores: path.resolve(__dirname, 'src/stores/'),
      utils: path.resolve(__dirname, 'src/utils/')
    },
    configure: (config, { env, paths }) => {
      // Add `webpack-bundle-analyzer` if `ANALYZE` env is set.
      if (env === 'production' && process.env.ANALYZE) {
        config.plugins.push(
          new BundleAnalyzerPlugin({
            analyzerMode: 'static',
            reportFilename: path.join(
              __dirname,
              '../webpack-bundle-analyzer/report.html'
            )
          })
        );
      }

      return config;
    }
  }
};
