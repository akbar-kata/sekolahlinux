import { Selector } from 'testcafe';

class Base {
  contextSelector: Selector;

  constructor(context?: string, baseClass?: string) {
    this.contextSelector = context
      ? Selector(context)
      : baseClass
        ? Selector(baseClass)
        : Selector('body');
  }

  selectWithId(id: string) {
    return Selector(`[data-test-id="${id}"]`);
  }

  selectElement(elementClass: string, index: number = 0) {
    return this.contextSelector.find(elementClass).nth(index)();
  }

  countElements(elementClass: string) {
    return this.contextSelector.find(elementClass).count;
  }
}

export default Base;
