import Base from './Base';

class Login extends Base {
  constructor(context?: string) {
    super(context, 'auth-wrapper');
  }

  getInputUsername() {
    return this.selectWithId('auth-login-username');
  }

  getInputPassword() {
    return this.selectWithId('auth-login-password');
  }

  getButtonSubmit() {
    return this.selectWithId('auth-login-submit');
  }

  getBanner() {
    return this.selectElement('kata-banner');
  }
}

export default Login;
