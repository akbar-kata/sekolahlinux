import { ClientFunction } from 'testcafe';

import LoginModel from './Model/Login';
import Config from './Config';

const model = new LoginModel();

fixture('Login').page(`${Config.url}/login`);

test('Login failed', async (t: TestController) => {
  await t
    .typeText(model.getInputUsername(), 'fake')
    .typeText(model.getInputPassword(), 'fake')
    .click(model.getButtonSubmit())
    .expect(model.getBanner().hasClass('kata-banner--error')).ok;
});

test('Login success', async (t: TestController) => {
  const getLocation = ClientFunction(() => document.location.href.toString());

  await t
    .typeText(model.getInputUsername(), 'made')
    .typeText(model.getInputPassword(), 'made')
    .click(model.getButtonSubmit())
    .expect(getLocation())
    .contains('botstudio');
});
