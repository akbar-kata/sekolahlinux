process.env.TEST_URL_CI =
  process.env.TEST_URL_CI || 'https://new-platform.katalabs.io';
process.env.TEST_URL_PROD =
  process.env.TEST_URL_PROD || 'https://platform.kata.ai';

const env = process.env.NODE_ENV || 'development';
let url = '';

if (env === 'production') {
  url = process.env.TEST_URL_PROD;
} else if (env === 'CI') {
  url = process.env.TEST_URL_CI;
} else {
  url = 'localhost:3000';
}

export default {
  env,
  url
};
